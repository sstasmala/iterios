<?php
/**
 * translations.map.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

return [
    'pages' => [
        '/' => [
            'signup_user_email_error' => [
                'en' => 'A user with this email already exists!',
                'ru' => 'Пользователь с этим e-mail\'ом уже существует!'
            ],
            'signup_rules_error' => [
                'ru' => 'Вы не приняли пользовательское соглашение!'
            ],
            'signup_user_pass_error' => [
                'en' => 'Passwords do not match!',
                'ru' => 'Пароли не равны!'
            ],
            'login_user_email_error' => [
                'en' => 'A user with this email does not exist!',
                'ru' => 'Пользователь с этим e-mail\'ом не существует!'
            ],
            'login_user_pass_error' => [
                'en' => 'Incorrect password or this user can be blocked!',
                'ru' => 'Неверный пароль или этот пользователь заблокирован!'
            ],
            'try_iterios' => [
                'en' => 'TRY ITERIOS. IT’S FREE'
            ],
            'try_iterios_desc' => [
                'en' => 'You can try any advanced travel automation solution by ITERIOS for free'
            ],
            'sign_up_desc' => [
                'en' => 'Please register to try ITERIOS solutions:'
            ],
            'login' => [
                'en' => 'Login To Your Account'
            ],
            'remember_me' => [
                'en' => 'Remember me'
            ],
            'f_pass' => [
                'en' => 'Forgot Password ?'
            ],
            'agree_text1' => [
                'en' => 'I accept conditions of the'
            ],
            'public_offer' => [
                'en' => 'Public offer'
            ],
            'legal_note' => [
                'en' => 'Legal note'
            ],
            'agree_text2' => [
                'en' => 'and I’ve read the'
            ],
            'ft_pass' => [
                'en' => 'Forgotten Password ?'
            ],
            'ft_pass_text' => [
                'en' => 'Enter your email to reset your password:'
            ],
            'complete_registration' => [
                'en' => 'To complete your registration please check your email.',
                'ru' => 'Чтобы завершить регистрацию, проверьте свой адрес электронной почты.'
            ],
            'ft_pass_success' => [
                'en' => 'Password recovery instruction has been sent to your email.',
                'ru' => 'Инструкция по восстановлению пароля была отправлена ​​на ваш адрес электронной почты'
            ],
            'user_blocked_tenant' => [
                'en' => 'Blocked!',
                'ru' => 'Заблокирован!'
            ],
            'user_blocked_oops' => [
                'en' => 'Oops!',
                'ru' => 'Ой!'
            ],
            'user_blocked_notification' => [
                'en' => 'You are Blocked in this Tenant!',
                'ru' => 'Вы заблокированы в этом Тенанте!'
            ],
            'added_user' => [
                'en' => 'Congratulations!',
                'ru' => 'Поздравляем!'
            ],
            'added_user_notification' => [
                'en' => 'You are successfully added to the tenant',
                'ru' => 'Вы успешно добавлены в тенант'
            ],
            'added_user_button' => [
                'en' => 'Go to',
                'ru' => 'Перейти в'
            ],
        ],
        '/create_tenant' => [
            'create_company' => [
                'en' => 'Create company'
            ],
            'create_company_desc' => [
                'en' => 'To continue it is necessary to create a company. Enter company name:'
            ],
            'create_company_success' => [
                'en' => 'Success!',
                'ru' => 'Успешно!'
            ],
        ],
        '/change_password' => [
            'change_pass' => [
                'en' => 'Change password',
                'ru' => 'Изменить пароль'

            ],
            'change_pass_desc1' => [
                'en' => 'You requested a password change from email',
                'ru' => 'Вы запросили изменение пароля по электронной почте'
            ],
            'change_pass_desc2' => [
                'en' => 'Enter new password:',
                'ru' => 'Введите новый пароль:'
            ],
            'change_pass_token_error' => [
                'en' => 'The token\'s lifetime has expired or token does not exist!',
                'ru' => 'Срок действия токена истек или токен не существует!'
            ],
            'change_pass_success' => [
                'en' => 'Password successfully changed!',
                'ru' => 'Пароль успешно изменен!'
            ],
        ],
        'ita/profile/index' => [
            'personal_details_success' => [
                'en' => 'Personal details successfully changed!',
                'ru' => 'Личные данные успешно изменены!',
            ],
            'profile_personal_details' => [
                'en' => 'Personal details'
            ],
            'personal_details_error' => [
                'en' => 'Personal details not save!'
            ],
            'profile_change_email_success' => [
                'en' => 'Email successfully changed!'
            ],
            'profile_update' => [
                'en' => 'Update Profile'
            ],
            'profile_change_picture' => [
                'en' => 'Change profile picture'
            ],
            'profile_notifications' => [
                'en' => 'Notifications',
                'ru' => 'Уведомления',
            ],
            'profile_enter_pass_again' => [
                'en' => 'Enter new password again'
            ],
            'profile_enter_pass_confirm' => [
                'en' => 'Enter your password to confirm'
            ],
            'profile_your_pass' => [
                'en' => 'Your password'
            ],
            'profile_your_first_name' => [
                'en' => 'Your first name'
            ],
            'profile_your_last_name' => [
                'en' => 'Your last name'
            ],
            'profile_your_middle_name' => [
                'en' => 'Your middle name'
            ],
            'profile_your_phone' => [
                'en' => 'Your phone number'
            ],
            'profile_picture_upload' => [
                'en' => 'Profile picture upload'
            ],
            'profile_picture_upload_title' => [
                'en' => 'Drop picture here or click to upload.'
            ],
            'profile_picture_upload_desc' => [
                'en' => 'Your profile picture will change after uploading a new image.'
            ],
            'profile_picture_upload_success' => [
                'en' => 'Profile picture successfully changed!'
            ],
            'profile_picture_upload_error' => [
                'en' => 'Profile picture not uploaded!'
            ],
            'profile_change_email_error' => [
                'en' => 'Email or Password is empty!'
            ],
            'profile_pass_error' => [
                'en' => 'Incorrect password!'
            ],
            'profile_fields_empty_error' => [
                'en' => 'Fill in all the fields!'
            ],
            'profile_add_tag_for_contact_title' => [
                'en' => 'Add tag for contact',
                'ru' => 'Добавлен тэг для контакта'
            ],
            'profile_overdue_task_title' => [
                'en' => 'Overdue task',
                'ru' => 'Просрочена задача'
            ],
            'profile_changed_request_status_title' => [
                'en' => 'Changed request status',
                'ru' => 'Изменен статус запроса'
            ],
            'profile_add_tag_lable' => [
                'en' => 'Select tags',
                'ru' => 'Выберите теги'
            ],
            'profile_tracking_level_lable' => [
                'en' => 'Tracking level',
                'ru' => 'Уровень отслеживания'
            ],
            'profile_channel_lable' => [
                'en' => 'Channel',
                'ru' => 'Канал'
            ],
            'profile_channel_notice_label' => [
                'en' => 'Notice',
                'ru' => 'Уведомление',
            ],
            'profile-notifications-tags-edit_placeholder' => [
                'en' => 'Select Tags',
                'ru' => 'Выбрать теги',
            ],
            'profile-notifications-statuses-edit_placeholder' => [
                'en' => 'Select Status',
                'ru' => 'Выберите статус',
            ],
            'profile_add_status_lable' => [
                'en' => 'Select Status',
                'ru' => 'Выбрать статус',
            ],
        ],
        'ita/dashboard/index' => [
        ],
        'ita/tasks/index' => [
            'tasks_page_title' => [
                'en' => 'Tasks',
                'ru' => 'Задачи',
            ],
            'tasks_page_tasks_filter_label' => [
                'en' => 'Tasks',
                'ru' => 'Задачи',
            ],
            'tasks_page_calendar_filter_label' => [
                'en' => 'Calendar',
                'ru' => 'Календарь',
            ],
            'all_tasks_filter_name' => [
                'en' => 'All tasks',
                'ru' => 'Все задачи',
            ],
            'tasks_datatable_column_title_title' => [
                'en' => 'Title',
                'ru' => 'Название',
            ],
            'tasks_datatable_column_type_title' => [
                'en' => 'Type',
                'ru' => 'Тип',
            ],
            'tasks_datatable_associated_with_title' => [
                'en' => 'Associated with',
                'ru' => 'Назначена',
            ],
            'tasks_datatable_due_date_title' => [
                'en' => 'Due date',
                'ru' => 'Срок',
            ],
            'add_task' => [
                'en' => 'Add task',
                'ru' => 'Добавить задачу',
            ],
            'create_task_button' => [
                'en' => 'Create task',
                'ru' => 'Создать задачу',
            ],
            'create_task_input_title_placeholder' => [
                'en' => 'Task title',
                'ru' => 'Название задачи',
            ],
            'edit_task' => [
                'en' => 'Edit task',
                'ru' => 'Редактировать задачу',
            ],
            'task_edit_modal_email_reminder_title' => [
                'en' => 'Mail reminder',
                'ru' => 'Mail напоминание',
            ],
            'edit_task_note_redactor_title' => [
                'en' => 'Notes',
                'ru' => 'Заметки',
            ],
            'edit_task_note_redactor_placeholder' => [
                'en' => 'Your notes...',
                'ru' => 'Ваша заметка...',
            ],
            'edit_task_type_select_placeholder' => [
                'en' => 'Select type',
                'ru' => 'Выберите тип'
            ],
            'edit_task_assigned_to_title' => [
                'en' => 'Assigned to',
                'ru' => 'Назначена на'
            ],
            'edit_task_assigned_to_select_placeholder' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите исполнителя'
            ],
            'tasks_delete_message1' => [
                'en' => 'Are you sure to delete this',
                'ru' => 'Вы уверены что хотите удалить'
            ],
            'tasks_delete_message2' => [
                'en' => 'items?',
                'ru' => 'задачи'
            ],
            'tasks_delete_item_message' => [
                'en' => 'Are you sure you want to delete this item?',
                'ru' => 'Вы уверены, что хотите удалить эту задачу?'
            ],
            'tasks_filter_open' => [
                'en' => 'Open tasks',
                'ru' => 'Открытые задачи'
            ],
            'tasks_filter_overdue' => [
                'en' => 'Overdue tasks',
                'ru' => 'Просроченные задачи'
            ],
            'tasks_filter_completed' => [
                'en' => 'Completed tasks',
                'ru' => 'Завершенные задачи'
            ],
            'filter_task_title' => [
                'en' => 'Task title',
                'ru' => 'Наименование'
            ],
            'filter_task_responsible' => [
                'en' => 'Task responsible',
                'ru' => 'Ответственный'
            ],
            'filter_task_author' => [
                'en' => 'Task author',
                'ru' => 'Создатель'
            ],
            'filter_task_type' => [
                'en' => 'Type task',
                'ru' => 'Тип'
            ],
            'filter_task_date_completion' => [
                'en' => 'Date of completion',
                'ru' => 'Дата выполнения'
            ],
            'filter_task_date_create' => [
                'en' => 'Created at',
                'ru' => 'Дата создания'
            ],
            'filter_task_date_update' => [
                'en' => 'Updated at',
                'ru' => 'Дата обновления'
            ],
            'tasks_calendar_filters_birthday' => [
                'en' => 'Birthday',
                'ru' => 'День рождения'
            ],
            'tasks_calendar_filters_task_complete' => [
                'en' => 'Сompleted tasks',
                'ru' => 'Выполненые задачи'
            ],
            'tasks_calendar_filters_request_date_end' => [
                'en' => 'Request date end',
                'ru' => 'Дата окончания запроса'
            ],
            'tasks_calendar_filters_holiday' => [
                'en' => 'Holiday',
                'ru' => 'Праздники'
            ],
            'tasks_calendar_task_number' => [
                'en' => 'Task #',
                'ru' => 'Задача №'
            ],
            'tasks_calendar_request_number' => [
                'en' => 'Request #',
                'ru' => 'Запрос №'
            ],
            'tasks_create_associated' => [
                'en' => 'Associated with',
                'ru' => 'Связать с'
            ],
            'tasks_create_associated_with_contact' => [
                'en' => 'Add a contact',
                'ru' => 'Добавить контакт'
            ],
            'tasks_create_associated_with_company' => [
                'en' => 'Add a company',
                'ru' => 'Добавить компанию'
            ],
            'tasks_create_associated_with_order' => [
                'en' => 'Add a order',
                'ru' => 'Добавить заявку'
            ],
            'tasks_create_select_contact' => [
                'en' => 'Select contact',
                'ru' => 'Выберите контакт'
            ],
            'tasks_create_select_company' => [
                'en' => 'Select company',
                'ru' => 'Выберите компанию'
            ],
            'tasks_create_select_order' => [
                'en' => 'Select order',
                'ru' => 'Выберите заявку'
            ],
            'tasks_month' => [
                'en' => 'month',
                'ru' => 'месяц',
            ],
            'tasks_week' => [
                'en' => 'week',
                'ru' => 'неделя',
            ],
            'tasks_day' => [
                'en' => 'day',
                'ru' => 'день',
            ],
            'tasks_list' => [
                'en' => 'list',
                'ru' => 'список',
            ],
            'tasks_company' => [
                'en' => 'Company',
                'ru' => 'Компания'
            ],
            'tasks_contact_name' => [
                'en' => 'Contact name',
                'ru' => 'Имя контакта'
            ]
        ],
        'ita/users/index' => [
            'users_page_title' => [
                'en' => 'Users',
                'ru' => 'Пользователи'
            ],
            'add_user_button_name' => [
                'en' => 'Add user',
                'ru' => 'Добавить пользователя'
            ],
            'users_datatable_column_fio_title' => [
                'en' => 'Name',
                'ru' => 'ФИО'
            ],
            'users_datatable_column_post_title' => [
                'en' => 'Post',
                'ru' => 'Должность'
            ],
            'users_datatable_column_phone_title' => [
                'en' => 'Phone',
                'ru' => 'Телефон'
            ],
            'users_datatable_column_access_title' => [
                'en' => 'Access',
                'ru' => 'Доступ'
            ],
            'users_datatable_column_operations_title' => [
                'en' => 'Operations',
                'ru' => 'Операции'
            ],
            'users_new_popup_title' => [
                'en' => 'New user',
                'ru' => 'Новый пользователь'
            ],
            'users_edit_popup_title' => [
                'en' => 'Edit user',
                'ru' => 'Редактировать пользователья'
            ],
            'users_delete_popup_title' => [
                'en' => 'Dismiss user',
                'ru' => 'Уволить пользователя'
            ],
            'user_delete_transfer_data' => [
                'en' => 'Transfer all data to:',
                'ru' => 'Передать все данные на:'
            ],
            'user_delete_not_transfer_data' => [
                'en' => 'Not transfer data',
                'ru' => 'Не передавать данные'
            ],
            'users_new_popup_email_select_placeholder' => [
                'en' => 'Search for an existing user by email',
                'ru' => 'Поиск существующего пользователя по email'
            ],
            'user_create_access_level_title' => [
                'en' => 'Access level',
                'ru' => 'Уровень доступа',
            ],
            'user_create_personal_data_title' => [
                'en' => 'Personal data',
                'ru' => 'Личные данные',
            ],
            'generate_password_button' => [
                'en' => 'Generate',
                'ru' => 'Генерировать',
            ],
            'user_delete_dismiss' => [
                'en' => 'Dismiss',
                'ru' => 'Уволить',
            ],
            'user_delete_tourists_counter_title' => [
                'en' => 'Tourists',
                'ru' => 'Туристов',
            ],
            'user_delete_reservations_counter_title' => [
                'en' => 'Reservations',
                'ru' => 'Бронировок',
            ],
            'user_delete_requests_counter_title' => [
                'en' => 'Requests',
                'ru' => 'Запросов',
            ],
            'user_delete_bids_counter_title' => [
                'en' => 'Bids',
                'ru' => 'Заявок',
            ],
            'user_create_notification_oops' => [
                'en' => 'Oops!',
                'ru' => 'Ой!'
            ],
            'user_create_notification_user_exist' => [
                'en' => 'User already exists in this tenant!',
                'ru' => 'Пользователь уже существует в этом тенанте!'
            ],
            'user_create_notification_well_done' => [
                'en' => 'Well done!',
                'ru' => 'Успех!'
            ],
            'user_create_notification_user_added' => [
                'en' => 'You successfully added user to tenant. Invite sended to email. User will appear after confirmation.',
                'ru' => 'Вы успешно добавили пользователя в тенант. Приглашение отправлено на почту. Пользователь появится после подтверждения.'
            ],
            'user_delete_tags' => [
                'en' => 'Assign tags to records:',
                'ru' => 'Присвоить записям тэг:'
            ],
            'user_repeat_email_notification' => [
                'en' => 'Send again',
                'ru' => 'Повторно отправить',
            ],
            'user_repeat_email_notification_hint' => [
                'en' => 'Resend user invitation email',
                'ru' => 'Повторно отправить приглашение на email пользователя',
            ],
            'user_repeat_notification_success' => [
                'en' => 'You have successfully resubmitted an invitation!',
                'ru' => 'Вы успешно отправили повторное приглашение пользователю!',
            ],
            'icon_tenant_owner' => [
                'en' => 'Owner',
                'ru' => 'Владелец',
            ],
            'icon_not_confirm_user' => [
                'en' => 'Not confirm user',
                'ru' => 'Не подтвержденный пользователь'
            ],
            'delete_not_confirm_text' => [
                'en' => 'Do you want to delete this user?',
                'ru' => 'Вы хотите удалить этого пользователя?'
            ],
            'delete_user_success' => [
                'en' => 'User deleted!',
                'ru' => 'Пользователь удален!'
            ],
            'delete_modal_bulk_select_tags' => [
                'en' => 'Select tags',
                'ru' => 'Выберите теги',
            ],
        ],
        'ita/roles/index' => [
            'roles_page_title' => [
                'en' => 'Roles',
                'ru' => 'Роли',
            ],
            'add_role' => [
                'en' => 'Add role',
                'ru' => 'Добавить роль',
            ],
            'roles_table_actions_column_title' => [
                'en' => 'Actions',
                'ru' => 'Операции',
            ],
            'roles_table_name_column_title' => [
                'en' => 'Name',
                'ru' => 'Название',
            ],
            'roles_table_employees_column_title' => [
                'en' => 'Employees',
                'ru' => 'Сотрудники',
            ],
            'roles_delete_notification_error' => [
                'en' => 'You can not delete this role because there are users in it!',
                'ru' => 'Вы не можете удалить данную роль, так как в ней присутствуют пользователи!'
            ],
            'roles_delete_notification' => [
                'en' => 'You won\'t be able to revert this!',
                'ru' => 'Вы не сможете это вернуть!'
            ],
            'roles_delete_oops' => [
                'en' => 'Oops!',
                'ru' => 'Ой!'
            ]
        ],
        'ita/roles/create' => [
            'create_roles_page_integration' => [
                'en' => 'Integration',
                'ru' => 'Интеграция'
            ],
            'create_roles_page_widgets' => [
                'en' => 'Widgets',
                'ru' => 'Виджеты'
            ],
            'create_roles_page_title' => [
                'en' => 'Create new role',
                'ru' => 'Создать новую роль',
            ],
            'create_roles_page_name_input_label' => [
                'en' => 'Role name',
                'ru' => 'Название роли',
            ],
            'create_roles_page_name_input_placeholder' => [
                'en' => 'Enter role name',
                'ru' => 'Введите название роли',
            ],
            'create_roles_page_access_rights_title' => [
                'en' => 'Access rights',
                'ru' => 'Права доступа',
            ],
            
            'create_roles_table_modules' => [
                'en' => 'Module',
                'ru' => 'Модуль'
            ],
            'create_roles_table_tasks' => [
                'en' => 'Tasks',
                'ru' => 'Задачи'
            ],
            'create_roles_table_tour_search' => [
                'en' => 'Tour search',
                'ru' => 'Поиск тура'
            ],
            'create_roles_table_requests' => [
                'en' => 'Requests',
                'ru' => 'Запросы'
            ],
            'create_roles_table_sales' => [
                'en' => 'Sales',
                'ru' => 'Продажи'
            ],
            'create_roles_table_sales_overviews' => [
                'en' => 'Sales overviews',
                'ru' => 'Обзор продаж'
            ],
            'create_roles_table_deals' => [
                'en' => 'Deals',
                'ru' => 'Сделки'
            ],
            'create_roles_table_reservations' => [
                'en' => 'Reservations',
                'ru' => 'Бронировки'
            ],
            'create_roles_table_contractors' => [
                'en' => 'Contractors',
                'ru' => 'Контрагенты'
            ],
            'create_roles_table_contacts' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'create_roles_table_companies' => [
                'en' => 'Companies',
                'ru' => 'Компании'
            ],
            'create_roles_table_marketing' => [
                'en' => 'Marketing',
                'ru' => 'Маркетинг'
            ],
            'create_roles_table_mailing' => [
                'en' => 'Mailing',
                'ru' => 'Рассылка'
            ],
            'create_roles_table_segments' => [
                'en' => 'Segments',
                'ru' => 'Сегменты'
            ],
            'create_roles_table_reminders' => [
                'en' => 'Reminders',
                'ru' => 'Напоминалки'
            ],
            'create_roles_table_reports' => [
                'en' => 'Reports',
                'ru' => 'Отчеты'
            ],
            'create_roles_table_settings' => [
                'en' => 'Settings',
                'ru' => 'Настройки'
            ],
            'create_roles_table_users' => [
                'en' => 'Users',
                'ru' => 'Пользователи'
            ],
            'create_roles_table_constructors' => [
                'en' => 'Constructors',
                'ru' => 'Констсрукторы'
            ],
            'create_roles_table_accounts_and_payments' => [
                'en' => 'Accounts and payments',
                'ru' => 'Счета и платежи'
            ],
            'create_roles_table_var_allowed' => [
                'en' => 'Allowed',
                'ru' => 'Разрешено'
            ],
            'create_roles_table_var_forbidden' => [
                'en' => 'Forbidden',
                'ru' => 'Запрещено'
            ],
            'create_roles_table_var_full_access' => [
                'en' => 'Full access',
                'ru' => 'Полный доступ'
            ],
            'create_roles_table_var_only_my_role' => [
                'en' => 'Only my role',
                'ru' => 'Только своей роли'
            ],
            'create_roles_table_var_only_own' => [
                'en' => 'Only own',
                'ru' => 'Только свои'
            ],
            'create_roles_table_col_title_reading' => [
                'en' => 'Reading',
                'ru' => 'Чтение'
            ],
            'create_roles_table_col_title_creating' => [
                'en' => 'Creating',
                'ru' => 'Создание'
            ],
            'create_roles_table_col_title_updating' => [
                'en' => 'Updating',
                'ru' => 'Изменение'
            ],
            'create_roles_table_col_title_deleting' => [
                'en' => 'Deleting',
                'ru' => 'Удаление'
            ],
            'create_roles_table_empty_cell_message' => [
                'en' => 'Not available',
                'ru' => 'Не доступно'
            ]
        ],
        'ita/roles/update' => [
            'update_role_page_title' => [
                'en' => 'Update role',
                'ru' => 'Редактировать роль'
            ]
        ],
        'ita/contacts/index' => [
            'contacts_create' => [
                'en' => 'Create contact'
            ],
            'contacts_search_notification' => [
                'en' => 'Look here! We already have several similar clients.'
            ],
            'contacts_search__contact_notification1' => [
                'en' => 'Oops! It looks like a contact with the email address'
            ],
            'contacts_search__contact_notification2' => [
                'en' => 'already exists.'
            ],
            'contacts_field_title_full_name' => [
                'en' => 'Full name',
                'ru' => 'ФИО'
            ],
            'contacts_field_title_gender' => [
                'en' => "Gender",
                'ru' => 'Пол'
            ],
            'contacts_field_title_contacts' => [
                'en' => "Contacts",
                'ru' => 'Контакты'
            ],
            'contacts_datatable_preview_btn' => [
                'en' => 'Preview',
                'ru' => 'Детали'
            ],
            'contacts_field_title_type' => [
                'en' => "Type",
                'ru' => 'Тип'
            ],
            'contacts_customer_type_tooltip' => [
                
            ],
            'contacts_tourist_type_tooltip' => [
                
            ],
            'contacts_lid_type_tooltip' => [
                
            ],
            'contacts_new_type_tooltip' => [
                
            ],
            'contacts_field_title_birth_date' => [
                'en' => "Birth date",
                'ru' => 'Дата рождения'
            ],
            'contacts_field_title_age' => [
                'en' => "Age",
                'ru' => 'Возраст'
            ],
            'contacts_field_nationality' => [
                'en' => "Nationality",
                'ru' => 'Гражданство'
            ],
            'contacts_field_company' => [
                'en' => "Company",
                'ru' => 'Компания'
            ],
            'contacts_field_tags' => [
                'en' => "Tags",
                'ru' => 'Теги'
            ],
            'contacts_field_created' => [
                'en' => "Created",
                'ru' => 'Создано'
            ],
            'contacts_field_updated' => [
                'en' => "Updated",
                'ru' => 'Обновлено'
            ],
            'contacts_field_actions' => [
                'en' => "Actions",
                'ru' => 'Операции'
            ],
            'contacts_delete_success' => [
                'en' => 'Success!',
                'ru' => 'Успешно!'
            ],
            'contact_no_contacts_message' => [
                'en' => 'No contacts',
                'ru' => 'Нет контактов'
            ],
            'contact_no_passports' => [
                'en' => 'No passports',
                'ru' => 'Нет паспортов'
            ],
            'contact_no_visas' => [
                'en' => 'No visas',
                'ru' => 'Нет виз'
            ],
            'contacts_delete_message1' => [
                'en' => 'Are you sure to delete this ',
                'ru' => 'Вы уверены что хотите удалить '
            ],
            'contacts_delete_message2' => [
                'en' => ' items?',
                'ru' => ' записей?'
            ],
            'contacts_contacts' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'companies_menu_item_title' => [
                'en' => 'Companies',
                'ru' => 'Компании'
            ],
            'contacts_bulk' => [
                'en' => 'Bulk',
                'ru' => 'Массовые операции'
            ],
            'contacts_new_contact' => [
                'en' => 'New Contact',
                'ru' => 'Добавить контакт'
            ],
            'contacts_menu_tags' => [
                'en' => 'Tags',
                'ru' => 'Теги'
            ],
            'contacts_menu_references' => [
                'en' => 'References',
                'ru' => 'Справочники'
            ],
            'contacts_menu_list_settings' => [
                'en' => 'List settings',
                'ru' => 'Настрока списков'
            ],
            'contacts_edit_window_settings' => [
                'en' => 'Edit window settings',
                'ru' => 'Настройка окна редактирования'
            ],
            'contacts_menu_list_import' => [
                'en' => 'Import',
                'ru' => 'Импорт'
            ],
            'contacts_menu_contacts_combine' => [
                'en' => 'Find duplicates',
                'ru' => 'Поиск дубликатов'
            ],
            'contacts_records_selected1' => [
                'en' => 'Selected ',
                'ru' => 'Выбрано '
            ],
            'contacts_records_selected2' => [
                'en' => ' records.',
                'ru' => ' записей.'
            ],
            'contacts_records_selected3' => [
                'en' => ' Select all ',
                'ru' => ' Выбрать все '
            ],
            'contacts_records_selected4' => [
                'en' => ' records.',
                'ru' => ' записей.'
            ],
            'contacts_merge_view_btn' => [
                'en' => 'View',
                'ru' => 'Посмотреть'
            ],
            'contacts_merge_found_message_1' => [
                'en' => 'Was found',
                'ru' => 'Найдено'
            ],
            'contacts_merge_found_message_2' => [
                'en' => 'similar contacts',
                'ru' => 'похожих контакта'
            ],
            'contacts_list_options_title' => [
                'en' => 'List settings',
                'ru' => 'Настройка списков'
            ],
            'contacts_delete' => [
                'en' => 'Delete',
                'ru' => 'Удалить'
            ],
            'contacts_types_customer' => [
                'en' => 'Customer',
                'ru' => 'Заказчик'
            ],
            'contacts_types_tourist' => [
                'en' => 'Tourist',
                'ru' => 'Турист'
            ],
            'contacts_types_lid' => [
                'en' => 'Lid',
                'ru' => 'Лид'
            ],
            'contacts_types_new' => [
                'en' => 'New',
                'ru' => 'Новый'
            ],
            'tags_table_name_title' => [
                'en' => 'Name',
                'ru' => 'Название',
            ],
            'tags_table_сompanies_title' => [
                'en' => 'Companies',
                'ru' => 'Компании',
            ],
            'tags_table_created_at_title' => [
                'en' => 'Created',
                'ru' => 'Создано',
            ],
            'tags_table_tag_name_placeholder' => [
                'en' => 'Tag name',
                'ru' => 'Имя тэга',
            ],
            'modal_bulc_actions_operation_type_label' => [
                'en' => 'Operation type',
                'ru' => 'Тип операции',
            ],
            'tag_delete_question' => [
                'en' => 'Delete this tag?',
                'ru' => 'Удалить этот тэг?',
            ],
            'add_tag' => [
                'en' => 'Add tag',
                'ru' => 'Добавить тэг'
            ],
            'tag_was_created' => [
                'en' => 'Tag was created',
                'ru' => 'Тэг создан'
            ],
            'modal_bulk_tags' => [
                'en' => 'Tags',
                'ru' => 'Теги',
            ],
            'modal_bulk_add_tags' => [
                'en' => 'Add tags',
                'ru' => 'Добавте теги',
            ],
            'modal_bulk_select_tags' => [
                'en' => 'Select tags',
                'ru' => 'Выберите теги',
            ],
            'modal_bulk_responsible' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный',
            ],
            'modal_bulk_select_responsible' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите ответственного',
            ],
            'system_tag_tooltip' => [
                'en' => 'System tag, edit disabled',
                'ru' => 'Системная тег, редактирование отключено'
            ],
            'contacts_merge_modal_tite' => [
                'en' => 'Was found similar contacts',
                'ru' => 'Найдены похожие контакты'
            ],
            'contacts_merge_all_btn' => [
                'en' => 'Merge all',
                'ru' => 'Объеденить все'
            ],
            'contacts_merge_btn' => [
                'en' => 'Merge',
                'ru' => 'Объеденить'
            ],
            'merge_all_alert_message_title' => [
                'en' => 'Contacts merge',
                'ru' => 'Объединение контактов'
            ],
            'merge_all_alert_message_question' => [
                'en' => 'Are you sure you want to merge all these contacts?',
                'ru' => 'Вы уверены что хотите объеденить все указанные контакты?'
            ],
            'merge_dismiss_message_title' => [
                'en' => 'Merge dismiss',
                'ru' => 'Отмена объединения'
            ],
            'merge_dismiss_message_question' => [
                'en' => "Do you want to exclude this combination of contacts and don't offer their merge in the future?",
                'ru' => 'Вы хотите исключить данную комбинацию контактов и не предлагать их объединение в дальнейшем?'
            ],
            'merge_this_contacts_message_title' => [
                'en' => 'Merge contacts',
                'ru' => 'Объеденить контакты'
            ],
            'merge_this_contacts_message_question' => [
                'en' => 'Do you realy want to merge this contacts?',
                'ru' => 'Вы действительно хотите объеденить эти контакты?'
            ],
            'quick_contact_modal_task_title' => [
                'en' => 'Create task',
                'ru' => 'Создать задачу'
            ],
            'quick_contact_modal_title_title' => [
                'en' => 'Title',
                'ru' => 'Название',
            ],
            'quick_contact_modal_due_date_title' => [
                'en' => 'Due date',
                'ru' => 'Срок',
            ],
            'quick_contact_modal_email_reminder_title' => [
                'en' => 'Email reminder',
                'ru' => 'Email напоминание',
            ],
            'quick_contact_modal_note_redactor_title' => [
                'en' => 'Notes',
                'ru' => 'Заметки',
            ],
            'quick_contact_modal_note_redactor_placeholder' => [
                'en' => 'Your notes...',
                'ru' => 'Ваша заметка...',
            ],
        ],
        'ita/contacts/view' => [
            'modal_add_note_title' => [
                'en' => 'Add note',
                'ru' => 'Добавить заметку'
            ],
            'modal_edit_note_title' => [
                'en' => 'Edit note',
                'ru' => 'Изменить заметку'
            ],
            'modal_add_passport_title' => [
                'en' => 'Add passport',
                'ru' => 'Добавить паспорт'
            ],
            'modal_add_passport_passport_type' => [
                'en' => 'Passport type',
                'ru' => 'Тип паспорта'
            ],
            'modal_add_passport_first_name' => [
                'en' => 'First name',
                'ru' => 'Имя'
            ],
            'modal_add_passport_last_name' => [
                'en' => 'Last name',
                'ru' => 'Фамилия'
            ],
            'modal_add_passport_birth_date' => [
                'en' => 'Birth date',
                'ru' => 'Дата рождения'
            ],
            'modal_add_passport_passport_serial' => [
                'en' => 'Passport serial',
                'ru' => 'Серия пасспорта'
            ],
            'modal_passport_serial_check' => [
                'en' => 'This passport serial is already used —',
                'ru' => 'Эта серия пасспорта уже используется —'
            ],
            'modal_passport_nationality_check' => [
                'en' => '\'Nationality\' cannot be blank',
                'ru' => 'Поле \'Гражданство\' необходимо заполнить'
            ],
            'modal_add_passport_limit_date' => [
                'en' => 'Limit date',
                'ru' => 'Срок действия'
            ],
            'modal_add_passport_issued_date' => [
                'en' => 'Issued date',
                'ru' => 'Дата выдачи'
            ],
            'modal_add_passport_issued_owner' => [
                'en' => 'Issued Owner',
                'ru' => 'Кем выдан'
            ],
            'modal_add_passport_nationality' => [
                'en' => 'Nationality',
                'ru' => 'Гражданство'
            ],
            'modal_add_passport_country' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'modal_add_passport_nothing_selected' => [
                'en' => 'Nothing selected',
                'ru' => 'Нечего не выбрано'
            ],
            'modal_edit_passport_title' => [
                'en' => 'Edit passport',
                'ru' => 'Редактировать паспорт'
            ],
            'modal_add_visa_title' => [
                'en' => 'Add visa',
                'ru' => 'Добавить визу'
            ],
            'modal_add_visa_country' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'modal_add_visa_begin_date' => [
                'en' => 'Begin date',
                'ru' => 'Действует с'
            ],
            'modal_add_visa_end_date' => [
                'en' => 'End date',
                'ru' => 'Действует до'
            ],
            'modal_add_visa_type' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'modal_add_visa_description' => [
                'en' => 'Description',
                'ru' => 'Описание'
            ],
            'modal_edit_visa_title' => [
                'en' => 'Edit visa',
                'ru' => 'Редактировать визу'
            ],
            'notification_icon_passport_date_limit_danger' => [
                'en' => "Passport's validity expires!",
                'ru' => 'Заканчивается срок действия паспорта!'
            ],
            'notification_icon_passport_date_limit_warning' => [
                'en' => 'The validity of the passport expired!',
                'ru' => 'Закончился срок действия паспорта!'
            ],
            'notification_icon_visa_date_limit_danger' => [
                'en' => "Visa's validity expires!",
                'ru' => 'Заканчивается срок действия визы!'
            ],
            'notification_icon_visa_date_limit_warning' => [
                'en' => 'The validity of the visa expired!',
                'ru' => 'Закончился срок действия визы!'
            ],
            'info-editable-table-date_of_birth_placeholder' => [
                'en' => 'Set new date...',
                'ru' => 'Выберите новую дату...'
            ],
            'info-editable-table-set_new_value_placeholder' => [
                'en' => 'Set new value...',
                'ru' => 'Выберите новое значение...'
            ],
            'info-editable-table-date_of_birth-label' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'info-editable-table-sex-label' => [
                'en' => 'Sex',
                'ru' => 'Пол'
            ],
            'info-editable-table-discount-label' => [
                'en' => 'Discount',
                'ru' => 'Дисконт'
            ],
            'info-editable-table-nationality-label' => [
                'en' => 'Nationality',
                'ru' => 'Национальность'
            ],
            'info-editable-table-company-label' => [
                'en' => 'Company',
                'ru' => 'Компания'
            ],
            'info-editable-table-living_address-label' => [
                'en' => 'Living address',
                'ru' => 'Адрес проживания'
            ],
            'info-editable-table-residence_address-label' => [
                'en' => 'Residence address',
                'ru' => 'Адрес прописки'
            ],
            'info-editable-table-tin-label' => [
                'en' => 'TIN',
                'ru' => 'ИНН'
            ],
            'info-editable-table-addressplaceholder-region' => [
                'en' => 'Region',
                'ru' => 'Регион'
            ],
            'info-editable-table-addressplaceholder-city' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'info-editable-table-addressplaceholder-street' => [
                'en' => 'Street',
                'ru' => 'Улица'
            ],
            'info-editable-table-addressplaceholder-house' => [
                'en' => 'House',
                'ru' => 'Дом'
            ],
            'info-editable-table-addressplaceholder-flat' => [
                'en' => 'Flat',
                'ru' => 'Квартира'
            ],
            'info-editable-table-addressplaceholder-postcode' => [
                'en' => 'Postcode',
                'ru' => 'Почтовый код'
            ],
            'info-editable-table-select_company_placeholder' => [
                'en' => 'Select company',
                'ru' => 'Выберите компанию'
            ],
            'contact-preview-phones' => [
                'en' => 'Phones',
                'ru' => 'Телефоны'
            ],
            'contact_preview_edit_contact' => [
                'en' => 'Edit contact',
                'ru' => 'Редактирование контактов'
            ],
            'contact_preview_quick_edit_modal' => [
                'en' => 'Edit contact',
                'ru' => 'Редактировать контакт'
            ],
            'contact-preview-emails' => [
                'en' => 'Emails',
                'ru' => 'Emails'
            ],
            'contact-preview-socials' => [
                'en' => 'Socials',
                'ru' => 'Соцсети'
            ],
            'contact-preview-messengers' => [
                'en' => 'Messengers',
                'ru' => 'Месенджеры'
            ],
            'contact-preview-sites' => [
                'en' => 'Sites',
                'ru' => 'Сайты'
            ],
            'conctact-edit-popup-enter_phone_number-placeholder' => [
                'en' => 'Phone number',
                'ru' => 'Номер телефона'
            ],
            'conctact-edit-popup-add-tag-plaseholder' => [
                'en' => 'Add a tag',
                'ru' => 'Добавьте тэг'
            ],
            'ajax_error_message_undefined_object_property' => [
                'en' => 'Undefined object property',
                'ru' => 'Неизвестное свойство объекта'
            ],
            'contact-settings-partlet-title' => [
                'en' => 'Settings',
                'ru' => 'Настройки'
            ],
            'contact-settings-partlet-responsible_label' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'contact-settings-partlet-responsible_select_placeholder' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите ответственного'
            ],
            'contact_delete_question' => [
                'en' => 'Delete this contact?',
                'ru' => 'Удалить этот контакт?'
            ],
            'passport_delete_question' => [
                'en' => 'Delete this passport?',
                'ru' => 'Удалить этот паспорт?'
            ],
            'visa_delete_question' => [
                'en' => 'Delete this visa?',
                'ru' => 'Удалить эту визу?'
            ],
            'log_fraa' => [
                'en' => 'Delete this visa?',
                'ru' => 'Удалить эту визу?'
            ],
            'contact_contact_info' => [
                'en' => 'Contact info',
                'ru' => 'Контакты'
            ],
            'contact_passports' => [
                'en' => 'Passports',
                'ru' => 'Паспорт'
            ],
            'contact_relations_portlet_title' => [
                'en' => 'Contact relations',
                'ru' => 'Связи контакта'
            ],
            'contact_relation_type_select_label' => [
                'en' => 'Relation type',
                'ru' => 'Тип связи'
            ],
            'contact_relation_type_select_placeholder' => [
                'en' => 'Select relation type',
                'ru' => 'Выберите тип связи'
            ],
            'contact_relation_contact_select_label' => [
                'en' => 'Contact',
                'ru' => 'Контакт'
            ],
            'contact_relation_contact_select_placeholder' => [
                'en' => 'Select contact',
                'ru' => 'Выберите контакт'
            ],
            'contact_name' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'contact_serial_number' => [
                'en' => 'Serial number',
                'ru' => 'Номер паспорта'
            ],
            'contact_limit' => [
                'en' => 'Limit',
                'ru' => 'Лимит'
            ],
            'contact_date' => [
                'en' => 'date',
                'ru' => 'дата'
            ],
            'contact_actions' => [
                'en' => 'Actions',
                'ru' => 'Действие'
            ],
            'contact_visas' => [
                'en' => 'Visas',
                'ru' => 'Визы'
            ],
            'contact_notes' => [
                'en' => 'Notes',
                'ru' => 'Заметки'
            ],
            'contact_notes_request' => [
                'en' => 'Request #',
                'ru' => 'Запрос №'
            ],
            'contact_change_history' => [
                'en' => 'Change history',
                'ru' => 'История изменения'
            ],
            'contact_type' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'contact_btn_address_copy_to_residence' => [
                'en' => 'Copy address to residence',
                'ru' => 'Взять данные с адреса проживания'
            ],
            'contact_country' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'contact_static_block_request' => [
                'en' => 'Request',
                'ru' => 'Запрос'
            ],
            'contact_static_block_commercial_offers' => [
                'en' => 'Compilation',
                'ru' => 'Подборка'
            ],
            'contact_static_block_deals' => [
                'en' => 'Commercial',
                'ru' => 'Коммерческие'
            ],
            'contact_static_block_reservations' => [
                'en' => 'Orders',
                'ru' => 'Заявки'
            ],
            'contact_static_block_your_requests' => [
                'en' => 'Your requests',
                'ru' => 'Ваши запросы'
            ],
            'contact_static_block_your_commercial_offers' => [
                'en' => 'Total prices added',
                'ru' => 'Всего добавлено цен'
            ],
            'contact_static_block_your_deals' => [
                'en' => 'Total sent offers',
                'ru' => 'Всего отправлено предложений'
            ],
            'contact_static_block_your_reservations' => [
                'en' => 'Total orders',
                'ru' => 'Всего заявок'
            ],
            'contacts_logs_action__create' => [
                'en' => 'created',
                'ru' => 'создал',
            ],
            'contacts_logs_action__update' => [
                'en' => 'updated',
                'ru' => 'обновил',
            ],
            'contacts_logs_action__delete' => [
                'en' => 'deleted',
                'ru' => 'удалил',
            ],
            'contacts_logs_table_name__contacts' => [
                'en' => 'contacts',
                'ru' => 'контактах',
            ],
            'contacts_logs_table_name_contacts__addresses' => [
                'en' => 'addresses',
                'ru' => 'адрес',
            ],
            'contacts_logs_table_name_contacts__emails' => [
                'en' => 'emails',
                'ru' => 'эл.почту',
            ],
            'contacts_logs_table_name_contacts__messengers' => [
                'en' => 'messengers',
                'ru' => 'мессенжер skype',
            ],
            'contacts_logs_table_name_contacts__passports' => [
                'en' => 'passports',
                'ru' => 'паспорт',
            ],
            'contacts_logs_table_name_contacts__phones' => [
                'en' => 'phones',
                'ru' => 'телефон',
            ],
            'contacts_logs_table_name_contacts__sites' => [
                'en' => 'sites',
                'ru' => 'сайт',
            ],
            'contacts_logs_table_name_contacts__socials' => [
                'en' => 'socials',
                'ru' => 'соц.сеть',
            ],
            'contacts_logs_table_name_contacts__tags' => [
                'en' => 'tags',
                'ru' => 'тег',
            ],
            'contacts_logs_table_name_contacts__visas' => [
                'en' => 'visas',
                'ru' => 'визу',
            ],
            'contacts_logs_table_name_contacts__notes'=> [
                'en' => 'notes',
                'ru' => 'заметку',
            ],
            'contacts_logs_contact_notes_column__value'=> [
                'en' => 'notes',
                'ru' => 'заметку',
            ],
            'contacts_logs_contact_column__notes'=> [
                'en' => 'notes',
                'ru' => 'заметку',
            ],
            'contacts_logs_message__in' => [
                'en' => 'in',
                'ru' => 'в',
            ],
            'contacts_logs_message__the_following_values' => [
                'en' => 'the following values',
                'ru' => 'следующие значение',
            ],
            'contacts_logs_message_column__value' => [
                'en' => 'main value of this table',
                'ru' => 'главном значении этой таблицы',
            ],
            'contacts_logs_contact_column__first_name' => [
                'en' => 'first name',
                'ru' => 'имя',
            ],
            'contacts_logs_contact_column__last_name' => [
                'en' => 'last name',
                'ru' => 'фамилия',
            ],
            'contacts_logs_contact_column__middle_name' => [
                'en' => 'middle name',
                'ru' => 'отчество',
            ],
            'contacts_logs_contact_column__date_of_birth' => [
                'en' => 'date of birth',
                'ru' => 'дату рождения',
            ],
            'contacts_logs_contact_column__gender' => [
                'en' => 'gender',
                'ru' => 'пол',
            ],
            'contacts_logs_contact_column__contacts_phones' => [
                'en' => 'phone',
                'ru' => 'номерах',
            ],
            'contacts_logs_contact_column__contacts_sites' => [
                'en' => 'site',
                'ru' => 'сайте',
            ],
            'contacts_logs_contact_column__contacts_socials' => [
                'en' => 'social',
                'ru' => 'социальной сети',
            ],
            'contacts_logs_contact_column__contacts_tags' => [
                'en' => 'tag',
                'ru' => 'теге',
            ],
            'contacts_logs_contact_column__contacts_visas' => [
                'en' => 'visa',
                'ru' => 'визе',
            ],

            'contacts_logs_contact_value__gender_man' => [
                'en' => 'man',
                'ru' => 'мужской',
            ],
            'contacts_logs_contact_value__gender_woman' => [
                'en' => 'woman',
                'ru' => 'женский',
            ],
            'contacts_logs_contact_column__discount' => [
                'en' => 'discount',
                'ru' => 'дисконт',
            ],
            'contacts_logs_contact_column__nationality' => [
                'en' => 'nationality',
                'ru' => 'национальность',
            ],
            'contacts_logs_contact_column__tin' => [
                'en' => 'tin',
                'ru' => 'ИНН',
            ],
            'contacts_logs_contact_column__type' => [
                'en' => 'type',
                'ru' => 'тип',
            ],
            'contacts_logs_contact_column__responsible' => [
                'en' => 'responsible',
                'ru' => 'ответственный',
            ],
            'contacts_logs_contact_mail_column__type' => [
                'en' => 'type',
                'ru' => 'тип',
            ],
            'contacts_logs_contact_addresses_column__country' => [
                'en' => 'country',
                'ru' => 'страна',
            ],
            'contacts_logs_contact_addresses_column__region' => [
                'en' => 'region',
                'ru' => 'регион',
            ],
            'contacts_logs_contact_addresses_column__city' => [
                'en' => 'city',
                'ru' => 'город',
            ],
            'contacts_logs_contact_addresses_column__street' => [
                'en' => 'street',
                'ru' => 'улица',
            ],
            'contacts_logs_contact_addresses_column__house' => [
                'en' => 'house',
                'ru' => 'дом',
            ],
            'contacts_logs_contact_addresses_column__flat' => [
                'en' => 'flat',
                'ru' => 'квартира',
            ],
            'contacts_logs_contact_addresses_column__postcode' => [
                'en' => 'postcode',
                'ru' => 'почтовый код',
            ],
            'contacts_logs_contacts_passports_column__first_name' => [
                'en' => 'first name',
                'ru' => 'имя',
            ],
            'contacts_logs_contacts_passports_column__last_name' => [
                'en' => 'last name',
                'ru' => 'фамилии',
            ],
            'contacts_logs_contacts_passports_column__serial' => [
                'en' => 'serial',
                'ru' => 'серии паспорта',
            ],
            'contacts_logs_contacts_passports_column__country' => [
                'en' => 'country',
                'ru' => 'стране',
            ],
            'contacts_logs_contacts_passports_column__nationality' => [
                'en' => 'nationality',
                'ru' => 'национальности',
            ],
            'contacts_logs_contacts_passports_column__issued_owner' => [
                'en' => 'issued owner',
                'ru' => 'кем выдан',
            ],
            'contacts_logs_contacts_passports_column__birth_date' => [
                'en' => 'birth date',
                'ru' => 'дата рождения',
            ],
            'contacts_logs_contacts_passports_column__issued_date' => [
                'en' => 'issued date',
                'ru' => 'дата выдачи',
            ],
            'contacts_logs_contacts_passports_column__date_limit' => [
                'en' => 'date limit',
                'ru' => 'лимитная дата',
            ],
            'contacts_logs_contacts_visa_column__country' => [
                'en' => 'country',
                'ru' => 'страна',
            ],
            'contacts_logs_contact_mail_column__begin_date' => [
                'en' => 'begin date',
                'ru' => 'начальная дата',
            ],
            'contacts_logs_contact_mail_column__end_date' => [
                'en' => 'end date',
                'ru' => 'конечная дата',
            ],
            'contacts_logs_contact_mail_column__description' => [
                'en' => 'description',
                'ru' => 'описание',
            ],
            'contacts_logs_column__phone' => [
                'en' => 'phone',
                'ru' => 'телефон',
            ],
            'contacts_logs_column__emails' => [
                'en' => 'email',
                'ru' => 'эл.почту',
            ],
            'contacts_logs_column__messengers' => [
                'en' => 'messengers',
                'ru' => 'месенджер',
            ],
            'contacts_logs_column__sites' => [
                'en' => 'site',
                'ru' => 'сайт',
            ],
            'contacts_logs_column__socials' => [
                'en' => 'social',
                'ru' => 'соц.сеть',
            ],
            'contacts_logs_column__tag' => [
                'en' => 'tag',
                'ru' => 'тег',
            ],
            'modal_history_and_notes__activity_tab_name' => [
                'en' => 'Activity',
                'ru' => 'Активность',
            ],
            'modal_history_and_notes__notes_tab_name' => [
                'en' => 'Notes',
                'ru' => 'Заметки',
            ],
            'modal_history_and_notes__tasks_tab_name' => [
                'en' => 'Tasks',
                'ru' => 'Задачи',
            ],
            'modal_history_and_notes__email_tab_name' => [
                'en' => 'Email',
                'ru' => 'Email',
            ],
            'modal_history_and_notes__sms_tab_name' => [
                'en' => 'SMS',
                'ru' => 'SMS',
            ],
            'modal_history_and_notes__calls_tab_name' => [
                'en' => 'Calls',
                'ru' => 'Звонки',
            ],
            'modal_history_and_notes__chat_tab_name' => [
                'en' => 'Chat',
                'ru' => 'Чат',
            ],
            'contacts_timeline_filter_title' => [
                'en' => 'Filter',
                'ru' => 'Фильтр',
            ],
            'contacts_timeline_filter_selection_activity' => [
                'en' => 'Activity',
                'ru' => 'Активность',
            ],
            'contacts_timeline_filter_selection_changes' => [
                'en' => 'Changes',
                'ru' => 'Изменения',
            ],
            'contacts_timeline_filter_selection_sales' => [
                'en' => 'Sales',
                'ru' => 'Продажи',
            ],
            'contacts_timeline_filter_selection_email' => [
                'en' => 'Email',
                'ru' => 'Email',
            ],
            'contacts_timeline_filter_selection_calls' => [
                'en' => 'Calls',
                'ru' => 'Звонки',
            ],
            'contacts_timeline_filter_label_tasks' => [
                'en' => 'Tasks',
                'ru' => 'Задачи',
            ],
            'contacts_timeline_filter_label_notes' => [
                'en' => 'Notes',
                'ru' => 'Заметки',
            ],
            'contacts_timeline_filter_label_creating' => [
                'en' => 'Created',
                'ru' => 'Создание',
            ],
            'contacts_timeline_filter_label_deleting' => [
                'en' => 'Deleted',
                'ru' => 'Удаление',
            ],
            'contacts_timeline_filter_label_updating' => [
                'en' => 'Edited',
                'ru' => 'Редактирование',
            ],
            'contacts_timeline_filter_label_requests' => [
                'en' => 'Requests',
                'ru' => 'Запросы',
            ],
            'contacts_timeline_filter_label_bids' => [
                'en' => 'Bids',
                'ru' => 'Заявки',
            ],
            'contacts_timeline_filter_label_income' => [
                'en' => 'Income',
                'ru' => 'Входящие',
            ],
            'contacts_timeline_filter_label_outgo' => [
                'en' => 'Outgo',
                'ru' => 'Исходящие',
            ],
            'contacts_timeline_search_input_button' => [
                'en' => 'Search',
                'ru' => 'Поиск',
            ],
            'contacts_timeline_search_input_placeholder' => [
                'en' => 'Search word',
                'ru' => 'Слово для поиска',
            ],
            'contacts_timeline_task_type_select_placeholder' => [
                'en' => 'Select type',
                'ru' => 'Выберите тип'
            ],
            'contacts_timeline_task_assigned_to_select_placeholder' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите исполнителя'
            ],
            'contacts_timeline_task_email_reminder_time_placeholder' => [
                'en' => 'Email time',
                'ru' => 'Email время'
            ],
            'contacts_timeline_create_task_acordion_title' => [
                'en' => 'Add new task',
                'ru' => 'Добавить новую задачу'
            ],
            'contacts_timeline_create_task_name_label' => [
                'en' => 'Task name',
                'ru' => 'Название задачи'
            ],
            'contacts_timeline_task_email_reminder_date_placeholder' => [
                'en' => 'Email date',
                'ru' => 'Email дата'
            ],
            'contacts_timeline_task_due_date_placeholder' => [
                'en' => 'Due date',
                'ru' => 'Дата выполнения'
            ],
            'contacts_timeline_task_due_time_placeholder' => [
                'en' => 'Due time',
                'ru' => 'Время выполнения'
            ],
            'contacts_timeline_task_note_textarea_placeholder' => [
                'en' => 'Your note...',
                'ru' => 'Введите заметку...'
            ],
            'contacts_timeline_task_note_textarea_labels' => [
                'en' => 'Notes',
                'ru' => 'Заметки'
            ],
            'contacts_timeline_task_due_date_label' => [
                'en' => 'Task due date',
                'ru' => 'Дата выполнения задачи'
            ],
            'contacts_timeline_task_type_label' => [
                'en' => 'Task type',
                'ru' => 'Тип задачи'
            ],
            'contacts_timeline_task_assigned_to_label' => [
                'en' => 'Assigned to',
                'ru' => 'Назначена на'
            ],
            'contacts_timeline_task_email_reminder_date_label' => [
                'en' => 'Email reminder date',
                'ru' => 'Дата email уведомления'
            ],
            'contacts_passport_update_birth' => [
                'en' => 'Update birth date in contact card?',
                'ru' => 'Обновить дату рождения в карточке контакта?'
            ],
            'contacts_passport_birth_notsame' => [
                'en' => 'Birth dates do not match',
                'ru' => 'Даты рождения не совпадают'
            ],
            'contact_travels_history_portlet_title' => [
                'en' => 'Travels history',
                'ru' => 'История поездок'
            ],
            'contact_travels_history_col_date_title' => [
                'en' => 'Data',
                'ru' => 'Дата'
            ],
            'contact_travels_history_col_country_title' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'contact_travels_history_col_value_title' => [
                'en' => 'Value',
                'ru' => 'Стоимость'
            ],
            'contact_files_portlet_title' => [
                'en' => 'Contact files',
                'ru' => 'Файлы контакта'
            ],
            'contact_page_add_doc_btn' => [
                'en' => 'Add document',
                'ru' => 'Добавить документ'
            ],
            'contact_files_not_found' => [
                'en' => 'Documents not found!',
                'ru' => 'Нет документов!'
            ],
            'contact_file_modal_generate_doc' => [
                'en' => 'Generate file',
                'ru' => 'Генерировать файл'
            ],
            'contact_file_modal_upload_doc' => [
                'en' => 'Upload file',
                'ru' => 'Загрузить файл'
            ],
            'manual_contacts_merge_portlet_title' => [
                'en' => 'Contacts merge',
                'ru' => 'Объединение контактов'
            ],
            'manual_contacts_merge_select_placeholder' => [
                'en' => 'Contact search',
                'ru' => 'Поиск контакта'
            ],
            'manual_contacts_merge_will_merge_message_part' => [
                'en' => 'will be merged into',
                'ru' => 'будет интергирован в'
            ],
            'contacts_view_action' => [
                'en' => 'View',
                'ru' => 'Показать'
            ],
        ],
        'ita/settings/account' => [
            'acc_set_main_settings_title' => [
                'en' => 'Main settings',
                'ru' => 'Основные настройки',
            ],
            'acc_set_name_input_label' => [
                'en' => 'Name',
                'ru' => 'Название',
            ],
            'acc_set_name_help_text' => [
                'en' => 'Enter your tenant name',
                'ru' => 'Введите имя своего тенанта',
            ],
            'acc_set_country_input_label' => [
                'en' => 'Country',
                'ru' => 'Страна',
            ],
            'acc_set_country_help_text' => [
                'en' => 'Select your tenant country',
                'ru' => 'Выберите страну своего тенанта',
            ],
            'acc_set_lang_input_label' => [
                'en' => 'Interface language',
                'ru' => 'Язык интерфейса',
            ],
            'acc_set_lang_help_text' => [
                'en' => 'Select interface language',
                'ru' => 'Выберите язык интерфейса',
            ],
            'acc_set_timezone_input_label' => [
                'en' => 'Timezone',
                'ru' => 'Часовой пояс',
            ],
            'acc_set_timezone_help_text' => [
                'en' => 'Change timezone',
                'ru' => 'Выберите часовой пояс',
            ],
            'acc_set_date_format_input_label' => [
                'en' => 'Date format',
                'ru' => 'Формат даты',
            ],
            'acc_set_time_format_input_label' => [
                'en' => 'Time format',
                'ru' => 'Формат времени',
            ],
            'acc_set_date_format_help_text' => [
                'en' => 'Change date format',
                'ru' => 'Выберите формат даты',
            ],
            'acc_set_time_format_help_text' => [
                'en' => 'Change time format',
                'ru' => 'Выберите формат времени',
            ],
            'acc_set_week_start_input_label' => [
                'en' => 'Week start',
                'ru' => 'Начало недели'
            ],
            'acc_set_week_start_help_text' => [
                'en' => 'Select start day of the week',
                'ru' => 'Выберите день начала недели'
            ],
            'acc_set_week_start_sunday' => [
                'en' => 'Sunday',
                'ru' => 'Воскресенье'
            ],
            'acc_set_week_start_monday' => [
                'en' => 'Monday',
                'ru' => 'Понедельник'
            ],
            'acc_set_reminders_title' => [
                'en' => 'Reminders',
                'ru' => 'Напоминалки',
            ],
            'acc_set_reminders_email_label' => [
                'en' => 'Email',
                'ru' => 'Email',
            ],
            'acc_set_reminders_from_label' => [
                'en' => 'From who',
                'ru' => 'От кого',
            ],
            'acc_set_required_field_error_message' => [
                'en' => 'This field is required.',
                'ru' => 'Это поле обязательно',
            ],
            'acc_set_form_has_error_message' => [
                'en' => 'There are some errors in your submission. Please correct them.',
                'ru' => 'В форме есть некоторые ошибки. Пожалуйста, исправьте их.',
            ],
            'acc_set_email_field_error_message' => [
                'en' => 'Not valid email',
                'ru' => 'Укажите email в правильном формате',
            ],
            'acc_set_form_submit_success' => [
                'en' => 'The data has been successfully saves!',
                'ru' => 'Данные были успешно сохранены!',
            ],
            'acc_save_data' => [
                'en' => 'Save the data, please!',
                'ru' => 'Сохраните данные, пожалуйста!'
            ],
            'acc_status_error'=> [
                'en' => 'Error',
                'ru' => 'Ошибка'
            ],
            'acc_unsaved_changes'=> [
                'en' => 'You have unsaved changes',
                'ru' => 'У вас есть несохраненные изменения'
            ],
            'acc_leave_now_question'=> [
                'en' => 'If you leave now, your changes will be lost',
                'ru' => 'Если вы сейчас уйдете, ваши изменения будут потеряны'
            ],
            'acc_leave'=> [
                'en' => 'Leave',
                'ru' => 'Покинуть'
            ],
            'acc_stay'=> [
                'en' => 'Stay',
                'ru' => 'Остаться'
            ],
            'acc_set_requisites_title' => [
                'en' => 'Requisites',
                'ru' => 'Реквизиты',
            ],
            'acc_set_agency_contacts_title' => [
                'en' => 'Agency contacts',
                'ru' => 'Контакты агенства',
            ],
            'acc_set_agency_contacts_phones_label' => [
                'en' => 'Phones',
                'ru' => 'Телефоны'
            ],
            'acc_set_agency_contacts_emails_label' => [
                'en' => 'Emails',
                'ru' => 'Emails'
            ],
            'acc_set_agency_contacts_socials_label' => [
                'en' => 'Socials',
                'ru' => 'Соцсети'
            ],
            'acc_set_agency_contacts_messengers_label' => [
                'en' => 'Messengers',
                'ru' => 'Мессенжеры'
            ],
            'acc_set_agency_contacts_site_label' => [
                'en' => 'Site',
                'ru' => 'Сайт'
            ],
            'acc_set_agency_contacts_adress_label' => [
                'en' => 'Adress',
                'ru' => 'Адрес'
            ],
            'acc_set_agency_contacts_logotype_label' => [
                'en' => 'Logotype',
                'ru' => 'Логотип'
            ],
            'tasks_delete_message1' => [
                'en' => 'Are you sure to delete this?',
                'ru' => 'Вы уверены что хотите удалить?'
            ],
            'agency_delete_logo' => [
              'en' => 'Delete logo',
              'ru' => 'Удалить логотип'
            ],
            'agency_picture_upload_success' => [
                'en' => 'Agency picture successfully changed!',
                'ru' => 'Изображение агентства успешно изменено!'
            ],
            'agency_picture_upload_error' => [
                'en' => 'Agency picture not uploaded!',
                'ru' => 'Изображение агентства не загружено!'
            ],
            'agency_picture_delete_success' => [
                'en' => 'Agency picture successfully deleted!',
                'ru' => 'Изображение агентства успешно удалено!'
            ],
            'agency_picture_delete_error' => [
                'en' => 'Agency picture not deleted!',
                'ru' => 'Изображение агентства не удалено!'
            ],
            'acc_set_agency_contacts_phone_placeholder' => [
                'en' => 'Phone number',
                'ru' => 'Номер телефона'
            ],
            'acc_set_agency_contacts_phone_input_placeholder' => [
                'en' => 'Enter phone number',
                'ru' => 'Введите номер телефона'
            ],
            'acc_set_agency_contacts_add_button' => [
                'en' => 'Add',
                'ru' => 'Добавить'
            ],
            'acc_set_agency_contacts_email_input_placeholder' => [
                'en' => 'Enter email',
                'ru' => 'Введите email'
            ],
            'acc_set_agency_contacts_social_input_placeholder' => [
                'en' => 'Enter social',
                'ru' => 'Введите соцсеть'
            ],
            'acc_set_agency_contacts_messenger_input_placeholder' => [
                'en' => 'Enter messenger',
                'ru' => 'Введите месенжер'
            ],
            'acc_set_agency_contacts_site_input_placeholder' => [
                'en' => 'Enter site',
                'ru' => 'Введите сайт'
            ],
            'acc_set_agency_contacts_street_input_placeholder' => [
                'en' => 'Enter street name',
                'ru' => 'Введите название улицы'
            ],
            'acc_set_agency_contacts_city_placeholder' => [
                'en' => 'Enter city',
                'ru' => 'Введите город'
            ],
            'acc_set_agency_contacts_house_input_placeholder' => [
                'en' => 'Enter house number',
                'ru' => 'Введите номер дома'
            ],
            'acc_set_agency_contacts_description_textarea_placeholder' => [
                'en' => 'Enter adress description',
                'ru' => 'Введите краткое описание'
            ],
            'acc_set_agency_contacts_logotype_dropzone_placeholder' => [
                'en' => 'Drop agency logotype or click to upload.',
                'ru' => 'Перетащите агентства логотип или нажмите, чтобы загрузить.'
            ],
            'acc_set_requisites_save_button' => [
                'en' => 'Save requisities',
                'ru' => 'Сохранить реквизиты',
            ],
            'acc_set_requisites_add_button' => [
                'en' => 'Add requisities',
                'ru' => 'Добавить реквизиты',
            ],
            'acc_set_usage_currency' => [
                'en' => 'Usage currency',
                'ru' => 'Используемая валюта',
            ],
            'acc_select_currency_all' => [
                'en' => 'Select currency',
                'ru' => 'Выберите валюту',
            ],
            'acc_select_currency_main' => [
                'en' => 'Select currency',
                'ru' => 'Выберите валюту',
            ],
            'acc_set_main_currency' => [
                'en' => 'Select main currency',
                'ru' => 'Выберите основную валюту',
                ],
            'acc_set_currency_display' => [
                'en' => 'Select currency display',
                'ru' => 'Выберите отображение валюты',
            ],
            'acc_set_system_set_title' => [
                'en' => 'System values',
                'ru' => 'Системные значения',
            ],
            'acc_set_input_system_tags_name' => [
                'en' => 'System tags',
                'ru' => 'Системные теги',
            ],
            'acc_set_input_system_tags_text' => [
                'en' => 'Use system tags',
                'ru' => 'Использовать системные теги',
            ],
            'acc_set_delete_message' => [
                'en' => 'Are you sure to delete this set?',
                'ru' => 'Вы уверены что хотите удалить этот набор ?',
            ],
            'acc_requisites_help_modal_title' => [
                'en' => 'Reference',
                'ru' => 'Справка',
            ],
            'acc_requisites_help_modal_text' => [
                'en' => 'Reference text',
                'ru' => 'Текст справки',
            ],
        ],
        'ita/reminders' => [
            'reminders_for_agent_page_title' => [
                'en' => 'Agent reminders',
                'ru' => 'Напоминалки агента',
            ],
            'reminders_for_tourist_page_title' => [
                'en' => 'Tourist reminders',
                'ru' => 'Напоминалки туриста',
            ],
            'reminders_for_agent' => [
                'en' => 'For agent',
                'ru' => 'Для агента',
            ],
            'reminders_for_tourist' => [
                'en' => 'For tourist',
                'ru' => 'Для туриста',
            ],
            'reminders_table_notification_column_title' => [
                'en' => 'Notification name',
                'ru' => 'Название напоминалки',
            ],
            'reminders_table_no_email_channel_message' => [
                'en' => 'No email',
                'ru' => 'Нет email',
            ],
            'reminders_table_no_notice_channel_message' => [
                'en' => 'No notice',
                'ru' => 'Нет уведомлния',
            ],
            'reminders_table_no_task_channel_message' => [
                'en' => 'No task',
                'ru' => 'Нет задачи',
            ],
            'reminders_table_notice_column_title' => [
                'en' => 'Notice',
                'ru' => 'Уведомление',
            ],
            'reminders_table_task_column_title' => [
                'en' => 'Task',
                'ru' => 'Задача',
            ],
            'reminders_tourist_birthday_row_title' => [
                'en' => 'Tourist birthday',
                'ru' => 'День рождения туриста',
            ],
            'reminders_new_year_and_christmas_row_title' => [
                'en' => 'New Year and Christmas',
                'ru' => 'Новый Год и Рождество',
            ],
            'reminders_edit_popup_source_title' => [
                'en' => 'Source',
                'ru' => 'Источник',
            ],
            'reminders_edit_popup_additional_params_title' => [
                'en' => 'Additional params',
                'ru' => 'Дополнительные параметры',
            ],
            'reminders_edit_popup_makr_search_title' => [
                'en' => 'Marks search',
                'ru' => 'Поиск меток',
            ],
            'reminders_edit_popup_notification_period_title' => [
                'en' => 'Notification period',
                'ru' => 'Период уведомления',
            ],
            'reminders_edit_popup_repeat_period_title' => [
                'en' => 'Repeat period',
                'ru' => 'Период повторения',
            ],
            'reminders_edit_popup_contacts_module_title' => [
                'en' => 'Contacts module',
                'ru' => 'Модуль контакты'
            ],
            'reminders_edit_popup_repeat_notifications_select_yes' => [
                'en' => 'Repeat',
                'ru' => 'Повторять'
            ],
            'reminders_edit_popup_repeat_notifications_select_once_a_year' => [
                'en' => 'Once a year',
                'ru' => 'Раз в год'
            ],
            'reminders_edit_popup_repeat_notifications_select_once_a_month' => [
                'en' => 'Once a month',
                'ru' => 'Раз в месяц'
            ],
            'reminders_edit_popup_repeat_notifications_select_no' => [
                'en' => 'Not repeat',
                'ru' => 'Не повторять'
            ],
            'reminders_edit_popup_notification_date_placeholder' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'reminders_edit_popup_notification_days_placeholder' => [
                'en' => 'Days',
                'ru' => 'Дни'
            ],
            'reminders_edit_popup_notification_time_placeholder' => [
                'en' => 'Time',
                'ru' => 'Время'
            ],
            'reminders_edit_popup_subscribers_select_title' => [
                'en' => 'Subscribers',
                'ru' => 'Подписчики'
            ],
            'reminders_system_status' => [
                'en' => 'System',
                'ru' => 'Cистемный'
            ],
            'notification_look_btn' => [
                'en' => 'Look',
                'ru' => 'Посмотреть'
            ],
            'notification_readonly_popup' => [
                'en' => 'Read only',
                'ru' => 'Только чтение'
            ],
            'notification_popup_сhannel_change_label' => [
                'en' => 'Channel change',
                'ru' => 'Выбор канала'
            ],
            'notification_popup_operators_change_label' => [
                'en' => 'Operators',
                'ru' => 'Операторы'
            ],
            'notification_popup_event_change_label' => [
                'en' => 'Event',
                'ru' => 'Событие'
            ],
            'notification_popup_notification_сhannel_title' => [
                'en' => 'Notification channel',
                'ru' => 'Канал уведомлений'
            ],
            'notification_activate_mode_select_option_1' => [
                'en' => 'In event moment',
                'ru' => 'В момент события',
            ],
            'notification_activate_mode_select_option_2' => [
                'en' => 'One time',
                'ru' => 'Один раз',
            ],
            'notification_activate_mode_select_option_3' => [
                'en' => 'Days before',
                'ru' => 'Кол-во дней до',
            ],
            'notification_activate_mode_select_option_4' => [
                'en' => 'Days after',
                'ru' => 'Кол-во дней после',
            ],
            'reminders_marks_modal_title' => [
                'en' => 'Marks',
                'ru' => 'Марки',
            ],
            'reminders_add_new_reminders_button' => [
                'en' => 'Add reminder',
                'ru' => 'Добавить напоминалку'
            ],
            'reminders_filter_title_all' => [
                'en' => 'All',
                'ru' => 'Все'
            ],
            'reminders_filter_title_system' => [
                'en' => 'System',
                'ru' => 'Системные'
            ],
            'reminders_filter_title_my' => [
                'en' => 'My',
                'ru' => 'Мои'
            ],
            'system_reminder_tooltip' => [
                'en' => 'System reminder, edit disabled',
                'ru' => 'Системная напоминалка, редактирование отключено'
            ],
            'reminders_modal_input_placeholder_email_theme' => [
                'en' => 'Enter email theme',
                'ru' => 'Укажите тему письма'
            ],
            'reminders_modal_input_placeholder_email_text' => [
                'en' => 'Enter email text',
                'ru' => 'Укажите текст письма'
            ],
            'reminders_modal_input_placeholder_notice_text' => [
                'en' => 'Enter notice text',
                'ru' => 'Укажите текст уведомления'
            ],
            'reminders_modal_input_placeholder_task_name' => [
                'en' => 'Enter task name',
                'ru' => 'Укажите название задачи'
            ],
            'reminders_modal_input_placeholder_task_text' => [
                'en' => 'Enter what needs to be done in the task',
                'ru' => 'Укажите что необходимо сделать в задче'
            ]
        ],
        'ita/requests/index' => [
            'rc_page_title' => [
                'en' => 'Requests',
                'ru' => 'Запросы'
            ],
            'rc_add_request_button' => [
                'en' => 'Add request',
                'ru' => 'Добавить запрос'
            ],
            'rc_delete_request' => [
                'en' => 'Delete request',
                'ru' => 'Удалить запрос'
            ],
            'rc_datatable_col_title_1' => [
                'en' => '№',
                'ru' => '№'
            ],
            'rc_datatable_col_title_2' => [
                'en' => 'Create date',
                'ru' => 'Дата создания'
            ],
            'rc_datatable_col_title_3' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'rc_datatable_col_title_4' => [
                'en' => 'Directions',
                'ru' => 'Направления'
            ],
            'rc_datatable_col_title_5' => [
                'en' => 'Dates',
                'ru' => 'Даты'
            ],
            'rc_datatable_col_title_6' => [
                'en' => 'Period',
                'ru' => 'Длительность'
            ],
            'rc_datatable_col_title_7' => [
                'en' => 'Raiting',
                'ru' => 'Звездность'
            ],
            'rc_datatable_col_title_8' => [
                'en' => 'Tourists',
                'ru' => 'Туристы'
            ],
            'rc_datatable_col_title_9' => [
                'en' => 'Budget',
                'ru' => 'Бюджет'
            ],
            'rc_datatable_col_title_10' => [
                'en' => 'Resposible',
                'ru' => 'Ответственный'
            ],
            'rc_datatable_col_title_11' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'rc_delete_message1' => [
                'en' => 'Are you sure to delete this',
                'ru' => 'Вы уверены что хотите удалить'
            ],
            'rc_delete_message2' => [
                'en' => 'items?',
                'ru' => 'запрос?'
            ],
            'rc_filter_custom_filter_1' => [
                'en' => 'Opened requests',
                'ru' => 'Открытые запросы'
            ],
            'rc_filter_custom_filter_2' => [
                'en' => 'Only my requests',
                'ru' => 'Только мои запросы'
            ],
            'rc_filter_custom_filter_3' => [
                'en' => 'Successfully completed',
                'ru' => 'Успешно завершенные'
            ],
            'rc_filter_custom_filter_4' => [
                'en' => 'Unrealized requests',
                'ru' => 'Нереализованные запросы'
            ],
            'rc_filter_accordion_title_1' => [
                'en' => 'Contact',
                'ru' => 'Контакт'
            ],
            'rc_filter_accordion_title_2' => [
                'en' => 'Request data',
                'ru' => 'Данные запроса'
            ],
            'rc_filter_accordion_title_3' => [
                'en' => 'Request settings',
                'ru' => 'Свойства запроса'
            ],
            'rc_filter_input_passport_placeholder' => [
                'en' => 'Passport series and number',
                'ru' => 'Серия и номер паспорта'
            ],
            'rc_filter_select_tags_placeholder' => [
                'en' => 'Change tags',
                'ru' => 'Выбрать теги'
            ],
            'rc_filter_request_number_input_placeholder' => [
                'en' => 'Request number',
                'ru' => 'Номер запроса'
            ],
            'rc_filter_request_create_dates_picker_placeholder' => [
                'en' => 'Create dates',
                'ru' => 'Даты создания'
            ],
            'rc_filter_request_update_dates_picker_placeholder' => [
                'en' => 'Update dates',
                'ru' => 'Даты обновления'
            ],
            'rc_filter_request_responsible_placeholder' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'request_budget_from' => [
                'en' => 'from',
                'ru' => 'от'
            ],
            'request_budget_to' => [
                'en' => 'to',
                'ru' => 'до'
            ]
        ],
        'ita/requests/view' => [
            'conctact-edit-popup-add-tag-plaseholder-req' => [
                'en' => 'Add a tag',
                'ru' => 'Добавьте тэг'
            ],
            'rv_page_change_responsible_label' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите исполнителя'
            ],
            'rv_page_no_tabs_message' => [
                'en' => 'No tabs :(',
                'ru' => 'Нет табов :('
            ],
            'rv_page_step_1_title' => [
                'en' => 'Change tour',
                'ru' => 'Выбрать тур'
            ],
            'rv_page_step_2_title' => [
                'en' => 'Sign the contract',
                'ru' => 'Подписать договор'
            ],
            'rv_page_step_3_title' => [
                'en' => 'Get paid',
                'ru' => 'Получить оплату'
            ],
            'rv_page_step_4_title' => [
                'en' => 'Reservation',
                'ru' => 'Забронировать'
            ],
            'rv_page_step_5_title' => [
                'en' => 'Pay to the supplier',
                'ru' => 'Оплатить поставщику'
            ],
            'rv_page_step_6_title' => [
                'en' => 'Issue documents',
                'ru' => 'Выдать документы'
            ],
            'rv_page_tab_1_1_title' => [
                'en' => 'Request',
                'ru' => 'Запрос'
            ],
            'rv_page_tab_1_2_title' => [
                'en' => 'Bid',
                'ru' => 'Заявка'
            ],
            'rv_page_tab_1_3_title' => [
                'en' => 'Tourist and finances',
                'ru' => 'Туристы и финансы'
            ],
            'country' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'city_select_label' => [
                'en' => 'City of departure',
                'ru' => 'Город вылета'
            ],
            'departure_date_datepicker_label' => [
                'en' => 'Departure date',
                'ru' => 'Дата вылета'
            ],
            'departure_date_datepicker_placeholder' => [
                'en' => 'Enter customer departure date',
                'ru' => 'Введите дату вылета'
            ],
            'trip_duration_inputs_label' => [
                'en' => 'Trip duration',
                'ru' => 'Продолжительность поездки'
            ],
            'trip_min_days_placeholder' => [
                'en' => 'Min',
                'ru' => 'Минимум'
            ],
            'trip_max_days_placeholder' => [
                'en' => 'Max',
                'ru' => 'Максимум'
            ],
            'hotel_category_select_label' => [
                'en' => 'Hotel category',
                'ru' => 'Категория отеля',
            ],
            'hotel_category_select_placeholder' => [
                'en' => 'Enter hotel category',
                'ru' => 'Введите категорию отеля',
            ],
            'budget_inputs_label' => [
                'en' => 'Budget',
                'ru' => 'Бюджет',
            ],
            'min_budget_input_placeholder' => [
                'en' => 'From',
                'ru' => 'От',
            ],
            'max_budget_input_placeholder' => [
                'en' => 'To',
                'ru' => 'До',
            ],
            'tourists_group_title' => [
                'en' => 'Tourists group',
                'ru' => 'Состав туристов',
            ],
            'req_edit_note_title' => [
                'en' => 'Edit note',
                'ru' => 'Редактировать заметку'
            ],
//            'adults_person_count_label' => [
//                'en' => 'Adults persons count',
//                'ru' => 'Кол-во взрослых',
//            ],
//            'children_count_label' => [
//                'en' => 'Сhildren count',
//                'ru' => 'Кол-во детей',
//            ],
//            'children_age_label' => [
//                'en' => 'Children age',
//                'ru' => 'Возраст детей',
//            ],
//            'additional_parameters' => [
//                'en' => 'Additional parameters',
//                'ru' => 'Дополнительные параметры',
//            ],
            'other_label' => [
                'en' => 'Other',
                'ru' => 'Другое',
            ],
            'source' => [
                'en' => 'Source',
                'ru' => 'Источник',
            ],
            'requests_field_description' => [
                'en' => 'Description',
                'ru' => 'Описание',
            ],
            'requests_canceled_reason' => [
                'en' => 'Canceled reason',
                'ru' => 'Причина отказа',
            ],
            'requests_field_settings' => [
                'en' => 'Settings',
                'ru' => 'Настройки'
            ],
            'requests_field_created' => [
                'en' => "Created",
                'ru' => 'Создано'
            ],
            'requests_field_updated' => [
                'en' => "Updated",
                'ru' => 'Обновлено'
            ],
            'requests_field_filter_task_responsible' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'requests_field_delete' => [
                'en' => 'Delete',
                'ru' => 'Удалить'
            ],
            'not_set' => [
                'en' => 'Not set',
                'ru' => 'Не задано',
            ],
            'request_status' => [
                'en' => 'Request status',
                'ru' => 'Статус запроса',
            ],
            'why_close' => [
                'en' => 'Reason close',
                'ru' => 'Причина отмены',
            ],
            'reason' => [
                'en' => 'Reason',
                'ru' => 'Причина',
            ],
            'rv_select_service_modal_title' => [
                'en' => 'Add service',
                'ru' => 'Добавить услугу'
            ],
            'rv_select_service_modal_title_update' => [
                'en' => 'Update service',
                'ru' => 'Изменить услугу'
            ],
            'rv_select_service_modal_partlet_1_title' => [
                'en' => 'Provider and service price',
                'ru' => 'Поставщик и стоимость услуги'
            ],
            'rv_select_service_modal_partlet_2_title' => [
                'en' => 'Additional services',
                'ru' => 'Дополнительные услуги'
            ],
            'rv_page_airport_select_label' => [
                'en' => 'Airport',
                'ru' => 'Аэропорт'
            ],
            'rv_page_airport_select_placeholder' => [
                'en' => 'Select airport',
                'ru' => 'Выберите аэропорт'
            ],
            'rv_page_city_select_label' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'rv_page_city_select_placeholder' => [
                'en' => 'Select city',
                'ru' => 'Выберите город'
            ],
            'rv_page_country_select_label' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'rv_page_country_select_placeholder' => [
                'en' => 'Select country',
                'ru' => 'Выберите страну'
            ],
            'rv_page_date_label' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'rv_page_date_placeholder' => [
                'en' => 'Select date',
                'ru' => 'Выберите дату'
            ],
            'rv_page_date_ending_label' => [
                'en' => 'Ending date',
                'ru' => 'Дата окончания'
            ],
            'rv_page_date_ending_placeholder' => [
                'en' => 'Select ending date',
                'ru' => 'Выберите дату окончания'
            ],
            'rv_page_date_starting_label' => [
                'en' => 'Starting date',
                'ru' => 'Дата начала'
            ],
            'rv_page_date_starting_placeholder' => [
                'en' => 'Select starting date',
                'ru' => 'Выберите дату начала'
            ],
            'rv_page_multiselect_label' => [
                'en' => 'Multiselect',
                'ru' => 'Мультиселект'
            ],
            'rv_page_multiselect_placeholder' => [
                'en' => 'Select value',
                'ru' => 'Выберите значение'
            ],
            'rv_page_radio_label' => [
                'en' => 'Radio buttons',
                'ru' => 'Переключатели'
            ],
            'rv_page_departure_city_select_label' => [
                'en' => 'Departure city',
                'ru' => 'Город вылета'
            ],
            'rv_page_departure_city_select_placeholder' => [
                'en' => 'Select departure city',
                'ru' => 'Выберите город вылета'
            ],
            'rv_page_dropdown_label' => [
                'en' => 'Dropdown',
                'ru' => 'Дропдаун'
            ],
            'rv_page_dropdown_placeholder' => [
                'en' => 'Select value',
                'ru' => 'Выберите значение'
            ],
            'rv_page_food_select_label' => [
                'en' => 'Food',
                'ru' => 'Питание'
            ],
            'rv_page_food_select_placeholder' => [
                'en' => 'Select food',
                'ru' => 'Выберите питание'
            ],
            'rv_page_number_label' => [
                'en' => 'Number',
                'ru' => 'Номер'
            ],
            'rv_page_number_placholder' => [
                'en' => 'Enter number',
                'ru' => 'Введите номер'
            ],
            'rv_page_hotel_select_label' => [
                'en' => 'Hotel',
                'ru' => 'Выберите отель'
            ],
            'rv_page_hotel_select_placeholder' => [
                'en' => 'Select hotel',
                'ru' => 'Отель'
            ],
            'rv_page_text_label' => [
                'en' => 'Text',
                'ru' => 'Текст'
            ],
            'rv_page_text_placeholder' => [
                'en' => 'Enter text',
                'ru' => 'Введите текст'
            ],
            'rv_page_link_label' => [
                'en' => 'Link',
                'ru' => 'Ссылка'
            ],
            'rv_page_link_placeholder' => [
                'en' => 'Enter link',
                'ru' => 'Введите ссылку'
            ],
            'rv_page_star_rating_select_label' => [
                'en' => 'Star rating',
                'ru' => 'Категория отеля'
            ],
            'rv_page_star_rating_select_placeholder' => [
                'en' => 'Select star rating',
                'ru' => 'Выберите категория отеля'
            ],
            'rv_page_nights_count_label' => [
                'en' => 'Nights count',
                'ru' => 'Кол-во ночей'
            ],
            'rv_page_nights_count_placholder' => [
                'en' => 'Enter nights count',
                'ru' => 'Введите кол-во ночей'
            ],
            'rv_page_varchar_label' => [
                'en' => 'Varchar',
                'ru' => 'Строка'
            ],
            'rv_page_varchar_placeholder' => [
                'en' => 'Enter varchar',
                'ru' => 'Введите строку'
            ],
            'rv_page_time_label' => [
                'en' => 'Time',
                'ru' => 'Время'
            ],
            'rv_page_time_placeholder' => [
                'en' => 'Enter time',
                'ru' => 'Введите время'
            ],
            'rv_page_suppliers_label' => [
                'en' => 'Suppliers',
                'ru' => 'Поставщики'
            ],
            'rv_page_suppliers_placeholder' => [
                'en' => 'Enter suppliers',
                'ru' => 'Добавьте поставщиков'
            ],
            'rv_page_currency_label' => [
                'en' => 'Currency',
                'ru' => 'Валюта'
            ],
            'rv_page_currency_placeholder' => [
                'en' => 'Select currency',
                'ru' => 'Выберите Валюту'
            ],
            'rv_page_source_placeholder' => [
                'en' => 'Select source',
                'ru' => 'Выберите источник'
            ],
            'rv_page_payment_to_supplier_label' => [
                'en' => 'Payment to supplier',
                'ru' => 'Платеж поставщику'
            ],
            'rv_page_payment_to_supplier_placeholder' => [
                'en' => 'Enter payment',
                'ru' => 'Введите платеж'
            ],
            'rv_page_commission_label' => [
                'en' => 'Commission',
                'ru' => 'Комиссия'
            ],
            'rv_page_commission_placeholder' => [
                'en' => 'Commission',
                'ru' => 'Комиссия'
            ],
            'rv_page_services_price_label' => [
                'en' => 'Services price',
                'ru' => 'Цена услуги'
            ],
            'rv_page_services_price_placeholder' => [
                'en' => 'Price',
                'ru' => 'Цена'
            ],
            'rv_page_profit_label' => [
                'en' => 'Profit',
                'ru' => 'Прибыль'
            ],
            'rv_page_title_info' => [
                'en' => 'Request info',
                'ru' => 'Информация запроса'
            ],
            'rv_page_services_list_partlet_title' => [
                'en' => 'Services',
                'ru' => 'Услуги'
            ],
            'rv_page_create_proposal_btn' => [
                'en' => 'Create proposal',
                'ru' => 'Создать предложение'
            ],
            'rv_page_add_service_btn' => [
                'en' => 'Add service',
                'ru' => 'Добавить услугу'
            ],
            'rv_page_nights_count' => [
                'en' => 'nights',
                'ru' => 'ночей'
            ],
            'delete_question' => [
                'en' => 'Are you sure?',
                'ru' => 'Вы уверены?'
            ],
            'request_delete_question' => [
                'en' => 'Delete this request?',
                'ru' => 'Удалить этот запрос?'
            ],
            'delete_confirmation' => [
                'en' => 'Yes, delete it!',
                'ru' => 'Да, удалить!'
            ],
            'delete_cancel' => [
                'en' => 'No, cancel!',
                'ru' => 'Нет, отмена!'
            ],
            'request_adult_reduction' => [
                'en' => 'ad.',
                'ru' => 'взр.'
            ],
            'request_children_reduction' => [
                'en' => 'ch.',
                'ru' => 'реб.'
            ],
            'request_change_contact' => [
                'en' => 'Change contact',
                'ru' => 'Изменить контакт'
            ],
            'request_current_contact' => [
                'en' => 'Current contact',
                'ru' => 'Текущий контакт'
            ],
            'request_new_contact' => [
                'en' => 'New contact',
                'ru' => 'Новый контакт'
            ],
            'request_select_new_contact' => [
                'en' => 'Select new contact',
                'ru' => 'Выберите новый контакт'
            ],
            'request_cpp_title' => [
                'en' => 'Сommercial proposals',
                'ru' => 'Коммерческие предложения'
            ],
            'request_cpp_table_head_name' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'request_cpp_table_head_type' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'request_cpp_table_head_status' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'request_cpp_table_head_click_map' => [
                'en' => 'Click map',
                'ru' => 'Карта кликов'
            ],
            'request_cpp_table_no_clicks_message' => [
                'en' => 'No clicks',
                'ru' => 'Нет кликов'
            ],
            'request_change_cp_method_title' => [
                'en' => 'Send method',
                'ru' => 'Метод отправки'
            ],
            'request_cp_sms_modal_sms_body_placeholder' => [
                'en' => 'Enter sms body...',
                'ru' => 'Введите текст смс...'
            ],
            'request_cp_email_modal_from_who_label' => [
                'en' => 'From who',
                'ru' => 'От кого'
            ],
            'request_cp_link_modal_bufer_copy_success' => [
                'en' => 'Link was copy in buffer',
                'ru' => 'Ссылка была скопирована в буфер'
            ],
            'request_cp_link_modal_bufer_copy_no_data_error' => [
                'en' => 'No data for copy',
                'ru' => 'Нечего копироать'
            ],
            'rv_create_reservation_modal_title' => [
                'en' => 'Create reservation',
                'ru' => 'Создать бронировку'
            ],
            'rv_create_reservation_modal_usefull_info_title' => [
                'en' => 'Useful information near',
                'ru' => 'Полезная информация под рукой'
            ],
        ],
        'ita/companies/index' => [
            'companies_index_page_title' => [
                'en' => 'Companies',
                'ru' => 'Компании'
            ],
            'ci_delete_company' => [
                'en' => 'Delete company',
                'ru' => 'Удалить компанию'
            ],
            'ci_add_company_button' => [
                'en' => 'Add compay',
                'ru' => 'Добавить компанию'
            ],
            'ci_delete_message1' => [
                'en' => 'Are you sure to delete this',
                'ru' => 'Вы уверены что хотите удалить'
            ],
            'ci_delete_message2' => [
                'en' => 'items?',
                'ru' => 'запрос?'
            ],
            'ci_filter_preset_all_companies_label' => [
                'en' => 'All companies',
                'ru' => 'Все компании'
            ],
            'ci_filter_preset_only_my_companies_label' => [
                'en' => 'Only my companies',
                'ru' => 'Только мои компании'
            ],
            'ci_filter_company_data_block_title' => [
                'en' => 'Company data',
                'ru' => 'Данные компании'
            ],
            'ci_filter_linked_contacts_block_title' => [
                'en' => 'Linked contacts',
                'ru' => 'Связанные контакты'
            ],
            'ci_filter_name_input_placeholder' => [
                'en' => 'Company name',
                'ru' => 'Название компании'
            ],
            'ci_filter_phone_input_placeholder' => [
                'en' => 'Company phone',
                'ru' => 'Телефон компании'
            ],
            'ci_filter_email_input_placeholder' => [
                'en' => 'Company email',
                'ru' => 'Email компании'
            ],
            'ci_filter_status_input_placeholder' => [
                'en' => 'Company status',
                'ru' => 'Статус компании'
            ],
            'ci_filter_type_input_placeholder' => [
                'en' => 'Company type',
                'ru' => 'Тип компании'
            ],
            'ci_filter_activities_types_input_placeholder' => [
                'en' => 'Company activities types',
                'ru' => 'Вид деятельности компании'
            ],
            'ci_filter_responsible_input_placeholder' => [
                'en' => 'Company responsible',
                'ru' => 'Ответственный'
            ],
            'ci_filter_passport_serial_input_placeholder' => [
                'en' => 'Passport serial number',
                'ru' => 'Серия и номер паспорта'
            ],
            'ci_filter_last_name_input_placeholder' => [
                'en' => 'Last name',
                'ru' => 'Фамилия контакта'
            ],
            'ci_filter_first_name_input_placeholder' => [
                'en' => 'First name',
                'ru' => 'Имя контакта'
            ],
            'ci_filter_contact_phone_input_placeholder' => [
                'en' => 'Contact phone',
                'ru' => 'Телефон контакта'
            ],
            'ci_filter_contact_email_input_placeholder' => [
                'en' => 'Contact email',
                'ru' => 'Email контакта'
            ],
            'ci_filter_contact_tags_input_placeholder' => [
                'en' => 'Contact tags',
                'ru' => 'Тэги контакта'
            ],
            'ci_datatable_col_title_1' => [
                'en' => 'Company name',
                'ru' => 'Название компании'
            ],
            'ci_datatable_col_title_2' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'ci_datatable_col_title_3' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'ci_datatable_col_title_4' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'ci_datatable_col_title_5' => [
                'en' => 'Kind of activity',
                'ru' => 'Вид деятельности'
            ],
            'ci_datatable_col_title_6' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'ci_search_notification' => [
                'en' => 'Look here! We already have several similar companies.',
                'ru' => 'Смотри сюда! У нас уже есть несколько подобных компаний.'
            ],
            'ci_search__contact_notification1' => [
                'en' => 'Oops! It looks like a company with the email address',
                'ru' => 'Ой! Компания с email адресом'
            ],
            'ci_search__contact_notification2' => [
                'en' => 'already exists.',
                'ru' => 'уже существует.'
            ],
        ],
        'ita/companies/view' => [
            'company-preview-phones' => [
                'en' => 'Phones',
                'ru' => 'Телефоны'
            ],
            'company-preview-emails' => [
                'en' => 'Emails',
                'ru' => 'Emails'
            ],
            'company-preview-socials' => [
                'en' => 'Socials',
                'ru' => 'Соцсети'
            ],
            'company-preview-messengers' => [
                'en' => 'Messengers',
                'ru' => 'Месенджеры'
            ],
            'company-preview-sites' => [
                'en' => 'Sites',
                'ru' => 'Сайты'
            ],
            'company_preview_edit_contact' => [
                'en' => 'Edit contact',
                'ru' => 'Редактирование контактов'
            ],
            'company_name_label' => [
                'en' => 'Company name',
                'ru' => 'Название компании'
            ],
            'company-edit-popup-enter_phone_number-placeholder' => [
                'en' => 'Phone number',
                'ru' => 'Номер телефона'
            ],
            'company_field_tags' => [
                'en' => 'Tags',
                'ru' => 'Теги'
            ],
            'company_contact_info' => [
                'en' => 'Company info',
                'ru' => 'Данные компании'
            ],
            'company_name_field_placeholder' => [
                'en' => 'Enter company name',
                'ru' => 'Введите название компании'
            ],
            'field_company_type' => [
                'en' => 'Company type',
                'ru' => 'Тип компании'
            ],
            'field_company_type_placeholder' => [
                'en' => 'Select company type',
                'ru' => 'Выберите тип компании'
            ],
            'field_company_kind_activity' => [
                'en' => 'Activities type',
                'ru' => 'Вид деятельности'
            ],
            'field_company_kind_activity_placeholder' => [
                'en' => 'Select company activities',
                'ru' => 'Выберите вид деятельности'
            ],
            'field_company_status' => [
                'en' => 'Company status',
                'ru' => 'Статус компании'
            ],
            'field_company_status_placeholder' => [
                'en' => 'Select company status',
                'ru' => 'Выберите статус'
            ],
            'field_company_adress' => [
                'en' => 'Company address',
                'ru' => 'Адрес компании'
            ],
            'company-editable-table-addressplaceholder-region' => [
                'en' => 'Region',
                'ru' => 'Регион'
            ],
            'company-editable-table-addressplaceholder-city' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'company-editable-table-addressplaceholder-street' => [
                'en' => 'Street',
                'ru' => 'Улица'
            ],
            'company-editable-table-addressplaceholder-house' => [
                'en' => 'House',
                'ru' => 'Дом'
            ],
            'company-editable-table-addressplaceholder-flat' => [
                'en' => 'Flat',
                'ru' => 'Квартира'
            ],
            'company-editable-table-addressplaceholder-postcode' => [
                'en' => 'Postcode',
                'ru' => 'Почтовый код'
            ],
            'company_static_block_request' => [
                'en' => 'Request',
                'ru' => 'Запрос'
            ],
            'company_static_block_commercial_offers' => [
                'en' => 'Commercial offers',
                'ru' => 'Коммерческие предложения'
            ],
            'company_static_block_deals' => [
                'en' => 'Deals',
                'ru' => 'Предложения'
            ],
            'company_static_block_reservations' => [
                'en' => 'Reservations',
                'ru' => 'Бронирование'
            ],
            'company_static_block_your_requests' => [
                'en' => 'Your requests',
                'ru' => 'Ваши запросы'
            ],
            'company_static_block_your_commercial_offers' => [
                'en' => 'Your commercial offers',
                'ru' => 'Ваши коммерческие предложения'
            ],
            'company_static_block_your_deals' => [
                'en' => 'Your deals',
                'ru' => 'Ваши предложения'
            ],
            'company_static_block_your_reservations' => [
                'en' => 'Your reservations',
                'ru' => 'Ваши бронировки'
            ],
            'company-settings-partlet-title' => [
                'en' => 'Settings',
                'ru' => 'Настройки'
            ],
            'company-settings-partlet-responsible_label' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'company_field_created' => [
                'en' => "Created",
                'ru' => 'Создано'
            ],
            'company_field_updated' => [
                'en' => "Updated",
                'ru' => 'Обновлено'
            ],
            'company_delete_success' => [
                'en' => 'f!',
                'ru' => 'Успешно!'
            ],
            'company-settings-partlet-responsible_select_placeholder' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите ответственного'
            ],
            'company_attached_contacts_title' => [
                'en' => 'Company contacts',
                'ru' => 'Контакты компании'
            ],
            'company-edit-popup-add-tag-placeholder' => [
                'en' => 'Add a tag',
                'ru' => 'Добавьте тэг'
            ],
            'company_contact_delete_question' => [
                'en' => 'Delete relation with contact?',
                'ru' => 'Удалить связь с контактом?'
            ],
            'company_delete_question' => [
                'en' => 'Delete this company?',
                'ru' => 'Удалить эту компанию?'
            ],
            'company_no_contacts' => [
                'en' => 'No relations contacts',
                'ru' => 'Нет связанных контактов'
            ],
            'company_added_contact' => [
                'en' => 'Add contact',
                'ru' => 'Добавить контакт'
            ],
            'company_added_contact_placeholder' => [
                'en' => 'Select contact',
                'ru' => 'Выберите контакт'
            ]
        ],
        'ita/orders/index' => [
            'oi_page_title' => [
                'en' => 'Orders',
                'ru' => 'Заявки'
            ],
            'oi_add_request_button' => [
                'en' => 'Add order',
                'ru' => 'Добавить заявку'
            ],
            'oi_delete_order' => [
                'en' => 'Delete order',
                'ru' => 'Удалить заявку'
            ],
            'oi_delete_message1' => [
                'en' => 'Are you sure to delete this',
                'ru' => 'Вы уверены что хотите удалить'
            ],
            'oi_delete_message2' => [
                'en' => 'orders?',
                'ru' => 'заявки?'
            ],
            'oi_income_label' => [
                'en' => 'Income',
                'ru' => 'Доход'
            ],
            'oi_consumption_label' => [
                'en' => 'Consumption',
                'ru' => 'Расход'
            ],
            'oi_profit_label' => [
                'en' => 'Profit',
                'ru' => 'Прибыль'
            ],
            /* Create order modal */
            'oicom_title' => [
                'en' => 'Create order',
                'ru' => 'Создать заявку'
            ],
            'oicom_add_customer_title' => [
                'en' => 'Add customer',
                'ru' => 'Заказчик'
            ],
            'oicom_add_tourists_title' => [
                'en' => 'Add tourists',
                'ru' => 'Туристы'
            ],
            'oicom_customer_is_touris_label' => [
                'en' => 'Customer is a tourist',
                'ru' => 'Заказчик является туристом'
            ],
            'oicom_passport_type_field_label' => [
                'en' => 'Passport type',
                'ru' => 'Тип паспорта'
            ],
            'oicom_tourist_status_checkbox_label' => [
                'en' => 'Tourist',
                'ru' => 'Турист'
            ],
            'oicom_contacts_was_found_title' => [
                'en' => 'Customers was found',
                'ru' => 'Были найдены контакты'
            ],
            /* Document view modal */
            'ovdvm_title' => [
                'en' => 'Document',
                'ru' => 'Документ'
            ],
            'ovdvm_download_pdf_btn' => [
                'en' => 'Download PDF',
                'ru' => 'Загрузить PDF'
            ],
            'ovdvm_download_word_btn' => [
                'en' => 'Download Word',
                'ru' => 'Загрузить Word'
            ],
            'ovdvm_print_btn' => [
                'en' => 'Print',
                'ru' => 'Печать'
            ],
            'ovapm_title' => [
                'en' => 'Add payment',
                'ru' => 'Внести оплату'
            ],
            'ovapm_date_selecktpicker_label' => [
                'en' => 'Payment date',
                'ru' => 'Дата оплаты'
            ],
            'ovapm_date_summ_label' => [
                'en' => 'Summ',
                'ru' => 'Сумма'
            ],
            'ovapm_exchange_rates_label' => [
                'en' => 'Exchange rates',
                'ru' => 'Курсы валют'
            ],
            'ovapm_payment_status_label' => [
                'en' => 'Payment status',
                'ru' => 'Статус оплаты'
            ],
            'ovapm_payment_note_label' => [
                'en' => 'Payment note',
                'ru' => 'Примечания к платежу'
            ],
            'oi_datatable_col_title_1' => [
                'en' => 'Name',
                'ru' => 'ФИО'
            ],
            'oi_datatable_col_title_3' => [
                'en' => 'Оплата',
                'ru' => 'Payment'
            ],
            'oi_datatable_col_title_4' => [
                'en' => 'Responsible',
                'ru' => 'Ответственый'
            ],
            'oi_filter_preset_open_orders_label' => [
                'en' => 'Open orders',
                'ru' => 'Открытые заявки'
            ],
            'oi_filter_preset_only_my_orders_label' => [
                'en' => 'Only my orders',
                'ru' => 'Только мои заявки'
            ],
            'oi_filter_preset_archive_orders_label' => [
                'en' => 'Archive orders',
                'ru' => 'Архив заявок'
            ],
            'oi_filter_contact_data_block_title' => [
                'en' => 'Contact data',
                'ru' => 'Данные контакта'
            ],
            'oi_filter_passport_serial_input_placeholder' => [
                'en' => 'Passport serial number',
                'ru' => 'Серия и номер паспорта'
            ],
            'oi_filter_last_name_input_placeholder' => [
                'en' => 'Last name',
                'ru' => 'Фамилия контакта'
            ],
            'oi_filter_first_name_input_placeholder' => [
                'en' => 'First name',
                'ru' => 'Имя контакта'
            ],
            'oi_filter_contact_phone_input_placeholder' => [
                'en' => 'Contact phone',
                'ru' => 'Телефон контакта'
            ],
            'oi_filter_contact_email_input_placeholder' => [
                'en' => 'Contact email',
                'ru' => 'Email контакта'
            ],
            'oi_filter_contact_tags_input_placeholder' => [
                'en' => 'Contact tags',
                'ru' => 'Тэги контакта'
            ],
            'oi_filter_order_create_dates_picker_placeholder' => [
                'en' => 'Create dates',
                'ru' => 'Даты создания'
            ],
            'oi_filter_order_update_dates_picker_placeholder' => [
                'en' => 'Update dates',
                'ru' => 'Даты обновления'
            ],
            'oi_filter_order_responsible_placeholder' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'oi_filter_order_data_block_title' => [
                'en' => 'Order data',
                'ru' => 'Данные заявки'
            ],
            'oi_filter_order_settings_block_title' => [
                'en' => 'Order settings',
                'ru' => 'Свойства заявки'
            ],
            'oi_filter_order_data_country_title' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'oi_filter_order_data_request_number_title' => [
                'en' => 'Request number',
                'ru' => 'Номер запроса'
            ],
            'oi_filter_order_data_reservation_number_title' => [
                'en' => 'Reservation number',
                'ru' => 'Номер бронировки'
            ],
            'oi_filter_order_data_departure_dates_title' => [
                'en' => 'Enter customer departure date',
                'ru' => 'Выберите даты вылета'
            ],
            'oi_filter_order_data_supplier_prices_title' => [
                'en' => 'Supplier prices',
                'ru' => 'Диапазон цены поставщика'
            ]
        ],
        'ita/orders/view' => [
            'ov_order_word' => [
                'en' => 'Order',
                'ru' => 'Заявки'
            ],
            'ov_preview_edit_contact' => [
                'en' => 'Edit customer',
                'ru' => 'Редактировать заказчика'
            ],
            'ov_customer-preview-phones' => [
                'en' => 'Phones',
                'ru' => 'Телефоны'
            ],
            'ov_edit-popup-enter_phone_number-placeholder' => [
                'en' => 'Phone number',
                'ru' => 'Номер телефона'
            ],
            'ov_contact-preview-emails' => [
                'en' => 'Emails',
                'ru' => 'Emails'
            ],
            'ov_contact-preview-socials' => [
                'en' => 'Socials',
                'ru' => 'Соцсети'
            ],
            'ov_contact-preview-messengers' => [
                'en' => 'Messengers',
                'ru' => 'Месенджеры'
            ],
            'ov_contact-preview-sites' => [
                'en' => 'Sites',
                'ru' => 'Сайты'
            ],
            'ov_contacts_field_tags' => [
                'en' => "Tags",
                'ru' => 'Теги'
            ],
            'ov_page_products_partlet_title' => [
                'en' => 'Products',
                'ru' => 'Продукты'
            ],
            'ov_reminders_portlet_title' => [
                'en' => 'Order reminders',
                'ru' => 'Напоминания'
            ],
            'ov_tourist_pay_partlet_title' => [
                'en' => 'Pay from tourist',
                'ru' => 'Оплата туриста'
            ],
            'ov_make_a_payment_btn_title' => [
                'en' => 'Make a payment',
                'ru' => 'Внести оплату'
            ],
            'ov_receipts_printing_btn_title' => [
                'en' => 'Print receipts',
                'ru' => 'Печать квитанций'
            ],
            'ov_tourist_pay_total_sum_label' => [
                'en' => 'Total sum',
                'ru' => 'Общая стоимость'
            ],
            'ov_tourist_pay_tourist_discount_label' => [
                'en' => 'Tourist discount',
                'ru' => 'Скидка туристу'
            ],
            'ov_tourist_pay_payments_history_label' => [
                'en' => 'Payments history',
                'ru' => 'История платежей'
            ],
            'ov_tourist_pay_total_sum_no_discount_label' => [
                'en' => 'Total sum without discount',
                'ru' => 'Общая стоимость без скидки'
            ],
            'ov_tourist_pay_payed_label' => [
                'en' => 'Payed',
                'ru' => 'Оплачено'
            ],
            'ov_payment_enter_sum' => [
                'en' => 'Enter sum',
                'ru' => 'Введите сумму'
            ],
            'ov_payment_enter_ex_rate' => [
                'en' => 'Enter rate',
                'ru' => 'Введите курс'
            ],
            'ov_tourist_pay_tourist_duty_label' => [
                'en' => 'Tourist duty',
                'ru' => 'Долг туриста'
            ],
            'ov_new_payment' => [
                'en' => 'New payment',
                'ru' => 'Новый платеж'
            ],
            'ov_delete_payment' => [
                'en' => 'Delete payment',
                'ru' => 'Удалить платеж'
            ],
            'ov_tourist_pay_due_date_label' => [
                'en' => 'Due date',
                'ru' => 'Срок оплаты'
            ],
            'ov_tourist_pay_currency_label' => [
                'en' => 'Payment currency',
                'ru' => 'Валюта оплаты'
            ],
            'ov_tourist_pay_exchange_rates_label' => [
                'en' => 'Exchange rates',
                'ru' => 'Курс'
            ],
            'ov_tourist_pay_exchange_rates_change_label' => [
                'en' => 'Set exchange rates',
                'ru' => 'Установите курс'
            ],
            'ov_tourist_pay_exchange_rates_change_placeholder' => [
                'en' => 'Set summ',
                'ru' => 'Введите сумму'
            ],
            'ov_tourist_pay_status_label' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'ov_tourist_pay_payment_details_label' => [
                'en' => 'Payment details',
                'ru' => 'Детали оплаты'
            ],
            'ov_tourist_pay_profit_label' => [
                'en' => 'Profit',
                'ru' => 'Прибыль'
            ],
            'ov_provider_pay_partlet_title' => [
                'en' => 'Payment',
                'ru' => 'Оплата'
            ],
            'ov_provider_pay_total_sum_label' => [
                'en' => 'Total sum',
                'ru' => 'Общая стоимость'
            ],
            'ov_provider_pay_provider_duty_label' => [
                'en' => 'Our duty',
                'ru' => 'Наш долг'
            ],
            'ov_provider_pay_сommission_label' => [
                'en' => 'Commission',
                'ru' => 'Комиссия'
            ],
            'ov_provider_pay_payed_label' => [
                'en' => 'Payed',
                'ru' => 'Оплачено'
            ],
            'ov_provider_pay_due_date_label' => [
                'en' => 'Due date',
                'ru' => 'Срок оплаты'
            ],
            'ov_provider_pay_currency_label' => [
                'en' => 'Payment currency',
                'ru' => 'Валюта оплаты'
            ],
            'ov_provider_pay_exchange_rates_label' => [
                'en' => 'Exchange rates',
                'ru' => 'Курс'
            ],
            'ov_provider_pay_status_label' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'ov_page_docs_partlet_title' => [
                'en' => 'Documents',
                'ru' => 'Документы'
            ],
            'ov_page_docs_subtitle_1' => [
                'en' => 'Generated documents',
                'ru' => 'Сгенерированые документы'
            ],
            'ov_page_docs_subtitle_2' => [
                'en' => 'Simple documents',
                'ru' => 'Обычные документы'
            ],
            'ov_page_add_doc_btn' => [
                'en' => 'Add document',
                'ru' => 'Добавить документ'
            ],
            'ov_page_download_icon_title' => [
                'en' => 'Download',
                'ru' => 'Загрузить'
            ],
            'ov_page_tourists_portlet_title' => [
                'en' => 'Tourists',
                'ru' => 'Туристы'
            ],
            'ov_page_tourists_add_button' => [
                'en' => 'Add tourists',
                'ru' => 'Добавить туриста'
            ],
            'ov_page_tourists_edit_button' => [
                'en' => 'Edit tourists',
                'ru' => 'Редактировать туристов'
            ],
            'ov_page_tourists_table_service_type_title' => [
                'en' => 'Service type',
                'ru' => 'Тип услуги'
            ],
            'ov_page_tourists_table_name_title' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'ov_page_tourists_table_passport_title' => [
                'en' => 'Passport',
                'ru' => 'Пасспорт'
            ],
            'ov_page_tourists_table_passport_birthday_icon' => [
                'en' => 'Birthday',
                'ru' => 'Дата рождения'
            ],
            'ov_page_tourists_table_passport_serial_icon' => [
                'en' => 'Serial number',
                'ru' => 'Серийный номер'
            ],
            'ov_page_tourists_table_end_date_icon' => [
                'en' => 'Passport end date',
                'ru' => 'Срок действия паспорта'
            ],
            'ov_page_tourists_table_visa_title' => [
                'en' => 'Visa',
                'ru' => 'Виза'
            ],
            'ov_page_tourists_table_no_visa_bage' => [
                'en' => 'No visa',
                'ru' => 'Нет визы'
            ],
            'ov_page_tourists_table_not_need_visa_bage' => [
                'en' => 'Visa is not needed',
                'ru' => 'Виза не нужна'
            ],
            'ov_page_settings_partlet_title' => [
                'en' => 'Settings',
                'ru' => 'Настройки'
            ],
            'ov_page_settings_partlet_responsible_label' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'ov_page_settings-partlet-responsible_select_placeholder' => [
                'en' => 'Select responsible',
                'ru' => 'Выберите ответственного'
            ],
            'ov_page__field_created' => [
                'en' => 'Created',
                'ru' => 'Создано'
            ],
            'ov_page__field_updated' => [
                'en' => 'Updated',
                'ru' => 'Обновлено'
            ],
            'ov_page_unlink_connection_question' => [
                'en' => 'Unlink this connection?',
                'ru' => 'Разсоединить эту связь?'
            ],
            'ov_page_unlink_confirmation' => [
                'en' => 'Yes, unlink it!',
                'ru' => 'Да, разсоединить!'
            ],
            'ov_reminders_set_date_link' => [
                'en' => 'Set date',
                'ru' => 'Установить дату'
            ],
            'ov_reminders_set_date_time_link' => [
                'en' => 'Set date & time',
                'ru' => 'Установить дату/время'
            ],
            'ov_service_tourists_title' => [
                'en' => 'Tourists',
                'ru' => 'Туристы'
            ],
            'ov_service_tourists_adult_persons_label' => [
                'en' => 'Adults',
                'ru' => 'Взрослый'
            ],
            'ov_service_tourists_children_persons_label' => [
                'en' => 'Children',
                'ru' => 'Дети'
            ],
            'ov_services_tourists_contact_search_select_placeholder' => [
                'en' => 'Select contact',
                'ru' => 'Выберите контакт'
            ],
            'reminder_due_time' => [
                'en' => 'Time',
                'ru' => 'Время'
            ],
            'ov_select_service_modal_title' => [
                'en' => 'Add service',
                'ru' => 'Добавить услугу'
            ],
            'ov_select_service_modal_title_update' => [
                'en' => 'Update service',
                'ru' => 'Изменить услугу'
            ],
            'ov_select_service_modal_partlet_1_title' => [
                'en' => 'Provider and service price',
                'ru' => 'Поставщик и стоимость услуги'
            ],
            'ov_select_service_modal_partlet_2_title' => [
                'en' => 'Additional services',
                'ru' => 'Дополнительные услуги'
            ],
            'ovmet_title' => [
                'en' => 'Edit tourist',
                'ru' => 'Редактировать туриста'
            ],
            'ovmetp_title' => [
                'en' => 'Edit tourist passports',
                'ru' => 'Редактировать паспорта туриста'
            ],
            'ovmat_title' => [
                'en' => 'Add tourist',
                'ru' => 'Добавить туриста'
            ],
            'ovmad_select_tourist' => [
                'en' => 'Select tourist',
                'ru' => 'Выберите туриста'
            ],
            'ovmet_service_type_select_label' => [
                'en' => 'Service type',
                'ru' => 'Тип'
            ],
            'ovmet_passport_type_select_label' => [
                'en' => 'Passport type',
                'ru' => 'Тип паспорта'
            ],
            'ovmet_passport_first_name_select_label' => [
                'en' => 'First name',
                'ru' => 'Имя'
            ],
            'ovmet_passport_last_name_select_label' => [
                'en' => 'Last name',
                'ru' => 'Фамилия'
            ],
            'ovmet_passport_serial_input_label' => [
                'en' => 'Serial number',
                'ru' => 'Серийный номер'
            ],
            'ovmet_passport_limit_date_label' => [
                'en' => 'Limit date',
                'ru' => 'Срок действия'
            ],
            'ovmet_passport_birth_date_label' => [
                'en' => "Birth date",
                'ru' => 'Дата рождения'
            ],
            'order_current_contact' => [
                'en' => 'Current contact',
                'ru' => 'Текущий контакт'
            ],
            'order_new_contact' => [
                'en' => 'New contact',
                'ru' => 'Новый контакт'
            ],
            'order_select_new_contact' => [
                'en' => 'Select new contact',
                'ru' => 'Выберите новый контакт'
            ],
            'order_change_contact' => [
                'en' => 'Change contact',
                'ru' => 'Изменить контакт'
            ],
            'order_delete_question' => [
                'en' => 'Delete this order?',
                'ru' => 'Удалить этот запрос?'
            ],
            'order_add_reminder' => [
                'en' => 'Add reminder',
                'ru' => 'Добавить напоминание'
            ],
            'order_closed_reminder' => [
                'en' => 'Reminder closed!',
                'ru' => 'Напоминание закрыто!'
            ],
            'order_opened_reminder' => [
                'en' => 'Reminder opened!',
                'ru' => 'Напоминание открыто!'
            ],
            'order_modal_add_reminder_title' => [
                'en' => 'Add reminder',
                'ru' => 'Добавить напоминание'
            ],
            'order_modal_add_reminder_input_name' => [
                'en' => 'Reminder name',
                'ru' => 'Название напоминания'
            ],
            'order_document_modal_generate_doc' => [
                'en' => 'Generate document',
                'ru' => 'Генерировать документ'
            ],
            'order_document_modal_upload_doc' => [
                'en' => 'Upload document',
                'ru' => 'Загрузить документ'
            ],
            'order_document_modal_dropzone_title' => [
                'en' => 'Drop file here or click to upload',
                'ru' => 'Перетащите файл сюда или кликните для загрузки'
            ],
            'order_document_modal_dropzone_desc' => [
                'en' => 'Only image, pdf and MS Office files are allowed for upload',
                'ru' => 'Только изображения, pdf и MS Office файлы разрешены для загрузки'
            ],
            'order_document_modal_upload_doc_btn' => [
                'en' => 'Save document',
                'ru' => 'Сохранить документ'
            ],
            'order_document_not_found' => [
                'en' => 'Documents not found!',
                'ru' => 'Нет документов!'
            ],
            'order_document_upload_success' => [
                'en' => 'Documents successfully uploaded!',
                'ru' => 'Документы успешно загружены!'
            ],
            'order_document_upload_error' => [
                'en' => 'An error occurred while uploading documents!',
                'ru' => 'Во время загрузки документов произошла ошибка!'
            ],
            'order_document_download' => [
                'en' => 'Download document',
                'ru' => 'Скачать документ'
            ],
            'order_document_template_not_found' => [
                'en' => 'Document templates not found!',
                'ru' => 'Шаблоны документов не найдены!'
            ],
            'document_not_save_alert_title' => [
                'en' => 'Oops!',
                'ru' => 'Ой!'
            ],
            'document_not_save_alert_text' => [
                'en' => 'You did not save the document before closing',
                'ru' => 'Вы не сохранили документ перед закрытием'
            ],
            'document_not_save_alert_btn' => [
                'en' => 'Close without saving',
                'ru' => 'Закрыть без сохранения'
            ],
            'document_not_save_alert_btn_cancel' => [
                'en' => 'Cancel',
                'ru' => 'Отменить'
            ],
            'order_settings_request_source_name' => [
                'en' => 'Request source',
                'ru' => 'Источник заявки'
            ],
            'order_select_request_source' => [
                'en' => 'Select request source',
                'ru' => 'Выберите источник заявки'
            ],
            'order_select_request' => [
                'en' => 'Select request',
                'ru' => 'Выберите запрос'
            ],
            'order_request' => [
                'en' => 'Request',
                'ru' => 'Запрос'
            ],
            'order_tourists_passport_limit' => [
                'en' => 'limit date',
                'ru' => 'дейст. до'
            ],
            'order_tourists_passport_age1' => [
                'en' => 'years',
                'ru' => 'год'
            ],
            'order_tourists_passport_age2' => [
                'en' => 'years',
                'ru' => 'года'
            ],
            'order_tourists_passport_age3' => [
                'en' => 'years',
                'ru' => 'лет'
            ],
            'order_tourist_added_type_input' => [
                'en' => 'Tourist type',
                'ru' => 'Тип туриста'
            ],
            'order_not_tourists' => [
                'en' => 'No tourists',
                'ru' => 'Нет туристов'
            ],
            'order_document_upload_wrong_file_ext_error' => [
                'en' => 'Only files with these extensions are allowed: png, jpg, gif, bmp, txt, pdf, doc, docx, xls, xlsx, ppt, pptx, rtf.',
                'ru' => 'Только файлы с этими расширениями доступны для загрузки: png, jpg, gif, bmp, txt, pdf, doc, docx, xls, xlsx, ppt, pptx, rtf.'
            ],
            'order_document_upload_too_big_error' => [
                'en' => 'The file "{file}" is too big. Its size cannot exceed {formattedLimit}.',
                'ru' => 'Размер файла "{file}" превышает допустимый лимит в {formattedLimit}.'
            ],
            'order_document_upload_too_many_error' => [
                'en' => 'You can upload at most {limit, number} {limit, plural, one{file} other{files}}.',
                'ru' => 'Вы можете загрузить не более {limit} файлов.'
            ],
            'order_document_upload_not_upl_error' => [
                'en' => 'Documents not upload!',
                'ru' => 'Документы не загружены!'
            ],
            'order_document_upload_too_big_error_js' => [
                'en' => 'The file is too big. Its size cannot exceed {{maxFilesize}}.',
                'ru' => 'Размер файла превышает допустимый лимит в {{maxFilesize}}.'
            ],
            'order_document_upload_too_many_error_js' => [
                'en' => 'You can upload at most {{maxFiles}} files.',
                'ru' => 'Вы можете загрузить не более {{maxFiles}} файлов.'
            ],
            'order_service_add_tourist_contact_block_title' => [
                'en' => 'Tourist data',
                'ru' => 'Данные туриста'
            ],
            'order_service_add_tourist_tourist_block_title' => [
                'en' => 'Passport data',
                'ru' => 'Паспортные данные'
            ],
            'ov_page_copy_credential_success_message' => [
                'en' => 'Text was copy in buffer',
                'ru' => 'Текст был скопирован в буфер'
            ],
            'order_tourists_table_add_contact' => [
                'en' => 'Add contact',
                'ru' => 'Добавить контакт'
            ],
            'order_tourists_table_add_passport' => [
                'en' => 'Add passport',
                'ru' => 'Добавить паспорт'
            ]
        ],
        'ita/delivery-sms/index' => [
            'dess_sms_text_title' => [
                'en' => 'Sms text',
                'ru' => 'Текст рассылки'
            ],
            'dess_sms_page_title' => [
                'en' => 'Sms deliveries',
                'ru' => 'Sms рассылки'
            ],
            'dess_sms_add_new_btn' => [
                'en' => 'Add delivering',
                'ru' => 'Добавить рассылку'
            ],
            'dess_sms_delete_message1' => [
                'en' => 'Are you sure to delete this',
                'ru' => 'Вы точно хотите удалить'
            ],
            'dess_sms_delete_message2' => [
                'en' => 'items?',
                'ru' => 'рассылок?'
            ],
            'ds_sms_datatable_col_title_1' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'ds_sms_datatable_col_title_2' => [
                'en' => 'Sending date',
                'ru' => 'Дата отправки'
            ],
            'ds_sms_datatable_col_title_3' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'ds_sms_datatable_col_title_4' => [
                'en' => 'Receivers',
                'ru' => 'Получатели'
            ],
            'ds_sms_datatable_col_title_5' => [
                'en' => 'Delivered',
                'ru' => 'Доставлено'
            ],
            'ds_sms_datatable_col_title_6' => [
                'en' => 'Not delivered',
                'ru' => 'Не доставлено'
            ],
            'ds_sms_datatable_col_title_7' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
            'status_ready'=> [
                'en' => 'Ready for send',
                'ru' => 'Готово к отправке'
            ],
            'status_sended'=> [
                'en' => 'Sended',
                'ru' => 'Отправлено'
            ],
            'status_error'=> [
                'en' => 'Error',
                'ru' => 'Ошибка'
            ],
            'status_draft'=> [
                'en' => 'Draft',
                'ru' => 'Черновик'
            ],
            'filter_top_placeholder_field_name' => [
                'en' => 'Name delivery',
                'ru' => 'Название рассылки'
            ],
            'filter_top_placeholder_field_status' => [
                'en' => 'Delivery status',
                'ru' => 'Статус'
            ],
            'filter_top_placeholder_field_provider' => [
                'en' => 'Delivery provider',
                'ru' => 'Провайдер'
            ],
            'filter_top_placeholder_field_date_send' => [
                'en' => 'Date send',
                'ru' => 'Дата отправки'
            ],
            'filter_top_placeholder_field_created' => [
                'en' => 'Owner',
                'ru' => 'Владелец'
            ],
            'all_sended_sms'=>[
                'en' => 'All delivered list',
                'ru' => 'Все рассылки'
            ],
            'my_sended_sms'=>[
                'en' => 'My delivered list',
                'ru' => 'Только мои рассылки'
            ],
        ],
        'ita/delivery-sms/create' => [
            'dess_sms_create_title' => [
                'en' => 'Create sms delivering',
                'ru' => 'Создать sms рассылку'
            ],
            'dess_sms_wizard_step_1_title' => [
                'en' => 'Main settings',
                'ru' => 'Основные настройки'
            ],
            'dess_sms_wizard_step_2_title' => [
                'en' => 'SMS',
                'ru' => 'SMS'
            ],
            'dess_sms_wizard_step_3_title' => [
                'en' => 'Segments',
                'ru' => 'Сегменты'
            ],
            'dess_sms_wizard_step_4_title' => [
                'en' => 'Finishing',
                'ru' => 'Завершение'
            ],
            'dev_sms_stats_contacts_quantity' => [
                'en' => 'Contacts quantity',
                'ru' => 'Кол-во контактов'
            ],
            'dev_sms_stats_contacts_quantity_desc' => [
                'en' => 'Delivered sms quantity',
                'ru' => 'Кол-во отправленных sms'
            ],
            'dev_sms_stats_deliveries_quantity' => [
                'en' => 'Deliveries quantity',
                'ru' => 'Кол-во рассылок'
            ],
            'dess_sms_delivery_name_label' => [
                'en' => 'Delivery name',
                'ru' => 'Название рассылки'
            ],
            'dess_sms_delivery_name_placeholder' => [
                'en' => 'Enter delivery name',
                'ru' => 'Введите название рассылки'
            ],
            'dess_sms_delivery_provider_label' => [
                'en' => 'Provider',
                'ru' => 'Провайдер'
            ],
            'dess_sms_delivery_provider_placeholder' => [
                'en' => 'Select provider',
                'ru' => 'Выберите провайдер'
            ],
            'dess_sms_delivery_sms_body_placeholder' => [
                'en' => 'Enter sms body...',
                'ru' => 'Введите текст смс...'
            ],
            'dess_sms_delivery_sms_body_label' => [
                'en' => 'Text',
                'ru' => 'Текст'
            ],
            'dess_sms_delivery_sms_character_length_label' => [
                'en' => 'Total',
                'ru' => 'Всего'
            ],
            'dess_sms_delivery_sms_count_label' => [
                'en' => 'Sms ',
                'ru' => 'Cмс'
            ],
            'dess_sms_delivery_sms_remain_label' => [
                'en' => 'Remain',
                'ru' => 'Осталось'
            ],
            'dess_wizard_form_has_error_message' => [
                'en' => 'There are some errors in your submission. Please correct them.',
                'ru' => 'В форме есть некоторые ошибки. Пожалуйста, исправьте их.',
            ],
            'dess_sms_delivery_alpha_name_label' => [
                'en' => 'Alpha name',
                'ru' => 'Альфа имя'
            ],
            'dess_sms_delivery_alpha_name_placeholder' => [
                'en' => 'Enter alpha name',
                'ru' => 'Введите альфа имя'
            ],
            'dess_sms_segments_select_label' => [
                'en' => 'Segments select',
                'ru' => 'Выбор сегментов'
            ],
            'dess_sms_search_contacts_placeholder' => [
                'en' => 'Search contact...',
                'ru' => 'Поиск контакта...'
            ],
            'dess_sms_segments_select_placeholder' => [
                'en' => 'Select segments',
                'ru' => 'Выберите сегменты'
            ],
            'dess_sms_preview_label' => [
                'en' => 'Sms preview',
                'ru' => 'Текст sms'
            ],
            'dess_sms_recievers_count_label' => [
                'en' => 'Receivers count',
                'ru' => 'Кол-во получателей'
            ],
            'dess_sms_sending_date_label' => [
                'en' => 'Sending date',
                'ru' => 'Дата отправки'
            ],
            'dont_have_provider_integration_link' => [
                'en' => 'Integration',
                'ru' => 'Интеграция'
            ],
            'dont_have_provider_label' => [
                'en' => 'You does not have active provider and you must activate',
                'ru' => 'У вас нет ни одного провайдера и вам нужно его активировать'
            ],
            'dess_sms_wizard_form_success' => [
                'en' => 'The form has been successfully submitted!',
                'ru' => 'Форма успешно отправлена!'
            ],
            'dess_sms_website_label' => [
                'en' => 'Website',
                'ru' => 'Веб сайт'
            ],
            'dess_sms_current_balance_label' => [
                'en' => 'Current balance',
                'ru' => 'Текущий баланс'
            ],
            'dess_sms_contacts_field_title_full_name' => [
                'en' => 'Full name',
                'ru' => 'ФИО'
            ],
            'dess_sms_contacts_field_title_phone' => [
                'en' => 'Phone',
                'ru' => 'Телефон'
            ],
        ],
        'ita/delivery-sms/view' => [
            'dess_sms_view_title' => [
                'en' => 'Sms delivery',
                'ru' => 'Рассылка sms'
            ],
            'dess_sms_view_recipients_count_label' => [
                'en' => 'Recipients',
                'ru' => 'Получателей'
            ],
            'dess_sms_view_send_label' => [
                'en' => 'Send',
                'ru' => 'Отправлено'
            ],
            'dess_sms_view_delivery_label' => [
                'en' => 'Delivery',
                'ru' => 'Доставлено'
            ],
            'dess_sms_view_error_label' => [
                'en' => 'Error',
                'ru' => 'Ошибок'
            ],
            'dess_sms_view_delivery_rate_label' => [
                'en' => 'Delivery rate',
                'ru' => 'Доставлено'
            ],
            'dess_sms_view_error_rate_label' => [
                'en' => 'Error rate',
                'ru' => 'Ошибки'
            ],
            'ds_sms_report_datatable_col_title_1' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'ds_sms_report_datatable_col_title_2' => [
                'en' => 'Phone',
                'ru' => 'Телефон'
            ],
            'ds_sms_report_datatable_col_title_3' => [
                'en' => 'Contact segments',
                'ru' => 'Сегменты контакта'
            ],
            'ds_sms_report_datatable_col_title_4' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'send_status' => [
                'en' => 'Sended',
                'ru' => 'Отправлено'
            ],
            'delivered_status' => [
                'en' => 'Delivered',
                'ru' => 'Доставлено'
            ],
            'error_status' => [
                'en' => 'Error',
                'ru' => 'Ошибка'
            ],
        ],
        'ita/delivery-email/index' => [
            'dess_email_page_title' => [
                'en' => 'Email deliveries',
                'ru' => 'Email рассылки'
            ],
            'dess_email_add_new_btn' => [
                'en' => 'Add delivering',
                'ru' => 'Добавить рассылку'
            ],
            'dess_email_delete_message1' => [
                'en' => 'Are you sure to delete this ',
                'ru' => 'Вы уверены что хотите удалить '
            ],
            'dess_email_delete_message2' => [
                'en' => ' items?',
                'ru' => ' записей?'
            ],
            'ds_email_datatable_col_name_title' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'ds_email_datatable_col_date_title' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'ds_email_datatable_col_status_title' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'ds_email_datatable_col_receivers_title' => [
                'en' => 'Receivers',
                'ru' => 'Получатели'
            ],
            'ds_email_datatable_col_opened_title' => [
                'en' => 'Opened',
                'ru' => 'Открыто'
            ],
            'ds_email_datatable_col_folowed_title' => [
                'en' => 'Folowed',
                'ru' => 'Переходы'
            ],
            'ds_email_datatable_col_errors_title' => [
                'en' => 'Errors',
                'ru' => 'Ошибки'
            ],
            'ds_email_datatable_col_responsible_title' => [
                'en' => 'Responsible',
                'ru' => 'Ответственный'
            ],
        ],
        'ita/delivery-email/create' => [
            'dess_email_create_title' => [
                'en' => 'Create email delivering',
                'ru' => 'Создать email рассылку'
            ],
            'dess_email_create_wizard_form_has_error_message' => [
                'en' => 'There are some errors in your submission. Please correct them.',
                'ru' => 'В форме есть некоторые ошибки. Пожалуйста, исправьте их.',
            ],
            'dess_email_create_form_success' => [
                'en' => 'The form has been successfully submitted!',
                'ru' => 'Форма успешно отправлена!'
            ],
            'dess_dont_have_provider_label' => [
                'en' => 'You does not have active provider and you must activate',
                'ru' => 'У вас нет ни одного провайдера и вам нужно его активировать'
            ],
            'dess_dont_have_provider_integration_link' => [
                'en' => 'Integration',
                'ru' => 'Интеграция'
            ],
            'dess_website_label' => [
                'en' => 'Website',
                'ru' => 'Веб сайт'
            ],
            'dess_email_current_balance_label' => [
                'en' => 'Current balance',
                'ru' => 'Текущий баланс'
            ],
            'dev_email_stats_contacts_quantity' => [
                'en' => 'Contacts quantity',
                'ru' => 'Кол-во контактов'
            ],
            'dev_email_stats_contacts_quantity_desc' => [
                'en' => 'Delivered sms quantity',
                'ru' => 'Кол-во отправленных sms'
            ],
            'dev_email_stats_deliveries_quantity' => [
                'en' => 'Deliveries quantity',
                'ru' => 'Кол-во рассылок'
            ],
            'dess_email_wizard_step_1_title' => [
                'en' => 'Main settings',
                'ru' => 'Основные настройки'
            ],
            'dess_email_delivery_name_label' => [
                'en' => 'Delivery name',
                'ru' => 'Название рассылки'
            ],
            'dess_email_delivery_name_placeholder' => [
                'en' => 'Enter delivery name',
                'ru' => 'Введите название рассылки'
            ],
            'dess_sms_delivery_theme_label' => [
                'en' => 'Delivery theme',
                'ru' => 'Тема рассылки'
            ],
            'dess_email_search_contacts_placeholder' => [
                'en' => 'Search contact...',
                'ru' => 'Поиск контакта...'
            ],
            'dess_email_subs_count' => [
                'en' => 'Subscribers count',
                'ru' => 'Количество подписчиков'
            ],
            'dess_email_delivery_theme_placeholder' => [
                'en' => 'Enter delivery theme',
                'ru' => 'Введите тему рассылки'
            ],
            'dess_email_delivery_provider_label' => [
                'en' => 'Provider',
                'ru' => 'Провайдер'
            ],
            'dess_email_delivery_provider_placeholder' => [
                'en' => 'Select provider',
                'ru' => 'Выберите провайдер'
            ],
            'dess_email_wizard_step_2_title' => [
                'en' => 'Email body',
                'ru' => 'Тело email'
            ],
            'dess_email_body_select_templates_label' => [
                'en' => 'Templates',
                'ru' => 'Шаблоны'
            ],
            'dess_email_body_select_email_designer_label' => [
                'en' => 'Email designer',
                'ru' => 'Email дизайнер'
            ],
            'dess_email_body_select_raw_html_label' => [
                'en' => 'Raw html',
                'ru' => 'Html редактор'
            ],
            'dess_email_body_my_templates_tab' => [
                'en' => 'My templates',
                'ru' => 'Мои шаблоны'
            ],
            'dess_email_body_gallery_tab' => [
                'en' => 'Gallery',
                'ru' => 'Галерея'
            ],
            'dess_email_body_select_templates_description' => [
                'en' => 'Choose from templates saved on your account or predesigned templates',
                'ru' => 'Выберите из шаблонов, сохраненных в вашей учетной записи, или предварительно настроенных шаблонов'
            ],
            'dess_email_body_select_email_designer_description' => [
                'en' => 'Use our user friendly Email Designer to create a new template',
                'ru' => 'Используйте наш удобный конструктор писем для создания нового шаблона'
            ],
            'dess_email_body_select_raw_html_description' => [
                'en' => 'Create a template directly from HTML code',
                'ru' => 'Создание шаблона непосредственно из HTML-кода'
            ],
            'dess_email_wizard_step_3_title' => [
                'en' => 'Segments',
                'ru' => 'Сегменты'
            ],
            'dess_email_segments_select_label' => [
                'en' => 'Segments select',
                'ru' => 'Выбор сегментов'
            ],
            'dess_email_wizard_step_4_title' => [
                'en' => 'Finishing',
                'ru' => 'Завершение'
            ],
            'dess_email_preview_label' => [
                'en' => 'Email preview',
                'ru' => 'Текст email'
            ],
            'dess_email_recievers_count_label' => [
                'en' => 'Receivers count',
                'ru' => 'Кол-во получателей'
            ],
            'dess_email_sending_date_label' => [
                'en' => 'Sending date',
                'ru' => 'Дата отправки'
            ],
            'dess_email_contacts_field_title_full_name' => [
                'en' => 'Full name',
                'ru' => 'ФИО'
            ],
            'dess_email_contacts_field_title_email' => [
                'en' => 'Email',
                'ru' => 'Email'
            ],
        ],
        'ita/delivery-email/view' => [
            'dess_email_view_title' => [
                'en' => 'Email delivery',
                'ru' => 'Рассылка email'
            ],
            'dess_email_view_recipients_count_label' => [
                'en' => 'Recipients',
                'ru' => 'Получателей'
            ],
            'dess_email_view_subject_label' => [
                'en' => 'Subject',
                'ru' => 'Тема'
            ],
            'dess_email_view_email_link_label' => [
                'en' => 'View email',
                'ru' => 'Просмотр email'
            ],
            'dess_email_view_unsubscribed_label' => [
                'en' => 'Unsubscribed',
                'ru' => 'Отписались'
            ],
            'dess_email_view_open_rate_label' => [
                'en' => 'Open rate',
                'ru' => 'Открыто'
            ],
            'dess_email_view_click_rate_label' => [
                'en' => 'Click rate',
                'ru' => 'Кликнуто'
            ],
            'dess_email_view_opened_label' => [
                'en' => 'Opened',
                'ru' => 'Открыто'
            ],
            'dess_email_view_clicked_label' => [
                'en' => 'Cicked',
                'ru' => 'Кликнуто'
            ],
            'dess_email_view_bounced_label' => [
                'en' => 'Bounced',
                'ru' => 'Не доставлено'
            ],
            'dess_email_report_datatable_col_title_1' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'dess_email_report_datatable_col_title_2' => [
                'en' => 'Email',
                'ru' => 'Email'
            ],
            'dess_email_report_datatable_col_title_3' => [
                'en' => 'Contact segments',
                'ru' => 'Сегменты контакта'
            ],
            'dess_email_report_datatable_col_title_4' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'dess_email_view_successful_deliveries_label' => [
                'en' => 'Successful deliveries',
                'ru' => 'Успешные доставки'
            ],
            'dess_email_view_total_opens_label' => [
                'en' => 'Total opens',
                'ru' => 'Всего открыто'
            ],
            'dess_email_view_last_opened_label' => [
                'en' => 'Last opened',
                'ru' => 'Последнее открытие'
            ],
            'dess_email_view_clicks_per_unique_opens_label' => [
                'en' => 'Clicks per unique opens',
                'ru' => 'Клики по уникальному открытию'
            ],
            'dess_email_view_total_clicks_label' => [
                'en' => 'Total clicks',
                'ru' => 'Всего кликов'
            ],
            'dess_email_view_last_clicked_label' => [
                'en' => 'Last clicked',
                'ru' => 'Последний клик'
            ],
            'dess_email_view_chartjs_title' => [
                'en' => '24-hour performance',
                'ru' => '24-часовая производительность'
            ],
            'dess_email_view_chartjs_opens_label' => [
                'en' => 'Opens',
                'ru' => 'Открыто'
            ],
            'dess_email_view_chartjs_clicks_label' => [
                'en' => 'Clicks',
                'ru' => 'Кликов'
            ],
            'dess_email_view_top_links_title' => [
                'en' => 'Top links clicked',
                'ru' => 'Топ кликнутых ссылок'
            ],
            'dess_email_view_top_subscribers_title' => [
                'en' => 'Subscribers with top opens',
                'ru' => 'Подписчики с найбольшем кол-вом открытий'
            ],
            'dess_email_view_locations_map_title' => [
                'en' => 'Subscribers with top opens',
                'ru' => 'Подписчики с найбольшем кол-вом открытий'
            ],
            'dess_email_view_locations_map_title' => [
                'en' => 'Top locations by opens',
                'ru' => 'Топ локаций по открытиям'
            ],
            'dess_email_view_recievers_table_title' => [
                'en' => 'Recievers',
                'ru' => 'Получатели'
            ]
        ],
        'ita/import/index' => [
            'imp_inx_title' => [
                'en' => 'Import',
                'ru' => 'Импорт'
            ],
            'imp_inx_import_portlet_title' => [
                'en' => 'Imports',
                'ru' => 'Импорты'
            ],
            'imp_inx_go_to_import_btn' => [
                'en' => 'Start an import',
                'ru' => 'Начать импорт'
            ],
            'imp_inx_import_filter_contact_title' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'imp_inx_import_filter_companies_title' => [
                'en' => 'Companies',
                'ru' => 'Компании'
            ],
            'imp_inx_import_filter_requests_title' => [
                'en' => 'Requests',
                'ru' => 'Запросы'
            ],
            'imp_inx_import_filter_orders_title' => [
                'en' => 'Orders',
                'ru' => 'Заявки'
            ],
            'imphst_datatable_col_name_title' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'imphst_datatable_col_date_title' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'imphst_datatable_col_type_title' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'imphst_datatable_col_processed_title' => [
                'en' => 'Processed',
                'ru' => 'Обработанно'
            ],
            'imphst_datatable_col_added_title' => [
                'en' => 'Added',
                'ru' => 'Добавленно'
            ],
            'imphst_datatable_col_errors_title' => [
                'en' => 'Errors',
                'ru' => 'Ошибок'
            ],
            'imphst_datatable_col_actions_title' => [
                'en' => 'Actions',
                'ru' => 'Действия'
            ],
            'imphst_datatable_col_actions_rollback_label' => [
                'en' => 'Import rollback',
                'ru' => 'Откат импорта'
            ],
            'imphst_datatable_col_actions_error_file_label' => [
                'en' => 'Download error file',
                'ru' => 'Cкачать файл с ошибками импорта'
            ],
            'imphst_datatable_col_actions_original_file_label' => [
                'en' => 'Download original file',
                'ru' => 'Cкачать оригинальный файл'
            ]
        ],
        'ita/import/create' => [
            'imp_crt_title' => [
                'en' => 'Create import',
                'ru' => 'Создание импорта'
            ],
            'imp_crt_step_1_title' => [
                'en' => 'Select module',
                'ru' => 'Выбор модуля'
            ],
            'imp_crt_step_2_title' => [
                'en' => 'Upload file',
                'ru' => 'Загрузить файл'
            ],
            'imp_crt_step_3_title' => [
                'en' => 'Properties mapping',
                'ru' => 'Маппинг свойств'
            ],
            'imp_crt_select_module_contacts_label' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'imp_crt_select_module_companies_label' => [
                'en' => 'Compaies',
                'ru' => 'Компании'
            ],
            'imp_crt_select_module_requests_label' => [
                'en' => 'Requests',
                'ru' => 'Запросы'
            ],
            'imp_crt_select_module_orders_label' => [
                'en' => 'Orders',
                'ru' => 'Заявки'
            ],
            'imp_crt_form_has_error_message' => [
                'en' => 'There are some errors in your submission. Please correct them.',
                'ru' => 'В форме есть некоторые ошибки. Пожалуйста, исправьте их.',
            ],
            'imp_crt_select_module_error_message' => [
                'en' => 'You must select a module',
                'ru' => 'Необходимо выбрать модуль'
            ],
            'imp_crt_import_faq_description_title' => [
                'en' => 'Some content about import',
                'ru' => 'Немного информации об импорте'
            ],
            'imp_crt_matches_columns_table_label' => [
                'en' => 'Columns with matches',
                'ru' => 'Колонки с совпадением'
            ],
            'imp_crt_not_matches_columns_table_label' => [
                'en' => 'Columns without matches',
                'ru' => 'Колонки без совпадений'
            ],
            'imp_crt_properties_select_placeholder' => [
                'en' => 'Select property type',
                'ru' => 'Выберите тип свойства'
            ],
            'imp_crt_matches_columns_label_title' => [
                'en' => 'Column label from csv',
                'ru' => 'Название колонки из csv'
            ],
            'imp_crt_matches_columns_preview_title' => [
                'en' => 'Preview information',
                'ru' => 'Содержание'
            ],
            'imp_crt_matches_columns_property_title' => [
                'en' => 'ITA property',
                'ru' => 'ITA свойство'
            ],
            'imp_crt_ignor_warning_message' => [
                'en' => 'Unmatched and unchecked columns will be ignored.',
                'ru' => 'Не отмеченные и не совпадающие столбцы будут проигнорированы.'
            ],
            'imp_crt_proccess_import_btn' => [
                'en' => 'Start import',
                'ru' => 'Начать импорт'
            ]
        ],
        'ita/import/view' => [
            'imp_view_title' => [
                'en' => 'Import detail',
                'ru' => 'Детали импорта'
            ],
            'imp_view_import_portlet_title' => [
                'en' => 'Import properties',
                'ru' => 'Импортируемые свойства',
            ],
            'impedt_datatable_col_contact_name_title' => [
                'en' => 'Name',
                'ru' => 'Имя',
            ],
            'impedt_datatable_col_contact_name_label' => [
                'en' => 'Contact name',
                'ru' => 'Имя контакта',
            ],
            'impedt_datatable_col_contact_last_name_title' => [
                'en' => 'Last name',
                'ru' => 'Фамилия',
            ],
            'impedt_datatable_col_contact_last_name_label' => [
                'en' => 'Contact last name',
                'ru' => 'Фамилия контакта',
            ],
            'impedt_datatable_col_contact_pnone_title' => [
                'en' => 'Phone',
                'ru' => 'Телефон',
            ],
            'impedt_datatable_col_contact_pnone_label' => [
                'en' => 'Contact phone',
                'ru' => 'Телефон контакта',
            ],
            'impedt_datatable_col_contact_email_title' => [
                'en' => 'Email',
                'ru' => 'Email',
            ],
            'impedt_datatable_col_contact_email_label' => [
                'en' => 'Contant email',
                'ru' => 'Email контакта',
            ],
            'impedt_datatable_col_contact_birthday_title' => [
                'en' => 'Birthday',
                'ru' => 'День рождения',
            ],
            'impedt_datatable_col_contact_birthday_label' => [
                'en' => 'Contact birthday',
                'ru' => 'День рождения контакта',
            ],
            'impedt_datatable_contact_birthday_select_placeholder' => [
                'en' => 'Select contact birthday',
                'ru' => 'Укажите день рождения контакта',
            ],
            'impedt_datatable_col_contact_tag_title' => [
                'en' => 'Contact tag',
                'ru' => 'Тэг контакта',
            ],
            'impedt_datatable_col_contact_tag_placeholder' => [
                'en' => 'Select tag',
                'ru' => 'Выберите тэг',
            ],
            'impedt_datatable_col_contact_tag_label' => [
                'en' => 'Contact tag',
                'ru' => 'Тэг контакта',
            ],
        ],
        'ita/segments/index' => [
            'segments_index_page_title' => [
                'en' => 'Segments',
                'ru' => 'Сегменты',
            ],
            'segments_index_add_segment_label' => [
                'en' => 'Add segment',
                'ru' => 'Добавить сегмент',
            ],
            'segments_index_update_segment_label' => [
                'en' => 'Update segment',
                'ru' => 'Редактировать сегмент',
            ],
            'segments_index_delete_message1' => [
                'en' => 'Are you sure to delete this ',
                'ru' => 'Вы уверены что хотите удалить '
            ],
            'segments_index_delete_message2' => [
                'en' => ' items?',
                'ru' => ' записей?'
            ],
            'sidt_col_name_title' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'sidt_col_contacts_count_title' => [
                'en' => 'Сontacts',
                'ru' => 'Контакты'
            ],
            'sidt_col_emails_count_title' => [
                'en' => 'Emails',
                'ru' => 'Emails'
            ],
            'sidt_col_phones_count_title' => [
                'en' => 'Phones',
                'ru' => 'Телефоны'
            ],
            'sidt_col_integration_title' => [
                'en' => 'Integration',
                'ru' => 'Интеграция'
            ],
            'sidt_col_actions_title' => [
                'en' => 'Actions',
                'ru' => 'Действия'
            ],
            'sidt_refresh' => [
                'en' => 'Refresh',
                'ru' => 'Обновить'
            ],
            'sidt_synchronize' => [
                'en' => 'Synchronize',
                'ru' => 'Синхронизировать'
            ],
            'sidt_export' => [
                'en' => 'Export',
                'ru' => 'Экспортировать'
            ],
            'sidt_enabled' => [
                'en' => 'Enabled',
                'ru' => 'Активно'
            ],
            'sidt_disabled' => [
                'en' => 'Disabled',
                'ru' => 'Не активно'
            ],
            'sidt_no_integrations' => [
                'en' => 'No integrations',
                'ru' => 'Нет интеграций'
            ],
            'sidt_filter_title_all' => [
                'en' => 'All',
                'ru' => 'Все'
            ],
            'sidt_filter_title_system' => [
                'en' => 'System',
                'ru' => 'Системные'
            ],
            'sidt_filter_title_my' => [
                'en' => 'My',
                'ru' => 'Мои'
            ],
            'sidt_sys_segment_notification' => [
                'en' => 'System segment, editing is disabled',
                'ru' => 'Системный сегмент, редактирование отключено'
            ],
            'sidt_contacts_table_title' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'sidt_contact_col_name_title' => [
                'en' => 'Name',
                'ru' => 'Имя'
            ],
            'sidt_contact_col_contacts_title' => [
                'en' => 'Contacts',
                'ru' => 'Контакты'
            ],
            'sidt_contact_col_add_date_title' => [
                'en' => 'Add date',
                'ru' => 'Дата добавления'
            ],
            'segment_create_modal_name_label' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'segment_update_modal_name_label' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'segment_create_modal_name_placeholder' => [
                'en' => 'Enter segment name',
                'ru' => 'Введите название сегмента'
            ],
            'segment_create_modal_module_placeholder' => [
                'en' => 'Select module',
                'ru' => 'Выберите модуль'
            ],
            'segment_create_modal_modules_label' => [
                'en' => 'Modules',
                'ru' => 'Модуль'
            ],
            'segment_update_modal_modules_label' => [
                'en' => 'Modules',
                'ru' => 'Модуль'
            ],
            'segment_create_modal_rules_label' => [
                'en' => 'Selection rules',
                'ru' => 'Правила выборки'
            ],
            'segment_update_modal_rules_label' => [
                'en' => 'Selection rules',
                'ru' => 'Правила выборки'
            ],
            'segment_create_modal_tag_placeholder' => [
                'en' => 'Select tag',
                'ru' => 'Выберите тэг',
            ],
            'segment_update_modal_tag_placeholder' => [
                'en' => 'Select tag',
                'ru' => 'Выберите тэг',
            ],
            'segment_create_modal_operator_placeholder' => [
                'en' => 'Select operator',
                'ru' => 'Выберите оператор',
            ],
            'segment_update_modal_operator_placeholder' => [
                'en' => 'Select operator',
                'ru' => 'Выберите оператор',
            ],
            'segment_create_modal_parameter_placeholder' => [
                'en' => 'Select parameter',
                'ru' => 'Выберите параметр',
            ],
            'segment_update_modal_parameter_placeholder' => [
                'en' => 'Select parameter',
                'ru' => 'Выберите параметр',
            ]
        ],
        'ita/integrations-providers/index' => [
            'ip_inx_page_title' => [
                'en' => 'Integrations with providers',
                'ru' => 'Интеграция с провайдерами'
            ],
            'ip_inx_ilp_title' => [
                'en' => 'Integrations',
                'ru' => 'Интеграции'
            ],
            'ip_inx_my_integrations_tab_label' => [
                'en' => 'My integrations',
                'ru' => 'Мои интеграции'
            ],
            'ip_inx_all_integrations_tab_label' => [
                'en' => 'All integrations',
                'ru' => 'Все интеграции'
            ],
            'ip_inx_view_integration_link' => [
                'en' => 'View integration',
                'ru' => 'Детали'
            ],
            'ip_inx_connect_integration_link' => [
                'en' => 'Connect',
                'ru' => 'Подключить'
            ],
            'ip_inx_api_domain_placeholder' => [
                'en' => 'Enter server domain',
                'ru' => 'Введите домен сервера'
            ],
            'ip_inx_api_key_placeholder' => [
                'en' => 'Enter API key',
                'ru' => 'Введите ключ API'
            ],
            'ip_inx_username_placeholder' => [
                'en' => 'Enter username',
                'ru' => 'Введите логин'
            ],
            'ip_inx_password_placeholder' => [
                'en' => 'Enter password',
                'ru' => 'Введите пароль'
            ],
            'ip_inx_api_key' => [
                'en' => 'API key',
                'ru' => 'Ключ API'
            ],
            'ip_inx_ip' => [
                'en' => 'IP address',
                'ru' => 'IP адрес'
            ],
            'ip_inx_alphaName' => [
                'en' => 'Alpha name',
                'ru' => 'Альфа имя'
            ],
            'ip_inx_username' => [
                'en' => 'Username',
                'ru' => 'Логин'
            ],
            'ip_inx_password' => [
                'en' => 'Password',
                'ru' => 'Пароль'
            ],
            'ip_inx_fromName' => [
                'en' => 'From name',
                'ru' => 'Имя отправителя'
            ],
            'ip_inx_fromEmail' => [
                'en' => 'From eMail',
                'ru' => 'eMail отправителя'
            ],
            'ip_inx_integration_params_list_label' => [
                'en' => 'Integration params',
                'ru' => 'Параметры интергации'
            ],
            'ip_inx_additional_config_list_label' => [
                'en' => 'Additional config',
                'ru' => 'Дополнительные параметры'
            ],
            'ip_inx_features_list_label' => [
                'en' => 'Features',
                'ru' => 'Фичи'
            ],
            'ip_header_desc' => [
                'en' => 'This is integrations providers. My integrations.',
                'ru' => 'Это интеграция с провайдерами. Мои интеграции.'
            ],
            'ip_header_desc_all_integrations' => [
                'en' => 'This is integrations providers. All integrations.',
                'ru' => 'Это интеграция с провайдерами. Все интеграции.'
            ],
            'ip_select_provider_type' => [
                'en' => 'Select provider type',
                'ru' => 'Выберите тип провадйера'
            ]
        ],
        'ita/integrations-suppliers/index' => [
            'is_inx_page_title' => [
                'en' => 'Integrations with suppliers',
                'ru' => 'Интеграция с поставщиками'
            ],
            'is_inx_ilp_title' => [
                'en' => 'Integrations',
                'ru' => 'Интеграции'
            ],
            'is_inx_my_integrations_tab_label' => [
                'en' => 'My integrations',
                'ru' => 'Мои интеграции'
            ],
            'is_inx_all_integrations_tab_label' => [
                'en' => 'All integrations',
                'ru' => 'Все интеграции'
            ],
            'is_inx_view_integration_link' => [
                'en' => 'View integration',
                'ru' => 'Детали'
            ],
            'is_inx_connect_integration_link' => [
                'en' => 'Connect',
                'ru' => 'Подключить'
            ],
            'is_inx_api_domain_placeholder' => [
                'en' => 'Enter server domain',
                'ru' => 'Введите домен сервера'
            ],
            'is_inx_api_key_placeholder' => [
                'en' => 'Enter API key',
                'ru' => 'Введите ключ API'
            ],
            'is_inx_username_placeholder' => [
                'en' => 'Enter username',
                'ru' => 'Введите логин'
            ],
            'is_inx_password_placeholder' => [
                'en' => 'Enter password',
                'ru' => 'Введите пароль'
            ],
            'is_inx_api_key' => [
                'en' => 'API key',
                'ru' => 'Ключ API'
            ],
            'is_inx_username' => [
                'en' => 'Username',
                'ru' => 'Логин'
            ],
            'is_inx_password' => [
                'en' => 'Password',
                'ru' => 'Пароль'
            ],
            'is_inx_features_list_label' => [
                'en' => 'Features',
                'ru' => 'Фичи'
            ],
            'is_inx_stats_list_label' => [
                'en' => 'Supplier stats',
                'ru' => 'Данные поставщика'
            ],
            'is_inx_integration_params_list_label' => [
                'en' => 'Integration params',
                'ru' => 'Параметры интергации'
            ],
            'is_inx_integration_stats_country_label' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'is_inx_integration_stats_code_label' => [
                'en' => 'Code',
                'ru' => 'Код'
            ],
            'is_inx_integration_stats_company_label' => [
                'en' => 'Company',
                'ru' => 'Компания'
            ],
            'is_inx_integration_stats_website_label' => [
                'en' => 'Website',
                'ru' => 'Веб сайт'
            ],
            'is_header_desc' => [
                'en' => 'This is integrations suppliers. My integrations.',
                'ru' => 'Это интеграция с поставщиками. Мои интеграции.'
            ],
            'is_header_desc_all_integrations' => [
                'en' => 'This is integrations suppliers. All integrations.',
                'ru' => 'Это интеграция с поставщиками. Все интеграции.'
            ],
            'is_select_supplier_type' => [
                'en' => 'Select supplier type',
                'ru' => 'Выберите тип поставщика'
            ]
        ],
        'ita/templates/email-and-sms-index' => [
            'teasi_portlet_title' => [
                'en' => 'Email and SMS templates',
                'ru' => 'Email и SMS шаблоны'
            ],
            'teasi_filter_title_all' => [
                'en' => 'All',
                'ru' => 'Все'
            ],
            'teasi_filter_title_system' => [
                'en' => 'System',
                'ru' => 'Системные'
            ],
            'teasi_filter_title_my' => [
                'en' => 'My',
                'ru' => 'Мои'
            ],
            'teasi_add_new_template_button' => [
                'en' => 'Add template',
                'ru' => 'Добавить шаблон'
            ],
            'teasi_table_col_name' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'teasi_table_col_type' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'teasi_editor_label' => [
                'en' => 'Template body',
                'ru' => 'Тело шаблона'
            ],
            'teasi_table_col_created_at' => [
                'en' => 'Created at',
                'ru' => 'Создано'
            ],
            'teasi_table_col_updated_at' => [
                'en' => 'Updated at',
                'ru' => 'Изменено'
            ],
            'teasi_table_col_actions' => [
                'en' => 'Actions',
                'ru' => 'Действия'
            ],
            'teasi_sys_template_notice' => [
                'en' => 'System template, edit disabled',
                'ru' => 'Системный шаблон, редактирование отключено'
            ],
            'teasi_filter_type_select_label' => [
                'en' => 'Type',
                'ru' => 'Тип шаблона'
            ],
            'teasi_filter_type_select_placeholder' => [
                'en' => 'Select template type',
                'ru' => 'Выберите тип шаблона'
            ],
            'teasi_email_body_block_title' => [
                'en' => 'Email body',
                'ru' => 'Текст email'
            ],
            'teasi_sms_body_block_title' => [
                'en' => 'SMS body',
                'ru' => 'Текст SMS'
            ],
            'teasi_sms_body_editor_placeholder' => [
                'en' => 'Enter sms body...',
                'ru' => 'Введите текст смс...'
            ],
            'teasi_sms_delivery_sms_character_length_label' => [
                'en' => 'Total',
                'ru' => 'Всего'
            ],
            'teasi_sms_delivery_sms_count_label' => [
                'en' => 'Sms ',
                'ru' => 'Cмс'
            ],
            'teasi_sms_delivery_sms_remain_label' => [
                'en' => 'Remain',
                'ru' => 'Осталось'
            ],
            'teasi_template_name_placeholder' => [
                'en' => 'Enter template name',
                'ru' => 'Введите название шаблона'
            ],
            'teasi_marks_col_title_name' => [
                'en' => 'Mark',
                'ru' => 'Метка'
            ],
            'teasi_copy_mark_btn' => [
                'en' => 'Copy mark',
                'ru' => 'Копировать метку'
            ],
            'teasi_marks_col_title_description' => [
                'en' => 'Description',
                'ru' => 'Описание'
            ],
            'teasi_mark_copy_success_message' => [
                'en' => 'Mark was copied!',
                'ru' => 'Метка скопирована!',
            ],
            'teasi_doc_type_label' => [
                'en' => 'Template type',
                'ru' => 'Тип шаблона',
            ],
            'teasi_doc_type_placeholder' => [
                'en' => 'Select template type',
                'ru' => 'Выберите тип шаблона',
            ],
            'teasi_doc_theme_label' => [
                'en' => 'Template theme',
                'ru' => 'Тема шаблона',
            ],
            'teasi_doc_theme_placeholder' => [
                'en' => 'Enter template theme',
                'ru' => 'Введите тему шаблона',
            ],
            'default_template_name' => [
                'en' => 'New template',
                'ru' => 'Новый шаблон'
            ]
        ],
        'ita/templates/documents-index' => [
            'tdi_portlet_title' => [
                'en' => 'Documents templates',
                'ru' => 'Шаблоны документов'
            ],
            'tdi_filter_title_all' => [
                'en' => 'All',
                'ru' => 'Все'
            ],
            'tdi_filter_title_system' => [
                'en' => 'System',
                'ru' => 'Системные'
            ],
            'tdi_filter_title_my' => [
                'en' => 'My',
                'ru' => 'Мои'
            ],
            'tdi_table_col_name' => [
                'en' => 'Name',
                'ru' => 'Название'
            ],
            'tdi_table_col_type' => [
                'en' => 'Type',
                'ru' => 'Тип'
            ],
            'tdi_table_col_created_at' => [
                'en' => 'Created at',
                'ru' => 'Создано'
            ],
            'tdi_table_col_updated_at' => [
                'en' => 'Updated at',
                'ru' => 'Изменено'
            ],
            'tdi_add_new_template_button' => [
                'en' => 'Add template',
                'ru' => 'Добавить шаблон'
            ],
            'tdi_marks_col_title_name' => [
                'en' => 'Mark',
                'ru' => 'Метка'
            ],
            'tdi_marks_col_title_description' => [
                'en' => 'Description',
                'ru' => 'Описание'
            ],
            'tdi_marks_col_title_example' => [
                'en' => 'Example',
                'ru' => 'Пример'
            ],
            'tdi_copy_mark_btn' => [
                'en' => 'Copy mark',
                'ru' => 'Копировать метку'
            ],
            'tdi_doc_body_block_title' => [
                'en' => 'Document body',
                'ru' => 'Текст документа'
            ],
            'tdi_system_doc_message' => [
                'en' => 'System template',
                'ru' => 'Системный шаблон'
            ],
            'tdi_system_doc_descripton' => [
                'en' => 'Editing is forbidden',
                'ru' => 'Редактирование запрещено'
            ],
            'tdi_system_doc_copy_button' => [
                'en' => 'Create copy',
                'ru' => 'Создать копию'
            ],
            'tdi_table_col_status' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'tdi_status_var_system' => [
                'en' => 'System',
                'ru' => 'Системный'
            ],
            'tdi_status_var_user' => [
                'en' => 'Custom',
                'ru' => 'Пользовательский'
            ],
            'tdi_table_col_actions' => [
                'en' => 'Actions',
                'ru' => 'Действия'
            ],
            'tdi_mark_copy_success_message' => [
                'en' => 'Mark was copied!',
                'ru' => 'Метка скопирована!',
            ],
            'tdi_sys_template_notice' => [
                'en' => 'System template, edit disabled',
                'ru' => 'Системный шаблон, редактирование отключено'
            ],
            'tdi_filter_type_select_label' => [
                'en' => 'Type',
                'ru' => 'Тип шаблона'
            ],
            'tdi_filter_type_select_placeholder' => [
                'en' => 'Select template type',
                'ru' => 'Выберите тип шаблона'
            ],
            'tdi_filter_country_select_label' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'tdi_filter_country_select_placeholder' => [
                'en' => 'Select country',
                'ru' => 'Выберите cтрану'
            ],
            'tdi_filter_supplier_select_label' => [
                'en' => 'Supplier',
                'ru' => 'Поставщик'
            ],
            'tdi_filter_supplier_select_placeholder' => [
                'en' => 'Select template supplier',
                'ru' => 'Выберите поставщика'
            ],
            'tdi_filter_languages' => [
                'en' => 'Languages',
                'ru' => 'Языки'
            ],
            'document_not_save_alert_title' => [
                'en' => 'Oops!',
                'ru' => 'Ой!'
            ],
            'document_not_save_alert_text' => [
                'en' => 'You did not save the document before closing',
                'ru' => 'Вы не сохранили документ перед закрытием'
            ],
            'document_not_save_alert_btn' => [
                'en' => 'Close without saving',
                'ru' => 'Закрыть без сохранения'
            ],
            'document_not_save_alert_btn_cancel' => [
                'en' => 'Cancel',
                'ru' => 'Отменить'
            ],
        ],
        'ita/billing/my-orders-index' => [
            'bmo_your_balance_label' => [
                'en' => 'Your balance',
                'ru' => 'Ваш баланс'
            ],
            'bmo_your_balance_desc' => [
                'en' => 'Amount of money in your account',
                'ru' => 'Кол-во денег на Вашем счету'
            ],
            'bmo_total_amount_label' => [
                'en' => 'Total amount',
                'ru' => 'Общая сумма'
            ],
            'bmo_replenish_account_label' => [
                'en' => 'Replenish an account',
                'ru' => 'Пополнить счет'
            ],
            'bmo_replenish_account_desc' => [
                'en' => 'Choose your preferred payment method',
                'ru' => 'Выберите удобный Вам способ оплаты'
            ],
            'bmo_pay_btn_label' => [
                'en' => 'Payment',
                'ru' => 'Оплатить'
            ],
            'bmo_сhargeable_features_label' => [
                'en' => 'Chargeable features',
                'ru' => 'Предоплаченные функции'
            ],
            'bmo_сhargeable_features_desc' => [
                'en' => 'Sms and email remain',
                'ru' => 'Остаток sms и emails'
            ],
            'bmo_sms_remain_label' => [
                'en' => 'Sms remain',
                'ru' => 'Осталось sms'
            ],
            'bmo_email_remain_label' => [
                'en' => 'Email remain',
                'ru' => 'Осталось email'
            ],
            'bmo_my_orders_portlet_title' => [
                'en' => 'My orders',
                'ru' => 'Мои заявки'
            ],
            'bmo_rate_name_col_name' => [
                'en' => 'Rate name',
                'ru' => 'Названи тарифа'
            ],
            'bmo_status_col_name' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'bmo_valid until_col_name' => [
                'en' => 'Valid until',
                'ru' => 'Актуален до'
            ],
            'bmo_days_before_closing_col_name' => [
                'en' => 'Days before closing',
                'ru' => 'Дней до закрытия'
            ],
            'bmo_cost_col_name' => [
                'en' => 'Cost',
                'ru' => 'Стоимость'
            ],
            'bmo_transactions_portlet_title' => [
                'en' => 'Transactions',
                'ru' => 'Транзакции'
            ],
            'bmo_payment_method_card_label' => [
                'en' => 'Card',
                'ru' => 'Карта'
            ],
            'bmo_payment_method_bill_label' => [
                'en' => 'Bill',
                'ru' => 'Перевод'
            ],
            'bmo_payment_method_invoice_label' => [
                'en' => 'Invoice',
                'ru' => 'Квитанция'
            ],
            'bmo_modal_card_title' => [
                'en' => 'Payment by card',
                'ru' => 'Оплата катрой'
            ],
            'bmo_modal_bill_title' => [
                'en' => 'Payment by bill',
                'ru' => 'Оплата переводом'
            ],
            'bmo_modal_invoice_title' => [
                'en' => 'Payment by invoice',
                'ru' => 'Оплата квитанцией'
            ]
        ],
        'ita/billing/rates-index' => [
            'br_rates_table_title' => [
                'en' => 'Rates',
                'ru' => 'Тарифы'
            ],
            'br_purchase_btn_label' => [
                'en' => 'Purchase',
                'ru' => 'Купить'
            ],
            'br_current_rate_btn_label' => [
                'en' => 'Current',
                'ru' => 'Текущий'
            ],
        ],
        'ita/api/index' => [
            'api_index_page_title' => [
                'en' => 'API',
                'ru' => 'API'
            ],
            'api_index_portlet_title' => [
                'en' => 'ITA API key',
                'ru' => 'ITA API ключ'
            ],
            'api_index_cgeate_new_key_btn' => [
                'en' => 'Create new key',
                'ru' => 'Создать новый ключ'
            ],
            'api_index_datatable_date_col' => [
                'en' => 'Date',
                'ru' => 'Дата'
            ],
            'api_index_datatable_method_col' => [
                'en' => 'Method',
                'ru' => 'Метод'
            ],
            'api_index_datatable_status_col' => [
                'en' => 'Status',
                'ru' => 'Статус'
            ],
            'api_index_datatable_url_col' => [
                'en' => 'URL',
                'ru' => 'URL'
            ],
            'api_index_datatable_ip_col' => [
                'en' => 'IP',
                'ru' => 'IP'
            ],
            'api_index_datatable_user_col' => [
                'en' => 'User',
                'ru' => 'Пользователь'
            ],
            'api_index_datatable_action_col' => [
                'en' => 'Action',
                'ru' => 'Действие'
            ],
            'api_index_datatable_key_col' => [
                'en' => 'Key',
                'ru' => 'Ключ'
            ],
            'api_index_generated_key_input_label' => [
                'en' => 'ITA API key',
                'ru' => 'ITA API ключ'
            ],
            'api_index_generated_key_input_placeholder' => [
                'en' => 'Generated key',
                'ru' => 'Сгенерированный ключ'
            ],
            'api_index_key_copy_succsess_message' => [
                'en' => 'Key was copyed!',
                'ru' => 'Ключ скопирован!'
            ],
            'api_index_key_copy_error_message' => [
                'en' => 'Key copy error! Try to copy key manually',
                'ru' => 'Ошибка копирования ключа! Скопируйте ключ вручную'
            ],
            'api_index_deactivation_proccess_title' => [
                'en' => 'API key deactivation',
                'ru' => 'Деактивация API ключа'
            ],
            'api_index_deactivation_proccess_description' => [
                'en' => 'Are you sure you want to deactivate this key?',
                'ru' => 'Вы действительно хотите деактивировать этот ключ?'
            ],
        ],
        'ita/handbooks/index' => [
            'hbi_page_title' => [
                'en' => 'Handbooks',
                'ru' => 'Справочники'
            ],
            'hbi_nav_services' => [
                'en' => 'Services',
                'ru' => 'Услуги'
            ],
            'hbi_nav_city' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'hbi_nav_hotel' => [
                'en' => 'Hotel',
                'ru' => 'Отель'
            ],
            'hbi_nav_source' => [
                'en' => 'Source',
                'ru' => 'Источник'
            ],
            'hbi_nav_reminders' => [
                'en' => 'Reminders',
                'ru' => 'Напоминания'
            ],
            'hbi_nav_reminders_orders' => [
                'en' => 'Orders reminders',
                'ru' => 'Напоминания заказов'
            ],
            'hbi_nav_reminders_requests' => [
                'en' => 'Requests reminders',
                'ru' => 'Напоминания запросов'
            ],
            'hbi_nav_companies' => [
                'en' => 'Companies',
                'ru' => 'Компании'
            ],
            'hbi_nav_company_type' => [
                'en' => 'Company type',
                'ru' => 'Тип компании'
            ],
            'hbi_nav_activity_type' => [
                'en' => 'Activity type',
                'ru' => 'Вид деятельности'
            ],
            'hbi_nav_company_status' => [
                'en' => 'Company status',
                'ru' => 'Статус компании'
            ],
            'hbi_nav_city_name_column' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'hbi_nav_country_name_column' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'hbi_nav_creator_avatar_column' => [
                'en' => 'Creator',
                'ru' => 'Создатель'
            ],
            'hbi_nav_actions_column' => [
                'en' => 'Actions',
                'ru' => 'Действия'
            ],
            'hbi_nav_hotel_name_column' => [
                'en' => 'Hotel',
                'ru' => 'Отель'
            ],
            'hbi_nav_hotel_rate_column' => [
                'en' => 'Hotel rate',
                'ru' => 'Категория отеля'
            ],
            'hbi_nav_hotel_city_column' => [
                'en' => 'City',
                'ru' => 'Город'
            ],
            'hbi_nav_source_name_column' => [
                'en' => 'Source',
                'ru' => 'Источник'
            ],
            'hbi_nav_reminder_name_column' => [
                'en' => 'Reminder name',
                'ru' => 'Название напоминалки'
            ],
            'hbi_nav_company_type_column' => [
                'en' => 'Company type',
                'ru' => 'Тип компании'
            ],
            'hbi_nav_activity_type_column' => [
                'en' => 'Activity type',
                'ru' => 'Вид деятельности'
            ],
            'hbi_nav_company_status_column' => [
                'en' => 'Company status',
                'ru' => 'Статус компании'
            ],
            'hbi_filter_by_city_name_label' => [
                'en' => 'Filter by city name',
                'ru' => 'Фильтр по названию'
            ],
            'hbi_filter_by_city_name_placeholder' => [
                'en' => 'Enter city name',
                'ru' => 'Введите название города'
            ],
            'hbi_filter_by_country_label' => [
                'en' => 'Filter by country',
                'ru' => 'Фильтр по стране'
            ],
            'hbi_filter_by_country_placeholder' => [
                'en' => 'Select country',
                'ru' => 'Выберите страну'
            ],
            'hbi_system_record_icon_popover_message' => [
                'en' => 'This is the system record',
                'ru' => 'Это системная запись'
            ],
            'hbi_system_record_datatable_message' => [
                'en' => 'System record',
                'ru' => 'Cистемная запись'
            ],
            'hbi_filter_by_hotel_name_label' => [
                'en' => 'Filter by hotel name',
                'ru' => 'Фильтр по названию'
            ],
            'hbi_filter_by_hotel_name_placeholder' => [
                'en' => 'Enter hotel name',
                'ru' => 'Введите название отеля'
            ],
            'hbi_filter_by_hotel_category_label' => [
                'en' => 'Filter by hotel category',
                'ru' => 'Фильтр по категории отеля'
            ],
            'hbi_filter_by_hotel_category_placeholder' => [
                'en' => 'Select hotel category',
                'ru' => 'Выберите категорию отеля'
            ],
            'hbi_filter_hotel_by_city_label' => [
                'en' => 'Filter by city',
                'ru' => 'Фильтр по городу'
            ],
            'hbi_filter_hotel_by_city_placeholder' => [
                'en' => 'Select city',
                'ru' => 'Выберите город'
            ],
            'hbi_modal_add_city_title' => [
                'en' => 'Add city',
                'ru' => 'Добавить город'
            ],
            'hbi_modal_add_hotel_title' => [
                'en' => 'Add hotel',
                'ru' => 'Добавить отель'
            ],
            'hbi_modal_add_source_title' => [
                'en' => 'Add source',
                'ru' => 'Добавить источник'
            ],
            'hbi_modal_add_reminders_orders_title' => [
                'en' => 'Add orders reminder',
                'ru' => 'Добавить напоминалку по заказам'
            ],
            'hbi_modal_add_reminders_requests_title' => [
                'en' => 'Add requests reminder',
                'ru' => 'Добавить напоминалку по запросам'
            ],
            'hbi_modal_add_company_type_title' => [
                'en' => 'Add company type',
                'ru' => 'Добавить тип компании'
            ],
            'hbi_modal_add_activity_type_title' => [
                'en' => 'Add activity type',
                'ru' => 'Добавить вид деятельности'
            ],
            'hbi_modal_add_company_status_title' => [
                'en' => 'Add company status',
                'ru' => 'Добавить статус компании'
            ],
            'hbi_form_city_name_input_label' => [
                'en' => 'City name',
                'ru' => 'Названи города'
            ],
            'hbi_form_city_name_input_placeholder' => [
                'en' => 'Enter city name',
                'ru' => 'Введите название города'
            ],
            'hbi_form_country_select_label' => [
                'en' => 'Country',
                'ru' => 'Страна'
            ],
            'hbi_form_country_select_placeholder' => [
                'en' => 'Select country',
                'ru' => 'Выберите страну'
            ],
            'hbi_form_hotel_name_input_label' => [
                'en' => 'Hotel name',
                'ru' => 'Название отеля'
            ],
            'hbi_form_hotel_name_input_placeholder' => [
                'en' => 'Enter hotel name',
                'ru' => 'Введите название отеля'
            ],
            'hbi_form_hotel_category_select_label' => [
                'en' => 'Hotel category',
                'ru' => 'Категория отеля'
            ],
            'hbi_form_hotel_category_select_placeholder' => [
                'en' => 'Select hotel category',
                'ru' => 'Выберите категорию отеля'
            ],
            'hbi_form_hotel_city_select_label' => [
                'en' => 'Hotel city',
                'ru' => 'Город отеля'
            ],
            'hbi_form_hotel_city_select_placeholder' => [
                'en' => 'Select hotel city',
                'ru' => 'Выберите город отеля'
            ],
            'hbi_form_source_name_input_label' => [
                'en' => 'Source name',
                'ru' => 'Название источника'
            ],
            'hbi_form_source_name_input_placeholder' => [
                'en' => 'Enter source name',
                'ru' => 'Введите название источника'
            ],
            'hbi_form_reminders_orders_name_input_label' => [
                'en' => 'Reminders orders name',
                'ru' => 'Название напоминалок заказов'
            ],
            'hbi_form_reminders_orders_name_input_placeholder' => [
                'en' => 'Enter reminders orders name',
                'ru' => 'Введите название напоминалок заказов'
            ],
            'hbi_form_reminders_requests_name_input_label' => [
                'en' => 'Reminders requests name',
                'ru' => 'Название напоминалок заявок'
            ],
            'hbi_form_reminders_requests_name_input_placeholder' => [
                'en' => 'Enter reminders requests name',
                'ru' => 'Введите название напоминалок заявок'
            ],
            'hbi_form_company_type_name_input_label' => [
                'en' => 'Company type name',
                'ru' => 'Название типа компании'
            ],
            'hbi_form_company_type_name_input_placeholder' => [
                'en' => 'Enter company type name',
                'ru' => 'Введите название типа компании'
            ],
            'hbi_form_activity_type_name_input_label' => [
                'en' => 'Activity type name',
                'ru' => 'Название вида деятельности'
            ],
            'hbi_form_activity_type_name_input_placeholder' => [
                'en' => 'Enter activity type name',
                'ru' => 'Введите название вида деятельности'
            ],
            'hbi_form_company_status_name_input_label' => [
                'en' => 'Company status name',
                'ru' => 'Название статуса компании'
            ],
            'hbi_form_company_status_name_input_placeholder' => [
                'en' => 'Enter company status name',
                'ru' => 'Введите название статуса компании'
            ],
        ]
    ],
    'general' => [
        'close_button' => [
            'en' => 'Close',
            'ru' => 'Закрыть'
        ],
        'send_button' => [
            'en' => 'Send',
            'ru' => 'Отправить',
        ],
        'create_company_modal_title' => [
            'en' => 'New company',
            'ru' => 'Новая компания',
        ],
        'complete' => [
            'en' => 'Complete',
            'ru' => 'Готово'
        ],
        'save_changes_button' => [
            'en' => 'Save changes',
            'ru' => 'Сохранить изменения'
        ],
        'male_sex' => [
            'en' => 'Male',
            'ru' => 'Мужской'
        ],
        'female_sex' => [
            'en' => 'Female',
            'ru' => 'Женский'
        ],
        'saved' => [
            'en' => 'Saved',
            'ru' => 'Сохранено'
        ],
        'gender_male' => [
            'en' => 'Male',
            'ru' => 'Мужской'
        ],
        'gender_female' => [
            'en' => 'Female',
            'ru' => 'Женский'
        ],
        'add' => [
            'en' => 'Add',
            'ru' => 'Добавить'
        ],
        'sign_up' => [
            'en' => 'Sign Up'
        ],
        'sign_in' => [
            'en' => 'Sign In'
        ],
        'cancel' => [
            'en' => 'Cancel',
            'ru' => 'Отмена'
        ],
        'select' => [
            'en' => 'Select',
            'ru' => 'Выбрать'
        ],
        'field_new_email' => [
            'en' => 'New email'
        ],
        'field_pass' => [
            'en' => 'Password',
            'ru' => 'Пароль'
        ],
        'field_new_pass' => [
            'en' => 'New password'
        ],
        'field_company' => [
            'en' => 'Company'
        ],
        'no_responsible_datatable_message' => [
            'en' => 'No responsible',
            'ru' => 'Нет ответственного'
        ],
        'field_first_name' => [
            'en' => 'First name',
            'ru' => 'Имя'
        ],
        'field_middle_name' => [
            'en' => 'Middle name',
            'ru' => 'Отчество'
        ],
        'field_last_name' => [
            'en' => 'Last name',
            'ru' => 'Фамилия'
        ],
        'field_confirm_pass' => [
            'en' => 'Confirm password'
        ],
        'field_confirm_new_pass' => [
            'en' => 'Confirm new password'
        ],
        'send' => [
            'en' => 'Send'
        ],
        'field_company_name' => [
            'en' => 'Company name',
            'ru' => 'Название компании'
        ],
        'marks' => [
            'en' => 'Marks',
            'ru' => 'Метки'
        ],
        'create' => [
            'en' => 'Create',
            'ru' => 'Создать'
        ],
        'field_phone' => [
            'en' => 'Phone',
            'ru' => 'Телефон'
        ],
        'change' => [
            'en' => 'Change'
        ],
        'my_profile' => [
            'en' => 'My profile',
            'ru' => 'Мой профиль'
        ],
        'change_email' => [
            'en' => 'Change email'
        ],
        'change_user' => [
            'en' => 'Change user',
            'ru' => 'Выберите пользователя'
        ],
        'change_pass' => [
            'en' => 'Change password'
        ],
        'save_picture' => [
            'en' => 'Save picture'
        ],
        'logout' => [
            'en' => 'Logout'
        ],
        'set_application' => [
            'en' => 'Set application',
            'ru' => 'Выбрать приложение'
        ],
        'set_tenant' => [
            'en' => 'Set tenant',
            'ru' => 'Мои аккаунты',
        ],
        'new' => [
            'en' => 'New'
        ],
        'reset' => [
            'en' => 'Reset',
            'ru' => 'Сброс',
        ],
        'user_notifications' => [
            'en' => 'User Notifications',
            'ru' => 'Уведомления пользователя'
        ],
        'requests' => [
            'en' => 'Requests',
            'ru' => 'Запросы'
        ],
        'delete_this_source_question' => [
            'en' => 'Delete this source?',
            'ru' => 'Удалить этот источник?'
        ],
        'search_text' => [
            'en' => 'Search and filter...',
            'ru' => 'Поиск и фильтр...'
        ],
        'apply' => [
            'en' => 'Apply',
            'ru' => 'Применить',
        ],
        'email_title' => [
            'en' => 'Email',
            'ru' => 'Email',
        ],
        'enter_link' => [
            'en' => 'Enter URL',
            'ru' => 'Введите URL',
        ],
        'select_country_placeholder' => [
            'en' => 'Select country',
            'ru' => 'Выберите страну',
        ],
        'select_city_placeholder' => [
            'en' => 'Select city',
            'ru' => 'Выберите город',
        ],
        'birthday' => [
            'en' => 'Birthday',
            'ru' => 'День рождения',
        ],
        'not_set' => [
            'en' => 'Not set',
            'ru' => 'Не задано',
        ],
        'error' => [
            'en' => 'Error',
            'ru' => 'Ошибка',
        ],
        'delete_question' => [
            'en' => 'Are you sure?',
            'ru' => 'Вы уверены?'
        ],
        'delete_confirmation' => [
            'en' => 'Yes, delete it!',
            'ru' => 'Да, удалить!'
        ],
        'delete_element_confirmation_text' => [
            'en' => 'Are you sure you want to delete this element?',
            'ru' => 'Вы уверены, что хотите удалить этот элемент?',
        ],
        'delete_cancel' => [
            'en' => 'No, cancel!',
            'ru' => 'Нет, отмена!'
        ],
        'delete' => [
            'en' => 'Delete',
            'ru' => 'Удалить'
        ],
        'deleted' => [
            'en' => 'Deleted',
            'ru' => 'Удалено'
        ],
        'edit' => [
            'en' => 'Edit',
            'ru' => 'Редактировать'
        ],
        'new_file' => [
            'en' => 'New file',
            'ru' => 'Новый файл'
        ],
        'yes' => [
            'en' => 'Yes',
            'ru' => 'Да'
        ],
        'no' => [
            'en' => 'No',
            'ru' => 'Нет'
        ],
        'pagination_first' => [
            'en' => 'First',
            'ru' => 'Первая'
        ],
        'pagination_previous' => [
            'en' => 'Previous',
            'ru' => 'Предыдущая'
        ],
        'pagination_next' => [
            'en' => 'Next',
            'ru' => 'Следующая'
        ],
        'pagination_last' => [
            'en' => 'Last',
            'ru' => 'Последняя'
        ],
        'pagination_more' => [
            'en' => 'More pages',
            'ru' => 'Больше страниц'
        ],
        'pagination_page_number' => [
            'en' => 'Page number',
            'ru' => 'Номер страницы'
        ],
        'pagination_page_size' => [
            'en' => 'Select page size',
            'ru' => 'Выбирете размер страницы'
        ],
        'pagination_records_info' => [
            'en' => 'Displaying {{start}} - {{end}} of {{total}} records',
            'ru' => 'Отображено {{start}} - {{end}} из {{total}} записей'
        ],
        'datatable_data_processiong' => [
            'en' => 'Please wait...',
            'ru' => 'Пожайлуста подождите...'
        ],
        'datatable_records_not_found' => [
            'en' => 'No records found',
            'ru' => 'Нет данных'
        ],
        'datatable_show_all_message' => [
            'en' => 'Show all',
            'ru' => 'Показать все'
        ],
        'account' => [
            'en' => 'Account',
            'ru' => 'Аккаунт'
        ],
        'back' => [
            'en' => 'Back',
            'ru' => 'Назад'
        ],
        'submit' => [
            'en' => 'Submit',
            'ru' => 'Отправить'
        ],
        'save' => [
            'en' => 'Save',
            'ru' => 'Сохранить'
        ],
        'age' => [
            'en' => 'Age',
            'ru' => 'Возраст'
        ],
        'tag' => [
            'en' => 'Tag',
            'ru' => 'Тэг'
        ],
        'parameter' => [
            'en' => 'Parameter',
            'ru' => 'Параметр'
        ],
        'copy' => [
            'en' => 'Copy',
            'ru' => 'Копировать',
        ],
        'deactivate' => [
            'en' => 'Deactivate',
            'ru' => 'Деактивировать',
        ],
        'all' => [
            'en' => 'All',
            'ru' => 'Все',
        ],
        'any' => [
            'en' => 'Any',
            'ru' => 'Любое',
        ],
        'filter' => [
            'en' => 'Filter',
            'ru' => 'Фильтр',
        ],
        'current' => [
            'en' => 'Current',
            'ru' => 'Текущий',
        ],
        'reservations' => [
            'en' => 'Reservations',
            'ru' => 'Бронирование'
        ],
        'to_reserve' => [
            'en' => 'Reserve',
            'ru' => 'Забронировать'
        ],
        'deals' => [
            'en' => 'Deals',
            'ru' => 'Сделки'
        ],
        'sales_owerview' => [
            'en' => 'Sales owerview',
            'ru' => 'Обзор продаж'
        ],
        'owner' => [
            'en' => 'Owner',
            'ru' => 'Владелец'
        ],
        'good_job' => [
            'en' => 'Good job!',
            'ru' => 'Успех!'
        ],
        'next' => [
            'en' => 'Next',
            'ru' => 'Далее'
        ],
        'no_data' => [
            'en' => 'No data',
            'ru' => 'Нет данных'
        ],
        //Requests - create modal
        'request' => [
            'en' => 'Request',
            'ru' => 'Запрос'
        ],
        'mcr_modal_title' => [
            'en' => 'Create request',
            'ru' => 'Создать запрос'
        ],
        'mhr_modal_title' => [
            'en' => 'My requests',
            'ru' => 'Мои запросы'
        ],
        'mhr_modal_commercial_proposals_label' => [
            'en' => 'comm. prop.',
            'ru' => 'ком. пред.'
        ],
        'mhr_modal_compilations_label' => [
            'en' => 'compilations',
            'ru' => 'подборок'
        ],
        'mhr_modal_comm_props_portlet_title' => [
            'en' => 'Commertial proposals',
            'ru' => 'Коммерческие предложения'
        ],
        'mhr_modal_services_list_partlet_title' => [
            'en' => 'Services',
            'ru' => 'Услуги'
        ],
        'mhr_modal_create_proposal_btn' => [
            'en' => 'Create proposal',
            'ru' => 'Создать предложение'
        ],
        'mhr_modal_add_service_btn' => [
            'en' => 'Add service',
            'ru' => 'Добавить услугу'
        ],
        'mhr_select_service_modal_title' => [
            'en' => 'Add service',
            'ru' => 'Добавить услугу'
        ],
        'mhr_select_service_modal_title_update' => [
            'en' => 'Update service',
            'ru' => 'Изменить услугу'
        ],
        'mhr_select_service_modal_partlet_1_title' => [
            'en' => 'Provider and service price',
            'ru' => 'Поставщик и стоимость услуги'
        ],
        'mhr_select_service_modal_partlet_2_title' => [
            'en' => 'Additional services',
            'ru' => 'Дополнительные услуги'
        ],
        'customer' => [
            'en' => 'Customer',
            'ru' => 'Заказчик'
        ],
        'mcr_email_select_placeholder' => [
            'en' => 'Enter customer email',
            'ru' => 'Введите email заказчика'
        ],
        'mcr_phone_select_placeholder' => [
            'en' => 'Enter customer phone',
            'ru' => 'Введите телефон заказчика'
        ],
        'mcr_countries_select_placeholder' => [
            'en' => 'Select countries',
            'ru' => 'Выберите страны'
        ],
        'mcr_first_name_input_placeholder' => [
            'en' => 'Enter customer first name',
            'ru' => 'Введите имя заказчика'
        ],
        'mcr_last_name_input_placeholder' => [
            'en' => 'Enter customer last name',
            'ru' => 'Введите фамилию заказчика'
        ],
        'country' => [
            'en' => 'Country',
            'ru' => 'Страна'
        ],
        'author' => [
            'en' => 'Author',
            'ru' => 'Автор'
        ],
        'status' => [
            'en' => 'Status',
            'ru' => 'Статус'
        ],
        'theme' => [
            'en' => 'Theme',
            'ru' => 'Тема'
        ],
        'number' => [
            'en' => 'Number',
            'ru' => 'Номер'
        ],
        'mcr_city_select_label' => [
            'en' => 'City of departure',
            'ru' => 'Город вылета'
        ],
        'mcr_city_select_placeholder' => [
            'en' => 'Enter city of departure',
            'ru' => 'Введите город вылета'
        ],
        'mcr_departure_date_datepicker_label' => [
            'en' => 'Departure date',
            'ru' => 'Дата вылета'
        ],
        'mcr_departure_date_datepicker_placeholder' => [
            'en' => 'Enter customer departure date',
            'ru' => 'Введите дату вылета'
        ],
        'mcr_trip_duration_inputs_label' => [
            'en' => 'Trip duration',
            'ru' => 'Продолжительность поездки'
        ],
        'mcr_trip_min_days_placeholder' => [
            'en' => 'Min',
            'ru' => 'Минимум'
        ],
        'mcr_trip_max_days_placeholder' => [
            'en' => 'Max',
            'ru' => 'Максимум'
        ],
        'mcr_hotel_category_select_label' => [
            'en' => 'Hotel category',
            'ru' => 'Категория отеля',
        ],
        'mcr_hotel_category_select_placeholder' => [
            'en' => 'Enter hotel category',
            'ru' => 'Введите категорию отеля',
        ],
        'mcr_budget_inputs_label' => [
            'en' => 'Budget',
            'ru' => 'Бюджет',
        ],
        'mcr_min_budget_input_paceholder' => [
            'en' => 'From',
            'ru' => 'От',
        ],
        'mcr_max_budget_input_paceholder' => [
            'en' => 'To',
            'ru' => 'До',
        ],
        'mcr_tourists_group_title' => [
            'en' => 'Tourists group',
            'ru' => 'Состав туристов',
        ],
        'mcr_adults_person_count_label' => [
            'en' => 'Adults persons count',
            'ru' => 'Кол-во взрослых',
        ],
        'mcr_children_count_label' => [
            'en' => 'Сhildren count',
            'ru' => 'Кол-во детей',
        ],
        'mcr_children_age_label' => [
            'en' => 'Children age',
            'ru' => 'Возраст детей',
        ],
        'mcr_additional_parameters' => [
            'en' => 'Additional parameters',
            'ru' => 'Дополнительные параметры',
        ],
        'mcr_other_label' => [
            'en' => 'Other',
            'ru' => 'Другое',
        ],

        'header_select_service_modal_title' => [
            'en' => 'Add service',
            'ru' => 'Добавить услугу'
        ],
        'request_cpp_no_changed_services_error' => [
            'en' => 'You dont change any services!',
            'ru' => 'Вы не выбрали ни одного сервиса!'
        ],
        'request_change_cp_method_title' => [
            'en' => 'Send method',
            'ru' => 'Метод отправки'
        ],
        'request_change_cp_method_email' => [
            'en' => 'Send Email',
            'ru' => 'Отправить Email'
        ],
        'request_change_cp_method_sms' => [
            'en' => 'Send SMS',
            'ru' => 'Отправить SMS'
        ],
        'request_change_cp_method_link' => [
            'en' => 'Share link',
            'ru' => 'Поделиться ссылкой'
        ],
        'request_cp_email_modal_from_who_label' => [
            'en' => 'From who',
            'ru' => 'От кого'
        ],
        'request_cp_email_modal_for_who_label' => [
            'en' => 'For who',
            'ru' => 'Для кого'
        ],
        'request_cp_email_modal_for_who_select_placeholder' => [
            'en' => 'Select phone nubmer',
            'ru' => 'Выберите телефон контакта'
        ],
        'request_cp_email_modal_create_new_contact_phone_btn' => [
            'en' => 'Create new',
            'ru' => 'Создать новый'
        ],
        'request_cp_email_modal_for_who_input_placeholder' => [
            'en' => 'Enter phone nubmer',
            'ru' => 'Введите номер телефона'
        ],
        'request_cp_email_modal_hidden_copy_to_label' => [
            'en' => 'Copy to',
            'ru' => 'Копия для'
        ],
        'request_cp_email_modal_hidden_copy_to_placeholder' => [
            'en' => 'Enter email',
            'ru' => 'Введите email'
        ],
        'request_cp_email_modal_template_label' => [
            'en' => 'Email template',
            'ru' => 'Шаблон Email'
        ],
        'request_cp_email_modal_template_placeholder' => [
            'en' => 'Select email template',
            'ru' => 'Выберите шаблон Email'
        ],
        'request_cp_email_modal_ckeditor_label' => [
            'en' => 'Email body',
            'ru' => 'Текст email'
        ],
        'request_cp_link_modal_link_input_placeholder' => [
            'en' => 'Here must be a link for copy',
            'ru' => 'Здесь должна быть ссылка для копирования'
        ],
        'request_cp_sms_modal_name_label' => [
            'en' => 'SMS name',
            'ru' => 'Название SMS'
        ],
        'request_cp_sms_modal_name_placeholder' => [
            'en' => 'Enter SMS name',
            'ru' => 'Введите название SMS'
        ],
        'request_cp_sms_modal_from_who_label' => [
            'en' => 'From who',
            'ru' => 'От кого'
        ],
        'request_cp_sms_modal_from_who_placeholder' => [
            'en' => 'Your public, personal or alpha name',
            'ru' => 'Ваше публичное, персональное или альфа имя'
        ],
        'request_cp_sms_modal_for_who_label' => [
            'en' => 'For who',
            'ru' => 'Для кого'
        ],
        'request_cp_sms_modal_for_who_select_placeholder' => [
            'en' => 'Select phone nubmer',
            'ru' => 'Выберите телефон контакта'
        ],
        'request_cp_sms_modal_create_new_contact_phone_btn' => [
            'en' => 'Create new',
            'ru' => 'Создать новый'
        ],
        'request_cp_sms_modal_for_who_input_placeholder' => [
            'en' => 'Enter phone nubmer',
            'ru' => 'Введите номер телефона'
        ],
        'request_cp_sms_modal_template_label' => [
            'en' => 'SMS template',
            'ru' => 'Шаблон SMS'
        ],
        'request_cp_sms_modal_template_placeholder' => [
            'en' => 'Select sms template',
            'ru' => 'Выберите шаблон SMS'
        ],
        'request_cp_sms_modal_sms_body_label' => [
            'en' => 'Sms body',
            'ru' => 'Sms текст'
        ],
        'request_cp_sms_modal_sms_character_length_label' => [
            'en' => 'Total',
            'ru' => 'Всего'
        ],
        'request_cp_sms_modal_sms_count_label' => [
            'en' => 'Sms ',
            'ru' => 'Cмс'
        ],
        'request_cp_sms_modal_sms_remain_label' => [
            'en' => 'Remain',
            'ru' => 'Осталось'
        ],
        'choose_source' => [
          'en' => 'Choose source',
          'ru' =>  'Выберите источник'
        ],
        'source' => [
            'en' => 'Source',
            'ru' => 'Источник',
        ],
        'description' => [
            'en' => 'Description',
            'ru' => 'Описание',
        ],
        'clear' => [
            'en' => 'Clear',
            'ru' => 'Очистить'
        ],
        'bootstrap_daterangepicker_apply_btn' => [
            'en' => 'Apply',
            'ru' => 'Выбрать'
        ],
        'bootstrap_daterangepicker_cancel_btn' => [
            'en' => 'Cancel',
            'ru' => 'Закрыть'
        ],
        'bootstrap_daterangepicker_from' => [
            'en' => 'From',
            'ru' => 'От'
        ],
        'bootstrap_daterangepicker_to' => [
            'en' => 'To',
            'ru' => 'До'
        ],
        'delete_forbidden_error' => [
            'en' => 'No permission to delete!',
            'ru' => 'Нет прав на удаление!'
        ],
        'notifications' => [
            'en' => 'Notifications',
            'ru' => 'Уведомления',
        ],
        'success_message' => [
            'en' => 'Success!',
            'ru' => 'Успешно!',
        ],
        'bids' => [
            'en' => 'Bids',
            'ru' => 'Заявки',
        ],
        'orders' => [
            'en' => 'Orders',
            'ru' => 'Заявки',
        ],
        'cant_get_data_error' => [
            'en' => 'Error with data getting!',
            'ru' => 'Неудалось получить данные!'
        ],
        'iframe_browser_error' => [
            'en' => 'Your browser does not support iframe!',
            'ru' => 'Ваш браузер не поддерживает iframe!'
        ],
        'timeline_added' => [
            'en' => 'added',
            'ru' => 'добавил'
        ],
        'timeline_closed' => [
            'en' => 'closed',
            'ru' => 'закрыл'
        ],
        'timeline_note' => [
            'en' => 'note',
            'ru' => 'заметку'
        ],
        'timeline_task' => [
            'en' => 'task',
            'ru' => 'задачу'
        ],
        'timeline_note_in' => [
            'en' => 'note in',
            'ru' => 'заметку в'
        ],
        'timeline_request' => [
            'en' => 'Request',
            'ru' => 'Запросе'
        ],
        'delivery' => [
            'en' => 'Delivery',
            'ru' => 'Рассылка'
        ],
        'no_result' => [
            'en' => 'No result!',
            'ru' => 'Нет результатов!'
        ],
        'save_and_continue' => [
            'en' => 'Save & Continue',
            'ru' => 'Сохранить и продолжить'
        ],
        'timeline_task_name' => [
            'en' => 'Task',
            'ru' => 'Задача'
        ],
        'timeline_task_closed' => [
            'en' => 'Closed',
            'ru' => 'Выполнено'
        ],
        'timeline_task_type' => [
            'en' => 'Task type',
            'ru' => 'Тип задачи'
        ],
        'timeline_load_more' => [
            'en' => 'Load More',
            'ru' => 'Загрузить больше'
        ],
        'segments' => [
            'en' => 'Segments',
            'ru' => 'Сегменты',
        ],
        'integration' => [
            'en' => 'Integration',
            'ru' => 'Интеграция',
        ],
        'providers' => [
            'en' => 'Providers',
            'ru' => 'Провайдеры',
        ],
        'type' => [
            'en' => 'Type',
            'ru' => 'Тип',
        ],
        'search' => [
            'en' => 'Search',
            'ru' => 'Поиск',
        ],
        'settings' => [
            'en' => 'Settings',
            'ru' => 'Настройки'
        ],
        'supplier' => [
            'en' => 'Supplier',
            'ru' => 'Поставщик'
        ],
        'suppliers' => [
            'en' => 'Suppliers',
            'ru' => 'Поставщики'
        ],
        'template' => [
            'en' => 'Template',
            'ru' => 'Шаблон'
        ],
        'templates' => [
            'en' => 'Templates',
            'ru' => 'Шаблоны'
        ],
        'documents' => [
            'en' => 'Documents',
            'ru' => 'Документы'
        ],
        'email_and_sms' => [
            'en' => 'Email and SMS',
            'ru' => 'Email и SMS'
        ],
        'language' => [
            'en' => 'Language',
            'ru' => 'Язык'
        ],
        'payment_status_payed' => [
            'en' => 'Payed',
            'ru' => 'Оплачено'
        ],
        'payment_status_partial' => [
            'en' => 'Partial',
            'ru' => 'Частично оплачено'
        ],
        'payment_status_not_payed' => [
            'en' => 'Payed',
            'ru' => 'Не оплачено'
        ],
        'ov_page_products_success_status_label' => [
            'en' => 'Confirmed',
            'ru' => 'Подтверждена'
        ],
//        'ov_page_payment_diff_warning' => [
//            'en' => 'Confirmed',
//            'ru' => 'Подтверждена'
//        ],
        'ov_page_products_canceled_status_label' => [
            'en' => 'Canceled',
            'ru' => 'Отменена'
        ],
        'ov_page_products_warning_status_label' => [
            'en' => 'Waiting',
            'ru' => 'Ожидание'
        ],
        'ov_page_products_danger_status_label' => [
            'en' => 'Not confirmed',
            'ru' => 'Не подтверждена'
        ],
        'request_cp_link_modal_bufer_copy_system_error' => [
            'en' => 'Copy system error!',
            'ru' => 'Системная ошибка копирования!'
        ],
        'billig_menu_item' => [
            'en' => 'Invoices and payments',
            'ru' => 'Счета и платежи'
        ],
        'my_orders_menu_item' => [
            'en' => 'My orders',
            'ru' => 'Мои заказы'
        ],
        'rates_menu_item' => [
            'en' => 'Rates',
            'ru' => 'Тарифы'
        ],
        'attention' => [
            'en' => 'Attention',
            'ru' => 'Внмание'
        ],
        'responsible' => [
            'en' => 'Responsible',
            'ru' => 'Ответственный'
        ],
        'enter' => [
            'en' => 'Enter',
            'ru' => 'Введите'
        ],
        'default' => [
            'en' => 'Default',
            'ru' => 'По умолчанию'
        ],
        'handbooks' => [
            'en' => 'Handbooks',
            'ru' => 'Справочники'
        ],
        'add_new' => [
            'en' => 'Add new',
            'ru' => 'Добавить новый'
        ],
        'add_new_hotel_title' => [
            'en' => 'Add new hotel',
            'ru' => 'Добавить новый отель'
        ],
        'hotel_name_input_label' => [
            'en' => 'Hotel name',
            'ru' => 'Название отеля'
        ],
        'hotel_category_input_label' => [
            'en' => 'Hotel category',
            'ru' => 'Категория отеля'
        ],
        'hotel_category_input_placeholder' => [
            'en' => 'Select hotel category',
            'ru' => 'Выберите категорию отеля'
        ],
        'hotel_city_input_label' => [
            'en' => 'Hotel city',
            'ru' => 'Город отеля'
        ],
        'hotel_city_input_placeholder' => [
            'en' => 'Select hotel city',
            'ru' => 'Выбрать город отеля'
        ],
        'hotel_country_input_label' => [
            'en' => 'Hotel country',
            'ru' => 'Страна отеля'
        ],
        'hotel_country_input_placeholder' => [
            'en' => 'Select hotel country',
            'ru' => 'Выбрать страну отеля'
        ],
        'hotel_name_input_placeholder' => [
            'en' => 'Enter hotel name',
            'ru' => 'Введите название отеля'
        ],
        'add_new_city_title' => [
            'en' => 'Add new city',
            'ru' => 'Добавить новый город'
        ],
        'city_name_input_label' => [
            'en' => 'City name',
            'ru' => 'Название города'
        ],
        'city_name_input_placeholder' => [
            'en' => 'Enter hotel name',
            'ru' => 'Введите название отеля'
        ],
        'send_status' => [
            'en' => 'Sent',
            'ru' => 'Отправлено'
        ],
        'bounce_status' => [
            'en' => 'Bounce',
            'ru' => 'Отклонено'
        ],
        'nothing_selected' => [
            'en' => 'Nothing selected',
            'ru' => 'Нечего не выбрано'
        ],
    ]
];