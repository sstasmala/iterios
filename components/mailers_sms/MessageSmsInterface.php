<?php
/**
 * MessageSmsInterface.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms;

/**
 * MessageSmsInterface is the interface that should be implemented by sms message classes.
 */
interface MessageSmsInterface
{
    /**
     * Returns alpha name of the message sender.
     * @return string alpha name of the sender
     */
    public function getFrom();

    /**
     * Sets alpha name of the message sender.
     * @param string $from alpha name of the sender.
     * @return $this self reference.
     */
    public function setFrom($from);

    /**
     * Returns the message recipient.
     * @return string the message recipient phone number.
     */
    public function getTo();

    /**
     * Sets the message recipient.
     * @param string $to recipient phone number.
     * @return $this self reference.
     */
    public function setTo($to);

    /**
     * Sets message plain text content.
     * @param string $text message plain text content.
     * @return $this self reference.
     */
    public function setText($text);

    /**
    * Sends this sms message.
    * @param MailerSmsInterface $mailer the mailer that should be used to send this message.
    * If null, the "mail" application component will be used instead.
    * @return bool whether this message is sent successfully.
    */
    public function send(MailerSmsInterface $mailer = null);
}