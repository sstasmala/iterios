<?php
/**
 * SmsMessage.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms\smsclub;

use app\components\mailers_sms\MailerSmsInterface;
use app\components\mailers_sms\MessageSmsInterface;

class SmsMessage implements MessageSmsInterface
{
    /**
     * @var SmsMailer
     */
    private $sms_mailer;

    /**
     * @var string alpha name message sender
     */
    private $from;

    /**
     * @var string phone number message recipient
     */
    private $to;

    /**
     * @var string
     */
    private $text;

    /**
     * Sms message constructor.
     * @param SmsMailer $mailer
     */
    public function __construct(SmsMailer $mailer)
    {
        $this->setSmsMailer($mailer);
    }

    /**
     * @param SmsMailer $mailer
     */
    public function setSmsMailer(SmsMailer $mailer)
    {
        $this->sms_mailer = $mailer;
    }

    /**
     * @return SmsMailer
     */
    public function getSmsMailer()
    {
        return $this->sms_mailer;
    }

    /**
     * Returns the message sender alpha name.
     * @return string the sender
     */
    public function getFrom()
    {
        if ($this->from !== null) {
            return $this->from;
        }

        return '';
    }

    /**
     * Sets the message sender alpha name.
     *
     * @param string $from
     *
     * @return $this self reference.
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Returns the message recipient phone number.
     * @return string the message recipient phone number
     */
    public function getTo()
    {
        if ($this->to !== null) {
            return $this->to;
        }

        return '';
    }

    /**
     * Sets the message recipient phone number.
     * @param string $to receiver phone number.
     * @return $this self reference.
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Sets message text content.
     * @param string $text message text content.
     * @return $this self reference.
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Sends this sms message.
     *
     * @param MailerSmsInterface $mailer the mailer that should be used to send this message.
     *                                   If null, the "mail" application component will be used instead.
     *
     * @return bool whether this message is sent successfully.
     * @throws \RuntimeException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(MailerSmsInterface $mailer = null)
    {
        if (null !== $mailer) {
            if (!($mailer instanceof SmsMailer)) {
                throw new \RuntimeException('Parameter "mailer" has to be instance of "' . SmsMailer::class . '"! Instance of "' . get_class($mailer) . '" given.');
            }

            return $mailer->send($this);
        }

        if (null === $this->getSmsMailer()) {
            throw new \RuntimeException('Mailer was not set');
        }

        return $this->getSmsMailer()->send($this);
    }

    /**
     * @throws \Exception
     */
    public function toArray()
    {
        if (empty($this->from)) {
            throw new \RuntimeException('Sender alpha name was not set');
        }

        if (empty($this->to)) {
            throw new \RuntimeException('Receiver phone number was not set');
        }

        if (empty($this->text)) {
            throw new \RuntimeException('SMS text was not set');
        }

        $data = [
            'from' => urlencode($this->from),
            'to' => str_replace('+', '', $this->to),
            'text' => urlencode(iconv('utf-8','windows-1251', $this->text))
        ];

        return $data;
    }
}