<?php
/**
 * SmsMailer.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms\smsclub;

use app\models\LogsSms;
use GuzzleHttp\Client;
use app\components\mailers_sms\MailerSmsInterface;
use app\components\mailers_sms\MessageSmsInterface;
use app\components\mailers_sms\MailerSmsTrait;
use app\components\mailers_sms\MailerSmsProviderException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;

class SmsMailer implements MailerSmsInterface
{
    use MailerSmsTrait;

    /**
     * @var Client
     */
    private $sms_club;

    public static $default_config = [
        'token' => [
            'username' => null,
            'token' => null
        ],
        'login' => [
            'username' => null,
            'password' => null
        ]
    ];

    public static $smsclub_events = [
        'ENROUTE' => LogsSms::STATUS_ENROUTE,
        'DELIVRD' => LogsSms::STATUS_DELIVERED,
        'EXPIRED' => LogsSms::STATUS_EXPIRED,
        'UNDELIV' => LogsSms::STATUS_UNDELIVERED,
        'REJECTD' => LogsSms::STATUS_REJECTED
    ];

    /**
     * SmsMailer constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->sms_club = $this->composeClient($config);
    }

    private function composeClient($config)
    {
        $config = $this->mergeConfig($config);

        $handler = new HandlerStack();
        $handler->setHandler(new CurlHandler());

        if (!empty($config['token']['token']) && !empty($config['token']['username'])) {
            // Add username
            $handler->unshift(Middleware::mapRequest(function(RequestInterface $request) use ($config) {
                return $request->withUri(Uri::withQueryValue($request->getUri(), 'username', $config['token']['username']));
            }));

            // Add token
            $handler->unshift(Middleware::mapRequest(function(RequestInterface $request) use ($config) {
                return $request->withUri(Uri::withQueryValue($request->getUri(), 'token', $config['token']['token']));
            }));

            return new Client(['base_uri' => 'https://gate.smsclub.mobi/token/', 'handler' => $handler]);
        }

        if (!empty($config['login']['username']) && !empty($config['login']['password'])) {
            // Add username
            $handler->unshift(Middleware::mapRequest(function(RequestInterface $request) use ($config) {
                return $request->withUri(Uri::withQueryValue($request->getUri(), 'username', $config['login']['username']));
            }));

            // Add password
            $handler->unshift(Middleware::mapRequest(function(RequestInterface $request) use ($config) {
                return $request->withUri(Uri::withQueryValue($request->getUri(), 'password', $config['login']['password']));
            }));

            return new Client(['base_uri' => 'https://gate.smsclub.mobi/http/', 'handler' => $handler]);
        }

        return null;
    }

    /**
     * @param $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \RuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function clientSendSms($data)
    {
        if ($this->sms_club === null || !($this->sms_club instanceof Client)) {
            throw new \RuntimeException('Client SMS Club not found or provider credentials is empty.');
        }

        return $this->sms_club->request('GET', 'index.php', [
            'query' => $data
        ]);
    }

    /**
     * @param $sms_id
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function clientGetStatusSms($sms_id)
    {
        return $this->sms_club->request('GET', 'state.php', [
            'query' => [
                'smscid' => $sms_id
            ]
        ]);
    }

    /**
     * @param $response
     *
     * @return bool
     */
    private function clientCheckResponse($response)
    {
        $response_array = explode('<br/>', $response);

        return false !== strpos($response_array[0], 'IDS START');
    }

    /**
     * @param $response
     *
     * @return string
     */
    private function getResponseSmsId($response)
    {
        list(, $id) = explode('<br/>', $response);

        return trim($id);
    }

    /**
     * @param $response
     *
     * @return string
     * @throws \RuntimeException
     * @throws \app\components\mailers_sms\MailerSmsProviderException
     */
    private function getResponseSmsStatus(\Psr\Http\Message\ResponseInterface $response)
    {
        if ($response->getStatusCode() !== 200) {
            throw new \RuntimeException('Bad request!');
        }

        $body = (string) $response->getBody();

        list(, $status_string) = explode('<br/>', $body);
        list(, $status) = explode(':', trim($status_string));

        $status = trim($status);

        if (!array_key_exists($status, $this->getEvents())) {
            throw new MailerSmsProviderException($status);
        }

        return $this->getEvents()[$status];
    }

    /**
     * @param $response
     *
     * @return string
     */
    private function getResponseError($response)
    {
        list($error) = explode('<br/>', $response);

        return trim($error);
    }

    /**
     * @param $sms_id
     *
     * @return string
     * @throws \RuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \app\components\mailers_sms\MailerSmsProviderException
     */
    public function getSmsStatus($sms_id)
    {
        return $this->getResponseSmsStatus($this->clientGetStatusSms($sms_id));
    }

    /**
     * @return array
     */
    public function getDefaultConfig()
    {
        return static::$default_config;
    }

    public function getEvents()
    {
        return static::$smsclub_events;
    }

    /**
     * Creates a new message instance and optionally composes its body content via view rendering.
     *
     * @return MessageSmsInterface message instance.
     */
    public function compose()
    {
        return new SmsMessage($this);
    }

    /**
     * Sends the given sms message.
     *
     * @param MessageSmsInterface $message sms message instance to be sent
     *
     * @return bool whether the message has been sent successfully
     * @throws \RuntimeException
     * @throws \app\components\mailers_sms\MailerSmsProviderException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send($message)
    {
        if (!($message instanceof SmsMessage)) {
            throw new \RuntimeException('Parameter "message" has to be instance of "' . SmsMessage::class . '"! Instance of "' . get_class($message) . '" given.');
        }

        $res = $this->clientSendSms($message->toArray());

        if ($res->getStatusCode() !== 200) {
            throw new \RuntimeException('Bad request!');
        }

        $body = (string) $res->getBody();

        if (!$this->clientCheckResponse($body))
            throw new MailerSmsProviderException($this->getResponseError($body));

        return $this->getResponseSmsId($body);
    }
}