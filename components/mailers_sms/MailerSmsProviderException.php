<?php
/**
 * MailerSmsProviderException.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms;


class MailerSmsProviderException extends \Exception
{

}