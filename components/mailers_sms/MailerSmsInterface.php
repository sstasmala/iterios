<?php
/**
 * MailerSmsInterface.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms;

/**
 * MailerSmsInterface is the interface that should be implemented by mailer sms classes.
 */
interface MailerSmsInterface
{
    /**
     * Creates a new message instance and optionally composes its body content via view rendering.
     *
     * @return MessageSmsInterface message instance.
     */
    public function compose();

    /**
     * Sends the given sms message.
     * @param MessageSmsInterface $message sms message instance to be sent
     * @return bool whether the message has been sent successfully
     */
    public function send($message);

    /**
     * Returns the sms mailer default config.
     *
     * @return array mailer config
     */
    public function getDefaultConfig();

    /**
     * Merge the sms mailer config with default config.
     *
     * @param $config
     *
     * @return array
     */
    public function mergeConfig($config);

    /**
     * @param $sms_id
     *
     * @return string - sms status
     */
    public function getSmsStatus($sms_id);

    /**
     * @return array - sms provider events
     */
    public function getEvents();
}