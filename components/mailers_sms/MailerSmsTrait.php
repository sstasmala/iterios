<?php
/**
 * MailerSmsTrait.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\mailers_sms;

trait MailerSmsTrait
{
    /**
     * @param $config
     *
     * @return array
     */
    public function mergeConfig($config)
    {
        $default_config = $this->getDefaultConfig();

        return array_replace_recursive($default_config,
            array_intersect_key(
                $config, $default_config
            )
        );
    }
}