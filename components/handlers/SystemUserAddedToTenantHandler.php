<?php
/**
 * SystemUserAddedToTenantHandler.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\handlers;

use app\components\base\BaseTemplatableHandler;
use app\components\notifications\contacts\ReceiversFactory;
use app\components\notifications\contacts\SenderFactory;
use app\components\notifications\NotificationFactory;
use app\components\notifications\Notificator;
use app\helpers\LogHelper;
use app\models\EmailProviders;
use app\models\Jobs;
use app\models\Settings;
use app\models\Templates;
use app\models\TemplatesHandlers;

class SystemUserAddedToTenantHandler extends BaseTemplatableHandler
{
    /**
     * @param \app\components\base\BaseEvent $event
     *
     * @throws \Exception
     */
    public static function handler($event)
    {
        $schema = [
            'FirstName' => $event->user->first_name,
            'MiddleName' => $event->user->middle_name,
            'LastName' => $event->user->last_name,
            'Phone' => $event->user->phone,
            'Email' => $event->user->email,
            'TenantName' => $event->tenant->name
        ];
        $receiver = ReceiversFactory::createReceiverFormUser($event->user);
        $sender = SenderFactory::create(SenderFactory::SENDER_SYSTEM_MAIL);
        $template_h = TemplatesHandlers::find()->where(['like','handler',self::class])->one();

        if (is_null($template_h)) {
            if (YII_DEBUG && YII_ENV_DEV)
                throw new \Exception('Email handler template not found!');

            return;
        }

        $template = Templates::findOne($template_h->template_id);

        if (is_null($template)) {
            if (YII_DEBUG && YII_ENV_DEV)
                throw new \Exception('Email template not found!');

            return;
        }

        $template = $template->translate(\Yii::$app->user->identity->tenant->language->iso,true);
        $notification = NotificationFactory::createFormTemplate($template,$schema);
        $mailer = Settings::findOne(['key'=>Settings::PROVIDER_SYSTEM]);

        if (is_null($mailer)) {
            if (YII_DEBUG && YII_ENV_DEV)
                throw new \Exception('System mailer not specified!');

            return;
        }

        $provider = EmailProviders::findOne(['id'=>$mailer->value]);

        if (is_null($provider)) {
            if (YII_DEBUG && YII_ENV_DEV)
                throw new \Exception('Mail provider not found!');

            return;
        }

        $mailer = [
            'class'=>$provider->provider,
            'param'=>$provider->api_key
        ];

        $log = LogHelper::saveEmailLog($notification, $receiver, $template);

        $notificator = new Notificator([
            'notification'=>$notification,
            'receivers'=>$receiver,
            'sender'=>$sender,
            'email_channel'=>$mailer,
            'email_log'=>$log,
            'channels'=>[Notificator::CHANNEL_EMAIL],
            'provider'=>$provider
        ]);
        $data = serialize($notificator);
        $job = new Jobs();
        $job->executor = 'app\components\executors\NotificationExecutor';
        $job->data = $data;
        $job->save();
    }

    /**
     * @return array
     */
    public static function getShortcodes()
    {
        $shortcodes = [
            'FirstName' => 'Ivan',
            'MiddleName' => 'Ivanovich',
            'LastName' => 'Ivanov',
            'Phone' => '+380999999999',
            'Email' => 'example@email.com',
            'TenantName' => 'Tenant'
        ];
        return $shortcodes;
    }

    /**
     * @return string
     */
    public static function getName()
    {
        return 'User added to tenant';
    }
}