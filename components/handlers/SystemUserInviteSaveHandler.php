<?php
/**
 * SystemUserInviteSaveHandler.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\handlers;


use app\components\base\BaseHandlerInterface;
use app\models\Invites;

class SystemUserInviteSaveHandler implements BaseHandlerInterface
{
    public static function handler($event)
    {
        $invite = Invites::findOne(['user_id' => $event->user->id, 'tenant_id' => $event->tenant->id]);

        if (is_null($invite))
            $invite = new Invites();

        $invite->token = $event->token;
        $invite->user_id = $event->user->id;
        $invite->tenant_id = $event->tenant->id;
        $invite->user_role = $event->role;

        $invite->save();
    }
}