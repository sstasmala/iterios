<?php
/**
 * Created by PhpStorm.
 * User: valeks
 * Date: 21.04.18
 * Time: 11:54
 */

namespace app\components\executors;

use app\helpers\PrintHelper;
use app\models\Jobs;
use app\models\LogsSms;

class UpdateSmsStatuses extends BaseExecutorInterface
{
    /**
     * @param null $data
     */
    public function execute($data = null)
    {
        $logs_sms = $this->getLogsSms();

        if (false !== $logs_sms) {
            /**
             * @var $log LogsSms
             */
            foreach ($logs_sms as $log) {
                if (empty($log->sms_id)) {
                    continue;
                }

                if (!class_exists($log->provider->provider) || empty($log->provider->credentials_config)) {
                    continue;
                }

                try {
                    /**
                     * @var $sms_mailer \app\components\mailers_sms\MailerSmsInterface
                     */
                    $sms_mailer = new $log->provider->provider(json_decode($log->provider->credentials_config, true));
                    $log->status = $sms_mailer->getSmsStatus($log->sms_id);
                    $log->save();

                    PrintHelper::printMessage('SMS status checked: ID ' . $log->sms_id . ' (STATUS - '.$log->status.')',
                        PrintHelper::SUCCESS_TYPE);
                } catch (\Exception $e) {
                    PrintHelper::printMessage('SMS status not checked(ID #' . $log->sms_id . '): "'.$e->getMessage().'"',
                        PrintHelper::WARNING_TYPE);
                }
            }

            if (false !== $this->getLogsSms()) {
                $job = new Jobs();
                $job->executor = 'app\components\executors\UpdateSmsStatuses';
                $job->save();
            }
        }
    }

    /**
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    private function getLogsSms()
    {
        $logs = LogsSms::find()
            ->where(['status' => LogsSms::STATUS_SENT])
            ->orWhere(['status' => LogsSms::STATUS_ENROUTE])
            ->andWhere('sms_id IS NOT NULL')
            ->all();

        return empty($logs) ? false : $logs;
    }
}