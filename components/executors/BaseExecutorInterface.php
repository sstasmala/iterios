<?php

/**
 * BaseExecutorInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\executors;


abstract class BaseExecutorInterface
{
    abstract public function execute($data);
}