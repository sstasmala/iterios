<?php
/**
 * NotificationExecuter.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\executors;


//use app\components\notifications\Notificator;
use app\helpers\PrintHelper;
use app\models\UiTranslations;

class UpdateTranslateExecutor extends BaseExecutorInterface
{
    public function execute($data = null)
    {
        $path = \Yii::$app->basePath . '/data/translations.map.php';
        $data = require $path;
        $pages = $data['pages'];
        $general = $data['general'];

        foreach ($pages as $group => $page) {
            foreach ($page as $code => $item) {
                $model = UiTranslations::findOne(['code' => $code]);
                if ($model) {
                    foreach ($item as $lang => $value) {
                        $translate = $model->translate($lang);
                        if ($translate->value == null) {
                            $model->value = $value;
                            $model->saveWithLang($lang);
                        }
                    }
                    $model->group = 'pages:'.$group;
                    $model->save();
                } else {
                    $newModel = new UiTranslations;
                    foreach ($item as $lang => $value) {
                        $newModel->value = $value;
                        $newModel->group = 'pages:'.$group;
                        $newModel->code = $code;
                        $newModel->saveWithLang($lang);
                    }
                }
            }
        }

        foreach ($general as $code => $item) {
            $model = UiTranslations::findOne(['code' => $code]);
            if ($model) {
                foreach ($item as $lang => $value) {
                    $translate = $model->translate($lang);
                    if ($translate->value == null) {
                        $model->value = $value;
                        $model->saveWithLang($lang);
                    }
                }
                $model->group = 'general';
                $model->save();
            } else {
                $newModel = new UiTranslations;
                foreach ($item as $lang => $value) {
                    $newModel->value = $value;
                    $newModel->group = 'general';
                    $newModel->code = $code;
                    $newModel->saveWithLang($lang);
                }
            }
        }

        PrintHelper::printMessage('Translation updated');

    }


}