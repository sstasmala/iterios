<?php
/**
 * NotificationExecuter.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\executors;


use app\helpers\PrintHelper;
use app\models\SegmentsResultList;

class UpdateSegmentsListExecutor extends BaseExecutorInterface
{
    public function execute($data = null)
    {
        SegmentsResultList::loadFromRelation();

        PrintHelper::printMessage('SegmentsList updated');
    }


}