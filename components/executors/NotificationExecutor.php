<?php
/**
 * NotificationExecuter.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\executors;


use app\components\notifications\Notificator;
use app\helpers\PrintHelper;

class NotificationExecutor extends BaseExecutorInterface
{
    public function execute($data)
    {
        /** @var Notificator $notificator */
        $dump = '';
        while(!feof($data))
        {
            $dump .= fgets($data);
        }
        $notificator = unserialize($dump);
        $notificator->send();
        PrintHelper::printMessage('Send message to: '.json_encode($notificator->getReceiversArray()),PrintHelper::SUCCESS_TYPE);
    }
}