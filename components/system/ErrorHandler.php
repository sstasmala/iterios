<?php
/**
 * ErrorHandler.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\system;


use Raven_Client;
use yii\web\BadRequestHttpException;
use yii\web\ErrorAction;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class ErrorHandler extends ErrorAction
{
    public $event_id = null;

    public function init()
    {
        parent::init();
        $this->exception;
        if(YII_ENV_PROD)
        {
            if($this->exception instanceof HttpException && ($this->exception->statusCode == 404 || $this->exception->statusCode == 403 || $this->exception->statusCode == 400))
                return;
            $client = new Raven_Client('https://8aee1bc4d73240a0a0cdaa817f415ce5:208f26234996464ca3e7495a8c8eb2bb@sentry.iterios.com/2');
            if(!($this->exception instanceof NotFoundHttpException) && !($this->exception instanceof BadRequestHttpException))
                $this->event_id = $client->captureException($this->exception,[
                    'tags' => ['env' => YII_ENV_PROD?'prod':'dev'],
                    'user' => [
                        'id'=>(!is_null(\Yii::$app->user))?\Yii::$app->user->getId():'system',
                        'tenant_id'=>(!is_null(\Yii::$app->user))?\Yii::$app->user->identity->tenant->id:null
                    ]
                ]);
        }
    }

    protected function getViewRenderParams()
    {
//        \Yii::$app->response->headers->add('Access-Control-Allow-Origin','*');
        return [
            'name' => $this->getExceptionName(),
            'message' => $this->getExceptionMessage(),
            'exception' => $this->exception,
            'event_id' => $this->event_id
        ];
    }
}