<?php


namespace app\components\twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FinderExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('finder', array($this, 'finderFilter')),
        );
    }

    public function finderFilter($str, $data)
    {
        if ($str !== '') {
            $result = '<span class="tooltip" title="{{' . $data . '}}">' . $str . '</span>';
        } else {
            $result = '<span>{{'.$data.'}}</span>';
        }


        return $result;
    }

}