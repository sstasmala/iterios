<?php
/**
 * PublicSmsCountFeature.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\features;


use app\components\features\base\BaseFeature;

class PublicSmsCountFeature extends BaseFeature
{
    const COUNTABLE = true;
}