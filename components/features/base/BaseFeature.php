<?php
/**
 * BaseFeature.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\features\base;


abstract class BaseFeature
{
    const COUNTABLE = false;
}