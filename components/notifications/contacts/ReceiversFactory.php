<?php
/**
 * ReceiversFactory.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\notifications\contacts;



use app\models\User;

class ReceiversFactory
{
    /**
     * @param $user User
     * @return Receiver
     * @throws \Exception
     */
    public static function createReceiverFormUser(User $user)
    {
        if(is_null($user) || !isset($user->email))
            throw  new \Exception('Invalid user');
        $receiver = new Receiver();

        $receiver->setEmail($user->email);
        $receiver->setPhone($user->phone);
        $receiver->setSystemId($user->id);
        return $receiver;
    }

    /**
     * @param $tourist Tourists
     * @return Receiver
     * @throws \Exception
     */
//    public static function createReceiverFormTourist(Tourists $tourist)
//    {
//        if(is_null($tourist) || !isset($tourist->email))
//            throw  new \Exception('Invalid tourist');
//        $receiver = new Receiver();
//        $receiver->setEmail($tourist->email);
//        return $receiver;
//    }

    /**
     * @param $email string
     * @return Receiver
     */
    public static function createReceiver($email, $phone, $system_id)
    {
        $receiver = new Receiver();
        $receiver->setEmail($email);
        return $receiver;
    }

}