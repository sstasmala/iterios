<?php
/**
 * ReplyToFactory.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\notifications\contacts;


class ReplyToFactory
{
    /**
     * @return Contact
     * @throws \Exception
     */
    public static function create()
    {
//        $email_params = \Yii::$app->params['email'];
//        if(is_null($email_params)){
//            throw new \Exception("Parameter 'email' was not found.");
//        }
//
//        if(!isset($email_params['reply_to'])){
//            throw new \Exception("Parameter 'reply_to' in 'email' was not found.");
//        }
//        $reply_to_params = $email_params['reply_to'];
//
//        if (!isset($reply_to_params['email'])) {
//            throw new \Exception("Parameter 'email' in 'reply_to' was not found.");
//        }

        $contact = new Contact();
        //TODO Change when setting will be ready
//        $contact->setEmail($reply_to_params['email']);

        return $contact;
    }
}