<?php
/**
 * Contact.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\notifications\contacts;


class Contact
{
    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var integer
     */
    protected $system_id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @param string $email
     * @throws \Exception
     */
    public function setEmail($email)
    {
        if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            throw new \Exception('Invalid email format');
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $name
     */
    public function setSystemId($system_id)
    {
        $this->system_id = $system_id;
    }

    /**
     * @return string
     */
    public function getSystemId()
    {
        return $this->system_id;
    }
}