<?php
/**
 * SenderFactory.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\notifications\contacts;


use app\models\Settings;

class SenderFactory
{
    const SENDER_SYSTEM = 'systemSender'; // common/config/params-local.php
    const SENDER_SYSTEM_MAIL = 'mailSystemSender';
    const SENDER_PUBLIC_MAIL = 'mailPublicSender';
    const SENDER_SMS = 'smsSender';

    /**
     * @param string $type
     * @return Sender
     * @throws \Exception
     */
    public static function create($type)
    {
        //TODO Change when settings will be ready
        $sender = new Sender();
        $system_email = Settings::findOne(['key'=>Settings::FROM_EMAIL]);
        $system_name = Settings::findOne(['key'=>Settings::FROM_NAME]);

        switch ($type)
        {
//            case static::SENDER_SMS: $sender->setPhone('9999999999'); break;
//            case static::SENDER_SYSTEM: $sender->setSystemId('0'); break;
            case static::SENDER_PUBLIC_MAIL:
                $sender->setName(null === $system_name ? null : $system_name->value);
                $sender->setEmail(null === $system_email ? null : $system_email->value);
                break;
            case static::SENDER_SYSTEM_MAIL:
                $sender->setName(null === $system_name ? null : $system_name->value);
                $sender->setEmail(null === $system_email ? null : $system_email->value);
                break;
            default:  throw new \Exception("Sender with alias '$type' not found!");
        }
        return $sender;
    }
}