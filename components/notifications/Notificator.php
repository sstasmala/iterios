<?php
/**
 * Notificator.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\notifications;

//use app\components\mailers\MailerProviderException;
//use common\models\EmailLog;
//use common\notifications\contacts\Contact;
//use common\notifications\contacts\Receiver;
//use common\notifications\contacts\Sender;
//use common\notifications\mailers\BaseMailerInterface;
//use common\notifications\mailers\BaseMessageInterface;
//use common\notifications\mailers\MailerProviderException;
use app\components\mailers\BaseMailerInterface;
use app\components\mailers\BaseMessageInterface;
use app\components\mailers\MailerProviderException;
use app\components\notifications\contacts\Contact;
use app\components\notifications\contacts\Receiver;
use app\components\notifications\contacts\Sender;
use app\components\notifications\Notification;
use app\components\notifications\NotificationMetadata;
use app\models\EmailProviders;
use app\models\LogsEmail;
use Exception;
use yii\base\Configurable;

/**
 * Class Notificator
 * @package common\notifications
 */
class Notificator implements Configurable
{
    const CHANNEL_EMAIL = 'email';
    const CHANNEL_SMS = 'sms';
    const CHANNEL_SYSTEM = 'system';
    const CHANNEL_TASK = 'task';
    /**
     * @var BaseMailerInterface $email_channel
     */
    public $email_channel;
    /**
     * @var Notification $notification
     */
    public $notification = null;
    /**
     * @var Receiver[]|Receiver $receivers
     */
    public $receivers = null;
    /**
     * @var Sender $sender
     */
    public $sender = null;
    /**
     * @var NotificationMetadata $meta
     */
    public $meta = null;
    /**
     * @var LogsEmail$email_log
     */
    public $email_log = null;
    /**
     * @var Sender $replay_to
     */
    public $reply_to = null;

    /**
     * @var array $channels
     */
    public $channels = [];

    /**
     * @var EmailProviders $provider
     */
    public $provider = null;

    public function __construct($config = [])
    {
        \Yii::configure($this,$config);
        if(!is_array($this->receivers))
            $this->receivers = [$this->receivers];
//        $this->email_channel = $this->email_channel->get();
    }

    private function sendByEmail()
    {
        /** @var BaseMailerInterface $email */
        $email = new $this->email_channel['class']($this->email_channel['param']);

        /** @var BaseMessageInterface $message */
        $message = $email->compose();
        if(is_null($this->notification->getHtml()))
            throw new Exception('Missing html!');
        if(is_null($this->notification->getText()))
            throw new Exception('Missing text!');
        if(is_null($this->notification->getSubject()))
            throw new Exception('Missing subject!');
        $message->setHtmlBody($this->notification->getHtml());
        $message->setTextBody($this->notification->getText());
        $message->setSubject($this->notification->getSubject());
        $message->setFrom($this->getEmailData($this->sender));
        $message->setTo($this->receiversToArrayEmails($this->receivers));
        if ($this->reply_to) {
            $message->setReplyTo($this->getEmailData($this->reply_to));
        }

        if(is_null($this->provider))
            throw new Exception("Email provider is not set!");
        if(is_null($this->email_log))
            throw new Exception("Email log is not set!");

        $message->setMetadata([
            'email_log_id' => $this->email_log->id,
            'email_provider_id' => $this->provider->id
        ]);
        $email->send($message);
    }

    /**
     * @param $notification Notification
     * @param $receivers Receiver[]|Receiver
     * @param $sender Sender
     * @param $meta NotificationMetadata
     */
    public function send()
    {

        foreach ($this->channels as $channel)
        switch ($channel)
        {
            case self::CHANNEL_EMAIL:$this->sendByEmail();break;
            case self::CHANNEL_SMS:break;
            case self::CHANNEL_SYSTEM:break;
            case self::CHANNEL_TASK:break;
        }
    }

    /**
     * @param Receiver[] $receivers
     * @return array of email
     */
    protected function receiversToArrayEmails(array $receivers)
    {
        return array_map(function(Receiver $receiver) {
            return $receiver->getEmail();
        }, $receivers);
    }

    public function getReceiversArray()
    {
        return array_map(function(Receiver $receiver) {
            return ['email'=>$receiver->getEmail(),'phone'=>$receiver->getPhone(),'system_id'=>$receiver->getSystemId()];
        }, $this->receivers);
    }


    private function sendByTask()
    {

    }

    private function sendByNotice()
    {

    }

    /**
     * @param Contact $contact
     * @return array|string
     * @throws Exception
     */
    protected function getEmailData(Contact $contact)
    {
        $result = null;
        if (empty($contact->getEmail())) {
            throw new Exception("Contact has to have email");
        }
        if (!empty($contact->getName())) {
            $email = $contact->getEmail();
            $result = [
                $email => $contact->getName(),
            ];
        } else {
            $result = $contact->getEmail();
        }
        return $result;
    }
}