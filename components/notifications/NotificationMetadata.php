<?php
/**
 * NotificationMetadata.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\notifications;


//use common\models\EmailLog;
use app\components\notifications\contacts\Contact;
use app\components\notifications\contacts\ReplyToFactory;

class NotificationMetadata
{
    /**
     * @var EmailLog
     */
    protected $email_log;

    /**
     * @var Contact
     */
    protected $reply_to;

    /**
     * @param EmailLog $email_log
     */
    public function setEmailLog(EmailLog $email_log)
    {
        $this->email_log = $email_log;
    }

    /**
     * @return EmailLog
     */
    public function getEmailLog()
    {
        return $this->email_log;
    }

    /**
     * @return Contact
     */
    public function getReplyTo()
    {
        if (is_null($this->reply_to)) {
            $this->reply_to = ReplyToFactory::create();
        }
        return $this->reply_to;
    }
}