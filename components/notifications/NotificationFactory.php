<?php
/**
 * NotificationFactory.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\notifications;


use app\helpers\TwigHelper;
use app\models\Templates;

class NotificationFactory
{
    /**
     * @param $template Templates
     * @param array $schema
     */
    public static function createFormTemplate($template,$schema = [])
    {   

        if(is_null($template) || !isset($template->subject) || !isset($template->body))
            throw new \Exception('Invalid template');

        $subject = TwigHelper::render($schema,$template->subject);
        $html = TwigHelper::render($schema,$template->body);
        $text = html_entity_decode(strip_tags($html));
        $notification = new Notification(['subject'=>$subject,'html'=>$html,'text'=>$text]);
        return $notification;
    }

    /**
     * @param $template array
     * @param array $schema
     */
    public static function createFromArray($template,$schema = [])
    {
        if(isset($template['subject']) && !is_null($template['subject']))
            $template['subject'] = TwigHelper::render($schema,$template['subject']);
        if(isset($template['body']) && !is_null($template['body'])) {
            $template['body'] = TwigHelper::render($schema, $template['body']);
            $template['text'] = html_entity_decode(strip_tags($schema, $template['body']));
        }
        $notification = new Notification($template);
        return $notification;
    }
}