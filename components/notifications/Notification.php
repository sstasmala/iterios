<?php
/**
 * Notification.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\notifications;


use yii\base\Configurable;

class Notification implements Configurable
{
    public $subject;

    public $html;

    public $text;

    public $sms;

    public $system_notification;

    public $task_name;

    public $task_body;

    /**
     * Notification constructor.
     * @param $subject string
     * @param $html string
     * @param $text string
     */
    public function __construct($config = [])
    {
        \Yii::configure($this,$config);
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    public function getSms()
    {
        return $this->sms;
    }

    public function getSystemNotifications()
    {
        return $this->system_notification;
    }

    public function getTaskName()
    {
        return $this->task_name;
    }

    public function getTaskBody()
    {
        return $this->task_body;
    }
}