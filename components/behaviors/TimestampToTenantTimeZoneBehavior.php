<?php
/**
 * TimestampToTenantTimeZoneBehavior.php
 * @copyright ©ita.local
 * @author Denis Kuchugurniy denis290593@gmail.com
 */

namespace app\components\behaviors;

use app\helpers\DateTimeHelper;
use yii\behaviors\AttributeBehavior;
use yii\base\InvalidConfigException;

class TimestampToTenantTimeZoneBehavior extends AttributeBehavior
{

    public $timeAttribute;

    public function getValue($event)
    {

        if (empty($this->timeAttribute)) {
            throw new InvalidConfigException(
                'Can`t find "fromAttribute" property in ' . $this->owner->className()
            );
        }

        if (!empty($this->owner->{$this->attributes[$event->name]})) {
            $this->owner->{$this->timeAttribute} = strtotime(
                $this->owner->{$this->attributes[$event->name]}
            );

            return date(isset(\Yii::$app->user->identity->tenant->date_format) ? \Yii::$app->user->identity->tenant->date_format : "Y-m-d H:i:s", $this->owner->{$this->timeAttribute});
        } else if (!empty($this->owner->{$this->timeAttribute})
            && is_numeric($this->owner->{$this->timeAttribute})
        ) {

            $this->owner->{$this->attributes[$event->name]} = DateTimeHelper::convertTimestampToDateTime(
                $this->owner->{$this->timeAttribute},
                isset(\Yii::$app->user->identity->tenant->date_format) ? \Yii::$app->user->identity->tenant->date_format : "Y-m-d H:i:s",
                isset(\Yii::$app->user->identity->tenant->timezone) ? \Yii::$app->user->identity->tenant->timezone : 'UTC'
            );

            return $this->owner->{$this->attributes[$event->name]};
        }

        return null;

    }

}