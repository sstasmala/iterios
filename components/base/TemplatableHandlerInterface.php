<?php

/**
 * TemplatableHandlerInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\base;

interface TemplatableHandlerInterface extends BaseHandlerInterface
{
    /**
     * @return array
     */
    public static function getShortcodes();
}