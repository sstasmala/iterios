<?php
/**
 * EventEmitter.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\base;


use app\components\base\BaseEvent;
use app\components\base\BaseHandlerInterface;
use yii\base\Configurable;

class EventEmitter implements Configurable
{

    public $map;

    private $handlers = [];

    /**
     * @return mixed
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @return array
     */
    public function getHandlers(): array
    {
        return $this->handlers;
    }

    /**
     * @param $event BaseEvent
     * @param $handler BaseHandlerInterface
     */
    public function on($event,$handler)
    {
        if(class_exists($event) && class_exists($handler)){
            BaseEvent::on(BaseEvent::className(),$event::getName(),[$handler,'handler']);
            array_push($this->handlers,$handler);
        }
    }

    /**
     *
     */
    private function init()
    {
        foreach ($this->map as $event => $handler)
        {
            if (is_array($handler))
                foreach ($handler as $h)
                    $this->on($event,$h);
            else
                $this->on($event,$handler);
        }
    }

    /**
     * EventEmitter constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        \Yii::configure($this,$config);
        $this->init();
    }

    /**
     * @param $event BaseEvent
     */
    public static function trigger($event)
    {
        BaseEvent::trigger(BaseEvent::className(),$event->getName(),$event);
    }
}