<?php
/**
 * BaseTemplatableHandler.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\base;

//use common\models\EmailLog;
//use common\models\Partners;
//use common\models\Templates;
//use common\notifications\contacts\Receiver;
//use common\notifications\contacts\Sender;
//use common\notifications\Notification;

abstract class BaseTemplatableHandler implements TemplatableHandlerInterface
{
    /**
     * @param $template Templates
     * @param $notification Notification
     * @param $receiver Receiver
     * @param $type string
     * @param Partners|null $partner
     * @param bool $conversion
     * @return EmailLog|false
     */
//    public static function getLog($template,$notification,$receiver,$type, Partners $partner = null, $conversion = true)
//    {
//        $log = new EmailLog();
//        $log->template_id = $template->id;
//        $log->recipient = $receiver->getEmail();
//        $log->status = EmailLog::STATUS_WAIT;
//        $log->subject = $notification->getSubject();
//        $log->html = $notification->getHtml();
//        $log->text = $notification->getText();
//        $log->type = $type;
//
//        if ($partner) {
//            $log->partner_id = $partner->id;
//        }
//
//        $log->conversion = $conversion;
//
//        if($log->save())
//            return $log;
//        return false;
//    }

    /**
     * @return string
     */
    abstract public static function getName();

    /**
     * @return array [shortcode_name => (string) fake_data, ...]
     */
    abstract public static function getShortcodes();
}