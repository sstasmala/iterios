<?php

/**
 * BaseHandlerInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\base;

use app\components\base\BaseEvent;

interface BaseHandlerInterface
{
    /**
     * @param $event BaseEvent
     */
    public static function handler($event);
}