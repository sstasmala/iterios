<?php
/**
 * SystemResetPasswordEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;


use app\components\base\BaseEvent;
use app\models\User;

class SystemResetPasswordEvent extends BaseEvent
{
    public $user = null;
    public $url = null;

    /**
     * EventUserRegistration constructor.
     * @param $user User
     * @param $password
     * @param array $config
     */
    public function __construct($user, $config = [])
    {
        if(is_null($user))
            throw new \Exception("User is required!");

        $this->name = $this->getName();
        $this->user = $user;
        $this->url = \Yii::$app->params['baseUrl'].'/change-password?token='.$user->password_reset_token;
        parent::__construct($config);
    }

    public static function getName()
    {
        return 'ResetPasswordEvent';
    }
}