<?php
/**
 * SystemUserInviteEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;


use app\components\base\BaseEvent;
use app\models\User;

class SystemUserInviteEvent extends BaseEvent
{
    public $user = null;
    public $tenant = null;
    public $token = null;
    public $role = null;

    /**
     * EventUserRegistration constructor.
     *
     * @param       $user User
     * @param       $tenant
     * @param       $token
     * @param $role
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct($user, $tenant, $token, $role, $config = [])
    {
        if (is_null($user))
            throw new \Exception("User is required!");

        if (is_null($tenant))
            throw new \Exception("Tenant is required!");

        if (is_null($token))
            throw new \Exception("Token is required!");

        if (empty($role))
            throw new \Exception("Role is required!");

        $this->name = $this->getName();
        $this->user = $user;
        $this->tenant = $tenant;
        $this->token = $token;
        $this->role = $role;

        parent::__construct($config);
    }

    public static function getName()
    {
        return 'UserInviteTenantEvent';
    }
}