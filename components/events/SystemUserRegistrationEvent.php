<?php
/**
 * SystemUserRegistrationEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;


use app\components\base\BaseEvent;
use app\models\User;

class SystemUserRegistrationEvent extends BaseEvent
{
    public $user = null;
    public $password = null;
    public $lang = null;

    /**
     * EventUserRegistration constructor.
     * @param $user User
     * @param $password
     * @param $lang
     * @param array $config
     */
    public function __construct($user, $password, $lang, $config = [])
    {
        $this->name = $this->getName();
        $this->user = $user;
        $this->password = $password;
        $this->lang = $lang;

        parent::__construct($config);
    }

    public static function getName()
    {
        return 'UserRegistrationEvent';
    }
}