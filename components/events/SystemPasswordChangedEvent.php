<?php
/**
 * SystemResetPasswordEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;


use app\components\base\BaseEvent;
use app\models\User;

class SystemPasswordChangedEvent extends BaseEvent
{
    public $user = null;
    public $password = null;

    /**
     * EventPasswordChanged constructor.
     *
     * @param       $user User
     * @param       $password
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct($user, $password, $config = [])
    {
        if(is_null($user))
            throw new \Exception("User is required!");

        $this->name = $this->getName();
        $this->user = $user;
        $this->password = $password;
        parent::__construct($config);
    }

    public static function getName()
    {
        return 'PasswordChangedEvent';
    }
}