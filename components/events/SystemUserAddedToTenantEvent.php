<?php
/**
 * SustemUserAddedToTenantEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;


use app\components\base\BaseEvent;
use app\models\User;

class SystemUserAddedToTenantEvent extends BaseEvent
{
    public $user = null;
    public $tenant = null;

    /**
     * EventUserRegistration constructor.
     *
     * @param       $user User
     * @param       $tenant
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct($user, $tenant, $config = [])
    {
        if(is_null($user))
            throw new \Exception("User is required!");
        if(is_int($tenant))
            throw new \Exception("Tenant is required!");
        $this->name = $this->getName();
        $this->user = $user;
        $this->tenant = $tenant;
        parent::__construct($config);
    }

    public static function getName()
    {
        return 'UserAddedToTenantEvent';
    }
}