<?php
/**
 * SystemUserInviteEvent.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\events;

use app\components\base\BaseEvent;
use app\models\User;

class SystemTaskEmailReminderEvent extends BaseEvent
{
    public $task = null;
    public $user = null;

    /**
     * SystemTaskEmailReminderEvent constructor
     *
     * @param       $task
     * @param       $user User
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct($task, $user, $config = [])
    {
        if(is_null($task))
            throw new \Exception("Task is required!");
        if(is_null($user))
            throw new \Exception("User is required!");

        $this->name = $this->getName();
        $this->task = $task;
        $this->user = $user;

        parent::__construct($config);
    }

    public static function getName()
    {
        return 'SystemTaskEmailReminderEvent';
    }
}