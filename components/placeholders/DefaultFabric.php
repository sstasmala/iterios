<?php
/**
 * DefaultFabric.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders;

use app\components\placeholders\base\CustomPlaceholder;
use app\components\placeholders\modules\base\PlaceholderModule;
use app\components\placeholders\modules\AgentModule;
use app\components\placeholders\modules\TouristModule;

class DefaultFabric extends CustomPlaceholder
{
    private $tourist;
    private $agent;

    /**
     * @return \app\components\placeholders\modules\base\PlaceholderModule
     */
    public function getTourist(): PlaceholderModule
    {
        if ($this->tourist === null)
            $this->tourist = new TouristModule();

        return $this->tourist;
    }

    /**
     * @return \app\components\placeholders\modules\base\PlaceholderModule
     */
    public function getAgent(): PlaceholderModule
    {
        if ($this->agent === null)
            $this->agent = new AgentModule();

        return $this->agent;
    }
}