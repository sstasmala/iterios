<?php
/**
 * ServiceModule.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders\modules;

use app\components\placeholders\modules\base\PlaceholderModule;
use app\models\ServicesFieldsDefault;

class ServiceModule implements PlaceholderModule
{
    /**
     * PlaceholderModule constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
    }

    /**
     * @return array
     */
    public static function getFields(): array
    {
        return ServicesFieldsDefault::find()
            ->select(['code'])->column();
    }

    /**
     * @param $field
     *
     * @return integer|string
     */
    public function getFieldValue($field)
    {
        // TODO: Implement getFieldValue() method.
    }
}