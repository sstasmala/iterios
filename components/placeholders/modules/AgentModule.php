<?php
/**
 * AgentModule.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders\modules;

use app\components\placeholders\modules\base\PlaceholderModule;
use app\models\User;

class AgentModule implements PlaceholderModule
{
    private $model_instance;

    /**
     * AgentModule constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->model_instance = User::findOne($id);
    }

    /**
     * @return array
     */
    public static function getFields(): array
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'last_login' => 'Last Login',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getFieldValue($field)
    {
        // TODO: Implement getFieldValue() method.
    }
}