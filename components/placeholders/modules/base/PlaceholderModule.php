<?php
/**
 * PlaceholderModule.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders\modules\base;

interface PlaceholderModule
{
    /**
     * PlaceholderModule constructor.
     *
     * @param $id
     */
    public function __construct($id);

    /**
     * @return array
     */
    public static function getFields(): array;

    /**
     * @param $field
     *
     * @return integer|string
     */
    public function getFieldValue($field);
}