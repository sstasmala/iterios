<?php
/**
 * TouristModule.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders\modules;

use app\components\placeholders\modules\base\PlaceholderModule;
use app\models\Contacts;

class TouristModule implements PlaceholderModule
{
    private $model_instance;

    /**
     * TouristModule constructor.
     *
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->model_instance = Contacts::findOne($id);
    }

    /**
     * @return array
     */
    public static function getFields(): array
    {
        return [
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'discount' => 'Discount',
            'nationality' => 'Nationality',
            'tin' => 'Tin',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param $field
     *
     * @return int|string|void
     */
    public function getFieldValue($field)
    {
        // TODO: Implement getFieldValue() method.
    }
}