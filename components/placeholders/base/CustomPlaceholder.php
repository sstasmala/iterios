<?php
/**
 * CustomPlaceholder.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\placeholders\base;

use app\components\placeholders\modules\base\PlaceholderModule;

abstract class CustomPlaceholder
{
    /**
     * @return \app\components\placeholders\modules\base\PlaceholderModule
     */
    abstract public function getTourist(): PlaceholderModule;

    /**
     * @return \app\components\placeholders\modules\base\PlaceholderModule
     */
    abstract public function getAgent(): PlaceholderModule;

    /**
     * @return array
     */
    public function getTouristFields(): array
    {
        return $this->getTourist()->getFields();
    }

    public function getAgentFields(): array
    {
        return $this->getAgent()->getFields();
    }
}