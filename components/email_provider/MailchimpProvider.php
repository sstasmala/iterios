<?php
/**
 * MailchimpProvider.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\email_provider;


use app\components\email_provider\base\EmailProvider;
use app\components\email_provider\base\EmailProviderInterface;
use app\helpers\MCHelper;
use app\models\Agencies;
use app\models\Tenants;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class MailchimpProvider extends EmailProvider implements EmailProviderInterface
{
    private $token = null;
    private $url = 'https://<dc>.api.mailchimp.com/3.0';
    private $tenant = null;
    private $list_id = null;
    private $provider_data = [];

    private $from_name = null;
    private $from_email = null;

    /**
     * @param $id
     * @return bool|mixed
     */
    private function getList()
    {
        $id = $this->list_id;
        if($this->tenant == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('GET',$this->url.'/lists/'.$id,['auth'=>['Tenant-'.$this->tenant->id,$this->token]]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return json_decode($response,true);
    }

    /**
     * MailchimpProvider constructor.
     * @param array $config
     * @param \app\models\Tenants $tenant
     */
    public function __construct($config,$tenant,$provider_data) {
        if(isset($config['apiKey'])) {
            $this->token = $config['apiKey'];
            $parts = explode('-',$this->token);
            $dc = end($parts);
            $this->url = str_replace('<dc>',$dc,$this->url);
        }
        if(isset($config['fromName']))
            $this->from_name = $config['fromName'];
        if(isset($config['fromEmail']))
            $this->from_email = $config['fromEmail'];
        $this->tenant = $tenant;
        if($provider_data!=null)
            $this->provider_data = $provider_data;
        if(isset($this->provider_data['list_id']))
            $this->list_id = $this->provider_data['list_id'];
        $this->clearErrors();
    }

    /**
     * @return bool|mixed
     */
    private function createList(){
        /**
         * @var $tenant Tenants
         */
        $tenant = $this->tenant;
        if($tenant == null)
            return false;
        /**
         * @var $contacts Agencies
         */
        $contacts = Agencies::find()->where(['tenant_id' => $tenant->id])->one();
        if($contacts == null) {
            $this->addError('Tenant['.$tenant->id.'] contact info is not found!');
            return false;
        }

        $data = [];
        $data['name'] = 'List-'.$tenant->name;
        $data['contact'] = [
            'company'=>$tenant->name??'',
            'address1'=>$contacts->street.' '.$contacts->house,
            'city'=>$contacts->city??'',
            'state'=>'',
            'zip'=>'',
            'country'=>($tenant->country!=null)?MCHelper::getCountryById($tenant->country,$tenant->language->iso)[0]['title']:'',
        ];
        $data['permission_reminder'] = 'You are receiving this email because you opted in via our website.';
        $data['campaign_defaults'] = [
            'from_name'=>$this->from_name??'',
            'from_email'=>$this->from_email??'',
            'subject'=>'',
            'language'=>$tenant->language->iso??''
        ];
        $data['email_type_option'] = true;

        $client = new Client(['base_uri'=>$this->url]);
        $list = json_encode($data);
        try{
            $response = $client->request('POST',$this->url.'/lists',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$list
            ]);
        }  catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        }catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return json_decode($response,true);
    }


    /**
     * @return bool|array
     */
    private function getListMembers() {
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        $count = 100;
        $offset = 0;
        $finished = false;
        $members = [];
        try{
            while (!$finished) {
                $response = $client->request('GET',$this->url.'/lists/'.$this->list_id.'/members',[
                    'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                    'query'=>['count'=>$count,'offset'=>$offset]
                ]);
                $response = $response->getBody()->getContents();
                $data = \json_decode($response,true);
                $members = \array_merge($members,$data['members']);
                if(($count + $offset)<$data['total_items'])
                    $offset += $count;
                else
                    $finished = true;
            }
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        return $members;
    }

    /**
     * @param $email
     * @return bool
     */
    private function addMember($email) {
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        $key = \md5(\strtolower($email));
        $member = \json_encode([
            'email_address'=>$email,
            'status_if_new'=>'subscribed'
        ]);
        try{
            $response = $client->request('PUT',$this->url.'/lists/'.$this->list_id.'/members/'.$key,[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$member
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        return true;
    }

    private function addMembersToSegment($emails_add,$emails_remove,$segment){
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        $members = \json_encode([
            'members_to_add'=>$emails_add,
            'members_to_remove'=>$emails_remove
        ]);
        try{
            $response = $client->request('POST',$this->url.'/lists/'.$this->list_id.'/segments/'.$segment,[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$members
            ]);
        }  catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        }catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        return true;
    }

    private function getSegmentMembers($id) {
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        $count = 100;
        $offset = 0;
        $finished = false;
        $members = [];
        try{
            while (!$finished) {
                $response = $client->request('GET',$this->url.'/lists/'.$this->list_id.'/segments/'.$id.'/members',[
                    'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                    'query'=>['count'=>$count,'offset'=>$offset]
                ]);
                $response = $response->getBody()->getContents();
                $data = \json_decode($response,true);
                $members = \array_merge($members,$data['members']);
                if(($count + $offset)<$data['total_items'])
                    $offset += $count;
                else
                    $finished = true;
            }
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        } catch (\Exception $e) {
            $$this->addError($e->getMessage());
            return false;
        }
        return $members;
    }


    /**
     * @param $name
     * @return bool|mixed
     */
    private function createSegment($name){
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        $segment = \json_encode([
            'name'=>$name,
            'static_segment'=>[]
        ]);
        try{
            $response = $client->request('POST',$this->url.'/lists/'.$this->list_id.'/segments',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$segment
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return json_decode($response,true);
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    private function deleteSegment($id){
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('DELETE',$this->url.'/lists/'.$this->list_id.'/segments/'.$id,[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = \json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(\json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return \json_decode($response,true);
    }

    /**
     * @param $segments
     * @return array|bool
     */
    public function synchronizeSegments($segments) {
        $this->clearErrors();
        $list = null;
        if($this->list_id !== null){
            $list = $this->getList();
        }
        else
        {
            $list = $this->createList();
        }
        if($list===false && $this->list_id == null)
            return false;
        else
            if($list===false)
                $list = $this->createList();
        if($list===false)
            return false;
        $this->provider_data['list_id'] = $this->list_id = $list['id'];

        $members = $this->getListMembers();
        if($members === false)
            return false;
        $new_emails = \array_column($segments,'emails');
        $new_emails = \array_unique(\array_merge(...$new_emails));
        $old_emails = \array_column($members,'email_address');

        $diff = \array_diff($new_emails,$old_emails);
        foreach ($diff as $item)
            $this->addMember($item);

        $segments_info = [];
        if(isset($this->provider_data['segments']))
            $segments_info = $this->provider_data['segments'];

        $fined = [];
        foreach ($segments as $segment) {
            $resource_id = null;
            if(isset($segments_info[$segment['id']])){
                $resource_id = $segments_info[$segment['id']];
                $fined[] = $segment['id'];
            }


            if($resource_id == null)
            {
                if(empty($segment['emails']))
                    continue;
                $resource_id = $this->createSegment($segment['name']??'Segment #'.$segment['id'])['id'];
                if($resource_id)
                {
                    $segments_info[$segment['id']] = $resource_id;
                    $fined[] = $segment['id'];
                }
                else
                    continue;
            }

            $old_members = $this->getSegmentMembers($resource_id);
            if($old_members === false)
            {
                $resource_id = $this->createSegment('Segment #'.$segment['id'])['id'];
                if($resource_id) {
                    $segments_info[$segment['id']] = $resource_id;
                    $fined[] = $segment['id'];
                    $old_members = $this->getSegmentMembers($resource_id);
                    if($old_members === false)
                        continue;
                }
                else
                    continue;
            }
            $old_members = \array_column($old_members,'email_address');
            $new_members = $segment['emails'];
            $df = \array_diff($new_members,$old_members);
            $rdf = \array_diff($old_members,$new_members);
            $this->addMembersToSegment($df,$rdf,$resource_id);
        }
        foreach ($segments_info as $k => $v) {
            if(\array_search($k,$fined)=== false) {
                $this->deleteSegment($v);
                unset($segments_info[$k]);
            }
        }
        $this->provider_data['segments'] = $segments_info;
        return $this->provider_data;
    }

    /**
     * @param $name
     * @param $html
     * @return bool|mixed
     */
    private function createTemplate($name,$html) {
        $client = new Client(['base_uri'=>$this->url]);
        $template = \json_encode([
            'name'=>$name,
            'html'=>$html
        ]);
        try{
            $response = $client->request('POST',$this->url.'/templates',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$template
            ]);
        }catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return \json_decode($response,true);
    }

    /**
     * @param $segments
     * @param $template_id
     * @return bool|mixed
     */
    private function createCampaign($segments,$name, $subject, $template_id ) {
        if($this->list_id == null)
            return false;
        /**
         * @var $tenant Tenants
         */
        $tenant = $this->tenant;
        if($tenant == null)
            return false;
        $s_ids = \array_column($segments,'id');
        if(!isset($this->provider_data['segments']))
            return false;
        $sgs = array_filter($this->provider_data['segments'],function($item)use($s_ids) {
            return  (\array_search($item,$s_ids)!== false);
        },ARRAY_FILTER_USE_KEY);
        $conditions = [];
        foreach ($sgs as $id)
            $conditions[] = ['op'=>'static_is','field'=>'static_segment','value'=>$id];
        $client = new Client(['base_uri'=>$this->url]);
        $campaign = \json_encode([
            'type'=>'regular',
            'recipients'=>[
                'list_id'=>$this->list_id,
                'segment_opts' => [
                    'match'=>'any',
                    'conditions' => $conditions
                ],
            ],
            'settings'=>[
                'template_id'=>$template_id,
                'subject_line'=>$subject??'',
                'from_name'=>$this->from_name??'',
                'reply_to'=>$this->from_email??'',
                'title'=>$name??''
            ]
        ]);
        try{
            $response = $client->request('POST',$this->url.'/campaigns',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                'headers'=>["Content-Type"=>"application/json"],
                'body'=>$campaign
            ]);
        }  catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        }catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return \json_decode($response,true);

    }

    /**
     * @param $id
     * @return bool|mixed
     */
    private function sendCampaign($id) {
        if($this->list_id == null)
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('POST',$this->url.'/campaigns/'.$id.'/actions/send',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            return false;
        }
        catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return \json_decode($response,true);
    }

    private function checkCampaign($id)
    {
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('GET',$this->url.'/campaigns/'.$id.'/send-checklist',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        return \json_decode($response,true);
    }

    /**
     * @param $segments array
     * @param $html string
     * @return bool|mixed
     */
    public function sendSegments($segments,$name, $subject, $html) {
        $this->clearErrors();
        $template_id = $this->createTemplate($name,$html);
        if($template_id == false)
            return false;
        $template_id = $template_id['id'];
        $campaign = $this->createCampaign($segments,$name, $subject, $template_id);
        if($campaign === false)
            return false;
        $check = $this->checkCampaign($campaign['id']);
        if($check === false)
            return false;
        if(!$check['is_ready']) {
            foreach ($check['items'] as $item)
                if($item['type'] == 'error')
                    $this->addError($item['heading'].': '.$item['details']);
            return false;
        }

        $send = $this->sendCampaign($campaign['id']);
        if($send === false)
            return false;
        return $campaign;
    }

    private function getLocations($id) {
        $client = new Client(['base_uri'=>$this->url]);
        $count = 100;
        $offset = 0;
        $finished = false;
        $locations= [];
        try{
            while (!$finished) {
                $response = $client->request('GET',$this->url.'/reports/'.$id.'/locations',[
                    'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                    'query'=>['count'=>$count,'offset'=>$offset]
                ]);
                $response = $response->getBody()->getContents();
                $data = \json_decode($response,true);
                $locations= \array_merge($locations,$data['locations']);
                if(($count + $offset)<$data['total_items'])
                    $offset += $count;
                else
                    $finished = true;
            }

        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        return $locations;
    }

    private function getEmailsInfo($id) {
        $client = new Client(['base_uri'=>$this->url]);
        $count = 100;
        $offset = 0;
        $finished = false;
        $emails= [];
        try{
            while (!$finished) {
                $response = $client->request('GET',$this->url.'/reports/'.$id.'/sent-to',[
                    'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                    'query'=>['count'=>$count,'offset'=>$offset]
                ]);
                $response = $response->getBody()->getContents();
                $data = \json_decode($response,true);
                $emails= \array_merge($emails,$data['sent_to']);
                if(($count + $offset)<$data['total_items'])
                    $offset += $count;
                else
                    $finished = true;
            }

        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        return $emails;
    }

    private function getLinks($id) {
        $client = new Client(['base_uri'=>$this->url]);
        $count = 100;
        $offset = 0;
        $finished = false;
        $urls_clicked= [];
        try{
            while (!$finished) {
                $response = $client->request('GET',$this->url.'/reports/'.$id.'/click-details',[
                    'auth'=>['Tenant-'.$this->tenant->id,$this->token],
                    'query'=>['count'=>$count,'offset'=>$offset]
                ]);
                $response = $response->getBody()->getContents();
                $data = \json_decode($response,true);
                $urls_clicked= \array_merge($urls_clicked,$data['urls_clicked']);
                if(($count + $offset)<$data['total_items'])
                    $offset += $count;
                else
                    $finished = true;
            }

        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }

        return $urls_clicked;
    }

    /**
     * @param $info
     * @param $data
     * @return bool|mixed|null|string
     */
    public function getStatistic($info,$data)
    {
        if(!isset($info['id']))
            return false;
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('GET',$this->url.'/reports/'.$info['id'],[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }

        $response = $response->getBody()->getContents();
        $response = \json_decode($response,true);
        $data['total'] = $response['emails_sent'];
        $data['opened_total'] = $response['opens']['opens_total'];
        $data['clicked_total'] = $response['clicks']['clicks_total'];
        $data['unique_opens'] = $response['opens']['unique_opens'];
        $data['unique_clicks'] = $response['clicks']['unique_clicks'];
        $data['not_delivered'] = \array_sum($response['bounces']);
        $data['unsubscribed'] = $response['unsubscribed'];
        $data['last_open'] = !empty($response['opens']['last_open'])?(new \DateTime($response['opens']['last_open']))->getTimestamp():null;
        $data['last_click'] = !empty($response['clicks']['last_click'])?(new \DateTime($response['clicks']['last_click']))->getTimestamp():null;
        $data['open_rate'] = $response['opens']['open_rate']*100;
        $data['click_rate'] = $response['clicks']['click_rate']*100;
        $data['history'] = [];
        foreach ($response['timeseries'] as $el) {
            $time = (new \DateTime($el['timestamp']))->getTimestamp();
            $data['history'][] = [
                'time' => $time,
                'clicks' => $el['recipients_clicks'],
                'opens' => $el['unique_opens']
            ];
        }
        $data['locations'] = [];
        $locations = $this->getLocations($info['id']);
        if($locations !== false) {
            foreach ($locations as $lc) {
                $data['locations'][] = [
                    'country' => $lc['country_code'],
                    'value' => $lc['opens']
                ];
            }
        }

        $data['emails'] = [];
        $emails = $this->getEmailsInfo($info['id']);
        if($emails !== false) {
            foreach ($emails as $email) {
                $data['emails'][] = [
                    'email' => $email['email_address'],
                    'opens' => $email['open_count'],
                    'status' => ($email['status']=='sent')?'send':'bounce'
                ];
            }
        }

        $data['links'] = [];
        $links = $this->getLinks($info['id']);
        if($links !== false) {
            foreach ($links as $link) {
                $data['links'][] = [
                    'url' => $link['url'],
                    'clicks' => $link['total_clicks']
                ];
            }
        }
        return $data;
    }


    public function getProviderInfo()
    {
        $client = new Client(['base_uri'=>$this->url]);
        try{
            $response = $client->request('GET',$this->url.'/',[
                'auth'=>['Tenant-'.$this->tenant->id,$this->token],
            ]);
        } catch (ClientException $e) {
            $data =$e->getResponse()->getBody()->getContents();
            $data = json_decode($data,true);
            if(isset($data['detail']))
                $this->addError($data['detail']);
            else
                $this->addError(json_encode($data));
            if(isset($data['errors']) && is_array($data['errors']))
                foreach ($data['errors'] as $error)
                    $this->addError($error);
            return false;
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return false;
        }
        $response = $response->getBody()->getContents();
        $response = \json_decode($response,true);
        $result = [];
        $result['total_sub'] = $response['total_subscribers'] ?? 0;
        return $result;
    }
}