<?php

/**
 * EmailProviderInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\email_provider\base;

use app\models\Tenants;

interface EmailProviderInterface
{
    /**
     * EmailProviderInterface constructor.
     * @param $config array
     * @param $tenant Tenants
     */
    public function __construct($config,$tenant,$provider_data);

    /**
     * @param $segments array
     * @return mixed
     */
    public function synchronizeSegments($segments);

    /**
     * @param $segments array
     * @param $html string
     * @return bool|mixed
     */
    public function sendSegments($segments,$name,$subject,$emails);

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @param $info
     * @param $data
     * @return bool|array
     */
    public function getStatistic($info,$data);

    /**
     * @return bool|array
     */
    public function getProviderInfo();
}