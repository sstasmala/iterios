<?php
/**
 * EmailProvider.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\email_provider\base;


class EmailProvider
{
    private $errors = [];

    /**
     * @return array
     */
    public function getErrors(){
        return $this->errors;
    }

    protected function addError($error){
        $this->errors[] = $error;
    }

    protected function clearErrors()
    {
        $this->errors = [];
    }
}