<?php
/**
 * BaseSmsInterface.php
 * @copyright ©ita.local
 * @author Denis Kuchugurniy denis290593@gmail.com
 */

namespace app\components\sms_provider\base;


interface BaseSmsInterface
{
//set body message
    public function setBody($message);

//parse responce from provider
    public function getParseResponse();

//save log to db
    public function saveResultToDb($phones, $send_sms_id);

}