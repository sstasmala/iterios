<?php

namespace app\components\sms_provider\base;

use GuzzleHttp\Client;

class BaseSmsProvider
{
    protected $_login;
    protected $_password;
    protected $_url;
    protected $_apiToken;

    protected $_body;
    protected $_alphaName;
    protected $_abonentList = [];

    public $responce;



    public function setLoginPassword($providerParams)
    {
        $this->_login = $providerParams['username'];
        $this->_password = $providerParams['password'];
    }

    public function setToken($providerParams)
    {
        $this->_apiToken = $providerParams['apiKey'];
    }

    public function setUrl($url)
    {
        $this->_url = $url;
    }

    public function setAlphaName($providerParams)
    {
        $this->_alphaName = $providerParams['alphaName'];
    }

    public function setAbonentList(array $abonentList)
    {
        $this->_abonentList = $abonentList;
    }


    public function send()
    {
        $client = new Client([
            'base_uri' => $this->_url,
        ]);

        $response = $client->request('POST', $this->_url, [
            'headers' => ['Content-type: text/xml; charset=utf-8'],
            'body' => $this->_body,
        ]);

        $this->responce = (string) $response->getBody();

    }


}