<?php

namespace app\components\sms_provider;

use app\helpers\PrintHelper;
use app\models\ProvidersCredentials;
use app\models\SendSms;
use app\models\SendSmsStatus;
use app\components\sms_provider\base\BaseSmsProvider;
use app\components\sms_provider\base\BaseSmsInterface;
use yii\web\HttpException;

class SmsClubXML extends BaseSmsProvider implements BaseSmsInterface
{
    protected $_url = 'https://gate.smsclub.mobi/xml/';
//    protected $_alphaName = 'ITERIOS';

    public function __construct($provider_credentials_id)
    {
//        $this->setUrl($url);
        $providerCredentials = ProvidersCredentials::find()->with(['provider'])->where(['id' => $provider_credentials_id])->one();


        $providerParams = json_decode($providerCredentials->params, 1);

        if(isset($providerParams['alphaName'])){
            $this->setAlphaName($providerParams);
        }else{
            throw new HttpException(403,'The alphaName not set');
        }

        switch ($providerCredentials->provider->authorization_type) {
            case 'login&pass':
                $this->setLoginPassword($providerParams);
                break;
            case 'api-token':
                $this->setToken($providerParams);
                break;
            default: throw new HttpException(403,'Autorization type not set');
        }

    }

    public function setBody($message)
    {
//        $text = iconv('utf-8', 'windows-1251', $message);
//        $text = urlencode($text);       // string Message

        $this->_body = "<?xml version='1.0' encoding='utf-8'?><request_sendsms><username><![CDATA[" .
            $this->_login .
            "]]></username><password><![CDATA[" .
            $this->_password .
            "]]></password><from><![CDATA[" .
            $this->_alphaName .
            "]]></from><to><![CDATA[" .
            implode($this->_abonentList, ';') .
            "]]></to><text><![CDATA[" .
            $message .
            "]]></text></request_sendsms>";

    }

    public function getParseResponse()
    {
        $result = [];
        $xmlBody = simplexml_load_string($this->responce);
        if(isset($xmlBody->ids->mess)) {
            foreach ($xmlBody->ids->mess as $key => $value) {
                $result[] = array(
                    'number' => (string)$value['tel'],
                    'sms_id' => (string)$value
                );
            }
        }
        return $result;
    }

    public function saveResultToDb($phones, $send_sms_id)
    {
        $parseResponse = $this->getParseResponse();
        $countDelivered = 0;
        PrintHelper::printMessage(count($phones));
        foreach ($phones as $phone) {
            $saveSms = new SendSmsStatus();             //если в ответе есть номер то значит статус ок
            $saveSms->status = SendSmsStatus::DELIVERY_STATUS['error'];

            foreach ($parseResponse as $val) {
                if (preg_replace("/[^0-9]/", '', $phone['phone']) == preg_replace("/[^0-9]/", '', $val['number'])) {
                    $saveSms->provider_sms_id = $val['sms_id'];
                    $saveSms->status = SendSmsStatus::DELIVERY_STATUS['send'];
                    $countDelivered++;
                    break;
                }
            }

            $saveSms->contact_phone_id = $phone['contact_phone_id'];
            $saveSms->send_sms_id = $send_sms_id;
            $saveSms->segments = $phone['segments'];
            $saveSms->time_send = time();
            $saveSms->save();
        }

        $sendSmsModel = SendSms::findOne($send_sms_id);
        $sendSmsModel->status = SendSms::STATUS_SENDED;
        $sendSmsModel->count_phones = count($phones);
        $sendSmsModel->count_delivered = $countDelivered;
        $sendSmsModel->count_not_delivered = count($phones) - $countDelivered;
        $sendSmsModel->save();
    }

}




















