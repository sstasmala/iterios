<?php
/**
 * ConditionConstructor.php
 * @copyright ©iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\constructor;



use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class ConditionConstructor
{
    private $_operators;
    private $_tables;
    private $_conditions;
    private $_timezone;
    private $_params = [];
    private $_queries = [];
    private $_additionals;
    private $_previous = null;

    /**
     * ConditionConstructor constructor.
     * @param $data
     * @param null $timezone
     */
    public function __construct($data,$timezone = null)
    {
        $this->_operators = [
            'equal' =>            '= ?',
            'not_equal' =>        '<> ?',
            'in' =>               ['op' => 'IN (?)',     'list' => true, 'sep' => ', ' ],
            'not_in' =>           ['op' => 'NOT IN (?)', 'list' => true, 'sep' => ', '],
            'less' =>             '< ?',
            'less_or_equal' =>    '<= ?',
            'greater' =>          '> ?',
            'greater_or_equal' => '>= ?',
            'between' =>          ['op' => 'BETWEEN ?',   'list' => true, 'sep' => ' AND '],
            'not_between' =>      ['op' => 'NOT BETWEEN ?',   'list' => true, 'sep' => ' AND '],
            'begins_with' =>      ['op' => 'LIKE ?',     'fn' => function($value){ return "$value%"; } ],
            'not_begins_with' =>  ['op' => 'NOT LIKE ?', 'fn' => function($value){ return "$value%"; } ],
            'contains' =>         ['op' => 'LIKE ?',     'fn' => function($value){ return "%$value%"; } ],
            'not_contains' =>     ['op' => 'NOT LIKE ?', 'fn' => function($value){ return "%$value%"; } ],
            'ends_with' =>        ['op' => 'LIKE ?',     'fn' => function($value){ return "%$value"; } ],
            'not_ends_with' =>    ['op' => 'NOT LIKE ?', 'fn' => function($value){ return "%$value"; } ],
            'is_empty' =>         '= ""',
            'is_not_empty' =>     '<> ""',
            'is_null' =>          'IS NULL',
            'is_not_null' =>      'IS NOT NULL'
        ];
        $this->_tables = [
            //'contacts' => 'app\models\Contacts',
            //'contacts_tags' => 'app\models\ContactsTags',
            //'tasks' => 'app\models\Tasks',
            //'requests_statuses' => 'app\models\RequestStatuses',
        ];
        if(is_null($timezone))
            $timezone = date_default_timezone_get();
        $this->_timezone = $timezone;
        $this->_conditions = $this->buildConditions($data);
    }

    /**
     * @param $field
     * @param $type
     * @param $params
     * @return array
     */
    protected function encodeRule($field, $type, $params)
    {

        $pattern = $this->_operators[$type];
        $keys = array_keys($params);


        if (is_string($pattern)) {
            $replacement = !empty($keys) ? $keys[0] : null;
        } else {
            $op = ArrayHelper::getValue($pattern, 'op');
            $list = ArrayHelper::getValue($pattern, 'list');
            if ($list){
                $sep = ArrayHelper::getValue($pattern, 'sep');
                $replacement = implode($sep, $keys);
            } else {
                $fn = ArrayHelper::getValue($pattern, 'fn');
                $replacement = key($params);
                $params[$replacement] = call_user_func($fn, $params[$replacement]);
            }
            $pattern = $op;
        }
        return [
            'where' => $field . " " . ($replacement ? str_replace("?", $replacement, $pattern) : $pattern),
            'params' => $params,
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function buildConditions($data)
    {
        if (!isset($data['rules']) || !$data['rules']) {
            return [];
        }

        $condition = ['condition'=>$data['condition']];

        foreach ($data['rules'] as $rule)
        {
            if (isset($rule['condition'])) {
                $condition['rules'][] = $this->buildConditions($rule);
            } else {
                $params = [];
                $operator = $rule['operator'];
                $field = $rule['field'];
                $unrepeat = [];
                $value = ArrayHelper::getValue($rule, 'value');

                $partials = explode('.',$field);
                if(count($partials)<2 || !isset($this->_tables[$partials[0]]))
                {
                    $table = null;
                    $model = null;
                }
                else{
                    $model = $this->_tables[$partials[0]];
                    $table = $partials[0];
                    $field = $partials[1];

                }

                if($partials[0] == 'contacts')
                    if($field == 'date_of_birth')
                    {
                        $operator = 'equal';
                        $field = "date_part('day',to_timestamp(date_of_birth) AT TIME ZONE '".$this->_timezone
                            ."')||'.'||date_part('month',to_timestamp(date_of_birth) AT TIME ZONE '"
                            .$this->_timezone."')";
                        $date = new \DateTime();
                        $date->setTimestamp(time());
                        $date->setTimezone(new \DateTimeZone($this->_timezone));
                        $date->setTime(0,0,0);
                        $now = $date->format('j.n');
                        $value = [$now];
                        $date->add(new \DateInterval('P1D'));
                        $unrepeat[] = ['field'=>'date_of_birth','type'=>'until','value'=>$date->getTimestamp()];
                    }
                    if($field == 'created_at')
                    {
                        $date = new \DateTime();
                        $date->setTimestamp(time());
                        $date->setTimezone(new \DateTimeZone($this->_timezone));
                        if($operator == 'equal')
                        {
                            $operator = 'between';
                            $date->setTime(0,0,0);
                            $form = $date->getTimestamp();
                            $date->setTime(23,59,59);
                            $to = $date->getTimestamp();
                            $value = [$form,$to];
                        }
                        if($operator == 'not_equal')
                        {
                            $operator = 'not_between';
                            $date->setTime(0,0,0);
                            $form = $date->getTimestamp();
                            $date->setTime(23,59,59);
                            $to = $date->getTimestamp();
                            $value = [$form,$to];
                        }
                        if($operator == 'between' || $operator == 'not_between')
                        {
                            $date->setTimestamp($value[0]);
                            $date->setTime(0,0,0);
                            $form = $date->getTimestamp();
                            $date->setTimestamp($value[1]);
                            $date->setTime(23,59,59);
                            $to = $date->getTimestamp();
                            $value = [$form,$to];
                        }
                    }
                if($partials[0] == 'tasks')
                    if($field == 'due_date')
                    {
                        if($operator = 'equal')
                            $operator = 'less';
                        else
                            $operator = 'greater';
                        $date = new \DateTime();
                        $date->setTimestamp(time());
                        $date->setTimezone(new \DateTimeZone($this->_timezone));
                        $now = $date->getTimestamp();
                        $value = [$now];
                    }

                if ($value !== null) {
                    $i = count($this->_params);

                    if (!is_array($value)) {
                        $value = [$value];
                    }

                    foreach ($value as $v) {
                        $params[":p$i"] = $v;
                        $i++;
                    }
                }
                $this->_params = array_merge($this->_params, $params);
                $r = $this->encodeRule($field, $operator, $params);
                $condition['rules'][] = [
                    'where'=> $r['where'],
                    'params'=> $r['params'],
                    'table'=>$table,
                    'model'=>$model,
                    'unrepeat'=>$unrepeat
                ];
            }
        }

        return $condition;
    }

    private function buildQueries($data)
    {
        if (!isset($data['rules']) || !$data['rules']) {
            return [];
        }
        $condition = ['condition'=>$data['condition']];
        $queries = [];
        foreach ($data['rules'] as $rule)
        {
            if (isset($rule['condition'])) {
                $condition['queries'][] = $this->buildQueries($rule);
            } else {
                if(!is_null($rule['table']))
                {
                    $model = $this->_tables[$rule['table']];
                    $q = $model::find();
                    $q->where($rule['where']);
                    $q->addParams($rule['params']);
                    $condition['queries'][] = ['query'=>$q,'table'=>$rule['table'],'unrepeat'=>$rule['unrepeat']];
                }
            }
        }

//        foreach ($queries as $table => $query)
//        {
//            $model = $this->_tables[$table];
//            /**
//             * @var $q ActiveQuery
//             */
//            $q = $model::find();
//            $params = [];
//            $unrepeat = [];
//            $items = array_column($query,'where');
//            $where = "(" .implode(' '.$condition['condition'].' ',$items). ")";
//            $q->andWhere($where);
//            foreach ($query as $el)
//            {
//                $params = array_merge($params,$el['params']);
//                $unrepeat = array_merge($unrepeat,$el['unrepeat']);
//            }
//            $q->addParams($params);
//            $condition['queries'][] = ['query'=>$q,'table'=>$table,'unrepeat'=>$unrepeat];
//        }
        return $condition;
    }

    /**
     * @return array
     */
    public function getConditions(){
        return $this->_conditions;
    }

    public function makeQueries()
    {
        $this->_queries = $this->buildQueries($this->_conditions);
        return $this->_queries;
    }

    private function executeQuery($data)
    {
        if (!isset($data['queries']) || !$data['queries']) {
            return [];
        }
        $condition = ['condition'=>$data['condition']];
        foreach ($data['queries'] as $query)
        {
            if (isset($query['condition'])) {
                $condition['results'][] = $this->buildQueries($query);
            } else {
                if(!isset($query['query'])
                    || is_null($query['query'])
                    || !isset($query['table'])
                    || is_null($query['table']) )
                {
                    $condition['queries'][] = array_merge($query,['result'=>[]]);
                    continue;
                }

                $q = $query['query'];
                if(isset($this->_additionals[$query['table']]))
                    foreach ($this->_additionals[$query['table']] as $a)
                        $q->andWhere($a);
                $result = $q->asArray()->all();
                $condition['results'][] =  array_merge($query,['result'=>$result]);
            }
        }
        return $condition;
    }

    private function makeResults($data)
    {
        if (!isset($data['results']) || !$data['results']) {
            return [];
        }
        $condition = $data['condition'];
        $subresults = [];
        $results = [];
        foreach ($data['results'] as $result)
        {
            if (isset($result['condition'])) {
                $subresults[] = $this->buildQueries($result);
            } else {
                if(!isset($result['result'])
                    || is_null($result['result'])
                    || !isset($result['table'])
                    || is_null($result['table']) )
                    continue;
               if(!empty($result['result']))
               {
                   if(!empty($result['unrepeat']) && !is_null($this->_previous))
                   {
                       $unrepeat = $result['unrepeat'];
                       if($unrepeat['type'] == 'until' && $unrepeat['value']>$this->_previous)
                           continue;
                   }
                   $results[$result['table']] = $result['result'];
               }

               else
                   if($condition == 'AND')
                       return [];
            }
        }
        if(empty($subresults) && $condition == 'AND')
            return[];
        foreach ($subresults as $k => $v)
        {
            if(isset($results[$k]))
                $results[$k] = array_merge($results[$k],$v);
            else
                $results[$k] = $v;
        }
        return $results;
    }

    public function executeQueries($previous, $additionals = [])
    {
        $this->_additionals = $additionals;
        $this->_previous = $previous;
        return $this->makeResults($this->executeQuery($this->_queries));
    }

}