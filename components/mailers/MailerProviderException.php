<?php
/**
 * MailerProviderException.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\mailers;

class MailerProviderException extends \Exception
{

}