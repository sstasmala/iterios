<?php
/**
 * Message.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\mailers\sparkpost;

use app\components\mailers\BaseMessageInterface;
use yii\mail\MailerInterface;

class Message implements BaseMessageInterface
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $charset;

    /**
     * @var array ['name' => (string), 'email' => (string)]
     */
    protected $from;

    /**
     * @var array [ ['name' => (string), 'email' => (string)], ... ]
     */
    protected $to;

    /**
     * @var array [ ['name' => (string), 'email' => (string)], ... ]
     */
    protected $reply_to;

    /**
     * @var array [ ['name' => (string), 'email' => (string)], ... ]
     */
    protected $cc;

    /**
     * @var array [ ['name' => (string), 'email' => (string)], ... ]
     */
    protected $bcc;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $text_body;

    /**
     * @var string
     */
    protected $html_body;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * Message constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->setMailer($mailer);
    }

    /**
     * Returns the character set of this message.
     * @return string the character set of this message.
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * Sets the character set of this message.
     * @param string $charset character set name.
     * @return $this self reference.
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * Returns the message sender.
     * @return string the sender
     */
    public function getFrom()
    {
        if (isset($this->from['email'])) {
            return $this->from['email'];
        }
        return '';
    }

    /**
     * Sets the message sender.
     * @param string|array $from sender email address.
     * You may pass an array of addresses if this message is from multiple people.
     * You may also specify sender name in addition to email address using format:
     * `[email => name]`.
     * @return $this self reference.
     */
    public function setFrom($from)
    {
        $emails = $this->buildEmailArray($from);
        if (!empty($emails)) {
            $this->from = $emails[0];
        }
        return $this;
    }

    /**
     * Returns the message recipient(s).
     * @return array the message recipients
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Sets the message recipient(s).
     * @param string|array $to receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return $this self reference.
     */
    public function setTo($to)
    {
        $emails = $this->buildEmailArray($to);
        if (!empty($emails)) {
            $this->to = $emails;
        }
        return $this;
    }

    /**
     * Returns the reply-to address of this message.
     * @return string the reply-to address of this message.
     */
    public function getReplyTo()
    {
        if (is_null($this->reply_to)) {
            return null;
        }
        return $this->contactsToString($this->reply_to);
    }

    /**
     * Sets the reply-to address of this message.
     * @param string|array $replyTo the reply-to address.
     * You may pass an array of addresses if this message should be replied to multiple people.
     * You may also specify reply-to name in addition to email address using format:
     * `[email => name]`.
     * @return $this self reference.
     */
    public function setReplyTo($replyTo)
    {
        $emails = $this->buildEmailArray($replyTo);
        if (!empty($emails)) {
            $this->reply_to = $emails;
        }
        return $this;
    }

    /**
     * Returns the Cc (additional copy receiver) addresses of this message.
     * @return array the Cc (additional copy receiver) addresses of this message.
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Sets the Cc (additional copy receiver) addresses of this message.
     * @param string|array $cc copy receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return $this self reference.
     */
    public function setCc($cc)
    {
        $emails = $this->buildEmailArray($cc);
        if (!empty($emails)) {
            $this->cc = $emails;
        }
        return $this;
    }

    /**
     * Returns the Bcc (hidden copy receiver) addresses of this message.
     * @return array the Bcc (hidden copy receiver) addresses of this message.
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Sets the Bcc (hidden copy receiver) addresses of this message.
     * @param string|array $bcc hidden copy receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return $this self reference.
     */
    public function setBcc($bcc)
    {
        $emails = $this->buildEmailArray($bcc);
        if (!empty($emails)) {
            $this->bcc = $emails;
        }
        return $this;
    }

    /**
     * Returns the message subject.
     * @return string the message subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the message subject.
     * @param string $subject message subject
     * @return $this self reference.
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Sets message plain text content.
     * @param string $text message plain text content.
     * @return $this self reference.
     */
    public function setTextBody($text)
    {
        $this->text_body = $text;
        return $this;
    }

    /**
     * Sets message HTML content.
     * @param string $html message HTML content.
     * @return $this self reference.
     */
    public function setHtmlBody($html)
    {
        $this->html_body = $html;
        return $this;
    }

    /**
     * Attaches existing file to the email message.
     * @param string $fileName full file name
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     * @return $this self reference.
     * @throws \Exception
     */
    public function attach($fileName, array $options = [])
    {
        throw new \Exception('Method ' . __METHOD__ . ' is not implemented yet.');
    }

    /**
     * Attach specified content as file for the email message.
     * @param string $content attachment file content.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     * @return $this self reference.
     * @throws \Exception
     */
    public function attachContent($content, array $options = [])
    {
        throw new \Exception('Method ' . __METHOD__ . ' is not implemented yet.');
    }

    /**
     * Attach a file and return it's CID source.
     * This method should be used when embedding images or other data in a message.
     * @param string $fileName file name.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     * @return string attachment CID.
     * @throws \Exception
     */
    public function embed($fileName, array $options = [])
    {
        throw new \Exception('Method ' . __METHOD__ . ' is not implemented yet.');
    }

    /**
     * Attach a content as file and return it's CID source.
     * This method should be used when embedding images or other data in a message.
     * @param string $content attachment file content.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     * @return string attachment CID.
     * @throws \Exception
     */
    public function embedContent($content, array $options = [])
    {
        throw new \Exception('Method ' . __METHOD__ . ' is not implemented yet.');
    }

    /**
     * Sends this email message.
     * @param MailerInterface $mailer the mailer that should be used to send this message.
     * If null, the "mail" application component will be used instead.
     * @return bool whether this message is sent successfully.
     * @throws \Exception
     */
    public function send(MailerInterface $mailer = null)
    {
        if (!is_null($mailer)) {
            if (!($mailer instanceof Mailer)) {
                throw new \Exception("Parameter 'mailer' has to be instance of '" . Mailer::class . "'. Instance of '" . get_class($mailer) . "' given.");
            }
            return $mailer->send($this);
        }
        if (is_null($this->getMailer())) {
            throw new \Exception("Mailer was not set");
        }
        return $this->getMailer()->send($this);
    }

    /**
     * Returns string representation of this message.
     * @return string the string representation of this message.
     */
    public function toString()
    {
        $parts = [];
        if (!is_null($this->getTo())) {
            $parts[] = "To: " . $this->contactsToString($this->to);
        }
        if (!empty($this->getFrom())) {
            $parts[] = "From: {$this->getFrom()}";
        }
        if (!empty($this->getSubject())) {
            $parts[] = "Subject: {$this->getSubject()}";
        }
        return implode('. ', $parts);
    }

    /**
     * Sets metadata for sending message
     * @param array $metadata
     * @return BaseMessageInterface
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = [];
        foreach ($metadata as $key => $value) {
            $this->metadata[$key] = $this->getMetadataValue($value);
        }
        return $this;
    }

    /**
     * Returns array of metadata for sending message
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param Mailer $mailer
     */
    public function setMailer(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function toSparkpostArray()
    {
        if (empty($this->from)) {
            throw new \Exception('Sender was not set');
        }
        if (empty($this->to)) {
            throw new \Exception('Receivers were not set');
        }
        if (empty($this->subject)) {
            throw new \Exception('Subject was not set');
        }
        if (empty($this->text_body) && empty($this->html_body)) {
            throw new \Exception('One of text or html of message has to be set');
        }
        if (!isset($this->getMetadata()['email_log_id'])) {
            throw new \Exception("Metadata has to contain 'email_log_id'");
        }
        if (!isset($this->getMetadata()['email_provider_id'])) {
            throw new \Exception("Metadata has to contain 'email_provider_id'");
        }

        $contact_callback = function (array $contact) {
            return [
                'address' => $contact,
            ];
        };

        $data = [
            'content' => [
                'from' => $this->from,
                'subject' => $this->subject,
                'html' => $this->html_body,
                'text' => $this->text_body,
                'reply_to' => $this->getReplyTo(),
            ],
            'recipients' => array_map($contact_callback, $this->to),
            'cc' => empty($this->cc) ? null : array_map($contact_callback, $this->cc),
            'bcc' => empty($this->bcc) ? null : array_map($contact_callback, $this->bcc),
            'metadata' => $this->getMetadata(),
        ];
        return $data;
    }

    /**
     * @param array|string $emails
     * @return array
     */
    protected function buildEmailArray($emails)
    {
        $result = [];
        if (is_string($emails)) {
            $result[] = [
                'email' => $emails,
                'name' => null,
            ];
        } elseif (is_array($emails)) {
            foreach ($emails as $key => $item) {
                if (is_string($key)) {
                    $result[] = [
                        'email' => $key,
                        'name' => $item,
                    ];
                } else {
                    $result[] = [
                        'email' => $item,
                        'name' => null,
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * @param array $contacts
     * @return string
     */
    protected function contactsToString(array $contacts)
    {
        if (empty($contacts)) {
            return '';
        }
        $result = [];
        foreach ($contacts as $item) {
            $result[] = "{$item['name']} <{$item['email']}>";
        }
        return implode(' ,', $result);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    protected function getMetadataValue($value)
    {
        if (is_object($value)) {
            if (method_exists($value, 'getId')) {
                return $value->getId();
            } elseif (property_exists($value, 'id')) {
                return $value->id;
            }
        } elseif (is_array($value)) {
            $res = [];
            foreach ($value as $key => $item) {
                $res[$key] = $this->getMetadataValue($item);
            }
            return $res;
        } else {
            return $value;
        }
    }
}