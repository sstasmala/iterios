<?php
/**
 * IncomingMessage.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\components\mailers\imap;


use SSilence\ImapClient\SubtypeBody;

class IncomingMessage extends \SSilence\ImapClient\IncomingMessage
{
    protected function getBody()
    {
        $objNew = new \stdClass();
        $i = 1;
        $subType = new SubtypeBody();
        foreach ($this->getSections(self::SECTION_BODY) as $section) {
            $obj = $this->getSection($section, array('class' => $subType));
            if(!$obj->__get('structure'))
                continue;
            $subtype = strtolower($obj->__get('structure')->subtype);
            if (!isset($objNew->$subtype)) {
                $objNew->$subtype = $obj;
            } else {
                $subtype = $subtype.'_'.$i;
                $objNew->$subtype = $obj;
                $i++;
            }
            $objNew->info[] = $obj;
            $objNew->types[] = $subtype;
            /*
             * Set charset
             */
            foreach ($objNew->$subtype->__get('structure')->parameters as $parameter) {
                $attribute = strtolower($parameter->attribute);
                if ($attribute === 'charset') {
                    $value = strtolower($parameter->value);
                    /*
                     * Here must be array, but
                     */
                    //$objNew->$subtype->charset[] = $value;
                    $objNew->$subtype->charset = $value;
                }
            }
        }
        if (isset($objNew->plain)) {
            $objNew->text = $objNew->plain;
            $objNew->types[] = 'text';
        } else {
            $objNew->text = null;
        }
        $this->message = $objNew;
    }
}