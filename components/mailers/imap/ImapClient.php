<?php
namespace app\components\mailers\imap;
use SSilence\ImapClient\ImapClientException;
use SSilence\ImapClient\IncomingMessage;

/**
 * ImapClient.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
class ImapClient extends \SSilence\ImapClient\ImapClient
{
    public $imap;
    public $mailbox = "";
    /**
     * Check message id
     *
     * @param integer $id
     * @return void
     * @throws ImapClientException
     */
    private function checkMessageId($id)
    {
        if(!is_int($id)){
            throw new ImapClientException('$id must be an integer!');
        };
        if($id <= 0){
            throw new ImapClientException('$id must be greater then 0!');
        };
        if($id > $this->countMessages()){
            throw new ImapClientException('$id does not exist');
        }
    }

    public function getMessage($id, $decode = IncomingMessage::DECODE)
    {
        $this->checkMessageId($id);
        $this->incomingMessage = new \app\components\mailers\imap\IncomingMessage($this->imap, $id, $decode);
        return $this->incomingMessage;
    }


    public function getFoldersDecode()
    {
        $folders = $folders = $this->getFolders('.',1);
        $list = [];
        foreach ($folders as $folder)
        {
            $p = explode('/',$folder);
            $item = end($p);
            if($folder == "INBOX" || imap_utf7_decode($folder)==$folder)
                $list[] = ['name'=>$folder,'id'=>$folder];
            else{
                $names = [];
                $parts = explode('-',$item);
                foreach ($parts as $part){
                    $part = str_replace(',','/',$part);
                    $names[] = mb_convert_encoding(base64_decode($part),"UTF-8","UTF-16BE");
                }
                $list[] = ['name' =>implode(' ',$names),'id'=>$folder];
            }
        }
        return $list;
    }

    public function setReaded($id)
    {
        return imap_setflag_full($this->imap,$id,"\\Seen");
    }

    public function saveMessageInSentA($data) {
        return imap_append($this->imap, $this->mailbox . $this->getSent(),$data . "\r\n", "\\Seen");
    }

    public function getSent()
    {
        $prefix = [];
        foreach ($this->getFolders(null, 1) as $folder) {
//            if(imap_utf7_decode($folder)!=$folder)
//            {
//                $prefix = explode('/',$folder);
//                unset($prefix[count($prefix)-1]);
//            }
            if (in_array(strtolower($folder), array('sent', 'gesendet', 'inbox.gesendet'))) {
                return $folder;
            }
        }

        // no sent folder found? create one

//        $this->addFolder(implode('/',$prefix).'/'.'Sent');
        return 'Sent';
    }
}