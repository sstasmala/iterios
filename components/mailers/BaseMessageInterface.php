<?php
/**
 * BaseMessageInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\mailers;

use yii\mail\MessageInterface;

interface BaseMessageInterface extends MessageInterface
{
    /**
     * Sets metadata for sending message
     * @param array $metadata
     * @return BaseMessageInterface
     */
    public function setMetadata(array $metadata);

    /**
     * Returns array of metadata for sending message
     * @return array
     */
    public function getMetadata();
}