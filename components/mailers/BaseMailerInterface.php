<?php
/**
 * BaseMailerInterface.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
namespace app\components\mailers;

use yii\mail\MailerInterface;

interface BaseMailerInterface extends MailerInterface
{

}