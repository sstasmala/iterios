<?php
/**
 * ConditionConstructor.php
 * @copyright ©iterios
 * @author Stas Osmolovskiy rabotasmala@gmail.com
 */

namespace app\components\notifications_now;

use app\models\NotificationsnowLog;
use app\models\Requests;
use app\models\RequestStatuses;
use app\models\NotificationsNow;
use app\models\ProfileNotifications;
use app\models\Contacts;
use app\models\Tags;
use app\models\SystemNotifications;
use app\helpers\TwigHelper;
use app\models\Tenants;
use app\components\notifications\contacts\ReceiversFactory;
use app\components\notifications\contacts\SenderFactory;
use app\components\notifications\NotificationFactory;
use app\models\Jobs;
use app\models\Settings;
use app\models\EmailProviders;
use app\models\Tasks;
use app\helpers\LogHelper;
use app\components\notifications\Notificator;
use yii\helpers\Json;
use app\models\Languages;


class NotificationActions
{
    const REQUEST_CHANGE_STATUS = 'request_change_status';
    const TASK_OVERDUE = 'task_overdue';
    const CONTACT_TAG_ADD = 'contact_tag_add';

    const ACTIONS = [
        self::CONTACT_TAG_ADD => 'Контакт - добавлен тег',
        self::TASK_OVERDUE => 'Задача просрочена',
        self::REQUEST_CHANGE_STATUS => 'Изменен статус запроса'
    ];

    const ACTIONS_ADMIN = [
        self::CONTACT_TAG_ADD => 'Contact Tag Add',
        self::TASK_OVERDUE => 'Task Overdue',
        self::REQUEST_CHANGE_STATUS => 'Request Change Status'
    ];

    private $action = false;
    private $element_id = 0;
    private $change_id = 0;
    private $notification = false;
    private $tags_data = [];
    private $tenant_data = [];

    public function getShortcodes() {
        return [
            self::CONTACT_TAG_ADD => [
                '{{contact_name}}',
                '{{contact_link}}',
                '{{agent_name}}',
                '{{tag_name}}',
            ],
            self::TASK_OVERDUE => [
                '{{contact_name}}',
                '{{agent_name}}',
                '{{task_name}}',
                '{{task due date}}',
            ],
            self::REQUEST_CHANGE_STATUS => [
                '{{contact_name}}',
                '{{contact_link}}',
                '{{agent_name}}',
                '{{request_status}}',
            ],
        ];
    }

    public function __construct($action, $element_id, $change_id = 0)
    {
        $this->action = $action;
        $this->element_id = $element_id;
        $this->change_id = $change_id;
    }

    private function getTag($tag_id) {
        if (isset($this->tags_data[$tag_id]))
            return $this->tags_data[$tag_id];

        $tag = Tags::find()->where(['id' => $tag_id])->asArray()->one();
        if (empty($tag)) 
            return false;

        return $this->tags_data[$tag_id] = $tag;
    }

    private function getPlaceholders($user, $elem) {

        if ($this->action == self::CONTACT_TAG_ADD) {
            $placeholders = [
                'contact_name' => $elem['first_name'],
                'contact_link' => \Yii::$app->params['baseUrl'] . '/ita/' . $elem['tenant_id'] . '/contacts/view?id=' . $elem['id'],

                'agent_name' => $user['user']['first_name'],
            ];

            $tag = $this->getTag($this->change_id);
            if ($tag) 
                $placeholders['tag_name'] = $tag['value'];

        }

        if ($this->action == self::TASK_OVERDUE) {

            $tenant = $this->getTenant($user['user']['current_tenant_id']);
            $format = ($tenant->date_format) ? $tenant->date_format : 'Y.m.d';

            $placeholders = [
                'agent_name' => $user['user']['first_name'],
                'task_name' => $elem['title'],
                'task_due_date' => date($format, $elem['due_date']),
            ];
        }

        if ($this->action == self::REQUEST_CHANGE_STATUS) {
            $contact = Contacts::find()->where(['id' => $elem['contact_id']])->one();
            $status = RequestStatuses ::find()->where(['id' => $elem['request_status_id']])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)->one();

            $placeholders = [
                'contact_name' => $contact->first_name,
                'contact_link' => \Yii::$app->params['baseUrl'] . '/ita/' . $elem['tenant_id'] . '/contacts/view?id=' . $elem['id'],
                'agent_name' => $user['user']['first_name'],
                'request_status' => $status->name
            ];
        }

        return $placeholders;
    }

    private function setLog($user, $element, $err) {
        if (empty($err))
            return false;


        foreach($err as $erkey => $error) {
            $send_log = new NotificationsnowLog();
            $send_log->status = (!is_array($error)) ? 1 : 0;
            $send_log->error = is_array($error) ? Json::encode($error) : '';

            $send_log->log_email_id = ($erkey == 'is_email') ? (int)$error : 0;
            $send_log->log_notice_id = ($erkey == 'is_notice') ? (int)$error : 0;

            $send_log->notification_id = $this->notification['id'];
            $send_log->user_id = $user['user']['id'];
            $send_log->element = Json::encode($element);
            $send_log->action = $user['action'];
            $send_log->chanel = $erkey;

            $send_log->save();
        }
    }

    private function toSend($user, $element) {

        $this->notification = $this->getNotification($user);
        if (empty($this->notification))
            return false;

        if ($this->notification['is_email'] == 0 && $this->notification['is_notice'] == 0)
            return false;

        $placeholders = $this->getPlaceholders($user, $element);
        $err = [];

        if ($user['is_notice'] == 1)
            $err['is_notice'] = $this->sendNotice($user, $placeholders);

        if ($user['is_email'] == 1)
            $err['is_email'] = $this->sendEmail($user, $placeholders);


        $this->setLog($user, $element, $err);
    }

    private function sendNotice($user, $placeholders)
    {
        if (empty($this->notification['text_notice']))
            return ['empty text_notice'];
        
        $text_notice = TwigHelper::render($placeholders, $this->notification['text_notice']);

        $model = new SystemNotifications();
        $model->read = 0;
        $model->text = $text_notice;
        $model->user_id = $user['user']['id'];
        $model->tenant_id = $user['user']['current_tenant_id'];
        
        if (!$model->save()) 
            return ['dont save notification'];

        return $model->id;
    }

    private function getTenant($tenant_id) {
        if (isset($this->tenant_data[$tenant_id]))
            return $this->tenant_data[$tenant_id];

        $tenant = Tenants::find()->where(['id' => $tenant_id])->one();
        return $this->tenant_data[$tenant_id] = $tenant;
    }

    private function sendEmail($user, $placeholders)
    {
        $tenant = $this->getTenant($user['user']['current_tenant_id']);
        $errors = [];

        if (empty($this->notification['title_email']) || empty($this->notification['text_email'])) 
            return ['no title_email or no text_email'];

        if (isset($tenant->reminders_email) && empty($tenant->reminders_email))
            return ['empty reminders_email'];

        if (isset($tenant->reminders_from_who) && empty($tenant->reminders_from_who)) 
            return ['empty reminders_from_who']; 

        $email = $user['user']['email'];

            $receiver = ReceiversFactory::createReceiver($email, '000000000', $user['user']['id']);
            $sender = SenderFactory::create(SenderFactory::SENDER_SYSTEM_MAIL);

            if (isset($user->reminders_email))
                $sender->email = $tenant->reminders_email;

            if (isset($user->reminders_from_who))
                $sender->name = $tenant->reminders_from_who;


            $notificationTempl = NotificationFactory::createFormTemplate((object)[
                'subject' => $this->notification['title_email'],
                'body' => $this->notification['text_email'],
            ], $placeholders);

            $mailer = Settings::findOne(['key'=>Settings::PROVIDER_PUBLIC]);

            if (is_null($mailer))
                return [$user->id .': no mailer'];

            $provider = EmailProviders::findOne(['id'=>$mailer->value]);

            if (is_null($provider))
                return [$user['user']['id'] .': no provider'];

            $mailer = [
                'class'=>$provider->provider,
                'param'=>$provider->api_key
            ];

            $log = LogHelper::saveEmailLog($notificationTempl, $receiver);

            $notific = new Notificator([
                'notification'=>$notificationTempl,
                'receivers'=>$receiver,
                'sender'=>$sender,
                'email_channel'=>$mailer,
                'email_log'=>$log,
                'channels'=>[Notificator::CHANNEL_EMAIL],
                'provider'=>$provider
            ]);

            $data = serialize($notific);
            $job = new Jobs();
            $job->executor = 'app\components\executors\NotificationExecutor';
            $job->data = $data;

            if (!$job->save())
                return ['error save job'];

        return $log->id;
    }

    public function send()
    {

        if ($this->action == self::CONTACT_TAG_ADD) {
            $contact = Contacts::find()->where(['id' => $this->element_id])->asArray()->one();
            $users = $this->getUsers();
            if (empty($users))
                return false;

            foreach($users as $user) {
                if ($user['level'] == 0 && $user['user']['id'] == $contact['responsible']) {
                    $this->toSend($user, $contact);
                }

                if ($user['level'] == 1 && $user['user']['current_tenant_id'] == $contact['tenant_id']) {
                    $this->toSend($user, $contact);
                }
            }
        }

        if ($this->action == self::TASK_OVERDUE) {
            $task = Tasks::find()->where(['id' => $this->element_id])->asArray()->one();
            $users = $this->getUsers();
            if (empty($users))
                return false;

            foreach($users as $user) {
                if ($user['level'] == 0 && $user['user']['id'] == $task['assigned_to_id']) {
                    $this->toSend($user, $task);
                }

                if ($user['level'] == 1 && $user['user']['current_tenant_id'] == $task['tenant_id']) {
                    $this->toSend($user, $task);
                }
            }
        }

        if ($this->action == self::REQUEST_CHANGE_STATUS) {
            $request = Requests::find()->where(['id' => $this->element_id])->asArray()->one();
            $users = $this->getUsers();
            if (empty($users))
                return false;

            foreach($users as $user) {
                if ($user['level'] == 0 && $user['user']['id'] == $request['responsible_id']) {
                    $this->toSend($user, $request);
                }

                if ($user['level'] == 1 && $user['user']['current_tenant_id'] == $request['tenant_id']) {
                    $this->toSend($user, $request);
                }
            }
        }
    }

    private function getNotification($user) {
        $tenant_id = $this->getTenantIdByUser($user['user']);
        $tenant = Tenants::find()->where(['id' => $tenant_id])->one();
        $lang = Languages::find()->where(['id' => $tenant->language_id])->one();

        return NotificationsNow::find()->where([
            'status' => 1,
            'class' => $this->action,
        ])->translate($lang->iso)->asArray()->one();
    }

    private function getTenantIdByUser($user)
    {
        if (isset($user['tenant_id']))
            return (int)$user['tenant_id'];

        if (isset($user['current_tenant_id']))
            return (int)$user['current_tenant_id'];

        return 0;
    }

    private function getUsers() {

        if ($this->action == self::CONTACT_TAG_ADD) {
            return ProfileNotifications::find()
                ->joinWith('tags')
                ->joinWith('user')
                ->where(['action' => $this->action, 'profile_notifications_tags.tag_id' => $this->change_id])->asArray()->all();
        }

        if ($this->action == self::TASK_OVERDUE) {
            return ProfileNotifications::find()
                ->joinWith('user')
                ->where(['action' => $this->action])->asArray()->all();
        }

        if ($this->action == self::REQUEST_CHANGE_STATUS) {
            return ProfileNotifications::find()
                ->joinWith('statuses')
                ->joinWith('user')
                ->where(['action' => $this->action, 'profile_notifications_statuses.status_id' => $this->change_id])->asArray()->all();
        }

        return false;
    }

}