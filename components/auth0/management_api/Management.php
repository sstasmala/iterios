<?php
/**
 * components/auth0/management_api/Management.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\auth0\management_api;

use Auth0\SDK\API\Authentication;
use Auth0\SDK\API\Management as ManagementApi;
use yii\base\BaseObject;

class Management extends BaseObject
{
    public $domain;

    public $client_id;

    public $client_secret;

    private $_authentication;

    private $_token;

    private $_client;

    /**
     * @return \Auth0\SDK\API\Authentication
     */
    protected function getAuthentication()
    {
        if (!($this->_authentication instanceof Authentication))
            $this->_authentication = new Authentication($this->domain, $this->client_id, $this->client_secret);

        return $this->_authentication;
    }

    /**
     * @throws \Auth0\SDK\Exception\ApiException
     */
    protected function getToken()
    {
        if (empty($this->_token)) {
            $token = $this->getAuthentication()->client_credentials([
                'audience' => 'https://' . $this->domain . '/api/v2/'
            ]);

            $this->_token = $token['access_token'];
        }

        return $this->_token;
    }

    /**
     * @throws \Auth0\SDK\Exception\ApiException
     * return \Auth0\SDK\API\Management
     */
    public function getClient()
    {
        if (!($this->_client instanceof ManagementApi))
            $this->_client = new ManagementApi($this->getToken(), $this->domain);

        return $this->_client;
    }
}