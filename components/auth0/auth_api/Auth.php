<?php
/**
 * components/auth0/management_api/Auth.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\auth0\auth_api;

use Auth0\SDK\API\Authentication;
use yii\base\BaseObject;

class Auth extends BaseObject
{
    public $domain;

    public $client_id;

    public $client_secret;

    private $_authentication;

    private $_token;

    private $_user_info;

    /**
     * @return \Auth0\SDK\API\Authentication
     */
    protected function getAuthentication()
    {
        if (!($this->_authentication instanceof Authentication))
            $this->_authentication = new Authentication($this->domain, $this->client_id, $this->client_secret);

        return $this->_authentication;
    }

    /**
     * @param $username
     * @param $password
     *
     * @return string $token
     * @throws \Auth0\SDK\Exception\ApiException
     */
    protected function getToken($username, $password)
    {
        if (empty($this->_token)) {
            $token = $this->getAuthentication()->login([
                'username' => trim($username),
                'password' => trim($password),
                'realm' => 'Username-Password-Authentication'
            ]);

            $this->_token = $token['access_token'];
        }

        return $this->_token;
    }

    /**
     * @param $email
     * @param $password
     *
     * @return array $user_info
     * @throws \Auth0\SDK\Exception\ApiException
     */
    public function getUserInfo($email, $password)
    {
        if (empty($this->_user_info))
            $this->_user_info = $this->getAuthentication()->userinfo($this->getToken($email, $password));

        return $this->_user_info;
    }
}