<?php
/**
 * components/auth0/Auth0.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\components\auth0;

use app\components\auth0\auth_api\Auth;
use app\components\auth0\management_api\Management;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Auth0 Component
 *
 * @package app\components
 */
class Auth0 extends BaseObject
{

    /**
     * @var string auth0 domain
     */
    public $domain;

    /**
     * @var array auth api client credentials
     */
    private $_auth_api_client = [];

    /**
     * @var array $management api client credentials
     */
    private $_management_api_client = [];

    /**
     * @param $management_api_client
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function setManagement_api_client($management_api_client)
    {
        if (!is_array($management_api_client) && !is_object($management_api_client)) {
            throw new InvalidConfigException('"' . get_class($this) . '::management_api_client" should be either object or array, "' . gettype($management_api_client) . '" given.');
        }
        $this->_management_api_client = $management_api_client;
    }

    /**
     * @return array|object
     */
    public function getManagement_api_client()
    {
        if (!is_object($this->_management_api_client)) {
            $this->_management_api_client = $this->createManagementApiClient($this->_management_api_client);
        }

        return $this->_management_api_client;
    }

    /**
     * @param array $config
     *
     * @return \Auth0\SDK\API\Management
     */
    protected function createManagementApiClient(array $config)
    {
        if (empty($config['domain']))
            $config['domain'] = $this->domain;

        $management_api_client = new Management($config);

        return $management_api_client->client;
    }

    /**
     * @param $auth_api_client
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function setAuth_api_client($auth_api_client)
    {
        if (!is_array($auth_api_client) && !is_object($auth_api_client)) {
            throw new InvalidConfigException('"' . get_class($this) . '::auth_api_client" should be either object or array, "' . gettype($auth_api_client) . '" given.');
        }
        $this->_auth_api_client = $auth_api_client;
    }

    /**
     * @return array|object
     */
    public function getAuth_api_client()
    {
        if (!is_object($this->_auth_api_client)) {
            $this->_auth_api_client = $this->createAuthApiClient($this->_auth_api_client);
        }

        return $this->_auth_api_client;
    }

    /**
     * @param array $config
     *
     * @return object
     */
    protected function createAuthApiClient(array $config)
    {
        if (empty($config['domain']))
            $config['domain'] = $this->domain;

        return new Auth($config);
    }
}