<?php

namespace app\models;

use app\helpers\SmsHelper;
use borales\extensions\phoneInput\PhoneInputValidator;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SendTestSmsForm extends Model
{
    public $type_alpha_name;
    public $phone_number;
    public $text;

    public $tenant_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['type_alpha_name', 'phone_number', 'text'], 'required'],
            [['type_alpha_name', 'phone_number', 'text'], 'string'],
            [['type_alpha_name', 'phone_number', 'text'], 'trim'],
            [['phone_number'], PhoneInputValidator::className()],
            [['tenant_id'], 'integer'],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['text'], 'string', 'max' => 480],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type_alpha_name' => 'Type Alpha name',
            'phone_number' => 'Phone number',
            'text' => 'SMS text'
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function send()
    {
        $send = SmsHelper::send($this->phone_number, $this->text, $this->type_alpha_name, $this->tenant_id);

        if (true === $send)
            return true;

        return $send;
    }
}
