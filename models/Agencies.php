<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agencies".
 *
 * @property int $id
 * @property string $site
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $address_description
 * @property string $logo
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Tenants $tenant
 */
class Agencies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agencies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_description'], 'string'],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['site', 'logo'], 'string', 'max' => 255],
            [['city', 'street', 'house'], 'string', 'max' => 100],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site' => 'Site',
            'city' => 'City',
            'street' => 'Street',
            'house' => 'House',
            'address_description' => 'Address Description',
            'logo' => 'Logo',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }
}
