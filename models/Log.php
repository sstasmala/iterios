<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property string $table_name
 * @property int $item_id
 * @property int $group_id
 * @property int $tenant_id
 * @property string $action
 * @property string $data
 * @property int $created_by
 * @property int $created_at
 *
 * @property Tenants $tenant
 * @property User $createdBy
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'group_id', 'tenant_id', 'created_by', 'created_at'], 'default', 'value' => null],
            [['item_id', 'group_id', 'tenant_id', 'created_by', 'created_at'], 'integer'],
            [['data'], 'string'],
            [['table_name'], 'string', 'max' => 255],
            [['action'], 'string', 'max' => 6],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'item_id' => 'Item ID',
            'group_id' => 'Group ID',
            'tenant_id' => 'Tenant ID',
            'action' => 'Action',
            'data' => 'Data',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
