<?php

namespace app\models\search;

use app\models\Languages;
use app\models\PlaceholdersDelivery;
use app\models\PlaceholdersDeliveryTypes;
use app\models\PlaceholdersDocument;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Placeholders as PlaceholdersModel;

/**
 * CustomPlaceholders represents the model behind the search form about `app\models\CustomPlaceholders`.
 */
class Placeholders extends PlaceholdersModel
{
    const TYPE_DOCUMENT = 'document';
    const TYPE_DELIVERY = 'delivery';

    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['short_code', 'default', 'type_placeholder', 'module', 'field'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null, $type = null)
    {
        if ($lang === null)
            $lang = Languages::getDefault();

        $query = PlaceholdersModel::find()->translate($lang);

        if ($type !== null && $type === static::TYPE_DOCUMENT) {
            $ph_doc_ids = PlaceholdersDocument::find()
                ->select('placeholder_id')->column();

            $query->where(['in', 'id', $ph_doc_ids]);
        }

        if ($type !== null && $type === static::TYPE_DELIVERY) {
            $ph_delivery_ids = PlaceholdersDelivery::find()
                ->select('placeholder_id');

            if (!empty($params['Placeholders']['delivery_type'])) {
                $delivery_placeholder_ids = PlaceholdersDeliveryTypes::find()
                    ->select('DISTINCT(placeholder_delivery_id)')
                    ->where(['delivery_type_id' => $params['Placeholders']['delivery_type']]);

                $ph_delivery_ids->where(['in', 'id', $delivery_placeholder_ids]);
            }

            $ph_delivery_ids = $ph_delivery_ids->column();

            $query->where(['in', 'id', $ph_delivery_ids]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'short_code', $this->short_code])
            ->andFilterWhere(['ilike', 'default', $this->default])
            ->andFilterWhere(['ilike', 'type_placeholder', $this->type_placeholder])
            ->andFilterWhere(['ilike', 'module', $this->module])
            ->andFilterWhere(['ilike', 'field', $this->field]);

        return $dataProvider;
    }
}
