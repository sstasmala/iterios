<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentGateways as PaymentGatewaysModel;

/**
 * PaymentGateways represents the model behind the search form about `app\models\PaymentGateways`.
 */
class PaymentGateways extends PaymentGatewaysModel
{
    public function rules()
    {
        return [
            [['id', 'demo', 'created_at', 'updated_at'], 'integer'],
            [['name', 'logo', 'countries', 'currencies'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PaymentGatewaysModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'demo' => $this->demo,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'logo', $this->logo])
            ->andFilterWhere(['ilike', 'countries', $this->countries])
            ->andFilterWhere(['ilike', 'currencies', $this->currencies]);

        return $dataProvider;
    }
}
