<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationsnowLog as NotificationsnowLogModel;

/**
 * NotificationsnowLog represents the model behind the search form about `app\models\NotificationsnowLog`.
 */
class NotificationsnowLog extends NotificationsnowLogModel
{
    public function rules()
    {
        return [
            [['id', 'status', 'notification_id', 'created_at', 'updated_at'], 'integer'],
            [['error', 'element', 'action', 'chanel'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = NotificationsnowLogModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['id' => SORT_DESC]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'notification_id' => $this->notification_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'error', $this->error])
            ->andFilterWhere(['ilike', 'element', $this->element])
            ->andFilterWhere(['ilike', 'action', $this->action])
            ->andFilterWhere(['ilike', 'chanel', $this->chanel]);

        return $dataProvider;
    }
}
