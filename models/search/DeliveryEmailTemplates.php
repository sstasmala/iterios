<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeliveryEmailTemplates as DeliveryEmailTemplatesModel;

/**
 * DeliveryEmailTemplates represents the model behind the search form about `app\models\DeliveryEmailTemplates`.
 */
class DeliveryEmailTemplates extends DeliveryEmailTemplatesModel
{
    public function rules()
    {
        return [
            [['id', 'delivery_type_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'subject', 'body', 'type'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $type = false, $lang = null)
    {
        if ($lang == null)
            $lang = Languages::getDefault();

        $query = DeliveryEmailTemplatesModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['type' => empty($type) ? null : $type]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'delivery_type_id' => $this->delivery_type_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}
