<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Providers as ProvidersModel;

/**
 * DocumentsTemplate represents the model behind the search form about `app\models\DocumentsTemplate`.
 */
class Providers extends ProvidersModel
{

    public function rules()
    {
        return [
            [['id', 'provider_type_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'code', 'type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();

        $query = ProvidersModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'provider_type_id' => $this->provider_type_id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'code', $this->code]);

        return $dataProvider;
    }
}
