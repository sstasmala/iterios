<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReminderEventTypes;

/**
 * ReminderEventTypesSearch represents the model behind the search form about `app\models\ReminderEventTypes`.
 */
class ReminderEventTypesSearch extends ReminderEventTypes
{
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['action', 'name', 'icon_class', 'color_type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if ($lang == null)
            $lang = Languages::getDefault();

        $query = ReminderEventTypes::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['id' => SORT_ASC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'action', $this->action])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'icon_class', $this->icon_class])
            ->andFilterWhere(['ilike', 'color_type', $this->color_type]);

        return $dataProvider;

        /*
        $query = ReminderEventTypes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'icon_class', $this->icon_class])
            ->andFilterWhere(['like', 'color_type', $this->color_type]);

        return $dataProvider;*/
    }
}
