<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TemplatesHandlers as TemplatesHandlersModel;

/**
 * TemplatesHandlers represents the model behind the search form about `app\models\TemplatesHandlers`.
 */
class TemplatesHandlers extends TemplatesHandlersModel
{
    public function rules()
    {
        return [
            [['id', 'template_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['handler'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$types = [])
    {
        $query = TemplatesHandlersModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'template_id' => $this->template_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'handler', $this->handler]);
        if(!is_array($types))
            $types = [$types];

        if(!empty($types))
        {
            $ids = Templates::find();
            foreach ($types as $type)
                $ids->orWhere(['ilike','type',$type]);
            $ids = array_column($ids->asArray()->all(),'id');
            $query->andFilterWhere(['in','template_id',$ids]);
        }

        return $dataProvider;
    }
}
