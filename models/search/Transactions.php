<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transactions as TransactionsModel;

/**
 * Transactions represents the model behind the search form about `app\models\Transactions`.
 */
class Transactions extends TransactionsModel
{
    public function rules()
    {
        return [
            [['id', 'action', 'tariff_id', 'financial_operation_id', 'tariff_order_id', 'owner', 'date', 'created_at','tenant_id', 'updated_at'], 'integer'],
            [['sum'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TransactionsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sum' => $this->sum,
            'action' => $this->action,
            'tariff_id' => $this->tariff_id,
            'tenant_id' => $this->tenant_id,
            'financial_operation_id' => $this->financial_operation_id,
            'tariff_order_id' => $this->tariff_order_id,
            'owner' => $this->owner,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
