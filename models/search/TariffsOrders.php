<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TariffsOrders as TariffsOrdersModel;

/**
 * TariffsOrders represents the model behind the search form about `app\models\TariffsOrders`.
 */
class TariffsOrders extends TariffsOrdersModel
{
    public function rules()
    {
        return [
            [['id', 'tariff_id', 'customer_id', 'duration_date', 'status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TariffsOrdersModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tariff_id' => $this->tariff_id,
            'customer_id' => $this->customer_id,
            'duration_date' => $this->duration_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
    }
}
