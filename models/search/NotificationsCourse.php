<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationsCourse as NotificationsCourseModel;

/**
 * NotificationsCourse represents the model behind the search form about `app\models\NotificationsCourse`.
 */
class NotificationsCourse extends NotificationsCourseModel
{
    public function rules()
    {
        return [
            [['id', 'send_type', 'user_id', 'tenant_id', 'user_type', 'send_status', 'noting_id', 'created_at'], 'integer'],
            [['date', 'time', 'day', 'is_notification', 'is_task', 'is_email', 'errors', 'timezone_date', 'timezone_time', 'timezone_day', 'rule_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = NotificationsCourseModel::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['timezone_date' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $likeData = [];
        $likeDataNot = [];
        
        if (!empty($params['NotificationsCourse']['is_task'])){
            if ($params['NotificationsCourse']['is_task'] === 'true') {
                $likeData[] = 'is_task":""';
            } else {
                $likeData[] = 'is_task":';
                $likeDataNot[] = 'is_task":""';
            }
        }

        if (!empty($params['NotificationsCourse']['is_email'])){
            if ($params['NotificationsCourse']['is_email'] === 'true') {
                $likeData[] = 'is_email":""';
            } else {
                $likeData[] = 'is_email":';
                $likeDataNot[] = 'is_email":""';
            }
        }

        if (!empty($params['NotificationsCourse']['is_notification'])){
            if ($params['NotificationsCourse']['is_notification'] === 'true') {
                $likeData[] = 'is_system_notification":""';
            } else {
                $likeData[] = 'is_system_notification":';
                $likeDataNot[] = 'is_system_notification":""';
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'send_type' => $this->send_type,
            'user_id' => $this->user_id,
            'tenant_id' => $this->tenant_id,
            'user_type' => $this->user_type,
            'send_status' => $this->send_status,
            'noting_id' => $this->noting_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['ilike', 'date', $this->date])
            ->andFilterWhere(['ilike', 'time', $this->time])
            ->andFilterWhere(['ilike', 'day', $this->day])
            ->andFilterWhere(['ilike', 'timezone_date', $this->timezone_date])
            ->andFilterWhere(['ilike', 'timezone_time', $this->timezone_time])
            ->andFilterWhere(['ilike', 'timezone_day', $this->timezone_day])
            ->andFilterWhere(['ilike', 'rule_id', $this->rule_id])
            ->andFilterWhere(['ilike', 'errors', $this->errors]);

        if (!empty($likeData))
            $query->andFilterWhere(['ilike', 'errors', $likeData]);

        if (!empty($likeDataNot))
            $query->andFilterWhere(['not like', 'errors', $likeDataNot]);

        return $dataProvider;
    }
}
