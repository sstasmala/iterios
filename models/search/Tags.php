<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tags as TagsModel;

/**
 * Tags represents the model behind the search form about `app\models\Tags`.
 */
class Tags extends TagsModel
{
    public function rules()
    {
        return [
            [['id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['value', 'type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $type, $lang = null)
    {
        if ($lang === null)
            $lang = Languages::getDefault();

        $query = TagsModel::find()->translate($lang)
            ->where(['type' => $type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'value', $this->value]);

        return $dataProvider;
    }
}
