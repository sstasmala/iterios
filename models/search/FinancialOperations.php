<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinancialOperations as FinancialOperationsModel;

/**
 * FinancialOperations represents the model behind the search form about `app\models\FinancialOperations`.
 */
class FinancialOperations extends FinancialOperationsModel
{
    public function rules()
    {
        return [
            [['id', 'payment_method', 'transaction_number', 'operation_type', 'customer_id', 'payment_gateway_id', 'currency', 'date', 'owner', 'created_at', 'updated_at'], 'integer'],
            [['account_number', 'comment'], 'safe'],
            [['sum', 'sum_usd'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = FinancialOperationsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'payment_method' => $this->payment_method,
            'transaction_number' => $this->transaction_number,
            'operation_type' => $this->operation_type,
            'customer_id' => $this->customer_id,
            'payment_gateway_id' => $this->payment_gateway_id,
            'sum' => $this->sum,
            'currency' => $this->currency,
            'sum_usd' => $this->sum_usd,
            'date' => $this->date,
            'owner' => $this->owner,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'account_number', $this->account_number])
            ->andFilterWhere(['ilike', 'comment', $this->comment]);

        return $dataProvider;
    }
}
