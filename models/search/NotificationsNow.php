<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationsNow as NotificationsNowModel;

/**
 * NotificationsNow represents the model behind the search form about `app\models\NotificationsNow`.
 */
class NotificationsNow extends NotificationsNowModel
{
    public function rules()
    {
        return [
            [['id', 'is_email', 'is_notice', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'class', 'text_email', 'text_notice'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault()->iso;
        $query = NotificationsNowModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_email' => $this->is_email,
            'is_notice' => $this->is_notice,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'class', $this->class])
            ->andFilterWhere(['ilike', 'text_email', $this->text_email])
            ->andFilterWhere(['ilike', 'text_notice', $this->text_notice]);

        return $dataProvider;
    }
}
