<?php

namespace app\models\search;

use app\models\Languages;
use app\models\Translations;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UiTranslations as UiTranslationsModel;

/**
 * UiTranslations represents the model behind the search form about `app\models\UiTranslations`.
 */
class UiTranslations extends UiTranslationsModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['group', 'code', 'value'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = UiTranslationsModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        if (!empty($this->value)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $this->value])
                ->orFilterWhere(['ilike', 'value',  $this->value])
                ->andWhere('"key" ILIKE :table', [':table'=>"ui_translations.value.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $query->andWhere(['in', 'id', array_unique($translate)]);
        }

        $query->andFilterWhere(['ilike', 'group', $this->group])
            ->andFilterWhere(['ilike', 'code', $this->code]);

        return $dataProvider;
    }
}
