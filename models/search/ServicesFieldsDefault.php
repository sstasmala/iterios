<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ServicesFieldsDefault as ServicesFieldsDefaultModel;

/**
 * ServicesFieldsDefault represents the model behind the search form about `app\models\ServicesFieldsDefault`.
 */
class ServicesFieldsDefault extends ServicesFieldsDefaultModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['code', 'type'], 'safe'],
            [['is_additional', 'in_header', 'is_default'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $default = false, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = ServicesFieldsDefaultModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($default) $query->where(['is_default'=>1]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_additional' => $this->is_additional,
            'in_header' => $this->in_header,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'code', $this->code])
            ->andFilterWhere(['ilike', 'type', $this->type]);

        return $dataProvider;
    }
}
