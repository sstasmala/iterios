<?php

namespace app\models\search;

use app\models\Languages;
use app\models\Translations;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DocumentsTemplate as DocumentsTemplateModel;

/**
 * DocumentsTemplate represents the model behind the search form about `app\models\DocumentsTemplate`.
 */
class DocumentsTemplate extends DocumentsTemplateModel
{

    public function rules()
    {
        return [
            [['id', 'documents_type_id', 'suppliers_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'languages', 'body', 'type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $type = false, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = DocumentsTemplateModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['type' => empty($type) ? null : $type]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'documents_type_id' => $this->documents_type_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);


        if (!empty($this->body)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['ilike', 'value',  $this->body])
                ->orFilterWhere(['ilike', 'value',  $this->body])
                ->andWhere('"key" ILIKE :table', [':table'=>"templates.body.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $query->andWhere(['in', 'id', array_unique($translate)]);
        }


        return $dataProvider;
    }
}
