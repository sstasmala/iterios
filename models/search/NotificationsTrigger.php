<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationsTrigger as NotificationsTriggerModel;

/**
 * NotificationsTrigger represents the model behind the search form about `app\models\NotificationsTrigger`.
 */
class NotificationsTrigger extends NotificationsTriggerModel
{
    public function rules()
    {
        return [
            [['id', 'element_id', 'value_int', 'type', 'trigger_time', 'agent_id', 'contact_id', 'tenant_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['action', 'value', 'data'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = NotificationsTriggerModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['trigger_time' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'element_id' => $this->element_id,
            'value_int' => $this->value_int,
            'type' => $this->type,
            'trigger_time' => $this->trigger_time,
            'agent_id' => $this->agent_id,
            'contact_id' => $this->contact_id,
            'tenant_id' => $this->tenant_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'action', $this->action])
            ->andFilterWhere(['ilike', 'value', $this->value])
            ->andFilterWhere(['ilike', 'data', $this->data]);

        return $dataProvider;
    }
}
