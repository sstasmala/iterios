<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BaseNotifications as BaseNotificationsModel;

/**
 * BaseNotifications represents the model behind the search form about `app\models\BaseNotifications`.
 */
class BaseNotifications extends BaseNotificationsModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'status', 'created_by', 'updated_by'], 'integer'],
            [['name', 'subject', 'body', 'module', 'system_notification', 'sms', 'task_config', 'condition', 'schedule'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();

        $query = BaseNotificationsModel::find()->translate($lang);

        $query->orderBy(['id' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'subject', $this->subject])
            ->andFilterWhere(['ilike', 'module', $this->module])
            ->andFilterWhere(['ilike', 'body', $this->body])
            ->andFilterWhere(['ilike', 'system_notification', $this->system_notification])
            ->andFilterWhere(['ilike', 'sms', $this->sms])
            ->andFilterWhere(['ilike', 'task_config', $this->task_config])
            ->andFilterWhere(['ilike', 'condition', $this->condition])
            ->andFilterWhere(['ilike', 'schedule', $this->schedule]);

        return $dataProvider;
    }
}
