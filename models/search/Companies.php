<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Companies as CompaniesModel;

/**
 * Companies represents the model behind the search form about `app\models\Companies`.
 */
class Companies extends CompaniesModel
{
    public function rules()
    {
        return [
            [['id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'responsible_id', 'company_address_id', 'company_type_id', 'company_kind_activity_id', 'company_contact_source_id', 'company_status_id'], 'integer'],
            [['name', 'legal_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CompaniesModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'responsible_id' => $this->responsible_id,
            'company_address_id' => $this->company_address_id,
            'company_type_id' => $this->company_type_id,
            'company_kind_activity_id' => $this->company_kind_activity_id,
            'company_contact_source_id' => $this->company_contact_source_id,
            'company_status_id' => $this->company_status_id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'legal_name', $this->legal_name]);

        return $dataProvider;
    }
}
