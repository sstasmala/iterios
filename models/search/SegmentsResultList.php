<?php

namespace app\models\search;

use app\models\ContactsEmails;
use app\models\ContactsPhones;
use Yii;
use yii\base\Model;
use yii\data\SqlDataProvider;
use app\models\SegmentsResultList as SegmentsResultListModel;


class SegmentsResultList extends SegmentsResultListModel
{
    public $segment;
    public $tenant;

//    public $contact;


    public function rules()
    {
        return [
            [['id', 'segment', 'tenant'], 'safe'],
//            [['id', 'id_relation', 'segment_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $query = SegmentsResultListModel::find()
            ->select([
                'segments.name as segment',
                'tenants.name as tenant',
                'count(contacts.id) as contact',
                'count(contacts_emails.value) as email',
                'count(contacts_phones.value) as phone',
            ])
            ->innerJoin(SegmentsRelations::tableName(), 'segments_result_list.id_relation = segments_relations.id')
            ->innerJoin(Segments::tableName(), 'segments_relations.id_segment = segments.id')
            ->leftJoin(ContactsEmails::tableName(), 'segments_result_list.id_contact = contacts_emails.contact_id')
            ->leftJoin(ContactsPhones::tableName(), 'segments_result_list.id_contact = contacts_phones.contact_id')
            ->innerJoin(Contacts::tableName(), 'segments_result_list.id_contact = contacts.id')
            ->innerJoin(Tenants::tableName(), 'contacts.tenant_id = tenants.id')
            ->andWhere('contacts_emails.contact_id is not null OR contacts_phones.contact_id is not null');

        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['ilike', 'segments.name', $this->segment]);
            $query->andFilterWhere(['ilike', 'tenants.name', $this->tenant]);
        }

        $query->orderBy('segments.name', 'tenants.name');
        $query->groupBy(['segments.name', 'tenants.name']);

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->getRawSql(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);


        return $dataProvider;
    }
}
