<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DocumentsType as DocumentsTypeModel;

/**
 * TasksTypes represents the model behind the search form about `app\models\TasksTypes`.
 */
class DocumentsType extends DocumentsTypeModel
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();

        $query = DocumentsTypeModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->value]);

        return $dataProvider;
    }
}
