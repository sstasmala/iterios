<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogsCron as LogsCronModel;

/**
 * LogsCron represents the model behind the search form about `app\models\LogsCron`.
 */
class LogsCron extends LogsCronModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['command', 'output_file', 'memory_usage', 'status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = LogsCronModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params) || !isset($params['sort']))
            $query->orderBy(['updated_at' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'command', $this->command])
            ->andFilterWhere(['ilike', 'output_file', $this->output_file])
            ->andFilterWhere(['ilike', 'memory_usage', $this->memory_usage])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}
