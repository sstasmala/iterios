<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Requisites as RequisitesModel;

/**
 * Requisites represents the model behind the search form about `app\models\Requisites`.
 */
class Requisites extends RequisitesModel
{
    public function rules()
    {
        return [
            [['id', 'country_id', 'requisites_group_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RequisitesModel::find()->select('country_id');
        $query->groupBy('country_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'requisites_group_id' => $this->requisites_group_id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
