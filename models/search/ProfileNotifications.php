<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProfileNotifications as ProfileNotificationsModel;

/**
 * ProfileNotifications represents the model behind the search form about `app\models\ProfileNotifications`.
 */
class ProfileNotifications extends ProfileNotificationsModel
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'level', 'is_email', 'is_notice', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['action'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ProfileNotificationsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'level' => $this->level,
            'is_email' => $this->is_email,
            'is_notice' => $this->is_notice,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'action', $this->action]);

        return $dataProvider;
    }
}
