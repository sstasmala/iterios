<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SegmentsRelations as SegmentsRelationsModel;

/**
 * DocumentsTemplate represents the model behind the search form about `app\models\DocumentsTemplate`.
 */
class SegmentsRelations extends SegmentsRelationsModel
{

    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'id_segment', 'id_tenant'], 'integer'],
            [['name', 'type','count_phones','count_emails','count_contact'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = SegmentsRelationsModel::find()->with(['created','updated']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_segment' => $this->id_segment,
            'id_tenant' => $this->id_tenant,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);


        return $dataProvider;
    }
}
