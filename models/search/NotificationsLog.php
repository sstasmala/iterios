<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationsLog as NotificationsLogModel;

/**
 * NotificationsLog represents the model behind the search form about `app\models\NotificationsLog`.
 */
class NotificationsLog extends NotificationsLogModel
{
    public function rules()
    {
        return [
            [['id', 'trigger_id', 'reminder_id', 'course_id', 'contact_id', 'agent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['channel', 'errors'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = NotificationsLogModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['id' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'trigger_id' => $this->trigger_id,
            'course_id' => $this->course_id,
            'reminder_id' => $this->reminder_id,
            'contact_id' => $this->contact_id,
            'agent_id' => $this->agent_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'channel', $this->channel])
            ->andFilterWhere(['ilike', 'errors', $this->errors]);

        return $dataProvider;
    }
}
