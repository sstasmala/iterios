<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmsCountries as SmsCountriesModel;

/**
 * SmsCountries represents the model behind the search form about `app\models\SmsCountries`.
 */
class SmsCountries extends SmsCountriesModel
{
    public function rules()
    {
        return [
            [['id', 'provider_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'iso_code', 'dial_code'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SmsCountriesModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy('id');

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'iso_code', $this->iso_code])
            ->andFilterWhere(['ilike', 'dial_code', $this->dial_code]);

        return $dataProvider;
    }
}
