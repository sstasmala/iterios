<?php

namespace app\models\search;

use app\models\DeliveryPlaceholderTypes;
use app\models\Languages;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeliveryPlaceholders as DeliveryPlaceholdersModel;

/**
 * DeliveryPlaceholders represents the model behind the search form about `app\models\DeliveryPlaceholders`.
 */
class DeliveryPlaceholders extends DeliveryPlaceholdersModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['short_code', 'module', 'path', 'default'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if ($lang === null)
            $lang = Languages::getDefault();

        $query = DeliveryPlaceholdersModel::find()->translate($lang);

        if (!empty($params['DeliveryPlaceholders']['type'])) {
            $delivery_placeholder_ids = DeliveryPlaceholderTypes::find()
                ->select('DISTINCT(delivery_placeholder_id)')
                ->where(['delivery_type_id' => $params['DeliveryPlaceholders']['type']]);

            $query->where(['in', 'id', $delivery_placeholder_ids]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'short_code', $this->short_code])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'default', $this->default]);

        return $dataProvider;
    }
}
