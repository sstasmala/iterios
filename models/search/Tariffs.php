<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tariffs as TariffsModel;

/**
 * Tariffs represents the model behind the search form about `app\models\Tariffs`.
 */
class Tariffs extends TariffsModel
{
    public function rules()
    {
        return [
            [['id', 'tariff_duration', 'tariff_start', 'tariff_end', 'days_before_stop', 'need_act', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'country'], 'safe'],
            [['price'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault()->iso;
        $query = TariffsModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tariff_duration' => $this->tariff_duration,
            'tariff_start' => $this->tariff_start,
            'tariff_end' => $this->tariff_end,
            'days_before_stop' => $this->days_before_stop,
            'price' => $this->price,
            'need_act' => $this->need_act,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'country', $this->country]);

        return $dataProvider;
    }
}
