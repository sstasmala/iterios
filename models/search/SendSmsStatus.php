<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SendSmsStatus as SendSmsStatusModel;


class SendSmsStatus extends SendSmsStatusModel
{
    public function rules()
    {
        return [
            [['send_sms_id', 'contact_phone_id', 'status','time_send'], 'integer'],
            [['segments', 'provider_sms_id'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SendSmsStatusModel::find()->joinWith('sendSms');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'send_sms_id' => $this->send_sms_id,
            'contact_phone_id' => $this->contact_phone_id,
            'status' => $this->status,
            'time_send' => $this->time_send,
            'provider_sms_id' => $this->provider_sms_id,
        ]);


        return $dataProvider;
    }
}
