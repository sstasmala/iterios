<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tenants;

/**
 * Customers represents the model behind the search form about `app\models\Tenants`.
 */
class Customers extends Tenants
{
    public function rules()
    {
        return [
            [['id', 'owner_id', 'language_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'main_currency', 'use_webmail', 'tariff_id', 'payment_method'], 'integer'],
            [['name', 'timezone', 'date_format', 'time_format', 'country', 'reminders_email', 'reminders_from_who', 'usage_currency', 'mailer_config', 'week_start', 'legal_name'], 'safe'],
            [['system_tags'], 'boolean'],
            [['balance'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Tenants::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'owner_id' => $this->owner_id,
            'language_id' => $this->language_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'main_currency' => $this->main_currency,
            'system_tags' => $this->system_tags,
            'use_webmail' => $this->use_webmail,
            'balance' => $this->balance,
            'tariff_id' => $this->tariff_id,
            'payment_method' => $this->payment_method,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'timezone', $this->timezone])
            ->andFilterWhere(['ilike', 'date_format', $this->date_format])
            ->andFilterWhere(['ilike', 'time_format', $this->time_format])
            ->andFilterWhere(['ilike', 'country', $this->country])
            ->andFilterWhere(['ilike', 'reminders_email', $this->reminders_email])
            ->andFilterWhere(['ilike', 'reminders_from_who', $this->reminders_from_who])
            ->andFilterWhere(['ilike', 'usage_currency', $this->usage_currency])
            ->andFilterWhere(['ilike', 'mailer_config', $this->mailer_config])
            ->andFilterWhere(['ilike', 'week_start', $this->week_start])
            ->andFilterWhere(['ilike', 'legal_name', $this->legal_name]);

        return $dataProvider;
    }
}
