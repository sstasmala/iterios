<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExchangeRates as ExchangeRatesModel;

/**
 * ExchangeRates represents the model behind the search form about `app\models\ExchangeRates`.
 */
class ExchangeRates extends ExchangeRatesModel
{
    public function rules()
    {
        return [
            [['id', 'currency_id', 'created_at', 'updated_at'], 'integer'],
            [['currency_iso'], 'safe'],
            [['rate'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ExchangeRatesModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'currency_id' => $this->currency_id,
            'rate' => $this->rate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'currency_iso', $this->currency_iso]);

        return $dataProvider;
    }
}
