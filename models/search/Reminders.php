<?php

namespace app\models\search;

use app\models\Languages;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reminders as RemindersModel;

/**
 * Reminders represents the model behind the search form about `app\models\Reminders`.
 */
class Reminders extends RemindersModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['type', 'name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $type, $lang = null)
    {
        if ($lang === null)
            $lang = Languages::getDefault();

        $query = RemindersModel::find()->translate($lang)
            ->where(['type' => $type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
