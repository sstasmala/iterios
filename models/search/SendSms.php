<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SendSms as SendSmsModel;


class SendSms extends SendSmsModel
{
    public $dateFrom;
    public $dateTo;
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'provider_id', 'tenant_id','dateFrom','dateTo'], 'integer'],
            [['name','status'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SendSmsModel::find()->select('send_sms.*, users.photo')->joinWith('created')->indexBy('id');

        if(isset($params['title'])&&$params['title']!='') $this->name = $params['title'];
        if(isset($params['quick-search'])&&$params['quick-search']!='') $this->name = $params['quick-search'];
        if(isset($params['select_status'])) $this->status = $params['select_status'];
        if(isset($params['select_provider'])) $this->provider_id = $params['select_provider'];
        if(isset($params['date_start'])) $this->dateFrom = $params['date_start'];
        if(isset($params['date_end'])) $this->dateTo = $params['date_end'];
        if(isset($params['select_user'])) $this->created_by = $params['select_user'];
        $this->tenant_id = Yii::$app->user->identity->tenant->id;

        if ($this->validate()) {

            $query->andFilterWhere([
                'send_sms.status' => $this->status,
                'provider_id' => $this->provider_id,
                'created_by' => $this->created_by,
                'tenant_id' => $this->tenant_id,
            ])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['between', 'send_sms.created_at', $this->dateFrom, $this->dateTo]);

            return $query;
        }

    }
}
