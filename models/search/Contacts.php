<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contacts as ContactsModel;

/**
 * Contacts represents the model behind the search form about `app\models\Contacts`.
 */
class Contacts extends ContactsModel
{
    public function rules()
    {
        return [
            [['id', 'sex', 'discount', 'company_id', 'residence_address_id', 'living_address_id', 'responsible', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'date_of_birth', 'nationality', 'tin', 'type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ContactsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sex' => $this->sex,
            'discount' => $this->discount,
            'company_id' => $this->company_id,
            'residence_address_id' => $this->residence_address_id,
            'living_address_id' => $this->living_address_id,
            'responsible' => $this->responsible,
            'tenant_id' => $this->tenant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'middle_name', $this->middle_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'date_of_birth', $this->date_of_birth])
            ->andFilterWhere(['ilike', 'nationality', $this->nationality])
            ->andFilterWhere(['ilike', 'tin', $this->tin])
            ->andFilterWhere(['ilike', 'type', $this->type]);

        return $dataProvider;
    }
}
