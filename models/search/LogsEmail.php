<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogsEmail as LogsEmailModel;

/**
 * LogsEmail represents the model behind the search form about `app\models\LogsEmail`.
 */
class LogsEmail extends LogsEmailModel
{
    public function rules()
    {
        return [
            [['id', 'template_id', 'created_at', 'updated_at'], 'integer'],
            [['subject', 'text', 'html', 'recipient', 'status', 'reason', 'ip', 'city'], 'safe'],
            [['conversion'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $type = false)
    {
        $query = LogsEmailModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['type' => (empty($type) ? null : $type)]);

        if (empty($params))
            $query->orderBy(['created_at' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->created_at)) {
            list($d_start, $d_end) = explode(' - ', $this->created_at);

            $d_start = date_create($d_start);
            $d_end = date_create($d_end);

            $query->andFilterWhere(['between', 'created_at', date_format($d_start, 'U'), date_format($d_end, 'U')]);
        }

        if (!empty($this->updated_at)) {
            list($d_start, $d_end) = explode(' - ', $this->updated_at);

            $d_start = date_create($d_start);
            $d_end = date_create($d_end);

            $query->andFilterWhere(['between', 'updated_at', date_format($d_start, 'U'), date_format($d_end, 'U')]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'template_id' => $this->template_id,
            'conversion' => $this->conversion
        ]);

        $query->andFilterWhere(['ilike', 'subject', $this->subject])
            ->andFilterWhere(['ilike', 'text', $this->text])
            ->andFilterWhere(['ilike', 'html', $this->html])
            //->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'recipient', $this->recipient])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'reason', $this->reason])
            ->andFilterWhere(['ilike', 'ip', $this->ip])
            ->andFilterWhere(['ilike', 'city', $this->city]);

        return $dataProvider;
    }
}
