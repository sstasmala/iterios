<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User as UserModel;

/**
 * User represents the model behind the search form about `app\models\User`.
 */
class User extends UserModel
{
    public function rules()
    {
        return [
            [['id', 'status', 'last_login', 'created_at', 'updated_at', 'current_tenant_id'], 'integer'],
            [['auth_key', 'password_hash', 'password_reset_token', 'first_name', 'last_name', 'middle_name', 'email', 'phone', 'photo'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UserModel::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'last_login' => $this->last_login,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'current_tenant_id' => $this->current_tenant_id,
        ]);

        if (isset($this->status))
            $query->andFilterWhere(['status' => ($this->status == true || $this->status == '' ? 10 : 0)]);

        $query->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
            ->andFilterWhere(['ilike', 'password_hash', $this->password_hash])
            ->andFilterWhere(['ilike', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'middle_name', $this->middle_name])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'photo', $this->photo]);

        if($this->is_admin == '1') {
            $query->andFilterWhere(['in', 'id', \app\helpers\RbacHelper::getAssignedUsers('system_admin')]);
        } elseif($this->is_admin === '0') {
            $query->andFilterWhere(['not in', 'id', \app\helpers\RbacHelper::getAssignedUsers('system_admin')]);
        }
        return $dataProvider;
    }
}
