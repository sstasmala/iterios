<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TariffsFeature as TariffsFeatureModel;

/**
 * TariffsFeature represents the model behind the search form about `app\models\TariffsFeature`.
 */
class TariffsFeature extends TariffsFeatureModel
{
    public function rules()
    {
        return [
            [['id', 'countable', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'class'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault()->iso;
        $query = TariffsFeatureModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'countable' => $this->countable,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'class', $this->class]);

        return $dataProvider;
    }
}
