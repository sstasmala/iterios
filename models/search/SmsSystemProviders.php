<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmsSystemProviders as SmsSystemProvidersModel;

/**
 * SmsSystemProviders represents the model behind the search form about `app\models\SmsSystemProviders`.
 */
class SmsSystemProviders extends SmsSystemProvidersModel
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['provider', 'name', 'system_aname', 'public_aname'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SmsSystemProvidersModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'provider' => $this->provider,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'system_aname', $this->system_aname])
            ->andFilterWhere(['ilike', 'public_aname', $this->public_aname]);

        return $dataProvider;
    }
}
