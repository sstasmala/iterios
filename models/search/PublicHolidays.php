<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PublicHolidays as PublicHolidaysModel;

/**
 * SystemHolidays represents the model behind the search form about `app\models\SystemHolidays`.
 */
class PublicHolidays extends PublicHolidaysModel
{
    public function rules()
    {
        return [
            [['id', 'day', 'mounts','user_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PublicHolidaysModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'day' => $this->day,
            'mounts' => $this->mounts,
            'user_id'=> $this->user_id,
        ]);

        $query->with('user');

        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
