<?php

namespace app\models\search;

use Yii;
use app\models\Languages;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DocumentPlaceholders as DocumentPlaceholdersModel;

/**
 * This is the model class for table "document_placeholders".
 *
 * @property int $id
 * @property string $short_code
 * @property string $module
 * @property string $path
 * @property string $default
 */
class DocumentPlaceholders extends DocumentPlaceholdersModel
{

    public function rules()
    {
        return [
            [['short_code', 'module', 'path', 'default'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();

        $query = DocumentPlaceholdersModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'short_code' => $this->short_code,
            'module' => $this->module,
            'path' => $this->path,
            'default' => $this->default,
        ]);

        return $dataProvider;
    }
}