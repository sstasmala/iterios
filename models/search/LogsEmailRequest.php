<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogsEmailRequest as LogsEmailRequestModel;

/**
 * LogsEmailRequest represents the model behind the search form about `app\models\LogsEmailRequest`.
 */
class LogsEmailRequest extends LogsEmailRequestModel
{
    public function rules()
    {
        return [
            [['id', 'log_email_id', 'email_provider_id', 'created_at'], 'integer'],
            [['event', 'method', 'headers', 'body', 'url'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = LogsEmailRequestModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['created_at' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'log_email_id' => $this->log_email_id,
            'email_provider_id' => $this->email_provider_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['ilike', 'event', $this->event])
            ->andFilterWhere(['ilike', 'method', $this->method])
            ->andFilterWhere(['ilike', 'headers', $this->headers])
            ->andFilterWhere(['ilike', 'body', $this->body])
            ->andFilterWhere(['ilike', 'url', $this->url]);

        return $dataProvider;
    }
}
