<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogsSms as LogsSmsModel;

/**
 * LogsSms represents the model behind the search form about `app\models\LogsSms`.
 */
class LogsSms extends LogsSmsModel
{
    public function rules()
    {
        return [
            [['id', 'provider_id', 'country_id', 'alpha_name_id', 'created_at', 'updated_at'], 'integer'],
            [['text', 'recipient', 'status', 'alpha_name_type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = LogsSmsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (empty($params))
            $query->orderBy(['created_at' => SORT_DESC]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'country_id' => $this->country_id,
            'alpha_name_id' => $this->alpha_name_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'text', $this->text])
            ->andFilterWhere(['ilike', 'recipient', $this->recipient])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'alpha_name_type', $this->alpha_name_type]);

        return $dataProvider;
    }
}
