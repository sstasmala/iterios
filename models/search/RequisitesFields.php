<?php

namespace app\models\search;

use app\models\Languages;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequisitesFields as RequisitesFieldsModel;

/**
 * RequisitesFields represents the model behind the search form about `app\models\RequisitesFields`.
 */
class RequisitesFields extends RequisitesFieldsModel
{
    public function rules()
    {
        return [
            [['id', 'type', 'requisite_group_id'], 'integer'],
            [['code', 'variants', 'name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param null $lang
     * @return ActiveDataProvider
     */
    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = RequisitesFieldsModel::find()->translate($lang);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'name' => $this->name,
            'requisite_group_id' => $this->requisite_group_id,
        ]);

        $query->andFilterWhere(['ilike', 'code', $this->code])
            ->andFilterWhere(['ilike', 'variants', $this->variants])
            ->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
