<?php

namespace app\models\search;

use app\models\Languages;
use app\models\Translations;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Templates as TemplatesModel;

/**
 * Templates represents the model behind the search form about `app\models\Templates`.
 */
class Templates extends TemplatesModel
{

    public function rules()
    {
        return [
            [['id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'type', 'subject', 'body', 'handler'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $lang = null)
    {
        if($lang == null)
            $lang = Languages::getDefault();
        $query = TemplatesModel::find()->translate($lang);

        $query->addSelect('((SELECT handler FROM templates_handlers WHERE template_id = templates.id LIMIT 1)) AS handler');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);



        if (!empty($this->handler)) {
//            $query->leftJoin(TemplatesHandlers::tableName(),['template_id'=>'templates.id'])
//                ->andWhere(['handler' => $this->handler]);

            $templateId = TemplatesHandlers::find()->select(['template_id'])
                ->where(['handler' => $this->handler])
                ->asArray()
                ->column();
            $query->andWhere(['in', 'id', array_unique($templateId)]);
        }

        if (!empty($this->name)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['ilike', 'value',  $this->name])
                ->orFilterWhere(['ilike', 'value',  $this->name])
                ->andWhere('"key" ILIKE :table', [':table'=>"templates.name.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $query->andWhere(['in', 'id', array_unique($translate)]);
        }
        if (!empty($this->subject)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['ilike', 'value',  $this->subject])
                ->orFilterWhere(['ilike', 'value',  $this->subject])
                ->andWhere('"key" ILIKE :table', [':table'=>"templates.subject.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $query->andWhere(['in', 'id', array_unique($translate)]);
        }
        if (!empty($this->body)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['ilike', 'value',  $this->body])
                ->orFilterWhere(['ilike', 'value',  $this->body])
                ->andWhere('"key" ILIKE :table', [':table'=>"templates.body.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $query->andWhere(['in', 'id', array_unique($translate)]);
        }



        $query//->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'type', $this->type]);
            //->andFilterWhere(['ilike', 'subject', $this->subject])
            //->andFilterWhere(['ilike', 'body', $this->body]);

        return $dataProvider;
    }
}
