<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payment_gateways".
 *
 * @property int $id
 * @property string $name
 * @property int $demo
 * @property int $type
 * @property string $logo
 * @property string $countries
 * @property string $currencies
 * @property int $created_at
 * @property int $updated_at
 *
 * @property string $provider
 * @property string $provider_config
 * @property int $document_template_id
 *
 * @property FinancialOperations[] $financialOperations
 * @property DocumentsTemplate $documentTemplate
 */
class PaymentGateways extends \yii\db\ActiveRecord
{
    const TYPE_CARD = 0;
    const TYPE_BILL = 1;
    const TYPE_RECEIPT = 2;

    const TYPES = [
        self::TYPE_CARD => 'Card',
        self::TYPE_BILL => 'Bill',
        self::TYPE_RECEIPT => 'Receipt',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_gateways';
    }

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['demo', 'created_at', 'updated_at', 'document_template_id'], 'default', 'value' => null],
            [['demo', 'created_at', 'updated_at', 'type', 'document_template_id'], 'integer'],
            [['type'],'default','value'=>0],
            [['logo', 'countries', 'currencies', 'provider_config'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['provider'], 'string', 'max' => 250],
            [['document_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsTemplate::className(), 'targetAttribute' => ['document_template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'demo' => 'Demo',
            'logo' => 'Logo',
            'countries' => 'Countries',
            'currencies' => 'Currencies',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'provider' => 'Provider',
            'provider_config' => 'Provider Config',
            'document_template_id' => 'Document Template ID',
        ];
    }

    public function upload()
    {

        if (isset($this->imageFile)) {
            if ($this->validate()) {
                if(!file_exists('storage/payment-gateways'))
                    mkdir('storage/payment-gateways');
                $this->imageFile->saveAs('storage/payment-gateways/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
                $this->logo = 'storage/payment-gateways/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                return true;
            } else {
                return false;
            }
        }else {
            $this->logo = $this->getOldAttribute('logo');
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialOperations()
    {
        return $this->hasMany(FinancialOperations::className(), ['payment_gateway_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTemplate()
    {
        return $this->hasOne(DocumentsTemplate::className(), ['id' => 'document_template_id']);
    }
}
