<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ChangePasswordForm extends Model
{
    public $password;
    public $confirm_password;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['password', 'confirm_password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->password !== $this->confirm_password)
                $this->addError($attribute, 'Passwords do not match');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function changePassword()
    {
        if ($this->validate()) {
            if ($this->changePasswordFromAuth0()) {
                $this->user->setPassword($this->password);
                $this->user->removePasswordResetToken();
                $this->user->save();

                return $this->getUser();
            }

            return false;
        }

        return false;
    }

    public function setUser(User $user)
    {
        $this->_user = $user;
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function changePasswordFromAuth0()
    {
        $client = Yii::$app->auth0->management_api_client;

        $user_data = $client->users->update($this->user->auth->source_id, [
            'connection' => 'Username-Password-Authentication',
            'password' => $this->password
        ]);

        return (isset($user_data['user_id']) ? $user_data['user_id'] : false);
    }
}
