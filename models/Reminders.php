<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reminders".
 *
 * @property int                 $id
 * @property string              $type
 * @property string              $name
 * @property int                 $created_at
 * @property int                 $updated_at
 * @property int                 $created_by
 * @property int                 $updated_by
 *
 * @property OrderReminders[]    $orderReminders
 * @property \yii\db\ActiveQuery $updated
 * @property \yii\db\ActiveQuery $created
 * @property RemindersTenant[]   $reminderTenant
 */
class Reminders extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';

    const TYPE_SYSTEM = 'system';
    const TYPE_PUBLIC = 'public';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reminders';
    }

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['type', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderReminders()
    {
        return $this->hasMany(OrderReminders::className(), ['reminder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminderTenant()
    {
        return $this->hasOne(RemindersTenant::className(), ['reminder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
