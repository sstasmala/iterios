<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;

/**
 * This is the model class for table "requisites_fields".
 *
 * @property int $id
 * @property int $type
 * @property string $code
 * @property string $variants
 * @property string $name
 * @property string $field_placeholder
 * @property string $field_description
 * @property int $requisite_group_id
 *
 * @property Requisites[] $requisites
 * @property RequisitesGroups $requisiteGroup
 */
class RequisitesFields extends BaseTranslationModel
{
    const TRANSLATABLE = ['name','field_placeholder','field_description'];
    protected $primaryKey = 'id';

    const TYPE_VARCHAR = 0;
    const TYPE_TEXT = 1;
    const TYPE_NUMBER = 2;
    const TYPE_LINK = 3;
    const TYPE_IMAGE = 4;
    const TYPE_DROPDOWN = 5;
    const TYPE_MULTISELECT = 6;
    const TYPE_DATE = 7;
    const TYPE_CHECKBOX = 8;
    const TYPE_RADIOBUTTON = 9;
    const TYPE_EMAIL = 10;
    const TYPE_PHONE = 11;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'code','name','requisite_group_id'], 'required'],
            [['type', 'requisite_group_id'], 'default', 'value' => null],
            [['type', 'requisite_group_id'], 'integer'],
            [['variants'], 'string'],
            [['code'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
            [['field_placeholder','field_description'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['requisite_group_id'], 'exist', 'skipOnError' => true, 'targetClass' =>
                RequisitesGroups::className(), 'targetAttribute' => ['requisite_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'code' => 'Code',
            'variants' => 'Variants',
            'name' => 'Name',
            'field_placeholder' => 'Field Placeholder',
            'field_description' => 'Field Description',
            'requisite_group_id' => 'Requisite Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisites()
    {
        return $this->hasMany(Requisites::className(), ['requisites_field_id' => 'id']);
    }

    public static function getTypesLabels()
    {
        $labels = [
            RequisitesFields::TYPE_VARCHAR => 'varchar',
            RequisitesFields::TYPE_TEXT => 'text',
            RequisitesFields::TYPE_NUMBER => 'number',
            RequisitesFields::TYPE_LINK => 'link',
            RequisitesFields::TYPE_IMAGE => 'image',
            RequisitesFields::TYPE_DROPDOWN => 'dropdown',
            RequisitesFields::TYPE_MULTISELECT => 'multiselect',
            RequisitesFields::TYPE_DATE => 'date',
            RequisitesFields::TYPE_CHECKBOX => 'checkbox',
            RequisitesFields::TYPE_RADIOBUTTON => 'radiobutton',
            RequisitesFields::TYPE_EMAIL => 'email',
            RequisitesFields::TYPE_PHONE => 'phone',
        ];
        return $labels;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisiteGroup()
    {
        return $this->hasOne(RequisitesGroups::className(), ['id' => 'requisite_group_id']);
    }

    public static function getTypeName($type)
    {
        switch ($type) {
            case RequisitesFields::TYPE_VARCHAR:
                return 'varchar';
            case RequisitesFields::TYPE_TEXT:
                return 'text';
            case RequisitesFields::TYPE_NUMBER:
                return 'number';
            case RequisitesFields::TYPE_LINK:
                return 'link';
            case RequisitesFields::TYPE_IMAGE:
                return 'image';
            case RequisitesFields::TYPE_DROPDOWN:
                return 'dropdown';
            case RequisitesFields::TYPE_MULTISELECT:
                return 'multiselect';
            case RequisitesFields::TYPE_DATE:
                return 'date';
            case RequisitesFields::TYPE_CHECKBOX:
                return 'checkbox';
            case RequisitesFields::TYPE_RADIOBUTTON:
                return 'radiobutton';
            case RequisitesFields::TYPE_EMAIL:
                return 'email';
            case RequisitesFields::TYPE_PHONE:
                return 'phone';
        }
    }
}