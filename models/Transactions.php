<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property string $sum
 * @property int $action
 * @property int $tariff_id
 * @property int $financial_operation_id
 * @property int $tariff_order_id
 * @property int $owner
 * @property int $date
 * @property int $created_at
 * @property int $updated_at
 * @property int $tenant_id
 *
 * @property FinancialOperations $financialOperation
 * @property Tariffs $tariff
 * @property TariffsOrders $tariffOrder
 * @property Tenants $tenant
 */
class Transactions extends \yii\db\ActiveRecord
{

    const ACTION_CHARGE = 0;
    const ACTION_WRITE_OFF = 1;
    const ACTION_REFUND = 2;

    const ACTIONS = [
        self::ACTION_CHARGE => 'Charge',
        self::ACTION_WRITE_OFF => 'Write-off',
        self::ACTION_REFUND => 'Refund'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum'], 'number'],
            [['action', 'tariff_id', 'financial_operation_id', 'tariff_order_id', 'owner', 'date', 'created_at', 'updated_at', 'tenant_id'], 'default', 'value' => null],
            [['action', 'tariff_id', 'financial_operation_id', 'tariff_order_id', 'owner', 'date', 'created_at', 'updated_at', 'tenant_id'], 'integer'],
            [['financial_operation_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancialOperations::className(), 'targetAttribute' => ['financial_operation_id' => 'id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['tariff_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => TariffsOrders::className(), 'targetAttribute' => ['tariff_order_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'action' => 'Action',
            'tariff_id' => 'Tariff ID',
            'financial_operation_id' => 'Financial Operation ID',
            'tariff_order_id' => 'Tariff Order ID',
            'owner' => 'Owner',
            'date' => 'Date',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialOperation()
    {
        return $this->hasOne(FinancialOperations::className(), ['id' => 'financial_operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffOrder()
    {
        return $this->hasOne(TariffsOrders::className(), ['id' => 'tariff_order_id']);
    }

    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }
}
