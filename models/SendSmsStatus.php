<?php

namespace app\models;

use Yii;


class SendSmsStatus extends \yii\db\ActiveRecord
{
    const DELIVERY_STATUS = array(
        'send'      => '1',
        'delivered' => '2',
        'error'     => '0',
    );

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'send_sms_status';
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            $delivered_data = Contacts::find()
            ->select([
                'CONCAT(contacts.last_name, \' \', contacts.first_name) as fio',
                'contacts_phones.value as phones',
            ])
            ->leftJoin(ContactsPhones::tableName(), "contacts.id = contacts_phones.contact_id")
            ->where(['contacts_phones.id' => $this->contact_phone_id]);

            $delivered_data = $delivered_data->asArray()->all();
            $delivered_data = json_encode($delivered_data);

            $this->data = $delivered_data;
            return true;
        }
    return false;
}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['send_sms_id', 'contact_phone_id', 'status'], 'default', 'value' => null],
            [['send_sms_id', 'contact_phone_id', 'status','time_send'], 'integer'],
            [['segments', 'provider_sms_id', 'data'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'send_sms_id' => 'Send Sms ID',
            'contact_phone_id' => 'Contact ID',
            'status' => 'Status',
            'time_send' => 'Data',
            'provider_sms_id' => 'Sms ID',
            'segments' => 'Segments',
            'data' => 'Data'
        ];
    }

    public function getSendSms()
    {
        return $this->hasOne(SendSms::className(), ['id' => 'send_sms_id']);
    }

    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    public function getPhone()
    {
        return $this->hasOne(ContactsPhones::className(), ['contact_id' => 'contact_phone_id']);
    }
}
