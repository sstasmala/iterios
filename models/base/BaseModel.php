<?php
/**
 * BaseModel.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\base;


use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{

}