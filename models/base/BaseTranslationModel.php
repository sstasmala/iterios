<?php
/**
 * BaseTranslationModel.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\base;


use app\models\Languages;
use app\models\scopes\TranslationsScope;
use app\models\Translations;
use app\utilities\behaviors\TranslateBehavior;

class BaseTranslationModel extends BaseModel
{
    const TRANSLATABLE = [];
    protected $primaryKey = null;
    private $saved = false;

    /**
     * @return array
     */
    public function getTranslatable()
    {
        return static::TRANSLATABLE;
    }

//    public function behaviors()
//    {
//
//        return [['class' => TranslateBehavior::className()]];
//    }

    /**
     * @param $lang
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Exception
     */
    public function saveWithLang($lang,$runValidation = true, $attributeNames = null)
    {
        if(is_null($this->primaryKey))
            throw new \Exception('Primary key is not set!');

        if(!$this->saved){
            if(!parent::save($runValidation, $attributeNames))
                return false;
            $this->saved = true;
        }


        $tableName  = static::class;
        $tableName = $tableName::tableName();
        $language = Languages::findOne(['iso'=>$lang]);
        if(is_null($language))
            throw new \Exception('Language not found!');

        foreach ($this->translatable as $v)
        {
            $key = $tableName.'.'.$v.'.'.$this->{$this->primaryKey};
            $model = Translations::findOne(['key'=>$key,'lang_id'=>$language->id]);
            if(is_null($model))
                $model = new Translations();
            if(!Translations::saveTranslation($key, $this->{$v}, $language, $model))
                return false;
        }
        return true;
    }

    /**
     * @param $lang
     * @return $this
     * @throws \Exception
     */
    public function translate($lang,$default = false)
    {
        if(is_null($this->primaryKey))
            throw new \Exception('Primary key is not set!');

        $tableName  = static::className();
        $tableName = $tableName::tableName();
        foreach ($this->translatable as $v)
        {
            $key = $tableName.'.'.$v.'.'.$this->{$this->primaryKey};
            $this->{$v} = Translations::getTranslation($key,$lang,$default);
        }

        return $this;
    }

    /**
     * @param $data
     * @return bool
     */
    private function trSave($data)
    {
        foreach ($data[$this->translatable[0]] as $lang => $l)
        {
            $n_data = [];
            foreach ($this->attributes as $k=>$v)
            {
                if(in_array($k,$this->translatable)) {
                    if (isset($data[$k][$lang]))
                        $n_data[$k] = $data[$k][$lang];
                }
                else
                {
                    if (isset($data[$k]))
                        $n_data[$k] = $data[$k];
                }
            }
            $this->setAttributes($n_data);
            if(!$this->saveWithLang($lang))
                return false;
        }
        return true;
    }

    /**
     * @param $data
     * @param null $formName
     * @return bool
     */
    public function loadWithLang($data,$formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data)) {
            return $this->trSave($data);
        } elseif (isset($data[$scope])) {
            return $this->trSave($data[$scope]);
        }
        return false;
    }

    public static function find()
    {
        return new TranslationsScope(get_called_class());
    }

    public function afterDelete()
    {
        $tableName  = static::className();
        $tableName = $tableName::tableName();

        foreach ($this->translatable as $v)
        {
            $key = $tableName.'.'.$v.'.'.$this->{$this->primaryKey};
            $tr = Translations::findAll(['key' => $key]);

            foreach ($tr as $t)
                $t->delete();
        }
        return parent::afterDelete();
    }
}