<?php

namespace app\models;

use app\components\notifications_now\NotificationActions;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use yii\helpers\Json;
/**
 * This is the model class for table "requests".
 *
 * @property int $id
 * @property int $contact_id
 * @property int $departure_date_start
 * @property int $departure_date_end
 * @property int $trip_duration_start
 * @property int $trip_duration_end
 * @property int $budget_currency
 * @property int $budget_from
 * @property int $budget_to
 * @property int $tourists_adult_count
 * @property int $tourists_children_count
 * @property string $tourists_children_age
 * @property int $request_type_id
 * @property string $request_description
 * @property int $request_status_id
 * @property int $request_canceled_reason_id
 * @property int $responsible_id
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property RequestHotelCategories[] $requestHotelCategories
 * @property Contacts $contact
 * @property RequestCanceledReasons $requestCanceledReason
 * @property RequestStatuses $requestStatus
 * @property RequestType $requestType
 * @property User $responsible
 */
class Requests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id'], 'required'],
            [['departure_date_start', 'departure_date_end', 'trip_duration_start', 'trip_duration_end', 'budget_currency', 'budget_from', 'budget_to', 'tourists_adult_count', 'tourists_children_count', 'request_type_id', 'request_status_id', 'request_canceled_reason_id', 'responsible_id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['contact_id', 'departure_date_start', 'departure_date_end', 'trip_duration_start', 'trip_duration_end', 'budget_currency', 'budget_from', 'budget_to', 'tourists_adult_count', 'tourists_children_count', 'request_type_id', 'request_status_id', 'request_canceled_reason_id', 'responsible_id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['request_description'], 'string'],
            [['tourists_children_age'], 'string', 'max' => 255],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['request_canceled_reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestCanceledReasons::className(), 'targetAttribute' => ['request_canceled_reason_id' => 'id']],
            [['request_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestStatuses::className(), 'targetAttribute' => ['request_status_id' => 'id']],
            [['request_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestType::className(), 'targetAttribute' => ['request_type_id' => 'id']],
            [['responsible_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_id' => 'Contact ID',
            'departure_date_start' => 'Departure Date Start',
            'departure_date_end' => 'Departure Date End',
            'trip_duration_start' => 'Trip Duration Start',
            'trip_duration_end' => 'Trip Duration End',
            'budget_currency' => 'Budget Currency',
            'budget_from' => 'Budget From',
            'budget_to' => 'Budget To',
            'tourists_adult_count' => 'Tourists Adult Count',
            'tourists_children_count' => 'Tourists Children Count',
            'tourists_children_age' => 'Tourists Children Age',
            'request_type_id' => 'Request Type ID',
            'request_description' => 'Request Description',
            'request_status_id' => 'Request Status ID',
            'request_canceled_reason_id' => 'Request Canceled Reason ID',
            'responsible_id' => 'Responsible ID',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestHotelCategories()
    {
        return $this->hasMany(RequestHotelCategories::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestCountries()
    {
        return $this->hasMany(RequestCountries::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestCities()
    {
        return $this->hasMany(RequestCities::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestCanceledReason()
    {
        return $this->hasOne(RequestCanceledReasons::className(), ['id' => 'request_canceled_reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatus()
    {
        return $this->hasOne(RequestStatuses::className(), ['id' => 'request_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(RequestType::className(), ['id' => 'request_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        // on change date_of_birth
        if (in_array('departure_date_end', array_keys($changedAttributes))) {

            $model = NotificationsTrigger::findOne(['action' => BaseNotifications::REQUESTS_DAYS_COMPLETE, 'element_id' => $this->id]);
            if (empty($model)) {
                $model = new NotificationsTrigger();
                $model->created_at = time();
                $model->action = BaseNotifications::REQUESTS_DAYS_COMPLETE;
                $model->element_id = $this->id;
            }

            // save to process check
            $model->data = Json::encode([]);
            $model->agent_id = $this->responsible_id;
            $model->contact_id = $this->contact_id;
            $model->tenant_id = $this->tenant_id;
            $model->status = 0;
            $model->type = 0;
            $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->departure_date_end));
            $model->trigger_day = date('Y.m.d', $this->departure_date_end);

            $model->updated_at = time();
            $model->save();
        }

        if (in_array('request_status_id', array_keys($changedAttributes))) {

            $model = NotificationsTrigger::findOne(['action' => BaseNotifications::REQUESTS_STATUSES_ID, 'element_id' => $this->id]);
            if (empty($model)) {
                $model = new NotificationsTrigger();
                $model->created_at = time();
                $model->action = BaseNotifications::REQUESTS_STATUSES_ID;
                $model->element_id = $this->id;
            }

            // save to process check
            $model->data = Json::encode([]);
            $model->value_int = $this->request_status_id;
            $model->agent_id = $this->responsible_id;
            $model->contact_id = $this->contact_id;
            $model->tenant_id = $this->tenant_id;
            $model->status = 0;
            $model->type = 1;
            $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->departure_date_end));
            $model->trigger_day = date('Y.m.d', $this->departure_date_end);
            $model->updated_at = time();
            $model->save();

            // action (class), id contact, id tag
            $action = new NotificationActions(NotificationActions::REQUEST_CHANGE_STATUS, $this->id, $this->request_status_id);
            $action->send();
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {

        $where = ['action' => BaseNotifications::REQUESTS_DAYS_COMPLETE, 'element_id' => $this->id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }
        unset($trigger); unset($where);

        $where = ['action' => BaseNotifications::REQUESTS_STATUSES_ID, 'element_id' => $this->id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }

}
