<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_service_fields_view".
 *
 * @property int $id
 * @property string $value
 * @property string $type
 */
class OrdersServiceFieldsView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_service_fields_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['value'], 'string'],
            [['type'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'type' => 'Type',
        ];
    }
}
