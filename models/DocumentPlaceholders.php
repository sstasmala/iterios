<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use app\helpers\MCHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "document_placeholders".
 *
 * @property int $id
 * @property int $field
 * @property string $short_code
 * @property string $module
 * @property string $path
 * @property string $default
 * @property boolean $fake
 * @property string  $description
 */
class DocumentPlaceholders extends BaseTranslationModel
{
    const TRANSLATABLE = ['short_code'];
    const USED_MODULES = ['Requisites', 'ServicesFields', 'Other'];
    const MODULE_REQUISITE = 'Requisites';
    CONST MODULE_SERVICE = 'ServicesFields';

    protected $primaryKey = 'id';


    public static function tableName()
    {
        return 'document_placeholders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short_code', 'module', 'path', 'default'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['fake'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_code' => 'Short Code',
            'module' => 'Module',
            'path' => 'Path',
            'default' => 'Default',
            'fake' => 'Fake placeholder',
            'description' => 'Description'
        ];
    }

    public function beforeSave($insert)
    {
        $result =  parent::beforeSave($insert);
        $this->short_code = str_replace(' ','_', $this->short_code);

        if ($this->fake) {
            $this->module = null;
            $this->path = null;
            $this->default = null;
        }

        return $result;
    }

    public function getModules() {
        $modules_map=[];
        $modules = self::USED_MODULES;
        foreach ($modules as $value) $modules_map[$value]=$value;
        return $modules_map;
    }

    public function getField(){
            switch ($this->module){
                case 'Requisites':
                    return RequisitesFields::find()->where(['id'=>$this->path])->one();
                    break;
                case 'ServicesFields':
                    return ServicesFieldsDefault::find()->where(['id'=>$this->path])->one();
                    break;
            }
    }

    public function getAllPath($module){
        if (in_array($module,self::USED_MODULES) && $module !== 'Other') {
            $class = 'app\models\\' . $module;
            $results_tmp = $class::find()->all();

            $result_unique = [];

            foreach ($results_tmp as $model){
                switch ($module){
                    case 'Requisites':
                        $result_unique[$model->requisitesField->id] = $model->requisitesField->code . '/' . $model->requisitesField->name;
                        break;
                    case 'ServicesFields':
                        $result_unique[$model->field->id] = $model->field->code .'/'.$model->field->name;
                        break;
                }
            }

            return array_unique($result_unique);
        }

        return [];
    }

    public static function getShortcodes()
    {
        $models = self::find()->asArray()->all();

        return ArrayHelper::map($models,'short_code', 'default');
    }


}

