<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_services_links".
 *
 * @property int $id
 * @property int $order_id
 * @property int $service_id
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $adults_count
 * @property int $children_count
 * @property int $due_date
 * @property int $status
// * @property string $ex_rate
 *
 * @property OrdersServicesAdditionalLinks[] $ordersServicesAdditionalLinks
 * @property Orders $order
 * @property Services $service
 * @property Tenants $tenant
 * @property OrdersServicesValues[] $ordersServicesValues
 */
class OrdersServicesLinks extends \yii\db\ActiveRecord
{
    const MAIN_ITEM = 'order_id';

    const STATUS_WAIT = 1;
    const STATUS_NOT_ACCEPTED = 0;
    const STATUS_ACCEPTED = 2;
    const STATUS_CANCELED = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_services_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'service_id', 'tenant_id'], 'required'],
            [['order_id', 'service_id', 'tenant_id', 'due_date', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['order_id', 'service_id','status', 'tenant_id','due_date', 'created_at', 'updated_at', 'created_by', 'updated_by', 'adults_count', 'children_count'], 'integer'],
//            [['ex_rate'],'number'],
//            [['ex_rate'],'default','value'=>1.00],
            [['status'],'default','value'=>0],
            [['adults_count', 'children_count','status'],'default','value'=>0],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'service_id' => 'Service ID',
            'tenant_id' => 'Tenant ID',
            'adults_count' => 'adults_count',
            'children_count' => 'children_count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersServicesAdditionalLinks()
    {
        return $this->hasMany(OrdersServicesAdditionalLinks::className(), ['link_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersServicesValues()
    {
        return $this->hasMany(OrdersServicesValues::className(), ['link_id' => 'id']);
    }
}
