<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests_services_additional_links".
 *
 * @property int $id
 * @property int $link_id
 * @property int $type
 * @property int $service_id
 *
 * @property RequestsServicesLinks $link
 * @property Services $service
 * @property RequestsServicesValues[] $requestsServicesValues
 */
class RequestsServicesAdditionalLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests_services_additional_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'service_id'], 'required'],
            [['link_id', 'service_id'], 'default', 'value' => null],
            [['link_id', 'service_id','type'], 'integer'],
            [['type'], 'default', 'value' => 0],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestsServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'type' => 'Type',
            'service_id' => 'Service ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(RequestsServicesLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsServicesValues()
    {
        return $this->hasMany(RequestsServicesValues::className(), ['additional_link_id' => 'id']);
    }
}
