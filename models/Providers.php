<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "segments".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $code               [varchar(255)]
 * @property string $logo               [varchar(255)]
 * @property string $provider_type_id   [integer]
 * @property string $description
 * @property string $website            [varchar(255)]
 * @property string $authorization_type [varchar(255)]
 * @property string $provider_class     [varchar(255)]
 * @property string $functions          [varchar(255)]
 * @property string $additional_config  [varchar(255)]
 */
class Providers extends BaseTranslationModel
{
    const TRANSLATABLE = [
        'name',
        'description'
    ];

    const AUTHORIZATION_TYPES = [
        'login&pass',
        'api-token',
    ];

    const ADDITIONAL_CONFIG = [
        'ip',
        'alphaName',
        'fromName',
        'fromEmail'
    ];

    const CLASS_PROVIDER = [
        'app\components\sms_provider\SmsClubXML',
    ];

    protected $primaryKey = 'id';

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    public static function tableName()
    {
        return 'providers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provider_type_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'functions', 'additional_config', 'logo'], 'default', 'value' => null],
            [['provider_type_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['name', 'code', 'logo', 'website', 'authorization_type', 'provider_class', 'functions', 'additional_config'], 'string', 'max' => 255],
            [['provider_class'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'logo' => 'Logo',
            'provider_type_id' => 'Provider Type',
            'functions' => 'Provider Function',
            'description' => 'Description',
            'website' => 'Website',
            'additional_config' => 'Additional Config',
            'provider_class' => 'Provider Class',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCredential()
    {
        return $this->hasMany(ProvidersCredentials::className(), ['provider_id' => 'id']);
    }

    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getType()
    {
        return $this->hasOne(ProviderType::className(), ['id' => 'provider_type_id']);
    }

    public static function getAuthorizationTypes()
    {
        $type_map = [];
        $types = self::AUTHORIZATION_TYPES;
        foreach ($types as $value) $type_map[$value] = $value;
        return $type_map;
    }

    public static function getAdditionalConfig()
    {
        $conf_map = [];
        $conf = self::ADDITIONAL_CONFIG;
        foreach ($conf as $value) $conf_map[$value] = $value;
        return $conf_map;
    }

    public static function getClassProvider()
    {
        $providers = Yii::$app->params['dispatch_providers'];
        $providers = \array_merge($providers['email'],$providers['sms']);
        return $providers;
//        $conf_map = [];
//        $conf = self::CLASS_PROVIDER;
//        foreach ($conf as $value) $conf_map[$value] = $value;
//        return $conf_map;
    }

}
