<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commercial_proposals_services".
 *
 * @property int $id
 * @property int $commercial_proposal_id
 * @property int $request_service_link_id
 *
 * @property CommercialProposals $commercialProposal
 * @property RequestsServicesLinks $requestServiceLink
 */
class CommercialProposalsServices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commercial_proposals_services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['commercial_proposal_id', 'request_service_link_id'], 'default', 'value' => null],
            [['commercial_proposal_id', 'request_service_link_id'], 'integer'],
            [['commercial_proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => CommercialProposals::className(), 'targetAttribute' => ['commercial_proposal_id' => 'id']],
            [['request_service_link_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestsServicesLinks::className(), 'targetAttribute' => ['request_service_link_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'commercial_proposal_id' => 'Commercial Proposal ID',
            'request_service_link_id' => 'Request Service Link ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommercialProposal()
    {
        return $this->hasOne(CommercialProposals::className(), ['id' => 'commercial_proposal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestServiceLink()
    {
        return $this->hasOne(RequestsServicesLinks::className(), ['id' => 'request_service_link_id']);
    }
}
