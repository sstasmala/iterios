<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requisites".
 *
 * @property int $id
 * @property int $country_id
 * @property int $requisites_group_id
 * @property int $group_position
 * @property int $requisites_field_id
 * @property int $field_position
 * @property int $in_header
 *
 * @property RequisitesFields $requisitesField
 * @property RequisitesGroups $requisitesGroup
 * @property RequisitesTenants[] $requisitesTenants
 */
class Requisites extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'requisites_group_id', 'group_position', 'requisites_field_id', 'field_position'], 'default', 'value' => null],
            [['country_id', 'requisites_group_id', 'group_position', 'requisites_field_id', 'field_position','in_header'], 'integer'],
            [['in_header'], 'default', 'value' => 0],
            [['requisites_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequisitesFields::className(), 'targetAttribute' => ['requisites_field_id' => 'id']],
            [['requisites_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequisitesGroups::className(), 'targetAttribute' => ['requisites_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'requisites_group_id' => 'Requisites Group',
            'group_position' => 'Group Position',
            'requisites_field_id' => 'Requisites Field',
            'field_position' => 'Field Position',
            'in_header' => 'In Header',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesField()
    {
        return $this->hasOne(RequisitesFields::className(), ['id' => 'requisites_field_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesGroup()
    {
        return $this->hasOne(RequisitesGroups::className(), ['id' => 'requisites_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesTenants()
    {
        return $this->hasMany(RequisitesTenants::className(), ['requisite_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesCurrentTenant(): \yii\db\ActiveQuery
    {
        return $this->hasMany(RequisitesTenants::className(), ['requisite_id' => 'id'])
            ->onCondition(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
    }
}
