<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sms_system_providers".
 *
 * @property int $id
 * @property string $provider
 * @property string $name
 * @property string $system_aname
 * @property string $public_aname
 * @property string $credentials_config
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class SmsSystemProviders extends \yii\db\ActiveRecord
{
    const ANAME_SYSTEM_TYPE = 'system';
    const ANAME_PUBLIC_TYPE = 'public';
    const ANAME_PERSONAL_TYPE = 'personal';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_system_providers';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['provider', 'name'], 'string', 'max' => 255],
            [['system_aname', 'public_aname'], 'string', 'max' => 11],
            [['system_aname', 'public_aname'], 'match', 'not' => true, 'pattern' => '/[^a-zA-Z 0-9]/', 'message' => 'Invalid characters, only latin letters, numbers and space.'],
            [['credentials_config'], 'string'],
            [['provider', 'credentials_config', 'system_aname', 'public_aname'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider' => 'Provider',
            'name' => 'Name',
            'system_aname' => 'System AlphaName',
            'public_aname' => 'Public AlphaName',
            'credentials_config' => 'Credentials Config',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
