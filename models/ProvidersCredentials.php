<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tasks_types".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string  $provider_id [integer]
 * @property string  $tenant_id   [integer]
 * @property string  $params      [varchar(255)]
 * @property string  $provider_data
 */
class ProvidersCredentials extends \yii\db\ActiveRecord
{
    const PARAMS = [
        'username',
        'password',
        'apiKey',
        'alphaName',
        'ip',
        'fromName',
        'fromEmail'
    ];

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'providers_credentials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by','provider_data','provider_errors'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'provider_id', 'tenant_id'], 'integer'],
            [['params','provider_data'], 'string', 'max' => 255],
            [['provider_data','provider_errors'], 'string'],
            [['credentialParams'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'params' => 'Params',
            'provider_id' => 'Provider',
            'tenant_id' => 'Tenant',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function setCredentialParams($value)
    {
        $this->params = json_encode($value);
    }

    public function getAlphaName()
    {
        $params = json_decode($this->params, 1);
        return isset($params['alphaName']) ? $params['alphaName'] : '';
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['id' => 'provider_id']);
    }

    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

}
