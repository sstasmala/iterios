<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "base_notifications".
 *
 * @property int $id
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property string $system_notification
 * @property string $sms
 * @property string $task_name
 * @property string $task_body
 * @property string $condition
 * @property string $trigger_time
 * @property string $module
 * @property int $trigger
 * @property int $is_email
 * @property int $is_system_notification
 * @property int $is_task
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class BaseNotifications extends BaseTranslationModel
{
    const TRANSLATABLE = ['name','body','subject','sms','system_notification','task_name','task_body'];
    protected $primaryKey = 'id';

    const CONTACTS = 'contacts';
    const AGENTS = 'agents';

    const MODULES = [
        self::CONTACTS =>'Contacts',
        self::AGENTS => 'Agents'
    ];

    const SUBSCRIBERS = [
        'all' => 'All',
        'rowner' => 'Record Owner'
    ];

    const CONTACTS_TAGS_TAG_ID = 'contacts_tags.tag_id';
    const CONTACTS_DATE_BIRTH = 'contacts.date_of_birth';
    const PASSPORT_DUE_DATE = 'contacts.passport_due_date';
    const VISA_DUE_DATE = 'contacts.visa_due_date';
    const REQUESTS_DAYS_COMPLETE = 'requests.days_complete';
    const REQUESTS_STATUSES_ID = 'requests_statuses.id';
    const DEPARTURE_DATE_START = 'orders.departure_date_start';
    const DEPARTURE_DATE_END = 'orders.departure_date_end';
    const TASKS_DAYS_COMPLETE = 'tasks.days_complete';
    const TASKS_OVERDUE = 'tasks.overdue';
    const HOLIDAYS_SYSTEM = 'holidays.system';


    const DEPARTURE_TIME_START = 'orders.departure_time_start';
    const DEPARTURE_TIME_END = 'orders.departure_time_end';

    const TRIGGERS = [
        self::CONTACTS_DATE_BIRTH => 'Contacts: birth date',
        self::REQUESTS_DAYS_COMPLETE => 'Requests: days complete',
        self::TASKS_DAYS_COMPLETE => 'Task: days complete',
        self::TASKS_OVERDUE => 'Tasks: Overdue',
        self::PASSPORT_DUE_DATE => 'Contacts: passport due date',
        self::VISA_DUE_DATE => 'Contacts: visa due date',
        self::DEPARTURE_DATE_START => 'Orders: departure date starting',
        self::DEPARTURE_DATE_END => 'Orders: departure date ending',
        self::HOLIDAYS_SYSTEM => "System Holidays"
    ];

    const TRIGGERS_PLACEHOLDERS = [
        self::CONTACTS_DATE_BIRTH => [
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Agent_Name}}',
            '{{Contact_BirthDay}}',
        ],
        self::REQUESTS_DAYS_COMPLETE => [
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Agent_Name}}',
            '{{Request_Link}}',
            '{{Request_Dates}}',
            '{{Request_Country}}',
        ],
        self::TASKS_DAYS_COMPLETE => [
            '{{Agent_Name}}',
            '{{Task_Link}}',
            '{{Task_Due_date_and_Time}}'
        ],
        self::PASSPORT_DUE_DATE => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Int_Pass_Number}}',
            '{{Int_Pass_Expiry}}'
        ],
        self::VISA_DUE_DATE => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Visa_Country}}',
            '{{Visa_Expiry}}'
        ],
        self::DEPARTURE_DATE_START => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Departure_Date}}',
            '{{Deprature_Time}}',
            '{{Order_Link}}'
        ],
        self::DEPARTURE_DATE_END => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Departure_Date}}',
            '{{Deprature_Time}}',
            '{{Order_Link}}'
        ],
        self::HOLIDAYS_SYSTEM => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Holiday_Name}}',
            '{{Holiday_Date}}'
        ],
        self::TASKS_OVERDUE => [
            '{{Agent_Name}}',
            '{{Contact_Name}}',
            '{{Contact_Link}}',
            '{{Agency_Logo}}',
            '{{Agency_Web}}',
            '{{Agency_Address}}',
            '{{Agency_Social}}',
            '{{Agency_Name}}',
            '{{Agency_Phone}}',
            '{{Agency_Web}}',
            '{{Overdue_Task_List}}',
            '{{Tasks_Link}}',
            '{{Task_Settings_Link}}'
        ]
    ];

    const TRIGGER_TYPE_W_TR = 0;
    const TRIGGER_TYPE_ONE_TIME = 1;
    const TRIGGER_TYPE_COUNT_DAYS_BEFORE = 2;
    const TRIGGER_TYPE_COUNT_DAYS_AFTER = 3;

    const TRIGGER_TYPES = [
            self::TRIGGER_TYPE_W_TR => 'When triggered',
            self::TRIGGER_TYPE_ONE_TIME => 'One time',
            self::TRIGGER_TYPE_COUNT_DAYS_BEFORE => 'Count days before',
            self::TRIGGER_TYPE_COUNT_DAYS_AFTER => 'Count days after'
    ];

    // user_type = 1 - CONTACT

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'base_notifications';
    }

    private function clearNotify() {
        // удалить все очереди
        NotificationsCourse:: deleteAll(['noting_id' => $this->id]);
        NotificationsTrigger::updateAll(['value' => ''], ['action' => self::TASKS_DAYS_COMPLETE, 'value' => $this->id]);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) return parent::afterSave($insert, $changedAttributes);

        $this->clearNotify();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() 
    {
        $this->clearNotify();

        return parent::afterDelete();
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'module'], 'required'],
            [['body', 'is_slave_contact', 'system_notification', 'sms', 'condition', 'trigger_time', 'task_body', 'trigger_action'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'trigger', 'is_email', 'is_system_notification', 'is_task', 'trigger_day'], 'default', 'value' => null],
            [['created_at', 'is_slave_contact', 'status', 'updated_at', 'created_by', 'updated_by', 'trigger', 'is_email', 'is_system_notification', 'is_task', 'trigger_day'], 'integer'],
            [['name', 'subject'], 'string', 'max' => 250],
            [['module'], 'string', 'max' => 100],
            [['task_name', 'subscribers'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'body' => 'Body',
            'system_notification' => 'System Notification',
            'sms' => 'Sms',
            'condition' => 'Condition',
            'module' => 'Module',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'trigger' => 'Trigger',
            'trigger_time' => 'Trigger Time',
            'task_name' => 'Task Name',
            'task_body' => 'Task Body',
            'is_email' => 'Is Email',
            'is_system_notification' => 'Is System Notification',
            'is_task' => 'Is Task',
            'trigger_day' => 'Trigger Day',
            'subscribers' => 'Subscribers',
            'trigger_action' => 'Trigger Action',
            'is_slave_contact' => 'Task Contact Slave',
            'status' => 'Status'
        ];
    }

    public function getTenantNotifications()
    {
        return $this->hasMany(TenantNotifications::className(), ['base_notification_id' => 'id']);
    }
}
