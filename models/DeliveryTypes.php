<?php

namespace app\models;

use app\models\base\BaseModel;
use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "delivery_types".
 *
 * @property int $id
 * @property string $for
 * @property string $type
 * @property string $name
 * @property int $updated_at
 * @property int $updated_by
 */
class DeliveryTypes extends BaseTranslationModel
{
    const FOR_EMAIL = 'email';
    const FOR_SMS = 'sms';

    const TYPE_EMAIL_OFFER = 'email_offer';
    const TYPE_EMAIL_LETTER = 'email_letter';
    const TYPE_EMAIL_DELIVERY = 'email_delivery';
    const TYPE_SMS_OFFER = 'sms_offer';
    const TYPE_SMS_SINGLE = 'sms_single';
    const TYPE_SMS_DELIVERY = 'sms_delivery';

    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_types';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['updated_at', 'updated_by'], 'default', 'value' => null],
            [['updated_at', 'updated_by'], 'integer'],
            [['for', 'type', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'for' => 'For',
            'type' => 'Type',
            'name' => 'Name',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
