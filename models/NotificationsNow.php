<?php

namespace app\models;
use app\models\base\BaseTranslationModel;

/**
 * This is the model class for table "notifications_now".
 *
 * @property int $id
 * @property string $name
 * @property string $class
 * @property int $is_email
 * @property int $is_notice
 * @property string $text_email
 * @property string $text_notice
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class NotificationsNow extends BaseTranslationModel
{
    const TRANSLATABLE = ['name', 'text_email', 'title_email', 'text_notice'];
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_now';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_email', 'is_notice', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['is_email', 'is_notice', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['text_email','title_email'], 'string'],
            [['name', 'class', 'text_notice'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'class' => 'Class',
            'is_email' => 'Is Email',
            'is_notice' => 'Is Notice',
            'title_email' => 'Title Email',
            'text_email' => 'Text Email',
            'text_notice' => 'Text Notice',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
