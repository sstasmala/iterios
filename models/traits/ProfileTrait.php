<?php

namespace app\models\traits;

use Yii;

trait ProfileTrait
{
    /**
     * @param array $array
     */
    protected function setParams(array $array)
    {
        foreach ($array as $keyParam => $valParam) {
            Yii::$app->view->params[$keyParam] = [$valParam => true];
        }
    }

    protected function setData(array $array){
        foreach ($array as $keyParam => $valParam) {
            $this->data[$keyParam] = $valParam;
        }
    }

}