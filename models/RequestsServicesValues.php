<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests_services_values".
 *
 * @property int $id
 * @property int $link_id
 * @property int $additional_link_id
 * @property int $service_id
 * @property int $field_id
 * @property string $value
 *
 * @property RequestsServicesAdditionalLinks $additionalLink
 * @property RequestsServicesLinks $link
 * @property Services $service
 * @property ServicesFieldsDefault $field
 */
class RequestsServicesValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests_services_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'service_id', 'field_id'], 'required'],
            [['link_id', 'additional_link_id', 'service_id', 'field_id'], 'default', 'value' => null],
            [['link_id', 'additional_link_id', 'service_id', 'field_id'], 'integer'],
            [['value'], 'string'],
            [['additional_link_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestsServicesAdditionalLinks::className(), 'targetAttribute' => ['additional_link_id' => 'id']],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestsServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicesFieldsDefault::className(), 'targetAttribute' => ['field_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'additional_link_id' => 'Additional Link ID',
            'service_id' => 'Service ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalLink()
    {
        return $this->hasOne(RequestsServicesAdditionalLinks::className(), ['id' => 'additional_link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(RequestsServicesLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id'])->translate(Yii::$app->user->identity->tenant->language->iso);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(ServicesFieldsDefault::className(), ['id' => 'field_id'])->translate(Yii::$app->user->identity->tenant->language->iso);
    }
}
