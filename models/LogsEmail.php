<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logs_email".
 *
 * @property int $id
 * @property string $subject
 * @property string $text
 * @property string $html
 * @property string $type
 * @property string $recipient
 * @property int $template_id
 * @property bool $conversion
 * @property string $status
 * @property string $reason
 * @property string $ip
 * @property string $city
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Templates $template
 */
class LogsEmail extends \yii\db\ActiveRecord
{
    // Statuses which set during sending message
    const STATUS_WAIT = 'wait';
    const STATUS_ERROR = 'error';
    const STATUS_SENT = 'sent';
    const STATUS_REJECTED = 'rejected';

    // Statuses which set in callback
    const STATUS_DELAY = 'delay';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_OPEN = 'open';
    const STATUS_CLICK = 'click';
    const STATUS_BOUNCE = 'bounce';

    const TYPE_SYSTEM = 'system';
    const TYPE_PUBLIC = 'public';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs_email';
    }

    /**
     * @return array
     */
    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'type', 'recipient', 'status'], 'required'],
            [['text', 'html', 'reason'], 'string'],
            [['template_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['template_id', 'created_at', 'updated_at'], 'integer'],
            [['conversion'], 'boolean'],
            [['subject', 'type', 'recipient', 'status', 'ip', 'city'], 'string', 'max' => 255],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Templates::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'text' => 'Text',
            'html' => 'Html',
            'type' => 'Type',
            'recipient' => 'Recipient',
            'template_id' => 'Template ID',
            'conversion' => 'Conversion',
            'status' => 'Status',
            'reason' => 'Reason',
            'ip' => 'Ip',
            'city' => 'City',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Templates::className(), ['id' => 'template_id']);
    }
}
