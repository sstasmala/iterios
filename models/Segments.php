<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "segments".
 *
 * @property int $id
 * @property string $name
 * @property string $modules
 * @property string $type
 * @property string $rules
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Segments extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];

    const USED_MODULES = ['Contacts'];

    const TYPE_SYSTEM = 'system';
    const TYPE_PUBLIC = 'public';

    protected $primaryKey = 'id';

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    public static function tableName()
    {
        return 'segments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'modules', 'type'], 'string', 'max' => 255],
            [['rules'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'modules' => 'Modules',
            'type' => 'Type',
            'rules' => 'Rules',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public static function getModules()
    {
        $modules_map = [];
        $modules = self::USED_MODULES;
        foreach ($modules as $value) $modules_map[$value] = $value;
        return $modules_map;
    }

    public static function getFilterWithModules($module)
    {
        $filterList = [];

        switch ($module) {
            case 'Contacts':
                $filterList = array(
                    array(
                        'id' => 'contacts_emails.value',
                        'label' => 'Email',
                        'type' => 'string',
                        'operators'=> ['not_contains', 'contains', 'begins_with', 'ends_with', 'equal'],
                    ),
                    array(
                        'id' => 'contacts_phones.value',
                        'label' => 'Phone',
                        'type' => 'string',
                        'operators'=> ['not_contains', 'contains', 'begins_with', 'ends_with', 'equal'],
                    ),
                    array(
                        'id' => 'contacts.created_at',
                        'label' => 'Date added',
                        'type' => 'datetime',
                        'plugin' => 'datetimepicker',
                        'plugin_config' => array(
                            'format' => 'YYYY-MM-DD',
                        ),
                        'operators'=> ['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
                    ),
                    array(
                        'id' => 'contacts.updated_at',
                        'label' => 'Date updated',
                        'type' => 'datetime',
                        'plugin' => 'datetimepicker',
                        'plugin_config' => array(
                            'format' => 'YYYY-MM-DD',
                        ),
                        'operators'=> ['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
                    ),
                    array(
                        'id' => 'count_date_added',
                        'field' => 'contacts.created_at',
                        'label' => 'Now - Date added (days)',
                        'type' => 'integer',
                        'operators'=> ['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
                    ),
                    array(
                        'id' => 'count_date_updated',
                        'field' => 'contacts.updated_at',
                        'label' => 'Now - Date updated (days)',
                        'type' => 'integer',
                        'operators'=> ['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
                    ),
                );
        }
        return $filterList;
    }



}
