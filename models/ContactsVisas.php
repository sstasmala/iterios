<?php

namespace app\models;

use app\models\log\BaseLogModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contacts_visas".
 *
 * @property int $id
 * @property int $type_id
 * @property string $country
 * @property int $contact_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $begin_date
 * @property int $end_date
 * @property string $description
 *
 * @property Contacts $contact
 * @property VisasTypes $type
 */
class ContactsVisas extends BaseLogModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts_visas';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'begin_date', 'end_date'], 'default', 'value' => null],
            [['type_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'begin_date', 'end_date'], 'integer'],
            [['description'], 'string'],
            [['country'], 'string', 'max' => 255],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => VisasTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'country' => 'Country',
            'contact_id' => 'Contact ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'begin_date' => 'Begin Date',
            'end_date' => 'End Date',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(VisasTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     *
     * @return int|void
     */
    public function afterSave($insert, $changedAttributes)
    {

        // on change date_of_birth
        if (!array_key_exists('end_date', $changedAttributes))
            return parent::afterSave($insert, $changedAttributes);

        $model = NotificationsTrigger::findOne(['action' => BaseNotifications::VISA_DUE_DATE, 'contact_id' => $this->contact_id, 'element_id' => $this->id]);

        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = BaseNotifications::VISA_DUE_DATE;
            $model->element_id = $this->id;
        }

        $contact = Contacts::find()->where(['id' => $this->contact_id])->one();

        $model->agent_id = $contact->responsible;
        $model->tenant_id = $contact->tenant_id;
        $model->contact_id = $this->contact_id;
        $model->status = 0;
        $model->type = 0;
        $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->end_date));
        $model->trigger_day = date('Y.m.d', $this->end_date);
        $model->data = '[]'; //  to do adding data
        $model->updated_at = time();
        $model->save();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        $where = ['action' => BaseNotifications::VISA_DUE_DATE, 'contact_id' => $this->contact_id, 'element_id' => $this->id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }
}
