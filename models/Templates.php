<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use app\models\TemplatesHandlers;
use Yii;

/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property string $type
 * @property string $subject
 * @property string $body
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Tenants $tenant
 */
class Templates extends BaseTranslationModel
{
    const TRANSLATABLE = ['subject','name','body'];
    protected $primaryKey = 'id';
    public $handler;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['subject', 'body'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['type'], 'string', 'max' => 100],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'tenant_id' => 'Tenant ID',
            'type' => 'Type',
            'subject' => 'Subject',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

}
