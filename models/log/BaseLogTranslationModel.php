<?php

namespace app\models\log;

use app\models\base\BaseTranslationModel;
use app\models\Languages;
use app\models\Log;
use app\models\Translations;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class BaseLogTranslationModel extends BaseTranslationModel
{
    private $_oldAttributes;

    private $afterItem = null;
    private $previousItem = null;
    private $data = null;
    public $log_id = null;
    public $group_id = null;
    public $force_update = false;
    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!is_null($this->afterItem)) {
            return self::beforeSave($insert);
        }
        if(!$insert) {
            $this->previousItem = self::className()::findOne($this->id);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->afterItem = self::className()::findOne($this->id);
        return $this->createLog();
    }

    public function beforeDelete()
    {
        $this->data = ArrayHelper::toArray(self::className()::findOne($this->id));
//        $this->createDeleteLog();
//        return parent::beforeDelete();
        $this->previousItem = self::className()::findOne($this->id);
        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        parent::afterDelete();
        return $this->createDeleteLog();
    }

    private function createLog()
    {
        if (is_null($this->afterItem)) {
            return true;
        }
        $jsonData = ['previous' => null, 'after' => null];
        $afterItem = ArrayHelper::toArray(self::className()::findOne($this->id));
        if(!is_null($this->previousItem)) {
            $previousItem = ArrayHelper::toArray($this->previousItem);
            $previousItemDiff = array_diff_assoc($previousItem, $afterItem);
            $afterItemDiff = array_diff_assoc($afterItem, $previousItem);
        } else {
            $previousItemDiff = null;
            $afterItemDiff = $afterItem;
        }
        if(empty($previousItemDiff) && empty($afterItemDiff) && !$this->force_update) {
            return true;
        }
        $log = new Log();

        if(is_null($previousItemDiff)) {
            $log['action'] = 'create';
        } else {
            $log['action'] = 'update';
        }

        $jsonData['previous'] = empty($previousItemDiff) ? null : $previousItemDiff;
        $jsonData['after'] = empty($afterItemDiff) ? null : $afterItemDiff;
        $jsonData = json_encode($jsonData);
        if (isset($this->log_id)) {
            $log['group_id'] = $this->log_id;
        }
        $log['item_id'] = $this->id;
        $log['table_name'] = static::tableName();
        $log['tenant_id'] = \Yii::$app->user->identity->tenant->id;
        $log['data'] = $jsonData;
        $log['created_at'] = time();
        $log['created_by'] = \Yii::$app->user->id;
        $log->save();
        $this->log_id = $log->id;
        return $log->id;
    }

    private function createDeleteLog()
    {
        if (!isset(Yii::$app->user))
            return false;

        $jsonData = ['previous' => null, 'after' => null];
        $jsonData['previous'] = $this->data;
        $jsonData['after'] = null;
        $jsonData = json_encode($jsonData);

        $log = new Log();

        $log['item_id'] = $this->id;
        $log['table_name'] = static::tableName();
        $log['tenant_id'] = \Yii::$app->user->identity->tenant->id;
        $log['action'] = 'delete';
        $log['data'] = $jsonData;
        $log['created_at'] = time();
        $log['created_by'] = \Yii::$app->user->id;
        $log->save();
        return $log->id;
    }

    protected function insertInternal($attributes = null)
    {
        if (!$this->beforeSave(true)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (($primaryKeys = static::getDb()->schema->insert(static::tableName(), $values)) === false) {
            return false;
        }
        foreach ($primaryKeys as $name => $value) {
            $id = static::getTableSchema()->columns[$name]->phpTypecast($value);
            $this->setAttribute($name, $id);
            $values[$name] = $id;
        }

        $changedAttributes = array_fill_keys(array_keys($values), null);
        $this->setOldAttributes($values);
        return $this->afterSave(true, $changedAttributes);;
    }

    public function insert($runValidation = true, $attributes = null)
    {
        if ($runValidation && !$this->validate($attributes)) {
            Yii::info('Model not inserted due to validation error.', __METHOD__);
            return false;
        }

        if (!$this->isTransactional(self::OP_INSERT)) {
            return $this->insertInternal($attributes);
        }

        $transaction = static::getDb()->beginTransaction();
        try {
            $result = $this->insertInternal($attributes);
            if ($result === false) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }

            return $result;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->getIsNewRecord()) {
            return $this->insert($runValidation, $attributeNames);
        }

        return $this->update($runValidation, $attributeNames);// !== false;
    }
    public function update($runValidation = true, $attributeNames = null)
    {
        if ($runValidation && !$this->validate($attributeNames)) {
            Yii::info('Model not updated due to validation error.', __METHOD__);
            return false;
        }

        if (!$this->isTransactional(self::OP_UPDATE)) {
            return $this->updateInternal($attributeNames);
        }

        $transaction = static::getDb()->beginTransaction();
        try {
            $result = $this->updateInternal($attributeNames);
            if ($result === false) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }

            return $result;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    protected function updateInternal($attributes = null)
    {
        if (!$this->beforeSave(false)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (empty($values)) {

            return $this->afterSave(false, $values);
        }
        $condition = $this->getOldPrimaryKey(true);
        $lock = $this->optimisticLock();
        if ($lock !== null) {
            $values[$lock] = $this->$lock + 1;
            $condition[$lock] = $this->$lock;
        }
        // We do not check the return value of updateAll() because it's possible
        // that the UPDATE statement doesn't change anything and thus returns 0.
        $rows = static::updateAll($values, $condition);

        if ($lock !== null && !$rows) {
            throw new StaleObjectException('The object being updated is outdated.');
        }

        if (isset($values[$lock])) {
            $this->$lock = $values[$lock];
        }

        $changedAttributes = [];
        foreach ($values as $name => $value) {
            $changedAttributes[$name] = isset($this->_oldAttributes[$name]) ? $this->_oldAttributes[$name] : null;
            $this->_oldAttributes[$name] = $value;
        }

        return $this->afterSave(false, $changedAttributes);
    }

    protected function deleteInternal()
    {
        if (!$this->beforeDelete()) {
            return false;
        }

        // we do not check the return value of deleteAll() because it's possible
        // the record is already deleted in the database and thus the method will return 0
        $condition = $this->getOldPrimaryKey(true);
        $lock = $this->optimisticLock();
        if ($lock !== null) {
            $condition[$lock] = $this->$lock;
        }
        $result = static::deleteAll($condition);
        if ($lock !== null && !$result) {
            throw new StaleObjectException('The object being deleted is outdated.');
        }
        $this->setOldAttributes(null);

        return $this->afterDelete();
    }

    public function saveWithLang($lang,$runValidation = true, $attributeNames = null)
    {
        if(is_null($this->primaryKey))
            throw new \Exception('Primary key is not set!');
        $log_id = null;
        if(!$this->saved){
            $log_id = $this->save($runValidation, $attributeNames);
            if(!$log_id)
                return false;
            $this->saved = true;
        }

        $tableName  = static::class;
        $tableName = $tableName::tableName();
        $language = Languages::findOne(['iso'=>$lang]);
        if(is_null($language))
            throw new \Exception('Language not found!');

        foreach ($this->translatable as $v)
        {
            $key = $tableName.'.'.$v.'.'.$this->{$this->primaryKey};
            $model = Translations::findOne(['key'=>$key,'lang_id'=>$language->id]);
            if(is_null($model))
                $model = new Translations();
            if(!Translations::saveTranslation($key, $this->{$v}, $language, $model, $this->log_id))
                return false;
        }
        return true;
    }

    public function delete()
    {
        if (!$this->isTransactional(self::OP_DELETE)) {
            return $this->deleteInternal();
        }

        $transaction = static::getDb()->beginTransaction();
        try {
            $result = $this->deleteInternal();
            if ($result === false) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }

            return $result;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}