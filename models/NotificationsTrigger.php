<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications_trigger".
 *
 * @property int $id
 * @property string $action
 * @property int $element_id
 * @property string $value
 * @property int $value_int
 * @property int $trigger_time
 * @property int $agent_id
 * @property int $contact_id
 * @property string $data
 * @property int $created_at
 * @property int $updated_at
 */
class NotificationsTrigger extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_trigger';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action'], 'required'],
            [['element_id', 'type', 'value_int', 'trigger_time', 'trigger_day', 'agent_id', 'contact_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['element_id', 'status', 'tenant_id', 'value_int', 'type', 'trigger_time', 'agent_id', 'contact_id', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['action', 'value', 'trigger_day'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'element_id' => 'Element ID',
            'value' => 'Value',
            'type' => 'Type',
            'value_int' => 'Value Int',
            'trigger_time' => 'Server Time',
            'trigger_day' => 'Server Day',
            'agent_id' => 'Agent ID',
            'contact_id' => 'Contact ID',
            'tenant_id' => 'Tenant',
            'status' => 'Status',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}