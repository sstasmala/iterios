<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "notificationsnow_log".
 *
 * @property int $id
 * @property int $status
 * @property string $error
 * @property int $notification_id
 * @property string $element
 * @property string $action
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class NotificationsnowLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notificationsnow_log';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'log_notice_id', 'log_email_id', 'user_id', 'notification_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['status', 'log_notice_id', 'log_email_id', 'notification_id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['error', 'element'], 'string'],
            [['action'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'error' => 'Error',
            'log_email_id' => 'Log Email',
            'log_notice_id' => 'Log Notice',
            'notification_id' => 'Notification ID',
            'user_id' => 'User ID',
            'element' => 'Element',
            'action' => 'Action',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
