<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logs_sms".
 *
 * @property int $id
 * @property string $text
 * @property string $recipient
 * @property string $status
 * @property int $provider_id
 * @property int $country_id
 * @property string $alpha_name_type
 * @property int $alpha_name_id
 * @property string $sms_id
 * @property string $error_text
 * @property int $created_at
 * @property int $updated_at
 *
 * @property SmsAlphaNames $alphaName
 * @property SmsCountries $country
 * @property SmsSystemProviders $provider
 */
class LogsSms extends \yii\db\ActiveRecord
{
    const ANAME_TYPE_PERSONAL = 'personal';
    const ANAME_TYPE_SYSTEM = 'system';
    const ANAME_TYPE_PUBLIC = 'public';

    // system statuses
    const STATUS_WAIT = 'wait';
    const STATUS_ERROR = 'error';
    const STATUS_SENT = 'sent';

    // provider statuses
    const STATUS_ENROUTE = 'enroute';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_EXPIRED = 'expired';
    const STATUS_UNDELIVERED = 'undelivered';
    const STATUS_REJECTED = 'rejected';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs_sms';
    }

    /**
     * @return array
     */
    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'sms_id', 'error_text'], 'string'],
            [['recipient', 'status'], 'required'],
            [['provider_id', 'country_id', 'alpha_name_id', 'sms_id', 'error_text', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['provider_id', 'country_id', 'alpha_name_id', 'created_at', 'updated_at'], 'integer'],
            [['recipient', 'status', 'alpha_name_type'], 'string', 'max' => 255],
            [['alpha_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsAlphaNames::className(), 'targetAttribute' => ['alpha_name_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsCountries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsSystemProviders::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'recipient' => 'Recipient',
            'status' => 'Status',
            'provider_id' => 'Provider ID',
            'country_id' => 'Country ID',
            'alpha_name_type' => 'Alpha Name Type',
            'alpha_name_id' => 'Alpha Name ID',
            'sms_id' => 'SMS ID',
            'error_text' => 'Error text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlphaName()
    {
        return $this->hasOne(SmsAlphaNames::className(), ['id' => 'alpha_name_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(SmsCountries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(SmsSystemProviders::className(), ['id' => 'provider_id']);
    }
}
