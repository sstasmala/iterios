<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "orders_tourists".
 *
 * @property int $id
 * @property int $order_id
 * @property int $passport_id
 * @property int $link_id
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Contacts $contact
 * @property OrdersServicesLinks $link
 * @property Orders $order
 * @property string $contact_id [integer]
 */
class OrdersTourists extends \yii\db\ActiveRecord
{
    const TYPE_ADULT = 0;
    const TYPE_CHILD = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_tourists';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'link_id'], 'required'],
            [['order_id', 'contact_id', 'passport_id', 'link_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['type'], 'default', 'value' => 0],
            [['order_id', 'contact_id', 'passport_id', 'link_id', 'type', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['passport_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsPassports::className(), 'targetAttribute' => ['passport_id' => 'id']],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'contact_id' => 'Contact ID',
            'passport_id' => 'Passport ID',
            'link_id' => 'Link ID',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(ContactsPassports::className(), ['id' => 'passport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(OrdersServicesLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }
}
