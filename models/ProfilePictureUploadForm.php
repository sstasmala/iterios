<?php
/**
 * models/ProfilePictureUploadForm.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\models;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;

class ProfilePictureUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $profile_picture;

    public function rules()
    {
        return [
            [['profile_picture'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 10],
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate()) {
            $path = 'storage/users/' . \Yii::$app->user->id;
            $picture_name = \Yii::$app->security->generateRandomString(14) . '.' . $this->profile_picture->extension;

            if (FileHelper::createDirectory($path))
                if ($this->profile_picture->saveAs($path . '/' . $picture_name))
                    return $this->savePicture($path, $picture_name);
        }

        return false;
    }

    /**
     * @param $path
     * @param $picture_name
     *
     * @return bool
     */
    private function savePicture($path, $picture_name)
    {
        Image::thumbnail($path . '/' . $picture_name, 150, 150)
            ->save($path . '/' . $picture_name, ['quality' => 80]);

        $user = User::findOne(\Yii::$app->user->id);

        if (!empty($user->photo)) {
            if (file_exists($user->photo))
                FileHelper::unlink($user->photo);
        }

        $user->photo = $path . '/' . $picture_name;

        return ($user->save() ? $user->photo : false);
    }
}