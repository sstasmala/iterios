<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_tourists_payments".
 *
 * @property int $id
 * @property int $order_id
 * @property int $link_id
 * @property string $ext_rates
 * @property string $sum
 * @property string $note
 * @property int $date
 * @property int $type
 *
 * @property Orders $order
 * @property OrdersServicesLinks $link
 */
class OrdersTouristsPayments extends \yii\db\ActiveRecord
{
    const TYPE_TOURIST = 0;
    const TYPE_PROVIDER = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_tourists_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'link_id', 'ext_rates','sum'], 'required'],
            [['order_id', 'link_id', 'date'], 'default', 'value' => null],
            [['order_id', 'link_id', 'date','type'], 'integer'],
            [['type'], 'default', 'value' => 0],
            [['ext_rates','note'], 'string'],
            [['sum'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'link_id' => 'Link ID',
            'ext_rates' => 'Ext Rates',
            'sum' => 'Sum',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(OrdersServicesLinks::className(), ['id' => 'link_id']);
    }
}
