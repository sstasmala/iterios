<?php

namespace app\models;

use app\models\log\BaseLogModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\components\notifications_now\NotificationActions;
use yii\helpers\Json;
use app\models\BaseNotifications;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property int $date_of_birth
 * @property int $discount
 * @property string $nationality
 * @property string $tin
 * @property string $type
 * @property int $company_id
 * @property int $residence_address_id
 * @property int $living_address_id
 * @property int $responsible
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Companies $company
 * @property ContactsAddresses $residenceAddress
 * @property ContactsAddresses $livingAddress
 * @property Tenants $tenant
 * @property ContactsEmails[] $contactsEmails
 * @property ContactsMessengers[] $contactsMessengers
 * @property ContactsPassports[] $contactsPassports
 * @property ContactsPhones[] $contactsPhones
 * @property ContactsSites[] $contactsSites
 * @property ContactsSocials[] $contactsSocials
 * @property ContactsTags[] $contactsTags
 * @property ContactsVisas[] $contactsVisas
 * @property string          $gender [integer]
 * @property string          $gender [integer]
 */


class Contacts extends BaseLogModel
{
    const CUSTOMER = 'customer';
    const TOURIST = 'tourist';
    const LID = 'lid';
    const NEW_ITEM = 'new';

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;

    /**
     * @param $insert
     * @param $changedAttributes
     *
     * @return int|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        // on change date_of_birth
        if (!array_key_exists('date_of_birth', $changedAttributes))
            return parent::afterSave($insert, $changedAttributes);

        $model = NotificationsTrigger::findOne(['action' => BaseNotifications::CONTACTS_DATE_BIRTH, 'element_id' => $this->id]);
        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = BaseNotifications::CONTACTS_DATE_BIRTH;
            $model->element_id = $this->id;
        }

        $model->agent_id = $this->responsible;
        $model->contact_id = $this->id;
        $model->tenant_id = $this->tenant_id;
        $model->status = 0;
        $model->type = 0;
        $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->date_of_birth));
        $model->trigger_day = date('m.d', $this->date_of_birth);
        $model->data = Json::encode([]); //  to do adding data
        $model->updated_at = time();
        $model->save();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        NotificationsTrigger::deleteAll(['action' => BaseNotifications::CONTACTS_DATE_BIRTH, 'element_id' => $this->id]);
        return parent::afterDelete();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name','tenant_id'], 'required'],
            [['first_name', 'middle_name', 'last_name', 'type'], 'string'],
            [['gender', 'discount', 'company_id', 'residence_address_id', 'living_address_id', 'responsible', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['date_of_birth','gender', 'discount', 'company_id', 'residence_address_id', 'living_address_id', 'responsible', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nationality'], 'string', 'max' => 100],
            [['tin'], 'string', 'max' => 50],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['residence_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsAddresses::className(), 'targetAttribute' => ['residence_address_id' => 'id']],
            [['living_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsAddresses::className(), 'targetAttribute' => ['living_address_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'discount' => 'Discount',
            'nationality' => 'Nationality',
            'tin' => 'Tin',
            'type' => 'Type',
            'company_id' => 'Company ID',
            'residence_address_id' => 'Residence Address ID',
            'living_address_id' => 'Living Address ID',
            'responsible' => 'Responsible',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResidenceAddress()
    {
        return $this->hasOne(ContactsAddresses::className(), ['id' => 'residence_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLivingAddress()
    {
        return $this->hasOne(ContactsAddresses::className(), ['id' => 'living_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsEmails()
    {
        return $this->hasMany(ContactsEmails::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsMessengers()
    {
        return $this->hasMany(ContactsMessengers::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsPassports()
    {
        return $this->hasMany(ContactsPassports::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsPhones()
    {
        return $this->hasMany(ContactsPhones::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsSites()
    {
        return $this->hasMany(ContactsSites::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsSocials()
    {
        return $this->hasMany(ContactsSocials::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsTags()
    {
        return $this->hasMany(ContactsTags::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsVisas()
    {
        return $this->hasMany(ContactsVisas::className(), ['contact_id' => 'id']);
    }

    public function getResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible']);
    }
}
