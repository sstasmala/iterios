<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "commercial_proposals_links".
 *
 * @property int $id
 * @property string $original_link
 * @property string $short_link
 * @property int $clicks_count
 * @property string $clicks_data
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class CommercialProposalsLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commercial_proposals_links';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clicks_count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['clicks_count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['clicks_data'], 'string'],
            [['original_link', 'short_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_link' => 'Original Link',
            'short_link' => 'Short Link',
            'clicks_count' => 'Clicks Count',
            'clicks_data' => 'Clicks Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
