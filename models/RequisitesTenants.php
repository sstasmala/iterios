<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requisites_tenants".
 *
 * @property int $id
 * @property int $requisite_id
 * @property int $set_id
 * @property string $value
 * @property int $tenant_id
 *
 * @property Requisites $requisite
 * @property RequisitesSets $set
 * @property Tenants $tenant
 */
class RequisitesTenants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites_tenants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requisite_id', 'set_id', 'tenant_id'], 'default', 'value' => null],
            [['requisite_id', 'set_id', 'tenant_id'], 'integer'],
            [['value'], 'string'],
            [['requisite_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requisites::className(), 'targetAttribute' => ['requisite_id' => 'id']],
            [['set_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequisitesSets::className(), 'targetAttribute' => ['set_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requisite_id' => 'Requisite ID',
            'set_id' => 'Set ID',
            'value' => 'Value',
            'tenant_id' => 'Tenant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisite()
    {
        return $this->hasOne(Requisites::className(), ['id' => 'requisite_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSet()
    {
        return $this->hasOne(RequisitesSets::className(), ['id' => 'set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }
}
