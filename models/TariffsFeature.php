<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tariffs_feature".
 *
 * @property int $id
 * @property string $name
 * @property string $class
 * @property int $countable
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class TariffsFeature extends BaseTranslationModel
{
    const TRANSLATABLE = [
        'name'
    ];

    protected $primaryKey = 'id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariffs_feature';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'class'], 'required'],
            [['class'], 'string'],
            [['countable', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['countable', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['countable'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'class' => 'Class',
            'countable' => 'Countable',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
