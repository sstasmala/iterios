<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property ServicesFields[] $servicesFields
 */
class Services extends BaseTranslationModel
{
    const TRANSLATABLE = ['name', 'multiservices_name'];
    protected $primaryKey = 'id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'icon'], 'required'],
            [['icon', 'multiservices_name'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'multiservices'], 'integer'],
            [['multiservices'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 500],
            [['multiservices_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'icon' => 'Icon',
            'multiservices' => 'Multiservices',
            'multiservices_name' => 'Multiservices name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesFields()
    {
        return $this->hasMany(ServicesFields::className(), ['id_service' => 'id']);
    }

    /**
     * @param null|int $id
     * @return array
     */
    public static function getServicesWithFields($id = null)
    {
        $formatted = [];
        $tenant = Yii::$app->user->identity->tenant;
        if (is_null($id) || is_array($id)) {
            if(is_array($id))
                $services = Services::find()->where(['in','id',$id])->translate($tenant->language->iso)->all();
            else
                $services = Services::find()->translate($tenant->language->iso)->all();
            foreach ($services as $service) {
                $fields = $service->getServicesFields()->orderBy('position')->asArray()->all();
                $ids = array_column($fields, 'id_field');
                $l_fields = ServicesFieldsDefault::find()->where(['in', 'id', $ids])->translate($tenant->language->iso)->all();
                $formatted[$service->id] = [
                    'id' => $service->id,
                    'icon' => $service->icon,
                    'name' => (is_null($service->name) || empty($service->name)) ? '[Not translated]' : $service->name,
                ];
                $links = array_column(ServicesLinks::find()->where(['parent_id' => $service->id])->asArray()->all(), 'child_id');
                $formatted[$service->id]['links'] = $links;
                foreach ($l_fields as $field) {
                    $pos = array_search($field->id, $ids);
                    if ($field->is_default) {
                        $formatted[$service->id]['fields_def'][$pos] = $field->toArray();
                        $formatted[$service->id]['fields_def'][$pos]['name']
                            = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                    }
                    else {
                        $formatted[$service->id]['fields'][$pos] = $field->toArray();
                        $formatted[$service->id]['fields'][$pos]['name']
                            = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                    }
                }
            }
            foreach ($formatted as $k => $v) {
                foreach ($v['links'] as $i => $l) {
                    $formatted[$k]['links'][$i] = $formatted[$l];
                    unset($formatted[$k]['links'][$i]['links']);
                }
            }
        } else {
            $service = Services::find()->where(['id' => intval($id)])->translate($tenant->language->iso)->one();
            if (is_null($service))
                return null;
            $fields = $service->getServicesFields()->orderBy('position')->asArray()->all();
            $ids = array_column($fields, 'id_field');
            $l_fields = ServicesFieldsDefault::find()->where(['in', 'id', $ids])->translate($tenant->language->iso)->all();
            $formatted = [
                'id' => $service->id,
                'icon' => $service->icon,
                'name' => (is_null($service->name) || empty($service->name)) ? '[Not translated]' : $service->name,
            ];

            $links = array_column(ServicesLinks::find()->where(['parent_id' => $service->id])->asArray()->all(), 'child_id');
            $add = Services::find()->where(['in', 'id', $links])->translate($tenant->language->iso)->all();

            foreach ($add as $a) {
                $aids = array_column($fields, 'id_field');
                $l_afields = ServicesFieldsDefault::find()->where(['in', 'id', $aids])->translate($tenant->language->iso)->all();
                $data = [
                    'id' => $a->id,
                    'icon' => $a->icon,
                    'name' => (is_null($a->name) || empty($a->name)) ? '[Not translated]' : $a->name,
                ];
                foreach ($l_afields as $field) {
                    $pos = array_search($field->id, $aids);
                    if ($field->is_default) {
                        $data['fields_def'][$pos] = $field->toArray();
                        $data['fields_def'][$pos]['name']
                            = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                    }
                    else{
                        $data['fields'][$pos] = $field->toArray();
                        $data['fields'][$pos]['name']
                            = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                    }
                }
                $formatted['links'][] = $data;
            }

            foreach ($l_fields as $field) {
                $pos = array_search($field->id, $ids);
                if ($field->is_default) {
                    $formatted['fields_def'][$pos] = $field->toArray();
                    $formatted['fields_def'][$pos]['name']
                        = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                }
                else{
                    $formatted['fields'][$pos] = $field->toArray();
                    $formatted['fields'][$pos]['name']
                        = (is_null($field->name) || empty($field->name)) ? '[Not translated]' : $field->name;
                }
            }
        }
        return $formatted;
    }
}
