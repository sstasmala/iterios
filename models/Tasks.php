<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use yii\helpers\Json;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $due_date
 * @property int $email_reminder
 * @property bool $status
 * @property bool $email_reminder_send
 * @property string $type_id
 * @property int $assigned_to_id
 * @property int $tenant_id
 * @property int $contact_id
 * @property int $company_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property User $assignedTo
 * @property Contacts $contact
 * @property Companies $company
 * @property Orders $order
 * @property bool $due_date_send
 * @property int $order_id
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user)  ? Yii::$app->user->id : null,
            ],
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['due_date', 'due_date_send', 'email_reminder', 'assigned_to_id', 'type_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['due_date', 'email_reminder', 'assigned_to_id', 'type_id', 'tenant_id', 'contact_id', 'company_id', 'order_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['status', 'email_reminder_send', 'due_date_send'], 'boolean'],
            [['title'], 'string', 'max' => 255],
            [['assigned_to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['assigned_to_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TasksTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'due_date' => 'Due Date',
            'due_date_send' => 'Due_date Send',
            'email_reminder' => 'Email Reminder',
            'status' => 'Status',
            'email_reminder_send' => 'Email Reminder Send',
            'type_id' => 'Type',
            'assigned_to_id' => 'Assigned To ID',
            'tenant_id' => 'Tenant id',
            'contact_id' => 'Contact id',
            'company_id' => 'Company id',
            'order_id' => 'Order id',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedTo()
    {
        return $this->hasOne(User::className(), ['id' => 'assigned_to_id']);
    }

    public function getType()
    {
        return $this->hasOne(TasksTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // changes fields
        $keys = array_keys($changedAttributes);

        // on change date_of_birth
        if (!in_array('due_date', $keys) && !in_array('due_time', $keys)) return parent::afterSave($insert, $changedAttributes);

        $model = NotificationsTrigger::findOne(['action' => BaseNotifications::TASKS_DAYS_COMPLETE, 'element_id' => $this->id]);
        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = BaseNotifications::TASKS_DAYS_COMPLETE;
            $model->element_id = $this->id;
        }

        // save to process check
        $model->agent_id = $this->assigned_to_id;
        $model->contact_id = 0;
        $model->trigger_time = $this->due_date;
        $model->trigger_day = date('Y.m.d', $this->due_date);
        $model->tenant_id = $this->tenant_id;
        $model->status = 0;
        $model->type = 0;
        $model->data = Json::encode([]);
        $model->updated_at = time();
        $model->save();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() 
    {
        $where = ['action' => BaseNotifications::TASKS_DAYS_COMPLETE, 'element_id' => $this->id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }
}
