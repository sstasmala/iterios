<?php

namespace app\models;

use Yii;
use app\models\base\BaseTranslationModel;

/**
 * This is the model class for table "reminder_event_types".
 *
 * @property int $id
 * @property string $name
 * @property string $icon_class
 * @property string $color_type
 */
class ReminderEventTypes extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';

    const COLOR_TYPES = [
        'success',
        'warning',
        'danger',
        'info',
        'primary',
        'default'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reminder_event_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['action', 'name', 'icon_class', 'color_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'name' => 'Name',
            'icon_class' => 'Icon Class',
            'color_type' => 'Color Type',
            'status' => 'Status',
        ];
    }
}
