<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "placeholders".
 *
 * @property int $id
 * @property string $short_code
 * @property string $type_placeholder
 * @property string $module
 * @property string $field
 * @property string $default
 * @property string $description
 * @property bool $fake
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Placeholders extends BaseTranslationModel
{
    const TRANSLATABLE = ['short_code'];

    const TYPE_PLACEHOLDER_FIELD = 'field';
    const TYPE_PLACEHOLDER_TEMPLATE = 'additional_template';

    const TYPE_PLACEHOLDERS = [
        self::TYPE_PLACEHOLDER_FIELD => 'Field (single)',
        self::TYPE_PLACEHOLDER_TEMPLATE => 'Additional template (multiple)'
    ];

    const TYPE_PLACEHOLDERS_KEY = [
        self::TYPE_PLACEHOLDER_FIELD,
        self::TYPE_PLACEHOLDER_TEMPLATE
    ];

    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placeholders';
    }

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['fake'], 'boolean'],
            [['default', 'module', 'field', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['short_code', 'type_placeholder', 'module', 'field', 'default'], 'string', 'max' => 255],
            [['short_code'], 'required'],
            // [['type_placeholder', 'module'], 'required', 'when' => function ($model) {
            //     return $model->fake !== true;
            // }],
            ['type_placeholder', 'in', 'range' => static::TYPE_PLACEHOLDERS_KEY],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_code' => 'Short Code',
            'type_placeholder' => 'Type Placeholder',
            'module' => 'Module',
            'field' => 'Field',
            'default' => 'Default',
            'description' => 'Description',
            'fake' => 'Fake',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
    {
        $result =  parent::beforeSave($insert);
        $this->short_code = str_replace(' ','_', $this->short_code);

        if ($this->fake) {
            $this->type_placeholder = null;
            $this->module = null;
            $this->field = null;
            $this->default = null;
            $this->description = null;
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
