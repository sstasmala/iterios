<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "features_tariffs".
 *
 * @property int $id
 * @property int $tariff_id
 * @property int $feature_id
 * @property int $count
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Tariffs $tariff
 * @property TariffsFeature $feature
 */
class FeaturesTariffs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'features_tariffs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tariff_id', 'feature_id'], 'required'],
            [['tariff_id', 'feature_id', 'count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tariff_id', 'feature_id', 'count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['feature_id'], 'exist', 'skipOnError' => true, 'targetClass' => TariffsFeature::className(), 'targetAttribute' => ['feature_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'feature_id' => 'Feature ID',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this->hasOne(TariffsFeature::className(), ['id' => 'feature_id']);
    }
}
