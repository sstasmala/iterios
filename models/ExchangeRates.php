<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "exchange_rates".
 *
 * @property int $id
 * @property int $currency_id
 * @property string $currency_iso
 * @property string $rate
 * @property int $created_at
 * @property int $updated_at
 */
class ExchangeRates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exchange_rates';
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_id', 'currency_iso'], 'required'],
            [['currency_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['currency_id', 'created_at', 'updated_at'], 'integer'],
            [['rate'], 'number'],
            [['rate'], 'default','value'=>1],
            [['currency_iso'], 'string', 'max' => 10],
            [['currency_id'], 'unique'],
            [['currency_iso'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_id' => 'Currency ID',
            'currency_iso' => 'Currency Iso',
            'rate' => 'Rate',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
