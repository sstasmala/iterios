<?php

namespace app\models;

use app\helpers\TranslationHelper;
use app\models\log\BaseLogModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\BaseNotifications;

/**
 * This is the model class for table "contacts_passports".
 *
 * @property int $id
 * @property int $type_id
 * @property string $first_name
 * @property string $last_name
 * @property string $serial
 * @property string $country
 * @property string $nationality
 * @property string $birth_date
 * @property string $date_limit
 * @property string $issued_date
 * @property string $issued_owner
 * @property int $contact_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Contacts $contact
 * @property PassportsTypes $type
 */
class ContactsPassports extends BaseLogModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts_passports';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['birth_date', 'date_limit', 'issued_date','type_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'last_name', 'type_id', 'contact_id', 'birth_date', 'date_limit'], 'required'],
            [['last_name', 'serial'], 'string'],
            [['first_name', 'country',  'issued_owner'], 'string', 'max' => 255],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PassportsTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            ['serial', 'validateSerial'],
//            ['nationality', 'validateNationality', 'skipOnEmpty' => false, 'skipOnError' => false],

        ];
    }

    public function validateSerial($attribute)
    {
        $contact_serial = $this->$attribute;
        $query = static::find()
            ->joinWith('contact')
            ->where(['contacts.tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->andWhere(['contacts_passports.serial' => $contact_serial]);

        if ($this->id !== null)
            $query->andWhere(['!=', 'contacts_passports.id', $this->id]);

        $query = $query->count();

        if ($query) {
            $this->addError($attribute, TranslationHelper::getTranslation('modal_passport_serial_check', \Yii::$app->user->identity->tenant->language->iso) . $contact_serial);

            return false;
        }

        return true;
    }

    public function validateNationality($attribute)
    {
        $contact_nationality = $this->$attribute;
            if (empty($contact_nationality)) {
                $this->addError($attribute,  TranslationHelper::getTranslation('modal_passport_nationality_check', \Yii::$app->user->identity->tenant->language->iso) . $contact_nationality);
                return false;
            }

        return true;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'serial' => 'Serial',
            'country' => 'Country',
            'nationality' => 'Nationality',
            'birth_date' => 'Birth Date',
            'date_limit' => 'Date Limit',
            'issued_date' => 'Issued Date',
            'issued_owner' => 'Issued Owner',
            'contact_id' => 'Contact ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PassportsTypes::className(), ['id' => 'type_id']);
    }


    /**
     * @param $insert
     * @param $changedAttributes
     *
     * @return int|void
     */
    public function afterSave($insert, $changedAttributes)
    {

        // on change date_of_birth
        if (!array_key_exists('date_limit', $changedAttributes))
            return parent::afterSave($insert, $changedAttributes);

        $model = NotificationsTrigger::findOne(['action' => BaseNotifications::PASSPORT_DUE_DATE, 'contact_id' => $this->contact_id, 'element_id' => $this->id]);

        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = BaseNotifications::PASSPORT_DUE_DATE;
            $model->element_id = $this->id;
        }

        $contact = Contacts::find()->where(['id' => $this->contact_id])->one();
        
        $model->agent_id = $contact->responsible;
        $model->tenant_id = $contact->tenant_id;
        $model->contact_id = $this->contact_id;
        $model->status = 0;
        $model->type = 0;
        $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->date_limit));
        $model->trigger_day = date('Y.m.d', $this->date_limit);
        $model->data = '[]'; //  to do adding data
        $model->updated_at = time();
        $model->save();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        $where = ['action' => BaseNotifications::PASSPORT_DUE_DATE, 'contact_id' => $this->contact_id, 'element_id' => $this->id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }
}
