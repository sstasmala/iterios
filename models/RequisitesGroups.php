<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;

/**
 * This is the model class for table "requisites_groups".
 *
 * @property int $id
 * @property string $name
 */
class RequisitesGroups extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
