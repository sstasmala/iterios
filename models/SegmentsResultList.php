<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\helpers\QueryBuilderTranslatorHelper;
use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\ContactsPhones;
use app\models\SegmentsRelations;

/**
 * This is the model class for table "segments_result_list".
 *
 * @property int $id
 * @property int $id_segment
 * @property int $id_contact
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Contacts $contact
 * @property Segments $segment
 */
class SegmentsResultList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'segments_result_list';
    }

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_relation', 'id_contact', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['id_relation', 'id_contact', 'created_at', 'updated_at'], 'integer'],
            [['id_contact'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['id_contact' => 'id']],
            [['id_relation'], 'exist', 'skipOnError' => true, 'targetClass' => SegmentsRelations::className(), 'targetAttribute' => ['id_relation' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_relation' => 'Id Relation',
            'id_contact' => 'Id Contact',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'id_contact']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSegmentRelation()
    {
        return $this->hasOne(SegmentsRelations::className(), ['id' => 'id_relation']);
    }

    public function getContactPhones()
    {
        return $this->hasMany(ContactsPhones::className(), ['contact_id' => 'id_contact']);
    }

    public function getContactEmails()
    {
        return $this->hasMany(ContactsEmails::className(), ['contact_id' => 'id_contact']);
    }


    static public function addFromContactList(array $contactList, $id_relation)
    {
        self::deleteAll(['id_relation' => $id_relation]);
        $count = 0;

        foreach ($contactList as $contact) {
            $result = new SegmentsResultList();
            $result->id_relation = $id_relation;
            $result->id_contact = $contact->id;
            $result->save();
            $count++;
        }
        return $count;
    }

    static public function loadFromRelation()
    {

        $segmentAll = Segments::find()->select(['id'])->where(['type' => Segments::TYPE_SYSTEM])->all();
        $tenantAll = Tenants::find()->select(['id'])->all();
//        SegmentsRelations::deleteAll(['type' => SegmentsRelations::TYPE_SYSTEM]);


        foreach ($segmentAll as $segment) {
            foreach ($tenantAll as $tenant) {
                $segmentRelationsNew = SegmentsRelations::find()->where(['id_segment'=>$segment->id])->andWhere(['id_tenant'=>$tenant->id])->one();
                if($segmentRelationsNew == null)
                    $segmentRelationsNew = new SegmentsRelations();
                $segmentRelationsNew->id_segment = $segment->id;
                $segmentRelationsNew->id_tenant = $tenant->id;
                $segmentRelationsNew->type = SegmentsRelations::TYPE_SYSTEM;
                $segmentRelationsNew->name = $segment->name . ' ' . $tenant->name;
                $segmentRelationsNew->save();
            }
        }

        $allRelation = SegmentsRelations::find()->with(['segment'])->all();

        foreach ($allRelation as $relation) {

            $rules = json_decode($relation->segment->rules, true);
            $builderTranslator = new QueryBuilderTranslatorHelper($rules);

            $findContact = Contacts::find()
                ->leftJoin(ContactsEmails::tableName(), 'contacts.id = contacts_emails.contact_id')
                ->leftJoin(ContactsPhones::tableName(), 'contacts.id = contacts_phones.contact_id');

            $findContact->where('contacts.tenant_id = ' . $relation->id_tenant);

            $findContact->andWhere($builderTranslator->where())
                ->addParams($builderTranslator->params());

            $contactCount = $findContact->count();

            $findContactForEmail = clone $findContact;
            $findContactForEmail->andWhere(['not', ['contacts_emails.contact_id' => null]]);
            $emailsCount = $findContactForEmail->count();

            $findContactForPhone = clone $findContact;
            $findContactForPhone->andWhere(['not', ['contacts_phones.contact_id' => null]]);
            $phonesCount = $findContactForPhone->count();

            //Update counter in rules

            $relation->count_contact = $contactCount;
            $relation->count_phones = $phonesCount;
            $relation->count_emails = $emailsCount;
            $relation->save();

            $allContact = $findContact->all();

            $countAdd = SegmentsResultList::addFromContactList($allContact, $relation->id);
            echo "For relation $relation->id loaded $countAdd contacts" . "\n";
        }

        return 1;
    }

    static public function getPhonesFromSegmentsRelationId(array $relationId)
    {
        return self::find()->select([
            'contacts_phones.value as phone',
            'contacts_phones.id as contact_phone_id',
            'id_contact',
            'string_agg((select CAST ( id_segment AS VARCHAR) FROM segments_relations WHERE "segments_relations"."id" = "segments_result_list"."id_relation"),\';\') as segments'
        ])
            ->joinWith('contactPhones')
            ->where(['in', 'segments_result_list.id_relation', $relationId])
            ->andWhere(['is not', 'contacts_phones.value', null])
            ->groupBy(['contacts_phones.value', 'contacts_phones.id', 'id_contact'])
            ->asArray()
            ->all();
    }
}
