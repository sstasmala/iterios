<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $remember = true;

    private $_user = false;

    private $lang;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->lang = substr(Yii::$app->request->getPreferredLanguage(), 0, 2);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            [['email'], 'email'],
            // rememberMe must be a boolean value
            ['remember', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!User::checkAuth0User($this->email)) {
                $this->addError($attribute, \app\helpers\TranslationHelper::getTranslation('login_user_email_error', $this->lang));
            } else {
                if (!$this->getUser())
                    $this->addError($attribute, \app\helpers\TranslationHelper::getTranslation('login_user_pass_error', $this->lang));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            if (is_null(AgentsMapping::findOne(['tenant_id' => $user->current_tenant_id, 'user_id' => $user->id]))) {
                $user_tenant = AgentsMapping::findOne(['user_id' => $user->id]);

                $user->current_tenant_id = !is_null($user_tenant) ? $user_tenant->tenant_id : null;
            }

            $user->last_login = time();
            $user->save();

            return Yii::$app->user->login($user, $this->remember ? 3600*24*30 : 0);
        }

        return false;
    }

    /**
     * Finds user by auth0 authenticated user
     *
     * @return \app\models\search\User
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function getUser()
    {
        if ($this->_user === false) {
            try {
                $auth0Data = Yii::$app->auth0->auth_api_client->getUserInfo($this->email, $this->password);
                $this->_user = User::findByAuth0($auth0Data);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
            }
        }

        return $this->_user;
    }
}
