<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "segments_relations".
 *
 * @property int $id
 * @property int $id_segment
 * @property int $id_tenant
 * @property string $type
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Segments $segment
 * @property Tenants $tenant
 */
class SegmentsRelations extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];

    const TYPE_SYSTEM = 'system';
    const TYPE_PUBLIC = 'public';

    protected $primaryKey = 'id';

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    public static function tableName()
    {
        return 'segments_relations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_segment', 'id_tenant', 'created_at', 'updated_at', 'created_by', 'updated_by', 'count_phones','count_emails','count_contact'], 'default', 'value' => null],
            [['id_segment', 'id_tenant', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['type', 'name'], 'string', 'max' => 255],
            [['id_segment'], 'exist', 'skipOnError' => true, 'targetClass' => Segments::className(), 'targetAttribute' => ['id_segment' => 'id']],
            [['id_tenant'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['id_tenant' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_segment' => 'Segment',
            'id_tenant' => 'Tenant',
            'type' => 'Type',
            'name' => 'Name',
            'count_phones'=> 'Count phones',
            'count_emails'=> 'Count emails',
            'count_contact'=> 'Count contact',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSegment()
    {
        return $this->hasOne(Segments::className(), ['id' => 'id_segment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'id_tenant']);
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
