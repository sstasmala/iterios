<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "notifications_log".
 *
 * @property int $id
 * @property int $trigger_id
* @property int $course_id
 * @property int $reminder_id
 * @property int $contact_id
 * @property int $agent_id
 * @property string $data
 * @property int $created_at
 * @property int $updated_at
 */
class NotificationsLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_log';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trigger_id', 'send_data', 'course_id', 'system_notifications_id', 'jobs_ids','reminder_id', 'contact_id', 'agent_id', 'status', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['trigger_id', 'course_id', 'system_notifications_id', 'reminder_id', 'contact_id', 'agent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['errors', 'send_data'], 'string'],
            [['channel', 'jobs_ids'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trigger_id' => 'Trigger ID',
            'reminder_id' => 'Reminder ID',
            'system_notifications_id' => 'system notif. id',
            'jobs_ids' => 'jobs ids',
            'course_id' => 'Queue Id',
            'contact_id' => 'Contact ID',
            'agent_id' => 'Agent ID',
            'channel' => 'Channel',
            'status' => 'Status',
            'errors' => 'Errors',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'send_data' => 'Text',
        ];
    }
}
