<?php

namespace app\models;

/**
 * This is the model class for table "contacts_passports_view".
 *
 * @property int $passport_id
 * @property int $contact_id
 * @property string $first_name
 * @property string $last_name
 * @property string $serial
 * @property string $tenant_id [integer]
 * @property int    $date_limit [bigint]
 * @property int    $birth_date [bigint]
 * @property string $type_id    [integer]
 */
class ContactsPassportsView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts_passports_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_limit', 'birth_date', 'type_id'], 'default', 'value' => null],
            [['passport_id', 'contact_id', 'tenant_id', 'date_limit', 'birth_date', 'type_id', 'issued_date'], 'integer'],
            [['first_name', 'last_name', 'serial', 'issued_owner', 'nationality'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'passport_id' => 'Passport ID',
            'contact_id' => 'Contact ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'serial' => 'Serial',
            'date_limit' => 'Date Limit',
            'birth_date' => 'Birth Date',
            'type_id' => 'Type ID',
            'tenant_id' => 'Tenant ID',
            'issued_date' => 'Issued Date',
            'issued_owner' => 'Issued Owner',
            'nationality' => 'Nationality'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(ContactsPassports::className(), ['id' => 'passport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PassportsTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }
}
