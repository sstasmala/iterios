<?php

namespace app\models;

use app\modules\ia\controllers\base\BaseController;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "financial_operations".
 *
 * @property int $id
 * @property int $payment_method
 * @property string $account_number
 * @property int $transaction_number
 * @property int $operation_type
 * @property int $customer_id
 * @property int $payment_gateway_id
 * @property string $comment
 * @property string $sum
 * @property int $currency
 * @property string $sum_usd
 * @property int $date
 * @property int $owner
 * @property int $created_at
 * @property int $updated_at
 *
 * @property PaymentGateways $paymentGateway
 * @property Tenants $customer
 */
class FinancialOperations extends \yii\db\ActiveRecord
{

    const OPERATION_UNKNOWN = 0;
    const OPERATION_BALANCE = 1;
    const OPERATION_WITHDRAWAL = 2;

    const OPERATIONS = [
        self::OPERATION_UNKNOWN => 'Unknown',
        self::OPERATION_BALANCE => 'Balance',
        self::OPERATION_WITHDRAWAL => 'Withdrawal'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'financial_operations';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_method'], 'required'],
            [['payment_method', 'transaction_number', 'operation_type', 'customer_id', 'payment_gateway_id', 'currency', 'date', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['payment_method', 'operation_type', 'customer_id', 'payment_gateway_id', 'currency', 'owner', 'created_at', 'updated_at'], 'integer'],
            [['comment'], 'string'],
            [['date'], 'safe'],
            [['sum', 'sum_usd'], 'number'],
            [['account_number','transaction_number'], 'string', 'max' => 255],
            [['owner'], 'default', 'value' => 0],
            [['sum', 'sum_usd'], 'default', 'value' => 0.00],
            [['payment_gateway_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentGateways::className(), 'targetAttribute' => ['payment_gateway_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_method' => 'Payment Method',
            'account_number' => 'Account Number',
            'transaction_number' => 'Transaction Number',
            'operation_type' => 'Operation Type',
            'customer_id' => 'Customer ID',
            'payment_gateway_id' => 'Payment Gateway ID',
            'comment' => 'Comment',
            'sum' => 'Sum',
            'currency' => 'Currency',
            'sum_usd' => 'Sum Usd',
            'date' => 'Date',
            'owner' => 'Owner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentGateway()
    {
        return $this->hasOne(PaymentGateways::className(), ['id' => 'payment_gateway_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'customer_id']);
    }

}
