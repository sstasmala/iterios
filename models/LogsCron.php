<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logs_cron".
 *
 * @property int $id
 * @property string $command
 * @property string $output_file
 * @property int $time_start
 * @property int $time_end
 * @property string $memory_usage
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class LogsCron extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 'success';
    const STATUS_EMPTY = 'empty';
    const STATUS_EXECUTING = 'executing';
    const STATUS_ERROR = 'error';
    const STATUS_WARNING = 'warning';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs_cron';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time_start', 'time_end', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['time_start', 'time_end', 'created_at', 'updated_at'], 'integer'],
            [['command', 'output_file'], 'string', 'max' => 255],
            [['memory_usage', 'status'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'command' => 'Command',
            'output_file' => 'Output File',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'memory_usage' => 'Memory Usage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getDuration()
    {
        $diff = $this->time_end - $this->time_start;
        $min = floor($diff / 60);

        if ($min == 0)
            return '0.' . ($diff >= 10 ? $diff : '0'.$diff) . ' seconds';

        $sec = $diff - ($min * 60);
        return $min . '.' . ($sec >= 10 ? $sec : '0'.$sec) . ' minutes';
    }
}
