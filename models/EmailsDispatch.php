<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emails_dispatch".
 *
 * @property int $id
 * @property string $name
 * @property string $subject
 * @property int $provider_id
 * @property int $status
 * @property int $opens
 * @property int $clicks
 * @property string $errors
 * @property string $receivers
 * @property string $email
 * @property string $provider_data
 * @property string $statistic_data
 * @property int $email_type
 * @property int $date
 * @property int $stat_update
 * @property int $created_at
 * @property int $updated_at
 * @property int $tenant_id
 * @property int $owner_id
 *
 * @property ProvidersCredentials $provider
 * @property Tenants $tenant
 * @property User $owner
 */
class EmailsDispatch extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_COMPLETE = 2;
    const STATUS_ERROR = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails_dispatch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'subject', 'provider_id', 'email', 'tenant_id', 'owner_id'], 'required'],
            [['status','opens','clicks','stat_update'],'default','value'=>0],
            [['provider_id', 'status', 'opens', 'clicks', 'email_type', 'date', 'created_at', 'updated_at', 'tenant_id', 'owner_id'], 'default', 'value' => null],
            [['provider_id', 'status', 'opens', 'clicks', 'email_type', 'date', 'created_at', 'updated_at', 'tenant_id', 'owner_id'], 'integer'],
            [['errors', 'receivers', 'email','provider_data','statistic_data'], 'string'],
            [['name', 'subject'], 'string', 'max' => 255],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProvidersCredentials::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'provider_id' => 'Provider ID',
            'status' => 'Status',
            'opens' => 'Opens',
            'clicks' => 'Clicks',
            'errors' => 'Errors',
            'receivers' => 'Receivers',
            'email' => 'Email',
            'email_type' => 'Email Type',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tenant_id' => 'Tenant ID',
            'owner_id' => 'Owner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(ProvidersCredentials::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
