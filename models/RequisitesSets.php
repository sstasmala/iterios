<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requisites_sets".
 *
 * @property int $id
 * @property int $tenant_id
 * @property string $name
 *
 * @property Tenants $tenant
 * @property RequisitesTenants[] $requisitesTenants
 */
class RequisitesSets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites_sets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'default', 'value' => null],
            [['tenant_id'], 'integer'],
            [['name'], 'string'],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenant_id' => 'Tenant',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesTenants()
    {
        return $this->hasMany(RequisitesTenants::className(), ['set_id' => 'id']);
    }
}
