<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "delivery_placeholders".
 *
 * @property int $id
 * @property string $short_code
 * @property string $module
 * @property integer $path
 * @property string $default
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property bool $fake [boolean]
 * @property string $description
 *
 */
class DeliveryPlaceholders extends BaseTranslationModel
{
    const TRANSLATABLE = ['short_code'];
    const USED_MODULES = [
        'other' => 'Other'
    ];

    const MODULE_OTHER = 'Other';
    const MODULE_OTHER_KEY = 'other';

    //const MODULE_SERVICE_KEY = 'services_fields';

    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_placeholders';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['path', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['short_code', 'module', 'default'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['fake'], 'boolean'],
            [['short_code'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_code' => 'Short Code',
            'module' => 'Module',
            'path' => 'Path',
            'default' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'fake' => 'Fake placeholder',
            'description' => 'Description'
        ];
    }

    public function beforeSave($insert)
    {
        $result =  parent::beforeSave($insert);
        $this->short_code = str_replace(' ','_', $this->short_code);

        if ($this->fake) {
            $this->short_code = null;
            $this->module = null;
            $this->path = null;
            $this->default = null;
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getDeliveryPlaceholderTypesMapping()
    {
        return $this->hasMany(DeliveryPlaceholderTypes::className(), ['delivery_placeholder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryPlaceholderTypes()
    {
        return $this->hasMany(DeliveryTypes::className(), ['id' => 'delivery_type_id'])
            ->via('deliveryPlaceholderTypesMapping');
    }

    /**
     * @param $module
     *
     * @return array
     */
    // public static function getAllPath($module, $search){
    //     if (!empty(self::USED_MODULES[$module]) && self::USED_MODULES[$module] !== self::MODULE_OTHER) {
    //         $class = 'app\models\\' . self::USED_MODULES[$module];
    //         $results_tmp = $class::find();
    //
    //         switch (self::USED_MODULES[$module]) {
    //             case self::MODULE_SERVICE:
    //                 $results_tmp->joinWith('field')
    //                     ->where(['like', "CONCAT(services_fields_default.code, '/', services_fields_default.name)", $search])
    //                     ->andWhere(['ilike', "CONCAT(services_fields_default.code, '/', services_fields_default.name)", $search]);
    //                 break;
    //         }
    //
    //         $results_tmp = $results_tmp->limit(100)
    //             ->all();
    //
    //         $result_unique = [];
    //
    //         foreach ($results_tmp as $model){
    //             switch (self::USED_MODULES[$module]) {
    //                 case self::MODULE_SERVICE:
    //                     $result_unique[$model->id_field] = $model->field->code .'/'.$model->field->name;
    //                     break;
    //             }
    //         }
    //
    //         return array_unique($result_unique);
    //     }
    //
    //     return [];
    // }

    public static function getShortcodes()
    {
        $models = self::find()->asArray()->all();

        return ArrayHelper::map($models,'short_code', 'default');
    }

    // public static function getShortcodesEmail()
    // {
    //     $models = static::find()
    //         ->where(['!=', 'module', static::MODULE_SERVICE_KEY])
    //         ->asArray()->all();
    //
    //     $placeholders = ArrayHelper::map($models,'short_code', 'default');
    //
    //     $models_ph_services = static::find()
    //         ->where(['module' => static::MODULE_SERVICE_KEY])
    //         ->asArray()->all();
    //
    //     $placeholders['services_placeholders'] = [
    //         ArrayHelper::map($models_ph_services,'short_code', 'default')
    //     ];
    //
    //     return $placeholders;
    // }
}
