<?php

namespace app\models;

use app\models\log\BaseLogModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $legal_name
 * @property int $responsible_id
 * @property int $company_address_id
 * @property int $company_type_id
 * @property int $company_kind_activity_id
 * @property int $company_contact_source_id
 * @property int $company_status_id
 *
 * @property CompaniesAddresses $companyAddress
 * @property CompaniesContactSources $companyContactSource
 * @property CompaniesKindActivities $companyKindActivity
 * @property CompaniesStatuses $companyStatus
 * @property CompaniesTypes $companyType
 * @property Tenants $tenant
 * @property User $responsible
 * @property CompaniesEmails[] $companiesEmails
 * @property CompaniesMessengers[] $companiesMessengers
 * @property CompaniesPhones[] $companiesPhones
 * @property CompaniesSites[] $companiesSites
 * @property CompaniesSocials[] $companiesSocials
 * @property CompaniesTags[] $companiesTags
 * @property Contacts[] $contacts
 * @property CompaniesEmployees[] $employees
 */
class Companies extends BaseLogModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'responsible_id', 'company_address_id', 'company_type_id', 'company_kind_activity_id', 'company_contact_source_id', 'company_status_id'], 'default', 'value' => null],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'responsible_id', 'company_address_id', 'company_type_id', 'company_kind_activity_id', 'company_contact_source_id', 'company_status_id'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['legal_name'], 'string', 'max' => 255],
            [['company_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesAddresses::className(), 'targetAttribute' => ['company_address_id' => 'id']],
            [['company_contact_source_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesContactSources::className(), 'targetAttribute' => ['company_contact_source_id' => 'id']],
            [['company_kind_activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesKindActivities::className(), 'targetAttribute' => ['company_kind_activity_id' => 'id']],
            [['company_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesStatuses::className(), 'targetAttribute' => ['company_status_id' => 'id']],
            [['company_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesTypes::className(), 'targetAttribute' => ['company_type_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['responsible_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'tenant_id' => 'Tenant ID',
            'legal_name' => 'Legal Name',
            'responsible_id' => 'Responsible ID',
            'company_address_id' => 'Company Address ID',
            'company_type_id' => 'Company Type ID',
            'company_kind_activity_id' => 'Company Kind Activity ID',
            'company_contact_source_id' => 'Company Contact Source ID',
            'company_status_id' => 'Company Status ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAddress()
    {
        return $this->hasOne(CompaniesAddresses::className(), ['id' => 'company_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyContactSource()
    {
        return $this->hasOne(CompaniesContactSources::className(), ['id' => 'company_contact_source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKindActivity()
    {
        return $this->hasOne(CompaniesKindActivities::className(), ['id' => 'company_kind_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyStatus()
    {
        return $this->hasOne(CompaniesStatuses::className(), ['id' => 'company_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompaniesTypes::className(), ['id' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesEmails()
    {
        return $this->hasMany(CompaniesEmails::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesMessengers()
    {
        return $this->hasMany(CompaniesMessengers::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesPhones()
    {
        return $this->hasMany(CompaniesPhones::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesSites()
    {
        return $this->hasMany(CompaniesSites::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesSocials()
    {
        return $this->hasMany(CompaniesSocials::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesTags()
    {
        return $this->hasMany(CompaniesTags::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contacts::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(CompaniesEmployees::className(), ['company_id' => 'id']);
    }
}
