<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;

/**
 * This is the model class for table "document_placeholders".
 *
 * @property int $id
 * @property string $short_code
 * @property string $module
 * @property string $path
 * @property string $default
 * @property string $name [varchar(255)]
 */
class DocumentsType extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];

    protected $primaryKey = 'id';


    public static function tableName()
    {
        return 'documents_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}

