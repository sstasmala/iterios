<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications_course".
 *
 * @property int $id
 * @property int $send_type
 * @property int $user_id
 * @property int $user_type
 * @property string $date
 * @property string $time
 * @property string $day
 * @property string $timezone_date
 * @property string $timezone_time
 * @property string $timezone_day
 * @property int $send_status
 * @property string $rule_id
 * @property int $noting_id
 * @property int $created_at
 */
class NotificationsCourse extends \yii\db\ActiveRecord
{
    public $is_email = '';
    public $is_task = '';
    public $is_notification = '';

/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['send_type', 'user_id', 'user_type', 'send_status', 'noting_id', 'created_at', 'tenant_id', 'trigger_id'], 'default', 'value' => null],
            [['send_type', 'user_id', 'user_type', 'send_status', 'noting_id', 'created_at', 'tenant_id', 'trigger_id'], 'integer'],
            [['errors'], 'string'],
            [['date', 'time', 'day', 'timezone_date', 'timezone_time', 'timezone_day', 'rule_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'send_type' => 'Send Type',
            'user_id' => 'User ID',
            'user_type' => 'User Type',
            'date' => 'User Date',
            'time' => 'Time',
            'day' => 'Day',
            'timezone_date' => 'Server Date',
            'timezone_time' => 'Timezone Time',
            'timezone_day' => 'Timezone Day',
            'send_status' => 'Send Status',
            'rule_id' => 'Rule ID',
            'noting_id' => 'Noting ID',
            'created_at' => 'Created At',
            'tenant_id' => 'Tenant ID',
            'trigger_id' => 'Trigger ID',
            'errors' => 'Errors',
        ];
    }
}

