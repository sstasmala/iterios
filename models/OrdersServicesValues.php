<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_services_values".
 *
 * @property int $id
 * @property int $link_id
 * @property int $additional_link_id
 * @property int $service_id
 * @property int $field_id
 * @property string $value
 *
 * @property OrdersServicesAdditionalLinks $additionalLink
 * @property OrdersServicesLinks $link
 * @property Services $service
 * @property ServicesFieldsDefault $field
 */
class OrdersServicesValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_services_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'service_id', 'field_id'], 'required'],
            [['link_id', 'additional_link_id', 'service_id', 'field_id'], 'default', 'value' => null],
            [['link_id', 'additional_link_id', 'service_id', 'field_id'], 'integer'],
            [['value'], 'string'],
            [['additional_link_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersServicesAdditionalLinks::className(), 'targetAttribute' => ['additional_link_id' => 'id']],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicesFieldsDefault::className(), 'targetAttribute' => ['field_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'additional_link_id' => 'Additional Link ID',
            'service_id' => 'Service ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalLink()
    {
        return $this->hasOne(OrdersServicesAdditionalLinks::className(), ['id' => 'additional_link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(OrdersServicesLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(ServicesFieldsDefault::className(), ['id' => 'field_id']);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     *
     * @return int|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        $field = ServicesFieldsDefault::find()->where(['id' => $this->field_id])->one()->toArray();
        if (empty($field))
            return parent::afterSave($insert, $changedAttributes);


        // on change value
        if (!in_array($field['type'], [
            ServicesFieldsDefault::TYPE_DATE_STARTING,
            ServicesFieldsDefault::TYPE_DATE_ENDING,
            ServicesFieldsDefault::TYPE_TIME_STARTING,
            ServicesFieldsDefault::TYPE_TIME_ENDING,
        ]))
            return parent::afterSave($insert, $changedAttributes);

        $action = ($field['type'] == ServicesFieldsDefault::TYPE_DATE_STARTING) ? BaseNotifications::DEPARTURE_DATE_START : BaseNotifications::DEPARTURE_DATE_END;

        $service = OrdersServicesLinks::find()->where(['id' => $this->link_id])->asArray()->one();
        if (empty($service))
            return parent::afterSave($insert, $changedAttributes);

        $order = Orders::find()->where(['id' => $service['order_id']])->one();
        if (empty($order))
            return parent::afterSave($insert, $changedAttributes);

        $contact_id = $order->contact_id; // id contact
        $agent_id = $order->responsible_id; // id agent


        if (in_array($field['type'], [
            ServicesFieldsDefault::TYPE_DATE_STARTING,
            ServicesFieldsDefault::TYPE_DATE_ENDING
        ])) {

            $model = NotificationsTrigger::findOne(['action' => $action, 'element_id' => $this->link_id]);
            if (empty($model)) {
                $model = new NotificationsTrigger();
                $model->created_at = time();
                $model->action = $action;
                $model->element_id = $this->link_id;
            }

            $model->agent_id = $agent_id;
            $model->tenant_id = $order->tenant_id;
            $model->contact_id = $contact_id;
            $model->status = 0;
            $model->type = 0;
            $model->trigger_time = strtotime(date('Y-m-d 00:00:00', $this->value));
            $model->trigger_day = date('Y.m.d', $this->value);
            $model->data = '{"order_id": ' . $order->id . ', "service_id": '.$service['id'].'}'; //  to do adding data
            $model->updated_at = time();

            $model->save();
        }

        if (in_array($field['type'], [
            ServicesFieldsDefault::TYPE_TIME_STARTING,
            ServicesFieldsDefault::TYPE_TIME_ENDING
        ])) {

            $action = ($field['type'] == ServicesFieldsDefault::TYPE_TIME_STARTING) ? BaseNotifications::DEPARTURE_TIME_START : BaseNotifications::DEPARTURE_TIME_END;

            $model = NotificationsTrigger::findOne(['action' => $action, 'element_id' => $this->link_id]);
            if (empty($model)) {
                $model = new NotificationsTrigger();
                $model->created_at = time();
                $model->action = $action;
                $model->element_id = $this->link_id;
            }

            $model->agent_id = 0;
            $model->tenant_id = 0;
            $model->contact_id = 0;
            $model->status = 0;
            $model->type = 0;
            $model->value = $order->id . $service['id'];
            $model->trigger_day = '';
            $model->data = '{"value": "' . $this->value . '"}'; //  to do adding data
            $model->updated_at = time();

            $model->save();
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        $where = ['in', 'action', [BaseNotifications::TYPE_DATE_STARTING, BaseNotifications::DEPARTURE_DATE_END, ServicesFieldsDefault::TYPE_TIME_STARTING, ServicesFieldsDefault::TYPE_TIME_ENDING], 'element_id' => $this->link_id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }
}
