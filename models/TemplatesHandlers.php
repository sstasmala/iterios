<?php

namespace app\models;

use app\models\base\BaseModel;
use Yii;

/**
 * This is the model class for table "templates_handlers".
 *
 * @property int $id
 * @property string $handler
 * @property int $template_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Templates $template
 */
class TemplatesHandlers extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'templates_handlers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['handler', 'template_id'], 'required'],
            [['handler', 'template_id'], 'unique'],
            [['handler'], 'string'],
            [['template_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['template_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Templates::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'handler' => 'Handler',
            'template_id' => 'Template ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Templates::className(), ['id' => 'template_id']);
    }
}
