<?php

namespace app\models;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property string $group
 * @property int $updated_at
 * @property int $updated_by
 */
class Settings extends \yii\db\ActiveRecord
{
    const EMAIL_PROVIDERS_GROUP = 'email_providers';

    const PROVIDER_SYSTEM = 'provider_system';
    const PROVIDER_PUBLIC = 'provider_public';

    const FROM_NAME = 'email_from_name';
    const FROM_EMAIL = 'email_from';
    const REPLY_TO = 'email_reply_to';

    const EMAIL_TITLES = [
        self::PROVIDER_SYSTEM => 'Provider for system email',
        self::PROVIDER_PUBLIC => 'Provider for public email',
        self::FROM_NAME => 'From name',
        self::FROM_EMAIL => 'From email',
        self::REPLY_TO => 'Reply to'
    ];

    const MCHELPER_GROUP = 'MCHelper';

    const MCHELPER_BASE_URI = 'MCHelper_base_uri';
    const MCHELPER_KEY = 'MCHelper_Key';

    const MCHELPER_TITLES = [
        self::MCHELPER_BASE_URI => 'MCHelper base uri',
        self::MCHELPER_KEY => 'MCHelper X-Iterios-Api-Key',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['updated_at', 'updated_by'], 'default', 'value' => null],
            [['updated_at', 'updated_by'], 'integer'],
            [['key'], 'string', 'max' => 55],
            [['value', 'group'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'group' => 'Group',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @param string $key
     * @return null|string
     */
    public static function getByKey($key)
    {
        $setting = self::findOne(['key' => $key]);
        return $setting ? $setting->value : false;
    }

    /**
     * @param $group
     *
     * @return static[]
     */
    public static function getByGroup($group)
    {
        return self::find()->where(['group' => $group])->orderBy('id')->all();
    }

    /**
     * @param string $key
     * @param string $value
     * @param null   $group
     *
     * @return bool
     */
    public static function saveByKey($key, $value, $group = null)
    {
        $setting = self::findOne(['key' => $key]);

        if (is_null($setting)) {
            $setting = new self();
            $setting->key = $key;
        }

        $setting->value = $value;

        if ($group)
            $setting->group = $group;

        return $setting->save();
    }

    public static function dropByKey($key)
    {
        $setting = self::findOne(['key' => $key]);

        if (is_null($setting))
            return false;

        return $setting->delete();
    }
}
