<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenant_notifications".
 *
 * @property int $id
 * @property int $user_notification_id
 * @property int $base_notification_id
 * @property int $is_email
 * @property int $is_system_notification
 * @property int $is_task
 * @property int $tenant_id
 * @property int $last_run
 * @property int $repeat
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property BaseNotifications $baseNotification
 * @property Tenants $tenant
 * @property UserNotifications $userNotification
 */
class TenantNotifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenant_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_notification_id', 'base_notification_id', 'is_email', 'is_system_notification', 'is_task', 'tenant_id', 'last_run', 'repeat', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['user_notification_id', 'base_notification_id', 'is_email', 'is_system_notification', 'is_task', 'tenant_id', 'last_run', 'repeat', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['base_notification_id'], 'exist', 'skipOnError' => true, 'targetClass' => BaseNotifications::className(), 'targetAttribute' => ['base_notification_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
            [['user_notification_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserNotifications::className(), 'targetAttribute' => ['user_notification_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_notification_id' => 'User Notification ID',
            'base_notification_id' => 'Base Notification ID',
            'is_email' => 'Is Email',
            'is_system_notification' => 'Is System Notification',
            'is_task' => 'Is Task',
            'tenant_id' => 'Tenant ID',
            'last_run' => 'Last Run',
            'repeat' => 'Repeat',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseNotification()
    {
        return $this->hasOne(BaseNotifications::className(), ['id' => 'base_notification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotification()
    {
        return $this->hasOne(UserNotifications::className(), ['id' => 'user_notification_id']);
    }
}
