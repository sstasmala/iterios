<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placeholders_delivery_types".
 *
 * @property int $id
 * @property int $delivery_type_id
 * @property int $placeholder_delivery_id
 *
 * @property DeliveryTypes $deliveryType
 * @property PlaceholdersDelivery $placeholderDelivery
 */
class PlaceholdersDeliveryTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placeholders_delivery_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['delivery_type_id', 'placeholder_delivery_id'], 'default', 'value' => null],
            [['delivery_type_id', 'placeholder_delivery_id'], 'integer'],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryTypes::className(), 'targetAttribute' => ['delivery_type_id' => 'id']],
            [['placeholder_delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlaceholdersDelivery::className(), 'targetAttribute' => ['placeholder_delivery_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_type_id' => 'Delivery Type ID',
            'placeholder_delivery_id' => 'Placeholder Delivery ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryType()
    {
        return $this->hasOne(DeliveryTypes::className(), ['id' => 'delivery_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceholderDelivery()
    {
        return $this->hasOne(PlaceholdersDelivery::className(), ['id' => 'placeholder_delivery_id']);
    }
}
