<?php

namespace app\models;

use Yii;
use app\models\base\BaseTranslationModel;
use yii\helpers\Json;

/**
 * This is the model class for table "system_holidays".
 *
 * @property int $id
 * @property int $day
 * @property int $mounts
 * @property string $name
 */
class SystemHolidays extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_holidays';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day', 'mounts', 'country_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country',
            'day' => 'Day',
            'mounts' => 'Mounts',
            'name' => 'Name',
        ];
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if (empty($this->mounts) || empty($this->day))
            return parent::afterSave($insert, $changedAttributes);

        $mounts = ($this->mounts < 10) ? '0' . $this->mounts : $this->mounts;
        $day = ($this->day < 10) ? '0' . $this->day : $this->day;

        $model = NotificationsTrigger::findOne(['action' => 'holidays.system', 'element_id' => $this->id]);

        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = 'holidays.system';
            $model->element_id = $this->id;
        }

        $model->trigger_day = $mounts.'.'.$day;

        // save to process check
        $model->contact_id = 0;
        $model->agent_id = 0;
        $model->tenant_id = 0;
        $model->trigger_time = strtotime(date('Y-'.$mounts.'-'.$day.' 00:00:00'));
        $model->status = 0;
        $model->type = 0;
        $model->data = Json::encode([]); //  to do adding data
        $model->updated_at = time();
        $model->save();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() 
    {
        // save to process check
        NotificationsTrigger::deleteAll(['action' => 'contacts.tag_id', 'element_id' => $this->tag_id, 'contact_id' => $this->contact_id]);
        return parent::afterDelete();
    }
}
