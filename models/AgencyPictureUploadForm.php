<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;

class AgencyPictureUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $agency_picture;

    public function rules()
    {
        return [
            [['agency_picture'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 10],
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate()) {
            $path = 'storage/tenants/'. Yii::$app->user->identity->tenant->id .'/agencies';
            $picture_name = \Yii::$app->security->generateRandomString(14) . '.' . $this->agency_picture->extension;

            if (FileHelper::createDirectory($path))
                if ($this->agency_picture->saveAs($path . '/' . $picture_name))
                    return $this->savePicture($path, $picture_name);
        }

        return false;
    }

    /**
     * @param $path
     * @param $picture_name
     *
     * @return bool
     */
    private function savePicture($path, $picture_name)
    {
        Image::thumbnail($path . '/' . $picture_name, 150, 150)
            ->save($path . '/' . $picture_name, ['quality' => 80]);

        $agency = Agencies::findOne(['tenant_id' => Yii::$app->user->identity->tenant->id]);

        if (!empty($agency->logo)) {
            if (file_exists($agency->logo))
                FileHelper::unlink($agency->logo);
        }

        $agency->logo = $path . '/' . $picture_name;

        return ($agency->save() ? $agency->logo : false);
    }
}