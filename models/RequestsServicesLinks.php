<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "requests_services_links".
 *
 * @property int $id
 * @property int $request_id
 * @property int $service_id
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property RequestsServicesAdditionalLinks[] $requestsServicesAdditionalLinks
 * @property Requests $request
 * @property Services $service
 * @property Tenants $tenant
 * @property RequestsServicesValues[] $requestsServicesValues
 */
class RequestsServicesLinks extends \yii\db\ActiveRecord
{

    const MAIN_ITEM = 'request_id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests_services_links';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'service_id', 'tenant_id'], 'required'],
            [['request_id', 'service_id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['request_id', 'service_id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requests::className(), 'targetAttribute' => ['request_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'Request ID',
            'service_id' => 'Service ID',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsServicesAdditionalLinks()
    {
        return $this->hasMany(RequestsServicesAdditionalLinks::className(), ['link_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Requests::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsServicesValues()
    {
        return $this->hasMany(RequestsServicesValues::className(), ['link_id' => 'id'])->with('field','service');
    }


    public static function getFields($request_id = null,$link_id = null)
    {
        $query = RequestsServicesLinks::find();
        if(!is_null($request_id))
            $query->where(['request_id'=>intval($request_id)]);

        if(!is_null($link_id))
            $query->where(['id'=>intval($link_id)]);

        $links = $query->asArray()->all();
        if(empty($links))
            return [];

        $ids = array_column($links,'id');
        $values = RequestsServicesValues::find()->where(['in','link_id',$ids])->asArray()->all();
        $f_ids = array_column($values,'field_id');
        $s_ids = array_column($values,'service_id');
        $fields = ServicesFieldsDefault::find()->where(['in','id',$f_ids])
            ->translate(Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        $services = Services::find()->where(['in','id',$s_ids])
            ->translate(Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        $fields = array_combine(array_column($fields,'id'),$fields);
        $services = array_combine(array_column($services,'id'),$services);

        $positions = ServicesFields::find()->where(['in','id_field',$f_ids])->asArray()->all();
        $positions = array_combine(array_column($positions,'id_field'),array_column($positions,'position'));

        foreach ($values as $i => $value)
        {
            $values[$i]['service'] = $services[$value['service_id']];
            $values[$i]['field'] = $fields[$value['field_id']];
            $values[$i]['field']['position'] = $positions[$value['field_id']];
        }

        foreach ($links as $i => $link)
        {
            $value = array_filter($values,function($var) use ($link) {
                return $var['link_id']==$link['id'];
            });
            $links[$i]['service'] = $services[$link['service_id']];
            $links[$i]['fields'] = $value;
        }

        if(!is_null($link_id) && count($links)==1)
            return $links[0];
        return $links;
    }
}
