<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property string $iso
 * @property bool $activated
 *
 */
class Languages extends \yii\db\ActiveRecord
{
    const DEFAULT_LANGUAGE_ISO = 'en';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'original_name', 'iso'], 'required'],
            [['activated'], 'boolean'],
            [['name', 'original_name'], 'string', 'max' => 250],
            [['iso'], 'string', 'max' => 2],
            [['iso'], 'unique'],
            [['name'], 'unique'],
            [['original_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'original_name' => 'Original Name',
            'iso' => 'Iso',
            'activated' => 'Activated',
        ];
    }

    /**
     * @return Languages|null
     */
    public static function getDefault()
    {

        return Languages::findOne(['iso' => static::DEFAULT_LANGUAGE_ISO]);
    }

    /**
     * @param $iso
     * @return int
     * @throws \Exception
     */
    public static function getIdFromIso($iso)
    {
        if(!Yii::$app->hasProperty('session')){
            $res = Languages::findOne(['iso'=>$iso]);
            if(!is_null($res)){
                return $res->id;
            }
            throw new \Exception('Language '.$iso.' is not found!');
        }
        $session = Yii::$app->session;
        if($session->has('languages_iso')){
            $lgs = $session->get('languages_iso');
            if(isset($lgs[$iso]))
                return $lgs[$iso]->id;
            $lgs[$iso] = Languages::findOne(['iso'=>$iso]);
            if(is_null($lgs[$iso]))
                throw new \Exception('Language '.$iso.' is not found!');
            $session->set('languages_iso',$lgs);
            return $lgs[$iso]->id;
        }
        $res = Languages::findOne(['iso'=>$iso]);
        if(!is_null($res)){
            $session->set('languages_iso',[$iso=>$res]);
            return $res->id;
        }
        throw new \Exception('Language '.$iso.' is not found!');
    }

    /**
     * @param $iso
     * @return Languages|null
     * @throws \Exception
     */
    public static function getByIso($iso)
    {
        if(!Yii::$app->hasProperty('session')){
            $res = Languages::findOne(['iso'=>$iso]);
            if(!is_null($res)){
                return $res;
            }
            throw new \Exception('Language '.$iso.' is not found!');
        }
        $session = Yii::$app->session;
        if($session->has('languages_iso')){
            $lgs = $session->get('languages_iso');
            if(isset($lgs[$iso]))
                return $lgs[$iso];
            $lgs[$iso] = Languages::findOne(['iso'=>$iso]);
            if(is_null($lgs[$iso]))
                return null;
            $session->set('languages_iso',$lgs);
            return $lgs[$iso];
        }
        $res = Languages::findOne(['iso'=>$iso]);
        if(!is_null($res)){
            $session->set('languages_iso',[$iso=>$res]);
            return $res;
        }
        return null;
    }
}
