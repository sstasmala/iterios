<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs_email_request".
 *
 * @property int $id
 * @property string $event
 * @property int $log_email_id
 * @property int $email_provider_id
 * @property string $method
 * @property string $headers
 * @property string $body
 * @property string $url
 * @property int $created_at
 *
 * @property LogsEmail $logEmail
 */
class LogsEmailRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs_email_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event', 'log_email_id', 'method', 'headers', 'url', 'created_at'], 'required'],
            [['created_at'], 'default', 'value' => null],
            [['log_email_id', 'created_at', 'email_provider_id'], 'integer'],
            [['headers', 'body'], 'string'],
            [['event', 'method', 'url'], 'string', 'max' => 255],
            [['log_email_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogsEmail::className(), 'targetAttribute' => ['log_email_id' => 'id']],
            [['email_provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailProviders::className(), 'targetAttribute' => ['email_provider_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event' => 'Event',
            'log_email_id' => 'Log eMail',
            'email_provider_id' => 'eMail Provider',
            'method' => 'Method',
            'headers' => 'Headers',
            'body' => 'Body',
            'url' => 'Url',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEmail()
    {
        return $this->hasOne(LogsEmail::className(), ['id' => 'log_email_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailProvider()
    {
        return $this->hasOne(EmailProviders::className(), ['id' => 'email_provider_id']);
    }
}
