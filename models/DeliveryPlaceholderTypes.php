<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery_placeholder_types".
 *
 * @property int $id
 * @property int $delivery_type_id
 * @property int $delivery_placeholder_id
 *
 * @property DeliveryPlaceholders $deliveryPlaceholder
 * @property DeliveryTypes $deliveryType
 */
class DeliveryPlaceholderTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_placeholder_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['delivery_type_id', 'delivery_placeholder_id'], 'default', 'value' => null],
            [['delivery_type_id', 'delivery_placeholder_id'], 'integer'],
            [['delivery_placeholder_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryPlaceholders::className(), 'targetAttribute' => ['delivery_placeholder_id' => 'id']],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryTypes::className(), 'targetAttribute' => ['delivery_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_type_id' => 'Delivery Type ID',
            'delivery_placeholder_id' => 'Delivery Placeholder ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryPlaceholder()
    {
        return $this->hasOne(DeliveryPlaceholders::className(), ['id' => 'delivery_placeholder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryType()
    {
        return $this->hasOne(DeliveryTypes::className(), ['id' => 'delivery_type_id']);
    }
}
