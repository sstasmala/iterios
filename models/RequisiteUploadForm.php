<?php
/**
 * RquisiteUploadForm.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models;


use yii\base\Model;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class RequisiteUploadForm extends Model
{
    /**
     * @var UploadedFile
     */

    public $requisite_picture;

    public function rules()
    {
        return [
            [['requisite_picture'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 10],
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate()) {
            $path = 'storage/company/' . \Yii::$app->user->identity->tenant->id;
            $picture_name = \Yii::$app->security->generateRandomString(14) . '.' . $this->requisite_picture->extension;

            if (FileHelper::createDirectory($path))
                if ($this->requisite_picture->saveAs($path . '/' . $picture_name))
                    return $this->savePicture($path, $picture_name);
        }

        return false;
    }

    /**
     * @param $path
     * @param $picture_name
     *
     * @return bool
     */
    private function savePicture($path, $picture_name)
    {
        Image::thumbnail($path . '/' . $picture_name, 300, 300)
            ->save($path . '/' . $picture_name, ['quality' => 100]);

        return $path . '/' . $picture_name;
    }
}