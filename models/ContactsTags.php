<?php

namespace app\models;

use app\models\log\BaseLogTranslationModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use app\components\notifications_now\NotificationActions;

/**
 * This is the model class for table "contacts_tags".
 *
 * @property int $id
 * @property int $tag_id
 * @property int $contact_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Contacts $contact
 * @property Tags $tag
 */
class ContactsTags extends BaseLogTranslationModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {   
        // action (class), id contact, id tag
        // $action = new NotificationActions('contact_tag_add', 37, 102);
        // $action->send();

        return 'contacts_tags';
    }

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tag_id', 'contact_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_id' => 'Tag ID',
            'contact_id' => 'Contact ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if (!$insert) return parent::afterSave($insert, $changedAttributes);


        $model = NotificationsTrigger::findOne(['action' => BaseNotifications::CONTACTS_TAGS_TAG_ID, 'element_id' => $this->tag_id, 'contact_id' => $this->contact_id]);

        if (empty($model)) {
            $model = new NotificationsTrigger();
            $model->created_at = time();
            $model->action = BaseNotifications::CONTACTS_TAGS_TAG_ID;
            $model->element_id = $this->tag_id;
            $model->contact_id = $this->contact_id;
        }

        $contact = Contacts::find()->where(['id' => $this->contact_id])->one();

        // save to process check
        $model->agent_id = $contact->responsible;
        $model->tenant_id = $contact->tenant_id;
        $model->trigger_time = time();
        $model->status = 0;
        $model->type = 1;
        $model->data = Json::encode([]); //  to do adding data
        $model->updated_at = time();
        $model->save();
        
        // action (class), id contact, id tag
        $action = new NotificationActions(NotificationActions::CONTACT_TAG_ADD, $this->contact_id, $this->tag_id);
        $action->send();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() 
    {
        $where = ['action' => BaseNotifications::CONTACTS_TAGS_TAG_ID, 'element_id' => $this->tag_id, 'contact_id' => $this->contact_id];

        // save to process check
        $trigger = NotificationsTrigger::find()->where($where)->one();
        if (!empty($trigger)) {
            NotificationsCourse::deleteAll(['trigger_id' => $trigger->id]);
            $trigger->delete();
        }

        return parent::afterDelete();
    }
}
