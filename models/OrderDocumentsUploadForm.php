<?php
/**
 * models/OrderDocumentsUploadForm.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\models;

use app\helpers\TranslationHelper;
use Imagine\Image\Box;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;

class OrderDocumentsUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file_instances;

    public function __construct(array $config = [])
    {
        $this->file_instances = UploadedFile::getInstancesByName('documents');

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [
                [
                    'file_instances'
                ],
                'file',
                'skipOnEmpty' => false,
                'checkExtensionByMimeType' => false,
                'mimeTypes' => 'image/*,application/pdf,text/plain,application/msword,application/docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/rtf,text/rtf,text/richtext',
                'extensions' => 'png, jpg, gif, bmp, txt, pdf, doc, docx, xls, xlsx, ppt, pptx, rtf',
                'maxFiles' => 10,
                'maxSize' => 1024 * 1024 * 25,
                'message' => TranslationHelper::getTranslation('order_document_upload_not_upl_error', \Yii::$app->user->identity->tenant->language->iso),
                'tooBig' => TranslationHelper::getTranslation('order_document_upload_too_big_error', \Yii::$app->user->identity->tenant->language->iso),
                'tooMany' => TranslationHelper::getTranslation('order_document_upload_too_many_error', \Yii::$app->user->identity->tenant->language->iso),
                'wrongExtension' => TranslationHelper::getTranslation('order_document_upload_wrong_file_ext_error', \Yii::$app->user->identity->tenant->language->iso),
                'wrongMimeType' => TranslationHelper::getTranslation('order_document_upload_wrong_file_ext_error', \Yii::$app->user->identity->tenant->language->iso)
            ],
        ];
    }

    /**
     * @param \app\models\Orders $order
     *
     * @return array
     * @throws \yii\base\Exception
     */
    public function upload(Orders $order): array
    {
        if ($this->validate()) {
            $path = 'storage/tenants/' . \Yii::$app->user->identity->tenant->id .'/orders/'.$order->id.'/documents';

            $documents = [];

            foreach ($this->file_instances as $file) {
                $document_name = \Yii::$app->security->generateRandomString(14) . '.' . $file->extension;

                if (FileHelper::createDirectory($path)) {
                    if ($file->saveAs($path . '/' . $document_name)) {
                        $documents[] = $this->saveFileToDataBase($order, $file, $path . '/' .$document_name);
                    }
                }
            }

            return $documents;
        }

        throw new BadRequestHttpException($this->getFirstError('file_instances'), 400);
    }

    /**
     * @param \app\models\Orders    $order
     * @param \yii\web\UploadedFile $file
     * @param                       $file_path
     *
     * @return \app\models\OrderUploadDocuments
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \yii\web\BadRequestHttpException
     */
    private function saveFileToDataBase(Orders $order, UploadedFile $file, $file_path): OrderUploadDocuments
    {
        $allowedImgTypes = array('image/png', 'image/jpeg', 'image/gif', 'image/bmp');

        if (\in_array($file->type, $allowedImgTypes, true)) {
            $image = Image::getImagine()->open($file_path);

            $newWidth = $image->getSize()->getWidth() > 1200 ? 1200 : $image->getSize()->getWidth();

            if ($newWidth < 1200) {
                $newHeight = $image->getSize()->getHeight();
            } else {
                $ratio = $image->getSize()->getHeight() / $image->getSize()->getWidth();

                $newHeight = round($newWidth * $ratio);
            }

            $image->resize(new Box($newWidth, $newHeight))
                ->save($file_path, ['quality' => 80]);
        }

        $model = new OrderUploadDocuments();
        $model->order_id = $order->id;
        $model->title = substr($file->name, 0, strrpos($file->name, '.'));
        $model->file_type = $file->type;
        $model->file_size = $file->size;
        $model->file_path = $file_path;

        if (!$model->save())
            throw new BadRequestHttpException('Document '.$file->name.' don\'t save!', 400);

        return $model;
    }
}