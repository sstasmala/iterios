<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tasks_types".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string  $name               [varchar(255)]
 * @property string  $country_id         [integer]
 * @property string  $logo               [varchar(255)]
 * @property string  $code               [varchar(255)]
 * @property string  $authorization_type [varchar(255)]
 * @property string  $supplier_type_id   [integer]
 * @property string  $description
 * @property string  $web_site           [varchar(255)]
 * @property string  $company            [varchar(255)]
 * @property string  $functions          [varchar(255)]
 */
class Suppliers extends BaseTranslationModel
{
    const TRANSLATABLE = [
        'name',
        'description'
    ];

    const AUTHORIZATION_TYPES = ['login&pass','api-token'];

    protected $primaryKey = 'id';

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suppliers';
    }

    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'functions', 'logo'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'country_id','supplier_type_id'], 'integer'],
            [['name', 'code','logo', 'authorization_type', 'web_site', 'company', 'functions'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'logo' => 'Logo',
            'code' => 'Code',
            'authorization_type' => 'Authorization Type',
            'country_id' => 'Country',
            'supplier_type_id' => 'Supplier type',
            'functions' => 'Supplier Function',
            'web_site' => 'Website',
            'company' => 'Company',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getType()
    {
        return $this->hasOne(SupplierType::className(), ['id' => 'supplier_type_id']);
    }

    public static function getAuthorizationTypes()
    {
        $type_map = [];
        $types = self::AUTHORIZATION_TYPES;
        foreach ($types as $value) $type_map[$value] = $value;
        return $type_map;
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getCredentials()
    {
        return $this->hasMany(SuppliersCredentials::className(), ['supplier_id' => 'id']);
    }
}
