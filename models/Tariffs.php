<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tariffs".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $tariff_duration
 * @property int $tariff_start
 * @property int $tariff_end
 * @property string $country
 * @property int $days_before_stop
 * @property string $price
 * @property int $need_act
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property FeaturesTariffs[] $featuresTariffs
 */
class Tariffs extends BaseTranslationModel
{
    const TRANSLATABLE = [
        'name','description'
    ];


    const DURATIONS = [
        0 => '1 month',
        1 => '6 months',
        2 => '1 year'
    ];

    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariffs';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','days_before_stop','price'], 'required'],
            [['description', 'country'], 'string'],
            [['tariff_duration', 'tariff_start', 'tariff_end',  'need_act', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tariff_duration', 'days_before_stop', 'need_act', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['tariff_start', 'tariff_end'],'safe'],
            [['days_before_stop','price'], 'default', 'value' => 0],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'tariff_duration' => 'Tariff Duration',
            'tariff_start' => 'Tariff Start',
            'tariff_end' => 'Tariff End',
            'country' => 'Country',
            'days_before_stop' => 'Days Before Stop',
            'price' => 'Price',
            'need_act' => 'Need Act',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeaturesTariffs()
    {
        return $this->hasMany(FeaturesTariffs::className(), ['tariff_id' => 'id']);
    }
}
