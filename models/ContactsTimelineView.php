<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts_timeline_view".
 *
 * @property int $id
 * @property string $value
 * @property int $contact_id
 * @property int $request_id
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property string $table_name
 * @property int $item_id
 * @property int $group_id
 * @property int $tenant_id
 * @property string $action
 * @property string $data
 * @property string $title
 * @property string $decription
 * @property bool $status
 * @property int $type_id
 * @property string $type
 */
class ContactsTimelineView extends \yii\db\ActiveRecord
{
    const TYPE_REQUEST_NOTE = 'request_note';
    const TYPE_CONTACT_NOTE = 'contact_note';
    const TYPE_LOG = 'log';
    const TYPE_TASK = 'task';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts_timeline_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'request_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'item_id', 'group_id', 'tenant_id', 'type_id'], 'default', 'value' => null],
            [['id', 'contact_id', 'request_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'item_id', 'group_id', 'tenant_id', 'type_id'], 'integer'],
            [['value', 'table_name', 'action', 'data', 'title', 'decription', 'type'], 'string'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'contact_id' => 'Contact ID',
            'request_id' => 'Request ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'table_name' => 'Table Name',
            'item_id' => 'Item ID',
            'group_id' => 'Group ID',
            'tenant_id' => 'Tenant ID',
            'action' => 'Action',
            'data' => 'Data',
            'title' => 'Title',
            'decription' => 'Decription',
            'status' => 'Status',
            'type_id' => 'Type ID',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Requests::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType()
    {
        return $this->hasOne(TasksTypes::className(), ['id' => 'type_id']);
    }
}
