<?php

namespace app\models;

use Yii;
use app\models\log\BaseLogModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "profile_notifications".
 *
 * @property int $id
 * @property int $user_id
 * @property int $level
 * @property int $is_email
 * @property int $is_notice
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $action
 *
 * @property ProfileNotificationsTags[] $profileNotificationsTags
 */
class ProfileNotifications extends BaseLogModel
{
    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    const LEVELS = [
        0 => 'Моя запись',
        1 => 'Любая запись',
        //2 => 'Запись моей роли'
    ];

    const LEVELS_ADMIN = [
        0 => 'My owner',
        1 => 'Other owner',
        //2 => 'Ovner by role'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'level', 'is_email', 'is_notice', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['user_id', 'level', 'is_email', 'is_notice', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['action'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'level' => 'Level',
            'is_email' => 'Is Email',
            'is_notice' => 'Is Notice',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'action' => 'Action',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ProfileNotificationsTags::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(ProfileNotificationsStatuses::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
