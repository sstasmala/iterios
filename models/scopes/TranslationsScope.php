<?php
/**
 * TransaltionsScope.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\scopes;


use app\models\Languages;
use app\models\Translations;
use yii\db\ActiveQuery;

class TranslationsScope extends ActiveQuery
{
    public function translate($lang = null, $map = [], $translatable = [])
    {
        $model = $this->modelClass;
        $attributes = $model::getTableSchema()->getColumnNames();
        $tr_attributes = $model::TRANSLATABLE;
        if(!empty($translatable))
            $tr_attributes = $translatable;
        if(is_null($lang))
            $lang = Languages::getDefault();
        if(empty($map))
            $clear_attributes = array_diff($attributes,$tr_attributes);
        else
            $clear_attributes = $this->select;
        foreach ($tr_attributes as $a)
        {
            $name_t = Translations::tableName();
            $result_name = $a;
            $pos = null;
            if(!empty($map) && isset($map[$a])){
                $pos = \array_search($map[$a],$clear_attributes);
                // $result_name = $clear_attributes[$pos];
            }
            $name = $model::tableName();
            $sql = 'SELECT value from '.$name_t.' where key LIKE \''.$name.'.'.$a.'.\' || "'.$name
                .'".id and "'.$name_t.'".lang_id = '.Languages::getIdFromIso($lang).' LIMIT 1';
            $sql = '(('.$sql.')) as "'.$result_name.'"';
            if($pos!=null)
                $clear_attributes[$pos] = $sql;
            else
                $clear_attributes[] = $sql;
        }
        return $this->select($clear_attributes);
    }
}