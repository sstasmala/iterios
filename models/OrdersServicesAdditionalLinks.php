<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_services_additional_links".
 *
 * @property int $id
 * @property int $link_id
 * @property int $service_id
 * @property int $type
 *
 * @property OrdersServicesLinks $link
 * @property Services $service
 * @property OrdersServicesValues[] $ordersServicesValues
 */
class OrdersServicesAdditionalLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_services_additional_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'service_id'], 'required'],
            [['link_id', 'service_id'], 'default', 'value' => null],
            [['link_id', 'service_id', 'type'], 'integer'],
            [['type'], 'default', 'value' => 0],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersServicesLinks::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'service_id' => 'Service ID',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(OrdersServicesLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersServicesValues()
    {
        return $this->hasMany(OrdersServicesValues::className(), ['additional_link_id' => 'id']);
    }
}
