<?php

namespace app\models;

use app\models\log\BaseLogModel;
use Yii;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property int $lang_id
 *
 * @property Languages $lang
 */
class Translations extends BaseLogModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'lang_id'], 'required'],
            [['value'], 'string'],
//            [['lang_id'], 'default', 'value' => null],
            [['lang_id'], 'integer'],
            [['key'], 'string', 'max' => 250],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @param $key
     * @param $value
     * @param $language
     * @param $model
     * @param null $group_id
     * @return mixed
     */
    public static function saveTranslation($key, $value, $language, $model, $group_id = null)
    {
        if ($group_id != null) {
            $model->group_id = $group_id;
        }
        $model->key = $key;
        $model->value = $value;
        $model->lang_id = $language->id;
        return $model->save();
    }

    /**
     * @param $key
     * @param $lang
     * @param bool $default
     * @return null|string
     * @throws \Exception
     */
    public static function getTranslation($key, $lang, $default = false)
    {
        $language = Languages::getByIso($lang);

        if(is_null($language))
            throw new \Exception('Language not found!');

        $model = Translations::findOne(['key'=>$key,'lang_id'=>$language->id]);
        if(is_null($model)){
            if($default)
            {
                $language = Languages::getByIso(Languages::DEFAULT_LANGUAGE_ISO);
                $model =   Translations::findOne(['key'=>$key,'lang_id'=>$language->id]);
                if(is_null($model)){
                    $model = Translations::findOne(['key'=>$key]);
                    if(is_null($model))
                        return null;
                }
            }
            else
                return null;
        }
        return $model->value;
    }
}
