<?php
/**
 * FakeTenant.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\fake;


class FakeTenant
{
    public $id;
    public $name;
    public $owner_id;
    public $language_id;
    public $language;
    public $timezone;
    public $date_format;
    public $time_format;

    public function __construct()
    {
        $this->id = 1;
        $this->name = 'Fake tenant';
        $this->owner_id = 1;
        $this->language_id = 1;
        $this->timezone = 'UTC';
        $this->date_format = 'd.m.Y';
        $this->time_format = 'H:i:s';
    }
}