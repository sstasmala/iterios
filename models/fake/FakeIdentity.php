<?php
/**
 * FakeIdentity.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\fake;


use yii\web\IdentityInterface;

class FakeIdentity implements IdentityInterface
{

    public $id;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $email;
    public $phone;
    public $photo;
    public $tenant;
    public $tenants;

    public function __construct()
    {
        $this->id = 1;
        $this->first_name = 'Ivan';
        $this->last_name = 'Ivanov';
        $this->middle_name = 'Ivanovich';
        $this->email = 'example@email.com';
        $this->phone = '+380123456789';
        $this->photo = 'metronic/assets/app/media/img/users/user4.jpg';

        $tenant = new FakeTenant();
        $language = new FakeLanguage();

        $tenant->language = $language;
        $this->tenant = $tenant;
        $this->tenants = [$tenant];
    }

    public static function findIdentity($id)
    {
        return new self();
    }

    public function getId()
    {
        return 1;
    }

    public function getAuthKey()
    {
        return 'aaaaaaaaaaaaaaaaaaaa';
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return new self();
    }

    public function getTenants()
    {
        return [$this->tenant];
    }
}