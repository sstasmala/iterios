<?php
/**
 * FakeLanguage.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\models\fake;


class FakeLanguage
{
    public $id;
    public $name;
    public $original_name;
    public $iso;

    public function __construct()
    {
        $this->id = 1;
        $this->name = 'Russian';
        $this->original_name = 'русский язык';
        $this->iso = 'ru';
    }
}