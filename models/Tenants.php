<?php

namespace app\models;

use app\models\base\BaseModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tenants".
 *
 * @property int $id
 * @property string $name
 * @property int $owner_id
 * @property int $language_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $use_webmail
 * @property int $tariff_id
 * @property string $timezone
 * @property string $country
 * @property string $mailer_config
 * @property string $reminders_email
 * @property string $reminders_from_who
 * @property string $date_format
 * @property string $time_format
 * @property boolean $system_tags
 * @property string $week_start
 * @property string $currency_display
 *
 * @property AgentsMapping[] $agentsMappings
 * @property Languages $language
 * @property User $owner
 */
class Tenants extends BaseModel
{
    private $fake_language;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenants';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'language_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['owner_id', 'language_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'main_currency','use_webmail', 'payment_method'], 'integer'],
            [['name', 'usage_currency', 'legal_name'], 'string', 'max' => 255],
            [['timezone'],'string','max'=>50],
            [['balance'], 'number'],
            [['country', 'week_start'],'string','max'=>10],
            [['reminders_email'],'string','max'=>129],
            [['reminders_from_who','mailer_config'],'string'],
            [['date_format','time_format'],'string','max'=>50],
            [['timezone'],'default','value'=>'UTC'],
            [['date_format'],'default','value'=>'d.m.Y'],
            [['time_format'],'default','value'=>'H:i:s'],
            [['currency_display'],'default','value'=>'symbol'],
            ['currency_display','in','range'=>['symbol','code']],
            [['system_tags', 'status'], 'boolean'],
            [['system_tags'], 'default', 'value' => true],
            [['status'], 'default', 'value' => false],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'id' => 'ID',
            'name' => 'Name',
            'owner_id' => 'Owner ID',
            'language_id' => 'Language ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'timezone' => 'Timezone',
            'date_format' => 'Date Format',
            'time_format' => 'Time Format',
            'country' => 'Country',
            'reminders_email' => 'Reminders Email',
            'reminders_from_who' => 'Reminders From Who',
            'usage_currency' => 'Usage Currency',
            'main_currency' => 'Main Currency',
            'system_tags' => 'System Tags',
            'status' => 'Status',
            'mailer_config' => 'Mailer Config',
            'use_webmail' => 'Use Webmail',
            'week_start' => 'Week Start',
            'legal_name' => 'Legal Name',
            'balance' => 'Balance',
            'tariff_id' => 'Tariff ID',
            'payment_method' => 'Payment Method',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentsMappings()
    {
        return $this->hasMany(AgentsMapping::className(), ['tenant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        if (Yii::$app->params['stage'] == 'prot')
            return $this->fake_language;

        return $this->hasOne(Languages::className(), ['id' => 'language_id']);
    }

    public function setLanguage(Languages $language)
    {
        $this->fake_language = $language;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->via('agentsMappings');
    }

    public function checkUser()
    {
        return !is_null(AgentsMapping::findOne(['tenant_id' => $this->id, 'user_id' => Yii::$app->user->id])) ? true : false;
    }

    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }
}
