<?php
/**
 * UserAdminView.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\models;

/**
 * This is the model class for view "users_admin_view".
 *
 * @property int $system_admin
 */
class UserAdminView extends User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_admin_view';
    }
}