<?php

namespace app\models;

use app\models\log\BaseLogTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $value
 * @property string $type
 * @property int $tenant_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property ContactsTags[] $contactsTags
 * @property Tenants $tenant
 */
class Tags extends BaseLogTranslationModel
{
    const TRANSLATABLE = ['value'];
    protected $primaryKey = 'id';
    protected $saved = null;

    const TYPE_SYSTEM = 'system';
    const TYPE_CUSTOM = 'custom';

    public function behaviors()
    {

        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['type'], 'string'],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'type' => 'Type',
            'tenant_id' => 'Tenant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsTags()
    {
        return $this->hasMany(ContactsTags::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
