<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "commercial_proposals".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $status
 * @property int $status_created_at
 * @property string $link_code
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $template
 * @property string $log_id [integer]
 */
class CommercialProposals extends \yii\db\ActiveRecord
{
    const TYPE_EMAIL = 'email';
    const TYPE_SMS = 'sms';
    const TYPE_LINK = 'link';

    const TYPES = [
        self::TYPE_EMAIL,
        self::TYPE_SMS,
        self::TYPE_LINK
    ];

    const STATUS_SENDED = 'sended';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_OPENED = 'opened';
    const STATUS_CLICKED = 'clicked';

    const STATUSES = [
        self::STATUS_SENDED,
        self::STATUS_DELIVERED,
        self::STATUS_OPENED,
        self::STATUS_CLICKED
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commercial_proposals';
    }

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_created_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['log_id', 'status_created_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'type', 'status', 'link_code'], 'string', 'max' => 255],
            ['type', 'in', 'range' => static::TYPES],
            ['status', 'in', 'range' => static::STATUSES],
            [['template'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'log_id' => 'Log',
            'status' => 'Status',
            'status_created_at' => 'Status Created At',
            'link_code' => 'Link Code',
            'template' => 'Template',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
