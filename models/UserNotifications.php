<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_notifications".
 *
 * @property int $id
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property string $system_notification
 * @property string $sms
 * @property int $trigger
 * @property string $trigger_time
 * @property string $task_name
 * @property string $condition
 * @property string $module
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property TenantNotifications[] $tenantNotifications
 */
class UserNotifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'module'], 'required'],
            [['body', 'system_notification', 'sms', 'trigger_time', 'condition'], 'string'],
            [['trigger', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['trigger', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'subject'], 'string', 'max' => 250],
            [['task_name'], 'string', 'max' => 255],
            [['module'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'body' => 'Body',
            'system_notification' => 'System Notification',
            'sms' => 'Sms',
            'trigger' => 'Trigger',
            'trigger_time' => 'Trigger Time',
            'task_name' => 'Task Name',
            'condition' => 'Condition',
            'module' => 'Module',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantNotifications()
    {
        return $this->hasMany(TenantNotifications::className(), ['user_notification_id' => 'id']);
    }
}
