<?php

namespace app\models;

use app\helpers\DateTimeHelper;
use app\models\base\BaseModel;
use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\components\behaviors\TimestampToTenantTimeZoneBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;



class SendSms extends BaseModel
{
//    const TRANSLATABLE = ['name'];
//    protected $primaryKey = 'id';

    const STATUS = ['ready', 'sended', 'error', 'draft'];

    const STATUS_READY = 'ready';
    const STATUS_SENDED = 'sended';
    const STATUS_ERROR = 'error';
    const STATUS_DRAFT = 'draft';

    public $created_at_formatted;

    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
            [
                'class' => TimestampToTenantTimeZoneBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => 'created_at_formatted',
                    ActiveRecord::EVENT_AFTER_FIND => 'created_at_formatted',
                ],
                'timeAttribute' => 'created_at'
            ],
        ];

        return array_merge($behaviors, parent::behaviors());
    }


    public static function tableName()
    {
        return 'send_sms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'count_phones', 'count_delivered', 'count_not_delivered', 'created_at', 'updated_at', 'created_by', 'updated_by', 'provider_id'], 'default', 'value' => null],
            [['count_phones', 'count_delivered', 'count_not_delivered', 'created_at', 'updated_at', 'created_by', 'updated_by', 'provider_id', 'tenant_id'], 'integer'],
            [['name', 'segment_relation_id', 'text', 'alpha_name', 'status', 'segment_copy'], 'string', 'max' => 255],
            [['segmentsArr', 'created_at_formatted'], 'safe'],

        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $decodeSegments = json_decode($this->segment_relation_id);
            $delivered_segm_copy = SegmentsRelations::find()
                ->select([
                    'segments_relations.id_segment',
                    'segments.name',
                ])
                ->distinct()
                ->leftJoin(Segments::tableName(), "segments_relations.id_segment = segments.id")
                ->where(['segments_relations.id' => $decodeSegments]);

            $delivered_segm_copy = $delivered_segm_copy->asArray()->all();
            $in = [];
            $assoc = [];
            foreach($delivered_segm_copy as $seg) {
                $in[] = 'segments.name.' . $seg['id_segment'];
                $assoc['segments.name.' . $seg['id_segment']] = $seg['id_segment'];
            }

            $lang_segments_list = Translations::find()
                ->select([
                    'translations.key',
                    'translations.value',
                    'translations.lang_id'
                ])
                ->where(array('in', 'key', $in))
                ->asArray()->all();

            foreach($lang_segments_list as $key_el => $elem_list) {
                $str_repl = str_replace('segments.name.','',$elem_list['key']);
                $lang_segments_list[$key_el]['el_id'] = (int) $str_repl;
                foreach($delivered_segm_copy as $key_deliv => $deliv) {
                    if($delivered_segm_copy[$key_deliv]['id_segment'] == $lang_segments_list[$key_el]['el_id'])
                        $delivered_segm_copy[$key_deliv][$elem_list['lang_id']] = $elem_list;
                }
            }

            $delivered_segm_copy = json_encode($delivered_segm_copy);
            $this->segment_copy = $delivered_segm_copy;

            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'count_phones' => 'Count Phones',
            'count_delivered' => 'Count Delivered',
            'count_not_delivered' => 'Count Not Delivered',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'segments' => 'Segments',
            'tenant_id' => 'Tenant',
            'text' => 'Text',
            'alpha_name' => 'Alpha Name',
            'provider_id' => 'Provider ID',
            'segment_copy' => 'Segment Copy'
        ];
    }

    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['id' => 'provider_id']);
    }

    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    public function getProviderCredential()
    {
        return $this->hasOne(ProvidersCredentials::className(), ['provider_id' => 'provider_id']);
    }

    public function setSegmentsArr($value)
    {
        $this->segment_relation_id = json_encode($value);
    }

    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    static public function send()
    {
        $readySms = SendSms::find()
            ->with('provider')
            ->where(['status' => SendSms::STATUS_READY])
            ->all();

        foreach ($readySms as $sms) {
            $providerClass = $sms->provider->provider_class;
            if (isset($providerClass)) {
                $segmentRelIds = json_decode($sms->segment_relation_id, 1);

                $phones = SegmentsResultList::getPhonesFromSegmentsRelationId($segmentRelIds);
                if (isset($phones)) {
                    $phonesForSend = ArrayHelper::getColumn($phones, 'phone');
                    $phonesForSend = array_unique($phonesForSend);

                    $providerCredentials = ProvidersCredentials::find()
                        ->where(['provider_id' => $sms->provider_id])
                        ->andWhere(['=', 'tenant_id', $sms->tenant_id])
                        ->one();

                    if (isset($providerCredentials)) {
                        $smsPackage = new $providerClass($providerCredentials->id);
                        $smsPackage->setAbonentList($phonesForSend);
                        $smsPackage->setBody($sms->text);
                        $smsPackage->send(); //FOR TEST!!!!!
/*                        $smsPackage->responce = "<?xml version=\"1.0\" encoding=\"utf-8\"?><response><status>OK</status><text>Сообщения отправлены</text><ids><mess tel=\"380500688610\">485058991</mess><mess tel=\"380638184650\">485058992</mess></ids></response>";*/
                        $smsPackage->saveResultToDb($phones, $sms->id);
                        echo "sended phones: " . var_export($phonesForSend);

                    } else {
                        echo "Provider Credentials does not exist";
                    }
                } else {
                    echo "For segment Relation Id: " . implode(';', $segmentRelIds) . " no find phones";
                }
            } else {
                echo "For send sms id $sms->id don't find class provider: " . $sms->provider->provider_class;
            }
        }
    }
}
