<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "documents_template".
 *
 * @property int $id
 * @property string $title
 * @property string $languages
 * @property int $documents_type_id
 * @property int $suppliers_id
 * @property string $body
 * @property string $type
 * @property integer $tenant_id
 * @property int     $created_at [bigint]
 * @property int     $updated_at [bigint]
 * @property string  $created_by [integer]
 * @property string  $updated_by [integer]
 */
class DocumentsTemplate extends \yii\db\ActiveRecord
{
    const SYSTEM_TYPE = 'system';
    const PUBLIC_TYPE = 'public';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return array_merge($behaviors,parent::behaviors());
    }

    public static function tableName()
    {
        return 'documents_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['documents_type_id', 'title', 'languages', 'type'], 'required'],
            [['suppliers_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['documents_type_id', 'suppliers_id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['body'], 'string'],
            [['title', 'languages', 'type'], 'string', 'max' => 255],
            [['suppliers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Suppliers::className(), 'targetAttribute' => ['suppliers_id' => 'id']],
            [['documents_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsType::className(), 'targetAttribute' => ['documents_type_id' => 'id']],
            [['languages'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['languages' => 'iso']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['tenant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'languages' => 'Languages',
            'documents_type_id' => 'Documents Type',
            'suppliers_id' => 'Suppliers',
            'body' => 'Body',
            'type' => 'Type',
            'tenant_id' => 'Tenant',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getTypes()
    {
        return $this->hasOne(DocumentsType::className(), ['id' => 'documents_type_id']);
    }

    public function getSupplier()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'suppliers_id']);
    }

    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['iso' => 'languages']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
