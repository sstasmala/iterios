<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services_fields".
 *
 * @property int $id
 * @property int $id_service
 * @property int $id_field
 * @property int $position
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Services $service
 * @property ServicesFieldsDefault $field
 */
class ServicesFields extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_service', 'id_field'], 'required'],
            [['id_service', 'id_field', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['id_service', 'id_field', 'position', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['position'], 'default', 'value' => 0],
            [['id_service'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['id_service' => 'id']],
            [['id_field'], 'exist', 'skipOnError' => true, 'targetClass' => ServicesFieldsDefault::className(), 'targetAttribute' => ['id_field' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_service' => 'Id Service',
            'id_field' => 'Id Field',
            'position' => 'Position',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(ServicesFieldsDefault::className(), ['id' => 'id_field']);
    }
}
