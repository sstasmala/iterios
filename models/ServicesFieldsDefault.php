<?php

namespace app\models;

use app\models\base\BaseTranslationModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "services_fields_default".
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property string $name
 * @property int $is_additional
 * @property int $in_header
 * @property int $is_default
 * @property int $size
 * @property int $position
 * @property int $only_for_order
 * @property string $variables
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class ServicesFieldsDefault extends BaseTranslationModel
{

    const TRANSLATABLE = [
        'name',
        'variables'
    ];

    protected $primaryKey = 'id';

    const TYPE_DATE = 'date';
    const TYPE_TIME = 'time';
    const TYPE_DROPDOWN = 'dropdown';
    const TYPE_MULTISELECT = 'multiselect';
    const TYPE_RADIO = 'radio';
    const TYPE_VARCHAR = 'varchar';
    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';
    const TYPE_LINK = 'link';
    const TYPE_DEPARTURE_CITY = 'departure_city';
    const TYPE_COUNTRY = 'country';
    const TYPE_CITY = 'city';
    const TYPE_HOTEL = 'hotel';
    const TYPE_STAR_RATING = 'star_rating';
    const TYPE_FOOD = 'food';
    const TYPE_AIRPORT = 'airport';
    const TYPE_DATE_STARTING = 'date_starting';
    const TYPE_DATE_ENDING = 'date_ending';
    const TYPE_NIGHTS_COUNT = 'nights_count';
    const TYPE_SUPPLIERS = 'suppliers';
    const TYPE_CURRENCY = 'currency';
    const TYPE_PAYMENT_TO_SUPPLIER = 'payment_to_supplier';
    const TYPE_COMMISSION = 'commission';
    const TYPE_SERVICES_PRICE = 'services_price';
    const TYPE_PROFIT = 'profit';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_SWITCH = 'switch';
    const TYPE_TOURISTS_COUNT = 'tourists_count';
    const TYPE_RESERVATION = 'reservation';
    const TYPE_RESERVATION_URL = 'reservation_url';
    const TYPE_EXCHANGE_RATE = 'exchange_rate';
    const TYPE_TIME_STARTING = 'time_starting';
    const TYPE_TIME_ENDING = 'time_ending';

    const TYPES = [
        self::TYPE_AIRPORT => 'Airport',
        self::TYPE_CITY => 'City',
        self::TYPE_COUNTRY => 'Country',
        self::TYPE_DATE => 'Date',
        self::TYPE_DATE_ENDING => 'Date ending',
        self::TYPE_DATE_STARTING => 'Date starting',
        self::TYPE_MULTISELECT => 'Multiselect',
        self::TYPE_RADIO => 'Radio',
        self::TYPE_DEPARTURE_CITY => 'Departure city',
        self::TYPE_DROPDOWN => 'Dropdown',
        self::TYPE_FOOD => 'Food',
        self::TYPE_NUMBER => 'Number',
        self::TYPE_HOTEL => 'Hotel',
        self::TYPE_TEXT => 'Text',
        self::TYPE_LINK => 'Link',
        self::TYPE_STAR_RATING => 'Star rating',
        self::TYPE_NIGHTS_COUNT => 'Nights count',
        self::TYPE_VARCHAR => 'Varchar',
        self::TYPE_TIME => 'Time',
        self::TYPE_SUPPLIERS => 'Suppliers',
        self::TYPE_CURRENCY => 'Currency',
        self::TYPE_PAYMENT_TO_SUPPLIER => 'Payment to supplier',
        self::TYPE_COMMISSION => 'Commission',
        self::TYPE_SERVICES_PRICE => 'Services price',
        self::TYPE_PROFIT => 'Profit',
        self::TYPE_CHECKBOX => 'Checkbox',
        self::TYPE_SWITCH => 'Switch',
        self::TYPE_TOURISTS_COUNT => 'Tourists count',
        self::TYPE_RESERVATION => 'Reservation',
        self::TYPE_RESERVATION_URL => 'Reservation url',
        self::TYPE_EXCHANGE_RATE => 'Exchange rate',
        self::TYPE_TIME_STARTING => 'Time starting',
        self::TYPE_TIME_ENDING => 'Time ending'
    ];

    public function behaviors()
    {
        $behaviors =  [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
            [
                'class'=> BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
            ],
        ];

        return $behaviors;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services_fields_default';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'type'], 'required'],
            [['is_additional', 'in_header', 'is_default','only_for_order','is_bold','in_creation_view'], 'default', 'value' => 0],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['is_additional', 'in_header', 'created_at', 'is_default', 'updated_at', 'created_by', 'updated_by',
                'position','size','only_for_order','is_bold','in_creation_view'], 'integer'],
            [['variables'], 'string'],
            [['size'], 'default', 'value' => 1],
            [['code', 'type','name'], 'string', 'max' => 250],
            ['code',function($attribute, $params){
               if($this->isNewRecord){
                   $sr = ServicesFieldsDefault::find()->where(['code'=>$this->$attribute])->one();
//                   $tr = UiTranslations::find()->where(['code'=>$this->$attribute])->one();
                   if(!is_null($sr))
                       $this->addError($attribute,"Code ".$this->$attribute." already exist in system!");
               }
               else {
                   if($this->getOldAttribute($attribute)!=$this->$attribute){
                       $sr = ServicesFieldsDefault::find()->where(['code'=>$this->$attribute])->one();
//                       $tr = UiTranslations::find()->where(['code'=>$this->$attribute])->one();
                       if(!is_null($sr))
                           $this->addError($attribute,"Code ".$this->$attribute." already exist in system!");
                   }
               }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'type' => 'Type',
            'is_additional' => 'Is Additional',
            'in_header' => 'In Header',
            'variables' => 'Variables',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
