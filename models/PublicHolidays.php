<?php

namespace app\models;

use Yii;
use app\models\base\BaseTranslationModel;

/**
 * This is the model class for table "public_holidays".
 *
 * @property int $id
 * @property int $day
 * @property int $mounts
 * @property string $name
 */
class PublicHolidays extends BaseTranslationModel
{
    const TRANSLATABLE = ['name'];
    protected $primaryKey = 'id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'public_holidays';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day', 'mounts','user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
            'mounts' => 'Mounts',
            'name' => 'Name',
            'user_id'=> 'User id',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
