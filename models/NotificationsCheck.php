<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications_check".
 *
 * @property int $id
 * @property string $action
 * @property int $element_id
 * @property string $value
 * @property int $value_int
 * @property string $data
 * @property int $status
 * @property int $created_at
 * @property int $cyclic_type
 */

/*
    cyclic_type = 1 = каждый год
*/
class NotificationsCheck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_check';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action'], 'required'],
            [['element_id', 'value_int', 'status', 'created_at', 'cyclic_type'], 'default', 'value' => null],
            [['element_id', 'value_int', 'status', 'created_at', 'cyclic_type'], 'integer'],
            [['data'], 'string'],
            [['action', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'element_id' => 'Element ID',
            'value' => 'Value',
            'value_int' => 'Value Int',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'cyclic_type' => 'Cyclic Type',
        ];
    }
}