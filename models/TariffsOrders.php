<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tariffs_orders".
 *
 * @property int $id
 * @property int $tariff_id
 * @property int $customer_id
 * @property int $duration_date
 * @property int $activation_date
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Tariffs $tariff
 * @property Tenants $customer
 */
class TariffsOrders extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SUSPENDED = 2;

    const STATUSES = [
        self::STATUS_NOT_ACTIVE => 'Not active',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_SUSPENDED => 'Suspended'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariffs_orders';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'tariff_id', 'customer_id'], 'required'],
            [['tariff_id', 'customer_id', 'duration_date', 'activation_date', 'status', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['status'], 'default', 'value' => 0],
            [['tariff_id', 'customer_id', 'duration_date', 'activation_date', 'status', 'created_at', 'updated_at'], 'integer'],
//            [['name'], 'string', 'max' => 255],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenants::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'customer_id' => 'Customer ID',
            'duration_date' => 'Duration Date',
            'activation_date' => 'Activation Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Tenants::className(), ['id' => 'customer_id']);
    }
}
