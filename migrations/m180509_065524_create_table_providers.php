<?php

use yii\db\Migration;

/**
 * Class m180509_065524_create_table_providers
 */
class m180509_065524_create_table_providers extends Migration
{
    public function safeUp()
    {
        $this->createTable('providers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'code' => $this->string(),
            'logo' => $this->string(),
            'provider_type_id'=>$this->integer(),
            'description' => $this->text(),
            'website' => $this->string(),
            'authorization_type' => $this->string(),
            'provider_class' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('providers');
    }
}
