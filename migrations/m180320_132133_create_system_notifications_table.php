<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications_system`.
 */
class m180320_132133_create_system_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('system_notifications', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'read' => $this->boolean()->defaultValue(false),
            'user_id' => $this->integer(),
            'tenant_id' => $this->integer(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint'
        ]);

        $this->addForeignKey('system_notifications_users_fk','system_notifications','user_id','users','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('system_notifications_tenants_fk','system_notifications','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('system_notifications_users_fk','system_notifications');
        $this->dropForeignKey('system_notifications_tenants_fk','system_notifications');

        $this->dropTable('system_notifications');
    }
}
