<?php

use yii\db\Migration;

/**
 * Class m180502_111432_create_table_segments_result_list
 */
class m180502_111432_create_table_segments_result_list extends Migration
{

    public function safeUp()
    {
        $this->createTable('segments_result_list', [
            'id' => $this->primaryKey(),
            'id_segment' => $this->integer(),
            'id_contact' => $this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
        ]);

        $this->createIndex(
            'idx-segments_result_list-id_segment',
            'segments_result_list',
            'id_segment'
        );

        $this->addForeignKey(
            'fk-segments_result_list-id_segment',
            'segments_result_list',
            'id_segment',
            'segments',
            'id',
            'NO ACTION'
        );

        $this->createIndex(
            'idx-segments_result_list-id_contact',
            'segments_result_list',
            'id_contact'
        );

        $this->addForeignKey(
            'fk-segments_result_list-id_contact',
            'segments_result_list',
            'id_contact',
            'contacts',
            'id',
            'NO ACTION'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropIndex(
            'idx-segments_result_list-id_segment',
            'segments_result_list'
        );

        $this->dropForeignKey(
            'fk-segments_result_list-id_segment',
            'segments_result_list'
        );

        $this->dropIndex(
            'idx-segments_result_list-id_contact',
            'segments_result_list'
        );

        $this->dropForeignKey(
            'fk-segments_result_list-id_contact',
            'segments_result_list'
        );

        $this->dropTable('segments_result_list');
    }


}
