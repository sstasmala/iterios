<?php

use yii\db\Migration;

/**
 * Class m180504_110301_update_table_notifications_now
 */
class m180504_110301_update_table_notifications_now extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_now', 'title_email', $this->string()->defaultValue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_now', 'title_email');
    }
}
