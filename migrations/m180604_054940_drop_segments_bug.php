<?php

use yii\db\Migration;

/**
 * Class m180604_054940_drop_segments_bug
 */
class m180604_054940_drop_segments_bug extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-segments_relations-id_segment',
            'segments_relations'
        );
        $this->dropForeignKey(
            'fk-segments_relations-id_tenant',
            'segments_relations'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addForeignKey(
            'fk-segments_relations-id_segment',
            'segments_relations',
            'id_segment',
            'segments',
            'id',
            'NO ACTION'
        );
        $this->addForeignKey(
            'fk-segments_relations-id_tenant',
            'segments_relations',
            'id_tenant',
            'segments',
            'id',
            'NO ACTION'
        );
    }
}
