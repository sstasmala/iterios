<?php

use yii\db\Migration;

/**
 * Class m180425_113308_update_table_base_notifications
 */
class m180425_113308_update_table_base_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('base_notifications', 'status', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('base_notifications', 'status');
    }
}
