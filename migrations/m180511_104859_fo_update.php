<?php

use yii\db\Migration;

/**
 * Class m180511_104859_fo_update
 */
class m180511_104859_fo_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('financial_operations','transaction_number','varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute(' ALTER TABLE "financial_operations" ALTER COLUMN "transaction_number" TYPE integer USING transaction_number::integer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180511_104859_fo_update cannot be reverted.\n";

        return false;
    }
    */
}
