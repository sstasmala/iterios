<?php

use yii\db\Migration;

/**
 * Class m180511_064333_tariff_orders
 */
class m180511_064333_tariff_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE tariffs_orders_id_seq;');
        $this->createTable('tariffs_orders',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'tariffs_orders_id_seq\')',
                'name'=>'varchar(255) NOT NULL',
                'tariff_id'=>'bigint NOT NULL',
                'customer_id'=>'bigint NOT NULL',
                'duration_date'=>'bigint',
                'status'=>'integer NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE tariffs_orders_id_seq OWNED BY tariffs_orders.id;');
        $this->addForeignKey('tariffs_orders_tenants_fk','tariffs_orders','customer_id',
            'tenants','id','CASCADE','CASCADE');
        $this->addForeignKey('tariffs_orders_tariffs_fk','tariffs_orders','tariff_id',
            'tariffs','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tariffs_orders_tenants_fk','tariffs_orders');
        $this->dropForeignKey('tariffs_orders_tariffs_fk','tariffs_orders');
        $this->dropTable('tariffs_orders');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180511_064333_tariff_orders cannot be reverted.\n";

        return false;
    }
    */
}
