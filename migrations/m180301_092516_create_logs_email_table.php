<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs_email`.
 */
class m180301_092516_create_logs_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('logs_email');

        $this->createTable('logs_email', [
            'id' => $this->primaryKey(),
            'subject' => $this->string()->notNull(),
            'text' => $this->text(),
            'html' => $this->text(),
            'type' => $this->string()->notNull(),
            'recipient' => $this->string()->notNull(),
            'template_id' => $this->integer(),
            'conversion' => $this->boolean(),
            'status' => $this->string()->notNull(),
            'reason' => $this->string(),
            'ip' => $this->string(),
            'city' => $this->string(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger()
        ]);

        $this->addForeignKey('logs_email_templates_fk', 'logs_email', 'template_id', 'templates', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('logs_email_templates_fk', 'logs_email');
        $this->dropTable('logs_email');

        $this->createTable('logs_email', [
            'id' => $this->primaryKey(),
        ]);
    }
}
