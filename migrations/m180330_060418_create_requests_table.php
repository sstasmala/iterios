<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requests`.
 */
class m180330_060418_create_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('requests', [
            'id' => $this->primaryKey(),
            'contact_id' => $this->integer(),
            'country' => $this->integer(),
            'city' => $this->integer(),
            'departure_date' => $this->bigInteger(),
            'trip_duration_start' => $this->integer(),
            'trip_duration_end' => $this->integer(),
            'budget_currency' => $this->integer(),
            'budget_from' => $this->integer(),
            'budget_to' => $this->integer(),
            'tourists_adult_count' => $this->integer(),
            'tourists_children_count' => $this->integer(),
            'tourists_children_age' => $this->string(),
            'request_type_id' => $this->integer(),
            'request_description' => $this->text(),
            'request_status_id' => $this->integer(),
            'request_canceled_reason_id' => $this->integer(),
            'responsible_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('requests_contacts_fk','requests','contact_id','contacts','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('requests_type_fk', 'requests', 'request_type_id', 'request_type', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('requests_status_fk', 'requests', 'request_status_id', 'request_statuses', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('requests_canceled_reason_fk', 'requests', 'request_canceled_reason_id', 'request_canceled_reasons', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('requests_users_fk', 'requests', 'responsible_id', 'users', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('requests_users_fk','requests');
        $this->dropForeignKey('requests_canceled_reason_fk', 'requests');
        $this->dropForeignKey('requests_status_fk', 'requests');
        $this->dropForeignKey('requests_type_fk', 'requests');
        $this->dropForeignKey('requests_contacts_fk','requests');
        $this->dropTable('requests');
    }
}
