<?php

use yii\db\Migration;

/**
 * Class m180303_074802_update_logs_table
 */
class m180303_074802_update_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('logs_tenant_fk', 'logs');
        $this->addForeignKey('logs_tenant_fk','logs','tenant_id','tenants','id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('logs_tenant_fk', 'logs');
        $this->addForeignKey('logs_tenant_fk','logs','tenant_id','tenants','id');
    }
}
