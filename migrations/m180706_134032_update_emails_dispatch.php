<?php

use yii\db\Migration;

/**
 * Class m180706_134032_update_emails_dispatch
 */
class m180706_134032_update_emails_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->truncateTable('emails_dispatch');
        $this->dropColumn('emails_dispatch','owner');
        $this->addColumn('emails_dispatch','tenant_id','bigint NOT NULL');
        $this->addColumn('emails_dispatch','owner_id','bigint NOT NULL');
        $this->addForeignKey('emails_dispatch_tenant_fk','emails_dispatch','tenant_id',
            'tenants','id','CASCADE','CASCADE');
        $this->addForeignKey('emails_dispatch_user_fk','emails_dispatch','owner_id',
            'users','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('emails_dispatch_tenant_fk','emails_dispatch');
        $this->dropForeignKey('emails_dispatch_user_fk','emails_dispatch');
        $this->dropColumn('emails_dispatch','tenant_id');
        $this->dropColumn('emails_dispatch','owner_id');
        $this->addColumn('emails_dispatch','owner','bigint NOT NULL DEFAULT 0');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_134032_update_emails_dispatch cannot be reverted.\n";

        return false;
    }
    */
}
