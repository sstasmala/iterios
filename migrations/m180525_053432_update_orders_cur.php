<?php

use yii\db\Migration;

/**
 * Class m180525_053432_update_orders_cur
 */
class m180525_053432_update_orders_cur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders','currency','integer');
        $this->addColumn('orders','discount','decimal(11,2) NOT NULL DEFAULT 0.00');
        $this->addColumn('orders','discount_type','integer NOT NULL DEFAULT 0');
        $this->addColumn('orders','due_date','bigint');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders','currency');
        $this->dropColumn('orders','discount');
        $this->dropColumn('orders','discount_type');
        $this->dropColumn('orders','due_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180525_053432_update_orders_cur cannot be reverted.\n";

        return false;
    }
    */
}
