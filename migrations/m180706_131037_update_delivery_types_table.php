<?php

use yii\db\Migration;

/**
 * Class m180706_131037_update_delivery_types_table
 */
class m180706_131037_update_delivery_types_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function safeUp()
    {
        $delivery_types = \app\models\DeliveryTypes::find()
            ->all();

        foreach ($delivery_types as $delivery_type) {
            $model = \app\models\DeliveryTypes::findOne($delivery_type->id);

            if ($model !== null) {
                $model->name = $delivery_type->name;
                $model->saveWithLang('en');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }
}
