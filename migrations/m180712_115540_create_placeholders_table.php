<?php

use yii\db\Migration;

/**
 * Handles the creation of table `placeholders`.
 */
class m180712_115540_create_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('placeholders', [
            'id' => $this->primaryKey(),
            'short_code' => $this->string(),
            'type_placeholder' => $this->string(),
            'module' => $this->string(),
            'field' => $this->string(),
            'default' => $this->string(),
            'description' => $this->text(),
            'fake' => $this->boolean(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('placeholders');
    }
}