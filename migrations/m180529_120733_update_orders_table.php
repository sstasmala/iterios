<?php

use yii\db\Migration;

/**
 * Class m180529_120733_update_orders_table
 */
class m180529_120733_update_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function safeUp()
    {
        $this->addColumn('request_type', 'system', $this->boolean()->defaultValue(false));
        $this->addColumn('request_type', 'system_type', $this->string());

        $model = new \app\models\RequestType();
        $model->system = true;
        $model->value = 'From Request';
        $model->system_type = \app\models\RequestType::SYSTEM_FROM_REQUEST_TYPE;
        $model->saveWithLang('en');

        $this->addColumn('orders', 'request_source_id', $this->integer());

        $this->addForeignKey('fk-orders-request_source_id', 'orders', 'request_source_id', 'request_type', 'id');
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     * @throws \Throwable
     */
    public function safeDown()
    {
        $model = \app\models\RequestType::findOne(['system_type' => \app\models\RequestType::SYSTEM_FROM_REQUEST_TYPE]);

        if ($model !== null)
            $model->delete();

        $this->dropColumn('request_type', 'system');
        $this->dropColumn('request_type', 'system_type');

        $this->dropForeignKey('fk-orders-request_source_id', 'orders');
        $this->dropColumn('orders', 'request_source_id');
    }
}
