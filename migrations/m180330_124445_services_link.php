<?php

use yii\db\Migration;

/**
 * Class m180330_124445_services_link
 */
class m180330_124445_services_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE services_links_id_seq;');
        $this->createTable('services_links',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_links_id_seq\')',
                'parent_id'=>'bigint NOT NULL',
                'child_id'=>'bigint NOT NULL'
            ]
        );
        $this->execute('ALTER SEQUENCE services_links_id_seq OWNED BY services_links.id;');
        $this->addForeignKey('services_fields_fk_services_p','services_links','parent_id','services','id','CASCADE','CASCADE');
        $this->addForeignKey('services_fields_fk_services_c','services_links','child_id','services','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('services_fields_fk_services_p','services_links');
        $this->dropForeignKey('services_fields_fk_services_c','services_links');
        $this->dropTable('services_links');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180330_124445_services_link cannot be reverted.\n";

        return false;
    }
    */
}
