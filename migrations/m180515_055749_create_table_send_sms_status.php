<?php

use yii\db\Migration;

/**
 * Class m180515_055749_create_table_send_sms_status
 */
class m180515_055749_create_table_send_sms_status extends Migration
{
    public function safeUp()
    {
        $this->createTable('send_sms_status', [
            'id' => $this->primaryKey(),
            'send_sms_id'=>$this->integer(),
            'contact_phone_id'=>$this->integer(),
            'status'=>$this->integer(),
            'segments'=>$this->string(),
            'time_send'=>$this->bigInteger(),
            'provider_sms_id'=>$this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('send_sms_status');
    }
}
