<?php

use yii\db\Migration;

/**
 * Class m180322_133819_update_invites_table
 */
class m180322_133819_update_invites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invites', 'user_role', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('invites', 'user_role');
    }
}
