<?php

use yii\db\Migration;

/**
 * Class m180418_083251_update_notifications_tables
 */
class m180418_083251_update_notifications_tables extends Migration
{
    private function downMigrations($down = false)
    {
        $this->dropTable('notifications_course');

        if ($down) {
            $this->dropIndex('idx-base_notifications-trigger_action', 'base_notifications');
            $this->dropColumn('base_notifications', 'trigger_action');
        }

        $this->dropIndex('idx-base_notifications-subscribers', 'base_notifications');
        $this->dropIndex('idx-base_notifications-trigger_day', 'base_notifications');

        $this->dropColumn('base_notifications', 'trigger_day');
        $this->dropColumn('base_notifications', 'subscribers');

        $this->dropIndex('ind_notifications_check_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_action', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_value', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_value_int', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_element_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_created_at', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_updated_at', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_agent_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_contact_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_tenant_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_status', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_trigger_time', '{{%notifications_trigger}}');

        $this->dropTable('{{%notifications_trigger}}');
    }

    private function upMigrations($up = true)
    {
        $this->createTable('{{%notifications_course}}', [
            'id' => $this->primaryKey(),
            'send_type' => $this->integer(),

            'user_id' => $this->integer(),
            'user_type' => $this->integer(),

            'date' => $this->string(),
            'time' => $this->string(),
            'day' => $this->string(),

            'timezone_date' => $this->string(),
            'timezone_time' => $this->string(),
            'timezone_day' => $this->string(),

            'send_status' => $this->integer()->defaultValue(0),

            'rule_id' => $this->string(),
            'noting_id' => $this->integer(),

            'created_at' => $this->bigInteger(),
        ]);

        $this->addColumn('base_notifications', 'trigger_day', 'integer NOT NULL DEFAULT 0');
        $this->addColumn('base_notifications', 'subscribers', $this->string());

        if ($up) {
            $this->addColumn('base_notifications', 'trigger_action', $this->text());
            $this->createIndex('idx-base_notifications-trigger_action', 'base_notifications', 'trigger_action');
        }

        $this->createIndex('idx-base_notifications-trigger_day', 'base_notifications', 'trigger_day');
        $this->createIndex('idx-base_notifications-subscribers', 'base_notifications', 'subscribers');

        $this->createTable('{{%notifications_trigger}}', [
            'id' => $this->primaryKey(),
            'action' => $this->string()->notNull(),
            'element_id' => $this->integer(),
            'value' => $this->string()->defaultValue(''),
            'value_int' => $this->integer()->defaultValue(0),

            'trigger_time' => $this->integer()->defaultValue(0),

            'agent_id' => $this->integer()->defaultValue(0),
            'contact_id' => $this->integer()->defaultValue(0),
            'tenant_id' => $this->integer()->defaultValue(0),
            'status' => $this->integer()->defaultValue(0),

            'data' => $this->text(),
            'created_at' => $this->integer()->defaultValue(0),
            'updated_at' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex('ind_notifications_check_id', '{{%notifications_trigger}}', 'action');
        $this->createIndex('ind_notifications_check_action', '{{%notifications_trigger}}', 'action');

        $this->createIndex('ind_notifications_check_value', '{{%notifications_trigger}}', 'value');
        $this->createIndex('ind_notifications_check_value_int', '{{%notifications_trigger}}', 'value_int');

        $this->createIndex('ind_notifications_check_element_id', '{{%notifications_trigger}}', 'element_id');
        $this->createIndex('ind_notifications_check_created_at', '{{%notifications_trigger}}', 'created_at');
        $this->createIndex('ind_notifications_check_updated_at', '{{%notifications_trigger}}', 'updated_at');

        $this->createIndex('ind_notifications_check_agent_id', '{{%notifications_trigger}}', 'agent_id');
        $this->createIndex('ind_notifications_check_contact_id', '{{%notifications_trigger}}', 'contact_id');
        $this->createIndex('ind_notifications_check_tenant_id', '{{%notifications_trigger}}', 'tenant_id');
        $this->createIndex('ind_notifications_check_status', '{{%notifications_trigger}}', 'status');

        $this->createIndex('ind_notifications_check_trigger_time', '{{%notifications_trigger}}', 'trigger_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->downMigrations();
        $this->upMigrations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->downMigrations(true);
        $this->upMigrations(false);
    }
}
