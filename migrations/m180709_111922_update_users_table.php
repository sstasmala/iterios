<?php

use yii\db\Migration;

/**
 * Class m180709_111922_update_users_table
 */
class m180709_111922_update_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\models\User::updateAll(['phone' => null]);
        $this->addColumn('users','phone_clone', 'varchar(32)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users','phone_clone');
    }
}
