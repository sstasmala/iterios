<?php

use yii\db\Migration;

/**
 * Class m180504_082542_create_table_profile_notifications_statuses
 */
class m180504_082542_create_table_profile_notifications_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('profile_notifications_statuses', [
            'id' => $this->primaryKey(),
            'notification_id' => $this->integer(),
            'status_id' => $this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

        $this->addForeignKey('fk_profile_notifications_statuses', 'profile_notifications_statuses', 'notification_id', 'profile_notifications', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_profile_notifications_statuses', 'profile_notifications_statuses');
        $this->dropTable('profile_notifications_statuses');
    }
}
