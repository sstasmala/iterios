<?php

use yii\db\Migration;

/**
 * Class m180426_052522_notifications_course_update
 */
class m180426_052522_notifications_course_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_course', 'errors', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_course', 'errors');
    }
}
