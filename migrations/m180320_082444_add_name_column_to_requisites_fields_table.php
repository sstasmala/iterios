<?php

use yii\db\Migration;

/**
 * Handles adding name to table `requisites_fields`.
 */
class m180320_082444_add_name_column_to_requisites_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requisites_fields', 'name', $this->string());
        $this->addColumn('requisites_fields', 'requisite_group_id', $this->integer());
        $this->addForeignKey('fk_rqsts_groups_rqsts_fields', 'requisites_fields', 'requisite_group_id', 'requisites_groups', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_rqsts_groups_rqsts_fields', 'requisites_fields');

        $this->dropColumn('requisites_fields', 'requisite_group_id');
        $this->dropColumn('requisites_fields', 'name');
    }
}
