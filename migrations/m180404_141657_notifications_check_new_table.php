<?php

use yii\db\Migration;

/**
 * Class m180404_141657_notifications_check_new_table
 */
class m180404_141657_notifications_check_new_table extends Migration
{
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('{{%notifications_trigger}}', [
            'id' => $this->primaryKey(),
            'action' => $this->string()->notNull(),
            'element_id' => $this->integer(),
            'value' => $this->string()->defaultValue(''),
            'value_int' => $this->integer()->defaultValue(0),

            'trigger_time' => $this->integer()->defaultValue(0),

            'agent_id' => $this->integer()->defaultValue(0),
            'contact_id' => $this->integer()->defaultValue(0),
            'tenant_id' => $this->integer()->defaultValue(0),
            'status' => $this->integer()->defaultValue(0),

            'data' => $this->text(),
            'created_at' => $this->integer()->defaultValue(0),
            'updated_at' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex('ind_notifications_check_id', '{{%notifications_trigger}}', 'action');
        $this->createIndex('ind_notifications_check_action', '{{%notifications_trigger}}', 'action');

        $this->createIndex('ind_notifications_check_value', '{{%notifications_trigger}}', 'value');
        $this->createIndex('ind_notifications_check_value_int', '{{%notifications_trigger}}', 'value_int');

        $this->createIndex('ind_notifications_check_element_id', '{{%notifications_trigger}}', 'element_id');
        $this->createIndex('ind_notifications_check_created_at', '{{%notifications_trigger}}', 'created_at');
        $this->createIndex('ind_notifications_check_updated_at', '{{%notifications_trigger}}', 'updated_at');
        
        $this->createIndex('ind_notifications_check_agent_id', '{{%notifications_trigger}}', 'agent_id'); 
        $this->createIndex('ind_notifications_check_contact_id', '{{%notifications_trigger}}', 'contact_id'); 
        $this->createIndex('ind_notifications_check_tenant_id', '{{%notifications_trigger}}', 'tenant_id'); 
        $this->createIndex('ind_notifications_check_status', '{{%notifications_trigger}}', 'status'); 
        
        $this->createIndex('ind_notifications_check_trigger_time', '{{%notifications_trigger}}', 'trigger_time'); 
    }

    public function down()
    {
        $this->dropIndex('ind_notifications_check_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_action', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_value', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_value_int', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_element_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_created_at', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_updated_at', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_agent_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_contact_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_tenant_id', '{{%notifications_trigger}}');
        $this->dropIndex('ind_notifications_check_status', '{{%notifications_trigger}}');

        $this->dropIndex('ind_notifications_check_trigger_time', '{{%notifications_trigger}}');

        $this->dropTable('{{%notifications_trigger}}');
    }
    
}
