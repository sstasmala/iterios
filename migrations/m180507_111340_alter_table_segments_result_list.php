<?php

use yii\db\Migration;

/**
 * Class m180507_111340_alter_table_segments_result_list
 */
class m180507_111340_alter_table_segments_result_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('segments_result_list','id_segment', 'id_relation');

        $this->dropIndex(
            'idx-segments_result_list-id_segment',
            'segments_result_list'
        );

        $this->dropForeignKey(
            'fk-segments_result_list-id_segment',
            'segments_result_list'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('segments_result_list','id_relation', 'id_segment');
    }

}
