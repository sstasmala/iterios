<?php

use yii\db\Migration;

/**
 * Class m180619_065717_update_order_tourists_table
 */
class m180619_065717_update_orders_tourists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->truncateTable('orders_tourists');

        $this->dropForeignKey('fk-orders_tourists-passport_id','orders_tourists');
        $this->dropColumn('orders_tourists', 'passport_id');

        $this->addColumn('orders_tourists', 'passport_id', $this->integer());
        $this->addForeignKey('fk-orders_tourists-passport_id','orders_tourists','passport_id',
            'contacts_passports','id','CASCADE','CASCADE');

        $this->addColumn('orders_tourists', 'contact_id', $this->integer());
        $this->addForeignKey('orders_tourists_cn_fk','orders_tourists','contact_id',
            'contacts','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_tourists_cn_fk','orders_tourists');
        $this->dropColumn('orders_tourists', 'contact_id');
    }
}
