<?php

use app\models\DeliveryPlaceholders;
use app\models\DocumentPlaceholders;
use app\models\Placeholders;
use app\models\RequisitesFields;
use app\models\ServicesFieldsDefault;
use app\models\Translations;
use yii\db\Migration;

/**
 * Class m180719_042406_convert_old_placeholder_to_new
 */
class m180719_042406_convert_old_placeholder_to_new extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $doc_phs = DocumentPlaceholders::find()->all();
//
//        echo 'Start document:'. "\n";
//
//        foreach ($doc_phs as $ph) {
//            $path = null;
//
//            if (!empty($ph->path)) {
//                $path = $ph->module === 'ServicesFields' ? ServicesFieldsDefault::find() : RequisitesFields::find();
//                $path->where(['id' => $ph->path]);
//                $path = $path->one();
//            }
//
//            $model = new Placeholders();
//            $model->short_code = $ph->short_code;
//            $model->type_placeholder = Placeholders::TYPE_PLACEHOLDER_FIELD;
//            $model->module = $ph->module === 'ServicesFields' ? 'app\components\placeholders\modules\ServiceModule' : 'app\components\placeholders\modules\RequisiteModule';
//            $model->field = !empty($ph->path) && $path !== null ? $path->code : '';
//            $model->default = $ph->default;
//            $model->description = $ph->description;
//            $model->fake = $ph->fake;
//            $model->created_at = time();
//            $model->updated_at = time();
//            $model->created_by = 1;
//            $model->updated_by = 1;
//
//            if ($model->save()) {
//                $translate = Translations::find();
//                $translate->andWhere(['key' => 'document_placeholders.short_code.'.$ph->id]);
//                $translate = $translate->all();
//
//                foreach ($translate as $tr) {
//                    $tr->key = 'placeholders.short_code.'.$model->id;
//                    $tr->save();
//                }
//
//                $doc_model = new \app\models\PlaceholdersDocument();
//                $doc_model->placeholder_id = $model->id;
//                $doc_model->created_at = time();
//                $doc_model->updated_at = time();
//                $doc_model->created_by = 1;
//                $doc_model->updated_by = 1;
//                $doc_model->save();
//            }
//
//            echo 'Document #'.$ph->id.' completed!'. "\n";
//        }
//
//        echo 'Start delivery:'. "\n";
//
//        $delivery_phs = DeliveryPlaceholders::find()->all();
//
//        foreach ($delivery_phs as $ph) {
//            $path = null;
//
//            if (!empty($ph->path)) {
//                $path = $ph->module === 'ServicesFields' ? ServicesFieldsDefault::find() : RequisitesFields::find();
//                $path->where(['id' => $ph->path]);
//                $path = $path->one();
//            }
//
//            $model = new Placeholders();
//            $model->short_code = $ph->short_code;
//            $model->type_placeholder = Placeholders::TYPE_PLACEHOLDER_FIELD;
//            $model->module = $ph->module === 'ServicesFields' ? 'app\components\placeholders\modules\ServiceModule' : 'app\components\placeholders\modules\RequisiteModule';
//            $model->field = !empty($ph->path) && $path !== null ? $path->code : '';
//            $model->default = $ph->default;
//            $model->description = $ph->description;
//            $model->fake = $ph->fake;
//            $model->created_at = $ph->created_at;
//            $model->updated_at = $ph->updated_at;
//            $model->created_by = $ph->created_by;
//            $model->updated_by = $ph->updated_by;
//
//            if ($model->save()) {
//                $translate = Translations::find();
//                $translate->andWhere(['key' => 'delivery_placeholders.short_code.'.$ph->id]);
//                $translate = $translate->all();
//
//                foreach ($translate as $tr) {
//                    $tr->key = 'placeholders.short_code.'.$model->id;
//                    $tr->save();
//                }
//
//                $delivery_model = new \app\models\PlaceholdersDelivery();
//                $delivery_model->placeholder_id = $model->id;
//                $delivery_model->created_at = $ph->created_at;
//                $delivery_model->updated_at = $ph->updated_at;
//                $delivery_model->created_by = $ph->created_by;
//                $delivery_model->updated_by = $ph->updated_by;
//
//                if ($delivery_model->save()) {
//                    foreach ($ph->deliveryPlaceholderTypesMapping as $ph_types) {
//                        $model_type = new \app\models\PlaceholdersDeliveryTypes();
//                        $model_type->delivery_type_id = $ph_types->delivery_type_id;
//                        $model_type->placeholder_delivery_id = $delivery_model->id;
//                        $model_type->save();
//                    }
//                }
//            }
//
//            echo 'Delivery #'.$ph->id.' completed!'. "\n";
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180719_042406_convert_old_placeholder_to_new cannot be reverted.\n";

        return false;
    }
}
