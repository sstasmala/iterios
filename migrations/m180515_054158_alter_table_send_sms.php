<?php

use yii\db\Migration;

/**
 * Class m180515_054158_alter_table_send_sms
 */
class m180515_054158_alter_table_send_sms extends Migration
{
    public function safeUp()
    {
        $this->addColumn('send_sms', 'segment_relation_id', $this->string());
        $this->addColumn('send_sms', 'text', $this->string());
        $this->addColumn('send_sms', 'alpha_name', $this->string());
        $this->addColumn('send_sms', 'provider_id', $this->integer());
        $this->addColumn('send_sms', 'tenant_id', $this->integer());
        $this->alterColumn('send_sms','status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('send_sms', 'segment_relation_id');
        $this->dropColumn('send_sms', 'text');
        $this->dropColumn('send_sms', 'alpha_name');
        $this->dropColumn('send_sms', 'provider_id');
        $this->dropColumn('send_sms', 'tenant_id');
    }
}
