<?php

use yii\db\Migration;

/**
 * Class m180504_131140_update_notifications_log_table
 */
class m180504_131140_update_notifications_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_log','jobs_ids', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_log','jobs_ids');
    }
}
