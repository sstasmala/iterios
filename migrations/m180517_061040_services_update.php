<?php

use yii\db\Migration;

/**
 * Class m180517_061040_services_update
 */
class m180517_061040_services_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requests_services_additional_links','type','integer NOT NULL DEFAULT 0');
        $this->addColumn('services','multiservices','integer NOT NULL DEFAULT 0');
        $this->addColumn('services','multiservices_name','varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requests_services_additional_links','type');
        $this->dropColumn('services','multiservices');
        $this->dropColumn('services','multiservices_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180517_061040_services_update cannot be reverted.\n";

        return false;
    }
    */
}
