<?php

use yii\db\Migration;

/**
 * Class m180403_051444_add_pl_to_requisites
 */
class m180403_051444_add_pl_to_requisites extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requisites_fields','field_placeholder', 'varchar(255)');
        $this->addColumn('requisites_fields','field_description', 'varchar(255)');
        $this->addColumn('requisites','in_header','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requisites_fields','field_placeholder');
        $this->dropColumn('requisites_fields','field_description');
        $this->dropColumn('requisites','in_header');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180403_051444_add_pl_to_requisites cannot be reverted.\n";

        return false;
    }
    */
}
