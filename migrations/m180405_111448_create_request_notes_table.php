<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_notes`.
 */
class m180405_111448_create_request_notes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request_notes', [
            'id' => $this->primaryKey(),
            'value' => $this->text(),
            'user_id' =>'integer',
            'contact_id'=>'integer',
            'request_id'=>'integer',
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('request_notes');
    }
}
