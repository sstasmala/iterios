<?php

use yii\db\Migration;

/**
 * Class m180322_114844_change
 */
class m180322_114844_change extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contacts','date_of_birth');
        $this->addColumn('contacts','date_of_birth','bigint');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts','date_of_birth');
        $this->addColumn('contacts','date_of_birth','varchar(100)');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_114844_change cannot be reverted.\n";

        return false;
    }
    */
}
