<?php

use yii\db\Migration;

/**
 * Class m180315_135924_update_tasks_types_table
 */
class m180315_135924_update_tasks_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks_types', 'default', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks_types', 'default');
    }
}
