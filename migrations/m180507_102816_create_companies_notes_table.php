<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_notes`.
 */
class m180507_102816_create_companies_notes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_notes', [
            'id' => $this->primaryKey(),
            'value' => $this->text(),
            'company_id'=>'integer',
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer',
        ]);

        $this->addForeignKey('fk-companies_notes-company_id', 'companies_notes', 'company_id', 'companies', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-companies_notes-company_id', 'companies_notes');
        $this->dropTable('companies_notes');
    }
}
