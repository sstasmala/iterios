<?php

use yii\db\Migration;

/**
 * Class m180509_114228_create_table_providers_credentials
 */
class m180509_114228_create_table_providers_credentials extends Migration
{
    public function safeUp()
    {
        $this->createTable('providers_credentials', [
            'id' => $this->primaryKey(),
            'provider_id'=>$this->integer(),
            'tenant_id'=>$this->integer(),
            'params' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('providers_credentials');
    }
}
