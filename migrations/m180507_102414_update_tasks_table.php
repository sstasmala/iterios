<?php

use yii\db\Migration;

/**
 * Class m180507_102414_update_tasks_table
 */
class m180507_102414_update_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'due_date_send', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'due_date_send');
    }
}
