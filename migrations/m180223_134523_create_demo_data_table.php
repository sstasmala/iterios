<?php

use yii\db\Migration;

/**
 * Handles the creation of table `demo_data`.
 */
class m180223_134523_create_demo_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('demo_data', [
            'id' => $this->primaryKey(),
            'type' => $this->string(24),
            'data' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('demo_data');
    }
}
