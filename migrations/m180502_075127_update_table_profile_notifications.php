<?php

use yii\db\Migration;

/**
 * Class m180502_075127_update_table_profile_notifications
 */
class m180502_075127_update_table_profile_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile_notifications', 'action', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile_notifications', 'action');
    }
}
