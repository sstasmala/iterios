<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notes`.
 */
class m180330_134442_create_notes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contacts_notes', [
            'id' => $this->primaryKey(),
            'value' => $this->text(),
            'user_id' =>'integer',
            'contact_id'=>'integer',
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts_notes');
    }
}
