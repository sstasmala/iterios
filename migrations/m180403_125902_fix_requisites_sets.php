<?php

use yii\db\Migration;

/**
 * Class m180403_125902_fix_requisites_sets
 */
class m180403_125902_fix_requisites_sets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_requisites_sets_requisites','requisites_sets');
        $this->addForeignKey('fk_requisites_sets_requisites','requisites_sets','tenant_id','tenants','id','CASCADE','CASCADE');
        $this->dropForeignKey('fk_requisites_requisites_field','requisites');
        $this->addForeignKey('fk_requisites_requisites_field', 'requisites', 'requisites_field_id', 'requisites_fields', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('fk_requisites_tenants_tenants','requisites_tenants');
        $this->addForeignKey('fk_requisites_tenants_tenants', 'requisites_tenants', 'requisite_id', 'requisites', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('fk_requisites_tenants_sets','requisites_tenants');
        $this->addForeignKey('fk_requisites_tenants_sets', 'requisites_tenants', 'set_id', 'requisites_sets', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_requisites_sets_requisites','requisites_sets');
        $this->addForeignKey('fk_requisites_sets_requisites','requisites_sets','tenant_id','tenants','id','RESTRICT','CASCADE');
        $this->dropForeignKey('fk_requisites_requisites_field','requisites');
        $this->addForeignKey('fk_requisites_requisites_field', 'requisites', 'requisites_field_id', 'requisites_fields', 'id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('fk_requisites_tenants_tenants','requisites_tenants');
        $this->addForeignKey('fk_requisites_tenants_tenants', 'requisites_tenants', 'requisite_id', 'requisites', 'id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('fk_requisites_tenants_sets','requisites_tenants');
        $this->addForeignKey('fk_requisites_tenants_sets', 'requisites_tenants', 'set_id', 'requisites_sets', 'id', 'RESTRICT', 'CASCADE');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180403_125902_fix_requisites_sets cannot be reverted.\n";

        return false;
    }
    */
}
