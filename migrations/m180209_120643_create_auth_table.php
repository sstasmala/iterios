<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth`.
 */
class m180209_120643_create_auth_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auth', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'source' => $this->string(),
            'source_id' => $this->string()
        ]);

        $this->addForeignKey('fk_users_auth', 'auth', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_users_auth', 'auth');
        $this->dropTable('auth');
    }
}
