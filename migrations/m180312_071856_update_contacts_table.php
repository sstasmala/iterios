<?php

use yii\db\Migration;

/**
 * Class m180312_071856_update_contacts_table
 */
class m180312_071856_update_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('contacts_tenant_fk', 'contacts');
        $this->addForeignKey('contacts_tenant_fk','contacts','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('contacts_tenant_fk', 'contacts');
        $this->addForeignKey('contacts_tenant_fk','contacts','tenant_id','tenants','id');
    }
}