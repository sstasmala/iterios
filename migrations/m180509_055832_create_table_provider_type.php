<?php

use yii\db\Migration;

/**
 * Class m180509_055832_create_table_provider_type
 */
class m180509_055832_create_table_provider_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('provider_type', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->bigInteger(),
            'updated_by'=>$this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('provider_type');
    }
}
