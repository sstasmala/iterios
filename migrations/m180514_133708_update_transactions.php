<?php

use yii\db\Migration;

/**
 * Class m180514_133708_update_transactions
 */
class m180514_133708_update_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('transactions','tenant_id','bigint');
        $this->addForeignKey('transactions_tenants_fk','transactions','tenant_id','tenants','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('transactions_tenants_fk','transactions');
        $this->dropColumn('transactions','tenant_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180514_133708_update_transactions cannot be reverted.\n";

        return false;
    }
    */
}
