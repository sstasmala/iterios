<?php

use yii\db\Migration;

/**
 * Class m180709_075328_update_delivery_email_templates_table
 */
class m180709_075328_update_delivery_email_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('delivery_email_templates', 'is_default', $this->boolean()->defaultValue(false));
        $this->addColumn('delivery_sms_templates', 'is_default', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_email_templates', 'is_default');
        $this->dropColumn('delivery_sms_templates', 'is_default');
    }
}
