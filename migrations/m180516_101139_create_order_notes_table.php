<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_notes`.
 */
class m180516_101139_create_order_notes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_notes', [
            'id' => $this->primaryKey(),
            'value' => $this->text(),
            'contact_id' => $this->integer(),
            'order_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addForeignKey('fk-order_notes-contact_id', 'order_notes', 'contact_id', 'contacts', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-order_notes-order_id', 'order_notes', 'order_id', 'orders', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_notes-contact_id', 'order_notes');
        $this->dropForeignKey('fk-order_notes-order_id', 'order_notes');

        $this->dropTable('order_notes');
    }
}
