<?php

use yii\db\Migration;

/**
 * Class m180508_051645_tariffs_feature
 */
class m180508_051645_tariffs_feature extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE tariffs_feature_id_seq;');
        $this->createTable('tariffs_feature',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'tariffs_feature_id_seq\')',
                'name'=>'varchar(255) NOT NULL',
                'class'=>'text NOT NULL',
                'countable'=>'integer NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE tariffs_feature_id_seq OWNED BY tariffs_feature.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tariffs_feature');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180508_051645_tariffs_feature cannot be reverted.\n";

        return false;
    }
    */
}
