<?php

use yii\db\Migration;

/**
 * Class m180418_055842_request_services
 */
class m180418_055842_request_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Main services connection to request
        $this->execute('CREATE SEQUENCE requests_services_links_id_seq;');
        $this->createTable('requests_services_links',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'requests_services_links_id_seq\')',
                'request_id'=>'bigint NOT NULL',
                'service_id'=>'bigint NOT NULL',
                'tenant_id'=>'bigint NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE requests_services_links_id_seq OWNED BY requests_services_links.id;');
        $this->addForeignKey('requests_services_links_rq_fk','requests_services_links','request_id',
            'requests','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_links_sr_fk','requests_services_links',
            'service_id','services','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_links_tn_fk','requests_services_links',
            'tenant_id','tenants','id','CASCADE','CASCADE');

        //Additional services connection to request
        $this->execute('CREATE SEQUENCE requests_services_additional_links_id_seq;');
        $this->createTable('requests_services_additional_links',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'requests_services_additional_links_id_seq\')',
                'link_id'=>'bigint NOT NULL',
                'service_id'=>'bigint NOT NULL',
            ]
        );
        $this->execute('ALTER SEQUENCE requests_services_additional_links_id_seq OWNED BY requests_services_additional_links.id;');
        $this->addForeignKey('requests_services_additional_links_rqsl_fk','requests_services_additional_links',
            'link_id','requests_services_links','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_additional_links_sr_fk','requests_services_additional_links',
            'service_id','services','id','CASCADE','CASCADE');

        //Values
        $this->execute('CREATE SEQUENCE requests_services_values_id_seq;');
        $this->createTable('requests_services_values',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'requests_services_values_id_seq\')',
                'link_id'=>'bigint NOT NULL',
                'additional_link_id'=>'bigint',
                'service_id'=>'bigint NOT NULL',
                'field_id'=>'bigint NOT NULL',
                'value'=>'text',
            ]
        );
        $this->execute('ALTER SEQUENCE requests_services_values_id_seq OWNED BY requests_services_values.id;');
        $this->addForeignKey('requests_services_values_lk_fk','requests_services_values',
            'link_id','requests_services_links','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_values_alk_fk','requests_services_values',
            'additional_link_id','requests_services_additional_links','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_values_sr_fk','requests_services_values',
            'service_id','services','id','CASCADE','CASCADE');
        $this->addForeignKey('requests_services_values_fd_fk','requests_services_values',
            'field_id','services_fields_default','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('requests_services_values_fd_fk','requests_services_values');
        $this->dropForeignKey('requests_services_values_sr_fk','requests_services_values');
        $this->dropForeignKey('requests_services_values_alk_fk','requests_services_values');
        $this->dropForeignKey('requests_services_values_lk_fk','requests_services_values');
        $this->dropForeignKey('requests_services_links_sr_fk','requests_services_links');
        $this->dropForeignKey('requests_services_additional_links_sr_fk','requests_services_additional_links');
        $this->dropForeignKey('requests_services_additional_links_rqsl_fk','requests_services_additional_links');
        $this->dropForeignKey('requests_services_links_rq_fk','requests_services_links');
        $this->dropForeignKey('requests_services_links_tn_fk','requests_services_links');

        $this->dropTable('requests_services_values');
        $this->dropTable('requests_services_additional_links');
        $this->dropTable('requests_services_links');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180418_055842_request_services cannot be reverted.\n";

        return false;
    }
    */
}
