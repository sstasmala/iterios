<?php

use yii\db\Migration;

/**
 * Class m180524_123926_update_orders
 */
class m180524_123926_update_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('orders_tourists_lk_fk','orders_tourists');
        $this->addForeignKey('orders_tourists_lk_fk','orders_tourists','link_id',
            'orders_services_links','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_tourists_lk_fk','orders_tourists');
        $this->addForeignKey('orders_tourists_lk_fk','orders_tourists','link_id',
            'contacts','id','CASCADE','CASCADE');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_123926_update_orders cannot be reverted.\n";

        return false;
    }
    */
}
