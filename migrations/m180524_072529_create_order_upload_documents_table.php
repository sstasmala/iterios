<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_upload_documents`.
 */
class m180524_072529_create_order_upload_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_upload_documents', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'title' => $this->string(),
            'file_type' => $this->string(),
            'file_size' => $this->integer(),
            'file_path' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer()
        ]);

        $this->addForeignKey('fk-order_documents-order_id', 'order_upload_documents', 'order_id', 'orders', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_documents-order_id', 'order_upload_documents');

        $this->dropTable('order_upload_documents');
    }
}
