<?php

use yii\db\Migration;

/**
 * Handles adding trigger_day to table `base_notifications`.
 */
class m180403_131929_add_trigger_day_column_to_base_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('base_notifications', 'trigger_day', 'integer NOT NULL DEFAULT 0');
        $this->addColumn('base_notifications', 'subscribers', $this->string());

        $this->createIndex('idx-base_notifications-trigger_day', 'base_notifications', 'trigger_day');
        $this->createIndex('idx-base_notifications-subscribers', 'base_notifications', 'subscribers');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-base_notifications-subscribers', 'base_notifications');
        $this->dropIndex('idx-base_notifications-trigger_day', 'base_notifications');

        $this->dropColumn('base_notifications', 'trigger_day');
        $this->dropColumn('base_notifications', 'subscribers');
    }
}
