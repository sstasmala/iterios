<?php

use yii\db\Migration;

/**
 * Class m180306_144121_update_logs_email_table
 */
class m180306_144121_update_logs_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('logs_email', 'reason');
        $this->addColumn('logs_email', 'reason', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logs_email', 'reason');
        $this->addColumn('logs_email', 'reason', $this->string());
    }
}
