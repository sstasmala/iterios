<?php

use yii\db\Migration;

/**
 * Class m180514_142826_alter_table_segments_result_list
 */
class m180514_142826_alter_table_segments_result_list extends Migration
{
    public function safeUp()
    {
        $this->dropIndex(
            'idx-segments_result_list-id_contact',
            'segments_result_list'
        );

        $this->dropForeignKey(
            'fk-segments_result_list-id_contact',
            'segments_result_list'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }
}
