<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery_types`.
 */
class m180531_124416_create_delivery_types_table extends Migration
{
    private $types = [
        [
            'for' => 'email',
            'type' => 'email_offer',
            'name' => 'eMail Offer'
        ],
        [
            'for' => 'email',
            'type' => 'email_letter',
            'name' => 'eMail Letter'
        ],
        [
            'for' => 'email',
            'type' => 'email_delivery',
            'name' => 'eMail Delivery'
        ],
        [
            'for' => 'sms',
            'type' => 'sms_offer',
            'name' => 'SMS Offer'
        ],
        [
            'for' => 'sms',
            'type' => 'sms_single',
            'name' => 'SMS Single'
        ],
        [
            'for' => 'sms',
            'type' => 'sms_delivery',
            'name' => 'SMS Delivery'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery_types', [
            'id' => $this->primaryKey(),
            'for' => $this->string(),
            'type' => $this->string(),
            'name' => $this->string(),
            'updated_at' => $this->bigInteger(),
            'updated_by' => $this->integer()
        ]);

        foreach ($this->types as $type) {
            $model = new \app\models\DeliveryTypes();
            $model->for = $type['for'];
            $model->type = $type['type'];
            $model->name = $type['name'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('delivery_types');
    }
}
