<?php

use yii\db\Migration;

/**
 * Class m180418_141005_update_logs_sms_table
 */
class m180418_141005_update_logs_sms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logs_sms', 'sms_id', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logs_sms', 'sms_id');
    }
}
