<?php

use yii\db\Migration;

/**
 * Class m180323_071447_update_requisites_field
 */
class m180323_071447_update_requisites_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_requisites_requisites_groups', 'requisites');
        $this->dropColumn('requisites','requisites_groups_id');

        $this->addColumn('requisites','requisites_group_id', 'integer');
        $this->addForeignKey('fk_requisites_requisites_group', 'requisites', 'requisites_group_id', 'requisites_groups', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn('requisites','group_position', 'integer');

        $this->addColumn('requisites','requisites_field_id', 'integer');
        $this->addForeignKey('fk_requisites_requisites_field', 'requisites', 'requisites_field_id', 'requisites_fields', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn('requisites','field_position', 'integer');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requisites','group_position');

        $this->dropForeignKey('fk_requisites_requisites_field', 'requisites');
        $this->dropColumn('requisites','requisites_field_id');

        $this->dropColumn('requisites','field_position');

        $this->dropForeignKey('fk_requisites_requisites_group', 'requisites');
        $this->dropColumn('requisites','requisites_group_id');

        $this->addColumn('requisites','requisites_groups_id', 'integer');
        $this->addForeignKey('fk_requisites_requisites_groups', 'requisites', 'requisites_groups_id', 'requisites_groups', 'id', 'RESTRICT', 'CASCADE');
    }
}
