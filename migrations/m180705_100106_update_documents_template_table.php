<?php

use yii\db\Migration;

/**
 * Class m180705_100106_update_documents_template_table
 */
class m180705_100106_update_documents_template_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('documents_template', 'type', $this->string());
        $this->addColumn('documents_template', 'tenant_id', $this->integer());

        $this->addForeignKey('fk-document_templates-tenant_id', 'documents_template', 'tenant_id', 'tenants', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-document_templates-tenant_id', 'documents_template');

        $this->dropColumn('documents_template', 'type');
        $this->dropColumn('documents_template', 'tenant_id');
    }
}
