<?php

use yii\db\Migration;

/**
 * Class m180413_100006_update_service_fields
 */
class m180413_100006_update_service_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('services_fields_default','name','varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('services_fields_default','name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180413_100006_update_service_fields cannot be reverted.\n";

        return false;
    }
    */
}
