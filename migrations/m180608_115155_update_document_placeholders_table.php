<?php

use yii\db\Migration;

/**
 * Class m180608_115155_update_document_placeholders_table
 */
class m180608_115155_update_document_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('document_placeholders', 'fake', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('document_placeholders', 'fake');
    }
}
