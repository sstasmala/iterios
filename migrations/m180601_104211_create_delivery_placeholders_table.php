<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery_placeholders`.
 */
class m180601_104211_create_delivery_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery_placeholders', [
            'id' => $this->primaryKey(),
            'short_code' => $this->string(),
            'module' => $this->string(),
            'path' => $this->string(),
            'default' => $this->string(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('delivery_placeholders');
    }
}
