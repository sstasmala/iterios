<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications_now`.
 */
class m180427_103418_create_notifications_now_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notifications_now', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'class' => $this->string(),
            'is_email' => $this->integer(),
            'is_notice' => $this->integer(),
            'text_email' => $this->text(),
            'text_notice' => $this->text(),
            'status' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notifications_now');
    }
}
