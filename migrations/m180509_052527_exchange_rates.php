<?php

use yii\db\Migration;

/**
 * Class m180509_052527_exchange_rates
 */
class m180509_052527_exchange_rates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE exchange_rates_id_seq;');
        $this->createTable('exchange_rates',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'exchange_rates_id_seq\')',
                'currency_id'=>'integer NOT NULL UNIQUE',
                'currency_iso'=>'varchar(10) NOT NULL UNIQUE',
                'rate'=>'decimal(11,2) NOT NULL DEFAULT 1.00',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE exchange_rates_id_seq OWNED BY exchange_rates.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('exchange_rates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180509_052527_exchange_rates cannot be reverted.\n";

        return false;
    }
    */
}
