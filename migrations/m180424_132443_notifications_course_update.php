<?php

use yii\db\Migration;

/**
 * Class m180424_132443_notifications_course_update
 */
class m180424_132443_notifications_course_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_course', 'trigger_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_course', 'trigger_id');
    }
}
