<?php

use yii\db\Migration;

/**
 * Class m180322_072933_update_auth_tables
 */
class m180322_072933_update_auth_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('auth_assignment', ['!=', 'item_name', 'system_admin']);
        $this->delete('auth_item', ['!=', 'name', 'system_admin']);
        $this->delete('auth_item_child', ['parent' => '1.Admin']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180322_072933_update_auth_tables cannot be reverted.\n";

        return true;
    }
}
