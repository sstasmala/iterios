<?php

use yii\db\Migration;

/**
 * Class m180504_074121_update_table_reminder_event_types
 */
class m180504_074121_update_table_reminder_event_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('reminder_event_types', 'status', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('reminder_event_types', 'status');
    }

}
