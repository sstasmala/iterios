<?php

use yii\db\Migration;

/**
 * Class m180426_060955_create_table_segments
 */
class m180426_060955_create_table_segments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('segments', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'modules' => $this->string(),
            'type' => $this->string(),
            'rules' => $this->text(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('segments');
    }

}
