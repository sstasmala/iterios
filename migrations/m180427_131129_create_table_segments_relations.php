<?php

use yii\db\Migration;

/**
 * Class m180427_131129_create_table_segments_relations
 */
class m180427_131129_create_table_segments_relations extends Migration
{
    public function safeUp()
    {
        $this->createTable('segments_relations', [
            'id' => $this->primaryKey(),
            'id_segment' => $this->integer(),
            'id_tenant' => $this->integer(),
            'type' => $this->string(),
            'name' => $this->string(),
            'count_phones' => $this->integer(),
            'count_emails' => $this->integer(),
            'count_contact' => $this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

        $this->createIndex(
            'idx-segments_relations-id_segment',
            'segments_relations',
            'id_segment'
        );

        $this->addForeignKey(
            'fk-segments_relations-id_segment',
            'segments_relations',
            'id_segment',
            'segments',
            'id',
            'NO ACTION'
        );

        $this->createIndex(
            'idx-segments_relations-id_tenant',
            'segments_relations',
            'id_tenant'
        );

        $this->addForeignKey(
            'fk-segments_relations-id_tenant',
            'segments_relations',
            'id_tenant',
            'tenants',
            'id',
            'NO ACTION'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropIndex(
            'idx-segments_relations-id_segment',
            'segments_relations'
        );

        $this->dropForeignKey(
            'fk-segments_relations-id_segment',
            'segments_relations'
        );

        $this->dropIndex(
            'idx-segments_relations-id_tenant',
            'segments_relations'
        );

        $this->dropForeignKey(
            'fk-segments_relations-id_tenant',
            'segments_relations'
        );

        $this->dropTable('segments_relations');
    }
}
