<?php

use yii\db\Migration;

/**
 * Handles dropping name from table `requisites`.
 */
class m180321_065516_drop_name_column_from_requisites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('requisites', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('requisites', 'name', $this->string());
    }
}
