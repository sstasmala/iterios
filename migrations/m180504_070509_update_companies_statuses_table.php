<?php

use yii\db\Migration;

/**
 * Class m180504_070509_update_companies_statuses_table
 */
class m180504_070509_update_companies_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies_statuses', 'color_type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies_statuses', 'color_type');
    }
}
