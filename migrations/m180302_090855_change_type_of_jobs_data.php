<?php

use yii\db\Migration;

/**
 * Class m180302_090855_change_type_of_jobs_data
 */
class m180302_090855_change_type_of_jobs_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('jobs','data');
        $this->addColumn('jobs','data','bytea');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jobs','data');
        $this->addColumn('jobs','data','text');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180302_090855_change_type_of_jobs_data cannot be reverted.\n";

        return false;
    }
    */
}
