<?php

use yii\db\Migration;

/**
 * Class m180509_120833_alter_table_suppliers
 */
class m180509_120833_alter_table_suppliers extends Migration
{
    public function safeUp()
    {
        $this->addColumn('suppliers', 'authorization_type', $this->string());
        $this->addColumn('suppliers', 'supplier_type_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('suppliers', 'authorization_type');
        $this->dropColumn('suppliers', 'supplier_type_id');

    }
}
