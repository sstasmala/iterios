<?php

use yii\db\Migration;

/**
 * Class m180625_130127_fix_segments_relation
 */
class m180625_130127_fix_segments_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->truncateTable('segments_result_list');
        $this->addForeignKey('segments_relations_segment','segments_relations','id_segment','segments','id','CASCADE','CASCADE');
        $this->addForeignKey('segments_relations_tenant','segments_relations','id_tenant','tenants','id','CASCADE','CASCADE');
        $this->addForeignKey('segments_result_segment_relation','segments_result_list','id_relation','segments_relations','id','CASCADE','CASCADE');
        $this->addForeignKey('segments_result_segment_contact','segments_result_list','id_contact','contacts','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('segments_relations_segment','segments_relations');
        $this->dropForeignKey('segments_relations_tenant','segments_relations');
        $this->dropForeignKey('segments_result_segment_relation','segments_result_list');
        $this->dropForeignKey('segments_result_segment_contact','segments_result_list');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180625_130127_fix_segments_relation cannot be reverted.\n";

        return false;
    }
    */
}
