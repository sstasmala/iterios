<?php

use yii\db\Migration;

/**
 * Class m180426_071200_notifications_log_update
 */
class m180426_071200_notifications_log_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_log', 'system_notifications_id', $this->integer());
        $this->addColumn('notifications_log', 'send_data', $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_log', 'system_notifications_id');
        $this->dropColumn('notifications_log', 'send_data');
    }
}
