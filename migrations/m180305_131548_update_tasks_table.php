<?php

use yii\db\Migration;

/**
 * Class m180305_131548_update_tasks_table
 */
class m180305_131548_update_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'tenant_id', $this->integer());

        $this->addForeignKey('tasks_tenant_fk', 'tasks', 'tenant_id', 'tenants', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tasks_tenant_fk', 'tasks');
        $this->dropColumn('tasks', 'tenant_id');
    }
}
