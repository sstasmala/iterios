<?php

use yii\db\Migration;

/**
 * Class m180301_133540_add_jobs_table
 */
class m180301_133540_add_jobs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE jobs_id_seq;');
        $this->createTable('jobs',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'jobs_id_seq\')',
                'executor'=>'varchar NOT NULL',
                'data'=>'text',
                'lock'=>'integer DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE jobs_id_seq OWNED BY jobs.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jobs');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180301_133540_add_jobs_table cannot be reverted.\n";

        return false;
    }
    */
}
