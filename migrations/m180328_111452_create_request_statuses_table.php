<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_statuses`.
 */
class m180328_111452_create_request_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request_statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'color_type' => $this->string(),
            'is_default' => $this->boolean()->defaultValue(false),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('request_statuses');
    }
}
