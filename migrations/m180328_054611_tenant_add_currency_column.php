<?php

use yii\db\Migration;

/**
 * Class m180328_054611_tenant_add_currency_column
 */
class m180328_054611_tenant_add_currency_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','usage_currency',$this->string());
        $this->addColumn('tenants','main_currency',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants','usage_currency');
        $this->dropColumn('tenants','main_currency');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180328_054611_tenant_add_currency_column cannot be reverted.\n";

        return false;
    }
    */
}
