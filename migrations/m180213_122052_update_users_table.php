<?php

use yii\db\Migration;

/**
 * Class m180213_122052_update_users_table
 */
class m180213_122052_update_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_users_tenants', 'users', 'current_tenant_id', 'tenants', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_users_tenants', 'users');
    }
}
