<?php

use yii\db\Migration;

/**
 * Class m180327_070222_services_fields
 */
class m180327_070222_services_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE services_fields_id_seq;');
        $this->createTable('services_fields',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_fields_id_seq\')',
                'code'=>'varchar(250) NOT NULL',
                'type'=>'varchar(250) NOT NULL',
                'is_additional'=>'integer NOT NULL DEFAULT 0',
                'in_header'=>'integer NOT NULL DEFAULT 0',
                'variables'=>'text',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE services_fields_id_seq OWNED BY services_fields.id;');

        $this->execute('CREATE SEQUENCE services_fields_default_id_seq;');
        $this->createTable('services_fields_default',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_fields_default_id_seq\')',
                'code'=>'varchar(250) NOT NULL',
                'type'=>'varchar(250) NOT NULL',
                'is_additional'=>'integer NOT NULL DEFAULT 0',
                'in_header'=>'integer NOT NULL DEFAULT 0',
                'variables'=>'text',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE services_fields_default_id_seq OWNED BY services_fields_default.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('services_fields');
        $this->dropTable('services_fields_default');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_070222_services_fields cannot be reverted.\n";

        return false;
    }
    */
}
