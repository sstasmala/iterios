<?php

use yii\db\Migration;

/**
 * Class m180328_052653_change_service_table
 */
class m180328_052653_change_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('services_fields_default','is_default','integer DEFAULT 0');
        $this->dropTable('services_fields');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropColumn('services_fields_default','is_default');
        $this->execute('CREATE SEQUENCE services_fields_id_seq;');
        $this->createTable('services_fields',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_fields_id_seq\')',
                'code'=>'varchar(250) NOT NULL',
                'type'=>'varchar(250) NOT NULL',
                'is_additional'=>'integer NOT NULL DEFAULT 0',
                'in_header'=>'integer NOT NULL DEFAULT 0',
                'variables'=>'text',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE services_fields_id_seq OWNED BY services_fields.id;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180328_052653_change_service_table cannot be reverted.\n";

        return false;
    }
    */
}
