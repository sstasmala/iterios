<?php

use yii\db\Migration;

/**
 * Handles the creation of table `placeholders`.
 */
class m180717_120638_create_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('placeholders_document', [
            'id' => $this->primaryKey(),
            'placeholder_id' => $this->integer(),
            'template' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-placeholders_document-placeholder_id', 'placeholders_document', 'placeholder_id', 'placeholders', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('placeholders_delivery', [
            'id' => $this->primaryKey(),
            'placeholder_id' => $this->integer(),
            'template' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-placeholders_delivery-placeholder_id', 'placeholders_delivery', 'placeholder_id', 'placeholders', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('placeholders_delivery_types', [
            'id' => $this->primaryKey(),
            'delivery_type_id' => $this->integer(),
            'placeholder_delivery_id' => $this->integer()
        ]);

        $this->addForeignKey('fk-placeholders_delivery_types-delivery_type_id', 'placeholders_delivery_types', 'delivery_type_id', 'delivery_types', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-placeholders_delivery_types-placeholder_delivery_id', 'placeholders_delivery_types', 'placeholder_delivery_id', 'placeholders_delivery', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-placeholders_delivery_types-placeholder_delivery_id', 'placeholder_delivery_types');
        $this->dropForeignKey('fk-placeholders_delivery_types-delivery_type_id', 'placeholder_delivery_types');

        $this->dropTable('placeholders_delivery_types');

        $this->dropForeignKey('fk-placeholders_delivery-placeholder_id', 'placeholders_delivery');
        $this->dropTable('placeholders_delivery');

        $this->dropForeignKey('fk-placeholders_document-placeholder_id', 'placeholders_document');
        $this->dropTable('placeholders_documents');
    }
}
