<?php

use yii\db\Migration;

/**
 * Class m180531_065830_providers_payments
 */
class m180531_065830_providers_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_tourists_payments','type','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_tourists_payments','type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180531_065830_providers_payments cannot be reverted.\n";

        return false;
    }
    */
}
