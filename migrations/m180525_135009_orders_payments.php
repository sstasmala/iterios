<?php

use yii\db\Migration;

/**
 * Class m180525_135009_orders_payments
 */
class m180525_135009_orders_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE orders_tourists_payments_id_seq;');
        $this->createTable('orders_tourists_payments',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_tourists_payments_id_seq\')',
                'order_id'=>'bigint NOT NULL',
                'link_id'=>'bigint NOT NULL',
                'ext_rates'=>'text NOT NULL',
                'sum'=>'decimal(11,2) NOT NULL DEFAULT 1.00',
                'date'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE orders_tourists_payments_id_seq OWNED BY orders_tourists_payments.id;');
        $this->addForeignKey('orders_tr_pm_orders_fk','orders_tourists_payments','order_id',
            'orders','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_tr_pm_orders_link_fk','orders_tourists_payments','link_id',
            'orders_services_links','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_tr_pm_orders_fk','orders_tourists_payments');
        $this->dropForeignKey('orders_tr_pm_orders_link_fk','orders_tourists_payments');
        $this->dropTable('orders_tourists_payments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180525_135009_orders_payments cannot be reverted.\n";

        return false;
    }
    */
}
