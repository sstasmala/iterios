<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180209_120433_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'middle_name' => $this->string(),
            'email' => $this->string(129)->unique(),
            'phone' => $this->string(32)->unique(),
            'photo' => $this->string(),
            'status' => $this->integer(2),
            'last_login' => $this->bigInteger(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
