<?php

use yii\db\Migration;

/**
 * Class m180614_102811_update_tenant_settings
 */
class m180614_102811_update_tenant_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','currency_display','varchar(50) NOT NULL DEFAULT \'symbol\'');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants','currency_display');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180614_102811_update_tenant_settings cannot be reverted.\n";

        return false;
    }
    */
}
