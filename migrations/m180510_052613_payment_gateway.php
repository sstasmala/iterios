<?php

use yii\db\Migration;

/**
 * Class m180510_052613_payment_gateway
 */
class m180510_052613_payment_gateway extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE payment_gateways_id_seq;');
        $this->createTable('payment_gateways',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'payment_gateways_id_seq\')',
                'name'=>'varchar(255) NOT NULL',
                'demo'=>'integer NOT NULL DEFAULT 0',
                'logo'=>'text',
                'countries'=>'text',
                'currencies'=>'text',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE payment_gateways_id_seq OWNED BY payment_gateways.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_gateways');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180510_052613_payment_gateway cannot be reverted.\n";

        return false;
    }
    */
}
