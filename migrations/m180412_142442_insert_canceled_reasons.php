<?php

use yii\db\Migration;

/**
 * Class m180412_142442_insert_canceled_reasons
 */
class m180412_142442_insert_canceled_reasons extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->insert('request_canceled_reasons', [
            'id' => '999999',
            'type' => 'system',
            'name' => 'Other',
        ]);

        $translate =  new \app\models\Translations();//::saveTranslation($key, $this->{$v}, $language, $model, $this->log_id));
        $language = \app\models\Languages::findOne(['iso'=>'en']);

        $translate->key = 'request_canceled_reasons.name.999999';
        $translate->value = 'Other';
        $translate->lang_id = $language->id;
        $translate->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('request_canceled_reasons', ['id' => 999999]);

    }


}
