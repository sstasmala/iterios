<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_hotel_categories`.
 */
class m180330_062841_create_request_hotel_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request_hotel_categories', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer(),
            'hotel_category' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('request_hotel_categories_requests_fk','request_hotel_categories','request_id','requests','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('request_hotel_categories_requests_fk','request_hotel_categories');
        $this->dropTable('request_hotel_categories');
    }
}
