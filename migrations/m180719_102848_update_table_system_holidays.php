<?php

use yii\db\Migration;

/**
 * Class m180719_102848_update_table_system_holidays
 */
class m180719_102848_update_table_system_holidays extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('system_holidays', 'country_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('system_holidays', 'country_id');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180719_102848_update_table_system_holidays cannot be reverted.\n";

        return false;
    }
    */
}
