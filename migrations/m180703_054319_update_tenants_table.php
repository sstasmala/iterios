<?php

use yii\db\Migration;

/**
 * Class m180703_054319_update_tenants_table
 */
class m180703_054319_update_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants', 'status', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants', 'status');
    }
}
