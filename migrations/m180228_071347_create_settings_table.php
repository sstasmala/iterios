<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180228_071347_create_settings_table extends Migration
{
    protected $system_provider = [
        'key' => 'provider_system',
        'value' => null
    ];

    protected $public_provider = [
        'key' => 'provider_public',
        'value' => null
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(55)->unique(),
            'value' => $this->string(),
            'updated_at' => $this->bigInteger(),
            'updated_by' => $this->integer(),
        ]);

        $this->insert('settings', $this->system_provider);
        $this->insert('settings', $this->public_provider);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete('settings', ['key' => $this->system_provider['key']]);
        $this->delete('settings', ['key' => $this->public_provider['key']]);

        $this->dropTable('settings');
    }
}
