<?php

use yii\db\Migration;

/**
 * Class m180502_061030_fix_companies_table
 */
class m180502_061030_fix_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('company_emails_fk','companies_emails');
        $this->addForeignKey('company_emails_fk','companies_emails','company_id','companies','id','CASCADE');

        $this->dropForeignKey('company_phones_fk','companies_phones');
        $this->addForeignKey('company_phones_fk','companies_phones','company_id','companies','id','CASCADE');

        $this->dropForeignKey('company_socials_fk','companies_socials');
        $this->addForeignKey('company_socials_fk','companies_socials','company_id','companies','id','CASCADE');

        $this->dropForeignKey('company_messengers_fk','companies_messengers');
        $this->addForeignKey('company_messengers_fk','companies_messengers','company_id','companies','id','CASCADE');

        $this->dropForeignKey('company_sites_fk','companies_sites');
        $this->addForeignKey('company_sites_fk','companies_sites','company_id','companies','id','CASCADE');

        $this->dropForeignKey('company_tags_fk','companies_tags');
        $this->addForeignKey('company_tags_fk','companies_tags','company_id','companies','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('company_emails_fk','companies_emails');
        $this->addForeignKey('company_emails_fk','companies_emails','company_id','companies','id');

        $this->dropForeignKey('company_phones_fk','companies_phones');
        $this->addForeignKey('company_phones_fk','companies_phones','company_id','companies','id');

        $this->dropForeignKey('company_socials_fk','companies_socials');
        $this->addForeignKey('company_socials_fk','companies_socials','company_id','companies','id');

        $this->dropForeignKey('company_messengers_fk','companies_messengers');
        $this->addForeignKey('company_messengers_fk','companies_messengers','company_id','companies','id');

        $this->dropForeignKey('company_sites_fk','companies_sites');
        $this->addForeignKey('company_sites_fk','companies_sites','company_id','companies','id');

        $this->dropForeignKey('company_tags_fk','companies_tags');
        $this->addForeignKey('company_tags_fk','companies_tags','company_id','companies','id');
    }
}
