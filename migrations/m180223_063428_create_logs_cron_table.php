<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs_cron`.
 */
class m180223_063428_create_logs_cron_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logs_cron', [
            'id' => $this->primaryKey(),
            'command' => $this->string(),
            'output_file' => $this->string(),
            'time_start' => $this->bigInteger(),
            'time_end' => $this->bigInteger(),
            'memory_usage' => $this->string(32),
            'status' => $this->string(32),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('logs_cron');
    }
}
