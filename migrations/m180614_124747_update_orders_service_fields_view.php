<?php

use yii\db\Migration;

/**
 * Class m180612_122747_create_orders_service_fields_view
 */
class m180614_124747_update_orders_service_fields_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DROP VIEW orders_service_fields_view");
        $this->execute("CREATE OR REPLACE VIEW orders_service_fields_view AS (SELECT orders.id, orders_services_values.value, services_fields_default.type FROM orders_services_values
LEFT JOIN services_fields_default ON orders_services_values.field_id = services_fields_default.id
LEFT JOIN orders_services_links ON orders_services_values.link_id = orders_services_links.id
LEFT JOIN orders ON orders_services_links.order_id = orders.id);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP VIEW orders_service_fields_view");

        $this->execute("CREATE OR REPLACE VIEW orders_service_fields_view AS (SELECT orders.*, orders_services_values.value, services_fields_default.type FROM orders_services_values
LEFT JOIN services_fields_default ON orders_services_values.field_id = services_fields_default.id
LEFT JOIN orders_services_links ON orders_services_values.link_id = orders_services_links.id
LEFT JOIN orders ON orders_services_links.order_id = orders.id
UNION SELECT orders.*, '', '' FROM orders WHERE orders.id NOT IN (SELECT orders_services_links.order_id FROM orders_services_links));");
    }
}
