<?php

use yii\db\Migration;

/**
 * Class m180328_054950_services_table
 */
class m180328_054950_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE services_id_seq;');
        $this->createTable('services',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_id_seq\')',
                'name'=>'varchar(500) NOT NULL',
                'icon'=>'text NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE services_id_seq OWNED BY services.id;');

        $this->execute('CREATE SEQUENCE services_fields_id_seq;');
        $this->createTable('services_fields',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'services_fields_id_seq\')',
                'id_service'=>'bigint NOT NULL',
                'id_field'=>'bigint NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE services_fields_id_seq OWNED BY services_fields.id;');
        $this->addForeignKey('services_fields_fk_services','services_fields','id_service','services','id','CASCADE','CASCADE');
        $this->addForeignKey('services_fields_fk_services_fields','services_fields','id_field','services_fields_default','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('services_fields_fk_services','services_fields');
        $this->dropForeignKey('services_fields_fk_services_fields','services_fields');
        $this->dropTable('services');
        $this->dropTable('services_fields');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180328_054950_services_table cannot be reverted.\n";

        return false;
    }
    */
}
