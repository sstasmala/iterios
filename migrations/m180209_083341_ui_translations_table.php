<?php

use yii\db\Migration;

/**
 * Class m180209_083341_ui_translations_table
 */
class m180209_083341_ui_translations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE ui_translations_id_seq;');
        $this->createTable('ui_translations',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'ui_translations_id_seq\')',
                'group'=>'varchar(250)',
                'code'=>'varchar(250) NOT NULL',
                'value'=>'varchar',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'bigint',
                'updated_by'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE ui_translations_id_seq OWNED BY ui_translations.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ui_translations');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_083341_ui_translations_table cannot be reverted.\n";

        return false;
    }
    */
}
