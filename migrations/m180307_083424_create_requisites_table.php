<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requisites`.
 */
class m180307_083424_create_requisites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('requisites_fields', [
            'id' => $this->primaryKey(),
            'type' => 'integer NOT NULL',
            'code' => 'varchar(250) NOT NULL UNIQUE',
            'variants'=>'text DEFAULT NULL',
        ]);

        $this->createTable('requisites', [
            'id' => $this->primaryKey(),
            'country_id' => 'integer',
            'requisites_field_id' => 'integer',
            'name' => 'varchar',
        ]);

        $this->addForeignKey('fk_requisites_fields_requisites_field', 'requisites', 'requisites_field_id', 'requisites_fields', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('requisites_sets', [
            'id' => $this->primaryKey(),
            'tenant_id' => 'integer',
            'name' => 'varchar',
        ]);
        $this->addForeignKey('fk_requisites_sets_requisites', 'requisites_sets', 'tenant_id', 'tenants', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('requisites_tenants', [
            'id' => $this->primaryKey(),
            'requisite_id' => 'integer',
            'set_id' => 'integer',
            'value' => 'varchar',
        ]);

        $this->addForeignKey('fk_requisites_tenants_tenants', 'requisites_tenants', 'requisite_id', 'requisites', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_requisites_tenants_sets', 'requisites_tenants', 'set_id', 'requisites_sets', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_requisites_fields_requisites_field', 'requisites');
        $this->dropForeignKey('fk_requisites_sets_requisites', 'requisites_sets');
        $this->dropForeignKey('fk_requisites_tenants_tenants', 'requisites_tenants');
        $this->dropForeignKey('fk_requisites_tenants_sets', 'requisites_tenants');

        $this->dropTable('requisites');
        $this->dropTable('requisites_sets');
        $this->dropTable('requisites_fields');
        $this->dropTable('requisites_tenants');
    }
}
