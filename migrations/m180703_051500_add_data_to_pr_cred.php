<?php

use yii\db\Migration;

/**
 * Class m180703_051500_add_data_to_pr_cred
 */
class m180703_051500_add_data_to_pr_cred extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('providers_credentials','provider_data','text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('providers_credentials','provider_data');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180703_051500_add_data_to_pr_cred cannot be reverted.\n";

        return false;
    }
    */
}
