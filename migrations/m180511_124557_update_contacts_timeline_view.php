<?php

use yii\db\Migration;

/**
 * Class m180511_124557_update_contacts_timeline_view
 */
class m180511_124557_update_contacts_timeline_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DROP VIEW contacts_timeline_view");
        $this->execute("CREATE OR REPLACE VIEW contacts_timeline_view AS (SELECT logs.id, null as value, null as contact_id, 0 as request_id, logs.created_at, logs.created_by, null as updated_at, null as updated_by, logs.table_name, logs.item_id, logs.group_id, logs.tenant_id, logs.action, logs.data, null as title, null as description, null as status, null as type_id, 'log' as type FROM logs WHERE logs.group_id IS NULL
UNION
SELECT tasks.id, null, tasks.contact_id, 0, tasks.created_at, tasks.created_by, tasks.updated_at, tasks.updated_by, null, null, null, null, null, null, tasks.title, tasks.description, tasks.status, tasks.type_id, 'task' as type FROM tasks WHERE tasks.contact_id IS NOT NULL
UNION
SELECT request_notes.id, request_notes.value, request_notes.contact_id, request_notes.request_id, request_notes.created_at, request_notes.created_by, request_notes.updated_at, request_notes.updated_by, null, null, null, null, null, null, null, null, null, null, 'request_note' as type FROM request_notes
UNION
SELECT contacts_notes.id, contacts_notes.value, contacts_notes.contact_id, 0, contacts_notes.created_at, contacts_notes.created_by, contacts_notes.updated_at, contacts_notes.updated_by, null, null, null, null, null, null, null, null, null, null, 'contact_note' as type FROM contacts_notes);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //$this->execute("DROP VIEW contacts_timeline_view");
    }
}
