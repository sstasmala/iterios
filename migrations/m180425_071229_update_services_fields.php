<?php

use yii\db\Migration;

/**
 * Class m180425_071229_update_services_fields
 */
class m180425_071229_update_services_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('services_fields_default','size','integer NOT NULL DEFAULT 1');
        $this->addColumn('services_fields_default','position','integer');
        $this->addColumn('services_fields_default','only_for_order','integer NOT NULL DEFAULT 0');
        $this->addColumn('services_fields_default','in_creation_view','integer NOT NULL DEFAULT 0');
        $this->addColumn('services_fields_default','is_bold','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('services_fields_default','size');
        $this->dropColumn('services_fields_default','position');
        $this->dropColumn('services_fields_default','only_for_order');
        $this->dropColumn('services_fields_default','in_creation_view');
        $this->dropColumn('services_fields_default','is_bold');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180425_071229_update_services_fields cannot be reverted.\n";

        return false;
    }
    */
}
