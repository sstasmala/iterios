<?php

use yii\db\Migration;

/**
 * Class m180515_103543_update_TO
 */
class m180515_103543_update_TO extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tariffs_orders','activation_date','bigint');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tariffs_orders','activation_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180515_103543_update_TO cannot be reverted.\n";

        return false;
    }
    */
}
