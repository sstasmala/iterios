<?php

use yii\db\Migration;

/**
 * Class m180514_055208_fo_update_name
 */
class m180514_055208_fo_update_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tariffs_orders','name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('tariffs_orders','name','varchar(255) NOT NULL DEFAULT \'\'');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180514_055208_fo_update_name cannot be reverted.\n";

        return false;
    }
    */
}
