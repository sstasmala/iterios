<?php

use yii\db\Migration;

/**
 * Class m180328_060132_update_tenants_table
 */
class m180328_060132_update_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants', 'system_tags', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants', 'system_tags');
    }
}
