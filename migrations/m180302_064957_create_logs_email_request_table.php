<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs_email_request`.
 */
class m180302_064957_create_logs_email_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logs_email_request', [
            'id' => $this->primaryKey(),
            'event' => $this->string()->notNull(),
            'log_email_id' => $this->integer()->notNull(),
            'email_provider_id' => $this->integer()->notNull(),
            'method' => $this->string()->notNull(),
            'headers' => $this->text()->notNull(),
            'body' => $this->text(),
            'url' => $this->string()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        $this->addForeignKey('logs_email_request_logs_email_fk', 'logs_email_request',
            'log_email_id', 'logs_email', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('logs_email_request_email_providers_fk', 'logs_email_request',
            'email_provider_id', 'email_providers', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('logs_email_request_email_providers_fk', 'logs_email_request');
        $this->dropForeignKey('logs_email_request_logs_email_fk', 'logs_email_request');
        $this->dropTable('logs_email_request');
    }
}
