<?php

use yii\db\Migration;

/**
 * Class m180605_122844_update_orders
 */
class m180605_122844_update_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders','total_sum','decimal(11,2)');
        $this->addColumn('orders','total_sum_wd','decimal(11,2)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders','total_sum');
        $this->dropColumn('orders','total_sum_wd');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_122844_update_orders cannot be reverted.\n";

        return false;
    }
    */
}
