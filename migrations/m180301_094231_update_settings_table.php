<?php

use yii\db\Migration;

/**
 * Class m180301_094231_update_settings_table
 */
class m180301_094231_update_settings_table extends Migration
{
    protected $from_name = [
        'key' => 'email_from_name',
        'value' => null
    ];

    protected $from_email = [
        'key' => 'email_from',
        'value' => null
    ];

    protected $reply_to = [
        'key' => 'email_reply_to',
        'value' => null
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', $this->from_name);
        $this->insert('settings', $this->from_email);
        $this->insert('settings', $this->reply_to);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings', ['key' => $this->from_name['key']]);
        $this->delete('settings', ['key' => $this->from_email['key']]);
        $this->delete('settings', ['key' => $this->reply_to['key']]);
    }
}
