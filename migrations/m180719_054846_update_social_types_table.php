<?php

use yii\db\Migration;

/**
 * Class m180719_054846_update_social_types_table
 */
class m180719_054846_update_social_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('messengers_types','img','varchar');
        $this->addColumn('socials_types','img','varchar');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('messengers_types','img');
        $this->dropColumn('socials_types','img');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180719_054846_update_social_types_table cannot be reverted.\n";

        return false;
    }
    */
}
