<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notificationsnow_log`.
 */
class m180511_121202_create_notificationsnow_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notificationsnow_log', [
            'id' => $this->primaryKey(),
            'status' => $this->integer()->defaultValue(0),
            'error' => $this->text(),
            'notification_id' => $this->integer(),
            'user_id' => $this->integer(),
            'element' => $this->text(),
            'action' => $this->string(),
            'chanel' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notificationsnow_log');
    }
}
