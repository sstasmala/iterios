<?php

use yii\db\Migration;

/**
 * Class m180424_055616_create_table_reminder_event_types
 */
class m180424_055616_create_table_reminder_event_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reminder_event_types', [
            'id' => $this->primaryKey(),
            'action' => $this->string(),
            'name' => $this->string(),
            'icon_class' => $this->string(),
            'color_type' => $this->string(),
            //'language_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('reminder_event_types');
    }
}
