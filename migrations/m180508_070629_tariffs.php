<?php

use yii\db\Migration;

/**
 * Class m180508_070629_tariffs
 */
class m180508_070629_tariffs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE tariffs_id_seq;');
        $this->createTable('tariffs',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'tariffs_id_seq\')',
                'name'=>'varchar(255) NOT NULL',
                'description'=>'text',
                'tariff_duration'=>'integer NOT NULL DEFAULT 0',
                'tariff_start'=>'bigint',
                'tariff_end'=>'bigint',
                'country' => 'text',
                'days_before_stop' => 'integer NOT NULL DEFAULT 0',
                'price' => 'decimal(11,2) NOT NULL DEFAULT 0.00',
                'need_act' => 'integer NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE tariffs_id_seq OWNED BY tariffs.id;');


        $this->execute('CREATE SEQUENCE features_tariffs_id_seq;');
        $this->createTable('features_tariffs',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'features_tariffs_id_seq\')',
                'tariff_id'=>'bigint NOT NULL',
                'feature_id'=>'bigint NOT NULL',
                'count'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE features_tariffs_id_seq OWNED BY features_tariffs.id;');
        $this->addForeignKey('features_tariffs_tariffs_fk','features_tariffs','tariff_id',
        'tariffs','id','CASCADE','CASCADE');
        $this->addForeignKey('features_tariffs_features_fk','features_tariffs','feature_id',
        'tariffs_feature','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('features_tariffs_features_fk','features_tariffs');
        $this->dropForeignKey('features_tariffs_tariffs_fk','features_tariffs');
        $this->dropTable('features_tariffs');
        $this->dropTable('tariffs');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180508_070629_tariffs cannot be reverted.\n";

        return false;
    }
    */
}
