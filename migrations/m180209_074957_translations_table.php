<?php

use yii\db\Migration;

/**
 * Class m180209_074957_translations_table
 */
class m180209_074957_translations_table extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE translations_id_seq;');
        $this->createTable('translations',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'translations_id_seq\')',
                'key'=>'varchar(250) NOT NULL',
                'value'=>'varchar',
                'lang_id'=>'integer NOT NULL'
            ]
        );
        $this->execute('ALTER SEQUENCE translations_id_seq OWNED BY translations.id;');
        $this->addForeignKey('translations_lang_fk','translations','lang_id','languages','id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('translations_lang_fk','translations');
        $this->dropTable('translations');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_074957_translations_table cannot be reverted.\n";

        return false;
    }
    */
}
