<?php

use yii\db\Migration;

/**
 * Class m180514_134756_create_table_send_sms
 */
class m180514_134756_create_table_send_sms extends Migration
{
    public function safeUp()
    {
        $this->createTable('send_sms', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'status'=>$this->integer(),
            'count_phones'=>$this->integer(),
            'count_delivered'=>$this->integer(),
            'count_not_delivered'=>$this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('send_sms');
    }
}
