<?php

use yii\db\Migration;

/**
 * Class m180306_112502_update_tasks_table
 */
class m180306_112502_update_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'email_reminder_send', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'email_reminder_send');
    }
}
