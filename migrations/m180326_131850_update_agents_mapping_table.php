<?php

use yii\db\Migration;

/**
 * Class m180326_131850_update_agents_mapping_table
 */
class m180326_131850_update_agents_mapping_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('agents_mapping', 'block', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('agents_mapping', 'block');
    }
}
