<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_providers`.
 */
class m180301_094636_create_email_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('email_providers', [
            'id' => $this->primaryKey(),
            'provider' => $this->string(),
            'name' => $this->string()->unique(),
            'api_key' => $this->string()->unique(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('email_providers');
    }
}
