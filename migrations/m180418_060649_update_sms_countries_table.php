<?php

use yii\db\Migration;

/**
 * Class m180418_060649_update_sms_countries_table
 */
class m180418_060649_update_sms_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/json/country-calling-codes.json'), true);

        /** @var array $data */
        foreach ($data as $country) {
            $model = new \app\models\SmsCountries();
            $model->name = $country['name'];
            $model->iso_code = $country['code'];
            $model->dial_code = $country['callingCode'];

            if ($model->save())
                echo 'Country - ' . $country['name'] . ' (ID #' . $model->id . ') successfully saved!' . PHP_EOL;
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('sms_countries');
    }
}
