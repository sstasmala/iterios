<?php

use yii\db\Migration;

/**
 * Class m180417_132618_create_table_documents_type
 */
class m180417_132618_create_table_documents_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documents_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('documents_type');
    }

}
