<?php

use yii\db\Migration;

/**
 * Class m180416_122807_create_table_documents_placeholders
 */
class m180416_122807_create_table_documents_placeholders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('document_placeholders', [
            'id' => $this->primaryKey(),
            'short_code' => $this->string(),
            'module' => $this->string(),
            'path' => $this->string(),
            'default' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('document_placeholders');
    }
}
