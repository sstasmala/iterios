<?php

use yii\db\Migration;

/**
 * Class m180706_052728_update_document_template_table
 */
class m180706_052728_update_document_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\models\DocumentsTemplate::updateAll(['type' => \app\models\DocumentsTemplate::SYSTEM_TYPE]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
