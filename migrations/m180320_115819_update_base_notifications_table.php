<?php

use yii\db\Migration;

/**
 * Class m180320_115819_update_base_notifications_table
 */
class m180320_115819_update_base_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('base_notifications','schedule');
        $this->dropColumn('base_notifications','task_config');
        $this->addColumn('base_notifications','trigger','integer NOT NULL DEFAULT 0');
        $this->addColumn('base_notifications','trigger_time','varchar');
        $this->addColumn('base_notifications','task_name','varchar(255)');
        $this->addColumn('base_notifications','task_body','text');
        $this->addColumn('base_notifications','is_email','integer NOT NULL DEFAULT 0');
        $this->addColumn('base_notifications','is_system_notification','integer NOT NULL DEFAULT 0');
        $this->addColumn('base_notifications','is_task','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('base_notifications','schedule','text');
        $this->addColumn('base_notifications','task_config','text');
        $this->dropColumn('base_notifications','trigger');
        $this->dropColumn('base_notifications','trigger_time');
        $this->dropColumn('base_notifications','task_name');
        $this->dropColumn('base_notifications','task_body');
        $this->dropColumn('base_notifications','is_email');
        $this->dropColumn('base_notifications','is_system_notification');
        $this->dropColumn('base_notifications','is_task');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180320_115819_update_base_notifications_table cannot be reverted.\n";

        return false;
    }
    */
}
