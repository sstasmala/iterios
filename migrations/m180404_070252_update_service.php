<?php

use yii\db\Migration;

/**
 * Class m180404_070252_update_service
 */
class m180404_070252_update_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('services_fields','position','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('services_fields','position');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180404_070252_update_service cannot be reverted.\n";

        return false;
    }
    */
}
