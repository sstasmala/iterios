<?php

use yii\db\Migration;

/**
 * Class m180525_070706_orders_ex_rates
 */
class m180525_070706_orders_ex_rates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE orders_ex_rates_id_seq;');
        $this->createTable('orders_ex_rates',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_ex_rates_id_seq\')',
                'order_id'=>'bigint NOT NULL',
                'currency'=>'integer NOT NULL',
                'value'=>'decimal(11,2) NOT NULL DEFAULT 1.00'
            ]
        );
        $this->execute('ALTER SEQUENCE orders_ex_rates_id_seq OWNED BY orders_ex_rates.id;');
        $this->addForeignKey('orders_ex_rates_orders_fk','orders_ex_rates','order_id',
            'orders','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_ex_rates_orders_fk','orders_ex_rates');
        $this->dropTable('orders_ex_rates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180525_070706_orders_ex_rates cannot be reverted.\n";

        return false;
    }
    */
}
