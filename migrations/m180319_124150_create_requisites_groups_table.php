<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requisites_groups`.
 */
class m180319_124150_create_requisites_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('requisites_groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('requisites_groups');
    }
}
