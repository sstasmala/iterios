<?php

use yii\db\Migration;

/**
 * Class m180522_070528_create_order_templates
 */
class m180522_070528_create_order_documents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_documents', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'template_id' => $this->integer(),
            'body' => $this->text(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

        $this->addForeignKey('fk-order_documents-order_id', 'order_documents', 'order_id', 'orders', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-order_documents-template_id', 'order_documents', 'template_id', 'documents_template', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_documents-template_id', 'order_documents');
        $this->dropForeignKey('fk-order_documents-order_id', 'order_documents');

        $this->dropTable('order_documents');
    }
}
