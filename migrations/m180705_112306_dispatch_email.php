<?php

use yii\db\Migration;

/**
 * Class m180705_112306_dispatch_email
 */
class m180705_112306_dispatch_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE emails_dispatch_id_seq;');
        $this->createTable('emails_dispatch',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'emails_dispatch_id_seq\')',
                'name'=>'varchar(255) NOT NULL',
                'subject'=>'varchar(255) NOT NULL',
                'provider_id'=>'bigint NOT NULL',
                'status'=>'integer NOT NULL DEFAULT 0',
                'opens'=>'integer NOT NULL DEFAULT 0',
                'clicks'=>'integer NOT NULL DEFAULT 0',
                'errors'=>'text',
                'receivers'=>'text',
                'email'=>'text NOT NULL',
                'email_type'=>'integer NOT NULL DEFAULT 0',
                'owner'=>'bigint NOT NULL DEFAULT 0',
                'date'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE emails_dispatch_id_seq OWNED BY emails_dispatch.id;');
        $this->addForeignKey('emails_dispatch_provider_fk','emails_dispatch','provider_id',
            'providers_credentials','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('emails_dispatch_provider_fk','emails_dispatch');
        $this->dropTable('emails_dispatch');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180705_112306_dispatch_email cannot be reverted.\n";

        return false;
    }
    */
}
