<?php

use yii\db\Migration;

/**
 * Class m180607_053450_update_agency_contacts_table
 */
class m180607_053450_update_agency_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('agencies_tenant_fk','agencies');

        $this->dropForeignKey('agencies_phones_fk','agencies_phones');
        $this->dropForeignKey('types_phones_fk','agencies_phones');

        $this->dropForeignKey('agencies_socials_fk','agencies_socials');
        $this->dropForeignKey('types_socials_fk','agencies_socials');

        $this->dropForeignKey('agencies_emails_fk','agencies_emails');
        $this->dropForeignKey('types_emails_fk','agencies_emails');

        $this->dropForeignKey('agencies_messengers_fk','agencies_messengers');
        $this->dropForeignKey('types_messengers_fk','agencies_messengers');

        $this->addForeignKey('agencies_tenant_fk','agencies','tenant_id','tenants','id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('agencies_phones_fk','agencies_phones','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('types_phones_fk','agencies_phones','type_id','phones_types','id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('agencies_socials_fk','agencies_socials','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('types_socials_fk','agencies_socials','type_id','socials_types','id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('agencies_emails_fk','agencies_emails','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('types_emails_fk','agencies_emails','type_id','emails_types','id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('agencies_messengers_fk','agencies_messengers','tenant_id','tenants','id','CASCADE', 'CASCADE');
        $this->addForeignKey('types_messengers_fk','agencies_messengers','type_id','messengers_types','id', 'SET NULL', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
