<?php

use yii\db\Migration;

/**
 * Class m180607_112837_update_services_statuses
 */
class m180607_112837_update_services_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_services_links','status','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_services_links','status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_112837_update_services_statuses cannot be reverted.\n";

        return false;
    }
    */
}
