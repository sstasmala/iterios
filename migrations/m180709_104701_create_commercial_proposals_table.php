<?php

use yii\db\Migration;

/**
 * Handles the creation of table `commercial_proposals`.
 */
class m180709_104701_create_commercial_proposals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('commercial_proposals', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'log_id' => $this->integer(),
            'status' => $this->string(),
            'status_created_at' => $this->bigInteger(),
            'link_code' => $this->string(),
            'template' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('commercial_proposals_links', [
            'id' => $this->primaryKey(),
            'original_link' => $this->string(),
            'short_link' => $this->string(),
            'clicks_count' => $this->integer(),
            'clicks_data' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('commercial_proposals_services', [
            'id' => $this->primaryKey(),
            'commercial_proposal_id' => $this->integer(),
            'request_service_link_id' => $this->integer()
        ]);

        $this->addForeignKey('fk-cp_services-cp_id', 'commercial_proposals_services', 'commercial_proposal_id', 'commercial_proposals', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-cp_services-request_service_link_id', 'commercial_proposals_services', 'request_service_link_id', 'requests_services_links', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-cp_services-cp_id', 'commercial_proposals_services');
        $this->dropForeignKey('fk-cp_services-request_service_link_id', 'commercial_proposals_services');

        $this->dropTable('commercial_proposals_services');

        $this->dropTable('commercial_proposals');
        $this->dropTable('commercial_proposals_links');
    }
}
