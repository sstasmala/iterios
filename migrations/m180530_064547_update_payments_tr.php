<?php

use yii\db\Migration;

/**
 * Class m180530_064547_update_payments_tr
 */
class m180530_064547_update_payments_tr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_tourists_payments','note','text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_tourists_payments','note');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180530_064547_update_payments_tr cannot be reverted.\n";

        return false;
    }
    */
}
