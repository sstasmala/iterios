<?php

use yii\db\Migration;

/**
 * Class m180709_133241_update_users_view
 */
class m180709_133241_update_users_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DROP VIEW users_admin_view");
        $this->execute("CREATE OR REPLACE VIEW users_admin_view AS
          (SELECT users.*, CASE WHEN CAST(auth_assignment.user_id AS INTEGER) > 0 THEN 1 ELSE 0 END as system_admin
          FROM users LEFT JOIN auth_assignment
          ON auth_assignment.item_name = 'system_admin' AND users.id = CAST(auth_assignment.user_id AS INTEGER))");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
