<?php

use yii\db\Migration;

/**
 * Class m180605_110733_update_services_links
 */
class m180605_110733_update_services_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_services_links','due_date','bigint');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('orders_services_links','due_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_110733_update_services_links cannot be reverted.\n";

        return false;
    }
    */
}
