<?php

use yii\db\Migration;

/**
 * Class m180427_111451_drop_columns
 */
class m180427_111451_drop_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('request_notes', 'user_id');
        $this->dropColumn('companies_emails', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('companies_emails', 'type', $this->integer());
        $this->addColumn('request_notes', 'user_id', $this->integer());
    }
}
