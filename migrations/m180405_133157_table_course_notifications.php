<?php

use yii\db\Migration;

/**
 * Class m180405_133157_table_course_notifications
 */
class m180405_133157_table_course_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */

    /*  send_type:
        const SEND_NOW = 0; // сразу отправлять
        const SEND_TIME = 1; // в определенное время

        user_type = 0; // отправлять пользователю
        user_type = 1; // отправлять контакту

        // course_id - id записи в таблице изменений
        // noting_id - id уведомления
        // course_created_at - дата записи в таблицу тригеров
        // send_date - дата события, если ежегодное (вытащить время отправки из send_time)

        // uniq_id уникальный код действия
    */

    /*
        array(
            'send_type' => 1,
            'send_time' => $send_time,
            'send_date' => date('m.d', $send_time),
            'send_status' => 0,

            'user_ids' => $users_to_ids,
            'user_type' => $user_type,

            'course_id' => $notch['id'],
            'noting_id' => $notif['id'],

            'uniq_id' => $uniq_id,
            'course_created_at' => $notch['created_at']
        );
    */


    public function safeUp()
    {
        $this->createTable('{{%notifications_course}}', [
            'id' => $this->primaryKey(),
            'send_type' => $this->integer(),

            'user_id' => $this->integer(),
            'user_type' => $this->integer(),

            'date' => $this->string(),
            'time' => $this->string(),
            'day' => $this->string(),

            'timezone_date' => $this->string(),
            'timezone_time' => $this->string(),
            'timezone_day' => $this->string(),
            
            'send_status' => $this->integer()->defaultValue(0),

            'rule_id' => $this->string(),
            'noting_id' => $this->integer(),

            'created_at' => $this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notifications_course');
    }
}
