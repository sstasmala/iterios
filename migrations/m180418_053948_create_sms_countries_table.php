<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_countries`.
 */
class m180418_053948_create_sms_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms_countries', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'iso_code' => $this->string(5),
            'dial_code' => $this->string(10),
            'provider_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('sms_countries_provider_id', 'sms_countries', 'provider_id', 'sms_system_providers', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('sms_countries_provider_id', 'sms_countries');
        $this->dropTable('sms_countries');
    }
}
