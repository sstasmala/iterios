<?php

use yii\db\Migration;

/**
 * Class m180705_103253_add_error_to_prov_cred
 */
class m180705_103253_add_error_to_prov_cred extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('providers_credentials','provider_errors','text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('providers_credentials','provider_errors');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180705_103253_add_error_to_prov_cred cannot be reverted.\n";

        return false;
    }
    */
}
