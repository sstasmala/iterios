<?php

use yii\db\Migration;

/**
 * Class m180718_061543_update_contacts_passports_view
 */
class m180718_061543_update_contacts_passports_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DROP VIEW contacts_passports_view");
        $this->execute("CREATE OR REPLACE VIEW contacts_passports_view AS 
        (SELECT contacts_passports.id as passport_id, contacts_passports.contact_id as contact_id, contacts_passports.first_name,
         contacts_passports.last_name, contacts_passports.serial, contacts_passports.date_limit, contacts_passports.birth_date,
          contacts_passports.type_id, contacts_passports.issued_date, contacts_passports.issued_owner, contacts_passports.nationality, (SELECT ct.tenant_id FROM contacts as ct WHERE ct.id = contacts_passports.contact_id) as tenant_id FROM contacts_passports
        UNION SELECT null, contacts.id, contacts.first_name, contacts.last_name, null, null, contacts.date_of_birth, null, null, null, null,
         contacts.tenant_id FROM contacts WHERE contacts.id NOT IN (SELECT contacts_passports.contact_id FROM contacts_passports))");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
