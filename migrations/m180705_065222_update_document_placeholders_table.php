<?php

use yii\db\Migration;

/**
 * Class m180705_065222_update_document_placeholders_table
 */
class m180705_065222_update_document_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('document_placeholders', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('document_placeholders', 'description');
    }
}
