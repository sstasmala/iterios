<?php

use yii\db\Migration;

/**
 * Class m180508_143804_create_table_supplier_type
 */
class m180508_143804_create_table_supplier_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('supplier_type', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->bigInteger(),
            'updated_by'=>$this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('supplier_type');
    }

}
