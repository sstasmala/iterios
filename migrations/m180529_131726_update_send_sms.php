<?php

use yii\db\Migration;

/**
 * Class m180529_131726_update_send_sms
 */
class m180529_131726_update_send_sms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('send_sms_status', 'data', $this->text());
        $this->addColumn('send_sms', 'segment_copy', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('send_sms_status', 'data');
        $this->dropColumn('send_sms', 'segment_copy');
    }

}
