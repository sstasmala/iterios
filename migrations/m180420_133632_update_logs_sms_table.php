<?php

use yii\db\Migration;

/**
 * Class m180420_133632_update_logs_sms_table
 */
class m180420_133632_update_logs_sms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logs_sms', 'error_text', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logs_sms', 'error_text');
    }
}
