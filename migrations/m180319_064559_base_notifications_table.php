<?php

use yii\db\Migration;

/**
 * Class m180319_064559_base_notifications_table
 */
class m180319_064559_base_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE base_notifications_id_seq;');
        $this->createTable('base_notifications',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'base_notifications_id_seq\')',
                'name'=>'varchar(250) NOT NULL',
                'subject'=>'varchar(250)',
                'body'=>'text',
                'system_notification'=>'text',
                'sms'=>'text',
                'task_config'=>'text',
                'condition'=>'text',
                'schedule'=>'text',
                'module'=>'varchar(100) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE base_notifications_id_seq OWNED BY base_notifications.id;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('base_notifications');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_064559_base_notifications_table cannot be reverted.\n";

        return false;
    }
    */
}
