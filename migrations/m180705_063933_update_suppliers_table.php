<?php

use yii\db\Migration;

/**
 * Class m180705_063933_update_suppliers_table
 */
class m180705_063933_update_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('suppliers', 'description', $this->text());
        $this->addColumn('suppliers', 'web_site', $this->string());
        $this->addColumn('suppliers', 'company', $this->string());
        $this->addColumn('suppliers', 'functions', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('suppliers', 'description');
        $this->dropColumn('suppliers', 'web_site');
        $this->dropColumn('suppliers', 'company');
        $this->dropColumn('suppliers', 'functions');
    }
}
