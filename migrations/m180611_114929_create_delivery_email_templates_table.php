<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery_email_templates`.
 */
class m180611_114929_create_delivery_email_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery_email_templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'delivery_type_id' => $this->integer(),
            'status' => $this->boolean(),
            'subject' => $this->string(),
            'body' => $this->text(),
            'type' => $this->string(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('delivery_sms_templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'delivery_type_id' => $this->integer(),
            'status' => $this->boolean(),
            'body' => $this->text(),
            'type' => $this->string(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-delivery_email_templates-delivery_type_id', 'delivery_email_templates', 'delivery_type_id', 'delivery_types', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-delivery_sms_templates-delivery_type_id', 'delivery_sms_templates', 'delivery_type_id', 'delivery_types', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-delivery_email_templates-delivery_type_id', 'delivery_email_templates');
        $this->dropForeignKey('fk-delivery_sms_templates-delivery_type_id', 'delivery_sms_templates');

        $this->dropTable('delivery_email_templates');
        $this->dropTable('delivery_sms_templates');
    }
}
