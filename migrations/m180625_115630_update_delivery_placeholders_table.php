<?php

use yii\db\Migration;

/**
 * Class m180625_115630_update_delivery_placeholders_table
 */
class m180625_115630_update_delivery_placeholders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('delivery_placeholders', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_placeholders', 'description');
    }
}
