<?php

use yii\db\Migration;

/**
 * Class m180718_105521_update_payment_gateways
 */
class m180718_105521_update_payment_gateways extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_gateways','provider','varchar(250)');
        $this->addColumn('payment_gateways','provider_config','text');
        $this->addColumn('payment_gateways','document_template_id','bigint');
        $this->addForeignKey('payment_gateways_dct_fk','payment_gateways','document_template_id','documents_template','id','SET NULL','SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('payment_gateways_dct_fk','payment_gateways');
        $this->dropColumn('payment_gateways','provider');
        $this->dropColumn('payment_gateways','provider_config');
        $this->dropColumn('payment_gateways','document_template_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180718_105521_update_payment_gateways cannot be reverted.\n";

        return false;
    }
    */
}
