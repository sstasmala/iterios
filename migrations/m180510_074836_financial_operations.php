<?php

use yii\db\Migration;

/**
 * Class m180510_074836_financial_operations
 */
class m180510_074836_financial_operations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE financial_operations_id_seq;');
        $this->createTable('financial_operations',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'financial_operations_id_seq\')',
                'payment_method'=>'integer NOT NULL',
                'account_number'=>'varchar(255)',
                'transaction_number'=>'integer',
                'operation_type'=>'integer NOT NULL DEFAULT 0',
                'customer_id'=>'bigint',
                'payment_gateway_id'=>'bigint',
                'comment'=>'text',
                'sum'=>'decimal(11,2) NOT NULL DEFAULT 0.00',
                'currency'=>'integer',
                'sum_usd'=>'decimal(11,2) NOT NULL DEFAULT 0.00',
                'date'=>'bigint',
                'owner'=>'bigint NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE financial_operations_id_seq OWNED BY financial_operations.id;');
        $this->addForeignKey('financial_operations_tenants_fk','financial_operations','customer_id',
            'tenants','id','CASCADE','CASCADE');
        $this->addForeignKey('financial_operations_payment_gateways_fk','financial_operations','payment_gateway_id',
            'payment_gateways','id','SET NULL','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('financial_operations_tenants_fk','financial_operations');
        $this->dropForeignKey('financial_operations_payment_gateways_fk','financial_operations');
        $this->dropTable('financial_operations');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180510_074836_financial_operations cannot be reverted.\n";

        return false;
    }
    */
}
