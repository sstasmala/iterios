<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs_sms`.
 */
class m180418_101839_create_logs_sms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logs_sms', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'recipient' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
            'provider_id' => $this->integer(),
            'country_id' => $this->integer(),
            'alpha_name_type' => $this->string(),
            'alpha_name_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger()
        ]);

        $this->addForeignKey('logs_sms_provider_id', 'logs_sms', 'provider_id', 'sms_system_providers', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('logs_sms_country_id', 'logs_sms', 'country_id', 'sms_countries', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('logs_sms_alpha_name_id', 'logs_sms', 'alpha_name_id', 'sms_alpha_names', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('logs_sms_alpha_name_id', 'logs_sms');
        $this->dropForeignKey('logs_sms_country_id', 'logs_sms');
        $this->dropForeignKey('logs_sms_provider_id', 'logs_sms');

        $this->dropTable('logs_sms');
    }
}
