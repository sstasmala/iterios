<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180515_085956_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'contact_id' => $this->integer(),
            'request_id' => $this->integer(),
            'responsible_id' => $this->integer(),
            'tenant_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-orders-contact_id','orders','contact_id','contacts','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-orders-request_id','orders','request_id','requests','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-orders-responsible_id', 'orders', 'responsible_id', 'users', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-orders-tenant_id','orders','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-orders-tenant_id','orders');
        $this->dropForeignKey('fk-orders-responsible_id', 'orders');
        $this->dropForeignKey('fk-orders-request_id','orders');
        $this->dropForeignKey('fk-orders-contact_id','orders');
        $this->dropTable('orders');
    }
}
