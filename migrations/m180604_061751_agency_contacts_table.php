<?php

use yii\db\Migration;

/**
 * Class m180604_061751_agency_contacts_table
 */
class m180604_061751_agency_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        /**
         * Agencies
         */
        $this->execute('CREATE SEQUENCE agencies_id_sequence;');
        $this->createTable('agencies',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'agencies_id_sequence\')',
                'site'=>'varchar(255)',
                'city'=>'varchar(100)',
                'street'=>'varchar(100)',
                'house'=>'varchar(100)',
                'address_description'=>'text',
                'logo'=>'varchar(255)',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE agencies_id_sequence OWNED BY agencies.id;');
        $this->addForeignKey('agencies_tenant_fk','agencies','tenant_id','tenants','id');

        /**
         * Agencies phones
         */
        $this->execute('CREATE SEQUENCE agencies_phones_id_seq;');
        $this->createTable('agencies_phones',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'agencies_phones_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(30) NOT NULL',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE agencies_phones_id_seq OWNED BY agencies_phones.id;');
        $this->addForeignKey('agencies_phones_fk','agencies_phones','tenant_id','tenants','id');
        $this->addForeignKey('types_phones_fk','agencies_phones','type_id','phones_types','id');

        /**
         * Agencies socials
         */
        $this->execute('CREATE SEQUENCE agencies_socials_id_seq;');
        $this->createTable('agencies_socials',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'agencies_socials_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(255) NOT NULL',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE agencies_socials_id_seq OWNED BY agencies_socials.id;');
        $this->addForeignKey('agencies_socials_fk','agencies_socials','tenant_id','tenants','id');
        $this->addForeignKey('types_socials_fk','agencies_socials','type_id','socials_types','id');

        /**
         * Agencies emails
         */
        $this->execute('CREATE SEQUENCE agencies_emails_id_seq;');
        $this->createTable('agencies_emails',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'agencies_emails_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(129) NOT NULL',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE agencies_emails_id_seq OWNED BY agencies_emails.id;');
        $this->addForeignKey('agencies_emails_fk','agencies_emails','tenant_id','tenants','id');
        $this->addForeignKey('types_emails_fk','agencies_emails','type_id','emails_types','id');

        /**
         * Agencies messengers
         */
        $this->execute('CREATE SEQUENCE agencies_messengers_id_seq;');
        $this->createTable('agencies_messengers',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'agencies_messengers_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(255) NOT NULL',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE agencies_messengers_id_seq OWNED BY agencies_messengers.id;');
        $this->addForeignKey('agencies_messengers_fk','agencies_messengers','tenant_id','tenants','id');
        $this->addForeignKey('types_messengers_fk','agencies_messengers','type_id','messengers_types','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('agencies_tenant_fk','agencies');

        $this->dropForeignKey('agencies_phones_fk','agencies_phones');
        $this->dropForeignKey('types_phones_fk','agencies_phones');

        $this->dropForeignKey('agencies_socials_fk','agencies_socials');
        $this->dropForeignKey('types_socials_fk','agencies_socials');

        $this->dropForeignKey('agencies_emails_fk','agencies_emails');
        $this->dropForeignKey('types_emails_fk','agencies_emails');

        $this->dropForeignKey('agencies_messengers_fk','agencies_messengers');
        $this->dropForeignKey('types_messengers_fk','agencies_messengers');

        $this->dropTable('agencies');
        $this->dropTable('agencies_phones');
        $this->dropTable('agencies_socials');
        $this->dropTable('agencies_emails');
        $this->dropTable('agencies_messengers');
    }

}
