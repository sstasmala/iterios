<?php

use yii\db\Migration;

/**
 * Handles the creation of table `providers`.
 */
class m180329_071946_rename_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('providers');
        $this->createTable('suppliers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country_id' => 'integer',
            'logo' => $this->string(),
            'code' => $this->string(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('suppliers');
        $this->createTable('providers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country_id' => 'integer',
            'logo' => $this->string(),
            'code' => $this->string(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer'
        ]);
    }
}
