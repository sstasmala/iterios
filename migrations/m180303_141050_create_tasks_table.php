<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m180303_141050_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'due_date' => $this->bigInteger(),
            'email_reminder' => $this->bigInteger(),
            'status' => $this->boolean()->defaultValue(false),
            'type' => $this->string(),
            'assigned_to_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk_tasks_users', 'tasks', 'assigned_to_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_tasks_users', 'tasks');
        $this->dropTable('tasks');
    }
}
