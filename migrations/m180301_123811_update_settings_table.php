<?php

use yii\db\Migration;

/**
 * Class m180301_123811_update_settings_table
 */
class m180301_123811_update_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'group', $this->string());

        $this->update('settings', ['group' => 'email_providers']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'group');
    }
}
