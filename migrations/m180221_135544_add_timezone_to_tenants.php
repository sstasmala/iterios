<?php

use yii\db\Migration;

/**
 * Class m180221_135544_add_timezone_to_tenants
 */
class m180221_135544_add_timezone_to_tenants extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','timezone',$this->string(50));
        $this->addColumn('tenants','date_format',$this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('tenants','timezone');
        $this->dropColumn('tenants','date_format');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180221_135544_add_timezone_to_tenants cannot be reverted.\n";

        return false;
    }
    */
}
