<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs_email`.
 */
class m180228_084306_create_logs_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logs_email', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('logs_email');
    }
}
