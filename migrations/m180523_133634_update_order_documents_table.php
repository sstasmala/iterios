<?php

use yii\db\Migration;

/**
 * Class m180523_133634_update_order_documents_table
 */
class m180523_133634_update_order_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_documents', 'title', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_documents', 'title');
    }
}
