<?php

use yii\db\Migration;

/**
 * Class m180523_110514_update_orders_links
 */
class m180523_110514_update_orders_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_services_links','adults_count','integer NOT NULL DEFAULT 0');
        $this->addColumn('orders_services_links','children_count','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_services_links','adults_count');
        $this->dropColumn('orders_services_links','children_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_110514_update_orders_links cannot be reverted.\n";

        return false;
    }
    */
}
