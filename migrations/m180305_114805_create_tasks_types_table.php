<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks_types`.
 */
class m180305_114805_create_tasks_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tasks', 'type');
        $this->addColumn('tasks', 'type_id', $this->integer());

        $this->createTable('tasks_types', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer'
        ]);

        $this->addForeignKey('tasks_type_fk', 'tasks', 'type_id', 'tasks_types', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tasks_type_fk', 'tasks');

        $this->dropTable('tasks_types');

        $this->dropColumn('tasks', 'type_id');
        $this->addColumn('tasks', 'type', $this->string());
    }
}
