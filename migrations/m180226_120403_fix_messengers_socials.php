<?php

use yii\db\Migration;

/**
 * Class m180226_120403_fix_messengers_socials
 */
class m180226_120403_fix_messengers_socials extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('messengers_types','icon','varchar');
        $this->addColumn('socials_types','icon','varchar');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('messengers_types','icon');
        $this->dropColumn('socials_types','icon');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180226_120403_fix_messengers_socials cannot be reverted.\n";

        return false;
    }
    */
}
