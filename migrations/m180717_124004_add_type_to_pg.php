<?php

use yii\db\Migration;

/**
 * Class m180717_124004_add_type_to_pg
 */
class m180717_124004_add_type_to_pg extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_gateways','type','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payment_gateways','type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180717_124004_add_type_to_pg cannot be reverted.\n";

        return false;
    }
    */
}
