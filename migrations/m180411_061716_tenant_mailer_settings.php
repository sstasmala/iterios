<?php

use yii\db\Migration;

/**
 * Class m180411_061716_tenant_mailer_settings
 */
class m180411_061716_tenant_mailer_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','mailer_config','text');
        $this->addColumn('tenants','use_webmail','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('tenants','mailer_config');
        $this->dropColumn('tenants','use_webmail');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180411_061716_tenant_mailer_settings cannot be reverted.\n";

        return false;
    }
    */
}
