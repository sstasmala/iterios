<?php

use yii\db\Migration;

/**
 * Class m180622_131334_update_delivery_placeholders
 */
class m180622_131334_update_delivery_placeholders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('delivery_placeholders', 'fake', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_placeholders', 'fake');
    }
}
