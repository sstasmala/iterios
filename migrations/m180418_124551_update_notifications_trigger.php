<?php

use yii\db\Migration;

/**
 * Class m180418_124551_update_notifications_trigger
 */
class m180418_124551_update_notifications_trigger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_trigger', 'trigger_day', "varchar(255)  DEFAULT ''");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_trigger', 'trigger_day');
    }
}
