<?php

use yii\db\Migration;

/**
 * Class m180515_082712_update_notificationsnow_log_table
 */
class m180515_082712_update_notificationsnow_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notificationsnow_log','log_email_id', $this->integer());
        $this->addColumn('notificationsnow_log','log_notice_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notificationsnow_log','log_email_id');
        $this->dropColumn('notificationsnow_log','log_notice_id');
    }
}
