<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery_placeholder_types`.
 */
class m180601_120744_create_delivery_placeholder_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery_placeholder_types', [
            'id' => $this->primaryKey(),
            'delivery_type_id' => $this->integer(),
            'delivery_placeholder_id' => $this->integer()
        ]);

        $this->addForeignKey('fk-delivery_placeholder_types-delivery_type_id', 'delivery_placeholder_types', 'delivery_type_id', 'delivery_types', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-delivery_placeholder_types-delivery_placeholder_id', 'delivery_placeholder_types', 'delivery_placeholder_id', 'delivery_placeholders', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-delivery_placeholder_types-delivery_placeholder_id', 'delivery_placeholder_types');
        $this->dropForeignKey('fk-delivery_placeholder_types-delivery_type_id', 'delivery_placeholder_types');
        $this->dropTable('delivery_placeholder_types');
    }
}
