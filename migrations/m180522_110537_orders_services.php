<?php

use yii\db\Migration;

/**
 * Class m180522_110537_orders_services
 */
class m180522_110537_orders_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Main services connection to orders
        $this->execute('CREATE SEQUENCE orders_services_links_id_seq;');
        $this->createTable('orders_services_links',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_services_links_id_seq\')',
                'order_id'=>'bigint NOT NULL',
                'service_id'=>'bigint NOT NULL',
                'tenant_id'=>'bigint NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE orders_services_links_id_seq OWNED BY orders_services_links.id;');
        $this->addForeignKey('orders_services_links_rq_fk','orders_services_links','order_id',
            'orders','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_links_sr_fk','orders_services_links',
            'service_id','services','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_links_tn_fk','orders_services_links',
            'tenant_id','tenants','id','CASCADE','CASCADE');

        //Additional services connection to request
        $this->execute('CREATE SEQUENCE orders_services_additional_links_id_seq;');
        $this->createTable('orders_services_additional_links',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_services_additional_links_id_seq\')',
                'link_id'=>'bigint NOT NULL',
                'service_id'=>'bigint NOT NULL',
                'type' => 'integer NOT NULL DEFAULT 0'
            ]
        );
        $this->execute('ALTER SEQUENCE orders_services_additional_links_id_seq OWNED BY orders_services_additional_links.id;');
        $this->addForeignKey('orders_services_additional_links_rqsl_fk','orders_services_additional_links',
            'link_id','orders_services_links','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_additional_links_sr_fk','orders_services_additional_links',
            'service_id','services','id','CASCADE','CASCADE');

        //Values
        $this->execute('CREATE SEQUENCE orders_services_values_id_seq;');
        $this->createTable('orders_services_values',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_services_values_id_seq\')',
                'link_id'=>'bigint NOT NULL',
                'additional_link_id'=>'bigint',
                'service_id'=>'bigint NOT NULL',
                'field_id'=>'bigint NOT NULL',
                'value'=>'text',
            ]
        );
        $this->execute('ALTER SEQUENCE orders_services_values_id_seq OWNED BY orders_services_values.id;');
        $this->addForeignKey('orders_services_values_lk_fk','orders_services_values',
            'link_id','orders_services_links','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_values_alk_fk','orders_services_values',
            'additional_link_id','orders_services_additional_links','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_values_sr_fk','orders_services_values',
            'service_id','services','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_services_values_fd_fk','orders_services_values',
            'field_id','services_fields_default','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_services_values_fd_fk','orders_services_values');
        $this->dropForeignKey('orders_services_values_sr_fk','orders_services_values');
        $this->dropForeignKey('orders_services_values_alk_fk','orders_services_values');
        $this->dropForeignKey('orders_services_values_lk_fk','orders_services_values');
        $this->dropForeignKey('orders_services_links_sr_fk','orders_services_links');
        $this->dropForeignKey('orders_services_additional_links_sr_fk','orders_services_additional_links');
        $this->dropForeignKey('orders_services_additional_links_rqsl_fk','orders_services_additional_links');
        $this->dropForeignKey('orders_services_links_rq_fk','orders_services_links');
        $this->dropForeignKey('orders_services_links_tn_fk','orders_services_links');

        $this->dropTable('orders_services_values');
        $this->dropTable('orders_services_additional_links');
        $this->dropTable('orders_services_links');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_110537_orders_services cannot be reverted.\n";

        return false;
    }
    */
}
