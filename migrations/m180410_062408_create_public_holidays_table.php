<?php

use yii\db\Migration;

/**
 * Handles the creation of table `public_holidays`.
 */
class m180410_062408_create_public_holidays_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('public_holidays', [
            'id' => $this->primaryKey(),
            'day' => $this->integer(),
            'mounts' => $this->integer(),
            'name'  => $this->string(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('public_holidays');
    }
}
