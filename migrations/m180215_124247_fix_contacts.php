<?php

use yii\db\Migration;

/**
 * Class m180215_124247_fix_contacts
 */
class m180215_124247_fix_contacts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contacts_emails','type');
        $this->dropForeignKey('contacts_emails_fk','contacts_emails');
        $this->addForeignKey('contacts_emails_fk','contacts_emails','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_phones_fk','contacts_phones');
        $this->addForeignKey('contacts_phones_fk','contacts_phones','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_socials_fk','contacts_socials');
        $this->addForeignKey('contacts_socials_fk','contacts_socials','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_messengers_fk','contacts_messengers');
        $this->addForeignKey('contacts_messengers_fk','contacts_messengers','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_sites_fk','contacts_sites');
        $this->addForeignKey('contacts_sites_fk','contacts_sites','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_tags_fk','contacts_tags');
        $this->addForeignKey('contacts_tags_fk','contacts_tags','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_passports_fk','contacts_passports');
        $this->addForeignKey('contacts_passports_fk','contacts_passports','contact_id','contacts','id','CASCADE');

        $this->dropForeignKey('contacts_visas_fk','contacts_visas');
        $this->addForeignKey('contacts_visas_fk','contacts_visas','contact_id','contacts','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('contacts_emails','type','integer');
        $this->dropForeignKey('contacts_emails_fk','contacts_emails');
        $this->addForeignKey('contacts_emails_fk','contacts_emails','contact_id','contacts','id');

        $this->dropForeignKey('contacts_phones_fk','contacts_phones');
        $this->addForeignKey('contacts_phones_fk','contacts_phones','contact_id','contacts','id');

        $this->dropForeignKey('contacts_socials_fk','contacts_socials');
        $this->addForeignKey('contacts_socials_fk','contacts_socials','contact_id','contacts','id');

        $this->dropForeignKey('contacts_messengers_fk','contacts_messengers');
        $this->addForeignKey('contacts_messengers_fk','contacts_messengers','contact_id','contacts','id');

        $this->dropForeignKey('contacts_sites_fk','contacts_sites');
        $this->addForeignKey('contacts_sites_fk','contacts_sites','contact_id','contacts','id');

        $this->dropForeignKey('contacts_tags_fk','contacts_tags');
        $this->addForeignKey('contacts_tags_fk','contacts_tags','contact_id','contacts','id');

        $this->dropForeignKey('contacts_passports_fk','contacts_passports');
        $this->addForeignKey('contacts_passports_fk','contacts_passports','contact_id','contacts','id');

        $this->dropForeignKey('contacts_visas_fk','contacts_visas');
        $this->addForeignKey('contacts_visas_fk','contacts_visas','contact_id','contacts','id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180215_124247_fix_contacts cannot be reverted.\n";

        return false;
    }
    */
}
