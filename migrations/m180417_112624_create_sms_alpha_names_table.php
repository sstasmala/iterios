<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_alpha_names`.
 */
class m180417_112624_create_sms_alpha_names_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms_alpha_names', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'provider_id' => $this->integer(),
            'tenant_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('sms_alpha_names_provider_id', 'sms_alpha_names', 'provider_id', 'sms_system_providers', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('sms_alpha_names_tenant_id', 'sms_alpha_names', 'tenant_id', 'tenants', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('sms_alpha_names_provider_id', 'sms_alpha_names');
        $this->dropForeignKey('sms_alpha_names_tenant_id', 'sms_alpha_names');

        $this->dropTable('sms_alpha_names');
    }
}
