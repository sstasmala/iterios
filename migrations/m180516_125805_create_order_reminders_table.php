<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_reminders`.
 */
class m180516_125805_create_order_reminders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_reminders', [
            'id' => $this->primaryKey(),
            'reminder_id' => $this->integer(),
            'order_id' => $this->integer(),
            'status' => $this->integer(),
            'due_date' => $this->bigInteger(),
            'task_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-order_reminders-reminder_id','order_reminders','reminder_id','reminders','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-order_reminders-order_id','order_reminders','order_id','orders','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-order_reminders-task_id','order_reminders','task_id','tasks','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_reminders-task_id','order_reminders');
        $this->dropForeignKey('fk-order_reminders-order_id','order_reminders');
        $this->dropForeignKey('fk-order_reminders-reminder_id','order_reminders');

        $this->dropTable('order_reminders');
    }
}
