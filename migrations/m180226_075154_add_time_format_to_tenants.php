<?php

use yii\db\Migration;

/**
 * Class m180226_075154_add_time_format_to_tenants
 */
class m180226_075154_add_time_format_to_tenants extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','time_format','varchar(50)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants','time_format');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180226_075154_add_time_format_to_tenants cannot be reverted.\n";

        return false;
    }
    */
}
