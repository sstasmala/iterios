<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reminders`.
 */
class m180516_125795_create_reminders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reminders', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'name' => $this->string(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('reminders_tenant', [
            'id' => $this->primaryKey(),
            'reminder_id' => $this->integer()->unique(),
            'tenant_id' => $this->integer(),
            'active' => $this->boolean()->defaultValue(false),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('reminders_tenant_system', [
            'id' => $this->primaryKey(),
            'reminder_id' => $this->integer(),
            'tenant_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'created_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-reminders_tenant-reminder_id','reminders_tenant','reminder_id','reminders','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-reminders_tenant-tenant_id','reminders_tenant','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-reminders_tenant_system-reminder_id','reminders_tenant_system','reminder_id','reminders','id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-reminders_tenant_system-tenant_id','reminders_tenant_system','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-reminders_tenant_system-tenant_id','reminders_tenant_system');
        $this->dropForeignKey('fk-reminders_tenant_system-reminder_id','reminders_tenant_system');
        $this->dropForeignKey('fk-reminders_tenant-tenant_id','reminders_tenant');
        $this->dropForeignKey('fk-reminders_tenant-reminder_id','reminders_tenant');

        $this->dropTable('reminders_tenant');
        $this->dropTable('reminders');
    }
}
