<?php

use yii\db\Migration;

/**
 * Class m180425_063231_create_notifications_log
 */
class m180425_063231_create_notifications_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notifications_log', [
            'id' => $this->primaryKey(),
            'trigger_id' => $this->integer(),
            'reminder_id' => $this->integer(),
            'contact_id' => $this->integer(),
            'course_id' => $this->integer(),
            'agent_id' => $this->integer(),
            'channel' => $this->string(),
            'status' => $this->integer(),
            'errors' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notifications_log');
    }
}
