<?php

use yii\db\Migration;

/**
 * Handles the creation of table `agents_mapping`.
 */
class m180209_151612_create_agents_mapping_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('agents_mapping', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'tenant_id' => $this->integer()
        ]);

        $this->addForeignKey('fk_agents_mapping_users', 'agents_mapping', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_agents_mapping_tenants', 'agents_mapping', 'tenant_id', 'tenants', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_agents_mapping_users', 'agents_mapping');
        $this->dropForeignKey('fk_agents_mapping_tenants', 'agents_mapping');

        $this->dropTable('agents_mapping');
    }
}
