<?php

use yii\db\Migration;

/**
 * Handles the creation of table `provider_function`.
 */
class m180510_110925_create_provider_function_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('providers','functions', $this->integer());

        $this->createTable('provider_function', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer'
        ]);

        $this->addForeignKey('provider_function_fk', 'providers', 'functions', 'provider_function', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('provider_function_fk', 'providers');
        $this->dropColumn('providers', 'functions');
        $this->dropTable('provider_function');
    }
}
