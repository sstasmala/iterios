<?php

use yii\db\Migration;

/**
 * Class m180402_114817_update_request_table
 */
class m180402_114817_update_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('requests', 'city');
        $this->dropColumn('requests', 'country');

        $this->createTable('request_cities', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer(),
            'city' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('request_cities_requests_fk','request_cities','request_id','requests','id', 'CASCADE', 'CASCADE');

        $this->createTable('request_countries', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer(),
            'country' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('request_countries_requests_fk','request_countries','request_id','requests','id', 'CASCADE', 'CASCADE');

        $this->dropColumn('requests', 'departure_date');
        $this->addColumn('requests', 'departure_date_start', $this->integer());
        $this->addColumn('requests', 'departure_date_end', $this->integer());

        $this->addColumn('requests', 'tenant_id', $this->integer());
        $this->addForeignKey('requests_tenant_fk','requests','tenant_id','tenants','id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('requests_tenant_fk','requests');
        $this->dropColumn('requests', 'tenant_id');

        $this->dropColumn('requests', 'departure_date_end');
        $this->dropColumn('requests', 'departure_date_start');
        $this->addColumn('requests', 'departure_date', $this->integer());

        $this->dropForeignKey('request_countries_requests_fk','request_countries');
        $this->dropTable('request_countries');

        $this->dropForeignKey('request_cities_requests_fk','request_cities');
        $this->dropTable('request_cities');

        $this->addColumn('requests', 'country', $this->integer());
        $this->addColumn('requests', 'city', $this->integer());
    }
}
