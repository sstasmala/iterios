<?php

use yii\db\Migration;

/**
 * Class m180306_151711_admin_role
 */
class m180306_151711_admin_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\helpers\RbacHelper::createRole('system_admin');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\helpers\RbacHelper::deleteRole('system_admin');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180306_151711_admin_role cannot be reverted.\n";

        return false;
    }
    */
}
