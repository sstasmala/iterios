<?php

use yii\db\Migration;

/**
 * Class m180313_081330_update_tenants_settings
 */
class m180313_081330_update_tenants_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','country','varchar(10)');
        $this->addColumn('tenants','reminders_email','varchar(129)');
        $this->addColumn('tenants','reminders_from_who','varchar');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants','country');
        $this->dropColumn('tenants','reminders_email');
        $this->dropColumn('tenants','reminders_from_who');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180313_081330_update_tenants_settings cannot be reverted.\n";

        return false;
    }
    */
}
