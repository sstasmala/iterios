<?php

use yii\db\Migration;

/**
 * Class m180709_110626_update_emails_dispatch
 */
class m180709_110626_update_emails_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('emails_dispatch','provider_data','text');
        $this->addColumn('emails_dispatch','statistic_data','text');
        $this->addColumn('emails_dispatch','stat_update','bigint NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('emails_dispatch','provider_data');
        $this->dropColumn('emails_dispatch','statistic_data');
        $this->dropColumn('emails_dispatch','stat_update');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180709_110626_update_emails_dispatch cannot be reverted.\n";

        return false;
    }
    */
}
