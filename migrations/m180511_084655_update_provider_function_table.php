<?php

use yii\db\Migration;

/**
 * Class m180511_084655_update_provider_function_table
 */
class m180511_084655_update_provider_function_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('provider_function_fk', 'providers');
        $this->dropColumn('providers','functions');
        $this->addColumn('providers','functions', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('providers','functions');
        $this->addColumn('providers','functions', $this->integer());
        $this->addForeignKey('provider_function_fk', 'providers', 'functions', 'provider_function', 'id', 'SET NULL', 'CASCADE');
    }
}
