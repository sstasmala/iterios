<?php

use yii\db\Migration;

/**
 * Class m180511_130123_update_provider_table
 */
class m180511_130123_update_provider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('providers','additional_config', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('providers','additional_config');
    }
}
