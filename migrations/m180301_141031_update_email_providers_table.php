<?php

use yii\db\Migration;

/**
 * Class m180301_141031_update_email_providers_table
 */
class m180301_141031_update_email_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('email_providers', 'type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('email_providers', 'type');
    }
}
