<?php

use yii\db\Migration;

/**
 * Class m180423_122654_notifications_trigger_update
 */
class m180423_122654_notifications_trigger_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_trigger', 'type', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_trigger', 'type');
    }
}