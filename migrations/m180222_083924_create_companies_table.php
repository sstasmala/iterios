<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies`.
 */
class m180222_083924_create_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_types', [
            'id' => $this->primaryKey(),
            'value' => $this->string(255),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('companies_kind_activities', [
            'id' => $this->primaryKey(),
            'value' => $this->string(255),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('companies_contact_sources', [
            'id' => $this->primaryKey(),
            'value' => $this->string(255),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('companies_statuses', [
            'id' => $this->primaryKey(),
            'value' => $this->string(255),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->createTable('companies_addresses', [
            'id' => $this->primaryKey(),
            'country' => $this->string(100),
            'region' => $this->string(100),
            'city' => $this->string(100),
            'street' => $this->string(100),
            'house' => $this->string(100),
            'flat' => $this->string(100),
            'postcode' => $this->string(100),
            'tenant_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('companies_addresses_tenant_fk','companies_addresses','tenant_id','tenants','id');

        $this->addColumn('companies', 'legal_name', $this->string());
        $this->addColumn('companies', 'responsible_id', $this->integer());
        $this->addColumn('companies', 'company_address_id', $this->integer());

        $this->addColumn('companies', 'company_type_id', $this->integer());
        $this->addColumn('companies', 'company_kind_activity_id', $this->integer());
        $this->addColumn('companies', 'company_contact_source_id', $this->integer());
        $this->addColumn('companies', 'company_status_id', $this->integer());

        $this->addForeignKey('companies_responsible_fk', 'companies', 'responsible_id', 'users', 'id', 'SET NULL');
        $this->addForeignKey('companies_addresses_fk','companies','company_address_id','companies_addresses','id','SET NULL');

        $this->addForeignKey('companies_types_fk', 'companies', 'company_type_id', 'companies_types', 'id', 'SET NULL');
        $this->addForeignKey('companies_kind_activities_fk', 'companies', 'company_kind_activity_id', 'companies_kind_activities', 'id', 'SET NULL');
        $this->addForeignKey('companies_contact_sources_fk', 'companies', 'company_contact_source_id', 'companies_contact_sources', 'id', 'SET NULL');
        $this->addForeignKey('companies_statuses_fk', 'companies', 'company_status_id', 'companies_statuses', 'id', 'SET NULL');

        $this->createTable('companies_employees', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('employees_user_fk', 'companies_employees', 'user_id', 'users', 'id');
        $this->addForeignKey('employees_company_fk', 'companies_employees', 'company_id', 'companies', 'id');

        $this->createTable('companies_emails', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'type_id' => $this->integer(),
            'value' => $this->string(129),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('types_emails_fk','companies_emails','type_id','emails_types','id');
        $this->addForeignKey('company_emails_fk', 'companies_emails', 'company_id', 'companies', 'id');

        $this->createTable('companies_phones', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'value' => $this->string(30),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('types_phones_fk','companies_phones','type_id','phones_types','id');
        $this->addForeignKey('company_phones_fk', 'companies_phones', 'company_id', 'companies', 'id');

        $this->createTable('companies_socials', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'value' => $this->string(255),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('types_socials_fk','companies_socials','type_id','socials_types','id');
        $this->addForeignKey('company_socials_fk', 'companies_socials', 'company_id', 'companies', 'id');

        $this->createTable('companies_messengers', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'value' => $this->string(255),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('types_messengers_fk','companies_messengers','type_id','messengers_types','id');
        $this->addForeignKey('company_messengers_fk', 'companies_messengers', 'company_id', 'companies', 'id');

        $this->createTable('companies_sites', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'value' => $this->string(255),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('types_sites_fk','companies_sites','type_id','sites_types','id');
        $this->addForeignKey('company_sites_fk', 'companies_sites', 'company_id', 'companies', 'id');

        $this->createTable('companies_tags', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer(),
            'company_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('tags_contacts_fk','companies_tags','tag_id','tags','id');
        $this->addForeignKey('company_tags_fk', 'companies_tags', 'company_id', 'companies', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('company_tags_fk', 'companies_tags');
        $this->dropForeignKey('tags_contacts_fk','companies_tags');

        $this->dropTable('companies_tags');

        $this->dropForeignKey('company_sites_fk', 'companies_sites');
        $this->dropForeignKey('types_sites_fk','companies_sites');

        $this->dropTable('companies_sites');

        $this->dropForeignKey('company_messengers_fk', 'companies_messengers');
        $this->dropForeignKey('types_messengers_fk','companies_messengers');

        $this->dropTable('companies_messengers');

        $this->dropForeignKey('company_socials_fk', 'companies_socials');
        $this->dropForeignKey('types_socials_fk','companies_socials');

        $this->dropTable('companies_socials');

        $this->dropForeignKey('company_phones_fk', 'companies_phones');
        $this->dropForeignKey('types_phones_fk','companies_phones');

        $this->dropTable('companies_phones');

        $this->dropForeignKey('company_emails_fk', 'companies_emails');
        $this->dropForeignKey('types_emails_fk','companies_emails');

        $this->dropTable('companies_emails');

        $this->dropForeignKey('employees_company_fk', 'companies_employees');
        $this->dropForeignKey('employees_user_fk', 'companies_employees');

        $this->dropTable('companies_employees');

        $this->dropForeignKey('companies_statuses_fk', 'companies');
        $this->dropForeignKey('companies_contact_sources_fk', 'companies');
        $this->dropForeignKey('companies_kind_activities_fk', 'companies');
        $this->dropForeignKey('companies_types_fk', 'companies');
        $this->dropForeignKey('companies_addresses_fk', 'companies');
        $this->dropForeignKey('companies_responsible_fk', 'companies');

        $this->dropColumn('companies', 'company_status_id');
        $this->dropColumn('companies', 'company_contact_source_id');
        $this->dropColumn('companies', 'company_kind_activity_id');
        $this->dropColumn('companies', 'company_type_id');

        $this->dropColumn('companies', 'company_address_id');
        $this->dropColumn('companies', 'responsible_id');
        $this->dropColumn('companies', 'legal_name');

        $this->dropForeignKey('companies_addresses_tenant_fk','companies_addresses');

        $this->dropTable('companies_addresses');

        $this->dropTable('companies_statuses');
        $this->dropTable('companies_contact_sources');
        $this->dropTable('companies_kind_activities');
        $this->dropTable('companies_types');
    }
}
