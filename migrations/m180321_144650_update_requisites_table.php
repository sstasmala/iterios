<?php

use yii\db\Migration;

/**
 * Class m180321_144650_update_requisites_table
 */
class m180321_144650_update_requisites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_requisites_fields_requisites_field', 'requisites');
        $this->dropColumn('requisites','requisites_field_id');

        $this->addColumn('requisites','requisites_groups_id', 'integer');
        $this->addForeignKey('fk_requisites_requisites_groups', 'requisites', 'requisites_groups_id', 'requisites_groups', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_requisites_requisites_groups', 'requisites');
        $this->dropColumn('requisites','requisites_groups_id');

        $this->addColumn('requisites','requisites_field_id', 'integer');
        $this->addForeignKey('fk_requisites_fields_requisites_field', 'requisites', 'requisites_field_id', 'requisites_fields', 'id', 'RESTRICT', 'CASCADE');
    }
}
