<?php

use yii\db\Migration;

/**
 * Class m180523_074352_orders_tourists
 */
class m180523_074352_orders_tourists extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE orders_tourists_id_seq;');
        $this->createTable('orders_tourists',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'orders_tourists_id_seq\')',
                'order_id'=>'bigint NOT NULL',
                'contact_id'=>'bigint NOT NULL',
                'link_id'=>'bigint NOT NULL',
                'type'=>'integer NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE orders_tourists_id_seq OWNED BY orders_tourists.id;');
        $this->addForeignKey('orders_tourists_or_fk','orders_tourists','order_id',
            'orders','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_tourists_cn_fk','orders_tourists','contact_id',
            'contacts','id','CASCADE','CASCADE');
        $this->addForeignKey('orders_tourists_lk_fk','orders_tourists','link_id',
            'contacts','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('orders_tourists_or_fk','orders_tourists');
        $this->dropForeignKey('orders_tourists_cn_fk','orders_tourists');
        $this->dropForeignKey('orders_tourists_lk_fk','orders_tourists');
        $this->dropTable('orders_tourists');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_074352_orders_tourists cannot be reverted.\n";

        return false;
    }
    */
}
