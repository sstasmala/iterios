<?php

use yii\db\Migration;

/**
 * Class m180214_071541_contacts_table
 */
class m180214_071541_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * Companies
         */
        $this->execute('CREATE SEQUENCE companies_id_seq;');
        $this->createTable('companies',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'companies_id_seq\')',
                'name'=>'varchar(250) NOT NULL',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE companies_id_seq OWNED BY companies.id;');
        $this->addForeignKey('companies_tenant_fk','companies','tenant_id','tenants','id');


        /**
         * Addresses
         */
        $this->execute('CREATE SEQUENCE contacts_addresses_id_seq;');
        $this->createTable('contacts_addresses',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_addresses_id_seq\')',
                'country'=>'varchar(100)',
                'region'=>'varchar(100)',
                'city'=>'varchar(100)',
                'street'=>'varchar(100)',
                'house'=>'varchar(100)',
                'flat'=>'varchar(100)',
                'postcode'=>'varchar(100)',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_addresses_id_seq OWNED BY contacts_addresses.id;');
        $this->addForeignKey('contacts_addresses_tenant_fk','contacts_addresses','tenant_id','tenants','id');

        /**
         * Contacts
         */
        $this->execute('CREATE TYPE contact_type AS ENUM (\'customer\', \'tourist\', \'lid\',\'new\');');
        $this->execute('CREATE SEQUENCE contacts_id_seq;');
        $this->createTable('contacts',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_id_seq\')',
                'first_name'=>'varchar NOT NULL',
                'middle_name'=>'varchar',
                'last_name'=>'varchar',
                'date_of_birth'=>'varchar(100)',
                'sex'=>'integer',
                'discount'=>'integer',
                'nationality'=>'varchar(100)',//multicodes
                'tin'=>'varchar(50)',
                'type'=>'contact_type',
                'company_id'=>'bigint',
                'residence_address_id'=>'bigint',
                'living_address_id'=>'bigint',
                'responsible'=>'integer',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;');
        $this->addForeignKey('contacts_tenant_fk','contacts','tenant_id','tenants','id');
        $this->addForeignKey('contact_companies_fk','contacts','company_id','companies','id');
        $this->addForeignKey('contact_addresses_residence_fk','contacts','residence_address_id','contacts_addresses','id','SET NULL');
        $this->addForeignKey('contact_addresses_living_fk','contacts','living_address_id','contacts_addresses','id','SET NULL');


        /**
         * Emails types
         */
        $this->execute('CREATE SEQUENCE emails_types_id_seq;');
        $this->createTable('emails_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'emails_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE emails_types_id_seq OWNED BY emails_types.id;');

        /**
         * Phones types
         */
        $this->execute('CREATE SEQUENCE phones_types_id_seq;');
        $this->createTable('phones_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'phones_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE phones_types_id_seq OWNED BY phones_types.id;');

        /**
         * Socials types
         */
        $this->execute('CREATE SEQUENCE socials_types_id_seq;');
        $this->createTable('socials_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'socials_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE socials_types_id_seq OWNED BY socials_types.id;');

        /**
         * Messengers types
         */
        $this->execute('CREATE SEQUENCE messengers_types_id_seq;');
        $this->createTable('messengers_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'messengers_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE messengers_types_id_seq OWNED BY messengers_types.id;');

        /**
         * Sites types
         */
        $this->execute('CREATE SEQUENCE sites_types_id_seq;');
        $this->createTable('sites_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'sites_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE sites_types_id_seq OWNED BY sites_types.id;');

        /**
         * Contacts emails
         */
        $this->execute('CREATE SEQUENCE contacts_emails_id_seq;');
        $this->createTable('contacts_emails',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_emails_id_seq\')',
                'type'=>'integer',
                'type_id'=>'integer',
                'value'=>'varchar(129) NOT NULL',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_emails_id_seq OWNED BY contacts_emails.id;');
        $this->addForeignKey('contacts_emails_fk','contacts_emails','contact_id','contacts','id');
        $this->addForeignKey('types_emails_fk','contacts_emails','type_id','emails_types','id');

        /**
         * Contacts phones
         */
        $this->execute('CREATE SEQUENCE contacts_phones_id_seq;');
        $this->createTable('contacts_phones',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_phones_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(30) NOT NULL',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_phones_id_seq OWNED BY contacts_phones.id;');
        $this->addForeignKey('contacts_phones_fk','contacts_phones','contact_id','contacts','id');
        $this->addForeignKey('types_phones_fk','contacts_phones','type_id','phones_types','id');

        /**
         * Contacts socials
         */
        $this->execute('CREATE SEQUENCE contacts_socials_id_seq;');
        $this->createTable('contacts_socials',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_socials_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(255) NOT NULL',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_socials_id_seq OWNED BY contacts_socials.id;');
        $this->addForeignKey('contacts_socials_fk','contacts_socials','contact_id','contacts','id');
        $this->addForeignKey('types_socials_fk','contacts_socials','type_id','socials_types','id');

        /**
         * Contacts messengers
         */
        $this->execute('CREATE SEQUENCE contacts_messengers_id_seq;');
        $this->createTable('contacts_messengers',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_messengers_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(255) NOT NULL',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_messengers_id_seq OWNED BY contacts_messengers.id;');
        $this->addForeignKey('contacts_messengers_fk','contacts_messengers','contact_id','contacts','id');
        $this->addForeignKey('types_messengers_fk','contacts_messengers','type_id','messengers_types','id');

        /**
         * Contacts sites
         */
        $this->execute('CREATE SEQUENCE contacts_sites_id_seq;');
        $this->createTable('contacts_sites',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_sites_id_seq\')',
                'type_id'=>'integer',
                'value'=>'varchar(255) NOT NULL',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_sites_id_seq OWNED BY contacts_sites.id;');
        $this->addForeignKey('contacts_sites_fk','contacts_sites','contact_id','contacts','id');
        $this->addForeignKey('types_sites_fk','contacts_sites','type_id','sites_types','id');

        /**
         * Tags
         */
        $this->execute('CREATE TYPE tag_type AS ENUM(\'system\',\'custom\');');
        $this->execute('CREATE SEQUENCE tags_id_seq;');
        $this->createTable('tags',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'tags_id_seq\')',
                'value'=>'varchar(255) NOT NULL',
                'type'=>'tag_type',
                'tenant_id'=>'integer',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE tags_id_seq OWNED BY tags.id;');
        $this->addForeignKey('tags_tenant_fk','tags','tenant_id','tenants','id');

        /**
         * Contacts tags
         */
        $this->execute('CREATE SEQUENCE contacts_tags_id_seq;');
        $this->createTable('contacts_tags',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_tags_id_seq\')',
                'tag_id'=>'integer',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_tags_id_seq OWNED BY contacts_tags.id;');
        $this->addForeignKey('contacts_tags_fk','contacts_tags','contact_id','contacts','id');
        $this->addForeignKey('tags_contacts_fk','contacts_tags','tag_id','tags','id');

        /**
         * Passports types
         */
        $this->execute('CREATE SEQUENCE passports_types_id_seq;');
        $this->createTable('passports_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'passports_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE passports_types_id_seq OWNED BY passports_types.id;');

        /**
         * Contacts passports
         */
        $this->execute('CREATE SEQUENCE contacts_passports_id_seq;');
        $this->createTable('contacts_passports',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_passports_id_seq\')',
                'type_id'=>'integer',
                'first_name'=>'varchar(255) NOT NULL',
                'last_name'=>'varchar',
                'serial'=>'varchar',
                'country'=>'varchar(255)',//multicodes
                'nationality'=>'varchar(255)',
                'birth_date'=>'varchar(50)',
                'date_limit' => 'varchar(50)',
                'issued_date' => 'varchar(50)',
                'issued_owner' => 'varchar(255)',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_passports_id_seq OWNED BY contacts_passports.id;');
        $this->addForeignKey('contacts_passports_fk','contacts_passports','contact_id','contacts','id');
        $this->addForeignKey('types_passports_fk','contacts_passports','type_id','passports_types','id');

        /**
         * Visas types
         */
        $this->execute('CREATE SEQUENCE visas_types_id_seq;');
        $this->createTable('visas_types',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'visas_types_id_seq\')',
                'value'=>'varchar(200) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE visas_types_id_seq OWNED BY visas_types.id;');

        /**
         * Visas passports
         */
        $this->execute('CREATE SEQUENCE contacts_visas_id_seq;');
        $this->createTable('contacts_visas',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'contacts_passports_id_seq\')',
                'type_id'=>'integer',
                'first_name'=>'varchar(255) NOT NULL',
                'last_name'=>'varchar',
                'serial'=>'varchar',
                'country'=>'varchar(255)',//multicodes
                'nationality'=>'varchar(255)',
                'birth_date'=>'varchar(50)',
                'date_limit' => 'varchar(50)',
                'issued_date' => 'varchar(50)',
                'issued_owner' => 'varchar(255)',
                'contact_id'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE contacts_visas_id_seq OWNED BY contacts_visas.id;');
        $this->addForeignKey('contacts_visas_fk','contacts_visas','contact_id','contacts','id');
        $this->addForeignKey('types_visas_fk','contacts_visas','type_id','visas_types','id');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('types_visas_fk', 'contacts_visas');
        $this->dropForeignKey('contacts_visas_fk', 'contacts_visas');

        $this->dropForeignKey('contacts_passports_fk', 'contacts_passports');
        $this->dropForeignKey('types_passports_fk', 'contacts_passports');

        $this->dropForeignKey('contacts_tags_fk', 'contacts_tags');
        $this->dropForeignKey('tags_contacts_fk', 'contacts_tags');

        $this->dropForeignKey('contacts_sites_fk', 'contacts_sites');
        $this->dropForeignKey('types_sites_fk', 'contacts_sites');

        $this->dropForeignKey('contacts_messengers_fk', 'contacts_messengers');
        $this->dropForeignKey('types_messengers_fk', 'contacts_messengers');

        $this->dropForeignKey('contacts_socials_fk', 'contacts_socials');
        $this->dropForeignKey('types_socials_fk', 'contacts_socials');

        $this->dropForeignKey('contacts_phones_fk', 'contacts_phones');
        $this->dropForeignKey('types_phones_fk', 'contacts_phones');

        $this->dropForeignKey('contacts_emails_fk', 'contacts_emails');
        $this->dropForeignKey('types_emails_fk', 'contacts_emails');

        $this->dropForeignKey('tags_tenant_fk', 'tags');

        $this->dropForeignKey('contacts_tenant_fk', 'contacts');
        $this->dropForeignKey('contact_companies_fk', 'contacts');
        $this->dropForeignKey('contact_addresses_residence_fk', 'contacts');
        $this->dropForeignKey('contact_addresses_living_fk', 'contacts');

        $this->dropForeignKey('contacts_addresses_tenant_fk', 'contacts_addresses');
        $this->dropForeignKey('companies_tenant_fk', 'companies');

        $this->dropTable('contacts_visas');
        $this->dropTable('visas_types');
        $this->dropTable('contacts_passports');
        $this->dropTable('passports_types');
        $this->dropTable('contacts_tags');
        $this->dropTable('tags');
        $this->dropTable('contacts_sites');
        $this->dropTable('contacts_messengers');
        $this->dropTable('contacts_socials');
        $this->dropTable('contacts_phones');
        $this->dropTable('contacts_emails');
        $this->dropTable('sites_types');
        $this->dropTable('messengers_types');
        $this->dropTable('socials_types');
        $this->dropTable('phones_types');
        $this->dropTable('emails_types');
        $this->dropTable('contacts');
        $this->dropTable('contacts_addresses');
        $this->dropTable('companies');

        $this->execute('DROP TYPE tag_type');
        $this->execute('DROP TYPE contact_type');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_071541_contacts_table cannot be reverted.\n";

        return false;
    }
    */
}
