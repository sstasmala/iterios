<?php

use yii\db\Migration;

/**
 * Class m180511_111530_transactions
 */
class m180511_111530_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE transactions_id_seq;');
        $this->createTable('transactions',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'transactions_id_seq\')',
                'sum'=>'decimal(11,2) NOT NULL DEFAULT 0.00',
                'action'=>'integer NOT NULL DEFAULT 0',
                'tariff_id'=>'bigint',
                'financial_operation_id'=>'bigint',
                'tariff_order_id'=>'bigint',
                'owner'=>'bigint NOT NULL DEFAULT 0',
                'date'=>'bigint',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
            ]
        );
        $this->execute('ALTER SEQUENCE transactions_id_seq OWNED BY transactions.id;');
        $this->addForeignKey('transactions_tariffs_fk','transactions','tariff_id',
            'tariffs','id','CASCADE','CASCADE');
        $this->addForeignKey('transactions_fo_fk','transactions','financial_operation_id',
            'financial_operations','id','CASCADE','CASCADE');
        $this->addForeignKey('transactions_tariff_order_fk','transactions','tariff_order_id',
            'tariffs_orders','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('transactions_tariff_order_fk','transactions');
        $this->dropForeignKey('transactions_fo_fk','transactions');
        $this->dropForeignKey('transactions_tariffs_fk','transactions');
        $this->dropTable('transactions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180511_111530_transactions cannot be reverted.\n";

        return false;
    }
    */
}
