<?php

use yii\db\Migration;

/**
 * Class m180606_141236_add_date_to_rates
 */
class m180606_141236_add_date_to_rates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_ex_rates','date','bigint');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_ex_rates','date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180606_141236_add_date_to_rates cannot be reverted.\n";

        return false;
    }
    */
}
