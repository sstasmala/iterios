<?php

use yii\db\Migration;

/**
 * Class m180509_114619_create_contacts_timeline_view
 */
class m180509_114619_create_contacts_timeline_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE OR REPLACE VIEW contacts_timeline_view AS (SELECT logs.id, null as value, null as contact_id, null as request_id, logs.created_at, logs.created_by, null as updated_at, null as updated_by, logs.table_name, logs.item_id, logs.group_id, logs.tenant_id, logs.action, logs.data, 'log' as type FROM logs
    UNION
    SELECT request_notes.id, request_notes.value, request_notes.contact_id,   request_notes.request_id, request_notes.created_at, request_notes.created_by, request_notes.updated_at, request_notes.updated_by, null, null, null, null, null, null, 'request_note' as type FROM request_notes
    UNION
    SELECT contacts_notes.id, contacts_notes.value, contacts_notes.contact_id, null, contacts_notes.created_at, contacts_notes.created_by, contacts_notes.updated_at, contacts_notes.updated_by, null, null, null, null, null, null, 'contact_note' as type FROM contacts_notes);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP VIEW contacts_timeline_view");
    }
}
