<?php

use yii\db\Migration;

/**
 * Class m180213_092721_set_unique_code
 */
class m180213_092721_set_unique_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->alterColumn('ui_translations','code','ADD UNIQUE');
        $this->execute('ALTER TABLE "ui_translations"  ADD CONSTRAINT ui_translations_code_unique UNIQUE ("code")');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE "ui_translations" DROP CONSTRAINT ui_translations_code_unique');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_092721_set_unique_code cannot be reverted.\n";

        return false;
    }
    */
}
