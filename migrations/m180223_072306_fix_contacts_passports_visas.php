<?php

use yii\db\Migration;

/**
 * Class m180223_072306_fix_contacts_passports_visas
 */
class m180223_072306_fix_contacts_passports_visas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contacts_passports','birth_date');
        $this->dropColumn('contacts_passports','date_limit');
        $this->dropColumn('contacts_passports','issued_date');
        $this->addColumn('contacts_passports','birth_date','bigint');
        $this->addColumn('contacts_passports','issued_date','bigint');
        $this->addColumn('contacts_passports','date_limit','bigint');

        $this->dropColumn('contacts_visas','first_name');
        $this->dropColumn('contacts_visas','last_name');
        $this->dropColumn('contacts_visas','nationality');
        $this->dropColumn('contacts_visas','issued_owner');
        $this->dropColumn('contacts_visas','issued_date');
        $this->dropColumn('contacts_visas','date_limit');
        $this->dropColumn('contacts_visas','birth_date');
        $this->dropColumn('contacts_visas','serial');
        $this->addColumn('contacts_visas','begin_date','bigint');
        $this->addColumn('contacts_visas','end_date','bigint');
        $this->addColumn('contacts_visas','description','text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts_passports','birth_date');
        $this->dropColumn('contacts_passports','date_limit');
        $this->dropColumn('contacts_passports','issued_date');
        $this->addColumn('contacts_passports','birth_date',$this->string(50));
        $this->addColumn('contacts_passports','issued_date',$this->string(50));
        $this->addColumn('contacts_passports','date_limit',$this->string(50));

        $this->dropColumn('contacts_visas','begin_date');
        $this->dropColumn('contacts_visas','end_date');
        $this->dropColumn('contacts_visas','description');
        $this->addColumn('contacts_visas','first_name',$this->string(255));
        $this->addColumn('contacts_visas','last_name','varchar');
        $this->addColumn('contacts_visas','nationality',$this->string(255));
        $this->addColumn('contacts_visas','issued_owner',$this->string(255));
        $this->addColumn('contacts_visas','date_limit',$this->string(50));
        $this->addColumn('contacts_visas','issued_date',$this->string(50));
        $this->addColumn('contacts_visas','birth_date',$this->string(50));
        $this->addColumn('contacts_visas','serial','varchar');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_072306_fix_contacts_passports_visas cannot be reverted.\n";

        return false;
    }
    */
}
