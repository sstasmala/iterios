<?php

use yii\db\Migration;

/**
 * Class m180411_060611_update_tenants_table
 */
class m180411_060611_update_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants', 'week_start', $this->string(10)->defaultValue('1'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tenants', 'week_start');
    }
}
