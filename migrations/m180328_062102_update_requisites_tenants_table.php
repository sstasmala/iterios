<?php

use yii\db\Migration;

/**
 * Class m180328_062102_update_requisites_tenants_table
 */
class m180328_062102_update_requisites_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('requisites_tenants', 'value');
        $this->addColumn('requisites_tenants', 'value', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requisites_tenants', 'value');
        $this->addColumn('requisites_tenants', 'value', $this->string());

    }
}
