<?php

use yii\db\Migration;

/**
 * Handles the creation of table `system_holidays`.
 */
class m180410_060656_create_system_holidays_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('system_holidays', [
            'id' => $this->primaryKey(),
            'day' => $this->integer(),
            'mounts' => $this->integer(),
            'name'  => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('system_holidays');
    }
}
