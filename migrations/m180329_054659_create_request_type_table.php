<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_type`.
 */
class m180329_054659_create_request_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request_type', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'created_at'=>'bigint',
            'updated_at'=>'bigint',
            'created_by'=>'integer',
            'updated_by'=>'integer'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('request_type');
    }
}
