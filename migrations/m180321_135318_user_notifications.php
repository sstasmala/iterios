<?php

use yii\db\Migration;

/**
 * Class m180321_135318_user_notifications
 */
class m180321_135318_user_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE user_notifications_id_seq;');
        $this->createTable('user_notifications',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'user_notifications_id_seq\')',
                'name'=>'varchar(250) NOT NULL',
                'subject'=>'varchar(250)',
                'body'=>'text',
                'system_notification'=>'text',
                'sms'=>'text',
                'trigger'=>'integer NOT NULL DEFAULT 0',
                'trigger_time'=>'varchar',
                'task_name'=>'varchar(255)',
                'condition'=>'text',
                'module'=>'varchar(100) NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE user_notifications_id_seq OWNED BY user_notifications.id;');

        $this->execute('CREATE SEQUENCE tenant_notifications_id_seq;');
        $this->createTable('tenant_notifications',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'tenant_notifications_id_seq\')',
                'user_notification_id'=>'bigint',
                'base_notification_id'=>'bigint',
                'is_email'=>'integer NOT NULL DEFAULT 0',
                'is_system_notification'=>'integer NOT NULL DEFAULT 0',
                'is_task'=>'integer NOT NULL DEFAULT 0',
                'tenant_id'=>'integer',
                'last_run'=>'bigint',
                'repeat'=>'integer NOT NULL DEFAULT 0',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE tenant_notifications_id_seq OWNED BY tenant_notifications.id;');

        $this->addForeignKey('tenant_notifications_fk_un','tenant_notifications','user_notification_id','user_notifications','id','CASCADE','CASCADE');
        $this->addForeignKey('tenant_notifications_fk_bn','tenant_notifications','base_notification_id','base_notifications','id','CASCADE','CASCADE');
        $this->addForeignKey('tenant_notifications_fk_tenant','tenant_notifications','tenant_id','tenants','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tenant_notifications_fk_un','tenant_notifications');
        $this->dropForeignKey('tenant_notifications_fk_bn','tenant_notifications');
        $this->dropForeignKey('tenant_notifications_fk_tenant','tenant_notifications');
        $this->dropTable('tenant_notifications');
        $this->dropTable('user_notifications');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_135318_user_notifications cannot be reverted.\n";

        return false;
    }
    */
}
