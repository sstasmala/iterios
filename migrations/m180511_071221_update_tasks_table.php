<?php

use yii\db\Migration;

/**
 * Class m180511_071221_update_tasks_table
 */
class m180511_071221_update_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'contact_id', $this->integer());
        $this->addColumn('tasks', 'company_id', $this->integer());

        $this->addForeignKey('fk-tasks-contact_id', 'tasks', 'contact_id', 'contacts', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tasks-company_id', 'tasks', 'company_id', 'companies', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-contact_id', 'tasks');
        $this->dropForeignKey('fk-tasks-company_id', 'tasks');

        $this->dropColumn('tasks', 'contact_id');
        $this->dropColumn('tasks', 'company_id');
    }
}
