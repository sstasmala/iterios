<?php

use yii\db\Migration;

/**
 * Class m180515_101821_update_base_notifications_table
 */
class m180515_101821_update_base_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('base_notifications','is_slave_contact','integer NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('base_notifications','is_slave_contact');
    }
}
