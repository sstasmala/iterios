<?php

use yii\db\Migration;

/**
 * Class m180623_110959_update_delivery_templates_table
 */
class m180623_110959_update_delivery_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('delivery_email_templates', 'tenant_id', $this->integer());
        $this->addForeignKey('fk-delivery_email_templates-tenant_id', 'delivery_email_templates', 'tenant_id', 'tenants', 'id');

        $this->addColumn('delivery_sms_templates', 'tenant_id', $this->integer());
        $this->addForeignKey('fk-delivery_sms_templates-tenant_id', 'delivery_sms_templates', 'tenant_id', 'tenants', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-delivery_sms_templates-tenant_id', 'delivery_sms_templates');
        $this->dropColumn('delivery_sms_templates', 'tenant_id');

        $this->dropForeignKey('fk-delivery_email_templates-tenant_id', 'delivery_email_templates');
        $this->dropColumn('delivery_email_templates', 'tenant_id');
    }
}
