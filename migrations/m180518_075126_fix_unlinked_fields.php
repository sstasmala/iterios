<?php

use yii\db\Migration;

/**
 * Class m180518_075126_fix_unlinked_fields
 */
class m180518_075126_fix_unlinked_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $links = \app\models\ServicesFields::find()->asArray()->all();
        $ids = array_column($links,'id_field');

        if (!empty($ids))
            $this->execute('DELETE FROM services_fields_default WHERE is_default = 0 AND id NOT IN ('.implode(',',$ids).')');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180518_075126_fix_unlinked_fields cannot be reverted.\n";

        return false;
    }
    */
}
