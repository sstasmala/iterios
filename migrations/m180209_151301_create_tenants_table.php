<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tenants`.
 */
class m180209_151301_create_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tenants', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'owner_id' => $this->integer()->null(),
            'language_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk_tenants_users', 'tenants', 'owner_id', 'users', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_tenants_languages', 'tenants', 'language_id', 'languages', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_tenants_users', 'tenants');
        $this->dropForeignKey('fk_tenants_languages', 'tenants');

        $this->dropTable('tenants');
    }
}
