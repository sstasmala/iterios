<?php

use yii\db\Migration;

/**
 * Class m180324_124305_update_requisites_tenants_table
 */
class m180324_124305_update_requisites_tenants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requisites_tenants','tenant_id','integer');
        $this->addForeignKey('fk_requisites_tenants_tenant', 'requisites_tenants', 'tenant_id', 'tenants', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_requisites_tenants_tenant','requisites_tenants');
        $this->dropColumn('requisites_tenants','tenant_id');
    }

}
