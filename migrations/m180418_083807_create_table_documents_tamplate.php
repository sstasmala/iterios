<?php

use yii\db\Migration;

/**
 * Class m180418_083807_create_table_documents_tamplate
 */
class m180418_083807_create_table_documents_tamplate extends Migration
{
    public function safeUp()
    {
        $this->createTable('documents_template', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'languages' => $this->string(),
            'documents_type_id' => $this->integer(),
            'suppliers_id' => $this->integer(),
            'body' => $this->text(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('documents_template');
    }
}
