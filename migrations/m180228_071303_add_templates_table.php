<?php

use yii\db\Migration;

/**
 * Class m180228_071303_add_templates_table
 */
class m180228_071303_add_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->execute('CREATE TYPE template_type AS ENUM(\'system\',\'custom\');');
        $this->execute('CREATE SEQUENCE templates_id_seq;');
        $this->createTable('templates',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'templates_id_seq\')',
                'name'=>'varchar(250) NOT NULL',
                'tenant_id'=>'integer',
                'type'=>'varchar(100) NOT NULL',
                'subject'=>'varchar',
                'body'=>'text',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE templates_id_seq OWNED BY templates.id;');
        $this->addForeignKey('templates_tenant_fk','templates','tenant_id','tenants','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('templates_tenant_fk','templates');
        $this->dropTable('templates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180228_071303_add_templates_table cannot be reverted.\n";

        return false;
    }
    */
}
