<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs`.
 */
class m180227_111948_create_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE logs_id_seq;');
        $this->createTable('logs', [
            'id' => 'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'logs_id_seq\')',
            'table_name' => 'varchar(255)',
            'item_id' => 'integer',
            'group_id' => 'integer',
            'tenant_id' => 'integer',
            'action' => 'varchar(6)',
            'data' => 'text',
            'created_by' => 'integer',
            'created_at' => 'bigint'
        ]);
        $this->execute('ALTER SEQUENCE logs_id_seq OWNED BY logs.id;');
        $this->addForeignKey('logs_created_by_fk', 'logs', 'created_by', 'users', 'id');
        $this->addForeignKey('logs_tenant_fk','logs','tenant_id','tenants','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('logs_created_by_fk', 'logs');
        $this->dropForeignKey('logs_tenant_fk', 'logs');
        $this->dropTable('logs');
    }
}
