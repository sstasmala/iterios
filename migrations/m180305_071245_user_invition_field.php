<?php

use yii\db\Migration;

/**
 * Class m180305_071245_user_invition_field
 */
class m180305_071245_user_invition_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE invites_id_seq;');
        $this->createTable('invites',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'invites_id_seq\')',
                'token'=>'varchar(32) NOT NULL',
                'user_id'=>'integer NOT NULL',
                'tenant_id'=>'integer NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE invites_id_seq OWNED BY invites.id;');
        $this->addForeignKey('invites_tenant_fk','invites','tenant_id','tenants','id','CASCADE');
        $this->addForeignKey('invites_users_fk','invites','user_id','users','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('invites_tenant_fk','invites');
        $this->dropForeignKey('invites_users_fk','invites');
        $this->dropTable('invites');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_071245_user_invition_field cannot be reverted.\n";

        return false;
    }
    */
}
