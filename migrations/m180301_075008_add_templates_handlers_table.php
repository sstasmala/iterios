<?php

use yii\db\Migration;

/**
 * Class m180301_075008_add_templates_handlers_table
 */
class m180301_075008_add_templates_handlers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE templates_handlers_id_seq;');
        $this->createTable('templates_handlers',
            [
                'id'=>'bigint PRIMARY KEY NOT NULL DEFAULT nextval(\'templates_handlers_id_seq\')',
                'handler'=>'varchar NOT NULL',
                'template_id'=>'bigint NOT NULL',
                'created_at'=>'bigint',
                'updated_at'=>'bigint',
                'created_by'=>'integer',
                'updated_by'=>'integer'
            ]
        );
        $this->execute('ALTER SEQUENCE templates_handlers_id_seq OWNED BY templates_handlers.id;');
        $this->addForeignKey('templates_handlers_tenant_fk','templates_handlers','template_id','templates','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('templates_handlers_tenant_fk','templates_handlers');
        $this->dropTable('templates_handlers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180301_075008_add_templates_handlers_table cannot be reverted.\n";

        return false;
    }
    */
}
