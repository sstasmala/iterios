<?php

use yii\db\Migration;

/**
 * Class m180216_132057_default_types
 */
class m180216_132057_default_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('emails_types','default','bool DEFAULT false');
        $this->addColumn('phones_types','default','bool DEFAULT false');
        $this->execute('ALTER TABLE contacts RENAME COLUMN sex TO gender');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('emails_types','default');
        $this->dropColumn('phones_types','default');
        $this->execute('ALTER TABLE contacts RENAME COLUMN gender TO sex');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_132057_default_types cannot be reverted.\n";

        return false;
    }
    */
}
