<?php

use yii\db\Migration;

/**
 * Class m180518_051641_update_tasks_table
 */
class m180518_051641_update_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'order_id', $this->integer());
        $this->addForeignKey('fk-tasks-order_id', 'tasks', 'order_id', 'orders', 'id', 'SET NULL', 'CASCADE');

        $this->dropForeignKey('fk-tasks-contact_id', 'tasks');
        $this->dropForeignKey('fk-tasks-company_id', 'tasks');
        $this->addForeignKey('fk-tasks-contact_id', 'tasks', 'contact_id', 'contacts', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tasks-company_id', 'tasks', 'company_id', 'companies', 'id', 'SET NULL', 'CASCADE');

        $this->dropForeignKey('fk-order_reminders-task_id','order_reminders');
        $this->addForeignKey('fk-order_reminders-task_id','order_reminders','task_id','tasks','id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-order_id', 'tasks');
        $this->dropColumn('tasks', 'order_id');
    }
}
