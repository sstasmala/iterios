<?php

use yii\db\Migration;

/**
 * Class m180209_071908_languages_table
 */
class m180209_071908_languages_table extends Migration
{
    private $data = [
        ['iso'=>'ab','original_name'=>'Аҧсуа','name' => 'Abkhazian'],
        ['iso'=>'aa','original_name'=>'Qafaraf','name' => 'Afar'],
        ['iso'=>'af','original_name'=>'Afrikaans','name' => 'Afrikaans'],
        ['iso'=>'ak','original_name'=>'Akan','name' => 'Akan'],
        ['iso'=>'sq','original_name'=>'Shqip','name' => 'Albanian'],
        ['iso'=>'am','original_name'=>'አማርኛ','name' => 'Amharic'],
        ['iso'=>'ar','original_name'=>'العربية','name' => 'Arabic'],
        ['iso'=>'an','original_name'=>'Aragonés','name' => 'Aragonese'],
        ['iso'=>'hy','original_name'=>'Հայերեն','name' => 'Armenian'],
        ['iso'=>'as','original_name'=>'অসমীয়া','name' => 'Assamese'],
        ['iso'=>'av','original_name'=>'авар мацӀ; магӀарул мацӀ','name' => 'Avaric'],
        ['iso'=>'ae','original_name'=>'avesta','name' => 'Avestan'],
        ['iso'=>'ay','original_name'=>'aymar aru','name' => 'Aymara'],
        ['iso'=>'az','original_name'=>'azərbaycan dili','name' => 'Azerbaijani'],
        ['iso'=>'bm','original_name'=>'bamanankan','name' => 'Bambara'],
        ['iso'=>'ba','original_name'=>'башҡорт теле','name' => 'Bashkir'],
        ['iso'=>'eu','original_name'=>'euskara','name' => 'Basque'],
        ['iso'=>'be','original_name'=>'Беларуская','name' => 'Belarusian'],
        ['iso'=>'bn','original_name'=>'বাংলা','name' => 'Bengali'],
        ['iso'=>'bh','original_name'=>'भोजपुरी','name' => 'Bihari languages'],
        ['iso'=>'bi','original_name'=>'Bislama','name' => 'Bislama'],
        ['iso'=>'bs','original_name'=>'bosanski jezik','name' => 'Bosnian'],
        ['iso'=>'br','original_name'=>'brezhoneg','name' => 'Breton'],
        ['iso'=>'bg','original_name'=>'български език','name' => 'Bulgarian'],
        ['iso'=>'my','original_name'=>'ဗမာစာ','name' => 'Burmese'],
        ['iso'=>'ca','original_name'=>'Català','name' => 'Catalan, Valencian'],
        ['iso'=>'km','original_name'=>'ភាសាខ្មែរ','name' => 'Central Khmer'],
        ['iso'=>'ch','original_name'=>'Chamoru','name' => 'Chamorro'],
        ['iso'=>'ce','original_name'=>'нохчийн мотт','name' => 'Chechen'],
        ['iso'=>'ny','original_name'=>'chiCheŵa; chinyanja','name' => 'Chichewa, Chewa, Nyanja'],
        ['iso'=>'zh','original_name'=>'中文, 汉语, 漢語','name' => 'Chinese'],
        ['iso'=>'cu','original_name'=>'Church Slavonic','name' => 'Church Slavonic, Old Bulgarian, Old Church Slavonic'],
        ['iso'=>'cv','original_name'=>'чӑваш чӗлхи','name' => 'Chuvash'],
        ['iso'=>'kw','original_name'=>'Kernewek','name' => 'Cornish'],
        ['iso'=>'co','original_name'=>'corsu; lingua corsa','name' => 'Corsican'],
        ['iso'=>'cr','original_name'=>'ᓀᐦᐃᔭᐍᐏᐣ','name' => 'Cree'],
        ['iso'=>'hr','original_name'=>'Hrvatski','name' => 'Croatian'],
        ['iso'=>'cs','original_name'=>'česky; čeština','name' => 'Czech'],
        ['iso'=>'da','original_name'=>'dansk','name' => 'Danish'],
        ['iso'=>'dv','original_name'=>'Divehi','name' => 'Divehi, Dhivehi, Maldivian'],
        ['iso'=>'nl','original_name'=>'Nederlands','name' => 'Dutch, Flemish'],
        ['iso'=>'dz','original_name'=>'རྫོང་ཁ','name' => 'Dzongkha'],
        ['iso'=>'en','original_name'=>'English','name' => 'English'],
        ['iso'=>'eo','original_name'=>'Esperanto','name' => 'Esperanto'],
        ['iso'=>'et','original_name'=>'Eesti keel','name' => 'Estonian'],
        ['iso'=>'ee','original_name'=>'Ɛʋɛgbɛ','name' => 'Ewe'],
        ['iso'=>'fo','original_name'=>'Føroyskt','name' => 'Faroese'],
        ['iso'=>'fj','original_name'=>'vosa Vakaviti','name' => 'Fijian'],
        ['iso'=>'fi','original_name'=>'suomen kieli','name' => 'Finnish'],
        ['iso'=>'fr','original_name'=>'français; langue française','name' => 'French'],
        ['iso'=>'ff','original_name'=>'Fulfulde','name' => 'Fulah'],
        ['iso'=>'gd','original_name'=>'Gàidhlig','name' => 'Gaelic, Scottish Gaelic'],
        ['iso'=>'gl','original_name'=>'Galego','name' => 'Galician'],
        ['iso'=>'lg','original_name'=>'Luganda','name' => 'Ganda'],
        ['iso'=>'ka','original_name'=>'ქართული','name' => 'Georgian'],
        ['iso'=>'de','original_name'=>'Deutsch','name' => 'German'],
        ['iso'=>'ki','original_name'=>'Gĩkũyũ','name' => 'Gikuyu, Kikuyu'],
        ['iso'=>'el','original_name'=>'Ελληνικά','name' => 'Greek (Modern)'],
        ['iso'=>'kl','original_name'=>'kalaallisut; kalaallit oqaasii','name' => 'Greenlandic, Kalaallisut'],
        ['iso'=>'gn','original_name'=>'Avañe\'ẽ','name' => 'Guarani'],
        ['iso'=>'gu','original_name'=>'ગુજરાતી','name' => 'Gujarati'],
        ['iso'=>'ht','original_name'=>'Kreyòl ayisyen','name' => 'Haitian, Haitian Creole'],
        ['iso'=>'ha','original_name'=>'هَوُسَ','name' => 'Hausa'],
        ['iso'=>'he','original_name'=>'עברית','name' => 'Hebrew'],
        ['iso'=>'hz','original_name'=>'Otjiherero','name' => 'Herero'],
        ['iso'=>'hi','original_name'=>'हिन्दी; हिंदी','name' => 'Hindi'],
        ['iso'=>'ho','original_name'=>'Hiri Motu','name' => 'Hiri Motu'],
        ['iso'=>'hu','original_name'=>'Magyar','name' => 'Hungarian'],
        ['iso'=>'is','original_name'=>'Íslenska','name' => 'Icelandic'],
        ['iso'=>'io','original_name'=>'Ido','name' => 'Ido'],
        ['iso'=>'ig','original_name'=>'Igbo','name' => 'Igbo'],
        ['iso'=>'id','original_name'=>'Bahasa Indonesia','name' => 'Indonesian'],
        ['iso'=>'ia','original_name'=>'interlingua','name' => 'Interlingua (International Auxiliary Language Association)'],
        ['iso'=>'ie','original_name'=>'Interlingue','name' => 'Interlingue'],
        ['iso'=>'iu','original_name'=>'ᐃᓄᒃᑎᑐᑦ','name' => 'Inuktitut'],
        ['iso'=>'ik','original_name'=>'Iñupiaq; Iñupiatun','name' => 'Inupiaq'],
        ['iso'=>'ga','original_name'=>'Gaeilge','name' => 'Irish'],
        ['iso'=>'it','original_name'=>'Italiano','name' => 'Italian'],
        ['iso'=>'ja','original_name'=>'日本語 (にほんご／にっぽんご)','name' => 'Japanese'],
        ['iso'=>'jv','original_name'=>'basa Jawa','name' => 'Javanese'],
        ['iso'=>'kn','original_name'=>'ಕನ್ನಡ','name' => 'Kannada'],
        ['iso'=>'kr','original_name'=>'Kanuri','name' => 'Kanuri'],
        ['iso'=>'ks','original_name'=>'कश्मीरी    كشميري','name' => 'Kashmiri'],
        ['iso'=>'kk','original_name'=>'Қазақ тілі','name' => 'Kazakh'],
        ['iso'=>'rw','original_name'=>'Kinyarwanda','name' => 'Kinyarwanda'],
        ['iso'=>'kv','original_name'=>'коми кыв','name' => 'Komi'],
        ['iso'=>'kg','original_name'=>'KiKongo','name' => 'Kongo'],
        ['iso'=>'ko','original_name'=>'한국어 (韓國語); 조선말 (朝鮮語)','name' => 'Korean'],
        ['iso'=>'kj','original_name'=>'Kuanyama','name' => 'Kwanyama, Kuanyama'],
        ['iso'=>'ku','original_name'=>'Kurdî; كوردی','name' => 'Kurdish'],
        ['iso'=>'ky','original_name'=>'кыргыз тили','name' => 'Kyrgyz'],
        ['iso'=>'lo','original_name'=>'ພາສາລາວ','name' => 'Lao'],
        ['iso'=>'la','original_name'=>'latine; lingua latina','name' => 'Latin'],
        ['iso'=>'lv','original_name'=>'latviešu valoda','name' => 'Latvian'],
        ['iso'=>'lb','original_name'=>'Lëtzebuergesch','name' => 'Letzeburgesch, Luxembourgish'],
        ['iso'=>'li','original_name'=>'Limburgs','name' => 'Limburgish, Limburgan, Limburger'],
        ['iso'=>'ln','original_name'=>'Lingála','name' => 'Lingala'],
        ['iso'=>'lt','original_name'=>'lietuvių kalba','name' => 'Lithuanian'],
        ['iso'=>'lu','original_name'=>'Luba-Katanga','name' => 'Luba-Katanga'],
        ['iso'=>'mk','original_name'=>'македонски јазик','name' => 'Macedonian'],
        ['iso'=>'mg','original_name'=>'Malagasy fiteny','name' => 'Malagasy'],
        ['iso'=>'ms','original_name'=>'bahasa Melayu; بهاس ملايو','name' => 'Malay'],
        ['iso'=>'ml','original_name'=>'മലയാളം','name' => 'Malayalam'],
        ['iso'=>'mt','original_name'=>'Malti','name' => 'Maltese'],
        ['iso'=>'gv','original_name'=>'Ghaelg','name' => 'Manx'],
        ['iso'=>'mi','original_name'=>'te reo Māori','name' => 'Maori'],
        ['iso'=>'mr','original_name'=>'मराठी','name' => 'Marathi'],
        ['iso'=>'mh','original_name'=>'Kajin M̧ajeļ','name' => 'Marshallese'],
        ['iso'=>'ro','original_name'=>'română;limba moldoveneasca','name' => 'Moldovan, Moldavian, Romanian'],
        ['iso'=>'mn','original_name'=>'Монгол','name' => 'Mongolian'],
        ['iso'=>'na','original_name'=>'Ekakairũ Naoero','name' => 'Nauru'],
        ['iso'=>'nv','original_name'=>'Diné bizaad; Dinékʼehǰí','name' => 'Navajo, Navaho'],
        ['iso'=>'nd','original_name'=>'isiNdebele','name' => 'Northern Ndebele'],
        ['iso'=>'ng','original_name'=>'Owambo','name' => 'Ndonga'],
        ['iso'=>'ne','original_name'=>'नेपाली','name' => 'Nepali'],
        ['iso'=>'se','original_name'=>'Davvisámegiella','name' => 'Northern Sami'],
        ['iso'=>'no','original_name'=>'Norsk','name' => 'Norwegian'],
        ['iso'=>'nb','original_name'=>'Norsk bokmål','name' => 'Norwegian Bokmål'],
        ['iso'=>'nn','original_name'=>'Norsk nynorsk','name' => 'Norwegian Nynorsk'],
        ['iso'=>'ii','original_name'=>'ꆇꉙ','name' => 'Nuosu, Sichuan Yi'],
        ['iso'=>'oc','original_name'=>'Occitan','name' => 'Occitan (post 1500)'],
        ['iso'=>'oj','original_name'=>'ᐊᓂᔑᓈᐯᒧᐎᓐ','name' => 'Ojibwa'],
        ['iso'=>'or','original_name'=>'ଓଡ଼ିଆ','name' => 'Oriya'],
        ['iso'=>'om','original_name'=>'Afaan Oromoo','name' => 'Oromo'],
        ['iso'=>'os','original_name'=>'Ирон æвзаг','name' => 'Ossetian, Ossetic'],
        ['iso'=>'pi','original_name'=>'पाऴि','name' => 'Pali'],
        ['iso'=>'pa','original_name'=>'ਪੰਜਾਬੀ; پنجابی','name' => 'Panjabi, Punjabi'],
        ['iso'=>'ps','original_name'=>'پښتو','name' => 'Pashto, Pushto'],
        ['iso'=>'fa','original_name'=>'فارسی','name' => 'Persian'],
        ['iso'=>'pl','original_name'=>'polski','name' => 'Polish'],
        ['iso'=>'pt','original_name'=>'Português','name' => 'Portuguese'],
        ['iso'=>'qu','original_name'=>'Runa Simi; Kichwa','name' => 'Quechua'],
        ['iso'=>'rm','original_name'=>'rumantsch grischun','name' => 'Romansh'],
        ['iso'=>'rn','original_name'=>'kiRundi','name' => 'Rundi'],
        ['iso'=>'ru','original_name'=>'русский язык','name' => 'Russian'],
        ['iso'=>'sm','original_name'=>'gagana fa\'a Samoa','name' => 'Samoan'],
        ['iso'=>'sg','original_name'=>'yângâ tî sängö','name' => 'Sango'],
        ['iso'=>'sa','original_name'=>'संस्कृतम्','name' => 'Sanskrit'],
        ['iso'=>'sc','original_name'=>'sardu','name' => 'Sardinian'],
        ['iso'=>'sr','original_name'=>'српски језик','name' => 'Serbian'],
        ['iso'=>'sn','original_name'=>'chiShona','name' => 'Shona'],
        ['iso'=>'sd','original_name'=>'सिन्धी; ‫سنڌي، سندھی','name' => 'Sindhi'],
        ['iso'=>'si','original_name'=>'සිංහල','name' => 'Sinhala, Sinhalese'],
        ['iso'=>'sk','original_name'=>'slovenčina','name' => 'Slovak'],
        ['iso'=>'sl','original_name'=>'slovenščina','name' => 'Slovenian'],
        ['iso'=>'so','original_name'=>'Soomaaliga; af Soomaali','name' => 'Somali'],
        ['iso'=>'st','original_name'=>'seSotho','name' => 'Sotho, Southern'],
        ['iso'=>'nr','original_name'=>'Ndébélé','name' => 'South Ndebele'],
        ['iso'=>'es','original_name'=>'español; castellano','name' => 'Spanish, Castilian'],
        ['iso'=>'su','original_name'=>'Basa Sunda','name' => 'Sundanese'],
        ['iso'=>'sw','original_name'=>'Kiswahili','name' => 'Swahili'],
        ['iso'=>'ss','original_name'=>'SiSwati','name' => 'Swati'],
        ['iso'=>'sv','original_name'=>'Svenska','name' => 'Swedish'],
        ['iso'=>'tl','original_name'=>'Tagalog','name' => 'Tagalog'],
        ['iso'=>'ty','original_name'=>'Reo Mā`ohi','name' => 'Tahitian'],
        ['iso'=>'tg','original_name'=>'тоҷикӣ; toğikī; ‫تاجیکی','name' => 'Tajik'],
        ['iso'=>'ta','original_name'=>'தமிழ்','name' => 'Tamil'],
        ['iso'=>'tt','original_name'=>'татарча; tatarça; ‫تاتارچا','name' => 'Tatar'],
        ['iso'=>'te','original_name'=>'తెలుగు','name' => 'Telugu'],
        ['iso'=>'th','original_name'=>'ไทย','name' => 'Thai'],
        ['iso'=>'bo','original_name'=>'བོད་ཡིག','name' => 'Tibetan'],
        ['iso'=>'ti','original_name'=>'ትግርኛ','name' => 'Tigrinya'],
        ['iso'=>'to','original_name'=>'faka Tonga','name' => 'Tonga (Tonga Islands)'],
        ['iso'=>'ts','original_name'=>'xiTsonga','name' => 'Tsonga'],
        ['iso'=>'tn','original_name'=>'seTswana','name' => 'Tswana'],
        ['iso'=>'tr','original_name'=>'Türkçe','name' => 'Turkish'],
        ['iso'=>'tk','original_name'=>'Türkmen; Түркмен','name' => 'Turkmen'],
        ['iso'=>'tw','original_name'=>'Twi','name' => 'Twi'],
        ['iso'=>'ug','original_name'=>'Uyƣurqə; ‫ئۇيغۇرچ','name' => 'Uighur, Uyghur'],
        ['iso'=>'uk','original_name'=>'Українська','name' => 'Ukrainian'],
        ['iso'=>'ur','original_name'=>'اردو','name' => 'Urdu'],
        ['iso'=>'uz','original_name'=>'O\'zbek; Ўзбек; أۇزبېك','name' => 'Uzbek'],
        ['iso'=>'ve','original_name'=>'tshiVenḓa','name' => 'Venda'],
        ['iso'=>'vi','original_name'=>'Tiếng Việt','name' => 'Vietnamese'],
        ['iso'=>'vo','original_name'=>'Volapük','name' => 'Volap_k'],
        ['iso'=>'wa','original_name'=>'Walon','name' => 'Walloon'],
        ['iso'=>'cy','original_name'=>'Cymraeg','name' => 'Welsh'],
        ['iso'=>'fy','original_name'=>'Frysk','name' => 'Western Frisian'],
        ['iso'=>'wo','original_name'=>'Wollof','name' => 'Wolof'],
        ['iso'=>'xh','original_name'=>'isiXhosa','name' => 'Xhosa'],
        ['iso'=>'yi','original_name'=>'ייִדיש','name' => 'Yiddish'],
        ['iso'=>'yo','original_name'=>'Yorùbá','name' => 'Yoruba'],
        ['iso'=>'za','original_name'=>'Saɯ cueŋƅ; Saw cuengh','name' => 'Zhuang, Chuang'],
        ['iso'=>'zu','original_name'=>'isiZulu','name' => 'Zulu']
    ];

    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE languages_id_seq;');
        $this->createTable('languages',
            [
                'id'=>'integer PRIMARY KEY NOT NULL DEFAULT nextval(\'languages_id_seq\')',
                'name'=>'varchar(250) NOT NULL UNIQUE',
                'original_name'=>'varchar(250) NOT NULL UNIQUE',
                'iso'=>'varchar(2) NOT NULL UNIQUE',
                'activated'=>'bool DEFAULT false'
            ]
        );
        $this->execute('ALTER SEQUENCE languages_id_seq OWNED BY languages.id;');


        foreach($this->data as $v)
        {
            if($v['iso']=='en' || $v['iso']=='ru')
                $this->insert('languages',['name'=>$v['name'],'original_name'=>$v['original_name'],'iso'=>$v['iso'],
                    'activated'=>true]);
            else
                $this->insert('languages',['name'=>$v['name'],'original_name'=>$v['original_name'],'iso'=>$v['iso']]);
        }
    }

    public function safeDown()
    {
        $this->dropTable('languages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_071908_languages_table cannot be reverted.\n";

        return false;
    }
    */
}
