<?php

use yii\db\Migration;

/**
 * Class m180509_110900_customer_renant_info
 */
class m180509_110900_customer_renant_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tenants','legal_name','varchar(255)');
        $this->addColumn('tenants','balance','decimal(11,2) NOT NULL DEFAULT 0.00');
        $this->addColumn('tenants','tariff_id','bigint');
        $this->addColumn('tenants','payment_method','integer');
        $this->addForeignKey('tenants_tariffs_fk','tenants','tariff_id',
            'tariffs','id','SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tenants_tariffs_fk','tenants');
        $this->dropColumn('tenants','legal_name');
        $this->dropColumn('tenants','balance');
        $this->dropColumn('tenants','tariff_id');
        $this->dropColumn('tenants','payment_method');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180509_110900_customer_renant_info cannot be reverted.\n";

        return false;
    }
    */
}
