<?php

use yii\db\Migration;

/**
 * Class m180314_140601_new_settings_MCHelper
 */
class m180314_140601_new_settings_MCHelper extends Migration
{

    protected $base_uri = [
        'key' => 'MCHelper_base_uri',
        'value' => 'https://api-multicodes.iterios.com/v1/',
        'group' => \app\models\Settings::MCHELPER_GROUP
    ];

    protected $key = [
        'key' => 'MCHelper_Key',
        'value' => '6hnTdNrtCv3yF3aMtpHwGAnpnH94RP2T',
        'group' => \app\models\Settings::MCHELPER_GROUP
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->insert('settings', $this->base_uri);
        $this->insert('settings', $this->key);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings', ['key' => $this->base_uri['key']]);
        $this->delete('settings', ['key' => $this->key['key']]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180314_140601_new_settings_MCHelper cannot be reverted.\n";

        return false;
    }
    */
}
