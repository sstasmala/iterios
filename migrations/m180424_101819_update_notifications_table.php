<?php

use yii\db\Migration;

/**
 * Class m180424_101819_update_notifications_table
 */
class m180424_101819_update_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifications_course', 'tenant_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifications_course', 'tenant_id');
    }
}
