<?php

use yii\db\Migration;

/**
 * Class m180604_120231_update_orders_links
 */
class m180604_120231_update_orders_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders_services_links','ex_rate','decimal(11,2) NOT NULL DEFAULT 1.00');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders_services_links','ex_rate');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_120231_update_orders_links cannot be reverted.\n";

        return false;
    }
    */
}
