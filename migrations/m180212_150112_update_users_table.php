<?php

use yii\db\Migration;

/**
 * Class m180212_150112_update_users_table
 */
class m180212_150112_update_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'current_tenant_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'current_tenant_id');
    }
}
