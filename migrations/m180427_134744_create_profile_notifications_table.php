<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile_notifications`.
 */
class m180427_134744_create_profile_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('profile_notifications', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'level' => $this->integer(),
            'is_email' => $this->integer(),
            'is_notice' => $this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

        $this->createTable('profile_notifications_tags', [
            'id' => $this->primaryKey(),
            'notification_id' => $this->integer(),
            'tag_id' => $this->integer(),
            'created_at'=>$this->bigInteger(),
            'updated_at'=>$this->bigInteger(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
        ]);

        $this->addForeignKey('fk_profile_notifications', 'profile_notifications_tags', 'notification_id', 'profile_notifications', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_profile_notifications', 'profile_notifications_tags');
        $this->dropTable('profile_notifications_tags');
        $this->dropTable('profile_notifications');
    }
}
