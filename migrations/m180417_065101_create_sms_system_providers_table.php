<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_system_providers`.
 */
class m180417_065101_create_sms_system_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms_system_providers', [
            'id' => $this->primaryKey(),
            'provider' => $this->string(),
            'name' => $this->string(),
            'system_aname' => $this->string(),
            'public_aname' => $this->string(),
            'credentials_config' => $this->text(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sms_system_providers');
    }
}
