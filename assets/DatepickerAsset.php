<?php
/**
 * DatepickerAsset.php
 * @copyright ©iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\assets;


use yii\web\AssetBundle;

class DatepickerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/eonasdan-bootstrap-datetimepicker';

    public $js =
        [
            'build/js/bootstrap-datetimepicker.min.js'
        ];
    public $css =
        [
            'build/css/bootstrap-datetimepicker.min.css'
        ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\AdminAsset'
    ];
}