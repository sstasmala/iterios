<?php

namespace app\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $baseUrl = '@web';
    public $css = [
        'css/layouts/main.min.css'
    ];
    public $js = [
        'admin/plugins/sms-counter/sms_counter.min.js',
        'js/layouts/render.min.js',
        'js/layouts/inputs_init.min.js',
        'js/layouts/main.min.js'
    ];
    public $depends = [
        'app\assets\MetronicAsset'
    ];
}
