<?php
/**
 * JQVMapAsset.php
 * @copyright ©iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\assets;


use yii\web\AssetBundle;

class JQVMapAsset extends AssetBundle
{
    public $baseUrl = '@web/metronic/assets/vendors/custom/jqvmap';
    public $js = [
            'jqvmap.bundle.js'
        ];
    public $css = [
            'jqvmap.bundle.css'
        ];
    public $depends = [
        'app\assets\MetronicAsset'
    ];
}