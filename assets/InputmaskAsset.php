<?php
/**
 * InputmaskAsset.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\assets;


use yii\web\AssetBundle;

class InputmaskAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/inputmask';

    public $js =
        [
            'dist/jquery.inputmask.bundle.js'
        ];

    public $css =
        [
//            'css/inputmask.css'
        ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\AdminAsset'
    ];
}