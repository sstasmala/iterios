<?php
/**
 * AdminAsset.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admin_lte/css/AdminLTE.css',
        'admin_lte/css/skins/skin-black-light.css',
        'metronic/src/vendors/fontawesome5/web-fonts-with-css/css/fontawesome-all.min.css',
        'metronic/src/vendors/flaticon/css/flaticon.css',
        'metronic/src/vendors/line-awesome/css/line-awesome.css',
        'admin/socicon/css/socicon.css'
//        'admin/plugins/select2/css/select2.min.css'
    ];
    public $js = [
        'admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'admin_lte/js/adminlte.js',
        'admin/main/main.js',
        'admin/plugins/moment.min.js',
        'admin/plugins/moment-timezone-with-data.js',
//        'admin/plugins/select2/js/select2.min.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}