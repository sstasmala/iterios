<?php

namespace app\assets;

use yii\web\AssetBundle;

class MetronicAsset extends AssetBundle
{
    public $baseUrl = '@web/metronic/assets';
    public $css = [
        'vendors/base/vendors.bundle.css',
        'demo/default/base/style.bundle.css'
    ];
    public $js = [
        'vendors/base/vendors.bundle.js',
        'demo/default/base/scripts.bundle.js'
    ];
    public $depends = [
        
    ];
}
