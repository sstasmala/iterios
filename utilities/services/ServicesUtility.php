<?php
namespace app\utilities\services;
use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\models\Contacts;
use app\models\ContactsPassports;
use app\models\ContactsPassportsView;
use app\models\OrdersServicesLinks;
use app\models\OrdersServicesValues;
use app\models\OrdersTourists;
use app\models\PassportsTypes;
use app\models\RequestsServicesLinks;
use app\models\RequestsServicesValues;
use app\models\ServicesFieldsDefault;
use app\models\Services;
use app\models\ServicesFields;
use app\models\ServicesLinks;
use app\models\Suppliers;
use app\models\Tenants;
use app\utilities\contacts\ContactsUtility;
use Yii;
use yii\web\HttpException;

/**
 * ServiceUtility.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
class ServicesUtility
{

    /**
     * Extract info from service object
     * @param array $service
     * @return array
     */
    private static function getServiceInfo(array $service):array
    {
        $result = $service;
        $result['name'] = (is_null($service['name']) || empty($service['name'])) ? '[Not translated]' : $service['name'];
        return $result;
    }


    /**
     * Extract info form filed object
     * @param array $field
     * @return array
     */
    private static function getField(array $field):array
    {
        $result = $field;
        $result['name']
            = (is_null($field['name']) || empty($field['name'])) ? '[Not translated]' : $field['name'];
        return $result;
    }

    /**
     * Get full configuration of service
     * @param $ids
     * @param $lang
     * @return array
     */
    public static function getServices($ids, string $lang):array
    {
        if(!is_array($ids))
            $ids = [$ids];

        $result = [];
        $services = Services::find()->where(['in','id',$ids])->translate($lang)->asArray()->all();

        $ids_s = array_column($services,'id');
        $links = ServicesLinks::find()->where(['in','parent_id',$ids_s])->asArray()->all();

        $ids_s = array_merge($ids_s,array_column($links,'child_id'));
        $ids_s = array_unique($ids_s);
        $services_full = Services::find()->where(['in','id',$ids_s])->translate($lang)->asArray()->all();

        $fields = ServicesFields::find()->where(['in','id_service',$ids_s])->asArray()->all();
        usort($fields,function ($a,$b){
            return $a['position']>$b['position'];
        });
        $ids_f = array_unique(array_column($fields,'id_field'));
        $fields_configs = ServicesFieldsDefault::find()->where(['in','id',$ids_f])->translate($lang)->asArray()->all();
        $fields_configs = array_combine(array_column($fields_configs,'id'),$fields_configs);

        /**
         * @var $services_full array
         */
        $data = [];
        foreach ($services_full as $service) {
            $data[$service['id']] = static::getServiceInfo($service);
            $data[$service['id']]['links']
                = array_column(array_filter($links,
                    function ($item)use($service){return $item['parent_id'] == $service['id'];})
                ,'child_id');

            $service_fields = array_filter($fields,
                function($item)use($service){return $item['id_service'] == $service['id'];});
            foreach ($service_fields as $k => $field) {
                $data[$service['id']][($fields_configs[$field['id_field']]['is_default'])?'fields_def':'fields'][$k]
                    = static::getField($fields_configs[$field['id_field']]);
            }
        }
        $data = array_combine(array_column($data,'id'),$data);

        /**
         * @var $services array
         */
        foreach ($services as $service) {
            $result[$service['id']] = $data[$service['id']];
            $links = $result[$service['id']]['links'];
            $result[$service['id']]['links'] = [];
            foreach ($links as $link)
                $result[$service['id']]['links'][] = $data[$link];
        }

        return $result;
    }


    /**
     * Format values structure from data
     * @param array $data
     * @return array
     */
    private static function processValues(array $data):array
    {
        $structure['base'] = array_filter($data,function ($elem){
            return $elem['additional_link_id'] === null;
        });

        $additional = array_filter($data,function ($elem){
            return $elem['additional_link_id'] !== null;
        });
        $structure['additional'] = [];

        $structure['base'] = array_combine(array_column($structure['base'],'field_id'),array_column($structure['base'],'value'));

        $a_links = array_unique(array_column($additional,'additional_link_id'));
        foreach ($a_links as $link) {
            $by_link = array_filter($additional,function($elem)use($link){return $elem['additional_link_id'] === $link;});
            $s_links = array_unique(array_column($by_link,'service_id'));
            foreach ($s_links as $s_link) {
                $structure['additional'][$link][$s_link] = array_filter($by_link,function($elem)use($s_link){
                    return $elem['service_id'] === $s_link;
                });
                $structure['additional'][$link][$s_link] = array_combine(array_column($structure['additional'][$link][$s_link],'field_id'),
                    array_column($structure['additional'][$link][$s_link],'value'));
            }
        }

        return $structure;
    }

    /**
     * Get values structure form request link
     * @param $link_id
     * @return array
     */
    public static function getRequestsValues($link_id):array
    {
        $values = RequestsServicesValues::find();
        if(is_array($link_id))
            $values->where(['in','link_id',$link_id]);
        else
            $values->where(['link_id'=>intval($link_id)]);
        $values = $values->asArray()->all();
        if(is_array($link_id)) {
            $vss = [];
            $ids = array_column($values,'link_id');
            $ids = array_unique($ids);
            foreach ($ids as $id)
            {
                $fields = array_filter($values,function($val)use($id){
                    return $val['link_id'] === $id;
                });
                $vss[$id] = static::processValues($fields);
            }
            return $vss;
        }
        return static::processValues($values);
    }

    /**
     * @param $link_id
     * @return array
     */
    public static function getOrdersValues($link_id):array
    {
        $values = OrdersServicesValues::find();
        if(is_array($link_id))
            $values->where(['in','link_id',$link_id]);
        else
            $values->where(['link_id'=>intval($link_id)]);
        $values = $values->asArray()->all();
        if(is_array($link_id)) {
            $vss = [];
            $ids = array_column($values,'link_id');
            $ids = array_unique($ids);
            foreach ($ids as $id)
            {
                $fields = array_filter($values,function($val)use($id){
                    return $val['link_id'] === $id;
                });
                $vss[$id] = static::processValues($fields);
            }
            return $vss;
        }
        return static::processValues($values);
    }

    /**
     * Set values in service fields
     * @param array $service
     * @param array $values
     */
    private static function setValues(array &$service, array $values)
    {
        if(isset($service['fields'])) {
            foreach ($service['fields'] as $k => $field) {
                if(isset($values[$field['id']]) && $values[$field['id']] !== null && $values[$field['id']] !== "")
                    $service['fields'][$k]['value'] = $values[$field['id']];
            }
        } else {
            $service['fields'] = [];
        }

        if(isset($service['fields_def'])) {
            foreach ($service['fields_def'] as $k => $field) {
                if(isset($values[$field['id']]) && $values[$field['id']] !== null && $values[$field['id']] !== "")
                    $service['fields_def'][$k]['value'] = $values[$field['id']];
            }
        } else {
            $service['fields_def'] = [];
        }

    }

    /**
     * @param array $service
     * @return array|mixed
     */
    public static function getFieldsFromService(array $service)
    {
        $fields = $service['fields'];
        $fields = array_merge($fields,$service['fields_def']);
        foreach ($service['links'] as $link) {
            $fields = array_merge($fields,$link['fields']);
            $fields = array_merge($fields,$link['fields_def']);
        }

        $fields = array_filter($fields, function($elem){
            return isset($elem['value']);
        });
        return $fields;
    }

    /**
     * @param array $services
     * @param array $values
     * @param Tenants $tenant
     * @return array
     */
    public static function fillServices(array $services, array $values,Tenants $tenant) {
        $fields = [];
        foreach ($services as $k => $service)
        {
            if(!isset($values[$k]))
                continue;
            if(!isset($services[$k]['fields'])){
                $services[$k]['fields'] = [];
                $service['fields'] = [];
            }

            $services[$k] = static::fillService($service, $values[$k]);
            $fields = array_merge($fields,static::getFieldsFromService( $services[$k]));
        }
        $reference = static::getMCValuesFields($fields,$tenant);
        $result = [];
        foreach ($services as $k => $service)
        {
            $service = self::setTitles($service,$reference);
            $result[$k] = $service;
        }
        return $result;
    }

    /**
     * @param $service
     * @param $reference
     * @return mixed
     */
    public static function setTitles($service,$reference) {
        if(isset($service['fields']))
            foreach ($service['fields'] as $i => $f) {
                if(isset($f['value']))
                    $service['fields'][$i]['value'] = static::getTitles($reference,$f['type'],$f['value']);
            }

        if(isset($service['fields_def']))
            foreach ($service['fields_def'] as $i => $f) {
                if(isset($f['value']))
                    $service['fields_def'][$i]['value'] = static::getTitles($reference,$f['type'],$f['value']);
            }

        if(isset($service['links']))   
            foreach ($service['links'] as $l => $s) {
                foreach ($s['fields'] as $i => $f) {
                    if(isset($f['value']))
                        $service['links'][$l]['fields'][$i]['value'] = static::getTitles($reference,$f['type'],$f['value']);
                }
                foreach ($s['fields_def'] as $i => $f) {
                    if(isset($f['value']))
                        $service['links'][$l]['fields_def'][$i]['value'] = static::getTitles($reference,$f['type'],$f['value']);
                }
            }

        return $service;
    }
    /**
     * Fill service and linked services
     * @param array $service
     * @param array $values
     * @param Tenants $tenant
     * @return array
     */
    public static function fillService(array $service, array $values)
    {
        static::setValues($service,$values['base']);
        if(isset($values['additional'])) {
            $links_info =  $service['links'];
            $service['links'] = [];
            foreach ($values['additional'] as $link => $srv) {
                if(!empty($srv)) {
                    $id = key($srv);
                    $service['links'][$link] = array_reduce($links_info,function($a,$b)use($id){
                        if($a!=null)
                            if($a['id']==$id)
                                return $a;
                        if($b['id']==$id)
                            return $b;
                        return null;
                    });
                    if($service['links'][$link] === null)
                    {
                        $service['links'][$link] = $service;
                        $service['links'][$link]['type'] = 1;
                        $service['links'][$link]['links'] = [];
                    }

                    static::setValues($service['links'][$link] ,reset($srv));
                }
            }
        }
        return $service;
    }

    /**
     * Collect id`s for MC api
     * @param array $fields
     * @return array
     */
    private static function mapFieldsMC(array $fields) {
        $suppliers = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_SUPPLIERS && !is_null($var['value']) && $var['value']!="");
        });
        $airports = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_AIRPORT && !is_null($var['value']) && $var['value']!="");
        });
        $cities = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_CITY && !is_null($var['value']) && $var['value']!="");
        });
        $countries = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_COUNTRY && !is_null($var['value']) && $var['value']!="");
        });
        $departure_cities = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_DEPARTURE_CITY && !is_null($var['value']) && $var['value']!="");
        });
        $foods = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_FOOD && !is_null($var['value']) && $var['value']!="");
        });
        $hotels = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_HOTEL && !is_null($var['value']) && $var['value']!="");
        });
        $star_ratings = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_STAR_RATING && !is_null($var['value']) && $var['value']!="");
        });
        $currencies = array_filter($fields,function($var){
            return ($var['type']==ServicesFieldsDefault::TYPE_CURRENCY && !is_null($var['value']) && $var['value']!="");
        });
        return compact('star_ratings','suppliers','airports','cities','countries','departure_cities','foods','hotels','currencies');
    }

    /**
     * Get values from MC api
     * @param array $fields
     * @param Tenants $tenant
     * @return array
     */
    public static function getMCValuesFields(array $fields,Tenants $tenant)
    {
        $data = static::mapFieldsMC($fields);
        $suppliers = Suppliers::find()->where(['in','id',array_column($data['suppliers'],'value')])->translate($tenant->language->iso)->asArray()->all();
        $airports = MCHelper::getAirportsById(array_column($data['airports'],'value'),$tenant->language->iso);
        $cities = MCHelper::getCityById(array_column($data['cities'],'value'),$tenant->language->iso);
        $countries = MCHelper::getCountryById(array_column($data['countries'],'value'),$tenant->language->iso);
        $departure_cities = MCHelper::getDepartureCityById(array_column($data['departure_cities'],'value'),$tenant->language->iso);
        $foods = MCHelper::getFoodOptionsById(array_column($data['foods'],'value'),$tenant->language->iso);
        $hotels = MCHelper::getHotelsOptionsById(array_column($data['hotels'],'value'),$tenant->language->iso);
        $star_ratings = MCHelper::getHotelCategoryById(array_column($data['star_ratings'],'value'),$tenant->language->iso);
        $currencies = MCHelper::getCurrencyById(array_column($data['currencies'],'value'),$tenant->language->iso);

        $data = compact('star_ratings','suppliers','airports','cities','countries','departure_cities','foods','hotels','currencies');
        foreach ($data as $i => $type)
        {
            $data[$i] = array_combine(array_column($type,'id'),$type);
        }
        return $data;
    }

    /**
     * Get values from MC api from multiple services
     * @param array $services
     * @param Tenants $tenant
     * @return array
     */
    public static function getMCValues(array $services,Tenants $tenant)
    {
        $fields_arr = array_column($services,'fields');
        $fields = [];
        foreach ($fields_arr as $f)
        {
            $fields = array_merge($fields,$f);
        }
        return static::getMCValuesFields($fields, $tenant);
    }

    /**
     * Normalize values
     * @param $values
     * @param $type
     * @param $id
     * @return array|null|string
     */
    public static function getTitles($values,$type,$id)
    {
        if($id == null)
            return null;
        switch ($type)
        {
            case \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS:
                return (isset($values['suppliers'][$id]['name']))?['id'=>$id,
                    'text'=>$values['suppliers'][$id]['name']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_AIRPORT:
                return (isset($values['airports'][$id]['title']))?['id'=>$id,
                    'text'=>$values['airports'][$id]['title'].' ('.$values['airports'][$id]['iata_code'].') - <i>'.$values['airports'][$id]['city_title'].', '.$values['airports'][$id]['country_title'].'</i>']:
                    ['id'=>$id, 'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_CITY:
                return (isset($values['cities'][$id]['title']))?['id'=>$id,
                    'text'=>$values['cities'][$id]['title']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_COUNTRY:
                return (isset($values['countries'][$id]['title']))?['id'=>$id,
                    'text'=>$values['countries'][$id]['title']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_DEPARTURE_CITY:
                return (isset($values['departure_cities'][$id]['title']))?['id'=>$id,
                    'text'=>$values['departure_cities'][$id]['title']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_FOOD:
                return (isset($values['foods'][$id]['title']))?['id'=>$id,
                    'text'=>$values['foods'][$id]['title']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_HOTEL:
                return (isset($values['hotels'][$id]['title']))?['id'=>$id,
                    'text'=>$values['hotels'][$id]['title'].' *'.$values['hotels'][$id]['hotel_category_title'].', '.$values['hotels'][$id]['country_title'].', '.$values['hotels'][$id]['city_title']]
                    :['id'=>$id,
                        'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_STAR_RATING:
                return (isset($values['star_ratings'][$id]['title']))?['id'=>$id,
                    'text'=>$values['star_ratings'][$id]['title']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_CURRENCY:
                return (isset($values['currencies'][$id]['iso_code']))?['id'=>$id,
                    'text'=>$values['currencies'][$id]['iso_code']]:['id'=>$id,
                    'text'=>$id];
                break;
            case \app\models\ServicesFieldsDefault::TYPE_COMMISSION:
                try{$data = json_decode($id,true);return $data;}catch (\Exception $e) {return $id;}
                break;
            case \app\models\ServicesFieldsDefault::TYPE_DATE:
            case \app\models\ServicesFieldsDefault::TYPE_DATE_STARTING:
            case \app\models\ServicesFieldsDefault::TYPE_DATE_ENDING:
                try{return DateTimeHelper::convertTimestampToDate($id);}catch (\Exception $e) {return $id;}
                break;
            break;
            default: return $id;
        }
    }

    /**
     * @param array $post
     * @param $services_links_model
     * @param $services_values_model
     * @param $services_additionals_model
     * @throws HttpException
     */
    public static function createService($post,$main_item_id,$tenant_id,$services_links_model,$services_values_model,$services_additionals_model) {
        $service = Services::find()->where(['id'=>intval($post['Service']['id'])])->one();
        if($service === null)
            throw new HttpException(400,'Service is not found!');
        $link = new $services_links_model();
        $link->{$services_links_model::MAIN_ITEM} = $main_item_id;
        $link->tenant_id = $tenant_id;
        $link->service_id = $post['Service']['id'];
        if($link->save())
        {
            $fields = $post['Service'];
            unset($fields['id']);
            $ids = array_keys($fields);
            $fs = ServicesFieldsDefault::find()->where(['in','id',$ids])->asArray()->all();
            $fs = array_combine(array_column($fs,'id'),$fs);

            foreach ($fields as $id=>$value)
            {
                if(is_array($value))
                    $value = json_encode($value);
                $rsvs = new $services_values_model();
                $rsvs->field_id = intval($id);
                $rsvs->link_id = $link->id;
                $rsvs->value = $value;
                if($fs[$id]['type'] == ServicesFieldsDefault::TYPE_DATE
                    || $fs[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_STARTING
                    || $fs[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_ENDING)
                    $rsvs->value = DateTimeHelper::convertDateToTimestamp($rsvs->value).'';
                $rsvs->service_id = intval($post['Service']['id']);
                $rsvs->save();
            }
            if(isset($post['ServiceAdditionals']))
            {
                $srvs = $post['ServiceAdditionals'];
                foreach ($srvs as $serv)
                {
                    $serv_id = intval($serv['id']);
                    unset($serv['id']);
                    $alink = new $services_additionals_model();
                    $alink->link_id = $link->id;
                    $alink->service_id = $serv_id;
                    if($alink->save()){
                        foreach ($serv as $id => $value)
                        {
                            if(is_array($value))
                                $value = json_encode($value);
                            $rsvs = new $services_values_model();
                            $rsvs->field_id = $id;
                            $rsvs->link_id = $link->id;
                            $rsvs->value = $value;
                            $rsvs->additional_link_id = $alink->id;
                            $rsvs->service_id = $serv_id;
                            $rsvs->save();
                        }
                    }
                }
            }
            return $link;
        }
        return false;
    }

    /**
     * @param $post
     * @param $main_item_id
     * @param $services_links_model
     * @param $services_values_model
     * @param $services_additionals_model
     * @throws HttpException
     */
    public static function updateService($post,$main_item_id,$tenant_id,$services_links_model,$services_values_model,$services_additionals_model)
    {
        $link = $services_links_model::find()->where([$services_links_model::MAIN_ITEM=>$main_item_id])
            ->andWhere(['id'=>intval($post['Service']['link_id'])])
            ->andWhere(['service_id'=>intval($post['Service']['id'])])
            ->andWhere(['tenant_id'=>intval($tenant_id)])->one();

        if($link === null)
            throw new HttpException(400,'Service is not found!');
        $fields = $post['Service'];
        unset($fields['id']);
        unset($fields['link_id']);
        $fields_info = ServicesFieldsDefault::find()->asArray()->all();
        $fields_info = array_combine(array_column($fields_info,'id'),$fields_info);
        foreach ($fields as $id => $value)
        {
            if(is_array($value))
                $value = json_encode($value);
            $record = $services_values_model::find()->where('additional_link_id IS NULL')
                ->andWhere(['service_id'=>intval($post['Service']['id'])])
                ->andWhere(['link_id'=>$link->id])
                ->andWhere(['field_id'=>intval($id)])->one();
            if(is_null($record)){
                $record = new $services_values_model();
                $record->service_id = intval($post['Service']['id']);
                $record->link_id = $link->id;
                $record->field_id = intval($id);
                $record->value = $value;
            }else{
                $record->value = $value;
            }
            if($fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE
                || $fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_STARTING
                || $fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_ENDING)
                $record->value = DateTimeHelper::convertDateToTimestamp($record->value).'';
            if($fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_PROFIT && (empty($record->value) || is_null($record->value)))
                $record->value = '0.00';
            if(is_null($record->value))
                $record->value = '';
            $record->save();
        }
        if(isset($post['ServiceAdditionals']))
        {
            $srvs = $post['ServiceAdditionals'];
            $additionals = $services_additionals_model::find()->where(['link_id'=>$link->id])->all();
            $old = [];
            foreach ($additionals as $additional)
                $old[$additional->id] = $additional;
            foreach ($srvs as $serv)
            {
                $serv_id = intval($serv['id']);
                if(isset($serv['type'])) {
                    $type = $serv['type'];
                    unset($serv['type']);
                } else $type = 0;
                unset($serv['id']);
                if(isset($serv['link_id'])){
                    $link_id = intval($serv['link_id']);
                    unset($serv['link_id']);
                    $record = $services_additionals_model::find()->where(['id'=>$link_id])->one();
                    if(!is_null($record)){
                        foreach ($serv as $id => $value)
                        {
                            if(is_array($value))
                                $value = json_encode($value);
                            $val = $services_values_model::find()->where(['link_id'=>$link->id])
                                ->andWhere(['additional_link_id'=>$link_id])
                                ->andWhere(['service_id'=>$serv_id])
                                ->andWhere(['field_id'=>intval($id)])->one();
                            if(is_null($val)) {
                                $val = new $services_values_model();
                                $val->additional_link_id = $link_id;
                                $val->service_id = $serv_id;
                                $val->link_id = $link->id;
                                $val->field_id = intval($id);
                                $val->value = $value;
                            }else{
                                $val->value = $value;
                            }
                            if($fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE
                                || $fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_STARTING
                                || $fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_DATE_ENDING)
                                $val->value = DateTimeHelper::convertDateToTimestamp($val->value).'';
                            if($fields_info[$id]['type'] == ServicesFieldsDefault::TYPE_PROFIT && (empty($val->value) || is_null($val->value)))
                                $val->value = '0.00';
                            if(is_null($val->value))
                                $val->value = '';
                            $val->save();
                        }
                        unset($old[$link_id]);
                    }
                } else{
                    $alink = new $services_additionals_model();
                    $alink->link_id = $link->id;
                    $alink->service_id = $serv_id;
                    $alink->type = $type;
                    if($alink->save()){
                        foreach ($serv as $id => $value)
                        {
                            if(is_array($value))
                                $value = json_encode($value);
                            $rsvs = new $services_values_model();
                            $rsvs->field_id = intval($id);
                            $rsvs->link_id = $link->id;
                            $rsvs->value = $value;
                            $rsvs->additional_link_id = $alink->id;
                            $rsvs->service_id = $serv_id;
                            $rsvs->save();
                        }
                    }
                }
            }
            foreach ($old as $r)
                $r->delete();
        }
        return $link;
    }

    /**
     * @param array $service
     * @return string
     */
    public static function getServicePrice(array $service) {
        if (!isset($service['fields']))
            $service['fields'] = [];
        $price = '0';
        $prices = array_filter($service['fields'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SERVICES_PRICE;
        });
        $prices = array_merge( $prices, array_filter($service['fields_def'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SERVICES_PRICE;
        }));
        if(!empty($prices) && isset(reset($prices)['value']))
            $price = reset($prices)['value'];

        return $price;
    }

    /**
     * @param array $service
     * @return string
     */
    public static function getServiceCurrency(array $service) {
        if (!isset($service['fields']))
            $service['fields'] = [];

        $currency = '';
        $currencies =  array_filter($service['fields'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_CURRENCY;
        });
        $currencies = array_merge($currencies, array_filter($service['fields_def'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_CURRENCY;
        }));
        if(!empty($currencies)&& isset(reset($currencies)['value']))
            $currency = reset($currencies)['value'];
        return $currency;
    }

    /**
     * @param array $service
     * @return array|string|ø
     */
    public static function getServiceReservation(array $service) {
        if (!isset($service['fields']))
            $service['fields'] = [];

        $reservation =  array_filter($service['fields'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_RESERVATION;
        });
        $reservation = array_merge($reservation, array_filter($service['fields_def'],function($var){
            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_RESERVATION;
        }));
        if(!empty($reservation)&& isset(reset($reservation)['value']))
            $reservation = reset($reservation);
        else
            return '';
        return $reservation['name'].': '.$reservation['value'];
    }

    /**
     * @param array $service
     * @param $currency
     * @return array
     */
    public static function getServiceHeader(array $service,$currency) {
        if (!isset($service['fields']))
            $service['fields'] = [];

        $in_header = array_filter($service['fields'],function($var){
            return $var['in_header'];
        });
        $in_header = array_merge($in_header, array_filter($service['fields_def'],function($var){
            return $var['in_header'];
        }));
        $header = [];
        foreach ($in_header as $i => $h)
        {
            if(isset($h['value']) && $h['value']!=="")
            {
                $header[$i] = $h['value'];

                switch ($h['type'])
                {
                    case \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS:
                    case \app\models\ServicesFieldsDefault::TYPE_AIRPORT:
                    case \app\models\ServicesFieldsDefault::TYPE_CITY:
                    case \app\models\ServicesFieldsDefault::TYPE_COUNTRY:
                    case \app\models\ServicesFieldsDefault::TYPE_DEPARTURE_CITY:
                    case \app\models\ServicesFieldsDefault::TYPE_FOOD:
                    case \app\models\ServicesFieldsDefault::TYPE_HOTEL:
                    case \app\models\ServicesFieldsDefault::TYPE_STAR_RATING:
                    case \app\models\ServicesFieldsDefault::TYPE_CURRENCY:
                        $header[$i] =$h['value']['text'];
                        break;
                    case \app\models\ServicesFieldsDefault::TYPE_COMMISSION:
//                        $temp = json_decode($header[$i],true);
                        if(isset($h['type']))
                        {
                            if($h['type'] == 'per')
                                $header[$i] = $h['value'].'%';
                            else
                                $header[$i] = $h['value']['value'].' '.((!empty($currency))?$currency['text']:"");
                        }
                        else
                            $header[$i] = $header[$i].'%';
                        break;
                    case \app\models\ServicesFieldsDefault::TYPE_SWITCH:
                    case \app\models\ServicesFieldsDefault::TYPE_DROPDOWN:
                        $list = [];
                        if(!empty($h['variants']))
                            $list = json_decode($h['variants'],true);
                        if(isset($list[(int)$h['value']]))
                            $header[$i] =  $list[(int)$h['value']];
                        break;
                    case \app\models\ServicesFieldsDefault::TYPE_MULTISELECT:
                        $list = [];
                        if(!empty($h['variants']))
                            $list = json_decode($h['variants'],true);
                        $values = [];
                        if(!empty($h['value']))
                            $values = json_decode($h['value'],true);
                        $result = [];
                        foreach ($values as $value)
                            if(isset($list[(int)$value]))
                                $result[] = $list[(int)$value];
                        $header[$i] = implode(', ',$result);
                        break;

                }
                if($h['is_bold'])
                    $header[$i] = '<b>'.$header[$i].'</b>';
            }
        }
        $header = implode(' / ',$header);
        return $header;
    }

    /**
     * @param $link
     * @param $lang
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getTouristsFromOrdersService($link,$lang)
    {
        $order_service  = OrdersServicesLinks::find()->where(['id'=>(int)$link])->one();

        if($order_service === null)
            return false;

        $tourists = OrdersTourists::find()->where(['link_id' => $order_service->id])->orderBy(['id' => SORT_ASC])->asArray()->all();

        $contact_ids = array_unique(array_column($tourists,'contact_id'));
        $contacts = Contacts::find()->where(['in', 'id', $contact_ids])->asArray()->all();
        $contacts = array_combine(array_column($contacts, 'id'), $contacts);

        $ids = array_unique(array_column($tourists,'passport_id'));
        $passports = ContactsPassports::find()->where(['in', 'id', $ids])->asArray()->all();
        foreach ($passports as $pk => $pv) {
            if(!empty($passports[$pk]['nationality'])) {
                $passports[$pk]['country'] = MCHelper::getCountryById($passports[$pk]['nationality'], \Yii::$app->user->identity->tenant->language->iso)[0]['title'];
            } else {
                $passports[$pk]['country'] = null;
            }
        }
        $passports = array_combine(array_column($passports, 'id'), $passports);

        $passport_type = PassportsTypes::find()
            ->where(['in', 'id', array_column($passports, 'type_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $passport_type = array_combine(array_column($passport_type, 'id'), $passport_type);

        $tourists = array_map(function($item) use ($contacts, $passports, $passport_type) {
            $item['order_tourist_id'] = $item['id'];
            unset($item['id']);

            if ( ! empty($item['contact_id'])) {
                $item['id'] = $item['contact_id'];
                $item['first_name'] = $contacts[$item['contact_id']]['first_name'];
                $item['last_name'] = $contacts[$item['contact_id']]['last_name'];

                if ( ! empty($item['passport_id'])) {
                    $passports[$item['passport_id']]['date_limit'] = DateTimeHelper::convertTimestampToDate($passports[$item['passport_id']]['date_limit']);
                    $passports[$item['passport_id']]['birth_date'] = DateTimeHelper::convertTimestampToDate($passports[$item['passport_id']]['birth_date']);
                    $passports[$item['passport_id']]['issued_date'] = DateTimeHelper::convertTimestampToDate($passports[$item['passport_id']]['issued_date']);
                    $passports[$item['passport_id']]['type']       = [
                        'id'    => $passports[$item['passport_id']]['type_id'],
                        'value' => $passport_type[$passports[$item['passport_id']]['type_id']]['value']
                    ];

                    $item['passport'] = $passports[$item['passport_id']];
                }
            } else {
                $item['empty_tourist'] = true;
            }

            return $item;
        }, $tourists);

        return $tourists;
    }

    /**
     * @param $service
     * @param $field_type
     * @return null|value
     */
    public static function getFieldFromService($service,$field_type)
    {
        if (!isset($service['fields']))
            $service['fields'] = [];

        $field =  array_filter($service['fields'],function($var) use ($field_type){
            return $var['type'] == $field_type;
        });
        $field = array_merge($field, array_filter($service['fields_def'],function($var) use ($field_type){
            return $var['type'] == $field_type;
        }));

        if(empty($field))
            return null;
        $field = reset($field);

        if(!isset($field['value']))
            return null;

        return $field['value'];
    }

    public static function getFieldInfoFromService($service,$field_type)
    {
        if (!isset($service['fields']))
            $service['fields'] = [];

            $field =  array_filter($service['fields'],function($var) use ($field_type){
                return $var['type'] == $field_type;
            });
            $field = array_merge($field, array_filter($service['fields_def'],function($var) use ($field_type){
                return $var['type'] == $field_type;
            }));

            if(empty($field))
                return null;
            $field = reset($field);

            return $field;
    }
}