<?php
/**
 * CompaniesUtility.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\utilities\companies;


use app\helpers\LogHelper;
use app\models\Companies;
use app\models\CompaniesTags;
use app\models\Tags;

class CompaniesUtility
{
    private static $keys_mapping = [
        'companiesEmails' => 'emails',
        'companiesPhones' => 'phones',
        'companyAddress' => 'company_address',
        //'companyContactSource' => 'source',
        'companyKindActivity' => 'kind_activity',
        'companiesMessengers' => 'messengers',
        'companiesSites' => 'sites',
        'companiesSocials' => 'socials',
        'companiesTags' => 'tags',
        'companyStatus' => 'status',
        'companyType' => 'type'
    ];

    private static $relations_mapping = [
        'phones' => 'app\models\CompaniesPhones',
        'emails' => 'app\models\CompaniesEmails',
        'socials' => 'app\models\CompaniesSocials',
        'messengers' => 'app\models\CompaniesMessengers',
        'sites' => 'app\models\CompaniesSites'
    ];

    /**
     * @param $data
     *
     * @return array
     */
    public static function prepareViewArray($data)
    {
        $new_data = [];

        foreach ($data as $key => $value)
            $new_data[isset(static::$keys_mapping[$key]) ? static::$keys_mapping[$key] : $key] = $value;

        $new_data['responsible'] = static::prepareResponsible($data['responsible']);
        $new_data['status'] = [
            'id' => $data['companyStatus']['id'],
            'value' => $data['companyStatus']['value'],
            'color' => $data['companyStatus']['color_type']
        ];
        $new_data['type'] = [
            'id' => $data['companyType']['id'],
            'value' => $data['companyType']['value']
        ];
        $new_data['kind_activity'] = [
            'id' => $data['companyKindActivity']['id'],
            'value' => $data['companyKindActivity']['value']
        ];
        $new_data['tags'] = static::prepareTags($new_data['tags']);

        return $new_data;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private static function prepareResponsible($data)
    {
        return [
            'id' => $data['id'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'full_name' => trim($data['first_name'] . ' ' . $data['last_name']),
            'photo' => !empty($data['photo']) ? $data['photo'] : 'img/profile_default.png'
        ];
    }

    private static function prepareTags($tags)
    {
        $new_tags = [];

        foreach ($tags as $tag) {
            $tag_query = Tags::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

            if (\Yii::$app->user->identity->tenant->system_tags)
                $tag_query->orWhere(['type' => 'system']);

            $tag_query->andWhere(['id' => $tag['tag_id']]);
            $tag_query->translate(\Yii::$app->user->identity->tenant->language->iso);
            $tag_query = $tag_query->one();

            if ($tag_query === null)
                continue;

            $new_tags[] = [
                'id' => $tag_query->id,
                'name' => $tag_query->value,
                'link' => '#'
            ];
        }

        return $new_tags;
    }

    /**
     * @param $company_id
     *
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public static function saveCompanyData($company_id)
    {
        $company_data = \Yii::$app->request->post('contacts', false);
        $company_tags = \Yii::$app->request->post('tags', false);

        $company = Companies::findOne($company_id);

        if ($company === null)
            return false;

        if ($company_data) {
            $company->name = $company_data['name'];
            $company->save();

            foreach ($company_data as $key => $data) {
                if (is_array($data))
                    static::saveCompanyContacts($company_id, $key, $data);
            }
        }

        static::saveCompanyTags($company_id, $company_tags);

        return true;
    }

    /**
     * @param $company_id
     * @param $relation_name
     * @param $data
     *
     * @throws \Exception
     * @throws \Throwable
     */
    private static function saveCompanyContacts($company_id, $relation_name, $data)
    {
        $contacts = static::$relations_mapping[$relation_name]::find()
            ->select('id')
            ->where(['company_id' => $company_id])
            ->column();
        $contacts_id = array_column($data, 'id');
        $contacts_diff = array_diff($contacts, $contacts_id);

        if (!empty($contacts_diff)) {
            foreach ($contacts_diff as $cid) {
                /** @var $companies_contact \yii\db\ActiveRecord */
                $companies_contact = static::$relations_mapping[$relation_name]::findOne(['id' => $cid]);

                if ($companies_contact !== null)
                    $companies_contact->delete();
            }
        }

        foreach ($data as $contact) {
            if (empty($contact['id']) && empty($contact['value']))
                continue;

            static::saveContact($company_id, $contact, $relation_name);
        }
    }

    /**
     * @param      $company_id
     * @param      $contact
     * @param      $relation
     *
     * @return bool
     */
    private static function saveContact($company_id, $contact, $relation)
    {
        /** @var $model \yii\db\ActiveRecord */
        $model = empty($contact['id']) ?
            new static::$relations_mapping[$relation]() :
            static::$relations_mapping[$relation]::findOne($contact['id']);
        $model->company_id = $company_id;
        $model->type_id = $contact['type_id'];
        $model->value = $contact['value'];

        $logItemId = $model->save();

        static::touchCompany($company_id, $logItemId);

        return $model->id;
    }

    /**
     * @param $company_id
     * @param $company_tags
     *
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    private static function saveCompanyTags($company_id, $company_tags)
    {
        static::clearCompanyTags($company_id, $company_tags);

        if (empty($company_tags))
            return false;

        $tags = Tags::find()->select('id')
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        if (\Yii::$app->user->identity->tenant->system_tags)
            $tags->orWhere(['type' => 'system']);

        $tags = $tags->column();

        /** @var array $company_tags */
        foreach ($company_tags as $tag) {
            $c_tag = CompaniesTags::findOne(['company_id' => $company_id, 'tag_id' => $tag]);

            if ($c_tag !== null)
                continue;

            if (in_array($tag, $tags)) {
                static::saveTag($company_id, $tag);
            } else {
                $model = new Tags();
                $model->type = Tags::TYPE_CUSTOM;
                $model->tenant_id = \Yii::$app->user->identity->tenant->id;
                $model->value = $tag;

                $logItemId = $model->saveWithLang(\Yii::$app->user->identity->tenant->language->iso);

                if (!$logItemId)
                    throw new \Exception('Tag not save!');

                static::touchCompany($company_id, $logItemId);
                static::saveTag($company_id, $model->id);
            }
        }
    }

    /**
     * @param $company_id
     * @param $company_tags
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private static function clearCompanyTags($company_id, $company_tags)
    {
        if ($company_tags !== false) {
            $tags = CompaniesTags::find()
                ->select('tag_id')
                ->where(['company_id' => $company_id])
                ->column();

            $tags_diff = array_diff($tags, $company_tags);

            if (!empty($tags_diff)) {
                foreach ($tags_diff as $tid) {
                    $companies_tags = CompaniesTags::findOne(['company_id' => $company_id, 'tag_id' => $tid]);

                    if ($companies_tags !== null)
                        $companies_tags->delete();
                }
            }
        }
    }

    /**
     * @param $company_id
     * @param $tag_id
     *
     * @return int
     * @throws \Exception
     */
    private static function saveTag($company_id, $tag_id)
    {
        $model = new CompaniesTags();

        $model->company_id = $company_id;
        $model->tag_id = (int) $tag_id;

        $logItemId = $model->save();

        if (!$logItemId)
            throw new \Exception('Tag not save!');

        static::touchCompany($company_id, $logItemId);

        return $model->id;
    }

    /**
     * @param $company_id
     * @param $logItemId
     *
     * @return bool
     */
    private static function touchCompany($company_id, $logItemId)
    {
        if (is_numeric($logItemId)) {
            $forceCompany = Companies::findOne($company_id);

            if ($forceCompany === null)
                return false;

            $forceCompany->touch('updated_at');
            $forceCompany->force_update = true;
            $logMainItemId = $forceCompany->save();
            LogHelper::setGroupId($logItemId, $logMainItemId);

            return true;
        }

        return false;
    }
}