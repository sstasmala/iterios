<?php
/**
 * TimeLineUtility.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\utilities\timeline;

use app\helpers\DateTimeHelper;
use app\helpers\TranslationHelper;
use app\models\ContactsTimelineView;
use app\models\Log;
use app\models\TasksTypes;
use app\utilities\logs\LogsUtility;
use yii\helpers\HtmlPurifier;

class TimeLineUtility
{
    const PARENT_CONTACT = 'contact';

    private static $template_demo = [
        'time' => '',
        'title' => '',
        'content' => '',
        'icon' => [
            'class' => 'la la-eye',
            'color' => 'warning'
        ],
        'buttons' => [
            'remove' => false,
            'edit' => false,
        ],
        'avatar' => [
            'src' => '',
            'link' => '',
            'title' => ''
        ],
    ];

    /**
     * @param     $id
     *
     * @param int $offset
     *
     * @param     $text_search
     *
     * @return array
     * @throws \Exception
     */
    public static function getContactTimeLine($id, $offset = 0, $text_search = null)
    {
        $timelines = ContactsTimelineView::find();
        $timelines->where(['contact_id' => $id])
            ->orWhere(['and', ['table_name' => 'contacts', 'item_id' => $id]]);

        if (!empty($text_search))
            $timelines->andWhere(['or', ['ilike', 'value', $text_search], ['ilike', 'data', $text_search]]);

        $timelines = $timelines->with('user', 'updatedUser')
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)->offset($offset)
            ->asArray()->all();

        $logs = array_filter($timelines, function ($item){
            return $item['type'] === ContactsTimelineView::TYPE_LOG;
        });
        $tasks = array_filter($timelines, function ($item){
            return $item['type'] === ContactsTimelineView::TYPE_TASK;
        });

        $logsGroup = Log::find()
            ->where(['group_id' => array_column($logs, 'id')]);

        if (!empty($text_search))
            $logsGroup->andWhere(['ilike', 'data', $text_search]);

        $logsGroup = $logsGroup->with('user')
            ->asArray()->all();

        $logs = array_merge($logs, $logsGroup);
        $tlines = LogsUtility::prepareLog($logs, \Yii::$app->user->identity->tenant->language->iso, static::$template_demo);

        $task_type = TasksTypes::find()
            ->where(['id' => array_column($tasks, 'type_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();
        $task_type = array_combine(array_column($task_type, 'id'), $task_type);

        foreach ($timelines as $timeline) {
            if ($timeline['type'] === ContactsTimelineView::TYPE_REQUEST_NOTE || $timeline['type'] === ContactsTimelineView::TYPE_CONTACT_NOTE)
                $tlines[] = static::prepareNote($timeline);

            if ($timeline['type'] === ContactsTimelineView::TYPE_TASK)
                $tlines[] = static::prepareTask($timeline, $task_type);
        }

        usort($tlines, function($a, $b) {
            if ($a['time_unix'] === $b['time_unix'])
                return 0;

            return ($a['time_unix'] > $b['time_unix']) ? -1 : 1;
        });

        return $tlines;
    }

    /**
     * @param $note
     *
     * @return array
     */
    public static function prepareNote($note)
    {
        $template = static::$template_demo;
        $template['id'] = $note['id'];
        $template['type_note'] = isset($note['request_id']) && $note['request_id'] !== 0 ? 'request' : 'other';
        $template['time_unix'] = $note['created_at'];
        $template['time'] = '<span class="moment-js invisible" data-format="unix">'.$template['time_unix'].'</span>';
        $template['icon'] = [
            'class' => 'la la-sticky-note',
            'color' => 'info'
        ];
        $template['buttons'] = [
            'remove' => true,
            'edit' => true
        ];
        $template['avatar'] = [
            'src' =>  \Yii::$app->params['baseUrl'] . '/' . (!empty($note['user']['photo']) ? $note['user']['photo'] : 'img/profile_default.png'),
            'link' => '#',
            'title' => $note['user']['first_name'] . ' ' . $note['user']['last_name']
        ];
        $template['content'] = HtmlPurifier::process($note['value']);
        $template['title'] = $template['avatar']['title'] .
            ' <span class="m--font-normal">'.TranslationHelper::getTranslation('timeline_added', \Yii::$app->user->identity->tenant->language->iso) . '</span> ';

        if (isset($note['request_id']) && $note['request_id'] !== 0) {
            $template['title'] .= TranslationHelper::getTranslation('timeline_note_in', \Yii::$app->user->identity->tenant->language->iso) .
                ' <a href="'.\Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/requests/view?id='.$note['request_id'].'" class="m-link">'.
                TranslationHelper::getTranslation('timeline_request', \Yii::$app->user->identity->tenant->language->iso).' #'.$note['request_id'].'</a>';
        } else {
            $template['title'] .= TranslationHelper::getTranslation('timeline_note', \Yii::$app->user->identity->tenant->language->iso);
        }

        return $template;
    }

    /**
     * @param $task
     *
     * @param $task_type
     * @return array
     */
    public static function prepareTask($task, $task_type)
    {
        $template = static::$template_demo;
        $template['id'] = $task['id'];
        $template['time_unix'] = ($task['status'] === true ? $task['updated_at'] : $task['created_at']);
        $template['time'] = '<span class="moment-js invisible" data-format="unix">'.$template['time_unix'].'</span>';
        $template['icon'] = [
            'class' => 'la la-check',
            'color' => 'success'
        ];

        if ($task['status']) {
            $template['avatar'] = [
                'src'   => \Yii::$app->params['baseUrl'] . '/' . (! empty($task['updatedUser']['photo']) ? $task['updatedUser']['photo'] : 'img/profile_default.png'),
                'link'  => '#',
                'title' => $task['updatedUser']['first_name'] . ' ' . $task['updatedUser']['last_name']
            ];
        } else {
            $template['avatar'] = [
                'src'   => \Yii::$app->params['baseUrl'] . '/' . (! empty($task['user']['photo']) ? $task['user']['photo'] : 'img/profile_default.png'),
                'link'  => '#',
                'title' => $task['user']['first_name'] . ' ' . $task['user']['last_name']
            ];
        }

        $template['content'] = '<b>'. TranslationHelper::getTranslation('timeline_task_name', \Yii::$app->user->identity->tenant->language->iso)
            . ':</b> ' . $task['title'] . '<br/>';

        //if ($task['status'])
            //$template['content'] .= TranslationHelper::getTranslation('timeline_task_closed', \Yii::$app->user->identity->tenant->language->iso) . ' - ' . DateTimeHelper::convertTimestampToDateTime($task['updated_at']) . '<br/>';

        if (isset($task_type[$task['type_id']]))
            $template['content'] .= '<b>'. TranslationHelper::getTranslation('timeline_task_type', \Yii::$app->user->identity->tenant->language->iso) . ':</b> ' . $task_type[$task['type_id']]['value'];

        if (!empty($task['description']))
            $template['content'] .= '<br/><br/>' . HtmlPurifier::process($task['description']);

        $template['title'] = $template['avatar']['title'] .
            ' <span class="m--font-normal">'.TranslationHelper::getTranslation($task['status'] ? 'timeline_closed' : 'timeline_added', \Yii::$app->user->identity->tenant->language->iso) . '</span> ';
        $template['title'] .= TranslationHelper::getTranslation('timeline_task', \Yii::$app->user->identity->tenant->language->iso);

        return $template;
    }

    /**
     * @param     $id
     *
     * @param int $offset
     *
     * @return array
     */
    public static function getContactNotes($id, $offset = 0)
    {
        $notes = ContactsTimelineView::find()
            ->where(['type' => ContactsTimelineView::TYPE_CONTACT_NOTE])
            ->orWhere(['type' => ContactsTimelineView::TYPE_REQUEST_NOTE])
            ->andWhere(['contact_id' => $id])
            ->with('user')
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)->offset($offset)
            ->asArray()->all();

        $allNotes = [];

        foreach ($notes as $note) {
            $allNotes[] = static::prepareNote($note);
        }

        return $allNotes;
    }

    /**
     * @param $id
     * @param int $offset
     * @return array
     */
    public static function getContactTasks($id, $offset = 0)
    {
        $tasks = ContactsTimelineView::find()
            ->where(['type' => ContactsTimelineView::TYPE_TASK])
            ->andWhere(['contact_id' => $id])
            ->with('user', 'updatedUser', 'taskType')
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)->offset($offset)
            ->asArray()->all();

        $task_type = TasksTypes::find()
            ->where(['id' => array_column($tasks, 'type_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();
        $task_type = array_combine(array_column($task_type, 'id'), $task_type);

        $allTasks = [];

        foreach ($tasks as $task) {
            $allTasks[] = static::prepareTask($task, $task_type);
        }

        return $allTasks;
    }
}