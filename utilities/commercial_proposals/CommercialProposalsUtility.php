<?php
/**
 * CommercialProposalsUtility.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\utilities\commercial_proposals;


use app\helpers\DateTimeHelper;
use app\helpers\PreviewShortcodesHelper;
use app\models\CommercialProposals;
use app\models\CommercialProposalsServices;
use app\models\DeliveryEmailTemplates;
use app\models\DeliveryPlaceholders;
use app\models\DeliveryTypes;
use app\models\RequestsServicesValues;
use app\models\ServicesFieldsDefault;
use app\models\UiTranslations;
use app\utilities\services\ServicesUtility;
use yii\web\NotFoundHttpException;

class CommercialProposalsUtility
{
    /**
     * @param $type
     * @param $services_ids
     *
     * @return bool|string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public static function createProposal($type, $services_ids) {
        if (!\in_array($type, CommercialProposals::TYPES, true))
            throw new NotFoundHttpException('This is Type Commercial Proposal is not found!');

        switch ($type) {
            case  CommercialProposals::TYPE_LINK:
                $proposal = static::proposalLink($type, $services_ids);
                static::saveCommercialProposalServices($proposal, $services_ids);

                return $proposal;

                break;
            case CommercialProposals::TYPE_SMS:
                break;
            case CommercialProposals::TYPE_EMAIL:
                break;
        }

        return false;
    }

    private static function saveCommercialProposalServices(CommercialProposals $proposal, $services_ids)
    {
        foreach ($services_ids as $services_id) {
            $model = new CommercialProposalsServices();
            $model->commercial_proposal_id = $proposal->id;
            $model->request_service_link_id = $services_id;
            $model->save();
        }
    }

    /**
     * @param $type
     *
     * @param $services_ids
     *
     * @return \app\models\CommercialProposals|bool
     * @throws \yii\base\Exception
     */
    private static function proposalLink($type, $services_ids)
    {
        $model = new CommercialProposals();
        $model->type = $type;
        $model->link_code = \Yii::$app->security->generateRandomString(10);

        try {
            $template = static::getTemplate();

            $model->name = $template->subject;
            //$model->template = self::prepareTemplate($template, $services_ids);
            $model->template = $template->body;

            if ($model->save())
                return $model;
        } catch (\Exception $e) {}

        return false;
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    private static function getTemplate(): DeliveryEmailTemplates
    {
        $type = DeliveryTypes::find()
            ->where(['type' => DeliveryTypes::TYPE_EMAIL_OFFER])
            ->one();

        if ($type === null)
            throw new NotFoundHttpException('Commercial Proposal Template type is not found!');

        $template = DeliveryEmailTemplates::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->where(['and',
                ['delivery_type_id' => $type->id],
                ['status' => true],
                ['is_default' => true],
                ['type' => DeliveryEmailTemplates::PUBLIC_TYPE],
                ['tenant_id' => \Yii::$app->user->identity->tenant->id]
            ])->one();

        if ($template !== null)
            return $template;

        $template = DeliveryEmailTemplates::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->where(['and',
                ['delivery_type_id' => $type->id],
                ['status' => true],
                ['is_default' => true],
                ['type' => DeliveryEmailTemplates::SYSTEM_TYPE]
            ])->one();

        if ($template === null)
            throw new NotFoundHttpException('Commercial Proposal Template is not found!');

        return $template;
    }

    /**
     * @param \app\models\DeliveryEmailTemplates $template
     *
     * @param                                    $services_ids
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    // private static function prepareTemplate(DeliveryEmailTemplates $template, $services_ids) {
    //     $placeholders = DeliveryPlaceholders::find()
    //         ->asArray()->all();
    //
    //     if (empty($placeholders))
    //         return $template->body;
    //
    //     $ph_services = array_filter($placeholders, function ($item) {
    //         return $item['module'] === DeliveryPlaceholders::MODULE_SERVICE_KEY;
    //     });
    //
    //     $services_schema = [];
    //
    //     foreach ($services_ids as $services_id) {
    //         $services_temp_schema = static::ServicesPlaceholderHandling($ph_services, $services_id);
    //
    //         $services_schema[] = $services_temp_schema;
    //     }
    //
    //     $schema['services_placeholders'] = $services_schema;
    //
    //     return PreviewShortcodesHelper::renderCommercialProposals($schema, $template->body);
    // }

    // private static function ServicesPlaceholderHandling(array $placeholder, int $services_id): array
    // {
    //     if (empty($placeholder))
    //         return [];
    //
    //     $service_fields_id = array_column($placeholder, 'path');
    //     $placeholder = array_combine($service_fields_id, array_column($placeholder, 'short_code'));
    //
    //     $service_fields = ServicesFieldsDefault::find()
    //         ->where(['in', 'id', array_diff($service_fields_id, [''])])
    //         ->asArray()->all();
    //     $service_fields = array_combine(array_column($service_fields, 'id'), $service_fields);
    //
    //     $requests_services_values = RequestsServicesValues::find()
    //         ->where(['in', 'link_id', $services_id])
    //         ->andWhere(['in', 'field_id', array_diff($service_fields_id, [''])])
    //         ->asArray()->all();
    //
    //     array_walk($requests_services_values, function (&$item) use ($service_fields) {
    //         $data = $service_fields[$item['field_id']];
    //         $data['value'] = $item['value'];
    //
    //         $item = $data;
    //     });
    //
    //     /** @var array $requests_services_values */
    //     $MCValues = ServicesUtility::getMCValuesFields($requests_services_values, \Yii::$app->user->identity->tenant);
    //
    //     $service_values = [];
    //
    //     foreach ($requests_services_values as $item)
    //         $service_values[$item['id']][] = static::getTitles($MCValues, $item['type'], $item['value']);
    //
    //     $schema = [];
    //
    //     foreach ($service_values as $key => $items)
    //         $schema[$placeholder[$key]] = implode(', ', $items);
    //
    //     return $schema;
    // }

    /**
     * Normalize values
     * @param $values
     * @param $type
     * @param $id
     * @return array|null|string
     */
    // private static function getTitles($values,$type,$id)
    // {
    //     if ($id === null)
    //         return null;
    //
    //     switch ($type)
    //     {
    //         case \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS:
    //             return $values['suppliers'][$id]['name'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_AIRPORT:
    //             return isset($values['airports'][$id]['title'])
    //                 ? $values['airports'][$id]['title'].' ('.$values['airports'][$id]['iata_code'].') - <i>'.$values['airports'][$id]['city_title'].', '.$values['airports'][$id]['country_title'].'</i>'
    //                 : $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_CITY:
    //             return $values['cities'][$id]['title'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_COUNTRY:
    //             return $values['countries'][$id]['title'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_DEPARTURE_CITY:
    //             return $values['departure_cities'][$id]['title'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_FOOD:
    //             return $values['foods'][$id]['title'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_HOTEL:
    //             return isset($values['hotels'][$id]['title'])
    //                 ? $values['hotels'][$id]['title'].' *'.$values['hotels'][$id]['hotel_category_title'].', '.$values['hotels'][$id]['country_title'].', '.$values['hotels'][$id]['city_title']
    //                 : $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_STAR_RATING:
    //             return $values['star_ratings'][$id]['title'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_CURRENCY:
    //             return $values['currencies'][$id]['iso_code'] ?? $id;
    //             break;
    //         case \app\models\ServicesFieldsDefault::TYPE_DATE:
    //         case \app\models\ServicesFieldsDefault::TYPE_DATE_STARTING:
    //         case \app\models\ServicesFieldsDefault::TYPE_DATE_ENDING:
    //             return DateTimeHelper::convertTimestampToDate($id);
    //             break;
    //         default: return $id;
    //     }
    // }
}