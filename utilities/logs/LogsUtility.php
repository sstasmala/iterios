<?php
/**
 * LogsUtility.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\utilities\logs;


use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\helpers\TranslationHelper;
use app\models\Tags;
use app\models\User;

class LogsUtility
{
    private static $template;

    /**
     * @param        $logs
     * @param string $lang
     *
     * @param        $template
     *
     * @return array
     * @throws \Exception
     */
    public static function prepareLog($logs, $lang = 'en', $template)
    {
        static::$template = $template;

        $logMessages = [];
        $i = 0;
        $isAddedColumn = false;
        foreach ($logs as $log) {
            $values = json_decode($log['data'], true);

            $log['created_by'] = isset($values['userName']) ? $values['userName'] : $log['user']['first_name'] . ' ' . $log['user']['last_name'];

            if (null !== $values['after']) {
                if($log['table_name'] === 'contacts_notes'){
                    $log = self::formationLog($log, $lang);
                    $tMessage = self::formationLogMessages($i, $log);
                    $logMessages[$i] = $tMessage;
                    $i++;
                    continue;
                }
                $log = self::formationLog($log, $lang);
                $logMessages[$i] = self::formationLogMessages($i, $log);
                if (count(array_keys($values['after'])) === 1) {

                    $array = array_slice($values['after'], 0, 1);
                    $array_key = array_keys($array);
                    if (!self::isNeedColumn($array_key[0])) {
                        continue;
                    }
                    $element_first = array_shift($array);
                    $data = self::mappingValueColumn($array_key[0], $element_first, $lang, $log['table_name_original']);
                    $logMessages[$i] = self::formationLogMessagesWithOneItem($i, $log);
                    $logMessages[$i]['content'] .= ' <b>' . $data['column'] . '</b> - ' . $data['value'] . '<br>';
                } else {
                    if (self::isAddedColumn($log['table_name_original'])) {
                        $isAddedColumn = true;
                        $values['after'] = [$log['table_name_original'] => $values['after']['value']];
                    }
                    if (self::isPassportVisa($log['table_name_original'],$log['action_original'])) {
                        $values = self::formationPassportVisa($log['table_name_original'], $values, 'after');
                    }
                    if ($log['table_name_original'] === 'contacts_tags') {
                        $isAddedColumn = true;
                        $values['after'] = [$log['table_name_original'] => $values['after']['tag_id']];
                    }

                    foreach ($values['after'] as $column => $value) {
                        if (!self::isNeedColumn($column)) {
                            continue;
                        }

                        if (null !== $value) {
                            $data = self::mappingValueColumn($column, $value, $lang, $log['table_name_original']);
                            if ($isAddedColumn == true) {
                                $logMessages[$i] = self::formationLogMessagesWithOneItem($i, $log);
                                $isAddedColumn = false;
                            }
                            $logMessages[$i]['content'] .= ' ' . $data['column'] . ' - ' . $data['value'] . '<br>';
                        }
                    }
                }
            } else {
                if ($log['action'] === 'delete' || null !== $values['previous']) {
                    $log = self::formationLog($log, $lang);
                    $logMessages[$i] = self::formationLogMessages($i, $log);

                    if (self::isPassportVisa($log['table_name_original'],$log['action_original'])) {
                        $values = self::formationPassportVisa($log['table_name_original'], $values, 'previous');
                    }
                    if ($log['table_name_original'] === 'contacts_tags') {
                        $isAddedColumn = true;
                        $values['previous'] = [$log['table_name_original'] => $values['previous']['tag_id']];
                    }
                    foreach ($values['previous'] as $column => $value) {
                        if(null !== $value) {
                            $data = self::mappingValueColumn($column, $value, $lang, $log['table_name_original']);
                            if ($isAddedColumn === true) {
                                $logMessages[$i] = self::formationLogMessagesWithOneItem($i, $log);
                                $logMessages[$i]['content'] .= ' ' . $data['column'] . ' - ' . $data['value'] . '<br>';
                                $isAddedColumn = false;
                            } else {
                                $logMessages[$i]['content'] .= ' ' . $data['column'] . ' - ' . $log['action'] . ' ' . $data['value'] . '<br>';
                            }
                        }
                    }
                }
                $i++;
                continue;
            }
            if (null === $logMessages[$i]['content']) {
                unset($logMessages[$i]);
            }
            $i++;
        }

//        if (count($logMessages) === 1 && $logMessages[0]['content'] === null)
//            return [];

        return $logMessages;
    }

    /**
     * @param $tableName
     *
     * @return bool
     */
    private static function isAddedColumn($tableName)
    {
        $arr = ['contacts_emails', 'contacts_messengers', 'contacts_phones', 'contacts_sites', 'contacts_socials'];
        if (in_array($tableName, $arr)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $tableName
     * @param $action
     *
     * @return bool
     */
    private static function isPassportVisa($tableName,$action)
    {
        $tableNameArr = ['contacts_passports', 'contacts_visas'];
        $actionArr = ['create', 'delete'];
        if (in_array($tableName, $tableNameArr) && in_array($action, $actionArr)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $tableName
     * @param $values
     * @param $action
     *
     * @return mixed
     */
    private static function formationPassportVisa($tableName, $values, $action)
    {
        if ($tableName === 'contacts_passports') {
            $value = $values[$action]['serial'];
            unset($values);
            $values[$action]['serial'] = $value;
            return $values;
        } else {
            $value = $values[$action]['country'];
            unset($values);
            $values[$action]['country'] = $value;
            return $values;
        }
    }

    /**
     * @param $log
     * @param $lang
     *
     * @return mixed
     */
    private static function formationLog($log, $lang)
    {
        $log['action_original'] = $log['action'];
        $log['action'] = self::mappingAction($log['action'], $lang);
        $log['table_name_original'] = $log['table_name'];
        $log['table_name'] = self::mappingTableName($log['table_name'], $lang);
        return $log;
    }

    /**
     * @param $i
     * @param $log
     *
     * @return mixed
     */
    private static function formationLogMessages($i, $log)
    {
        $template = static::$template;
        $template['time_unix'] = $log['created_at'];
        $template['time'] = '<span class="moment-js invisible" data-format="unix">'.$template['time_unix'].'</span>';
        $template['avatar'] = [
            'src' =>  \Yii::$app->params['baseUrl'] . '/' . (!empty($log['user']['photo']) ? $log['user']['photo'] : 'img/profile_default.png'),
            'link' => '#',
            'title' => $log['created_by']
        ];
        $template['content'] = '';
        $template['title'] = $log['created_by'] .
            ' <span class="m--font-normal">' . $log['action'] . '</span> ' . $log['table_name'];

        $logMessages[$i] = $template;
        $logMessages[$i]['content'] = null;

        return $logMessages[$i];
    }

    /**
     * @param $i
     * @param $log
     *
     * @return mixed
     */
    private static function formationLogMessagesWithOneItem($i, $log)
    {
        $template = static::$template;
        $template['time_unix'] = $log['created_at'];
        $template['time'] = '<span class="moment-js invisible" data-format="unix">'.$template['time_unix'].'</span>';
        $template['avatar'] = [
            'src' =>  \Yii::$app->params['baseUrl'] . '/' . (!empty($log['user']['photo']) ? $log['user']['photo'] : 'img/profile_default.png'),
            'link' => '#',
            'title' => $log['created_by']
        ];
        $template['content'] = '';
        $template['title'] = $log['created_by'] .
            ' <span class="m--font-normal">' . $log['action'] . '</span>';

        $logMessages[$i] = $template;
        $logMessages[$i]['content'] = null;

        return $logMessages[$i];
    }

    /**
     * @param $action
     * @param $lang
     *
     * @return mixed|null|string
     */
    private static function mappingAction($action, $lang)
    {
        switch ($action) {
            case 'create':
                return TranslationHelper::getTranslation('contacts_logs_action__create', $lang, 'create');
                break;
            case 'update':
                return TranslationHelper::getTranslation('contacts_logs_action__update', $lang, 'update');
                break;
            case 'delete':
                return TranslationHelper::getTranslation('contacts_logs_action__delete', $lang, 'delete');
                break;
        }
    }

    /**
     * @param $tableName
     * @param $lang
     *
     * @return mixed|null|string
     */
    private static function mappingTableName($tableName, $lang)
    {
        switch ($tableName) {
            case 'contacts':
                return TranslationHelper::getTranslation('contacts_logs_table_name__contacts', $lang, 'contacts');
                break;
            case 'contacts_addresses':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__addresses', $lang, 'addresses');
                break;
            case 'contacts_emails':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__emails', $lang, 'emails');
                break;
            case 'contacts_messengers':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__messengers', $lang, 'messengers');
                break;
            case 'contacts_passports':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__passports', $lang, 'passports');
                break;
            case 'contacts_phones':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__phones', $lang, 'phones');
                break;
            case 'contacts_sites':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__sites', $lang, 'sites');
                break;
            case 'contacts_socials':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__socials', $lang, 'socials');
                break;
            case 'contacts_tags':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__tags', $lang, 'tags');
                break;
            case 'contacts_visas':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__visas', $lang, 'visas');
                break;
            case 'contacts_notes':
                return TranslationHelper::getTranslation('contacts_logs_table_name_contacts__notes', $lang, 'notes');
                break;
        }
    }

    /**
     * @param $action
     *
     * @return string
     */
    private static function mappingCircleColor($action)
    {
        switch ($action) {
            case 'create':
                return 'm--font-warning';
                break;
            case 'update':
                return 'm--font-info';
                break;
            case 'delete':
                return 'm--font-danger';
                break;
        }
    }

    /**
     * @param $column
     *
     * @return bool
     */
    private static function isNeedColumn($column)
    {
        $arr = ['id', 'tenant_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'living_address_id',
            'residence_address_id', 'company_id', 'type_id', 'contact_id'];
        if (in_array($column, $arr)) {
            return false;
        } else {
            return true;
        }
        //type_id,responsible,tenant_id NEED mapping?!
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $tableName
     *
     * @return mixed
     * @throws \Exception
     */
    private static function mappingValueColumn($column, $value, $lang, $tableName)
    {
        $data = ['column' => null, 'value' => null];

        switch ($tableName) {
            case 'contacts':
                return self::mappingContactTable($column, $value, $lang, $data);
                break;
            case 'contacts_addresses':
                return self::mappingContactsAddresses($column, $value, $lang, $data);
                break;
            case 'contacts_emails':
                return self::mappingContactsEmails($column, $value, $lang, $data);
                break;
            case 'contacts_messengers':
                return self::mappingContactsMessengers($column, $value, $lang, $data);
                break;
            case 'contacts_passports':
                return self::mappingContactsPassports($column, $value, $lang, $data);
                break;
            case 'contacts_phones':
                return self::mappingContactsPhones($column, $value, $lang, $data);
                break;
            case 'contacts_sites':
                return self::mappingContactsSites($column, $value, $lang, $data);
                break;
            case 'contacts_socials':
                return self::mappingContactsSocials($column, $value, $lang, $data);
                break;
            case 'contacts_tags':
                return self::mappingContactsTags($column, $value, $lang, $data);
                break;
            case 'contacts_visas':
                return self::mappingContactsVisas($column, $value, $lang, $data);
                break;
            case 'contacts_notes':
                return self::mappingContactsNotes($column, $value, $lang, $data);
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactTable($column, $value, $lang, $data)
    {
        switch ($column) {
            case 'first_name':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__first_name', $lang, 'first name');
                $data['value'] = $value;
                return $data;
                break;
            case 'last_name':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__last_name', $lang, 'last name');
                $data['value'] = $value;
                return $data;
                break;
            case 'middle_name':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__middle_name', $lang, 'middle name');
                $data['value'] = $value;
                return $data;
                break;
            case 'date_of_birth':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__date_of_birth', $lang, 'date of birth');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
            case 'gender':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__gender', $lang, 'gender');
                if ($value == 1) {
                    $data['value'] = TranslationHelper::getTranslation('contacts_logs_contact_value__gender_man', $lang, 'man');
                } else {
                    $data['value'] = TranslationHelper::getTranslation('contacts_logs_contact_value__gender_woman', $lang, 'woman');
                }
                return $data;
                break;
            case 'discount':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__discount', $lang, 'discount');
                $data['value'] = $value . ' %';
                return $data;
                break;
            case 'nationality':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__nationality', $lang, 'nationality');
                $data['value'] = MCHelper::getCountryById($value, $lang)[0]['title'];
                return $data;
                break;
            case 'tin':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__tin', $lang, 'tin');
                $data['value'] = $value;
                return $data;
                break;
            case 'type':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'contacts_notes':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__notes', $lang, 'notes');
                $data['value'] = $value;
                return $data;
                break;
            case 'responsible':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_column__responsible', $lang, 'responsible');
                $user = User::findOne($value);
                if (null === $user) {
                    $data['value'] = 'user not found in database';
                    return $data;
                }
                $data['value'] = $user->first_name . ' ' . $user->last_name;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsAddresses($column, $value, $lang, $data)
    {
        switch ($column) {
            case 'country':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__country', $lang, 'country');
                $data['value'] = MCHelper::getCountryById($value, $lang)[0]['title'];
                return $data;
                break;
            case 'region':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__region', $lang, 'region');
                $data['value'] = $value;
                return $data;
                break;
            case 'city':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__city', $lang, 'city');
                $data['value'] = $value;
                return $data;
                break;
            case 'street':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__street', $lang, 'street');
                $data['value'] = $value;
                return $data;
                break;
            case 'house':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__house', $lang, 'house');
                $data['value'] = $value;
                return $data;
                break;
            case 'flat':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__flat', $lang, 'flat');
                $data['value'] = $value;
                return $data;
                break;
            case 'postcode':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_addresses_column__postcode', $lang, 'postcode');
                $data['value'] = $value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsEmails($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'value':
            case 'contacts_emails':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__emails', $lang, 'email');
                $data['value'] = $value;
                return $data;
                break;

        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsMessengers($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'value':
            case 'contacts_messengers':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__messengers', $lang, 'messengers');
                $data['value'] = $value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsPassports($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'first_name':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__first_name', $lang, 'first name');
                $data['value'] = $value;
                return $data;
                break;
            case 'last_name':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__last_name', $lang, 'last name');
                $data['value'] = $value;
                return $data;
                break;
            case 'serial':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__serial', $lang, 'serial');
                $data['value'] = $value;
                return $data;
                break;
            case 'country':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__country', $lang, 'country');
                $data['value'] = MCHelper::getCountryById($value, $lang)[0]['title'];
                return $data;
                break;
            case 'nationality':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__nationality', $lang, 'nationality');
                $data['value'] = MCHelper::getCountryById($value, $lang)[0]['title'];
                return $data;
                break;
            case 'issued_owner':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__issued_owner', $lang, 'issued owner');
                $data['value'] = $value;
                return $data;
                break;
            case 'birth_date':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__birth_date', $lang, 'birth date');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
            case 'issued_date':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__issued_date', $lang, 'issued date');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
            case 'date_limit':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_passports_column__date_limit', $lang, 'date limit');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsPhones($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'value':
            case 'contacts_phones':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__phone', $lang, 'phone');
                $data['value'] = $value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsSites($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'value':
            case 'contacts_sites':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__sites', $lang, 'site');
                $data['value'] = $value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsSocials($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'value':
            case 'contacts_socials':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__socials', $lang, 'social');
                $data['value'] = $value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     * @throws \Exception
     */
    private static function mappingContactsTags($column, $value, $lang, $data)
    {
        switch ($column) {
            case 'contacts_tags':
            case 'tag_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_column__tag', $lang, 'tag');
                $tag = Tags::findOne($value)->translate($lang);
                $data['value'] = $tag->value;
                return $data;
                break;
        }
    }

    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsVisas($column, $value, $lang, $data)
    {
        switch ($column) {
            // откуда этот тип доставать ???
            case 'type_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__type', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
            case 'country':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contacts_visa_column__country', $lang, 'country');
                $data['value'] = MCHelper::getCountryById($value, $lang)[0]['title'];
                return $data;
                break;
            case 'begin_date':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__begin_date', $lang, 'type');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
            case 'end_date':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__end_date', $lang, 'type');
                $data['value'] = DateTimeHelper::convertTimestampToDate($value);
                return $data;
                break;
            case 'description':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_mail_column__description', $lang, 'type');
                $data['value'] = $value;
                return $data;
                break;
        }
    }


    /**
     * @param $column
     * @param $value
     * @param $lang
     * @param $data
     *
     * @return mixed
     */
    private static function mappingContactsNotes($column, $value, $lang, $data)
    {
        switch ($column) {
            case 'value_id':
                $data['column'] = TranslationHelper::getTranslation('contacts_logs_contact_notes_column__value', $lang, 'notes');
                $data['value'] = $value;
                return $data;
                break;
        }
    }
}