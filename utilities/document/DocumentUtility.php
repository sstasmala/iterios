<?php
/**
 * DocumentUtilities.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\utilities\document;


use app\helpers\DateTimeHelper;
use app\helpers\PreviewShortcodesHelper;
use app\models\DocumentPlaceholders;
use app\models\DocumentsTemplate;
use app\models\Orders;
use app\models\OrdersServicesLinks;
use app\models\OrdersServicesValues;
use app\models\Requisites;
use app\models\ServicesFieldsDefault;
use app\utilities\services\ServicesUtility;

class DocumentUtility
{
    /**
     * @param \app\models\DocumentsTemplate $template
     * @param \app\models\Orders            $order
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function getDocumentPreview(DocumentsTemplate $template, Orders $order): string
    {
        $placeholders = DocumentPlaceholders::find()
            ->asArray()->all();

        if (empty($placeholders))
            return $template->body;

        $ph_requisites = array_filter($placeholders, function ($item) {
            return $item['module'] === DocumentPlaceholders::MODULE_REQUISITE;
        });

        $reqs_schema = static::RequisitesPlaceholderHandling($ph_requisites);

        $ph_services = array_filter($placeholders, function ($item) {
            return $item['module'] === DocumentPlaceholders::MODULE_SERVICE;
        });

        $services_schema = static::ServicesPlaceholderHandling($ph_services, $order->id);

        $schema = array_merge($reqs_schema, $services_schema);

        return PreviewShortcodesHelper::render($schema, $template->body);
    }

    /**
     * @param array $placeholder
     *
     * @return array
     */
    private static function RequisitesPlaceholderHandling(array $placeholder): array
    {
        if (empty($placeholder))
            return [];

        $req_fields_id = array_column($placeholder, 'path');
        $placeholder = array_combine($req_fields_id, array_column($placeholder, 'short_code'));

        $reqs = Requisites::find()
            ->select( ['requisites.id', 'requisites.requisites_field_id'])
            ->where(['in', 'requisites.requisites_field_id', array_diff($req_fields_id, [''])])
            ->with('requisitesCurrentTenant')
            ->asArray()->all();

        $schema = [];

        foreach ($reqs as $req)
            $schema[$placeholder[$req['requisites_field_id']]] = implode(', ', array_column($req['requisitesCurrentTenant'], 'value'));

        return $schema;
    }

    private static function ServicesPlaceholderHandling(array $placeholder, int $order_id): array
    {
        if (empty($placeholder))
            return [];

        $service_fields_id = array_column($placeholder, 'path');
        $placeholder = array_combine($service_fields_id, array_column($placeholder, 'short_code'));

        $service_fields = ServicesFieldsDefault::find()
            ->where(['in', 'id', array_diff($service_fields_id, [''])])
            ->asArray()->all();
        $service_fields = array_combine(array_column($service_fields, 'id'), $service_fields);

        $orders_services_links = OrdersServicesLinks::find()
            ->select('id')
            ->where(['order_id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->column();

        $orders_services_values = OrdersServicesValues::find()
            ->where(['in', 'link_id', $orders_services_links])
            ->andWhere(['in', 'field_id', array_diff($service_fields_id, [''])])
            ->asArray()->all();

        array_walk($orders_services_values, function (&$item) use ($service_fields) {
             $data = $service_fields[$item['field_id']];
             $data['value'] = $item['value'];

             $item = $data;
        });

        /** @var array $orders_services_values */
        $MCValues = ServicesUtility::getMCValuesFields($orders_services_values, \Yii::$app->user->identity->tenant);

        $service_values = [];

        foreach ($orders_services_values as $item)
            $service_values[$item['id']][] = static::getTitles($MCValues, $item['type'], $item['value']);

        $schema = [];

        foreach ($service_values as $key => $items)
            $schema[$placeholder[$key]] = implode(', ', $items);

        return $schema;
    }

    /**
     * Normalize values
     * @param $values
     * @param $type
     * @param $id
     * @return array|null|string
     */
    private static function getTitles($values,$type,$id)
    {
        if ($id === null)
            return null;

        switch ($type)
        {
            case \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS:
                return $values['suppliers'][$id]['name'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_AIRPORT:
                return isset($values['airports'][$id]['title'])
                    ? $values['airports'][$id]['title'].' ('.$values['airports'][$id]['iata_code'].') - <i>'.$values['airports'][$id]['city_title'].', '.$values['airports'][$id]['country_title'].'</i>'
                    : $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_CITY:
                return $values['cities'][$id]['title'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_COUNTRY:
                return $values['countries'][$id]['title'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_DEPARTURE_CITY:
                return $values['departure_cities'][$id]['title'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_FOOD:
                return $values['foods'][$id]['title'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_HOTEL:
                return isset($values['hotels'][$id]['title'])
                    ? $values['hotels'][$id]['title'].' *'.$values['hotels'][$id]['hotel_category_title'].', '.$values['hotels'][$id]['country_title'].', '.$values['hotels'][$id]['city_title']
                    : $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_STAR_RATING:
                return $values['star_ratings'][$id]['title'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_CURRENCY:
                return $values['currencies'][$id]['iso_code'] ?? $id;
                break;
            case \app\models\ServicesFieldsDefault::TYPE_DATE:
            case \app\models\ServicesFieldsDefault::TYPE_DATE_STARTING:
            case \app\models\ServicesFieldsDefault::TYPE_DATE_ENDING:
                return DateTimeHelper::convertTimestampToDate($id);
                break;
            default: return $id;
        }
    }
}