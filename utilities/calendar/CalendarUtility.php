<?php
/**
 * CalendarUtility.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\calendar;


use app\helpers\DateTimeHelper;
use app\helpers\TranslationHelper;
use app\models\Contacts;
use app\models\NotificationsTrigger;
use app\models\Requests;
use app\models\Tasks;
use app\models\User;

class CalendarUtility
{

    private static function processEvent($item,$references,$year,$base_url,$lang)
    {
        if(is_null($item))
            return [];

        $data = [];
        switch ($item['action'])
        {
            case 'contacts.date_of_birth':
                $contact = $references['contacts'][$item['element_id']];
                $data['title'] = TranslationHelper::getTranslation('tasks_calendar_filters_birthday',$lang);
                $data['description'] = TranslationHelper::getTranslation('tasks_calendar_filters_birthday',$lang)
                    .' '.$contact['first_name'].' '.$contact['last_name'];
                $data['start'] = str_replace('.','-',$year.'.'.$item['trigger_day']);
                $data['className'] = "m-fc-event--light m-fc-event--solid-primary";
                $data['iconClass'] = 'la la-birthday-cake mr-1';
                $data['url'] = $base_url.'/contacts/view?id='.intval($contact['id']);
                break;
            case 'requests.days_complete':
                $request = $references['requests'][$item['element_id']];
                $data['title'] = TranslationHelper::getTranslation('tasks_calendar_request_number',$lang).' '.$request['id'];
                $data['start'] = str_replace('.','-',$item['trigger_day']);
                $data['description'] = TranslationHelper::getTranslation('tasks_calendar_request_number',$lang).' '.$request['id'];
                $data['className'] = "m-fc-event--solid-focus m-fc-event--light";
                $data['iconClass'] = 'flaticon-exclamation-1';
                $data['url'] = $base_url.'/requests/view?id='.intval($request['id']);
                break;
            case 'tasks.days_complete':
                $task = $references['tasks'][$item['element_id']];
                $data['title'] = $task['title'];
                $data['start'] = str_replace(' ','T',DateTimeHelper::convertTimestampToDate($task['due_date'],'Y-m-d H:i:s'));
                $data['description'] = TranslationHelper::getTranslation('tasks_calendar_task_number',$lang)
                    .' '.$task['id'].' '.$task['description'];
                $data['className'] = "m-fc-event--solid-info m-fc-event--light";
                $data['iconClass'] = 'flaticon-list-2';
                $data['url'] = $base_url.'/tasks';
                break;
        }
        return $data;
    }

    public static function getEventsList($start, $end, $tenant_id, $agent_id, $options = [],$lang = null)
    {
        if($start==null || $end==null || $tenant_id==null || $agent_id==null || empty($options))
            return [];
        if($lang==null)
            $lang = \Yii::$app->user->identity->tenant->language->iso;
        $fstart = date('Y.m.d', intval($start));
        $fend = date('Y.m.d', intval($end));
        $tstart = date('m.d', intval($start));
        $tend = date('m.d', intval($end));
        $query = NotificationsTrigger::find()->andWhere(['or',['and',['>=','trigger_day',$fstart],['<=','trigger_day',$fend]],
            ['and',['>=','trigger_day',$tstart],['<=','trigger_day',$tend]]])
            ->andWhere(['type'=>0])->andWhere(['tenant_id'=>intval($tenant_id)])->andWhere(['agent_id'=>$agent_id]);

        $filter = ['or'];
        if(isset($options['birthday']) && $options['birthday'])
            $filter[] = ['like','notifications_trigger.action','contacts.date_of_birth'];
        if(isset($options['task']) && $options['task'])
            $filter[] = ['like','notifications_trigger.action','tasks.days_complete'];
        if(isset($options['request']) && $options['request'])
            $filter[] = ['like','notifications_trigger.action','requests.days_complete'];
        if(count($filter)>1)
            $query->andWhere($filter);
        else
            return [];

        $query->leftJoin('reminder_event_types','reminder_event_types.action = notifications_trigger.action');
        $query->andWhere(['or',['=','reminder_event_types.status',1],'reminder_event_types.status IS NULL']);
        $rez = $query->asArray()->all();

        $references = [];

        $contacts_ids = array_unique(array_column($rez,'contact_id'));
        $contacts_ids = array_filter($contacts_ids,function($item){
            return (!is_null($item) && $item && !empty($item));
        });
        $contacts = Contacts::find()->where(['in','id',$contacts_ids])->asArray()->all();
        $references['contacts'] = array_combine(array_column($contacts,'id'),$contacts);

        $users_ids = array_unique(array_column($rez,'agent_id'));
        $users = User::find()->where(['in','id',$users_ids])->asArray()->all();
        $references['users'] = array_combine(array_column($users,'id'),$users);

        $requests = array_filter($rez,function($item){
            return $item['action'] == 'requests.days_complete';
        });
        $requests_ids = array_unique(array_column($requests,'element_id'));
        $requests = Requests::find()->where(['in','id',$requests_ids])->asArray()->all();
        $references['requests'] = array_combine(array_column($requests,'id'),$requests);

        $tasks = array_filter($rez,function($item){
            return $item['action'] == 'tasks.days_complete';
        });
        $tasks_ids = array_unique(array_column($tasks,'element_id'));
        $tasks = Tasks::find()->where(['in','id',$tasks_ids])->asArray()->all();
        $references['tasks'] = array_combine(array_column($tasks,'id'),$tasks);


        $base_url = \Yii::$app->params['baseUrl'].'/ita/'.$tenant_id;
        $data = [];
        $year = date('Y', intval($start));
        foreach ($rez as $item)
        {
            $data[] = static::processEvent($item,$references,$year,$base_url,$lang);
        }
        return $data;
    }
}