<?php
/**
 * Created by PhpStorm.
 * User: zapleo
 * Date: 23.04.18
 * Time: 14:56
 */

namespace utilities\payment_providers;


use common\models\Settings;
use common\models\Transactions;

class PayCore implements BasePaymentInterface
{
    private $config;

    /**
     * PayCore constructor.
     *
     * @param $config
     *
     * @throws \Exception
     */
    public function __construct($config)
    {
        $this->config = [];
        $this->config = array_merge($this->config, $config);

        if (!isset($this->config['public_key']))
            throw new \Exception('Undefined parameters public_key');
    }

    public function renderForm($amount, $currency, $payment_id, $server_url, $description = '')
    {
        // TODO: Implement renderForm() method.
        $params = [];
        $params = array_merge($params, $this->config);
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['reference'] = $payment_id;
        $params['description'] = $description;
        $params['auto_redirect'] = true;

        if (!empty(Settings::findOne(['key'=> Settings::BASE_RETURN_URL_KEY])->value)) {
            $params['return_url'] = Settings::findOne(['key' => Settings::BASE_RETURN_URL_KEY])->value . '?code=' . $payment_id;
        } else {
            $params['return_url'] = \Yii::$app->params['backend_url'] . '/payment/success?code=' . $payment_id;
        }

        $params['ipn_url'] = \Yii::$app->params['backend_url'] . '/payment/pay-core-status-update';

        $form = '<form method="post" action="'.$params['action'].'" id="redirect_from">';

        foreach ($params as $name => $param) {
            if ($name === 'action' || $name === 'private_key') continue;

            $form .= '<input type="hidden" name="' . $name . '" value="' . $param . '">';
        }

        $form .= '</form>';
        $form .= '<script>
            var form = document.getElementsByTagName("form")[0];
            form.submit();
            </script>';

        return $form;
    }

    /**
     * @param $data
     *
     * @return bool
     * @throws \Exception
     */
    public function decodeServerData($data)
    {
        if (empty($this->config['private_key']))
            return false;

        $signature = base64_encode( sha1( $this->config['private_key'] . $data['data'] . $this->config['private_key'],1) );

        if ($signature != $data['signature'])
            return false;

        $params = json_decode(base64_decode($data['data']), true);
        $response['data'] = json_encode($params);

        switch ($params['state'])
        {
            case 'error': $response['status'] = Transactions::STATUS_ERROR; break;
            case 'failure': $response['status'] = Transactions::STATUS_ERROR; break;
            case 'expired': $response['status'] = Transactions::STATUS_ERROR; break;
            case 'canceled': $response['status'] = Transactions::STATUS_CANCELED; break;
            case 'refunded': $response['status'] = Transactions::STATUS_REFUNDED; break;
            case 'created': $response['status'] = Transactions::STATUS_PENDING; break;
            case 'pending': $response['status'] = Transactions::STATUS_PENDING; break;
            case 'success': $response['status'] = Transactions::STATUS_PAYED; break;
            default: $response['status'] = Transactions::STATUS_PROCESSING; break;
        }

        $response['description'] = 'ID: #' . $params['id'] . '. ' . ($params['test_mode'] ? 'Test mode!' : '');

        return $response;
    }
}