<?php
/**
 * Created by PhpStorm.
 * User: zapleo
 * Date: 20.11.17
 * Time: 13:03
 */

namespace utilities\payment_providers;


use common\models\Settings;
use common\models\Transactions;

class LiqPay implements BasePaymentInterface
{
    private $config;

    public function __construct($config)
    {
        $this->config = [];
        $this->config = array_merge($this->config, $config);
        if (!isset($this->config['private_key'])
            || !isset($this->config['public_key'])
        )
            throw new \Exception("Undefined parameters private_key or/and public_key");
    }

    public function renderForm($amount,$currency,$payment_code,$server_url,$description = '')
    {
        $liqPay = new \LiqPay($this->config['public_key'], $this->config['private_key']);
        $params = [];
        $params = array_merge($params, $this->config);
        $params['action'] = 'pay';
        $params['amount'] = $amount;
        $params['currency'] = $currency;
        $params['order_id'] = $payment_code;
        $params['description'] = $description;
        $params['server_url'] = $server_url;

        if(!isset($return_url)) {
            if (!empty(Settings::findOne(['key'=> Settings::BASE_RETURN_URL_KEY])->value)) {
                $params['result_url'] = Settings::findOne(['key' => Settings::BASE_RETURN_URL_KEY])->value . '?code=' . $payment_code;
            } else {
                $params['result_url'] = \Yii::$app->params['base_url'] . '/payment/success?code=' . $payment_code;
            }
        }

        if (!isset($params['version']))
            $params['version'] = '3';

        $html = $liqPay->cnb_form($params);
        $html .= '<script>
        let form = document.getElementsByTagName("form")[0];
        form.submit();
        </script>';
        return $html;
    }

    public function decodeServerData($data)
    {
        $liqPay = new \LiqPay($this->config['public_key'], $this->config['private_key']);
        $data = json_decode($data,true);
        $signature=base64_encode( sha1( $this->config['private_key'] . $data['data'] .$this->config['private_key'],1) );
        if($signature != $data['signature'])
            return false;
        $params = $liqPay->decode_params($data['data']);
        $response['data'] = json_encode($params);
        switch ($params['status'])
        {
            case 'error':$response['status'] = Transactions::STATUS_ERROR;break;
            case 'failure':$response['status'] = Transactions::STATUS_ERROR;break;
            case 'reversed':$response['status'] = Transactions::STATUS_PAYED;break;
            case 'sandbox':$response['status'] = Transactions::STATUS_PAYED;break;
            case 'subscribed':$response['status'] = Transactions::STATUS_PAYED;break;
            case 'success':$response['status'] = Transactions::STATUS_PAYED;break;
            case 'unsubscribed':$response['status'] = Transactions::STATUS_PAYED;break;
            default: $response['status'] = Transactions::STATUS_PROCESSING;break;
        }

        $response['description'] = $params['description'];
        if($response['status'] == Transactions::STATUS_ERROR)
        {
            if(isset($params['err_code']))
                $response['description'].=' Error code:'.$params['err_code'];
            if(isset($params['err_description']))
                $response['description'].=' Error detail:'.$params['err_description'];
        }
        return $response;
    }
}