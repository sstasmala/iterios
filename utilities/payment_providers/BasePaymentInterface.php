<?php
/**
 * Created by PhpStorm.
 * User: zapleo
 * Date: 20.11.17
 * Time: 13:03
 */

namespace utilities\payment_providers;


interface BasePaymentInterface
{
    public function __construct($config);

    public function renderForm($amount,$currency,$payment_id,$server_url,$description = '');

    public function decodeServerData($data);
}