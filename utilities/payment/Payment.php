<?php
/**
 * Payment.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\payment;


class Payment
{
    const PAYMENT_BANK_TRANSFER = 0;
    const PAYMENT_CARD = 1;
    const PAYMENT_CASH = 2;

    const PAYMENT_METHODS = [
        self::PAYMENT_BANK_TRANSFER => 'Bank transfer',
        self::PAYMENT_CARD => 'Card',
        self::PAYMENT_CASH => 'Cash'
    ];
}