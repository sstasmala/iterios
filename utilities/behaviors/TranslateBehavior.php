<?php
/**
 * TranslateBehavior.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\behaviors;


use app\models\Languages;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

class TranslateBehavior extends AttributeBehavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_FIND => function () { $this->translate(); },
        ];
    }

    /**
     */
    protected function translate()
    {
        $session = \Yii::$app->hasProperty('session');
        if ($session && \Yii::$app->session->has('language'))
            $this->owner->translate(\Yii::$app->session->get('language'));
        else
            $this->owner->translate(Languages::getDefault());
    }
}