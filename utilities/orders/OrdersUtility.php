<?php
/**
 * OrdersUtility.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\orders;


use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\models\Orders;
use app\models\OrdersServicesLinks;
use app\models\OrdersTouristsPayments;
use app\models\ServicesFieldsDefault;
use app\utilities\services\ServicesUtility;

class OrdersUtility
{
    public static function getPaymentInfo($ids) {
        if(is_array($ids))
            $order_ids = $ids;
        else
            $order_ids = [$ids];

        $orders = Orders::find()->where(['in','id',$order_ids])->all();

        $links = OrdersServicesLinks::find()->where(['in','order_id',$order_ids])
            ->andWhere(['in','status',[OrdersServicesLinks::STATUS_ACCEPTED,OrdersServicesLinks::STATUS_WAIT]])->asArray()->all();

        $l_ids = \array_column($links,'id');
        $s_ids = \array_combine($l_ids,\array_column($links,'service_id'));
        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
        $services = \array_combine(\array_column($services,'id'),$services);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getOrdersValues($l_ids);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);

        $payments = OrdersTouristsPayments::find()->where(['in','order_id',$order_ids])->asArray()->all();
        $links = \array_combine(\array_column($links,'id'),$links);

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        $results = [];
        foreach ($orders as $order) {
            $order_payments = \array_filter($payments,function ($item)use($order){return $item['order_id']==$order->id;});
            $order_links = \array_filter($links,function ($item)use($order){return $item['order_id']==$order->id;});

            $sum = 0;
            $supplier_sum = 0;
            $price_info = [];

            $tourist_payed = 0;
            $provider_payed = [];
            $tourist_duties = [];
            $tourist_payments = [];
            foreach ($order_links as $in => $link) {
                if(!isset($services[$in]))
                    continue;
                $service = $services[$in];

                $rate = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_EXCHANGE_RATE);
                $total_price = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_SERVICES_PRICE);
                $commission = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_COMMISSION);
                $supplier_price = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER);
                $currency = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_CURRENCY);
                $provider = ServicesUtility::getFieldFromService($service,ServicesFieldsDefault::TYPE_SUPPLIERS);

                if($commission!==null && !empty($commission) && isset($rate)) {
                    $cr = $currency['text'];
                    if($commission['type']=='per')
                        $commission = $commission['value'].'% / '.($total_price/(100-$commission['value']))*$commission['value']
                            .' '.$cr;
                    else{
                        $commission['value'] = ($rate==null)?$commission['value']:$commission['value']*$rate;
                        $commission = $commission['value']/(($total_price+$commission['value'])/100)
                            .'% / '.$commission['value'].' '.$cr;
                    }
                }

                if(!isset($rate) || !isset($total_price))
                    $sum = null;
                if(!isset($rate) || !isset($supplier_price))
                    $supplier_sum = null;
                if($sum !== null)
                {
                    $sum += $rate*$total_price;
                    $price_info[$in]['sum'] = $total_price;
                }
                if(isset($currency) && !empty($currency))
                    $price_info[$in]['currency'] = $currency;
                if($supplier_sum !== null)
                {
                    $supplier_sum += $supplier_price*$rate;
                    $price_info[$in]['supplier_sum'] = $supplier_price;
                }

                $service_payments = \array_filter($order_payments,function($item)use($in){
                    return ($item['link_id'] == $in && $item['type'] == OrdersTouristsPayments::TYPE_TOURIST);
                });

                if(!empty($provider)  && !empty($currency) && !empty($supplier_price)) {
                    $provider_payed[$in]['sum'] = 0;
                    $provider_payed[$in]['id'] = $provider['id'];
                    $provider_payed[$in]['cur'] = $currency;
                    $provider_payed[$in]['duty'] = $supplier_price;
                    $provider_payed[$in]['status'] = 0;
                    $provider_payed[$in]['commission'] = $commission;
                    if(isset($rate))
                        $provider_payed[$in]['price'] = $supplier_price*$rate;
                    else
                        $provider_payed[$in]['price'] = null;
                }
                if(!isset($tourist_duties[$currency['text']]))
                    $tourist_duties[$currency['text']] = (float)$total_price;
                else
                    $tourist_duties[$currency['text']] += (float)$total_price;
                foreach ($service_payments as $payment)
                {
                    $tourist_payed += $payment['sum'];
                    $tourist_payments[] =
                        [
                            'date' => (!empty($payment['date']))?DateTimeHelper::convertTimestampToDate($payment['date']):null,
                            'sum' => $payment['sum'],
                            'rate' => $payment['ext_rates']
                        ];
                    if(!empty($currency)  && !empty($supplier_price))
                        $tourist_duties[$currency['text']] -= $payment['sum']/$payment['ext_rates'];
                }

                if(!empty($provider)  && !empty($currency)  && !empty($supplier_price)) {

                    $provider_payments = \array_filter($order_payments,function($item)use($in){
                        return ($item['link_id'] == $in && $item['type'] == OrdersTouristsPayments::TYPE_PROVIDER);
                    });

                    foreach ($provider_payments as $payment)
                    {
                        $provider_payed[$in]['sum'] += $payment['sum'];
                        if(!empty($provider)  && !empty($currency)  && !empty($supplier_price)) {
                            $provider_payed[$in]['duty'] -= $payment['sum']/$payment['ext_rates'];
                        }
                        if($provider_payed[$in]['duty'] <= 0)
                            $provider_payed[$in]['status'] = 2;
                        else
                            $provider_payed[$in]['status'] = 1;
                    }
                }
            }
            $discount = null;
            $profit = null;
            if(isset($sum) && isset($supplier_sum))
                $profit = $sum - $supplier_sum;
            $sum_wd = $sum;
            if($order->discount!==null) {
                $discount = $order->discount;
                if($order->discount_type == 0) {
                    $discount .= '%';
                    if($sum!=null)
                        $sum = $sum - $sum/100*$order->discount;
                }
                else {
                    if($sum!=null)
                        $sum = $sum - $order->discount;
                    $discount .= ' '.$currencies[$order->currency]['iso_code'];
                }
            }
            $status = 0;
            $duty = \array_sum($tourist_duties);
            if($tourist_payed>0)
                $status = 1;
            if($duty <= 0)
                $status = 2;

            if($duty == 0 && $tourist_payed!=$sum && $tourist_payed!=$sum_wd && $sum!=null && $sum_wd != null && $tourist_payed!=null)
                $sum = $sum_wd = $tourist_payed;

            $results[$order->id]['sum'] = $sum;
            $results[$order->id]['profit'] = $profit;
            $results[$order->id]['sum_wd'] = $sum_wd;
            $results[$order->id]['discount'] = $discount;
            $results[$order->id]['cur'] = ['id'=>$order->currency,'text'=>$currencies[$order->currency]['iso_code']];
            $results[$order->id]['supplier_sum'] = $supplier_sum;
            $results[$order->id]['provider_payed'] = $provider_payed;
            $results[$order->id]['tourist_payed'] = $tourist_payed;
            $results[$order->id]['tourist_duties'] = $tourist_duties;
            $results[$order->id]['tourist_payments'] = $tourist_payments;
            $results[$order->id]['status'] = $status;
            $order->total_sum = $sum;
            $order->total_sum_wd = $sum_wd;
            $order->save();
        }
        if(!\is_array($ids))
            return \reset($results);
        return $results;
    }
}