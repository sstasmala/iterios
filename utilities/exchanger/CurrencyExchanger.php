<?php
/**
 * CurrencyExchanger.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\exchanger;


use app\models\ExchangeRates;

class CurrencyExchanger
{
    public static function exchangeFromTo($amount, $currency_from, $currency_to)
    {
        $form = ExchangeRates::find()->where(['ilike','currency_iso',$currency_from])->one();
        $to = ExchangeRates::find()->where(['ilike','currency_iso',$currency_to])->one();

        if($form===null || $to===null)
            return null;

        return (float) ($amount * ( $to->rate / $form->rate ));
    }
}