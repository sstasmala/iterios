<?php
/**
 * ContactsUtility.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\utilities\contacts;

use app\helpers\LogHelper;
use app\helpers\DateTimeHelper;
use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\ContactsMessengers;
use app\models\ContactsPassports;
use app\models\ContactsPhones;
use app\models\ContactsSites;
use app\models\ContactsSocials;
use app\models\ContactsTags;
use app\models\ContactsVisas;
use app\models\MessengersTypes;
use app\models\PassportsTypes;
use app\models\EmailsTypes;
use app\models\PhonesTypes;
use app\models\SocialsTypes;
use app\models\SitesTypes;
use app\models\Tags;
use app\models\VisasTypes;

class ContactsUtility
{

    public static function getContactsScheme()
    {
        return [
            'phones' => 'app\models\ContactsPhones',
            'emails' => 'app\models\ContactsEmails',
            'socials' => 'app\models\ContactsSocials',
            'messengers' => 'app\models\ContactsMessengers',
            'sites' => 'app\models\ContactsSites',
        ];
    }
    /**
     * @param $contact array
     * @return array
     */
    public static function prepareContactsViewArray($contact)
    {
        $keys = array_keys($contact);
        if(array_search( 'contactsEmails', $keys ) !== false)
            $keys[ array_search( 'contactsEmails', $keys ) ] = 'emails';
        if(array_search( 'contactsPhones', $keys ) !== false)
            $keys[ array_search( 'contactsPhones', $keys ) ] = 'phones';
        if(array_search( 'contactsTags', $keys ) !== false)
            $keys[ array_search( 'contactsTags', $keys ) ] = 'tags';
        if(array_search( 'contactsMessengers', $keys ) !== false)
            $keys[ array_search( 'contactsMessengers', $keys ) ] = 'messengers';
        if(array_search( 'contactsSites', $keys ) !== false)
            $keys[ array_search( 'contactsSites', $keys ) ] = 'sites';
        if(array_search( 'contactsSocials', $keys ) !== false)
            $keys[ array_search( 'contactsSocials', $keys ) ] = 'socials';
        if(array_search( 'contactsVisas', $keys ) !== false)
            $keys[ array_search( 'contactsVisas', $keys ) ] = 'visas';
        if(array_search( 'contactsPassports', $keys ) !== false)
            $keys[ array_search( 'contactsPassports', $keys ) ] = 'passports';
        if(array_search( 'gender', $keys ) !== false)
            $keys[ array_search( 'gender', $keys ) ] = 'sex';
        if(array_search( 'livingAddress', $keys ) !== false)
            $keys[ array_search( 'livingAddress', $keys ) ] = 'living_address';
        if(array_search( 'residenceAddress', $keys ) !== false)
            $keys[ array_search( 'residenceAddress', $keys ) ] = 'residence_address';
        $new_contact = array_combine( $keys, $contact );
        $tags = $new_contact['tags'];
        $tags = Tags::find()->where(['in','id',array_column($tags,'tag_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        foreach ($tags as $i => $item) {
            $keys = array_keys($item);
            $keys[ array_search( 'value', $keys ) ] = 'name';
            $item = array_combine( $keys, $item );
            if(is_null($item['name']))
                $item['name'] = '[Not translated]';
            $item['link'] = '#';//TODO change when tags link wil be resolved
            $tags[$i] = $item;
        }
        $new_contact['tags'] = $tags;
        return $new_contact;
    }

    /**
     * @param $post        array
     * @param $new_contact array
     * @param $contactId
     *
     * @return array|bool
     * @throws \Exception
     * @throws \Throwable
     */
    public static function postContactsContacts($post, $new_contact, $contactId)
    {
        if(!empty($post)){
            $main_fields = [];
            $scheme = static::getContactsScheme();
            if(isset($post["contacts"]))
            foreach ($post['contacts'] as $k=>$contact)
            {
                $old = null;
                $class = null;
                switch ($k)
                {
                    case 'phones':$class = 'app\models\ContactsPhones';$old =  $new_contact[$k];break;
                    case 'emails':$class = 'app\models\ContactsEmails';$old =  $new_contact[$k];break;
                    case 'socials':$class = 'app\models\ContactsSocials';$old =  $new_contact[$k];break;
                    case 'messengers':$class = 'app\models\ContactsMessengers';$old =  $new_contact[$k];break;
                    case 'sites':$class = 'app\models\ContactsSites';$old =  $new_contact[$k];break;
                }
                if(is_null($class) || is_null($old))
                {
                    if(!is_array($contact))
                    {
                        $main_fields[$k]=$contact;
                    }
                    continue;
                }
                unset($scheme[array_search($class,$scheme)]);


                $ids = array_column($old,'id');
                foreach ($contact as $item)
                {
                    if(!empty($item['value']) && !empty($item['type_id']))
                    if(isset($item['id']))
                    {
                        $model = $class::findOne(['id'=>$item['id']]);
                        if(is_null($model))
                            continue;
                        $model->value = $item['value'];
                        $model->type_id = $item['type_id'];
                        $logItemId = $model->save();
                        if (is_numeric($logItemId)) {
                        $forceContact = Contacts::findOne($contactId);
                        $forceContact->touch('updated_at');
                        $forceContact->force_update = true;
                        $logMainItemId = $forceContact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }
                        if(!$logItemId)
                            return $model->getErrors();

                        unset($old[array_search($item['id'],$ids)]);
                    }
                    else
                    {
                        $model = new $class();
                        $model->value = $item['value'];
                        $model->type_id = $item['type_id'];
                        $model->contact_id = $new_contact['id'];
                        $logItemId = $model->save();
                        if (is_numeric($logItemId)) {
                            $forceContact = Contacts::findOne($contactId);
                            $forceContact->touch('updated_at');
                            $forceContact->force_update = true;
                            $logMainItemId = $forceContact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }
                        if(!$logItemId)
                            return $model->getErrors();
                    }
                }
                foreach ($old as $item)
                {
                    $model = $class::findOne(['id'=>$item['id']]);
                    if(is_null($model)) {
                        continue;
                    }
                    $logItemId = $model->delete();
                    if (is_numeric($logItemId)) {
                        $forceContact = Contacts::findOne($contactId);
                        $forceContact->touch('updated_at');
                        $forceContact->force_update = true;
                        $logMainItemId = $forceContact->save();
                        LogHelper::setGroupId($logItemId, $logMainItemId);
                    }
                }
            }
            foreach ($scheme as $k => $v)
            {
                $models = $v::findAll(['contact_id'=>$new_contact['id']]);
                foreach ($models as $model) {
                    $logItemId = $model->delete();
                    if (is_numeric($logItemId)) {
                        $forceContact = Contacts::findOne($contactId);
                        $forceContact->touch('updated_at');
                        $forceContact->force_update = true;
                        $logMainItemId = $forceContact->save();
                        LogHelper::setGroupId($logItemId, $logMainItemId);
                    }
                }
            }

            if(isset($post['tags']))
            {
                $tenant = \Yii::$app->user->identity->tenant;
                $old = $new_contact['tags'];
                $old_ids = array_column($old,'id');
                foreach ($post['tags'] as $tid)
                {
                    $exist = array_search($tid,$old_ids);
                    if(!($exist===false))
                    {
                        unset($old_ids[$exist]);
                    //                        $safe[] = $exist;
                        continue;
                    }
                    if(is_numeric($tid))
                        $tag = Tags::find()->where(['type'=>'system'])
                            ->orWhere(['tenant_id'=>$tenant->id])
                            ->andFilterWhere(['id'=>$tid])->one();
                    else
                        $tag = null;
                    if(is_null($tag))
                    {
                        $tag = new Tags();
                        $tag->tenant_id = $tenant->id;
                        $tag->type = 'custom';
                        $tag->value = $tid;
                        $logItemId = $tag->saveWithLang($tenant->language->iso);
                        if (is_numeric($logItemId)) {
                            $forceContact = Contacts::findOne($contactId);
                            $forceContact->touch('updated_at');
                            $forceContact->force_update = true;
                            $logMainItemId = $forceContact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }
                        if(!$logItemId)
                            return $tag->getErrors();
                    }
                    $link = ContactsTags::find()->where(['tag_id'=>$tag->id])
                        ->andWhere(['contact_id'=>$new_contact['id']])->one();
                    if(is_null($link))
                    {
                        $link = new ContactsTags();
                        $link->contact_id = $new_contact['id'];
                        $link->tag_id = $tag->id;
                        $logItemId = $link->save();
                        if (is_numeric($logItemId)) {
                            $forceContact = Contacts::findOne($contactId);
                            $forceContact->touch('updated_at');
                            $forceContact->force_update = true;
                            $logMainItemId = $forceContact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }
                        if(!$logItemId)
                            return $link->getErrors();
                    }
                }
                $delete = $old_ids;
                foreach ($delete as $v)
                {
                    $model = ContactsTags::findOne(['tag_id'=>$v]);
                    if(is_null($model)) {
                        continue;
                    }
                    $logItemId = $model->delete();
                    if (is_numeric($logItemId)) {
                        $forceContact = Contacts::findOne($contactId);
                        $forceContact->touch('updated_at');
                        $forceContact->force_update = true;
                        $logMainItemId = $forceContact->save();
                        LogHelper::setGroupId($logItemId, $logMainItemId);
                    }
                }
            } else {
                $tags = ContactsTags::findAll(['contact_id' => $contactId]);

                foreach ($tags as $tag) {
                    $forceContact = Contacts::findOne($contactId);
                    $forceContact->touch('updated_at');
                    $forceContact->force_update = true;
                    $logMainItemId = $forceContact->save();
                    $logItemId = $tag->delete();
                    LogHelper::setGroupId($logItemId, $logMainItemId);
                }
            }

            if(!empty($main_fields))
            {
                $cn = Contacts::findOne(['id'=>$new_contact['id']]);
                if(!is_null($cn))
                {
                    foreach ($main_fields as $k=>$v)
                    {
                        switch ($k){
                            case 'first_name':$cn->first_name = $v;break;
                            case 'last_name':$cn->last_name = $v;break;
                            case 'middle_name':$cn->middle_name = $v;break;
                        }
                    }
                    if(!$cn->save())
                        return $cn->getErrors();
                }
            }
            return true;

        }
        return false;
    }

    /**
     * @param $lang string
     * @return mixed
     */

    public static function addPassport($post,$contactId)
    {
        if(isset($post['ContactPassport']))
        {
            $contact = Contacts::findOne(['id' => $contactId]);
            $data = $post['ContactPassport'];
            if(is_numeric($data['passport_id']))
                $passport = ContactsPassports::findOne(['id'=>$data['passport_id']]);
            else
                $passport = null;
            if(is_null($passport))
                $passport = new ContactsPassports();
            if(isset($data['birth_date'])){
                $passport->birth_date = DateTimeHelper::convertDateToTimestamp($data['birth_date']);
                if($post['action'] == 'not_same'){
                    if($post['helper'] == 'true') {
                        $contact->date_of_birth = DateTimeHelper::convertDateToTimestamp($data['birth_date']);
                        $contact->save();
                    } elseif ($post['helper'] == 'cancel') {
                        $passport->birth_date = DateTimeHelper::convertDateToTimestamp($data['birth_date']);
                    }
                } elseif($post['action'] == 'same') {
                    $passport->birth_date = DateTimeHelper::convertDateToTimestamp($data['birth_date']);
                }

            }
            if(isset($data['first_name']))
                $passport->first_name = $data['first_name'];
            if(isset($data['last_name']))
                $passport->last_name = $data['last_name'];
            if(isset($data['type']))
                $passport->type_id = (int) $data['type'];
            if(isset($data['serial']))
                $passport->serial = $data['serial'];
            if(isset($data['date_limit']))
                $passport->date_limit = DateTimeHelper::convertDateToTimestamp($data['date_limit']);
            if(isset($data['issued_date']))
                $passport->issued_date = DateTimeHelper::convertDateToTimestamp($data['issued_date']);
            if(isset($data['issued_owner']))
                $passport->issued_owner = $data['issued_owner'];
            if(isset($data['nationality']))
                $passport->nationality = $data['nationality'];
            if(isset($data['country']))
                $passport->country = $data['country'];
            $passport->contact_id = $contactId;
            $logItemId = $passport->save();
            if (is_numeric($logItemId)) {
                $forceContact = Contacts::findOne($contactId);
                $forceContact->touch('updated_at');
                $forceContact->force_update = true;
                $logMainItemId = $forceContact->save();
                LogHelper::setGroupId($logItemId, $logMainItemId);
            }
            if(!$logItemId){
                return $passport->getErrors();
            }else{
                return true;
            }

        }
    }


    public static function addVisa($post,$contactId)
    {
        if(isset($post['ContactVisa'])) {
            $data = $post['ContactVisa'];
            if (is_numeric($data['visa_id']))
                $visa = ContactsVisas::findOne(['id' => $data['visa_id']]);
            else
                $visa = null;
            if (is_null($visa))
                $visa = new ContactsVisas();
            if (isset($data['country']))
                $visa->country = $data['country'];
            if (isset($data['begin_date']))
                $visa->begin_date = DateTimeHelper::convertDateToTimestamp($data['begin_date']);
            if (isset($data['end_date']))
                $visa->end_date = DateTimeHelper::convertDateToTimestamp($data['end_date']);
            if (isset($data['description']))
                $visa->description = $data['description'];
            if (isset($data['type']))
                $visa->type_id = $data['type'];
            $visa->contact_id = $contactId;
            $logItemId = $visa->save();
            if (is_numeric($logItemId)) {
                $forceContact = Contacts::findOne($contactId);
                $forceContact->touch('updated_at');
                $forceContact->force_update = true;
                $logMainItemId = $forceContact->save();
                LogHelper::setGroupId($logItemId, $logMainItemId);
            }
            if (!$logItemId) {
                return $visa->getErrors();
            } else {
                return true;
            }
        }
    }

    public static function prepareTypes($lang)
    {
        $temp = PhonesTypes::find()->translate($lang)->asArray()->all();
        $types['phones'] = array_combine(array_column($temp,'id'),$temp);
        $temp = EmailsTypes::find()->translate($lang)->asArray()->all();
        $types['emails'] = array_combine(array_column($temp,'id'),$temp);
        $temp = SocialsTypes::find()->asArray()->all();
        $types['socials'] = array_combine(array_column($temp,'id'),$temp);
        $temp = MessengersTypes::find()->asArray()->all();
        $types['messengers'] = array_combine(array_column($temp,'id'),$temp);
        $temp = SitesTypes::find()->translate($lang)->asArray()->all();
        $types['sites'] = array_combine(array_column($temp,'id'),$temp);
        $temp = PassportsTypes::find()->translate($lang)->asArray()->all();
        $types['passports'] = array_combine(array_column($temp,'id'),$temp);
        $temp = VisasTypes::find()->translate($lang)->asArray()->all();
        $types['visas'] = array_combine(array_column($temp,'id'),$temp);

        foreach ($types as $k => $type)
        {
            foreach ($type as $i => $item)
            {
                $keys = array_keys($item);
                $keys[ array_search( 'value', $keys ) ] = 'name';
                $item = array_combine( $keys, $item );
                if(is_null($item['name']))
                    $item['name'] = '[Not translated]';
                $types[$k][$i]=$item;
            }
        }
        return $types;
    }

    private static function mapTypes($values,$types)
    {
        foreach ($values as $k=>$value)
            $values[$k]['type'] = $types[$value['type_id']];
        return $values;
    }


    public static function getContactsInfo($id,$lang) {
        $ids = $id;
        if(!is_array($ids))
            $ids = [$ids];
        if($ids == [])
            return [];
        $contacts = Contacts::find()->where(['in','id',$ids])->asArray()->all();

        //Get passports
        $passports = ContactsPassports::find()->where(['in','contact_id',$ids])->asArray()->all();
        $passports = array_combine(array_column($passports,'id'),$passports);
        $temp_ids = array_column($passports,'type_id');
        $passports_types = PassportsTypes::find()->where(['in','id',$temp_ids])->translate($lang)->asArray()->all();
        $passports_types = array_combine(array_column($passports_types,'id'),$passports_types);

        $phones = ContactsPhones::find()->where(['in','contact_id',$ids])->asArray()->all();
        $phones = array_combine(array_column($phones,'id'),$phones);
        $temp_ids = array_column($phones,'type_id');
        $phones_types = PhonesTypes::find()->where(['in','id',$temp_ids])->translate($lang)->asArray()->all();
        $phones_types = array_combine(array_column($phones_types,'id'),$phones_types);

        $emails = ContactsEmails::find()->where(['in','contact_id',$ids])->asArray()->all();
        $emails = array_combine(array_column($emails,'id'),$emails);
        $temp_ids = array_column($emails,'type_id');
        $emails_types = EmailsTypes::find()->where(['in','id',$temp_ids])->translate($lang)->asArray()->all();
        $emails_types = array_combine(array_column($emails_types,'id'),$emails_types);

        $socials = ContactsSocials::find()->where(['in','contact_id',$ids])->asArray()->all();
        $socials = array_combine(array_column($socials,'id'),$socials);
        $temp_ids = array_column($socials,'type_id');
        $socials_types = SocialsTypes::find()->where(['in','id',$temp_ids])->asArray()->all();
        $socials_types = array_combine(array_column($socials_types,'id'),$socials_types);

        $messengers = ContactsMessengers::find()->where(['in','contact_id',$ids])->asArray()->all();
        $messengers = array_combine(array_column($messengers,'id'),$messengers);
        $temp_ids = array_column($messengers,'type_id');
        $messengers_types = MessengersTypes::find()->where(['in','id',$temp_ids])->asArray()->all();
        $messengers_types = array_combine(array_column($messengers_types,'id'),$messengers_types);

        $sites = ContactsSites::find()->where(['in','contact_id',$ids])->asArray()->all();
        $sites = array_combine(array_column($sites,'id'),$sites);
        $temp_ids = array_column($sites,'type_id');
        $sites_types = SitesTypes::find()->where(['in','id',$temp_ids])->translate($lang)->asArray()->all();
        $sites_types = array_combine(array_column($sites_types,'id'),$sites_types);

        $visas = ContactsVisas::find()->where(['in','contact_id',$ids])->asArray()->all();
        $visas = array_combine(array_column($visas,'id'),$visas);
        $temp_ids = array_column($visas,'type_id');
        $visas_types = VisasTypes::find()->where(['in','id',$temp_ids])->translate($lang)->asArray()->all();
        $visas_types = array_combine(array_column($visas_types,'id'),$visas_types);

        $passports = static::mapTypes($passports,$passports_types);
        $phones = static::mapTypes($phones,$phones_types);
        $emails = static::mapTypes($emails,$emails_types);
        $socials = static::mapTypes($socials,$socials_types);
        $messengers = static::mapTypes($messengers,$messengers_types);
        $sites = static::mapTypes($sites,$sites_types);
        $visas = static::mapTypes($visas,$visas_types);

        foreach ($contacts as $k => $contact)
        {
            $contacts[$k]['passports'] = array_filter($passports,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['phones'] = array_filter($phones,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['emails'] = array_filter($emails,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['socials'] = array_filter($socials,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['messengers'] = array_filter($messengers,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['sites'] = array_filter($sites,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
            $contacts[$k]['visas'] = array_filter($visas,function($item)use($contact){
                return $item['contact_id'] == $contact['id'];
            });
        }

        $contacts = array_combine(array_column($contacts,'id'),$contacts);
        return $contacts;
    }
}