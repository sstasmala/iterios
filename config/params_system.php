<?php
/**
 * params_system.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

return [
    'email_providers' => [
        [
            'name' => 'SparkPost',
            'class' => 'app\components\mailers\sparkpost\Mailer',
            'type' => 'system'
        ],
        [
            'name' => 'SendGrid',
            'class' => 'app\components\mailers\sendgrid\Mailer',
            'type' => 'public'
        ],
    ],
    'sms_providers' => [
        [
            'name' => 'SMS Club',
            'class' => 'app\components\mailers_sms\smsclub\SmsMailer',
        ],
    ],
    'features' =>  [
        'app\components\features\CompaniesFeature' => 'CompaniesFeature',
        'app\components\features\EmailsSendFeature' => 'EmailsSendFeature',
        'app\components\features\SmsSendFeature' => 'SmsSendFeature',
        'app\components\features\SegmentsFeature' => 'SegmentsFeature',
        'app\components\features\PublicEmailCountFeature' => 'PublicEmailCountFeature',
        'app\components\features\PublicSmsCountFeature' => 'PublicSmsCountFeature',
        'app\components\features\RequestsFeature' => 'RequestsFeature',
        'app\components\features\ContactsFeature' => 'ContactsFeature',
        'app\components\features\OrdersFeature' => 'OrdersFeature',
        'app\components\features\RemindersFeature' => 'RemindersFeature',
    ],
    'dispatch_providers' => [
        'email' => [
            'app\components\email_provider\MailchimpProvider' => 'MailChimpProvider'
        ],
        'sms' => [
            'app\components\sms_provider\SmsClubXML' => 'SmsClubXML'
        ]
    ],
    'placeholders' => [
        'app\components\placeholders\modules\AgentModule' => 'Agent (User)',
        'app\components\placeholders\modules\RequisiteModule' => 'Requisite',
        'app\components\placeholders\modules\ServiceModule' => 'Service',
        'app\components\placeholders\modules\TouristModule' => 'Tourist (Contact)'
    ],
    'payment_providers' => [
        'LiqPay Test' => [
            'namespace'=> 'utilities\payment_providers\LiqPay',
            'config'=>[
                'public_key' => '',
                'private_key' => '',
                'version' => '3',
                'sandbox' => '1'
            ],
        ],
        'LiqPay v3' => [
            'namespace'=> 'utilities\payment_providers\LiqPay',
            'config'=>[
                'public_key' => '',
                'private_key' => '',
                'version' => '3',
            ],
        ]
    ],
];