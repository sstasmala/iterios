<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$auth0 = require __DIR__ . '/auth0.php';
$params_system = require __DIR__ . '/params_system.php';
$params = array_merge($params_system,$params);

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'timeZone' => date_default_timezone_get(),
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'UVpVUH3nk3jeFH-yU_mmGvFMCUzJLWDF',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
//        'session' => [
//            'class' => 'yii\web\Session',
//            'cookieParams' => ['httponly' => true, 'lifetime' =>60*60*24*7],
//            'timeout' => 60*60*24*1, //session expire
//            'useCookies' => true,
//        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '/'=>'site/index',
                'logout'=>'site/logout',
                'login/'=>'site/login',
                'sign-up/'=>'site/sign-up',
                'forgot-password' => 'site/forgot-password',
                'change-password' => 'site/change-password',
                'create-tenant' => 'site/create-tenant',
                'change-tenant' => 'site/change-tenant',
                'ia'=>'ia/dashboard/index',
                'ia/send-test-sms' => 'ia/sms-system-providers/send-test-sms',
                'ia/reminder-queue' => 'ia/notifications-course/index',
                'ia/reminder-queue/<action>' => 'ia/notifications-course/<action>',
                'ia/reminder-log' => 'ia/notifications-log/index',
                'ia/reminder-log/<action>' => 'ia/notifications-log/<action>',
                'ia/reminder-calendar' => 'ia/notifications-trigger/index',
                'ia/reminder-calendar/<action>' => 'ia/notifications-trigger/<action>',
                'ia/notification-constructor' => 'ia/notifications-now/index',
                'ia/notification-constructor/<action>' => 'ia/notifications-now/<action>',
                'ia/notification-user' => 'ia/profile-notifications/index',
                'ia/notification-user/<action>' => 'ia/profile-notifications/<action>',
                'ia/notification-log' => 'ia/notificationsnow-log/index',
                'ia/notification-log/<action>' => 'ia/notificationsnow-log/<action>',
                'ia/reminder-constructor' => 'ia/base-notifications/index',
                'ia/reminder-constructor/<action>' => 'ia/base-notifications/<action>',
                'ia/<controller>'=>'ia/<controller>/index',
                'ia/<controller>/<action>'=>'ia/<controller>/<action>',
                'ia/<controller>/<action>/<id:[\d]+>'=>'ia/<controller>/<action>',
                'webhooks/<key:[\w]+>/<controller>' => 'webhooks/<controller>/index',
                'webhooks/<key:[\w]+>/<controller>/<action>' => 'webhooks/<controller>/<action>',
                'ita'=>'ita/default/index',
                'ita/add-user'=>'ita/default/add-user',
                'ita/blocked'=>'ita/default/blocked',
                'ita/added-user'=>'ita/default/added-user',
                'ita/<ten_id:[\d]+>'=>'ita/dashboard/index',
                'ita/<ten_id:[\d]+>/templates/documents' => 'ita/templates/documents-index',
                'ita/<ten_id:[\d]+>/templates/documents/<action>' => 'ita/templates/documents-<action>',
                'ita/<ten_id:[\d]+>/templates/email-and-sms' => 'ita/templates/email-and-sms-index',
                'ita/<ten_id:[\d]+>/templates/email-and-sms/<action>' => 'ita/templates/email-and-sms-<action>',
                'ita/<ten_id:[\d]+>/billing/my-orders' => 'ita/billing/my-orders-index',
                'ita/<ten_id:[\d]+>/billing/my-orders/<action>' => 'ita/billing/my-orders-<action>',
                'ita/<ten_id:[\d]+>/billing/rates' => 'ita/billing/rates-index',
                'ita/<ten_id:[\d]+>/billing/rates/<action>' => 'ita/billing/rates-<action>',
                'POST ita/<controller>'=>'ita/<controller>/index',
                'POST ita/<controller>/<action>'=>'ita/<controller>/<action>',
                'ita/<ten_id:[\d]+>/<controller>'=>'ita/<controller>/index',
                'ita/<ten_id:[\d]+>/<controller>/<action>'=>'ita/<controller>/<action>',
//                'ita/<ten_id:[\d]+>/<controller>/<action>/<id:[\d]+>'=>'ita/<controller>/<action>',
                'ita/<controller>'=>'ita/default/index',
                'ita/<controller>/<action>'=>'ita/default/index',
                '<controller>'=>'<controller>/index',
                '<controller>/<action>'=>'<controller>/<action>',
                '<controller>/<action>/<id:[\d]+>'=>'<controller>/<action>',
            ],
        ],
        'auth0' => $auth0,
    ],
    'container' => [
        'singletons' => [
            'systemEmitter'=>[
                'class' => 'app\components\base\EventEmitter',
                'map' => [
                    'app\components\events\SystemUserRegistrationEvent'=>['app\components\handlers\SystemUserRegistrationHandler'],
                    'app\components\events\SystemResetPasswordEvent'=>['app\components\handlers\SystemUserResetPasswordHandler'],
                    'app\components\events\SystemTenantRegistrationEvent'=>['app\components\handlers\SystemTenantRegistrationHandler'],
                    'app\components\events\SystemUserAddedToTenantEvent'=>['app\components\handlers\SystemUserAddedToTenantHandler'],
                    'app\components\events\SystemUserInviteEvent'=>
                        ['app\components\handlers\SystemUserInviteHandler','app\components\handlers\SystemUserInviteSaveHandler'],
                    'app\components\events\SystemTaskEmailReminderEvent'=>['app\components\handlers\SystemTaskEmailReminderHandler'],
                    'app\components\events\SystemPasswordChangedEvent'=>['app\components\handlers\SystemUserPasswordChangedHandler'],
                ]
            ],
//            'eventEmitterApi'=>[
//                'class' => 'common\components\EventEmitter',
//                'map' => [
//                    'common\events\api\MailBuy'=>['common\handlers\advert\MailBuyNotification']
//                ]
//            ],
        ]
    ],
    'modules' => [
        'dynagrid'=> [
            'class'=>'\kartik\dynagrid\Module',
            // other module settings
        ],
        'gridview'=> [
            'class'=>'\kartik\grid\Module',
            // other module settings
        ],
        'ia' => [
            'class' => 'app\modules\ia\module',
        ],
        'ita' => [
            'class' => 'app\modules\ita\module',
        ],
        'webhooks' => [
            'class' => 'app\modules\webhooks\module',
        ],
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',
            'displaySettings' => [
                'date' => 'd-m-Y',
                'time' => 'H:i:s A',
                'datetime' => 'd-m-Y H:i:s A',
            ],
            'saveSettings' => [
                'date' => 'Y-m-d',
                'time' => 'H:i:s',
                'datetime' => 'Y-m-d H:i:s',
            ],
            'autoWidget' => true,

        ]
    ],
    'params' => $params,
];

if(isset($config['params']['stage']))
{
    if ($config['params']['stage'] == 'prot')
        $config['controllerNamespace'] = 'app\controllers\prot';
}

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    $config['modules']['gii']['generators'] = [
        'kartikgii-crud' => ['class' => 'warrence\kartikgii\crud\Generator'],
    ];
}

return $config;
