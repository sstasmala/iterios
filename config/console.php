<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$params_system = require __DIR__ . '/params_system.php';
$params = array_merge($params_system,$params);

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'timeZone' => date_default_timezone_get(),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
    'params' => $params,
    'container' => [
        'singletons' => [
            'systemEmitter'=>[
                'class' => 'app\components\base\EventEmitter',
                'map' => [
                    'app\components\events\SystemUserRegistrationEvent'=>['app\components\handlers\SystemUserRegistrationHandler'],
                    'app\components\events\SystemResetPasswordEvent'=>['app\components\handlers\SystemUserResetPasswordHandler'],
                    'app\components\events\SystemTenantRegistrationEvent'=>['app\components\handlers\SystemTenantRegistrationHandler'],
                    'app\components\events\SystemUserAddedToTenantEvent'=>['app\components\handlers\SystemUserAddedToTenantHandler'],
                    'app\components\events\SystemUserInviteEvent'=>
                        ['app\components\handlers\SystemUserInviteHandler','app\components\handlers\SystemUserInviteSaveHandler'],
                    'app\components\events\SystemTaskEmailReminderEvent'=>['app\components\handlers\SystemTaskEmailReminderHandler'],
                ]
            ],
        ]
    ],
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
