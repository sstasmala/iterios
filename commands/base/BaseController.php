<?php
namespace app\commands\base;
use app\helpers\LogsCronHelper;
use app\helpers\PrintHelper;
use app\models\LogsCron;


/**
 * BaseController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
class BaseController extends \yii\console\Controller
{
    public $log_file_path = null;
    protected $log_id = null;

    public function options($actionID)
    {
        return ['log_file_path'];
    }

    public function optionAliases()
    {
        return ['l' => 'log_file_path'];
    }

    public function runAction($id, $params = [])
    {
        $session =  \Yii::$app->hasProperty('session');
        try
        {
            return parent::runAction($id, $params);
        }
        catch (\Exception $e)
        {
            PrintHelper::printMessage('Error: '.$e->getMessage(),PrintHelper::ERROR_TYPE);
            PrintHelper::printMessage('Code: '.$e->getCode(),PrintHelper::ERROR_TYPE);
            PrintHelper::printMessage('File: '.$e->getFile(),PrintHelper::ERROR_TYPE);
            PrintHelper::printMessage('Line: '.$e->getLine(),PrintHelper::ERROR_TYPE);
            PrintHelper::printMessage('Stack trace: '.PHP_EOL.$e->getTraceAsString(),PrintHelper::ERROR_TYPE);
            if(is_null($this->log_id))
            {
                $this->log_id = LogsCronHelper::setLog([
                    'time_start'=>time(),
                    'command'=>$this->id.'/'.$this->action->id,
                    'output_file'=>$this->log_file_path,
                    'status'=>LogsCron::STATUS_ERROR,
                    'time_end'=>time(),
                    'memory_usage'=>memory_get_usage().''
                ]);
            }
            else
            {
                $this->log_id = LogsCronHelper::setLog([
                    'id'=>$this->log_id,
                    'status'=>LogsCron::STATUS_ERROR,
                    'time_end'=>time(),
                    'memory_usage'=>memory_get_usage().''
                ]);
            }
        }
        return false;
    }
    public function afterAction($action, $result)
    {
        if(file_exists($this->log_file_path))
        {
            $data = file_get_contents($this->log_file_path);
            if(empty($data))
                unlink($this->log_file_path);
        }
        return parent::afterAction($action, $result);
    }
}