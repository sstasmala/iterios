<?php
/**
 * NotificationsController.php
 * @copyright ©iterios
 * @author Osmolovskiy Stas rabotasmala@gmail.com
 */

namespace app\commands;

use app\commands\base\BaseController;
use app\models\Agencies;
use app\models\AgenciesEmails;
use app\models\AgenciesPhones;
use app\models\AgenciesSocials;
use app\models\OrdersServicesLinks;
use app\models\SystemHolidays;
use app\models\Tasks;
use app\models\TasksTypes;
use app\models\Contacts;
use app\models\ContactsPhones;
use app\models\ContactsEmails;
use app\models\Languages;
use app\models\Tenants;
use app\models\User;
use app\models\AgentsMapping;
use app\models\BaseNotifications;
use app\models\Requests;
use app\models\RequestCountries;
use app\models\SystemNotifications;
use app\models\NotificationsTrigger;
use app\models\NotificationsCourse;
use app\models\NotificationsLog;
use yii\helpers\Json;
use app\models\Settings;
use app\models\EmailProviders;
use app\helpers\TwigHelper;
use app\helpers\DateTimeHelper;
use app\helpers\LogsCronHelper;
use app\helpers\MCHelper;
use app\models\LogsCron;
use app\helpers\PrintHelper;
use app\components\notifications\Notificator;
use app\components\notifications\contacts\ReceiversFactory;
use app\components\notifications\contacts\SenderFactory;
use app\components\notifications\NotificationFactory;
use app\helpers\LogHelper;
use app\models\Jobs;
use app\models\LogsEmail;
use app\models\ContactsVisas;
use app\models\ContactsPassports;


class NotificationsController extends BaseController
{
	private $curentTime = '';
	private $curentTimestamp = 0;
	private $formatPeriod = '';
	private $curentTimezone = '';
	private $tmp_placeholders = [];
    private $tenant_times = [];
    private $log_start = 0;
    private $ruleGroups = [];

    public function init()
    {
		$this->curentTimezone = date_default_timezone_get();
		$this->curentTimestamp = time();
    	$this->formatPeriod = 'Y-m-d 00:00:00';
		$this->curentTime = DateTimeHelper::convertTimestampToDateTime($this->curentTimestamp, $this->formatPeriod, $this->curentTimezone);
		$this->log_start = 0;
        $this->ruleGroups = [];
	}

	private $contactsPlaceholders = [];
    private $tenantsPlaceholders = [];
	private function getContactPlaceholders($contact, $placeholders, $tenant_id) {

        $placeholdersCont = [];
        $placeholdersTen = [];
        if (!isset($this->contactsPlaceholders[$contact->id])) {
            $placeholdersCont['Contact_Name'] = $contact->first_name . ' ' . $contact->last_name;;
            $placeholdersCont['Contact_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $contact->tenant_id . '/contacts/view?id=' . $contact->id;

            $this->contactsPlaceholders[$contact->id] = $placeholdersCont;
        } else {
            $placeholdersCont = $this->contactsPlaceholders[$contact->id];
        }

        if (!isset($this->tenantsPlaceholders[$tenant_id])) {
            $agency = Agencies::find()->where(['tenant_id' => $tenant_id])->one();
            if ($agency) {

                if ($agency->logo)
                    $placeholdersTen['Agency_Logo'] = '<img src="'.\Yii::$app->params['baseUrl'] .'/'. $agency->logo.'" id="agency_logo" height="45"/>';
                $adress = [];

                if ($agency->site)
                    $placeholdersTen['Agency_Web'] = '<a target="_blank" href="' . $agency->site . '">' . str_replace(['http://', 'https://'], '', $agency->site) . '</a>';

                if ($agency->city)
                    $adress[] = $agency->city;

                if ($agency->street)
                    $adress[] = $agency->street;

                if ($agency->address_description)
                    $adress[] = $agency->address_description;

                if ($adress)
                    $placeholdersTen['Agency_Address'] = implode(',', $adress);
            }

            $social = AgenciesSocials::find()->joinWith('type')->where(['tenant_id' => $tenant_id])->one();
            if ($social) {
                $placeholdersTen['Agency_Social'] =  '<a href="' . \Yii::$app->params['baseUrl'] .'/'. $social->value. '" target="_blank"><img src="'.\Yii::$app->params['baseUrl'] .'/'. $social->type->img.'" id="agency_logo" height="24" width="24"/></a>';
            }

            $tenant = Tenants::find()->where(['id' => $tenant_id])->one();
            if ($tenant) {
                $placeholdersTen['Agency_Name'] = $tenant->name;
            }

            $phone = AgenciesPhones::find()->where(['id' => $tenant_id])->one();
            if ($phone) {
                $placeholdersTen['Agency_Phone'] = $phone->value;
            }

            $email = AgenciesEmails::find()->where(['id' => $tenant_id])->one();
            if ($email) {
                $placeholdersTen['Agency_Email'] = $email->value;
            }

            $this->tenantsPlaceholders[$tenant_id] = $placeholdersTen;
        } else {
            $placeholdersTen = $this->tenantsPlaceholders[$tenant_id];
        }

        $placeholders = array_merge($placeholders, $placeholdersCont, $placeholdersTen);
        return $placeholders;
    }

    /**
     * @param $send
     * @return array|mixed
     */
    private function getPlaceholders($send, $user_lang, $textTemplate)
    {

    	if (empty($send))
    		return [];

        $placeholders = [];
		switch ($send['rule_id']) {
            case BaseNotifications::HOLIDAYS_SYSTEM:

                $uniq_id = 'holidays' . $send['user_type'] . $send['user_id'];

                if (isset($this->tmp_placeholders[$uniq_id])) {
                    return $this->tmp_placeholders[$uniq_id];
                } else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

                    if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($agent) {
                        $placeholders['Agent_Name'] = $agent->first_name;
                    }

                    $placeholders['Holiday_Date'] = date('d-m-y', $send['trigger_time']);

                    $holiday = SystemHolidays::find()->where(['id' => $trigger_data['element_id']])->asArray()->one();
                    if ($holiday)
                        $placeholders['Holiday_Name'] = $holiday['name'];

                    print_r($placeholders);

                    return $this->tmp_placeholders[$uniq_id] = $placeholders;
                }

            case BaseNotifications::CONTACTS_DATE_BIRTH:
                $uniq_id = $send['user_type'] . $send['user_id'];

                if (isset($this->tmp_placeholders[$uniq_id])) {
                    return $this->tmp_placeholders[$uniq_id];
                } else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

                    if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($agent) {
                        $placeholders['Agent_Name'] = $agent->first_name;
                    }

                    print_r($placeholders);

                    return $this->tmp_placeholders[$uniq_id] = $placeholders;
                }

			case BaseNotifications::TASKS_DAYS_COMPLETE:

				if ($send['user_type'] == 1)
					return [];

				$uniq_id = 'task'.$send['user_id'];
				$placeholders = [];

				if (isset($this->tmp_placeholders[$uniq_id])) {
					$placeholders =  $this->tmp_placeholders[$uniq_id];
				} else {

					$agent = User::find()->where(['id' => $send['user_id']])->one();
					$placeholders['Agent_Name'] = (!empty($agent)) ? $agent->first_name : '';
				}

				if (empty($send['trigger_id']))
					return $placeholders;

				$uniq_id = 'task'.$send['user_id'].$send['trigger_id'];
				if (isset($this->tmp_placeholders[$uniq_id]))
					return $this->tmp_placeholders[$uniq_id];

				$triggerData = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->one();
				if (empty($triggerData))
					return $placeholders;

				$task = Tasks::find()->where(['id' => $triggerData->element_id])->one();
				if (empty($task))
					return $placeholders;

                print_r($placeholders);

				$placeholders['Task_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $task->tenant_id . '/tasks/';
				$placeholders['Task_Due_date_and_Time'] = date('Y-m-d H:i:s', $task->due_date);

				return $this->tmp_placeholders[$uniq_id] = $placeholders;

			case BaseNotifications::REQUESTS_DAYS_COMPLETE:
				$uniq_id = $send['user_type'].$send['user_id'];
				$placeholders = [];
				if (isset($this->tmp_placeholders[$uniq_id])) {
					$placeholders =  $this->tmp_placeholders[$uniq_id];
				} else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

					$placeholders = [];
					if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($agent)
					    $placeholders['Agent_Name'] = $agent->first_name;

					$this->tmp_placeholders[$uniq_id] = $placeholders;
				}

				if (empty($send['trigger_id']))
					return $placeholders;

				$uniq_id = 'requests_days_complete'.$send['user_type'].$send['user_id'].$send['trigger_id'];

				if (isset($this->tmp_placeholders[$uniq_id]))
					return $this->tmp_placeholders[$uniq_id];

				$triggerData = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
				if (empty($triggerData))
					return $placeholders;

				$request = Requests::find()->where(['id' => $triggerData['element_id']])->one();
				if (empty($request))
					return $placeholders;

				$country_id = RequestCountries::find()->where(['request_id' => $request->id])->one()->country;
				if (!empty($country_id)) {

					$placeholders['Request_Country'] = (isset($this->tmp_placeholders['country_id_' . $country_id])) ? $this->tmp_placeholders['country_id_' . $country_id] : MCHelper::getCountryById($country_id, 'ru')[0]['title'];

					$this->tmp_placeholders['country_id_' . $country_id] = $placeholders['Request_Country'];
				}

				$placeholders['Request_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $request->tenant_id . '/requests/view?id=' . $request->id;

				$placeholders['Request_Dates'] = date('Y-m-d', $request->departure_date_start) . ' - ' . date('Y-m-d', $request->departure_date_end);

                print_r($placeholders);

				return $this->tmp_placeholders[$uniq_id] = $placeholders;

			case BaseNotifications::VISA_DUE_DATE:
				$uniq_id = 'visa_' . $send['user_type'].$send['user_id'];
				$placeholders = [];
				if (isset($this->tmp_placeholders[$uniq_id])) {
					return $this->tmp_placeholders[$uniq_id];
				} else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

					if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
					}

					if ($agent) {
						$placeholders['Agent_Name'] = $agent->first_name;
					}

					$visa = ContactsVisas::find()->where(['id' => $trigger_data['element_id']])->one();

					if ($visa)
						$placeholders['Visa_Expiry'] = date('d-m-Y', $visa->end_date);

					$countryCur = [];
					if ($visa && $visa->country) {
						$country = MCHelper::getCountryById($visa->country, $user_lang);
						foreach($country as $co) {
							if ($visa->country == $co['id'])
								$countryCur = $co;
						}
					}

					if ($countryCur) {
						$placeholders['Visa_Country'] = $countryCur['title'];
					}

                    print_r($placeholders);

					return $this->tmp_placeholders[$uniq_id] = $placeholders;
				}

			case BaseNotifications::PASSPORT_DUE_DATE:
				$uniq_id = 'pasp_' . $send['user_type'].$send['user_id'];
				$placeholders = [];
				if (isset($this->tmp_placeholders[$uniq_id])) {
                    /*echo "\n";
                    echo 'tmp-'.$send['trigger_id'];
				    print_r($this->tmp_placeholders[$uniq_id]);*/

					return $this->tmp_placeholders[$uniq_id];
				} else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

					if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
					}

					if ($agent) {
						$placeholders['Agent_Name'] = $agent->first_name;
					}

					$pasport = ContactsPassports::find()->where(['id' => $trigger_data['element_id']])->one();

					if ($pasport) {
						$placeholders['Int_Pass_Number'] = $pasport->serial;
						$placeholders['Int_Pass_Expiry'] = date('d-m-Y', $pasport->date_limit);
					}

                    print_r($placeholders);

					return $this->tmp_placeholders[$uniq_id] = $placeholders;
				}

			case BaseNotifications::DEPARTURE_DATE_START:
				$uniq_id = 'dep_start_'.$send['user_type'].$send['user_id'].$send['trigger_id'];

				$placeholders = [];
				if (isset($this->tmp_placeholders[$uniq_id])) {
					return $this->tmp_placeholders[$uniq_id];
				} else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();

                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

					if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
					}

					if ($agent) {
						$placeholders['Agent_Name'] = $agent->first_name;
					}

					$data = json_decode($trigger_data['data'], true);
                    $ord_id = isset($data['order_id']) ? $data['order_id'] : 0;
                    $serv_id = isset($data['service_id']) ? $data['service_id'] : 0;

                    if ($ord_id)
                        $placeholders['Order_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $send['tenant_id'] . '/orders/view?id=' . $ord_id;

					$placeholders['Departure_Date'] = date('d-m-Y', $trigger_data['trigger_time']);

                    $trigger_data_time = NotificationsTrigger::find()
                         ->where([
                            'and',
                            ['action' => BaseNotifications::DEPARTURE_TIME_START],
                            ['value' => $ord_id . $serv_id]
                        ])
                        ->one();

                    if ($trigger_data_time)
                        $dep_time = json_decode($trigger_data_time->data, true);
                        if (!empty($dep_time) && is_array($dep_time) && isset($dep_time['value']))
                            $placeholders['Departure_Time'] = $dep_time['value'];

                    print_r($placeholders);

                    return $this->tmp_placeholders[$uniq_id] = $placeholders;
				}

			case BaseNotifications::DEPARTURE_DATE_END:
				$uniq_id = 'dep_end_' . $send['user_type'].$send['user_id'].$send['trigger_id'];
				$placeholders = [];
				if (isset($this->tmp_placeholders[$uniq_id])) {
					return $this->tmp_placeholders[$uniq_id];
				} else {

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($agent) {
                        $placeholders['Agent_Name'] = $agent->first_name;
                    }

                    $data = json_decode($trigger_data['data'], true);
                    $ord_id = isset($data['order_id']) ? $data['order_id'] : 0;
                    $serv_id = isset($data['service_id']) ? $data['service_id'] : 0;

                    if ($ord_id)
                        $placeholders['Order_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $send['tenant_id'] . '/orders/view?id=' . $ord_id;

                    $placeholders['Departure_Date'] = date('d-m-Y', $trigger_data['trigger_time']);

                    $trigger_data_time = NotificationsTrigger::find()
                        ->where([
                            'and',
                            ['action' => BaseNotifications::DEPARTURE_TIME_END],
                            ['value' => $ord_id . $serv_id]
                        ])
                        ->one();

                    if ($trigger_data_time)
                        $dep_time = json_decode($trigger_data_time->data, true);
                    if (!empty($dep_time) && is_array($dep_time) && isset($dep_time['value']))
                        $placeholders['Departure_Time'] = $dep_time['value'];

                    print_r($placeholders);

                    return $this->tmp_placeholders[$uniq_id] = $placeholders;
				}

            case BaseNotifications::TASKS_OVERDUE:

                    $trigger_data = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->asArray()->one();
                    if (empty($trigger_data))
                        return false;

                    if ($send['user_type'] == 0 && !empty($send['user_id']) ) {
                        $agent =  User::find()->where(['id' => $send['user_id']])->one();
                    } else {
                        $agent = !empty($trigger_data['agent_id']) ? User::find()->where(['id' => $trigger_data['agent_id']])->one() : 0;
                    }

                    if ($send['user_type'] == 1 && !empty($send['user_id']) ) {
                        $contact =  Contacts::find()->where(['id' => $trigger_data['contact_id']])->one();
                    } else {
                        $contact = (!empty($trigger_data['contact_id'])) ? Contacts::find()->where(['id' => $trigger_data['contact_id']])->one() : 0;
                    }

                    if ($contact) {
                        $placeholders = $this->getContactPlaceholders($contact, $placeholders, $send['tenant_id']);
                    }

                    if ($agent) {
                        $placeholders['Agent_Name'] = $agent->first_name;
                    }

                    $placeholders['Task_Settings_Link'] = '';

                    $placeholders['Tasks_Link'] = \Yii::$app->params['baseUrl'] . '/ita/' . $send['tenant_id'] . '/tasks';

                    print_r($placeholders);

                    if (!empty($send['tasks_data'])) {
                        $tasks = Tasks::find()->where(['in', 'id', $send['tasks_data'], 'status' => 0])->asArray()->all();

                        $Overdue_Task_List = '';
                        if (!empty($tasks))
                            foreach($tasks as $task) {
                                $Overdue_Task_List .= $this->getTaskTemplate($task);
                            }

                        $placeholders['Overdue_Task_List'] = $Overdue_Task_List;
                    }


                    return $placeholders;


			default:
				return [];
    	}
    }

    private function getTaskTemplate($task) {
        return $this->renderPartial('@app/modules/ia/views/base-notifications/task_template', $task);
    }

    /**
     * @param $notification
     * @param $send
     * @param $user
     * @return array|string
     * @throws \Exception
     */
    //private $is_sends = [];
    public function sendEmail($notification, $send, $user)
	{
		$errors = [];
		if (empty($notification['subject']) || empty($notification['body']))
			return ['no subject or no body message'];

		if (isset($user->reminders_email) && empty($user->reminders_email))
			return ['empty reminders_email'];

		if (isset($user->reminders_from_who) && empty($user->reminders_from_who))
			return ['empty reminders_from_who'];

		$emails = [];
		if (is_array($user->email)) {
			foreach($user->email as $em) {
				$emails[] = $em['value'];
			}
		} else {
			$emails[] = $user->email;
		}

		if (is_array($user->phone) && isset($user->phone[0]['value'])) {
			$phone = $user->phone[0]['value'];
		} else {
			$phone = $user->phone;
		}

		$job_ids = [];
		foreach($emails as $email) {

			$receiver = ReceiversFactory::createReceiver($email, $phone, $user->id);
			$sender = SenderFactory::create(SenderFactory::SENDER_PUBLIC_MAIL);

			if (isset($user->reminders_email))
				$sender->setEmail($user->reminders_email);

			if (isset($user->reminders_from_who))
				$sender->setName($user->reminders_from_who);

			$notificationTempl = NotificationFactory::createFormTemplate((object)[
				'subject' => $notification['subject'],
				'body' => $notification['body'],
			], $this->getPlaceholders($send, $user->lang, $notification['body']));

			$mailer = Settings::findOne(['key'=>Settings::PROVIDER_PUBLIC]);

			if (is_null($mailer)) {
				$errors[] = $user->id .': no mailer';
				continue;
			}

			$provider = EmailProviders::findOne(['id'=>$mailer->value]);
			if (is_null($provider)) {
				$errors[] = $user->id .': no provider';
				continue;
			}

			$mailer = [
				'class'=>$provider->provider,
				'param'=>$provider->api_key
			];

			$log = LogHelper::saveEmailLog($notificationTempl, $receiver, null, LogsEmail::TYPE_PUBLIC);
			$notific = new Notificator([
				'notification'=>$notificationTempl,
				'receivers'=>$receiver,
				'sender'=>$sender,
				'email_channel'=>$mailer,
				'email_log'=>$log,
				'channels'=>[Notificator::CHANNEL_EMAIL],
				'provider'=>$provider
			]);

			$job = new Jobs();
			$job->executor = 'app\components\executors\NotificationExecutor';
			$job->data = serialize($notific);

			if (!$job->save()) {
				$errors[] = $user->id . 'no job save';
				continue;
			}

			$job_ids[] = $log->id;
			PrintHelper::printMessage('job send save',PrintHelper::SUCCESS_TYPE);
		}

		if ($errors)
			return $errors;

		return strip_tags(implode(',', $job_ids).'||'.$notification['subject']."||".$notification['body']);
	}

    /**
     * @param $notification
     * @param $send
     * @param $user
     * @return array|string
     */
    public function sendNot($notification, $send, $user, $userObj)
	{
		if (empty($notification['system_notification']) ||  $send['user_type'] == 1)
			return ['empty system_notification OR user_type - contact'];
		$notification['system_notification'] = TwigHelper::render($this->getPlaceholders($send, $userObj->lang, $notification['system_notification']),$notification['system_notification']);

		$model = new SystemNotifications();
		$model->read = 0;
		$model->text = $notification['system_notification'];
		$model->user_id = $send['user_id'];
		$model->tenant_id = $send['tenant_id'];

		if (!$model->save())
			return ['dont save notification'];

		PrintHelper::printMessage('system_notification save',PrintHelper::SUCCESS_TYPE);

		return strip_tags($model->id.'||'.$notification['system_notification']);
	}

    /**
     * @param $notification
     * @param $send
     * @param $user
     * @return array|string
     */
    public function sendTask($notification, $send, $user, $userObj)
    {
		if (empty($notification['task_name']) || $send['user_type'] == 1)
			return ['empty task_name OR user_type contact'];


		$notification['trigger_action'] = Json::decode($notification['trigger_action']);
		$rule = $notification['trigger_action']['rules'][0];
		$due_date = strtotime($send['date']);

		if (in_array($rule['id'], ['requests.days_complete', 'tasks.days_complete']))
			$due_date = $due_date + ( (int)$rule['value'] * (60*60*24) );

		if ($notification['trigger_day'] > 0)
			$due_date = $due_date + ((int)$notification['trigger_day'] * (60*60*24));

		$model = Tasks::findOne([
			'assigned_to_id' => $send['user_id'],
			'due_date' => $due_date,
			'tenant_id' => $send['tenant_id'],
		]);

		if (!empty($model))
			return ['error, task is yes'];

		if ($notification['is_slave_contact']) {
			$trigger = NotificationsTrigger::find()->where(['id' => $send['trigger_id']])->one();
		}

		$set_contact_id = !empty($trigger) ? (int)$trigger->contact_id : 0;
		$notification['task_name'] = TwigHelper::render($this->getPlaceholders($send, $userObj->lang, $notification['task_name']),$notification['task_name']);
		$notification['task_body'] = TwigHelper::render($this->getPlaceholders($send, $userObj->lang, $notification['task_body']),$notification['task_body']);

		$model = new Tasks();
		$model->title = $notification['task_name'];
		$model->description = $notification['task_body'];
		$model->assigned_to_id = $send['user_id'];
		$model->created_by = $send['user_id'];
		$model->updated_by = $send['user_id'];
		$model->tenant_id = $send['tenant_id'];
		$model->due_date = $due_date;
		$model->contact_id = $set_contact_id;

		$type = TasksTypes::findOne(['default' => true]);
		if ( ! is_null($type)) {
			$model->type_id = $type->id;
		}

		$model->email_reminder = $model->due_date - (60 * 60 * 3); // 3 hour

		if ($model->save()) {
			PrintHelper::printMessage('task create: ' . $model->id,PrintHelper::SUCCESS_TYPE);
			return strip_tags($notification['task_name'].'||'.$notification['task_body'].'||'.$due_date);
		}

		return ['error, no save task'];
	}

	private function checkSends($send) {
        if ($send['rule_id'] == BaseNotifications::TASKS_OVERDUE) {

            $where = [
                'and',
                ['status' => 1],
                ['id' => $send['noting_id']]
            ];

            $notification = BaseNotifications::find()->where($where)->one();

            if (!empty($notification)) {

                $where = [
                    'and',
                    ['action' => BaseNotifications::TASKS_DAYS_COMPLETE ],
                    ['<', 'trigger_time', time()],
                    ['value' => $send['noting_id']]
                ];

                if ($send['user_type'] == 0) {
                    $where[] = ['agent_id' => $send['user_id']];
                } else {
                    $where[] = ['contact_id' => $send['user_id']];
                }

                //print_r($where);exit();

                $notifications_check = NotificationsTrigger::find()
                    ->where($where) // to delete
                    ->orderBy('updated_at')
                    ->asArray()
                    ->all();

                if (!empty($notifications_check)) {
                    $send['tasks_data'] = array_column($notifications_check, 'element_id');
                }
            }
        }

        return $send;
    }

    /**
     * @return bool
     * @throws \Exception
     */

    public function actionSend()
    {
    	$this->init();

    	$this->log_id = LogsCronHelper::setLog([
            'time_start' => time(),
            'command' => \Yii::$app->controller->route,
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_EXECUTING,
            'memory_usage' => memory_get_usage().''
        ]);

        $selectTime = date('Y-m-d H:i');
        $sends = NotificationsCourse::find()
            ->where(
                ['and',
                    ['send_status' => 0],
                    ['like', 'timezone_date', $selectTime . '%', false],
                    ['!=', 'rule_id', 'contacts.date_of_birth'],
                    ['!=', 'rule_id', 'holidays.system'],
                ])
            ->all();

        if (empty($sends))
            $sends = [];

		$selectTime = date('m-d H:i');

		$sendsDr = NotificationsCourse::find()
		->where(
			['and',
				['like', 'timezone_date', '%'. $selectTime . '%', false],
				['in', 'rule_id', ['holidays.system', 'contacts.date_of_birth']]
		])
		->all();

		if (!empty($sendsDr)) {
			foreach($sendsDr as $sdr) {
			    $sends[] = $sdr;
			}
		}

		if (empty($sends))  {
			LogsCronHelper::deleteLog($this->log_id);
			if (is_file($this->log_file_path))
				unlink($this->log_file_path);

			return true;
		}

		foreach($sends as $send) {
            $sendArr = $send->toArray();

            $sendArr = $this->checkSends($sendArr);

			PrintHelper::printMessage('send||'.$sendArr['user_type'].'-'.$sendArr['user_id'].'||'.$sendArr['rule_id'].'||'.$sendArr['noting_id'].'||'.$sendArr['timezone_date'],PrintHelper::SUCCESS_TYPE);

			// prepare log data
			$notification_log_data = [
				'course_id' => $sendArr['id'],
				'trigger_id' => (int)$sendArr['trigger_id'],
				'reminder_id' => (int)$sendArr['noting_id'],
				'system_notifications_id' => 0,
				'jobs_ids' => '',
				'send_data' => ''
			];

			if ($sendArr['user_type'] == 0) {
				$userThis = User::find()->where(['id' => $sendArr['user_id']])->one();
				if (empty($userThis))
					continue;

				$tenant = Tenants::find()->where(['id' => $sendArr['tenant_id']])->one();
				if (empty($tenant))
					continue;

				$lang = Languages::find()->where(['id' => $tenant->language_id])->one();

				$userObj = (object)[
					'id' => $userThis->id,
					'email' => $userThis->email,
					'phone' => $userThis->phone,
					'lang' => $lang->iso
				];

				$notification_log_data['contact_id'] = 0;
				$notification_log_data['agent_id'] = $userObj->id;
			}

			if ($sendArr['user_type'] == 1) {
				$userThis = Contacts::find()->where(['id' => $sendArr['user_id']])->one();
				if (empty($userThis))
					continue;

				$tenant = Tenants::find()->where(['id' => $sendArr['tenant_id']])->one();
				if (empty($tenant))
					continue;

				$lang = Languages::find()->where(['id' => $tenant->language_id])->one();

				//reminders_email
				$userObj = (object)[
					'id' => $userThis->id,
					'email' => ContactsEmails::find()->where(['contact_id' => $userThis->id])->asArray()->all(),
					'phone' => ContactsPhones::find()->where(['contact_id' => $userThis->id])->asArray()->all(),
					'reminders_email' => $tenant->reminders_email,
					'reminders_from_who' => $tenant->reminders_from_who,
					'lang' => $lang->iso
				];

				$notification_log_data['contact_id'] = $userObj->id;
				$notification_log_data['agent_id'] = 0;
			}

			// берем напоминание
			$notification = BaseNotifications::find()->where([
				'and',
				['status' => 1],
				['id' => $sendArr['noting_id']]
			])->translate($userObj->lang)->asArray()->one();
			if (empty($notification))
				continue;

			$is_sends = [];
			if ($notification['is_email']) {
				$res = $this->sendEmail($notification, $sendArr, $userObj);

				// save send log
				$notification_log = new NotificationsLog();
				foreach ($notification_log_data as $k => $v)
					$notification_log->{$k} = $v;

				if (!is_array($res)) {
					$jobs_ids = explode('||', $res);
					$notification_log->jobs_ids = $jobs_ids[0];
				}

				$notification_log->channel = Notificator::CHANNEL_EMAIL;
				$notification_log->send_data =  (!is_array($res)) ? $res : '';
				$is_sends['statuses'][] = $notification_log->status = !is_array($res) ? 1 : 0;
				$is_sends['errors']['is_email'] = $notification_log->errors = is_array($res) ? implode('||', $res) : '';
				$notification_log->save();
				// @end save send log
			}

			if ($notification['module'] == 'agents') {
				if ($notification['is_system_notification']) {
					$res = $this->sendNot($notification, $sendArr, $userThis, $userObj);

					// save send log
					$notification_log = new NotificationsLog();
					foreach ($notification_log_data as $k => $v)
						$notification_log->{$k} = $v;

					if (!is_array($res)) {
						$system_notifications_id = explode('||', $res);
						$notification_log->system_notifications_id = $system_notifications_id[0];
					}

					$notification_log->channel = Notificator::CHANNEL_SYSTEM;
					$notification_log->send_data =  (!is_array($res)) ? $res : '';
					$is_sends['statuses'][] = $notification_log->status = (!is_array($res)) ? 1 : 0;
					$is_sends['errors']['is_system_notification'] = $notification_log->errors = (is_array($res)) ? implode('||', $res) : '';
					$notification_log->save();
					// @end save send log
				}

				if ($notification['is_task']) {
					$res = $this->sendTask($notification, $sendArr, $userThis, $userObj);

					// save send log
					$notification_log = new NotificationsLog();
					foreach ($notification_log_data as $k => $v)
						$notification_log->{$k} = $v;

					$notification_log->channel = Notificator::CHANNEL_TASK;
					$notification_log->send_data =  (!is_array($res)) ? $res : '';
					$is_sends['statuses'][] = $notification_log->status = (is_array($res)) ? 0 : 1;
					$is_sends['errors']['is_task'] = $notification_log->errors = (is_array($res)) ? implode('||', $res) : '';
					$notification_log->save();
					// @end save send log
				}
			}

			// update status this
            if (isset($is_sends['statuses']))
			    $send->send_status =  (in_array(0, $is_sends['statuses'])) ? 0 : 1;

			$send->errors = (isset($is_sends['errors']) && $is_sends['errors']) ? Json::encode($is_sends['errors']) : '';
			$send->save();

			// PrintHelper::printMessage('Change Status - ' . $send->id.':'.$send->send_status,PrintHelper::SUCCESS_TYPE);
		}

		$this->log_id = LogsCronHelper::setLog([
			'id' => $this->log_id,
			'time_end' => time(),
			'command' => \Yii::$app->controller->route,
			'output_file' => $this->log_file_path,
			'status' => LogsCron::STATUS_SUCCESS,
			'memory_usage' => memory_get_usage().''
		]);

		return true;
    }

    /**
     * @return bool
     */
    public function actionDay()
    {
        $this->init();

        $this->log_id = LogsCronHelper::setLog([
            'time_start' => time(),
            'command' => \Yii::$app->controller->route,
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_EXECUTING,
            'memory_usage' => memory_get_usage() . ''
        ]);

        $notifications = BaseNotifications::find()->where(['status' => 1])
            ->asArray()->all();

        if (empty($notifications)) {
            return true;
        }

        foreach ($notifications as $notif) {
            $this->prepareConditionDay($notif);
        }

        if ($this->log_start == 1) {
            $this->log_id = LogsCronHelper::setLog([
                'id' => $this->log_id,
                'time_end' => time(),
                'command' => 'notifications/day',
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_SUCCESS,
                'memory_usage' => memory_get_usage() . ''
            ]);
        } else {
             //LogsCronHelper::deleteLog($this->log_id);
             if (is_file($this->log_file_path)) {
				// unlink($this->log_file_path);
             }
        }

		return true;
    }

    /**
     * @param $notif
     * @return bool
     */
    private function prepareConditionDay($notif)
    {
		if (empty($notif['trigger_action'])) return false;

		PrintHelper::printMessage('notification:' . $notif['id'],PrintHelper::DEFAULT_TYPE);

		$notif['trigger_action'] = Json::decode($notif['trigger_action']);
		$notif['condition'] = !empty($notif['condition']) ? Json::decode($notif['condition']) : false;
		$notif_action_rule = $notif['trigger_action']['rules'][0];
		$operation = 'get_checks_'.str_replace('.', '_', $notif_action_rule['id']);

		if (!method_exists($this, $operation)) return false;
		PrintHelper::printMessage('action:' . $notif_action_rule['id'],PrintHelper::DEFAULT_TYPE);

		$notifications_course = $this->$operation($notif_action_rule['id'], [$notif_action_rule], $notif, []);
		if (empty($notifications_course)) return false;

		$notifications_course = $notifications_course[$notif_action_rule['id']];

		// проверим дополнительные условия
		$notifications_course_condition = [];
		if (!empty($notif['condition'])) {
			$groupRules = [];
			foreach($notif['condition']['rules'] as $rule)
				$groupRules[$rule['id']][] = $rule;

			foreach($groupRules as $idRule => $gRules) {
				$operation = 'get_checks_'.str_replace('.', '_', $idRule);
				if (!method_exists($this, $operation))
					continue;

				$notifications_course_condition = $this->$operation($idRule, $gRules, $notif, $notifications_course_condition);
				if (empty($notifications_course_condition))
					continue;
			}
		}

    	// сгруппируем пользователей
		$notification_result = [];
		foreach($notifications_course as $notice) {
			// if (!empty($notification_result['user_ids']))
			//	$notice['user_ids'] = array_merge($notice['user_ids'], $notification_result['user_ids']);

			$notification_result[] = $notice;
    	}

		if ($notif['condition']['condition'] == 'AND') {
		    foreach($notification_result as $resk => $res) {
                foreach ($res['user_ids'] as $usk => $us_id) {
                    $isy = 0;
                    foreach ($notifications_course_condition as $gId => $sends)
                        foreach ($sends as $send)
                            if (in_array($us_id, $send['user_ids']))
                                $isy = 1;

                    if ($isy == 0)
                        unset($res['user_ids'][$usk]);
                }

                $notification_result[$resk] = $res;
            }

		} elseif(!empty($notifications_course_condition)) {
            foreach($notification_result as $resk => $res) {
                foreach ($notifications_course_condition as $gId => $sends) {
                    foreach ($sends as $send) {
                        $res['user_ids'] = array_unique(array_merge($res['user_ids'], $send['user_ids']));
                    }
                }
            }
		}

		if (empty($notification_result))
			return false;


		foreach ($notification_result as $elem_id => $notres) {

            $notres['user_ids'] = array_unique($notres['user_ids']);

            if ($notres['user_type'] == 0)
                $users = User::find()->where(['in', 'id', $notres['user_ids']])->asArray()->all();

            if ($notres['user_type'] == 1)
                $users = Contacts::find()->where(['in', 'id', $notres['user_ids']])->asArray()->all();

            $sends = [];
            $sendtime = 0;
            foreach ($users as $user) {

                $timezone = $this->getTenantTimezone($notres['tenant_id']);
                if (empty($timezone)) continue;

                //$notres['tenant_id'] = $this->getTenantIdByUser($user);
                $notres['user_id'] = $user['id'];
                $sendtime = (empty($sendtime)) ? $notres['send_time'] : $sendtime;
                $notres = $this->getTimesData($sendtime, $timezone, $notres);
                $taskTime = date($this->formatPeriod, strtotime($notres['send_timezone']));

                if (strtotime($this->curentTime) != strtotime($taskTime)) {

                    PrintHelper::printMessage("\n" . 'skip send:user_id-'.$user['id']. '('.$notres['user_type'].'):' .$notres['rule_id']. ':' . "\n --curent: " .$this->curentTime."\n --task:   ".$taskTime,PrintHelper::WARNING_TYPE);
                    continue;
                } //to delete

                PrintHelper::printMessage("\n" . 'yes send:user_id-'.$user['id']. '('.$notres['user_type'].'):' .$notres['rule_id']. ':' . "\n curent----: " .$this->curentTime."\n timezone--: " . $notres['send_timezone'] ,PrintHelper::SUCCESS_TYPE);
                $sends[] = $notres;
                $logs[] = '__course log --- user_id: ' . $user['id'] . '(' . $notres['user_type'] . ') -- ' . $notres['send_timezone'];
            }

            if (empty($sends))
                continue;

            // для tasks.overdue поставим статус что напоминание о задачах поставлено в очередь
            $complete_tasks_ids = [];
            foreach($sends as $s) {
                if ($s['rule_id'] == BaseNotifications::TASKS_OVERDUE)
                $complete_tasks_ids = array_merge($complete_tasks_ids, $s['triggers_ids']);
            }

            // поставить пометку, и удалять когда обновление нотификации происходит, в пометку добавить нотификатион ид
            if (!empty($complete_tasks_ids))
                NotificationsTrigger::updateAll(['value' => $notif['id']], ['in', 'id', $complete_tasks_ids]);

            PrintHelper::printMessage(" \n " . implode(" \n ", $logs), PrintHelper::SUCCESS_TYPE);

            $this->sendNotice($notif, $sends);
        }
	}


    /**
     * @param $user
     * @return int
     */
    private function getTenantIdByUser($user)
	{
		if (isset($user['tenant_id']))
		    return (int)$user['tenant_id'];

		if (isset($user['current_tenant_id']))
		    return (int)$user['current_tenant_id'];

		return 0;
	}

    /**
     * @param $user
     * @return int
     */
    private function getAgentIdByUser($user)
	{
		if (isset($user['responsible']))
			return (int)$user['responsible'];

		return 0;
	}


    /**
     * @param $user
     * @return bool|mixed
     */
    private function getTenantTimezone($tenant_id)
    {
        if (isset($this->tenant_times[$tenant_id]))
            return $this->tenant_times[$tenant_id];

        $timezone = Tenants::find()->where(['id' => $tenant_id])->one();
        if (empty($timezone))
            return $this->curentTimezone;
        
        return $this->tenant_times[$tenant_id] = $timezone->timezone;
    }

    /**
     * @param $notif
     * @param $sends
     * @return bool
     */
    private function sendNotice($notif, $sends)
    {
    	$new = 0;
		foreach($sends as $send) {

			unset($model);
			$model = NotificationsCourse::findOne([
				'rule_id' => $send['rule_id'],
				'noting_id' => $send['noting_id'],
				'user_id' => $send['user_id'],
				'date' => $send['time'],
				'user_type' => $send['user_type'],
                'trigger_id' => $send['trigger_id'],
			]);

			if (!empty($model))
				continue;

			$new = 1;
            $model = new NotificationsCourse();
            $model->rule_id = $send['rule_id'];
            $model->noting_id = $send['noting_id'];
            $model->user_type = $send['user_type'];
            $model->user_id = $send['user_id'];
            $model->date = $send['time'];
            $model->time = $send['send_time'];
            $model->day = $send['send_date'];
            $model->timezone_date = $send['send_timezone'];
            $model->timezone_time = $send['send_timezone_time'];
            $model->timezone_day = $send['send_timezone_date'];
            $model->created_at = time();
            $model->send_status = 0;
            $model->tenant_id = $send['tenant_id'];
            $model->trigger_id = $send['trigger_id'];

            $model->save();
		}

		if ($new && $this->log_start == 0)
			$this->log_start = $new;

		return $new;
	}

    /**
     * @param $notif
     * @param $sendtime
     * @return bool|false|int
     */
    private function getTimeByNotice($notif, $sendtime)
    {
		switch ($notif['trigger']) {
			case 0: return $sendtime;
			case 1: return strtotime(date('Y-m-d', $sendtime) .' '. $notif['trigger_time']);
			case 2: return strtotime(date('Y-m-d', ($sendtime - ($notif['trigger_day']*86400))).' '.$notif['trigger_time']);
			case 3: return strtotime(date('Y-m-d', ($sendtime + ($notif['trigger_day']*86400))).' '.$notif['trigger_time']);
		}

		return false;
	}

    /**
     * @param $sendtime
     * @param $timezone
     * @param $notification_result
     * @return mixed
     */
    private function getTimesData($sendtime, $timezone, $notification_result)
    {
    	// вычислим смещение (до секунд)
    	$tc = DateTimeHelper::convertTimestampToDateTime($sendtime, "H", $this->curentTimezone);
    	$tz = DateTimeHelper::convertTimestampToDateTime($sendtime, "H", $timezone);
    	$tzres = ($tc - $tz) * 60 * 60;

        $notification_result['time'] = DateTimeHelper::convertTimestampToDateTime($sendtime, "Y-m-d H:i:s", $this->curentTimezone);
        $notification_result['send_time'] = DateTimeHelper::convertTimestampToDateTime($sendtime, "H:i:s", $this->curentTimezone);
        $notification_result['send_date'] = DateTimeHelper::convertTimestampToDateTime($sendtime, "m.d", $this->curentTimezone);

        // прибавляем смещение
        $sendtime += $tzres;
        $notification_result['send_timezone'] = DateTimeHelper::convertTimestampToDateTime($sendtime, "Y-m-d H:i:s", $this->curentTimezone);
        $notification_result['send_timezone_time'] = DateTimeHelper::convertTimestampToDateTime($sendtime, 'H:i:s', $this->curentTimezone);
        $notification_result['send_timezone_date'] = DateTimeHelper::convertTimestampToDateTime($sendtime, 'm.d', $this->curentTimezone);
        return $notification_result;
    }

    /**
     * @param $notif
     * @param $path_day
     * @return array
     */
    private function getTriggerDays($notif, $path_day)
    {

		$trigger_day = ($notif['trigger'] == BaseNotifications::TRIGGER_TYPE_COUNT_DAYS_BEFORE) ? ($notif['trigger_day'] * 86400) : 0;

		$trigger_day = ($notif['trigger'] == BaseNotifications::TRIGGER_TYPE_COUNT_DAYS_AFTER) ? 0 - ($notif['trigger_day'] * 86400) : $trigger_day;

		$notif_action_rule = $notif['trigger_action']['rules'][0];

		if (in_array($notif_action_rule['id'], ['requests.days_complete', 'tasks.days_complete']))
			$trigger_day += $notif_action_rule['value'] * 86400;

		$trigger_days[] = DateTimeHelper::convertTimestampToDateTime($this->curentTimestamp - 86400 + $trigger_day, $path_day, $this->curentTimezone);
		$trigger_days[] = DateTimeHelper::convertTimestampToDateTime($this->curentTimestamp + $trigger_day, $path_day, $this->curentTimezone);
		$trigger_days[] = DateTimeHelper::convertTimestampToDateTime($this->curentTimestamp + 86400 + $trigger_day, $path_day, $this->curentTimezone);

		return array_unique($trigger_days);
	}

    /**
     * @param $tenant_id
     * @return array|bool
     */
    private function getUsersByTenantId($tenant_id) {
		$users_to_ids = AgentsMapping::find()->where(['tenant_id' => $tenant_id])->asArray()->all();
		if (empty($users_to_ids)) return false;
		$users_to_ids = array_column($users_to_ids, 'user_id');
		return array_unique($users_to_ids);
	}

    /**
     * @param $notch
     * @param $notif
     * @return array|bool
     */
    private function getUsersByNotch($notch, $notif)
    {
		if ($notif['module'] != 'agents')
			return [$notch['contact_id']];

		if ($notif['subscribers'] != 'all')
            return [$notch['agent_id']];

        return $this->getUsersByTenantId($notch['tenant_id']);
	}

    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
    private function get_checks_tasks_overdue($ruleId, $gRules, $notif, $notifications_course, $isvalue = '')
	{
        $time = strtotime(date('Y-m-d') .' '. date('23:59:59'));
        $where = [
            'and',
            ['action' => BaseNotifications::TASKS_DAYS_COMPLETE ],
            ['<', 'trigger_time', $time],
            ['value' => $isvalue],
            [Tasks::tableName().'.status' => 0]
        ];

        $notifications_check = NotificationsTrigger::find()
            ->select(NotificationsTrigger::tableName() . '.*')
            ->leftJoin(Tasks::tableName(), NotificationsTrigger::tableName() . '.element_id = ' .Tasks::tableName(). '.id')
            ->where($where) // to delete
            ->orderBy('updated_at')
            ->asArray()
            ->all();

        if (empty($notifications_check))
		    return false;

		$triggers_ids = [];
        foreach ($notifications_check as $notch) {
            $users_to_ids = $this->getUsersByNotch($notch, $notif);
            if (empty($users_to_ids))
                continue;

            $key = ($isvalue == '') ? implode(':', $users_to_ids) : $notch['id'];
            $triggers_ids[$key][] = $notch['id'];
        }

		foreach ($notifications_check as $notch) {
            if (empty($notch['trigger_time']))
                continue;

			$trigger_time = strtotime(date('Y-m-d') .' '. $notif['trigger_time']);

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $trigger_time);
			if (empty($send_time))
				continue;

            $users_to_ids = $this->getUsersByNotch($notch, $notif);
            if (empty($users_to_ids))
                continue;

            $key = ($isvalue == '') ? implode(':', $users_to_ids) : $notch['id'];

			$notifications_course[$ruleId][$key] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
                'triggers_ids' => $triggers_ids[$key]
			];
		}

		return $notifications_course;
	}

	/**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
    private function get_checks_holidays_system($ruleId, $gRules, $notif, $notifications_course)
	{
        $rules = !empty($notif['condition']['rules']) ? $notif['condition']['rules'] : false;
		$path_day = 'm.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

        $where = [
            'and',
            ['action' => $ruleId],
            ['in', 'trigger_day', $trigger_days]
        ];

        if ($rules) {
            $holidays_ids = array_column($rules, 'value');
            if ($holidays_ids) {
                foreach($holidays_ids as $ksi => &$si) {
                    $si = (int) $si;
                    if (empty($si))
                        unset($holidays_ids[$ksi]);
                }
            }

            if (!empty($holidays_ids)) {
                $holidays_ids = SystemHolidays::find()->where(['in', 'id', $holidays_ids])->asArray()->all();
                $holidays_ids = array_column($holidays_ids, 'id');
                if (!empty($holidays_ids))
                    $where[] = ['in', 'element_id', $holidays_ids];
            }
        }

        $notifications_check = NotificationsTrigger::find()
            ->where($where) // to delete
            ->orderBy('updated_at')
            ->asArray()->all();

		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			$notch['trigger_time'] = date("-m-d H:i:s", $notch['trigger_time']);
			$notch['trigger_time'] = strtotime(date('Y') . $notch['trigger_time']);

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);
			if (empty($send_time))
				continue;
/*


			print_r($notch);exit();

            // element_id

            if ($notres['rule_id'] == BaseNotifications::HOLIDAYS_SYSTEM) {
                var_dump($elem_id);
                print_r($notres);exit();
                $ten = Tenants::find()->where(['!=', 'country', ''])->asArray()->one();
                exit();
            }


            $ten = Tenants::find()->where(['!=', 'country', ''])->asArray()->one();
            print_r($ten);exit();

*/


			// все агенты
			if ($notif['module'] == 'agents') {
				$users = User::find()->select(['id'])->asArray()->all();
			}

			if ($notif['module'] == 'contacts') {
				$users = Contacts::find()->select(['id'])->asArray()->all();
			}

			$users_to_ids = array_column($users, 'id');
			if (empty($users_to_ids))
			    continue;

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
	}

	/**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
	private function get_checks_orders_departure_date_end($ruleId, $gRules, $notif, $notifications_course) {

        $rules = !empty($notif['condition']['rules']) ? $notif['condition']['rules'] : false;

	    $path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

        $where = [
            'and',
            ['action' => $ruleId],
            ['in', 'trigger_day', $trigger_days]
        ];

        if ($rules) {
            $services_ids = array_column($rules, 'value');
            if ($services_ids) {
                foreach($services_ids as $ksi => &$si) {
                    $si = (int) $si;
                    if (empty($si))
                        unset($services_ids[$ksi]);
                }
            }

            if (!empty($services_ids)) {
                $links_ids = OrdersServicesLinks::find()->where(['in', 'service_id', $services_ids])->asArray()->all();
                $links_ids = array_column($links_ids, 'id');
                if (!empty($links_ids))
                    $where[] = ['in', 'element_id', $links_ids];
            }

        }

        $notifications_check = NotificationsTrigger::find()
            ->where($where) // to delete
            ->orderBy('updated_at')
            ->asArray()->all();

		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);

			if (empty($send_time))
			    continue;

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$users_to_ids = array_unique($users_to_ids);

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
	}

 	/**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
	private function get_checks_orders_departure_date_start($ruleId, $gRules, $notif, $notifications_course) {

        $rules = !empty($notif['condition']['rules']) ? $notif['condition']['rules'] : false;

		$path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

		$where = [
            'and',
            ['action' => $ruleId],
            ['in', 'trigger_day', $trigger_days]
        ];

		if ($rules) {
		    $services_ids = array_column($rules, 'value');
		    if ($services_ids) {
		        foreach($services_ids as $ksi => &$si) {
		            $si = (int) $si;
		            if (empty($si))
		                unset($services_ids[$ksi]);
                }
            }

            if (!empty($services_ids)) {
                $links_ids = OrdersServicesLinks::find()->where(['in', 'service_id', $services_ids])->asArray()->all();
                $links_ids = array_column($links_ids, 'id');
                if (!empty($links_ids))
                    $where[] = ['in', 'element_id', $links_ids];
		    }

        }

		$notifications_check = NotificationsTrigger::find()
			->where($where) // to delete
			->orderBy('updated_at')
			->asArray()->all();

		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);

			if (empty($send_time))
			    continue;

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$users_to_ids = array_unique($users_to_ids);

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
	}


    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
	private function get_checks_contacts_passport_due_date($ruleId, $gRules, $notif, $notifications_course) {
		$path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

		$notifications_check = NotificationsTrigger::find()
			->where([
				'and',
				['action' => $ruleId],
				['in', 'trigger_day', $trigger_days]
			]) // to delete
			->orderBy('updated_at')
			->asArray()->all();


		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);

			if (empty($send_time))
			    continue;

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$users_to_ids = array_unique($users_to_ids);

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
	}

	/**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
	private function get_checks_contacts_visa_due_date($ruleId, $gRules, $notif, $notifications_course) {
		$path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

		$notifications_check = NotificationsTrigger::find()
			->where([
				'and',
				['action' => $ruleId],
				['in', 'trigger_day', $trigger_days]
			]) // to delete
			->orderBy('updated_at')
			->asArray()->all();


		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);

			if (empty($send_time))
			    continue;

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$users_to_ids = array_unique($users_to_ids);

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
	}


    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
    private function get_checks_tasks_days_complete($ruleId, $gRules, $notif, $notifications_course)
	{
		if ($notif['module'] != 'agents')
			return false;


		$path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);
		$notifications_check = NotificationsTrigger::find()
		->where(['action' => $ruleId])
		->where([
			'and',
			['action' => $ruleId],
			['in', 'trigger_day', $trigger_days]
		])// delete to
		->orderBy('updated_at')
		->asArray()->all();

		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);
			if (empty($send_time))
			    continue;

			// отнимаем дней до
			if (isset($gRules[0]['value']) && (int)$gRules[0]['value'] != 0) {
				$send_time = $send_time - ($gRules[0]['value']*86400);
			}

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => 0,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
    }

    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
    private function get_checks_requests_days_complete($ruleId, $gRules, $notif, $notifications_course)
    {
		$path_day = 'Y.m.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

		$notifications_check = NotificationsTrigger::find()
			->where([
				'and',
				['action' => $ruleId],
				['in', 'trigger_day', $trigger_days]
			]) // to delete
			->orderBy('updated_at')
			->asArray()->all();

		// responsible - пользователь, чей контакт
		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);

			if (empty($send_time))
			    continue;

			// отнимаем дней до
			if (isset($gRules[0]['value']) && (int)$gRules[0]['value'] != 0) {
				$send_time = $send_time - ($gRules[0]['value']*86400);
			}

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$users_to_ids = array_unique($users_to_ids);

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
    }

    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return mixed
     */
    private function get_checks_requests_status_id($ruleId, $gRules, $notif, $notifications_course)
    {
    	// get tags ids
		$statuses = [];
		foreach($gRules as $gRule) {

			if ($gRule['operator'] == 'equal') {
				$statuses['in'][] = $gRule['value'];
			} else {
				$statuses['not'][] = $gRule['value'];
			}
		}

    	$where = [
			'and',
			['action' => $ruleId],
		];

		if (!empty($statuses['in'])) {
			$statuses['in'] = array_unique($statuses['in']);
			$where[] = ['in', 'value_int', $statuses['in']];
		}

		if (!empty($statuses['not'])) {
			$statuses['not'] = array_unique($statuses['not']);
			$where[] = ['not in', 'value_int', $statuses['not']];
		}

		// проверим в каких контактах добавили новый тег
		$notifications_trigger = NotificationsTrigger::find()->where($where)->orderBy('updated_at')
			->asArray()->all();

		foreach ($notifications_trigger as $notch) {
			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
    }

    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return bool
     */
    private function get_checks_contacts_date_of_birth($ruleId, $gRules, $notif, $notifications_course)
    {

		$path_day = 'm.d';
		$trigger_days = $this->getTriggerDays($notif, $path_day);

		// проверим изменяли ли день рождение
		$notifications_check = NotificationsTrigger::find()
			->where([
				'and',
				['action' => $ruleId],
				['in', 'trigger_day', $trigger_days]
			]) // to delete
			->orderBy('updated_at')
			->asArray()->all();

		// responsible - пользователь, чей контакт
		if (empty($notifications_check))
		    return false;

		foreach ($notifications_check as $notch) {

			$notch['trigger_time'] = date("-m-d H:i:s", $notch['trigger_time']);
			$notch['trigger_time'] = strtotime(date('Y') . $notch['trigger_time']);

			unset($send_time);
			$send_time = $this->getTimeByNotice($notif, $notch['trigger_time']);
			if (empty($send_time))
				continue;

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids))
			    continue;

			$notifications_course[$ruleId][$notch['id']] = [
                'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'send_time' => $send_time,
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];
		}

		return $notifications_course;
    }

    /**
     * @param $ruleId
     * @param $gRules
     * @param $notif
     * @param $notifications_course
     * @return mixed
     */
    private function get_checks_contacts_tag_id($ruleId, $gRules, $notif, $notifications_course)
    {
		$tagsIds = [];
		foreach($gRules as $gRule) {

			if ($gRule['operator'] == 'equal') {
				$tagsIds['in'][] = $gRule['value'];
			} else {
				$tagsIds['not'][] = $gRule['value'];
			}
		}

		$where = [
			'and',
			['action' => $ruleId],
		];

		if (!empty($tagsIds['in'])) {
			$tagsIds['in'] = array_unique($tagsIds['in']);
			$where[] = ['in', 'element_id', $tagsIds['in']];
		}

		if (!empty($tagsIds['not'])) {
			$tagsIds['not'] = array_unique($tagsIds['not']);
			$where[] = ['not in', 'element_id', $tagsIds['not']];
		}

		// проверим в каких контактах добавили новый тег
		$notifications_trigger = NotificationsTrigger::find()->where($where)->orderBy('updated_at')
			->asArray()->all();

		foreach ($notifications_trigger as $notch) {

			$users_to_ids = $this->getUsersByNotch($notch, $notif);
			if (empty($users_to_ids)) continue;

			$notifications_course[$ruleId][$notch['id']] = [
			    'tenant_id' => $notch['tenant_id'],
				'trigger_id' => $notch['id'],
				'user_ids' => $users_to_ids,
				'user_type' => ($notif['module'] == 'agents')? 0 : 1,
				'rule_id' => $ruleId,
				'noting_id' => $notif['id'],
			];

			unset($users_to_ids);
		}

		return $notifications_course;
    }
}