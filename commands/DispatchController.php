<?php
/**
 * DispatchController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\commands;


use app\commands\base\BaseController;
use app\components\email_provider\base\EmailProviderInterface;
use app\helpers\LogsCronHelper;
use app\models\EmailsDispatch;
use app\models\LogsCron;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\utilities\contacts\ContactsUtility;

class DispatchController extends BaseController
{
    public function actionDispatchEmails() {
        $t_start = time();
        /**
         * @var $data EmailsDispatch[]
         */
        $data = EmailsDispatch::find()->where(['status'=>EmailsDispatch::STATUS_PENDING])->with('provider','tenant')->all();
        if(empty($data))
            return;

        $this->log_id = LogsCronHelper::setLog([
            'time_start' => $t_start,
            'command' => 'dispatch/dispatch-emails',
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_EXECUTING,
            'memory_usage' => memory_get_usage().''
        ]);

        foreach ($data as $item) {
            $item->status = EmailsDispatch::STATUS_IN_PROGRESS;
            $item->save();
        }
        foreach ($data as $item) {
            $segments_ids = $item->receivers;
            if(empty($segments_ids)) {
                $item->status = EmailsDispatch::STATUS_COMPLETE;
                $item->save();
                continue;
            }
            $segments_ids = \json_decode($segments_ids,true);
            $segments = SegmentsRelations::find()->where(['id_tenant'=>$item->tenant->id])->andWhere(['in','id',$segments_ids])->asArray()->all();
            $ids = \array_column($segments,'id');
            $relations = SegmentsResultList::find()->where(['in','id_relation',$ids])->asArray()->all();
            $ids = \array_column($relations,'id_contact');
            $contacts = ContactsUtility::getContactsInfo($ids,$item->tenant->language->iso);

            $params = $item->provider->params;
            if(!empty($params))
                $config = \json_decode($params,true);
            else
               continue;

            $data =  [];
            if(!empty($item->provider->provider_data))
                $data = \json_decode($item->provider->provider_data,true);

            if(empty($data))
                continue;

            $class = $item->provider->provider->provider_class;

            /**
             * @var $dispatch EmailProviderInterface
             */
            $dispatch = new $class($config,$item->tenant,$data);

            $info = [];
            foreach ($segments as $segment){
                $rls = \array_filter($relations,function ($item) use ($segment){
                    return $item['id_relation'] == $segment['id'];
                });
                $cids = \array_column($rls,'id_contact');
                $cnts = \array_filter($contacts,function ($item) use ($cids){
                    return (\array_search($item['id'],$cids)!== false);
                });
                $emails = \array_column($cnts,'emails');
                if(!empty($emails)) {
                    $emails = \array_merge(...$emails);
                    $emails = \array_column($emails, 'value');
                }
                $phones = \array_column($cnts,'phones');
                if(!empty($phones)) {
                    $phones = \array_merge(...$phones);
                    $phones = \array_column($phones,'value');
                }
                $info[$segment['id']]['id'] = $segment['id'];
                $info[$segment['id']]['emails'] = $emails;
                $info[$segment['id']]['phones'] = $phones;
            }

//            $data = $prov->synchronizeSegments($info);
            $email = '';
            switch ($item['email_type']) {
                case 0: $email = $item['email'];break;
            }
            $data = $dispatch->sendSegments($info,$item['name'],$item['subject'],$email);
            $errors = $dispatch->getErrors();
            if($data === false)
            {
                $item->errors = \json_encode($errors);
                $item->status = EmailsDispatch::STATUS_ERROR;
                $item->save();
                continue;
            }
            $item->provider_data = \json_encode($data);
            $item->status = EmailsDispatch::STATUS_COMPLETE;
            $item->save();
        }

        $this->log_id = LogsCronHelper::setLog([
            'id' => $this->log_id,
            'time_end' => \time(),
            'command' => 'dispatch/dispatch-emails',
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_SUCCESS,
            'memory_usage' => \memory_get_usage().''
        ]);
    }

    public function actionGetStatistic() {
        $t_start = \time();
        /**
         * @var $data EmailsDispatch[]
         */
        $data = EmailsDispatch::find()->where(['status'=>EmailsDispatch::STATUS_COMPLETE])
            ->andWhere(['or',['>','stat_update',\time()-24*60*60],['stat_update'=>0]])
            ->with('provider','tenant')->all();
        if(empty($data))
            return;

        $this->log_id = LogsCronHelper::setLog([
            'time_start' => $t_start,
            'command' => 'dispatch/get-statistic',
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_EXECUTING,
            'memory_usage' => memory_get_usage().''
        ]);

        foreach ($data as $item) {
            $params = $item->provider->params;
            if (!empty($params))
                $config = \json_decode($params, true);
            else
                continue;

            $data_s = [];
            if (!empty($item->provider->provider_data))
                $data_s = \json_decode($item->provider->provider_data, true);

            if (empty($data_s))
                continue;

            $class = $item->provider->provider->provider_class;

            /**
             * @var $dispatch EmailProviderInterface
             */
            $dispatch = new $class($config, $item->tenant, $data_s);

            $info = $item->provider_data;
            if(empty($info)) {
                $item->stat_update = \time();
                $item->save();
                continue;
            } else {
                $info = \json_decode($info,true);
                if($item->stat_update == 0) {
                    $item->stat_update = \time();
                    $item->save();
                }
            }

            $data_s = $item->statistic_data;
            if(empty($data_s)) {
                $data_s = [
                    'total' => 0,
                    'opened_total' => 0,
                    'clicked_total' => 0,
                    'unique_opens' => 0,
                    'unique_clicks' => 0,
                    'last_open' => 0,
                    'last_click' => 0,
                    'not_delivered' => 0,
                    'unsubscribed' => 0,
                    'open_rate' => 0,
                    'click_rate' => 0,
                    'history' => [],
                    'links' => [],
                    'emails' => [],
                    'locations' => []
                ];
            } else {
                $data_s =\json_decode($data_s,true);
            }

            $stat = $dispatch->getStatistic($info,$data_s);
            if($stat == false)
                continue;
            $item->clicks = $stat['unique_clicks']??0;
            $item->opens = $stat['unique_opens']??0;
            $item->statistic_data = \json_encode($stat);
            $item->save();
        }

        $this->log_id = LogsCronHelper::setLog([
            'id' => $this->log_id,
            'time_end' => \time(),
            'command' => 'dispatch/get-statistic',
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_SUCCESS,
            'memory_usage' => \memory_get_usage().''
        ]);
    }
}