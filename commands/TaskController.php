<?php
/**
 * TaskController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;

use app\commands\base\BaseController;
use app\components\events\SystemTaskEmailReminderEvent;
use app\helpers\LogsCronHelper;
use app\helpers\PrintHelper;
use app\models\LogsCron;
use app\models\Tasks;
use app\components\notifications_now\NotificationActions;

class TaskController extends BaseController
{
    /**
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCheckExpiring()
    {
        
        $tasksNotifications = Tasks::find()
            ->where(['status' => false])
            ->andWhere('assigned_to_id IS NOT NULL')
            ->andWhere(['<=', 'due_date', time()])
            ->andWhere(['due_date_send' => false])
            //->asArray()
            ->all();

        if (!empty($tasksNotifications)) {


            $this->log_id = LogsCronHelper::setLog([
                'time_start' => time(),
                'command' => \Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_EXECUTING,
                'memory_usage' => memory_get_usage().''
            ]);

            foreach ($tasksNotifications as $task) {
                if (empty($task->assigned_to_id)) {
                    continue;
                }

                $this->log_id = LogsCronHelper::setLog([
                    'id' => $this->log_id,
                    'output_file' => $this->log_file_path,
                    'status' => LogsCron::STATUS_EXECUTING,
                    'memory_usage' => memory_get_usage().''
                ]);


                $action = new NotificationActions(NotificationActions::TASK_OVERDUE, $task->id, 0);
                $action->send();

                $task->due_date_send = true;
                $task->save();
            }

            $this->log_id = LogsCronHelper::setLog([
                'id' => $this->log_id,
                'time_end' => time(),
                'command' => \Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_SUCCESS,
                'memory_usage' => memory_get_usage().''
            ]);
        }

        $tasks = Tasks::find()
            ->where(['status' => false])
            ->andWhere('assigned_to_id IS NOT NULL')
            ->andWhere(['<=', 'email_reminder', time()])
            ->andWhere(['email_reminder_send' => false])
            ->all();

        if (!empty($tasks)) {
            $this->log_id = LogsCronHelper::setLog([
                'time_start' => time(),
                'command' => \Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_EXECUTING,
                'memory_usage' => memory_get_usage().''
            ]);

            foreach ($tasks as $task) {
                if (empty($task->assigned_to_id)) {
                    continue;
                }

                $this->log_id = LogsCronHelper::setLog([
                    'id' => $this->log_id,
                    'output_file' => $this->log_file_path,
                    'status' => LogsCron::STATUS_EXECUTING,
                    'memory_usage' => memory_get_usage().''
                ]);

                PrintHelper::printMessage('TaskExpiring: '.$task->title . ' (ID #'.$task->id.')',PrintHelper::INFO_TYPE);

                $event = new SystemTaskEmailReminderEvent($task, $task->assignedTo);
                \Yii::$container->get('systemEmitter')->trigger($event);
            }

            $this->log_id = LogsCronHelper::setLog([
                'id' => $this->log_id,
                'time_end' => time(),
                'command' => \Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_SUCCESS,
                'memory_usage' => memory_get_usage().''
            ]);
        }

    }
}