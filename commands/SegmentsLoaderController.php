<?php
/**
 * TranslationsController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;


use app\components\email_provider\base\EmailProviderInterface;
use app\helpers\LogsCronHelper;
use app\models\LogsCron;
use app\models\Providers;
use app\models\ProvidersCredentials;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\models\Tenants;
use app\utilities\contacts\ContactsUtility;
use Yii;
use app\commands\base\BaseController;
use yii\console\ExitCode;
use app\helpers\PrintHelper;

class SegmentsLoaderController extends BaseController
{
    /**
     * @return int
     * @throws \Exception
     */
    public function actionLoader()
    {
        PrintHelper::printMessage('Work SegmentsLoader start',PrintHelper::INFO_TYPE);

        $this->log_id = LogsCronHelper::setLog([
            'time_start' => time(),
            'command' => 'segments-loader/loader',
            'output_file' => $this->log_file_path,
            'status' => LogsCron::STATUS_EXECUTING,
            'memory_usage' => memory_get_usage().''
        ]);

        $status = LogsCron::STATUS_SUCCESS;

        SegmentsResultList::loadFromRelation();

        $email_providers = Yii::$app->params['dispatch_providers']['email'];
        $email_providers = array_keys($email_providers);
        $email_providers = Providers::find()->where(['in','provider_class',$email_providers])->asArray()->all();
        $ids = \array_column($email_providers,'id');
        $email_providers = \array_combine($ids,$email_providers);

        $creds = ProvidersCredentials::find()->where(['in','provider_id',$ids])->asArray()->all();
        $ids = \array_column($creds,'tenant_id');

        $tenants = Tenants::find()->where(['in','id',$ids])->all();

        $segments = SegmentsRelations::find()->where(['in','id_tenant',$ids])->asArray()->all();

        $ids = \array_column($segments,'id');
        $relations = SegmentsResultList::find()->where(['in','id_relation',$ids])->asArray()->all();
        $ids = \array_column($relations,'id_contact');
        $contacts = ContactsUtility::getContactsInfo($ids,'en');

        $info = [];
        foreach ($segments as $segment){
            $rls = \array_filter($relations,function ($item) use ($segment){
                return $item['id_relation'] == $segment['id'];
            });
            $cids = \array_column($rls,'id_contact');
            $cnts = \array_filter($contacts,function ($item) use ($cids){
                return (\array_search($item['id'],$cids)!== false);
            });
            $emails = \array_column($cnts,'emails');
            if(!empty($emails)) {
                $emails = \array_merge(...$emails);
                $emails = \array_column($emails, 'value');
            }
            $phones = \array_column($cnts,'phones');
            if(!empty($phones)) {
                $phones = \array_merge(...$phones);
                $phones = \array_column($phones,'value');
            }
            $info[$segment['id_tenant']][] = [
                'id' => $segment['id'],
                'emails' => $emails,
                'phones' => $phones
            ];
        }

        foreach ($tenants as $tenant) {
            $t_segments = $info[$tenant->id]??[];
            $providers = \array_filter($creds,function ($item)use($tenant){
                return $item['tenant_id'] == $tenant->id;
            });
            $temp = ProvidersCredentials::find()->where(['in','id',\array_column($providers,'id')])->all();
            $src = [];
            foreach ($temp as $item)
                $src[$item->id] = $item;

            /**
             * @var $src ProvidersCredentials[]
             */
            foreach ($providers as $provider) {
                $pr = $email_providers[$provider['provider_id']];
                /**
                 * @var $ex EmailProviderInterface
                 */
                $ex = new $pr['provider_class'](
                    (!empty($provider['params'])?\json_decode($provider['params'],true):[]),
                    $tenant,
                    (!empty($provider['provider_data'])?\json_decode($provider['provider_data'],true):[])
                );

                $rez = $ex->synchronizeSegments($t_segments);

                if($rez !== false){
                    PrintHelper::printMessage('Synchronize segments: '.\implode(',',\array_column($t_segments,'id')));
                    $src[$provider['id']]->provider_data = \json_encode($rez);
                    $src[$provider['id']]->provider_errors = null;
                } else {
                    $errors = $ex->getErrors();
                    $src[$provider['id']]->provider_errors =\json_encode($errors);
                    foreach ($errors as $error)
                        if(is_array($error))
                            PrintHelper::printMessage(\json_encode($error),PrintHelper::WARNING_TYPE);
                        else
                            PrintHelper::printMessage($error,PrintHelper::WARNING_TYPE);
                    $status = LogsCron::STATUS_WARNING;
                }

                $src[$provider['id']]->save();
            }
        }


        $this->log_id = LogsCronHelper::setLog([
            'id' => $this->log_id,
            'time_end' => time(),
            'command' => 'segments-loader/loader',
            'output_file' => $this->log_file_path,
            'status' => $status,
            'memory_usage' => memory_get_usage().''
        ]);

        PrintHelper::printMessage('Work SegmentsLoader end.',PrintHelper::INFO_TYPE);
        return ExitCode::OK;
    }
}