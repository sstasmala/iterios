<?php
/**
 * JobsExecuter.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\commands;


use app\commands\base\BaseController;
use app\components\executors\BaseExecutorInterface;
use app\helpers\LogsCronHelper;
use app\helpers\PrintHelper;
use app\models\Jobs;
use app\models\LogsCron;

class JobsExecutorController extends BaseController
{
    public function actionRun()
    {
        $jobs = Jobs::find()->where(['lock'=>0])->all();
        Jobs::updateAll(['lock'=>1],['in','id',array_column($jobs,'id')]);
        if(!empty($jobs))
            $this->log_id = LogsCronHelper::setLog([
                'time_start'=>time(),
                'command'=>\Yii::$app->controller->route,
                'output_file'=>$this->log_file_path,
                'status'=>LogsCron::STATUS_EXECUTING,
                'memory_usage'=>memory_get_usage().''
            ]);
        foreach ($jobs as $job)
        {
            /** @var BaseExecutorInterface $executor */
            $temp = $job->toArray();
            unset($temp['data']);
            PrintHelper::printMessage('Starting job:'.json_encode($temp),PrintHelper::INFO_TYPE);
            $executor = new $job->executor();
            try
            {
                $executor->execute($job->data);
            }
            catch (\Exception $e)
            {
                PrintHelper::printMessage('Error: '.$e->getMessage(),PrintHelper::ERROR_TYPE);
                PrintHelper::printMessage('Code: '.$e->getCode(),PrintHelper::ERROR_TYPE);
                PrintHelper::printMessage('File: '.$e->getFile(),PrintHelper::ERROR_TYPE);
                PrintHelper::printMessage('Line: '.$e->getLine(),PrintHelper::ERROR_TYPE);
                PrintHelper::printMessage('Stack trace: '.PHP_EOL.$e->getTraceAsString(),PrintHelper::ERROR_TYPE);
            }
            $job->delete();
            $this->log_id = LogsCronHelper::setLog([
                'id'=>$this->log_id,
                'command'=>\Yii::$app->controller->route,
                'output_file'=>$this->log_file_path,
                'status'=>LogsCron::STATUS_EXECUTING,
                'memory_usage'=>memory_get_usage().''
            ]);
        }
        if(!empty($jobs))
            $this->log_id = LogsCronHelper::setLog([
                'id'=>$this->log_id,
                'time_end'=>time(),
                'command'=>\Yii::$app->controller->route,
                'output_file'=>$this->log_file_path,
                'status'=>LogsCron::STATUS_SUCCESS,
                'memory_usage'=>memory_get_usage().''
            ]);
    }
}