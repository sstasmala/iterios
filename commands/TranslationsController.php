<?php
/**
 * TranslationsController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;

use app\models\UiTranslations;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class TranslationsController extends Controller
{
    /**
     * @return int
     * @throws \Exception
     */
    public function actionUpdate()
    {
        $path = Yii::$app->basePath . '/data/translations.map.php';
        $data = require $path;
        $pages = $data['pages'];
        $general = $data['general'];

        foreach ($pages as $group => $page) {
            foreach ($page as $code => $item) {
                $model = UiTranslations::findOne(['code' => $code]);
                if ($model) {
                    foreach ($item as $lang => $value) {
                        $translate = $model->translate($lang);
                        if ($translate->value == null) {
                            $model->value = $value;
                            $model->saveWithLang($lang);
                            echo "update translate: value: {$model->value} group: {$model->group} code: $model->code ". "\n";
                        }
                    }
                    $model->group = 'pages:'.$group;
                    $model->save();
                } else {
                    $newModel = new UiTranslations;
                    foreach ($item as $lang => $value) {
                        $newModel->value = $value;
                        $newModel->group = 'pages:'.$group;
                        $newModel->code = $code;
                        $newModel->saveWithLang($lang);
                        echo "added new translate: value: {$newModel->value} group: {$newModel->group} code: $newModel->code ". "\n";
                    }
                }
            }
        }

        foreach ($general as $code => $item) {
            $model = UiTranslations::findOne(['code' => $code]);
            if ($model) {
                foreach ($item as $lang => $value) {
                    $translate = $model->translate($lang);
                    if ($translate->value == null) {
                        $model->value = $value;
                        $model->saveWithLang($lang);
                        echo "update translate: value: {$model->value} group: {$model->group} code: $model->code ". "\n";
                    }
                }
                $model->group = 'general';
                $model->save();
            } else {
                $newModel = new UiTranslations;
                foreach ($item as $lang => $value) {
                    $newModel->value = $value;
                    $newModel->group = 'general';
                    $newModel->code = $code;
                    $newModel->saveWithLang($lang);
                    echo "added new translate: value: {$newModel->value} group: {$newModel->group} code: $newModel->code ". "\n";
                }
            }
        }

        echo 'Success!' . "\n";

        return ExitCode::OK;
    }
}