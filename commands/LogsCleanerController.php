<?php
/**
 * LogsCleanerController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;

use app\commands\base\BaseController;
use app\helpers\LogsCronHelper;
use app\helpers\PrintHelper;
use app\models\LogsCron;
use DateInterval;
use DateTime;

class LogsCleanerController extends BaseController
{
    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionClean()
    {
        $date = new DateTime();
        $date->sub(new DateInterval('P1M'));

        $logs = LogsCron::find()
            ->where(['<', 'created_at', $date->getTimestamp()])
            ->all();

        if (!empty($logs)) {
            $this->log_id = LogsCronHelper::setLog([
                'time_start' => time(),
                'command' => \Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_EXECUTING,
                'memory_usage' => memory_get_usage().''
            ]);

            $del_count = 0;

            /**
             * @var $log LogsCron
             */
            foreach ($logs as $log) {
                if (file_exists($log->output_file)) {
                    unlink($log->output_file);
                }

                $log->delete();

                $del_count++;
            }

            PrintHelper::printMessage($del_count . ' CRON Logs cleared!', PrintHelper::SUCCESS_TYPE);

            $this->log_id = LogsCronHelper::setLog([
                'id' => $this->log_id,
                'time_end' => time(),
                'command' =>\Yii::$app->controller->route,
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_SUCCESS,
                'memory_usage' => memory_get_usage().''
            ]);
        }
    }

    public function actionCleanEmpty()
    {
        LogsCron::deleteAll(['status'=>'empty']);
    }
}