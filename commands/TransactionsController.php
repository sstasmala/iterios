<?php
/**
 * Transactions.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\commands;


use app\commands\base\BaseController;
use app\helpers\LogsCronHelper;
use app\helpers\PrintHelper;
use app\models\FinancialOperations;
use app\models\LogsCron;
use app\models\Tenants;
use app\models\TariffsOrders;
use app\models\Transactions;

class TransactionsController extends BaseController
{
    public function actionProcess()
    {
        $transactions = Transactions::find()->where('financial_operation_id IS NOT NULL')->asArray()->all();
        $ids = array_column($transactions,'financial_operation_id');

        $fo = FinancialOperations::find()->where(['not in','id',$ids])->andWhere(['<>','operation_type',0])->all();
        $ords = TariffsOrders::find()->where(['status'=>TariffsOrders::STATUS_NOT_ACTIVE])->all();

        if(!empty($fo) || !empty($ords))
            $this->log_id = LogsCronHelper::setLog([
                'time_start'=>time(),
                'command'=>\Yii::$app->controller->route,
                'output_file'=>$this->log_file_path,
                'status'=>LogsCron::STATUS_EXECUTING,
                'memory_usage'=>memory_get_usage().''
            ]);

        foreach ($fo as $o)
        {
            if($o->operation_type == FinancialOperations::OPERATION_BALANCE)
            {
                $trans = new Transactions();
                $trans->financial_operation_id = $o->id;
                $trans->action = Transactions::ACTION_CHARGE;
                $trans->sum = $o->sum_usd;
                $trans->date = time();
                $trans->owner = 0;
                $trans->tenant_id = $o->customer_id;
                if($trans->save()) {
                    PrintHelper::printMessage("Create transaction:".json_encode($trans->toArray()));
                    $customer = Tenants::find()->where(['id'=>$o->customer_id])->one();
                    if($customer!=null) {
                        $customer->balance = (float)$customer->balance + $trans->sum;
                        $customer->save();
                    }
                }
            }
            if($o->operation_type == FinancialOperations::OPERATION_WITHDRAWAL)
            {
                $customer = Tenants::find()->where(['id'=>$o->customer_id])->one();
                if($customer->balance>$o->sum_usd) {
                    $trans = new Transactions();
                    $trans->financial_operation_id = $o->id;
                    $trans->action = Transactions::ACTION_WRITE_OFF;
                    $trans->sum = $o->sum_usd;
                    $trans->date = time();
                    $trans->owner = 0;
                    $trans->tenant_id = $o->customer_id;
                    if($trans->save()) {
                        PrintHelper::printMessage("Create transaction:".json_encode($trans->toArray()));
                        if($customer!=null) {
                            $customer->balance = (float)$customer->balance -$trans->sum;
                            $customer->save();
                        }
                    }
                }

            }
        }
        foreach ($ords as $ord)
        {
            $customer = Tenants::find()->where(['id'=>$ord->customer_id])->one();

            if($customer !== null) {
                $tor = TariffsOrders::find()->where(['customer_id'=>$customer->id])->andWhere(['status'=>TariffsOrders::STATUS_ACTIVE])->all();
                if(empty($tor))
                {
                    $price = $ord->tariff->price;

                    if($customer->balance>$price) {
                        $trans = new Transactions();
                        $trans->tariff_order_id = $ord->id;
                        $trans->tariff_id = $ord->tariff_id;
                        $trans->action = Transactions::ACTION_WRITE_OFF;
                        $trans->sum = $price;
                        $trans->date = time();
                        $trans->owner = 0;
                        $trans->tenant_id = $customer->id;
                        if($trans->save()) {
                            PrintHelper::printMessage("Create transaction:".json_encode($trans->toArray()));
                            $customer->balance = (float)$customer->balance - $price;
                            $customer->tariff_id = $ord->tariff_id;
                            if($customer->save()) {
                                $ord->status = TariffsOrders::STATUS_ACTIVE;
                                $duration = new \DateTime();
                                $interval = $ord->tariff->tariff_duration;
                                switch ($interval)
                                {
                                    case 0:$duration->add(new \DateInterval('P1M'));break;
                                    case 1:$duration->add(new \DateInterval('P6M'));break;
                                    case 2:$duration->add(new \DateInterval('P1Y'));break;
                                }
                                $ord->duration_date = $duration->getTimestamp();
                                $ord->save();
                            }
                        }
                    }
                }

            }
        }
        if(!empty($fo) || !empty($ords))
            $this->log_id = LogsCronHelper::setLog([
                'id'=>$this->log_id,
                'time_end'=>time(),
                'command'=>\Yii::$app->controller->route,
                'output_file'=>$this->log_file_path,
                'status'=>LogsCron::STATUS_SUCCESS,
                'memory_usage'=>memory_get_usage().''
            ]);
    }



    public function actionCheckTariffs()
    {
        $start = time();
        /**
         * @var $ords TariffsOrders[]
         */
        $ords = TariffsOrders::find()->where(['status'=>TariffsOrders::STATUS_ACTIVE])->andWhere(['<','duration_date',time()])->all();
        if(empty($ords))
            return;

        $this->log_id = LogsCronHelper::setLog([
            'time_start'=>$start,
            'command'=>\Yii::$app->controller->route,
            'output_file'=>$this->log_file_path,
            'status'=>LogsCron::STATUS_EXECUTING,
            'memory_usage'=>memory_get_usage().''
        ]);

        foreach ($ords as $ord) {
            $ord->status = TariffsOrders::STATUS_SUSPENDED;
            /**
             * @var $customer Tenants
             */
            $customer = Tenants::find()->where(['id'=>$ord->customer_id])->one();
            if(!is_null($customer)) {
                $customer->tariff_id = null;
                $customer->save();
            }
            $ord->save();
        }

        $this->log_id = LogsCronHelper::setLog([
            'id'=>$this->log_id,
            'time_end'=>time(),
            'command'=>\Yii::$app->controller->route,
            'output_file'=>$this->log_file_path,
            'status'=>LogsCron::STATUS_SUCCESS,
            'memory_usage'=>memory_get_usage().''
        ]);
    }
}