<?php
/**
 * CronController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\commands;


use app\commands\base\BaseController;
use app\helpers\PrintHelper;
use GO\Scheduler;
use Symfony\Component\Process\Process;
use yii\console\Controller;

class CronController extends Controller
{
    private function getCmdLine($command,$with_log = true)
    {
        $date = new \DateTime();
        $line = '';
        $line .= \Yii::$app->params['path_to_php'].' ';
        $line .= \Yii::$app->params['path_to_app'].'/yii ';
        $line .= $command;
        if($with_log)
        {
            $path = $this->getLogFile($command);
            $line .= ' -l='.$path.' >> '.$path;
        }
        return $line;
    }

    /**
     * @param $command
     * @return string
     */
    private function getLogFile($command)
    {
        $date = new \DateTime();
        return \Yii::$app->basePath.'/logs/'.$date->format('Y-m-d').':'.time().':'.str_replace('/',':',$command).'.log';
    }

    public function actionRun()
    {
        $date = new \DateTime();

        $main_log_path  = \Yii::$app->basePath.'/main.log';
        $f = fopen($main_log_path,'w');
        fputs($f,time());
        fclose($f);

        $process = new Process($this->getCmdLine('cron/schedule',false));
        $process->run();
    }

    public function actionSchedule()
    {
        PrintHelper::printMessage('Start cron.', PrintHelper::INFO_TYPE);
        $scheduler = new Scheduler();

        //commands block start
        $scheduler->raw($this->getCmdLine('jobs-executor/run'))
            ->at('* * * * *');

        $scheduler->raw($this->getCmdLine('task/check-expiring'))
            ->at('*/10 * * * *');

       $scheduler->raw($this->getCmdLine('logs-cleaner/clean'))
           ->at('0 0 * * *');

        $scheduler->raw($this->getCmdLine('notifications/day'))
            ->at('0 * * * *');

        $scheduler->raw($this->getCmdLine('notifications/send'))
            ->at('* * * * *');

        $scheduler->raw($this->getCmdLine('segments-loader/loader'))
            ->at('0 * * * *');

        $scheduler->raw($this->getCmdLine('transactions/process'))
            ->at('*/10 * * * *');

        $scheduler->raw($this->getCmdLine('dispatch-sender-sms/send'))
            ->at('* * * * *');

        $scheduler->raw($this->getCmdLine('transactions/check-tariffs'))
            ->at('*/10 * * * *');

        $scheduler->raw($this->getCmdLine('dispatch/dispatch-emails'))
            ->at('*/10 * * * *');

        $scheduler->raw($this->getCmdLine('dispatch/get-statistic'))
            ->at('0 * * * *');

//        $command = $this->getCmdLine('files/check-files');
//        $scheduler->raw($command)->at('*/10 * * * *');

//        $scheduler  ->raw($this->getCmdLine('exchange-rates/update-rates'))
//            ->at('* * * * *');
//        $scheduler  ->raw($this->getCmdLine('users/synchronize'))
//            ->at('59 * * * *');
        //commands block end

        $scheduler->run();
        PrintHelper::printMessage('Finish cron.', PrintHelper::INFO_TYPE);
    }
}