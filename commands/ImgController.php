<?php
/**
 * TranslationsController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;

use app\models\Tenants;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\User;
use app\helpers\PrintHelper;
use app\models\Suppliers;
use app\models\RequisitesTenants;

class ImgController extends Controller
{
    /**
     * @return int
     * @throws \Exception
     */
    public function actionClear()
    {
        $users = User::find()
            ->where(['not', ['photo' => null]])
            ->all();

        foreach ($users as $user)
        {
            PrintHelper::printMessage('Work users start');
            $user_arr=explode("/", $user->photo);
            $imgPhoto = end($user_arr);
            $allImgUser = scandir(Yii::$app->basePath . '/web/storage/users/' . $user->id . '/');
            foreach ($allImgUser as $img)
            {
                if($img!=$imgPhoto&&$img!='.'&&$img!='..'){
                    unlink(Yii::$app->basePath . '/web/storage/users/'.$user->id.'/'.$img);
                    PrintHelper::printMessage('File '.Yii::$app->basePath . '/web/storage/users/'.$user->id.'/'.$img.' removed');
                }

            }
            PrintHelper::printMessage('Work users end');
        }

        $suppliers = Suppliers::find()
            ->where(['not', ['logo' => null]])
            ->all();
        foreach ($suppliers as $supplier)
        {
            PrintHelper::printMessage('Work Supplier start');
            $supplier_arr=explode("/", $supplier->logo);
            $imgPhoto = end($supplier_arr);
            $allImgSupplier = scandir(Yii::$app->basePath . '/web/storage/');
            foreach ($allImgSupplier as $img)
            {
                if($img!=$imgPhoto&&$img!='.'&&$img!='..'&&is_file(Yii::$app->basePath . '/web/storage/'.$img)&&$img!='.gitignore'){
                    unlink(Yii::$app->basePath . '/web/storage/'.$img);
                    PrintHelper::printMessage('File '.Yii::$app->basePath . '/web/storage/'.$img.' removed');
                }

            }
            PrintHelper::printMessage('Work Supplier end');
        }

        $requisitesTenants = RequisitesTenants::find()
            ->where(['not', ['value' => null]])
            ->andWhere(['set_id' => 4])
            ->all();

        foreach ($requisitesTenants as $requisit){
            $arrImgWithTenant[] = $requisit->value;
        }

        foreach ($requisitesTenants as $requisit)
        {

            PrintHelper::printMessage('Work requisitesTenants start');
            $requisit_arr=explode("/", $requisit->value);
//            $imgPhoto = end($requisit_arr);
            $allImgSupplier = scandir(Yii::$app->basePath . '/web/storage/company/'. $requisit->tenant_id . '/');
            foreach ($allImgSupplier as $img)
            {
                if(!in_array('storage/company/' . $requisit->tenant_id . '/'.$img,$arrImgWithTenant)&&$img!='.'&&$img!='..'&&is_file(Yii::$app->basePath . '/web/storage/company/'. $requisit->tenant_id .'/' .$img)&&$img!='.gitignore'){
                    unlink(Yii::$app->basePath . '/web/storage/company/'.$requisit->tenant_id . '/'.$img);
                    PrintHelper::printMessage('File '.Yii::$app->basePath . '/web/storage/'.$img.' removed');
                }

            }
            PrintHelper::printMessage('Work requisitesTenants end');
        }

        echo 'Success!' . "\n";

        return ExitCode::OK;
    }
}