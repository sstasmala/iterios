<?php
/**
 * RbacController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\commands;


use app\helpers\PrintHelper;
use app\helpers\RbacHelper;
use app\models\User;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionAssignAdmin($email)
    {
        $user = User::findOne(['email'=>$email]);
        if(is_null($user))
        {
            PrintHelper::printMessage("User not found!", PrintHelper::ERROR_TYPE);
            return;
        }
        RbacHelper::assignRole('system_admin',$user->id);
        PrintHelper::printMessage("Success!", PrintHelper::SUCCESS_TYPE);
    }
}