<?php
/**
 * TranslationsController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\commands;


use app\helpers\LogsCronHelper;
use app\models\LogsCron;
use app\models\SendSms;
use Yii;
use app\commands\base\BaseController;
use yii\console\ExitCode;
use app\helpers\PrintHelper;

class DispatchSenderSmsController extends BaseController
{
    /**
     * @return int
     * @throws \Exception
     */
    public function actionSend()
    {
        $readySms = SendSms::find()
            ->where(['status' => SendSms::STATUS_READY])
            ->all();

        if (isset($readySms) && count($readySms)> 0) {
            PrintHelper::printMessage('Work SenderSms start');

            $this->log_id = LogsCronHelper::setLog([
                'time_start' => time(),
                'command' => 'send-sms/send',
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_EXECUTING,
                'memory_usage' => memory_get_usage() . ''
            ]);

            SendSms::send();

            $this->log_id = LogsCronHelper::setLog([
                'id' => $this->log_id,
                'time_end' => time(),
                'command' => 'send-sms/send',
                'output_file' => $this->log_file_path,
                'status' => LogsCron::STATUS_SUCCESS,
                'memory_usage' => memory_get_usage() . ''
            ]);

            PrintHelper::printMessage('Work SenderSms end');


            echo 'Success!' . "\n";
        }
        return ExitCode::OK;
    }
}