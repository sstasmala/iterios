<?php
/**
 * config/auth0.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

return [
    'class' => 'app\components\auth0\Auth0',
    'domain' => 'ACCOUNT.auth0.com',
    'auth_api_client' => [
        'client_id' => 'YOUR_CLIENT_ID',
        'client_secret' => 'YOUR_CLIENT_SECRET'
    ],
    'management_api_client' => [
        'client_id' => 'YOUR_CLIENT_ID',
        'client_secret' => 'YOUR_CLIENT_SECRET'
    ]
];