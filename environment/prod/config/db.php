<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => (YII_ENV_PROD)?true:false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
