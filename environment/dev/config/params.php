<?php
return [
    'adminEmail' => 'admin@example.com',
    'stage'=>'live',
    'baseUrl'=>'http://ita.com',
    'passwordResetTokenExpire' => 8 * 60 * 60,
    'path_to_php'=>'/usr/bin/php',
    'path_to_app'=>'/var/www/ita'
];
