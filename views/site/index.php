<?php
/**
 * views/site/login.php
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
//$lang = substr(Yii::$app->request->getPreferredLanguage(), 0, 2);
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$this->title = \app\helpers\TranslationHelper::getTranslation('login',$lang);

$this->registerCssFile('@web/css/site/index.min.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

$this->registerJsFile('@web/js/site/index.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$list = \app\helpers\MCHelper::searchCountry($lang, $lang);
?>

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img//bg/bg-3.jpg' ?>);">
        <div class="m-login__wrapper-1 m-portlet-full-height">
            <div class="m-login__wrapper-1-1">
                <div class="m-login__contanier">
                    <div class="m-login__content">
                        <div class="m-login__logo">
                            <a href="/">
                                <img src="<?= Yii::$app->params['baseUrl'] . '/img/logo.png' ?>">
                            </a>
                        </div>
                        <div class="m-login__title">
                            <h3>
                                <?= \app\helpers\TranslationHelper::getTranslation('try_iterios',$lang);?>
                            </h3>
                        </div>
                        <div class="m-login__desc">
                            <?= \app\helpers\TranslationHelper::getTranslation('try_iterios_desc',$lang);?>
                        </div>
                        <div class="m-login__form-action">
                            <button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">
                                <?= \app\helpers\TranslationHelper::getTranslation('sign_up',$lang);?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="m-login__border">
                    <div></div>
                </div>
            </div>
        </div>
        <div class="m-login__wrapper-2 m-portlet-full-height">
            <div class="m-login__contanier">
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            <?= \app\helpers\TranslationHelper::getTranslation('login',$lang);?>
                        </h3>
                    </div>
                    <form class="m-login__form m-form" action="" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('email_title',$lang);?>" name="email" autocomplete="off">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="Password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_pass',$lang);?>" name="password">
                        </div>
                        <div class="row m-login__form-sub">
                            <div class="col m--align-left">
                                <label class="m-checkbox m-checkbox--focus">
                                    <input type="checkbox" name="remember" value="1" checked="checked">
                                    <?= \app\helpers\TranslationHelper::getTranslation('remember_me',$lang);?>
                                    <span></span>
                                </label>
                            </div>
                            <div class="col m--align-right">
                                <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                    <?= \app\helpers\TranslationHelper::getTranslation('f_pass',$lang);?>
                                </a>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                <?= \app\helpers\TranslationHelper::getTranslation('sign_in',$lang);?>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="m-login__signup">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            <?= \app\helpers\TranslationHelper::getTranslation('sign_up',$lang);?>
                        </h3>
                        <div class="m-login__desc">
                            <?= \app\helpers\TranslationHelper::getTranslation('sign_up_desc',$lang);?>
                        </div>
                    </div>
                    <form class="m-login__form m-form" autocomplete="off" action="" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <div class="form-group m-form__group mb-3 mt-3">
                            <select class="form-control m-bootstrap-select" id="account_country_select" name="country">
                            </select>
                        </div>
                        <?= Html::hiddenInput('language') ?>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_company',$lang);?>" name="company">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_first_name',$lang);?>" name="first_name">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_last_name',$lang);?>" name="last_name">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('email_title',$lang);?>" name="email" autocomplete="off">
                        </div>
                        <div class="form-group m-form__group">
                            <input id="password" class="form-control m-input" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_pass',$lang);?>" name="password">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_confirm_pass',$lang);?>" name="confirm_password">
                        </div>
                        <div class="m-login__form-sub">
                            <label class="m-checkbox m-checkbox--focus">
                                <input type="checkbox" name="agree" value="1">

                                <?= \app\helpers\TranslationHelper::getTranslation('agree_text1',$lang);?>
                                <a href="https://iterios.com/public-offer" class="m-link m-link--focus" target="_blank">
                                    <?= \app\helpers\TranslationHelper::getTranslation('public_offer',$lang);?>,
                                </a>
                                <?= \app\helpers\TranslationHelper::getTranslation('agree_text2',$lang);?>
                                <a href="https://iterios.com/legal" class="m-link m-link--focus" target="_blank">
                                    <?= \app\helpers\TranslationHelper::getTranslation('legal_note',$lang);?>
                                </a>
                                .
                                <span></span>
                            </label>
                            <span class="m-form__help"></span>
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                <?= \app\helpers\TranslationHelper::getTranslation('sign_up',$lang);?>
                            </button>
                            <button id="m_login_signup_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                <?= \app\helpers\TranslationHelper::getTranslation('cancel',$lang);?>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="m-login__forget-password">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            <?= \app\helpers\TranslationHelper::getTranslation('ft_pass',$lang);?>
                        </h3>
                        <div class="m-login__desc">
                            <?= \app\helpers\TranslationHelper::getTranslation('ft_pass_text',$lang);?>
                        </div>
                    </div>
                    <form class="m-login__form m-form" action="" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('email_title',$lang);?>" name="email" id="m_email" autocomplete="off">
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                <?= \app\helpers\TranslationHelper::getTranslation('send',$lang);?>
                            </button>
                            <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom ">
                                <?= \app\helpers\TranslationHelper::getTranslation('cancel',$lang);?>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->