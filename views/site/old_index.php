<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(/metronic/assets/app/media/img//bg/bg-2.jpg);">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="<?=Yii::$app->params['baseUrl'].'/img/logo.png'?>">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            <a href="<?=Yii::$app->params['baseUrl'].'/ita'?>" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" style="color: white">
                                To ITA
                            </a>
                        </h3>
                        <h3 class="m-login__title">
                            <a href="<?=Yii::$app->params['baseUrl'].'/ia'?>" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" style="color: white">
                                To IA
                            </a>
                        </h3>
                        <h3 class="m-login__title">
                            <a href="<?=Yii::$app->params['baseUrl'].'/login'?>" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" style="color: white">
                                Login
                            </a>
                        </h3>
                        <h3 class="m-login__title">
                            <a href="<?=Yii::$app->params['baseUrl'].'/signup'?>" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" style="color: white">
                                Sign Up
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>