<?php
/**
 * views/site/change_password.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

use yii\helpers\Html;

$lang = substr(Yii::$app->request->getPreferredLanguage(), 0, 2);

$this->title = \app\helpers\TranslationHelper::getTranslation('create_company',$lang);

$this->registerCssFile('@web/css/site/index.min.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

$this->registerJsFile('@web/js/site/index.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
?>

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img//bg/bg-3.jpg' ?>);">
        <div class="m-login__wrapper-2 m-portlet-full-height">
            <div class="m-login__contanier">
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <div class="m-login__logo" style="margin: 0 auto 2rem auto;">
                            <a href="/">
                                <img src="<?= Yii::$app->params['baseUrl'] . '/img/logo.png' ?>">
                            </a>
                        </div>
                        <h3 class="m-login__title">
                            <?= \app\helpers\TranslationHelper::getTranslation('create_company',$lang);?>
                        </h3>
                        <div class="m-login__desc">
                            <?= \app\helpers\TranslationHelper::getTranslation('create_company_desc',$lang);?>
                        </div>
                    </div>

                    <form class="m-login__form m-form" action="" autocomplete="off" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <?= Html::hiddenInput('language') ?>
                        <?= Html::hiddenInput('country') ?>
                        <div class="form-group m-form__group">
                            <input id="password" class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_company_name',$lang);?>" name="company">
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_create_tenant_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                <?= \app\helpers\TranslationHelper::getTranslation('create',$lang);?>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
