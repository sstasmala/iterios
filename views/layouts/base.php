<?php
use yii\bootstrap\Html;
use app\assets\MetronicAsset;
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset; ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <link rel="shortcut icon" href="<?=Yii::$app->params['baseUrl'].'/img/logo.ico'?>" />
    <?php echo $this->render('_base_js_options'); ?>
    <?php $this->head(); ?>
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<?php $this->beginBody() ?>
<?= $content ?>
<?php MetronicAsset::register($this); ?>
<?php $this->registerJsFile('@web/metronic/assets/demo/default/base/scripts.bundle.js', ['depends' => [MetronicAsset::className()]]);?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>