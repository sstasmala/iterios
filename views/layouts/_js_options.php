<?php
/**
 * _js_options.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
/**
 * @var $this \yii\web\View
 */
$this->registerJs('var baseUrl = "'.Yii::$app->params['baseUrl'].'";',\yii\web\View::POS_HEAD);
$tenant_id = Yii::$app->user->identity->tenant->id;
$this->registerJs('var tenantId = "'.$tenant_id.'";',\yii\web\View::POS_HEAD);

$tenant_lang = Yii::$app->user->identity->tenant->language->iso;
$tenant_lang_id = Yii::$app->user->identity->tenant->language->id;
$this->registerJs('var tenant_lang = "'.$tenant_lang.'";',\yii\web\View::POS_HEAD);
$this->registerJs('var tenant_lang_id = "'.$tenant_lang_id.'";',\yii\web\View::POS_HEAD);
$page = Yii::$app->controller->getRoute();
$session = \Yii::$app->session;

$translates =  \app\helpers\TranslationHelper::getPageTranslation($page,$tenant_lang);
$this->registerJs('var translations = '.json_encode($translates).';',\yii\web\View::POS_HEAD);

$date_format = Yii::$app->user->identity->tenant->date_format;
$this->registerJs('var date_format = "'.\app\helpers\DateTimeHelper::convertFormat($date_format).'";',\yii\web\View::POS_HEAD);
$this->registerJs('var date_format_datepicker = "'.\app\helpers\DateTimeHelper::convertFormat2($date_format).'";',\yii\web\View::POS_HEAD);

$time_format = Yii::$app->user->identity->tenant->time_format;
$this->registerJs('var time_format = "'.\app\helpers\DateTimeHelper::convertFormat($time_format).'";',\yii\web\View::POS_HEAD);

$week_start = Yii::$app->user->identity->tenant->week_start;
$this->registerJs('var week_start = "'.$week_start.'";',\yii\web\View::POS_HEAD);

$tenant_country_id = Yii::$app->user->identity->tenant->country ? Yii::$app->user->identity->tenant->country : 0;
$this->registerJs('var tenant_country_id = '.$tenant_country_id.';',\yii\web\View::POS_HEAD);

$this->registerJs('var userId = "'.Yii::$app->user->id.'";',\yii\web\View::POS_HEAD);
$this->registerJs('var currencyDisplay = "'.Yii::$app->user->identity->tenant->currency_display.'";',\yii\web\View::POS_HEAD);

$this->registerJs('function GetTranslation(key){ if(translations[key]!=undefined) return translations[key]; return "["+key+"]"}'
    ,\yii\web\View::POS_HEAD);
$js = <<<JS
function DisplayLocaleString(value,currency){
        return value.toLocaleString(tenant_lang, { style: 'currency', currency: currency, currencyDisplay:currencyDisplay})
}
JS;

$this->registerJs($js,\yii\web\View::POS_HEAD)
?>