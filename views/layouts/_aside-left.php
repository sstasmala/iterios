<?php
use app\helpers\RbacHelper;
use app\helpers\TranslationHelper;
use yii\helpers\Url;
$lang = Yii::$app->user->identity->tenant->language->iso;
$menu = [
    'settings' => [
            'items'=>[
                    'Account' => [
                        'title' => TranslationHelper::getTranslation('account', $lang, 'Account'),
                        'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/settings/account'),
                        'icon' => 'flaticon-profile-1',
                        'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/settings/account'), Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) === false) ? 0 : 1,
                    ],
                    'Users' => [
                        'title' => TranslationHelper::getTranslation('users_page_title', $lang, 'Users'),
                        'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/users'),
                        'icon' => 'flaticon-users',
                        'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/users'), Yii::$app->controller->id) === false) ? 0 : 1,
                    ],
                    'Roles' => [
                        'title' => TranslationHelper::getTranslation('roles_page_title', $lang, 'Roles'),
                        'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/roles'),
                        'icon' => 'flaticon-user-settings',
                        'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/roles'), Yii::$app->controller->id) === false) ? 0 : 1,
                    ]
            ],

    ],
    'delivery' => [
        'items'=>[
            'Email' => [
                'title' => 'Email',
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/delivery-email'),
                'icon' => 'flaticon-mail',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/delivery-email'), Yii::$app->controller->id) === false) ? 0 : 1,
            ],
            'Sms' => [
                'title' => 'Sms',
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/delivery-sms'),
                'icon' => 'flaticon-chat',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/delivery-sms'), Yii::$app->controller->id) === false) ? 0 : 1,
            ]
        ],

    ],
    'Reminders' => [
        'items'=>[
            'For agent' => [
                'title' => TranslationHelper::getTranslation('reminders_for_agent', $lang, 'For agent') ,
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/reminders/agent'),
                'icon' => 'fa fa fa-id-badge',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/reminders/agent'), Yii::$app->controller->id.'/'.Yii::$app->controller->action->id) === false) ? 0 : 1,
            ],
            'For tourist' => [
                'title' =>  TranslationHelper::getTranslation('reminders_for_tourist', $lang, 'For tourist') ,
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/reminders/tourist'),
                'icon' => 'fa fa-suitcase',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/reminders/tourist'), Yii::$app->controller->id.'/'.Yii::$app->controller->action->id) === false) ? 0 : 1,
            ],
        ],
    ],
    'Templates' => [
        'items'=>[
            'email_and_sms' => [
                'title' => TranslationHelper::getTranslation('email_and_sms', $lang, 'Email and SMS'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/templates/email-and-sms'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Yii::$app->request->url, '/templates/email-and-sms') === false) ? 0 : 1,
            ],
            'documents' => [
                'title' =>  TranslationHelper::getTranslation('documents', $lang, 'Documents'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/templates/documents'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Yii::$app->request->url, '/templates/documents') === false) ? 0 : 1,
            ],
        ],
    ],
    'Integrations' => [
        'items'=>[
            'suppliers' => [
                'title' => TranslationHelper::getTranslation('suppliers', $lang, 'Suppliers'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/integrations-suppliers'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/integrations-suppliers'), Yii::$app->controller->id) === false) ? 0 : 1,
            ],
            'providers' => [
                'title' =>  TranslationHelper::getTranslation('providers', $lang, 'Providers'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/integrations-providers'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/integrations-providers'), Yii::$app->controller->id) === false) ? 0 : 1,
            ],
        ],
    ],
    'Billing' => [
        'items'=>[
            'my_orders' => [
                'title' => TranslationHelper::getTranslation('my_orders_menu_item', $lang, 'My orders'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/billing/my-orders'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Yii::$app->request->url, '/billing/my-orders') === false) ? 0 : 1,
            ],
            'rates' => [
                'title' =>  TranslationHelper::getTranslation('rates_menu_item', $lang, 'Rates'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/billing/rates'),
                'icon' => 'm-menu__link-bullet--dot',
                'isActive' => (strpos(Yii::$app->request->url, '/billing/rates') === false) ? 0 : 1,
            ],
        ],
    ],
    'API' => [
        'items'=>[
            'apis' => [
                'title' => 'API',
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/api/index'),
                'icon' => 'fab fa-cloudsmith',
                'isActive' => (strpos(Yii::$app->request->url, '/api/index') === false) ? 0 : 1,
            ],
        ],
    ],
    'Handbooks' => [
        'items'=>[
            'handbooks' => [
                'title' => TranslationHelper::getTranslation('handbooks', $lang, 'Handbooks'),
                'url' => Url::toRoute(Yii::$app->user->identity->tenant->id . '/handbooks/index'),
                'icon' => 'fas fa-book-open',
                'isActive' => (strpos(Yii::$app->request->url, '/handbooks/index') === false) ? 0 : 1,
            ],
        ],
    ]
];
$menu['delivery']['childIsActive'] = array_sum(array_column($menu['delivery']['items'], 'isActive'));
$menu['Reminders']['childIsActive'] = array_sum(array_column($menu['Reminders']['items'], 'isActive'));
$menu['Templates']['childIsActive'] = array_sum(array_column($menu['Templates']['items'], 'isActive'));
$menu['Integrations']['childIsActive'] = array_sum(array_column($menu['Integrations']['items'], 'isActive'));
$menu['Billing']['childIsActive'] = array_sum(array_column($menu['Billing']['items'], 'isActive'));
$menu['API']['childIsActive'] = array_sum(array_column($menu['API']['items'], 'isActive'));
$menu['Handbooks']['childIsActive'] = array_sum(array_column($menu['Handbooks']['items'], 'isActive'));
$menu['settings']['childIsActive'] = array_sum(array_column($menu['settings']['items'], 'isActive'))
        + $menu['Reminders']['childIsActive']
        + $menu['Templates']['childIsActive']
        + $menu['Integrations']['childIsActive']
        + $menu['Billing']['childIsActive']
        + $menu['API']['childIsActive']
        + $menu['Handbooks']['childIsActive'];


?>
<button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item m-aside-left m-aside-left--skin-dark">
    <div
        id="m_ver_menu"
        class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark"
        data-menu-vertical="true"
        data-menu-scrollable="false"
        data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow">

            <?php if(RbacHelper::can('tasks.read',['full','only_own','only_own_role']) || RbacHelper::owner()):?>
            <li class="m-menu__item <?php $curr = strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/tasks'), Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/tasks') ?>" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-list-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('tasks_page_title', $lang, 'Tasks') ?></span>
                            <span id="overdue_task_badge" class="m-menu__link-badge" style="display: none"><span class="m-badge m-badge--danger"></span></span>
                        </span>
                    </span>
                </a>
            </li>
            <?php endif;?>
            <li class="m-menu__item">
                <a href="#" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-search-magnifier-interface-symbol"></i>
                    <span class="m-menu__link-text">Tours search</span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    <?= strtoupper(TranslationHelper::getTranslation('create_roles_table_sales', $lang, 'SALES')); ?>
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item <?php $curr = strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/requests'), Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/requests/index') ?>" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-exclamation-1"></i>
                    <span class="m-menu__link-text">
                        <?= TranslationHelper::getTranslation('requests', $lang, 'Requests') ?>
                    </span>
                </a>
            </li>
            <li class="m-menu__item <?php $curr = strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/orders'), Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/orders/index') ?>" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-map"></i>
                    <span class="m-menu__link-text">
                        <?= TranslationHelper::getTranslation('orders', $lang, 'Orders') ?>
                    </span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    <?= strtoupper(TranslationHelper::getTranslation('create_roles_table_contacts', $lang, 'CONTRACTORS')); ?>
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <?php if(RbacHelper::can('contacts.read',['full','only_own','only_own_role']) || RbacHelper::owner()):?>
            <li class="m-menu__item <?php $curr = strpos('/ita/contacts', Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="/ita/contacts" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('contacts_contacts', $lang, 'Contacts') ?></span>
                </a>
            </li>
            <?php endif;?>
            <li class="m-menu__item <?php $curr = strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/companies'), Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/companies') ?>" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-layers"></i>
                    <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('companies_menu_item_title', $lang, 'Companies') ?></span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    MARKETING
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?= ($menu['delivery']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-alert"></i>
                    <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('delivery', $lang, 'Delivery') ?></span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <?php foreach ($menu['delivery']['items'] as $key => $value){ ?>
                            <li class="m-menu__item m-menu__item--submenu <?= ($value['isActive']) ? 'm-menu__item--active':''; ?>">
                                <a href="<?= $value['url']; ?>" class="m-menu__link">
                                    <i class="m-menu__link-icon <?= $value['icon']?>"></i>
                                    <span class="m-menu__link-text"><?= $value['title'];?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item <?php $curr = strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/segments'), Yii::$app->controller->id); echo ($curr===false) ? '':'m-menu__item--active'; ?>">
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/segments') ?>" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-pie-chart"></i>
                    <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('segments', $lang, 'Segments') ?></span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    OTHER
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item">
                <a href="#" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-signs-1"></i>
                    <span class="m-menu__link-text">Reports</span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?= ($menu['settings']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-settings-1"></i>
                    <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('settings', $lang, 'Settings') ?></span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <?php if(RbacHelper::can('settings.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item m-menu__item--submenu <?= ($menu['settings']['items']['Account']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a href="<?= $menu['settings']['items']['Account']['url']; ?>" class="m-menu__link">
                                <i class="m-menu__link-icon <?=$menu['settings']['items']['Account']['icon'];?>"></i>
                                <span class="m-menu__link-text"><?=$menu['settings']['items']['Account']['title'];?></span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(RbacHelper::can('users.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item m-menu__item--submenu <?= ($menu['settings']['items']['Users']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a href="<?= $menu['settings']['items']['Users']['url']; ?>" class="m-menu__link">
                                <i class="m-menu__link-icon <?=$menu['settings']['items']['Users']['icon'];?>"></i>
                                <span class="m-menu__link-text"><?= $menu['settings']['items']['Users']['title'];?></span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(RbacHelper::can('settings.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item m-menu__item--submenu <?= ($menu['settings']['items']['Roles']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a href="<?= $menu['settings']['items']['Roles']['url']; ?>" class="m-menu__link">
                                <i class="m-menu__link-icon <?=$menu['settings']['items']['Roles']['icon'];?>"></i>
                                <span class="m-menu__link-text">
                                    <?=$menu['settings']['items']['Roles']['title'];?>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                        <li class="m-menu__item  m-menu__item--submenu <?= ($menu['Reminders']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="#" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-information"></i>
                                <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('reminders_page_title', $lang, 'Reminders') ?></span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= ($menu['Reminders']['items']['For agent']['isActive']) ? 'm-menu__item--active':''; ?>">
                                        <a href="<?= $menu['Reminders']['items']['For agent']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-icon <?= $menu['Reminders']['items']['For agent']['icon']?>"></i>
                                            <span class="m-menu__link-text">
                                                <?= $menu['Reminders']['items']['For agent']['title'] ?>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= ($menu['Reminders']['items']['For tourist']['isActive']) ? 'm-menu__item--active':''; ?>">
                                        <a href="<?= $menu['Reminders']['items']['For tourist']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-icon <?= $menu['Reminders']['items']['For tourist']['icon'] ?>"></i>
                                            <span class="m-menu__link-text">
                                                <?= $menu['Reminders']['items']['For tourist']['title'] ?>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu <?= ($menu['Templates']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a  href="#" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-app"></i>
                                <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('templates', $lang, 'Templates') ?></span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= ($menu['Templates']['items']['email_and_sms']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/templates/email-and-sms') ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Templates']['items']['email_and_sms']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('email_and_sms', $lang, 'Email and SMS') ?></span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= ($menu['Templates']['items']['documents']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/templates/documents') ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Templates']['items']['documents']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('documents', $lang, 'Documents') ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php if(RbacHelper::can('settings.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item  m-menu__item--submenu <?= ($menu['Integrations']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a  href="#" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-open-box"></i>
                                <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('integration', $lang, 'Integration') ?></span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= ($menu['Integrations']['items']['suppliers']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= $menu['Integrations']['items']['suppliers']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Integrations']['items']['suppliers']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('suppliers', $lang, 'Suppliers') ?></span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= ($menu['Integrations']['items']['providers']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= $menu['Integrations']['items']['providers']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Integrations']['items']['providers']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('providers', $lang, 'Providers') ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php endif;?>
                        <li class="m-menu__item  m-menu__item--submenu <?= ($menu['Billing']['childIsActive']>0)?'m-menu__item--open m-menu__item--expanded':'';?>" aria-haspopup="true" data-menu-submenu-toggle="hover">
                            <a  href="#" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-coins"></i>
                                <span class="m-menu__link-text"><?= TranslationHelper::getTranslation('billig_menu_item', $lang, 'Invoices and payments') ?></span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= ($menu['Billing']['items']['my_orders']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= $menu['Billing']['items']['my_orders']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Billing']['items']['my_orders']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= $menu['Billing']['items']['my_orders']['title'] ?></span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= ($menu['Billing']['items']['rates']['isActive']) ? 'm-menu__item--active':''; ?>" aria-haspopup="true">
                                        <a href="<?= $menu['Billing']['items']['rates']['url'] ?>" class="m-menu__link">
                                            <i class="m-menu__link-bullet <?= $menu['Billing']['items']['rates']['icon'] ?>">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= $menu['Billing']['items']['rates']['title'] ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php if(RbacHelper::can('settings.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item <?= (strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/api'), Yii::$app->controller->id)===false) ? '':'m-menu__item--active'; ?>">
                            <a href="<?= $menu['API']['items']['apis']['url'] ?>" class="m-menu__link">
                                <i class="m-menu__link-icon <?= $menu['API']['items']['apis']['icon'] ?>"></i>
                                <span class="m-menu__link-text">
                                    <?= $menu['API']['items']['apis']['title'] ?>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(RbacHelper::can('settings.read.access') || RbacHelper::owner()):?>
                        <li class="m-menu__item <?= ((strpos(Url::toRoute(Yii::$app->user->identity->tenant->id . '/handbooks'), Yii::$app->controller->id))===false) ? '':'m-menu__item--active'; ?>">
                            <a href="<?= $menu['Handbooks']['items']['handbooks']['url'] ?>" class="m-menu__link">
                                <i class="m-menu__link-icon <?= $menu['Handbooks']['items']['handbooks']['icon'] ?>"></i>
                                <span class="m-menu__link-text">
                                    <?= $menu['Handbooks']['items']['handbooks']['title'] ?>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>