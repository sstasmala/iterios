<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$currTenant = Yii::$app->user->identity->tenant->id;
?>

<header class="m-grid__item m-header" data-minimize-offset="200" data-minimize-mobile-offset="200">
    <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand m-brand--skin-dark">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="<?= Yii::$app->params['baseUrl'] ?>/ita/" class="m-brand__logo-wrapper">
                            <img alt="" src="<?= Yii::$app->params['baseUrl'] . '/img/logo_ita.png' ?>"/>
                        </a>
                    </div>
                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
                        <!-- BEGIN: Left Aside Minimize Toggle -->
                        <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                            <span></span>
                        </a>
                        <!-- END -->
                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>
                        <!-- END -->
                        <!-- BEGIN: Responsive Header Menu Toggler -->
                        <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>
                        <!-- END -->
                        <!-- BEGIN: Topbar Toggler -->
                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                            <i class="flaticon-more"></i>
                        </a>
                        <!-- BEGIN: Topbar Toggler -->
                    </div>
                </div>
            </div>
            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head<?= ((isset(Yii::$app->view->params['header']['show_search_panel'])) && (Yii::$app->view->params['header']['show_search_panel']=== true)) ? ' has_search_panel' : '' ?>" id="m_header_nav">
                <div class="flex-inner m-topbar">
                     <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
                        <i class="la la-close"></i>
                    </button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >
                        <ul class="m-menu__nav m-menu__nav--submenu-arrow">
                            <li class="m-menu__item m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                <a href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon flaticon-add"></i>
                                    <span class="m-menu__link-text">
                                        <?= \app\helpers\TranslationHelper::getTranslation('create',$lang);?>
                                    </span>
                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a href="header/actions.html" class="m-menu__link" data-toggle="modal" data-target="#m_modal_create_contact">
                                                <i class="m-menu__link-icon flaticon-user-add"></i>
                                                <span class="m-menu__link-text">
                                                    Contact
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a  href="header/actions.html" class="m-menu__link" data-toggle="modal" data-target="#m_modal_create_company">
                                                <i class="m-menu__link-icon flaticon-layers"></i>
                                                <span class="m-menu__link-text">
                                                    Company
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a href="header/actions.html" class="m-menu__link" data-toggle="modal" data-target="#m_modal_create_request">
                                                <i class="m-menu__link-icon flaticon-exclamation-1"></i>
                                                <span class="m-menu__link-text">
                                                    <?= TranslationHelper::getTranslation('request', $lang, 'Request') ?>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a href="header/actions.html" class="m-menu__link" data-toggle="modal" data-target="#m_modal_create_deal">
                                                <i class="m-menu__link-icon flaticon-coins"></i>
                                                <span class="m-menu__link-text">
                                                    Deal
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a href="header/actions.html" class="m-menu__link" data-toggle="modal" data-target="#m_modal_create_reservation">
                                                <i class="m-menu__link-icon flaticon-calendar"></i>
                                                <span class="m-menu__link-text">
                                                    Reservation
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- END: Horizontal Menu -->
                    <?php if((isset(Yii::$app->view->params['header']['show_search_panel'])) && (Yii::$app->view->params['header']['show_search_panel']=== true)){ ?>
                        <!-- BEGIN: Quick search input -->
                        <?= $this->render('_search-dropdown'); ?>
                        <!-- END: Quick search input -->
                    <?php } ?>
                    
                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__quick-actions">
                                    <a href="#" class="m-nav__link" data-toggle="modal" data-target="#m_modal_header_requests">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-network" data-title="m-tooltip" title="<?= TranslationHelper::getTranslation('mhr_modal_title', $lang, 'Only my requests') ?>"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" id="notifications_block" m-dropdown-toggle="click" data-dropdown-persistent="true">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger" style="display: none;"></span>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-music-2" data-title="m-tooltip" title="<?= TranslationHelper::getTranslation('user_notifications', $lang, 'User Notifications') ?>"></i>
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper notivications-dropdown">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img/misc/notification_bg.jpg' ?>); background-size: cover;">
                                                <span class="m-dropdown__header-title">
                                                    <span id="notifications_count">0</span> <?= \app\helpers\TranslationHelper::getTranslation('new',$lang);?>
                                                </span>
                                                <span class="m-dropdown__header-subtitle">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('user_notifications',$lang);?>
                                                </span>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <div id="notifications-wrap">
                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                            <div id="notifications-list" class="m-list-timeline__items"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="<?= (count(Yii::$app->user->identity->tenants)>1)?'':'m--hide';?> m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-share" data-title="m-tooltip" title="<?= TranslationHelper::getTranslation('set_tenant', $lang, 'Set tenant') ?>"></i>
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper tenants_dropdown_table">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="margin-right: -10px;"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img/misc/notification_bg.jpg' ?>); background-size: cover;">
                                                <span class="m-dropdown__header-title">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('set_tenant',$lang);?>
                                                </span>
                                            </div>
                                            <div class="m-dropdown__body m-dropdown__body--paddingless">
                                                <div class="m-dropdown__content">
                                                    <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                                                        <div class="m-nav-grid m-nav-grid--skin-light">
                                                            <?php
                                                                $i = 0;
                                                            ?>

                                                            <?php foreach (Yii::$app->user->identity->tenants as $tenant): ?>
                                                                <?php $i++ ?>
                                                                <?= ($i % 2 ? '<div class="m-nav-grid__row">' : '') ?>
                                                                    <a href="/change-tenant?id=<?= $tenant->id ?>" class="m-nav-grid__item">
                                                                        <i class="m-nav-grid__icon flaticon-suitcase"></i>
                                                                        <span class="m-nav-grid__text">
                                                                            <?= $tenant->name ?>
                                                                        </span>
                                                                        <span class="m-nav-grid__text account-id-info">
                                                                            <?= app\helpers\TranslationHelper::getTranslation('account', $lang, 'Account') . ' ID: ' . $tenant->id ?>
                                                                        </span>
                                                                        <?= ($tenant->id==$currTenant) ? '<span class="m-badge m-badge--warning m-badge--wide m--margin-top-10">' . app\helpers\TranslationHelper::getTranslation('current', $lang, 'Current') . '</span>': ''; ?>
                                                                    </a>
                                                                <?= (!($i % 2) ? '</div>' : '') ?>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click"  data-title="m-tooltip" title="<?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?>">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-topbar__userpic">
                                            <img src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty(Yii::$app->user->identity->photo) ? Yii::$app->user->identity->photo : 'img/profile_default.png') ?>" class="m--img-rounded m--marginless m--img-centered profile_picture" alt=""/>
                                        </span>
                                        <span class="m-topbar__username m--hide">
                                            Nick
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img/misc/user_profile_bg.jpg' ?>); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty(Yii::$app->user->identity->photo) ? Yii::$app->user->identity->photo : 'img/profile_default.png') ?>" class="m--img-rounded m--marginless profile_picture" alt=""/>
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500 profile_name">
                                                            <?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?>
                                                        </span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link profile_email">
                                                            <?= Yii::$app->user->identity->email ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">
                                                                Section
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="<?= Yii::$app->params['baseUrl'] . '/ita/profile' ?>" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
                                                                    <span class="m-nav__link-wrap">
                                                                        <span class="m-nav__link-text">
                                                                            <?= \app\helpers\TranslationHelper::getTranslation('my_profile',$lang);?>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="<?= Yii::$app->params['baseUrl'] . '/ita/profile?tab=3' ?>" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-alert-2"></i>
                                                                <span class="m-nav__link-title">
                                                                    <span class="m-nav__link-wrap">
                                                                        <span class="m-nav__link-text">
                                                                            <?= \app\helpers\TranslationHelper::getTranslation('notifications', $lang, 'Notifications');?>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                                        <li class="m-nav__item">
                                                            <?= yii\helpers\Html::beginForm(['/logout'], 'post') .
                                                                yii\helpers\Html::submitButton(\app\helpers\TranslationHelper::getTranslation('logout',$lang),
                                                                    ['class' => 'btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder']) .
                                                                yii\helpers\Html::endForm() ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="<?= (\Yii::$app->authManager->getAssignment('system_admin',\Yii::$app->user->id))?'':'m--hide';?> m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-grid-menu"  data-title="m-tooltip" title="<?= TranslationHelper::getTranslation('set_application', $lang, 'Set application') ?>"></i>
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper" style="margin-right: -10px;">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(<?= Yii::$app->params['baseUrl'] . '/metronic/assets/app/media/img/misc/quick_actions_bg.jpg' ?>); background-size: cover;">
                                                <span class="m-dropdown__header-title">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('set_application',$lang);?>
                                                </span>
                                            </div>
                                            <div class="m-dropdown__body m-dropdown__body--paddingless">
                                                <div class="m-dropdown__content">
                                                    <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                                                        <div class="m-nav-grid m-nav-grid--skin-light">
                                                            <div class="m-nav-grid__row">
                                                                <?php if(\Yii::$app->authManager->getAssignment('system_admin',\Yii::$app->user->id)):?>
                                                                <a href="<?= Yii::$app->params['baseUrl'] ?>/ia" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-edit"></i>
                                                                    <span class="m-nav-grid__text">ITERIOS Admin</span>
                                                                </a>
                                                                <?php endif;?>
                                                                <a href="<?= Yii::$app->params['baseUrl'] ?>/ita" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-computer"></i>
                                                                    <span class="m-nav-grid__text">ITERIOS Agency Network</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </div>
</header>