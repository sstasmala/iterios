<?php
use yii\bootstrap\Html;
use app\assets\MetronicAsset;
use app\assets\MainAsset;

if (Yii::$app->user->isGuest) {
    $lang = substr(Yii::$app->request->getPreferredLanguage(), 0, 2);
} else {
    $lang = Yii::$app->user->identity->tenant->language->iso;
}
if($lang != 'en')
    $this->registerJsFile('@web/plugins/jquery-validation/src/localization/messages_'.$lang.'.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->user->identity->tenant->language->iso ?>">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,700" rel="stylesheet">
        <link rel="shortcut icon" href="<?=Yii::$app->params['baseUrl'].'/img/logo.ico'?>" />
        <?php echo $this->render('_js_options');?>
        <?php $this->head(); ?>
        <?php if(YII_ENV_PROD):?>
        <script>
            window['_fs_debug'] = false;
            window['_fs_host'] = 'fullstory.com';
            window['_fs_org'] = 'BMJXP';
            window['_fs_namespace'] = 'FS';
            (function(m,n,e,t,l,o,g,y){
                if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
                g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[];
                o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
                y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
                g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)};
                y="rec";g.shutdown=function(i,v){g(y,!1)};g.restart=function(i,v){g(y,!0)};
                y="consent";g[y]=function(a){g(y,!arguments.length||a)};
                g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
                g.clearUserCookie=function(){};
            })(window,document,window['_fs_namespace'],'script','user');
            <?php if(!Yii::$app->user->isGuest):?>
            FS.identify('[<?=Yii::$app->user->getId();?>]', {
                displayName: '<?=Yii::$app->user->identity->first_name.' '.Yii::$app->user->identity->last_name;?>',
                email: '<?=Yii::$app->user->identity->email?>',
                <?php if(isset(Yii::$app->user->identity->tenant) && !is_null(Yii::$app->user->identity->tenant)):?>
                tenant_int:<?=Yii::$app->user->identity->tenant->id;?>,
                <?php endif;?>
                // TODO: Add your own custom user variables here, details at
                // http://help.fullstory.com/develop-js/setuservars.
                reviewsWritten_int: 14,
            });
            <?php endif;?>
        </script>
        <?php endif;?>
    </head>
    
    <body class="<?= ((isset(Yii::$app->user->identity->tenant->language->iso))&&(!empty(Yii::$app->user->identity->tenant->language->iso))) ? 'lang-' . Yii::$app->user->identity->tenant->language->iso . ' ' : ''; ?>m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default no-padding">
        <?php $this->beginBody() ?>
        <!-- begin::Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- begin::Header -->
            <?php echo $this->render('_header'); ?>
            <!-- end::Header -->
            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <!-- begin::Left Aside -->
                <?php echo $this->render('_aside-left'); ?>
                <!-- end::Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <!-- begin::Subheader -->
                    <?php if (isset(Yii::$app->view->params['subheader']['show']) && Yii::$app->view->params['subheader']['show']):?>
                    <?php echo $this->render('_subheader'); ?>
                    <?php endif;?>
                    <!-- end::Subheader -->
                    <div class="m-content">
                        <?= $content ?>
                    </div>
                </div>
            </div>
            <!-- end::Body -->
            <!-- begin::Footer -->
            <?php echo $this->render('_footer'); ?>
            <!-- end::Footer -->
        </div>
        <!-- end::Page -->
        <!-- begin::Scroll Top -->
        <?php echo $this->render('_scroll-top'); ?>
        <!-- end::Scroll Top -->
        
        <?php MetronicAsset::register($this); ?>
        <?php MainAsset::register($this); ?>
        
        <!--Begin::Main Modals-->
        <?= $this->render('/partials/modals/_m_modal_create_company') ?>
        <?= $this->render('/partials/modals/_m_modal_create_contact') ?>
        <?= $this->render('/partials/modals/_m_modal_create_deal') ?>
        <?= $this->render('/partials/modals/_m_modal_create_request') ?>
        <?= $this->render('/partials/modals/_m_modal_create_reservation') ?>
        <?= $this->render('/partials/modals/_m_modal_header_requests') ?>
        <?= $this->render('/partials/modals/_m_modal_create_new_hotel') ?>
        <?= $this->render('/partials/modals/_m_modal_create_new_city') ?>
        <?= $this->render('/partials/modals/_m_header_modal_service_details') ?>
        <?= $this->render('/partials/modals/_m_header_modal_select_service') ?>
        <?= $this->render('/partials/modals/_m_header_modal_change_cp_method') ?>
        <?= $this->render('/partials/modals/_m_header_modal_cp_method_email') ?>
        <?= $this->render('/partials/modals/_m_header_modal_cp_method_link') ?>
        <?= $this->render('/partials/modals/_m_header_modal_cp_method_sms') ?>
        <!--End::Main Modals-->
        
        <?php if (isset($this->blocks['extra_modals'])){ ?>
            <?= $this->blocks['extra_modals'] ?>
        <?php }?>

        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>