<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title">
                <?= $this->title ?>
            </h3>
        </div>
        <?php if (isset($this->blocks['subheader_controls'])){ ?>
            <?= $this->blocks['subheader_controls'] ?>
        <?php }?>
    </div>
</div>