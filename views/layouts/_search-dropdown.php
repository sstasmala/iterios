<?php

use yii\helpers\Html;
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div id="search-panel" class="m-topbar">
    <form class="m-form m-form--fit m-form--label-align-right" autocomplete="off" id="search-top-filter">
        <div class="input_block">
            <div class="input_block__filters_indicator">
                <span id="filters_indicator" class="m-badge m-badge--primary m-badge--wide m-badge--rounded">3 опции</span>
            </div>
            <div class="input_block__input_body">
                <?=
                Html::input('text', 'quick-search', '', [
                    'placeholder' => TranslationHelper::getTranslation('search_text', $lang),
                    'id' => 'quick-search-input',
                    'class' => 'form-control m-input'
                ]);
                ?>
            </div>
            <div class="input_block__summary">
                <span id="summary_indicator">0 записей</span>
            </div>
            <div id="reset_search_input_button" class="input_block__reset">
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div id="search-dropdown">
            <div class="modal-content m-scrollable" data-scrollable="true">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-3 filters-left">
                            <div class="filters-search">
                                <?php if (isset($this->blocks['filters-left'])){ ?>
                                    <?= $this->blocks['filters-left'] ?>
                                <?php }?>
                            </div>
                        </div>
                        <div class="col-lg-9 filters-right">
                            <?php if (isset($this->blocks['filters-right'])){ ?>
                                <?= $this->blocks['filters-right'] ?>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="control-buttons">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9 buttons-col">
                        <a href="#" class="btn btn-primary btn-sm m-btn m-btn m-btn--icon" id="apply_filter">
                            <span>
                                <i class="la la-filter"></i>
                                <span><?= TranslationHelper::getTranslation('apply', $lang, 'Apply') ?></span>
                            </span>
                        </a>
                        <a href="#" class="btn btn-secondary m-btn btn-sm m-btn m-btn--icon" id="reset_filter">
                            <span>
                                <i class="la la-ban"></i>
                                <span><span><?= TranslationHelper::getTranslation('reset', $lang, 'Reset') ?></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
