<?php
/**
 * _js_options.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$this->registerJs('var baseUrl = "'.Yii::$app->params['baseUrl'].'";',\yii\web\View::POS_HEAD);
$this->registerJs('var serverTimezone = "'.date_default_timezone_get().'";',\yii\web\View::POS_HEAD);