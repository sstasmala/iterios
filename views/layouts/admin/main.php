<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

//AppAsset::register($this);
\app\assets\AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head()//sidebar-collapse ?>
    <link href="/admin/css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=Yii::$app->params['baseUrl']?>/img/admin_logo.ico" type="image/x-icon"/>
    <?= $this->render('_js_options');?>
</head>
<body class="hold-transition skin-black-light sidebar-mini  fixed">
<?php $this->beginBody() ?>

<div class="wrapper">

    <?= $this->render('header')?>
    <?= $this->render('menu')?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1><?=$this->title?></h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        </section>
        <section class="content">
        <?= $content ?>
        </section>
    </div>
    <?php $this->render('footer')?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
