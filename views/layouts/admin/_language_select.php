<?php
/**
 * _language_select.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/partials/_select_language.js', ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

$this->registerJs(
    "var translatable_controller_path = '{$model_controller_path}';"
    . "var default_language = '{$language->iso}';"
    , \yii\web\View::POS_HEAD
);

if (isset($model)) {
    $model_class_name_parts = explode('\\', get_class($model));
    $model_class_short_name = array_pop($model_class_name_parts);
    $this->registerJs(
        "var translatable_model_id = '{$model->id}';"
        . "var translatable_model_name = '{$model_class_short_name}';"
        , \yii\web\View::POS_HEAD
    );
}

if (isset($model) && ($model instanceof \app\models\base\BaseTranslationModel)) {
    $this->registerJs(
        "var translatable_attributes = ['". implode("', '", $model->getTranslatable()) . "'];"
        , \yii\web\View::POS_HEAD
    );
} else {
    $this->registerJs(
        "var translatable_attributes = [];"
        , \yii\web\View::POS_HEAD
    );
}
?>

<select id="lang-list" data-action="<?= $action ?>">
    <option selected="selected" value="<?= $language->iso ?>"><?= $language->name ?> (<?= $language->original_name ?>)</option>
</select>