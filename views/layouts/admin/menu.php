<?php
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
<!--        <div class="user-panel">-->
<!--            <div class="pull-left image">-->
<!--                <img src="/theme/img/avatar5.png"class="img-circle" alt="User Image">-->
<!--            </div>-->
<!--            <div class="pull-left info">-->
<!--                <p>Alexander Pierce</p>-->
<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
<!--            </div>-->
<!--        </div>-->
        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search...">-->
<!--                <span class="input-group-btn">-->
<!--                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree" style="margin-bottom: 90px">
            <li class="header">MAIN NAVIGATION</li>
            <li id="tenants"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tenants"><i class="fa fa-object-group"></i><span>Tenants</span></a></li>
            <li id="users"><a href="<?=Yii::$app->params['baseUrl']?>/ia/users"><i class="fa fa-users"></i><span>Users</span></a></li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money-bill"></i> <span>Accounts & payments</span>
                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="customers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/customers"><i class="fa fa-user"></i><span>Customers</span></a></li>
                    <li id="payment-gateways"><a href="<?=Yii::$app->params['baseUrl']?>/ia/payment-gateways"><i class="fa fa-money-bill"></i>Payment gateways</a></li>
                    <li id="exchange-rates"><a href="<?=Yii::$app->params['baseUrl']?>/ia/exchange-rates"><i class="fa fa-money-bill"></i>Exchange Rates</a></li>
                    <li id="financial-operations"><a href="<?=Yii::$app->params['baseUrl']?>/ia/financial-operations"><i class="fa fa-money-bill"></i>Financial operations</a></li>
                    <li id="tariffs-orders"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tariffs-orders"><i class="fa fa-money-bill"></i>Orders</a></li>
                    <li id="transactions"><a href="<?=Yii::$app->params['baseUrl']?>/ia/transactions"><i class="fa fa-random"></i>Transactions</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap"></i> <span>Tariffs</span>
                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="tariffs-feature"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tariffs-feature"><i class="fa fa-cogs"></i>Tariffs Feature</a></li>
                    <li id="tariffs"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tariffs"><i class="fa fa-calendar-check"></i>Tariffs</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap"></i> <span>Integration</span>
                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="suppliers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/suppliers"><i class="fa fa-tags"></i>Suppliers</a></li>
                    <li id="suppliers-credentials"><a href="<?=Yii::$app->params['baseUrl']?>/ia/suppliers-credentials"><i class="fa fa-tags"></i>Suppliers Credentials</a></li>
                    <li id="providers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/providers"><i class="fa fa-tags"></i>Providers</a></li>
                    <li id="providers-credentials"><a href="<?=Yii::$app->params['baseUrl']?>/ia/providers-credentials"><i class="fa fa-tags"></i>Providers Credentials</a></li>
                </ul>
            </li>

            <li class="treeview" id="testsg">
                <a href="#">
                    <i class="fa fa-file-alt"></i> <span>Logs</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="logs-cron"><a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-cron"><i class="fa fa-file"></i>Cron</a></li>
                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-file"></i>Notification</a></li>
                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia/log"><i class="fa fa-file"></i>CRUD</a></li>
                     <li id="reminder-log"><a href="<?=Yii::$app->params['baseUrl']?>/ia/reminder-log">
                        <i class="fa fa-bell"></i>Reminder Log sends</a>
                     </li>
                    <li id="notification-log"><a href="<?=Yii::$app->params['baseUrl']?>/ia/notification-log">
                        <i class="fa fa-bell"></i>Notification Log sends</a>
                    </li>


                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-alt"></i> <span>Mail logs</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="logs-system-email"><a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-system-email"><i class="fa fa-file"></i>System mail</a></li>
                    <li id="logs-public-email"><a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-public-email"><i class="fa fa-file"></i>Public mail</a></li>
                    <li id="logs-email-request"><a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-email-request"><i class="fa fa-file"></i>WebHooks</a></li>
                    <li id="email-test-send"><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-file"></i>Test send</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-alt"></i> <span>SMS logs</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="logs-sms"><a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-sms"><i class="fa fa-file"></i>System sms</a></li>
                    <li id="send-test-sms"><a href="<?=Yii::$app->params['baseUrl']?>/ia/send-test-sms"><i class="fa fa-file"></i>Test send</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-alt"></i> <span>Delivery</span>
                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="sms-list"><a href="<?=Yii::$app->params['baseUrl']?>/ia/sms-list"><i class="fa fa-clone"></i>Sms list</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Constructors</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-bell"></i> <span>Reminders</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="reminder-constructor"><a href="<?=Yii::$app->params['baseUrl']?>/ia/reminder-constructor"><i class="fa fa-cogs"></i>Reminder Constructor</a></li>
                            <li id="reminder-calendar"><a href="<?=Yii::$app->params['baseUrl']?>/ia/reminder-calendar"><i class="fa fa-calendar-alt"></i>Reminder Calendar</a></li>
                            <li id="reminder-queue"><a href="<?=Yii::$app->params['baseUrl']?>/ia/reminder-queue"><i class="fa fa-chevron-circle-right"></i>Reminder Queue</a></li>
                            <li id="reminder-event-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/reminder-event-types"><i class="fa fa-first-order-alt"></i>Reminder Event Types</a></li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-list-alt"></i> <span>Order reminders</span>
                                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li id="order-reminder-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/order-reminder-system"><i class="fa fa-circle"></i>System</a></li>
                                    <li id="order-reminder-public"><a href="<?=Yii::$app->params['baseUrl']?>/ia/order-reminder-public"><i class="fa fa-circle"></i>Public</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-bell"></i> <span>Notifications</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="notification-constructor"><a href="<?=Yii::$app->params['baseUrl']?>/ia/notification-constructor"><i class="fa fa-cogs"></i>Notification Constructor</a></li>
                            <li id="notification-user"><a href="<?=Yii::$app->params['baseUrl']?>/ia/notification-user"><i class="fa fa-users"></i>Notification User</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-clone"></i> <span>Segments</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="segments"><a href="<?=Yii::$app->params['baseUrl']?>/ia/segments"><i class="fa fa-clone"></i>Segments constructor</a></li>
                            <li id="segments-relations"><a href="<?=Yii::$app->params['baseUrl']?>/ia/segments-relations"><i class="fa fa-clone"></i>Segments Relations</a></li>
                            <li id="segments-result-list"><a href="<?=Yii::$app->params['baseUrl']?>/ia/segments-result-list"><i class="fa fa-clone"></i>Segments Result List</a></li>
                        </ul>
                    </li>

                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-list-ul"></i>Email templates constructor</a></li>
                    <li id="templates"><a href="<?=Yii::$app->params['baseUrl']?>/ia/templates"><i class="fa fa-envelope"></i>System mail constructor</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-list-ul"></i> <span>Documents constructor</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="documents-template-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/documents-template-system"><i class="fa fa-circle"></i>System</a></li>
                            <li id="documents-template-public"><a href="<?=Yii::$app->params['baseUrl']?>/ia/documents-template-public"><i class="fa fa-circle"></i>Public</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-list-ul"></i> <span>EMAIL, SMS Templates</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-list-ul"></i> <span>EMAIL</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li id="delivery-email-templates-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/delivery-email-templates-system"><i class="fa fa-circle"></i>System</a></li>
                                    <li id="delivery-email-templates-public"><a href="<?=Yii::$app->params['baseUrl']?>/ia/delivery-email-templates-public"><i class="fa fa-circle"></i>Public</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-list-ul"></i> <span>SMS</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li id="delivery-sms-templates-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/delivery-sms-templates-system"><i class="fa fa-circle"></i>System</a></li>
                                    <li id="delivery-sms-templates-public"><a href="<?=Yii::$app->params['baseUrl']?>/ia/delivery-sms-templates-public"><i class="fa fa-circle"></i>Public</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-list-ul"></i>Payments constructor</a></li>
                    <li id="services"><a href="<?=Yii::$app->params['baseUrl']?>/ia/services"><i class="fa fa-star"></i>Services constructor</a></li>
                    <li id="services-fields-default"><a href="<?=Yii::$app->params['baseUrl']?>/ia/services-fields-default"><i class="fa fa-square"></i>Services default fields</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-clone"></i> <span>Requisites</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="requisites-groups"><a href="<?=Yii::$app->params['baseUrl']?>/ia/requisites-groups"><i class="fa fa-list-ul"></i>Requisites groups</a></li>
                            <li id="requisites"><a href="<?=Yii::$app->params['baseUrl']?>/ia/requisites"><i class="fa fa-list-ul"></i>Requisites</a></li>
                            <li id="requisites-fields"><a href="<?=Yii::$app->params['baseUrl']?>/ia/requisites-fields"><i class="fa fa-list-ul"></i>Requisites field constructor</a></li>
                            <li id="requisites-sets"><a href="<?=Yii::$app->params['baseUrl']?>/ia/requisites-sets"><i class="fa fa-list-ul"></i>Requisites sets</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-database"></i> <span>References</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-database"></i> <span>Types</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="emails-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/emails-types"><i class="fa fa-circle"></i>Email types</a></li>
                            <li id="phones-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/phones-types"><i class="fa fa-circle"></i>Phones types</a></li>
                            <li id="socials-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/socials-types"><i class="fa fa-circle"></i>Socials types</a></li>
                            <li id="messengers-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/messengers-types"><i class="fa fa-circle"></i>Messengers types</a></li>
                            <li id="sites-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/sites-types"><i class="fa fa-circle"></i>Sites types</a></li>
                            <li id="passports-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/passports-types"><i class="fa fa-circle"></i>Passports types</a></li>
                            <li id="visas-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/visas-types"><i class="fa fa-circle"></i>Visas types</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-database"></i> <span>Companies</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="companies-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/companies-types"><i class="fa fa-circle"></i>Types</a></li>
                            <li id="companies-kind-activities"><a href="<?=Yii::$app->params['baseUrl']?>/ia/companies-kind-activities"><i class="fa fa-circle"></i>Kind of activities</a></li>
<!--                            <li id="companies-contact-sources"><a href="Yii::$app->params['baseUrl']/ia/companies-contact-sources"><i class="fa fa-circle"></i>Contact sources</a></li>-->
                            <li id="companies-statuses"><a href="<?=Yii::$app->params['baseUrl']?>/ia/companies-statuses"><i class="fa fa-circle"></i>Statuses</a></li>
                        </ul>
                    </li>
                    <li id="tasks-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tasks-types"><i class="fa fa-tags"></i>Tasks types</a></li>
                    <li id="provider-function"><a href="<?=Yii::$app->params['baseUrl']?>/ia/provider-function"><i class="fa fa-tags"></i>Provider & Supplier function</a></li>
                    <li id="request-channels"><a href="<?=Yii::$app->params['baseUrl']?>/ia/request-channels"><i class="fa fa-tags"></i>Request channels</a></li>
                    <li id="supplier-type"><a href="<?=Yii::$app->params['baseUrl']?>/ia/supplier-type"><i class="fa fa-tags"></i>Supplier type</a></li>
                    <li id="provider-type"><a href="<?=Yii::$app->params['baseUrl']?>/ia/provider-type"><i class="fa fa-tags"></i>Provider type</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tags"></i> <span>Tags</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="tags-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tags-system"><i class="fa fa-circle"></i>Tags system</a></li>
                            <li id="tags-custom"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tags-custom"><i class="fa fa-circle"></i>Tags custom</a></li>
                        </ul>
                    </li>

                    <li id="placeholders"><a href="<?=Yii::$app->params['baseUrl']?>/ia/placeholders"><i class="fa fa-tags"></i>Placeholders</a></li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tags"></i> <span>Document</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="document-placeholders"><a href="<?=Yii::$app->params['baseUrl']?>/ia/placeholders-document"><i class="fa fa-circle"></i>Placeholders</a></li>
                            <li id="documents-type"><a href="<?=Yii::$app->params['baseUrl']?>/ia/documents-type"><i class="fa fa-circle"></i>Documents Type</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tags"></i> <span>Delivery</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="delivery-placeholders"><a href="<?=Yii::$app->params['baseUrl']?>/ia/placeholders-delivery"><i class="fa fa-circle"></i>Placeholders</a></li>
                            <li id="delivery-types"><a href="<?=Yii::$app->params['baseUrl']?>/ia/delivery-types"><i class="fa fa-circle"></i>Delivery Types</a></li>
                        </ul>
                    </li>

                    <li id="request-statuses"><a href="<?=Yii::$app->params['baseUrl']?>/ia/request-statuses"><i class="fa fa-database"></i>Request statuses</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-database"></i> <span>Request canceled</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="request-canceled-reasons-system"><a href="<?=Yii::$app->params['baseUrl']?>/ia/request-canceled-reasons-system"><i class="fa fa-circle"></i>System reason</a></li>
                            <li id="request-canceled-reasons-other"><a href="<?=Yii::$app->params['baseUrl']?>/ia/request-canceled-reasons-other"><i class="fa fa-circle"></i>Others reason</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-database"></i> <span>Holidays</span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="system-holidays"><a href="<?=Yii::$app->params['baseUrl']?>/ia/system-holidays"><i class="fa fa-circle"></i>System holidays</a></li>
                            <li id="public-holidays"><a href="<?=Yii::$app->params['baseUrl']?>/ia/public-holidays"><i class="fa fa-circle"></i>Public holidays</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-language"></i> <span>Translations</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="ui-translations"><a href="<?=Yii::$app->params['baseUrl']?>/ia/ui-translations"><i class="fa fa-window-maximize"></i>UI translations</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i> <span>Settings</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="settings"><a href="<?=Yii::$app->params['baseUrl']?>/ia/settings"><i class="fa fa-cogs"></i>Base settings</a></li>
                    <li id="email-providers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/email-providers"><i class="fa fa-envelope"></i>eMail providers</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-comment"></i> <span>SMS Providers</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="sms-system-providers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/sms-system-providers"><i class="fa fa-circle"></i>System providers</a></li>
                            <li id="sms-countries"><a href="<?=Yii::$app->params['baseUrl']?>/ia/sms-countries"><i class="fa fa-circle"></i>Countries</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-magic"></i>Wizard</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-map"></i> <span>Demo data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li id="demo-contacts"><a href="<?=Yii::$app->params['baseUrl']?>/ia/demo-contacts"><i class="fa fa-newspaper"></i>Contacts</a></li>
                            <li id="demo-companies"><a href="<?=Yii::$app->params['baseUrl']?>/ia/demo-companies"><i class="fa fa-industry"></i>Companies</a></li>
                            <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-share-square"></i>Requests</a></li>
                            <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-check-square"></i>Reservation</a></li>
                            <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-tags"></i>Tags</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=Yii::$app->params['baseUrl']?>/ia"><i class="fa fa-check-square"></i>First steps</a></li>
                    <li id="templates-handlers"><a href="<?=Yii::$app->params['baseUrl']?>/ia/templates-handlers"><i class="fa fa-arrows-alt-h"></i>System Temp. Handl.</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cubes"></i> <span>Email</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="mailer"><a href="<?=Yii::$app->params['baseUrl']?>/ia/mailer"><i class="fa fa-cube"></i>Web Mail</a></li>
                    <li id="tenant-settings"><a href="<?=Yii::$app->params['baseUrl']?>/ia/tenant-settings"><i class="fa fa-cube"></i>Tenant settings</a></li>

                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
