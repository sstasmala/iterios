<?php

use app\helpers\DateTimeHelper;
use app\models\AgentsMapping;
use app\models\Tenants;

$curentTime = date('Y-m-d H:i:s');
$curTimezone = date_default_timezone_get();

$user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
$tenants = [];
$tenants_ids = AgentsMapping::find()->where(['user_id' => $user_id])->asArray()->all();
if (!empty($tenants_ids)) {
    $tenants_ids = array_column($tenants_ids, 'tenant_id');
    if ($tenants_ids)
        $tenants = Tenants::find()->select(['timezone', 'name'])->where(['in', 'id', $tenants_ids])->asArray()->all();
}

$tenantsTimes = [];
if ($tenants) {
    foreach($tenants as $ten) {
        $tenantsTimes[$ten['name'] . '(<span style="font-size:10px">'.$ten['timezone'].'</span>)'] = DateTimeHelper::convertTimestampToDateTime(time(), 'Y-m-d H:i:s', $ten['timezone']);
    }
}

$this->registerJs("jQuery(document).ready(function(){
    function addzero(number) {
        if (number < 10) {
            return '0' + number;
        }

        return number;
    }

    var seconds = 1;
    setInterval(function(){
        var timeBlocks = jQuery('.server-time');
        
        timeBlocks.each(function(){
            var time = $(this).data('timeJs');
            var name = $(this).data('type');
    
            var date = new Date(time);
            date.setSeconds(date.getSeconds() +seconds);
    
            var datestr = 
                date.getFullYear() + '-' + 
                addzero(date.getMonth()+1) + '-' +  
                addzero(date.getDate()) + ' ' +  
                addzero(date.getHours()) + ':' +  
                addzero(date.getMinutes()) + ':' +  
                addzero(date.getSeconds());
           
    
            $(this).html('<strong>' + name +'</strong>: '+ datestr);
        });
        
        seconds++;
    }, 1000);
});", \yii\web\View::POS_END);

?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?=Yii::$app->params['baseUrl']?>/ia" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>I</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Iterios</b>Admin</span>
<!--        <img class="logo-lg" src="/img/logo_ita.png">-->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle fas fa-bars" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="btn-group" style="margin: 7px 0px 0px 10px;">
            <button type="button" class="btn btn-default server-time" data-time-js="<?= $curentTime; ?>" data-type='Server (<span style="font-size:10px"><?= $curTimezone; ?></span>)'>
                <strong>Server(<span style="font-size:10px"><?= $curTimezone; ?></span>)</strong>: <?= $curentTime; ?>
            </button>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Tenant Times</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <?php if (!empty($tenantsTimes)): $i = 0; ?>
                    <?php foreach($tenantsTimes as $tName => $tTime): $i++; ?>
                        <li style="width: 400px;text-align: center;" data-type='<?= $tName; ?>' class="server-time" data-time-js="<?= $tTime; ?>">
                            <strong><?= $tName . '</strong>: ' . $tTime; ?>
                        </li>
                        <?php if (count($tenantsTimes) != $i): ?>
                        <li class="divider"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="user-menu">
                    <a href="<?=Yii::$app->params['baseUrl']?>/ia/logs-cron">
                        <?php
                        $last_run = '<div style="color: red;display: inline">Never</div>';
                        if(file_exists(Yii::$app->basePath.'/main.log'))
                        {
                            $f = fopen(Yii::$app->basePath.'/main.log','r');
                            $val = fgets($f);
                            $last_run = DateTimeHelper::convertTimestampToDateTime($val,'Y-m-d H:i:s',date_default_timezone_get());
                            $last_run = '<div style="color: green;display: inline">'.$last_run.'</div>';
                            fclose($f);
                        }
                        ?>
                        <div>Last CRON: <?=$last_run?></div>
                    </a>
                </li>
                <!-- Messages: style can be found in dropdown.less-->
                <li class="user-menu">
                    <a href="<?=Yii::$app->params['baseUrl']?>/ita">
                        <i class="fa fa-arrow-right"></i>
                        <img class="user-image" src="<?=Yii::$app->params['baseUrl']?>/img/logo.png" style="width: 25px;height: 25px">
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty(Yii::$app->user->identity->photo) ? Yii::$app->user->identity->photo : 'img/profile_default.png') ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty(Yii::$app->user->identity->photo) ? Yii::$app->user->identity->photo : 'img/profile_default.png') ?>" class="img-circle" alt="User Image">

                            <p>
                                <?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?>
                                <small><?= Yii::$app->user->identity->email ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
<!--                        <li class="user-body">-->
<!--                            <div class="row">-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Followers</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Sales</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Friends</a>-->
<!--                                </div>-->
<!--                            </div>-->
                            <!-- /.row -->
<!--                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Yii::$app->params['baseUrl'] . '/ita/profile' ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= yii\helpers\Html::beginForm(['/logout'], 'post') .
                                yii\helpers\Html::submitButton('Logout',
                                    ['class' => 'btn btn-default btn-flat']) .
                                yii\helpers\Html::endForm() ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="<?= Yii::$app->params['baseUrl'] . '/ia/filemanager' ?>"><i class="fas fa-save"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
