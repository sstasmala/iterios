<?php
/**
 * _base_js_options.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$this->registerJs('var baseUrl = "'.Yii::$app->params['baseUrl'].'";',\yii\web\View::POS_HEAD);
$page = Yii::$app->controller->getRoute();
$session = \Yii::$app->session;

$translates =  \app\helpers\TranslationHelper::getPageTranslation($page,substr(Yii::$app->request->getPreferredLanguage(), 0, 2));
$this->registerJs('var translations = '.json_encode($translates).';',\yii\web\View::POS_HEAD);

$this->registerJs('function GetTranslation(key){ if(translations[key]!=undefined) return translations[key]; return "["+key+"]"}'
    ,\yii\web\View::POS_HEAD);