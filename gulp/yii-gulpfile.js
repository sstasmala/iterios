var gulp = require('gulp');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var uglify = require('gulp-uglify-es').default;
var concat = require('gulp-concat');
var rename = require("gulp-rename");

/***** CONFIG PARAMS *****/
var paths = {
    css : [
        'web/css/**/*.scss'
    ],
    js : [
        'web/js/**/*.js',
        '!web/js/**/*.min.js'
    ]
};

/***** COMPILE FRONT CSS *****/
gulp.task('build-css', function() {
    return gulp.src(paths.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleancss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(file){
            return file.base;
        }));
});

/***** COMPILE FRONT JS *****/
gulp.task('build-js', function() {
    return gulp.src(paths.js)
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(file){
            return file.base;
        }));
});

/***** DEFAULT *****/
gulp.task('yii-default', [
    'build-css',
    'build-js'
]);

/***** WATCH *****/
gulp.task('yii-watch', function () {
    gulp.watch(paths.css , ['build-css']);
    gulp.watch(paths.js , ['build-js']);
});