<?php

namespace app\controllers;

use app\components\events\SystemResetPasswordEvent;
use app\components\events\SystemTenantRegistrationEvent;
use app\components\events\SystemUserRegistrationEvent;
use app\helpers\MCHelper;
use app\helpers\TenantHelper;
use app\models\ChangePasswordForm;
use app\models\SignUpForm;
use app\models\Tenants;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['change-tenant', 'logout', 'error', 'index', 'create-tenant', 'get-country'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'get-countries-options', 'get-country', 'sign-up', 'forgot-password', 'change-password', 'error', 'index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
        $this->layout = 'base';
    }

    public function actionGetCountry($title)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = MCHelper::searchCountry($title, $lang);
        if(is_null($result))
            return [];
        return $result;
    }

    public function actionGetCountriesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $search = Yii::$app->request->post('search', '');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = MCHelper::searchCountry($search, $lang);
        if(is_null($result))
            return [];
        return $result;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\system\ErrorHandler'//'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $id
     */
    public function actionChangeTenant($id)
    {
        $user = User::findOne(Yii::$app->user->id);

        if ($user->changeCurrentTenant($id))
            $user->save();

        $this->redirect('/ita');
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->getIsGuest())
            return $this->render('index');

        if (!\Yii::$app->user->identity->tenant)
            return $this->render('create_tenant');

        $this->redirect('/ita');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateTenant()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!\Yii::$app->user->identity->tenant) {
            $company_name = Yii::$app->request->post('company', false);
            $language = Yii::$app->request->post('language', false);

            if (empty($company_name) || empty($language))
                $this->redirect('/');

            $model = User::findOne(Yii::$app->user->id);
            $model->current_tenant_id = TenantHelper::createTenant($model, $company_name, $language);
            if($model->save())
            {
                $tenant = Tenants::findOne(['id'=>$model->current_tenant_id]);
                if(!is_null($tenant))
                {
                    $event = new SystemTenantRegistrationEvent($model,$tenant);
                    Yii::$container->get('systemEmitter')->trigger($event);
                }
            }

            return ['success' => true];
        }

        return ['error' => true];
    }

    /**
     * Login action.
     *
     * @return bool|array
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionLogin()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $post_data = Yii::$app->request->post();

        $model = new LoginForm();
        $data = [
            'LoginForm' => $post_data
        ];

        if ($model->load($data)) {
            if ($user = $model->login())
                return ['login' => true, 'redirect_url' => empty($post_data['redirect_url']) ? false : base64_decode($post_data['redirect_url'])];

            return ['error' => $this->errorHandler($model->errors)];
        }

        return false;
    }

    public function actionForgotPassword()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email', false);

        if (empty($email))
            return ['error' => 'Field "email" is empty!'];

        $user = User::findByEmail($email);

        if (empty($user))
            return ['error' => 'User with email <b>'.$email.'</b> does not exist!'];

        if (empty($user->password_reset_token) || User::isPasswordResetTokenValid($user->password_reset_token) == false) {
            $user->generatePasswordResetToken();
            if($user->save())
            {
                $event = new SystemResetPasswordEvent($user);
                Yii::$container->get('systemEmitter')->trigger($event);

                return ['success' => true];
            }
        }

        $event = new SystemResetPasswordEvent($user);
        Yii::$container->get('systemEmitter')->trigger($event);

        return ['success' => true];
    }

    public function actionChangePassword($token)
    {
        $user = User::findByPasswordResetToken($token);

        if (is_null($user))
            throw new NotFoundHttpException('User with token does not exist!');

        $model = new ChangePasswordForm();
        $model->user = $user;
        $data = [
            'ChangePasswordForm' => Yii::$app->request->post()
        ];

        if ($model->load($data)) {
            if ($model = $model->changePassword()) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                if (empty($model->errors))
                    return ['success' => true];

                return ['error' => $this->errorHandler($user->errors)];
            }
        }

        return $this->render('change_password', ['user' => $user]);
    }

    /**
     * @return bool|array
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionSignUp()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new SignUpForm();
        $data = [
            'SignUpForm' => Yii::$app->request->post()
        ];

        if ($model->load($data)) {
            if ($user = $model->signUp()) {
                if (empty($user->errors)) {
                    return ['user_id' => $user->id];
                }
                return ['error' => $this->errorHandler($user->errors)];
            }
        }

        return ['error' => $model->errors];
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    /**
     * @param $errors
     *
     * @return string
     */
    private function errorHandler($errors)
    {
        $error_str = '';

        foreach ($errors as $error)
            $error_str .= implode(' ', $error).'<br/>';

        return $error_str;
    }
}
