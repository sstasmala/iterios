<?php
/**
 * SiteController.php
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\controllers\prot;

use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    public function init()
    {
        parent::init();
        $this->layout = 'base';
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionChangeTenant($id)
    {
        $this->redirect('/ita');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['login' => true];
    }

    public function actionForgotPassword()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionSignUp()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['user_id' => 1];
    }
}