<?php
/**
 * SystemNotificationsHelper.php
 * @copyright ©Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;

use app\models\SystemNotifications;
use app\models\Tenants;
use app\models\User;

class SystemNotificationsHelper
{
    /**
     * @param $text
     * @param User $user
     * @param Tenants $tenant
     * @return bool|int
     */
    public static function addNotification($text, User $user, Tenants $tenant)
    {
        $notification = new SystemNotifications();
        $notification->text = $text;
        $notification->user_id = $user->id;
        $notification->tenant_id = $tenant->id;

        return $notification->save() ? $notification->id : false;
    }

    /**
     * getNotifications() - get all notification current user & current tenant
     * getNotifications(false | true) - get read | not read notification current user & current tenant
     * getNotifications(null, false, false) - get ALL notifications
     * getNotifications(false | true, false, false) - get ALL read | not read notifications
     * getNotifications(null, 1, 2) - get all notifications user ID 1 & tenant ID 2
     * getNotifications(false | true, 1, 2) - get all read | not read notifications user ID 1 & tenant ID 2
     *
     * @param bool $read
     * @param integer $user_id
     * @param integer $tenant_id
     * @return bool|\yii\db\ActiveQuery|\yii\db\ActiveRecord[]
     */
    public static function getNotifications($read = null, $user_id = null, $tenant_id = null)
    {
        $notifications = static::getNotificationsObject($read, $user_id, $tenant_id);

        return isset($notifications) ? $notifications->asArray()->all() : false;
    }

    /**
     * @param null $read
     * @param null $user_id
     * @param null $tenant_id
     * @return bool|\yii\db\ActiveQuery|\yii\db\ActiveRecord[]
     */
    public static function getNotificationsObject($read = null, $user_id = null, $tenant_id = null)
    {
        if (!is_null($read) && !is_bool($read))
            return false;

        if (is_null($user_id))
            $user_id = \Yii::$app->user->id;

        if (is_null($tenant_id))
            $tenant_id = \Yii::$app->user->identity->tenant->id;

        $notifications = SystemNotifications::find()
            ->orderBy('created_at DESC');

        if ($user_id !== false)
            $notifications->where(['user_id' => $user_id]);

        if ($tenant_id !== false)
            $notifications->andWhere(['tenant_id' => $tenant_id]);

        if (!is_null($read))
            $notifications->andWhere(['read' => $read]);

        return $notifications;
    }
}