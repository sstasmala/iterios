<?php
/**
 * RbacHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


use yii\web\HttpException;

class RbacHelper
{
    /**
     * @param string $permission
     * @param null|string $description
     * @throws \Exception
     */
    public static function createPermission($permission,$description = null)
    {
        $auth = \Yii::$app->authManager;
        $perm = $auth->createPermission($permission);
        if(!is_null($description))
            $perm->description = $description;
        $auth->add($perm);
    }

    /**
     * @param string $role
     * @param null|string $description
     * @throws \Exception
     */
    public static function createRole($role,$description = null)
    {
        $auth = \Yii::$app->authManager;
        $rl = $auth->createRole($role);
        if(!is_null($description))
            $rl->description = $description;
        $auth->add($rl);
    }

    /**
     * @param string $role
     * @param string $permission
     * @throws \yii\base\Exception
     */
    public static function assignPermission($role,$permission)
    {
        $auth = \Yii::$app->authManager;
        $r = $auth->getRole($role);
        $p = $auth->getPermission($permission);
        $auth->addChild($r,$p);
    }

    /**
     * @param string $role
     * @param string $permission
     */
    public static function unassignPermission($role,$permission)
    {
        $auth = \Yii::$app->authManager;
        $r =  $auth->getRole($role);
        $p =  $auth->getPermission($permission);
        $auth->removeChild($r,$p);
    }

    /**
     * @return \yii\rbac\Role[]
     */
    public static function getRoles()
    {
        return \Yii::$app->authManager->getRoles();
    }

    /**
     * @param integer $tenant_id
     * @return \yii\rbac\Role[]
     */
    public static function getRolesByTenant($tenant_id)
    {
        $roles = \Yii::$app->authManager->getRoles();
        $result = [];
        foreach ($roles as $r)
        {
            $parts = explode('.',$r->name);
            if($parts[0] == $tenant_id)
                $result[] = $r;
        }
        return $result;
    }

    /**
     * @param $name
     * @return null|\yii\rbac\Role
     */
    public static function getRole($name)
    {
        return \Yii::$app->authManager->getRole($name);
    }

    /**
     * @param string $role
     * @return \yii\rbac\Item[]
     */
    public static function getAssignedPermissions($role)
    {
        return \Yii::$app->authManager->getChildren($role);
    }

    /**
     * @param integer $user_id
     * @return \yii\rbac\Assignment[]
     */
    public static function getAssignedRoles($user_id,$tenant_id = null)
    {
        $roles = \Yii::$app->authManager->getAssignments($user_id);
        if(!is_null($tenant_id))
            foreach ($roles as $k => $v)
            {
                $parts = explode('.',$v->roleName);
                if($parts[0] != $tenant_id)
                    unset($roles[$k]);
            }
        return $roles;
    }

    /**
     * @param string $role
     * @param string $permission
     * @return bool
     */
    public static function isAssigned($role,$permission)
    {
        $auth = \Yii::$app->authManager;
        $r = self::getRole($role);
        $p = self::getPermission($permission);
        if(is_null($r) || is_null($p))
            return false;
        return $auth->hasChild($r,$p);
    }


    /**
     * @return \yii\rbac\Permission[]
     */
    public static function getPermissions()
    {
        return \Yii::$app->authManager->getPermissions();
    }

    /**
     * @param $name
     * @return null|\yii\rbac\Permission
     */
    public static function getPermission($name)
    {
        return \Yii::$app->authManager->getPermission($name);
    }

    /**
     * @param string $role
     * @param integer $user_id
     * @throws \Exception
     */
    public static function assignRole($role,$user_id)
    {
        $auth = \Yii::$app->authManager;
        $r = $auth->getRole($role);
        $auth->assign($r,$user_id);
    }

    /**
     * @param string $role
     * @param integer $user_id
     */
    public static function unassignRole($role,$user_id)
    {
        $auth = \Yii::$app->authManager;
        $r = $auth->getRole($role);
        $auth->revoke($r,$user_id);
    }

    /**
     * @param string $role
     */
    public static function deleteRole($role)
    {
        $auth = \Yii::$app->authManager;
        $role = self::getRole($role);
        $auth->remove($role);
    }

    /**
     * @param string $role
     * @return array
     */
    public static function getAssignedUsers($role)
    {
        $auth = \Yii::$app->authManager;
        return $auth->getUserIdsByRole($role);
    }

    /**
     * @param string $old_name
     * @param $obj
     * @return bool
     * @throws \Exception
     */
    public static function updateItem($old_name, $obj)
    {
        $auth = \Yii::$app->authManager;
        return $auth->update($old_name,$obj);
    }

    /**
     * @param string $permission
     * @return bool
     * @throws HttpException
     */
    public static function can($permission, $additional = [])
    {
        if(\Yii::$app->user->isGuest)
            throw new HttpException(403);
        if(!isset(\Yii::$app->user) || !isset(\Yii::$app->user->identity) || !isset(\Yii::$app->user->identity->tenant))
            throw new HttpException(403);
        $tenant = \Yii::$app->user->identity->tenant;
        if(!empty($additional)) {
            foreach ($additional as $v)
                if (\Yii::$app->user->can($tenant->id . '.' . $permission . '.' . $v))
                    return true;
        }
        else
            if(\Yii::$app->user->can($tenant->id.'.'.$permission))
                return true;
        return false;
    }

    /**
     * @return bool
     * @throws HttpException
     */
    public static function owner()
    {
        if(\Yii::$app->user->isGuest)
            throw new HttpException(403);
        if(!isset(\Yii::$app->user) || !isset(\Yii::$app->user->identity) || !isset(\Yii::$app->user->identity->tenant))
            throw new HttpException(403);

        if (\Yii::$app->user->id === \Yii::$app->user->identity->tenant->owner_id)
            return true;

        return false;
    }
}