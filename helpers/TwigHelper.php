<?php
/**
 * TwigHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


class TwigHelper
{
    /**
     * To change delimiters:
     * $lexer = new \Twig_Lexer($twig,[
     * 'tag_variable' => ['{','}']
     * ]);
     *
     * $twig->setLexer($lexer);
     *
     * @param $scheme array
     * @param $text   string
     * @param $strict bool
     *
     * @return string
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public static function render($scheme, $text, $strict = false): string
    {
        foreach ($scheme as $key => $item) {
            $text = str_replace('{{' . $key . '}}','{{' . $key.'|raw' . '}}', $text);
        }

        $loader = new \Twig_Loader_Array(['text' => $text]);
        $twig = new \Twig_Environment($loader,['strict_variables' => $strict]);

        return $twig->render('text', $scheme);
    }
}