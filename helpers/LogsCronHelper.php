<?php
/**
 * LognCronHelper.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;


use app\models\LogsCron;

class LogsCronHelper
{
    /**
     * @param array $data = [
     *     'command' => 'backup_contacts',
     *     'status' => LogsCron::STATUS_SUCCESS,
     *     'output_file' => 'logs/cron-12345.log',
     *     'time_start' => 123456789,
     *     'time_end' => 987654321,
     *     'memory_usage' => '1.25mb'
     * ]
     *
     * @return bool
     */
    public static function setLog(array $data) {
        $logs_model = null;
        if(isset($data['id']))
            $logs_model = LogsCron::findOne(['id'=>$data['id']]);
        else
            $logs_model = new LogsCron();

        $data = [
            'LogsCron' => $data
        ];

        if ($logs_model->load($data) && $logs_model->save())
            return $logs_model->id;

        return false;
    }

    public static function deleteLog(int $id) {
        $mod = LogsCron::findOne(['id' => $id]);
        if (empty($mod))
            return;

        $mod->delete();
    }
}