<?php

namespace app\helpers;

use app\components\notifications\contacts\Receiver;
use app\components\notifications\Notification;
use app\models\Log;
use app\models\LogsEmail;
use app\models\Templates;

class LogHelper
{
    public static function setGroupId($itemId, $mainItemId)
    {
        if(!is_array($itemId))
            $itemId = [$itemId];
        foreach ($itemId as $item) {
            $log = Log::find()->where([
                'id' => $item,
            ])->one();
            if(!is_null($log)) {
                $log->group_id = $mainItemId;
                $log->save();
            }
        }
    }

    /**
     * @param \app\components\notifications\Notification      $notification
     * @param \app\components\notifications\contacts\Receiver $receiver
     * @param \app\models\Templates                           $template
     *
     * @param string                                          $type
     *
     * @return \app\models\LogsEmail
     * @throws \Exception
     */
    public static function saveEmailLog(Notification $notification, Receiver $receiver, Templates $template = null, $type = LogsEmail::TYPE_SYSTEM) {
        $log = new LogsEmail();
        $log->subject = $notification->getSubject();
        $log->text = $notification->getText();
        $log->html = $notification->getHtml();
        $log->type = $type;
        $log->recipient = $receiver->getEmail();
        $log->status = LogsEmail::STATUS_SENT;
        
        if ($template !== null)
            $log->template_id = $template->id;

        if(!$log->save())
            throw new \Exception('Can`t save log! '.json_encode($log->getErrors()));

        return $log;
    }
}