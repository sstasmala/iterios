<?php
/**
 * ContactsHelper.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;


use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\ContactsPhones;
use app\models\EmailsTypes;
use app\models\PhonesTypes;

class ContactsHelper
{
    /**
     * @param $data
     *
     * @return int|bool
     */
    public static function create(array $data)
    {
        $model = new Contacts();

        $data['tenant_id'] = \Yii::$app->user->identity->tenant->id;
        $data['type'] = Contacts::NEW_ITEM;
        $data['responsible'] = \Yii::$app->user->id;

        $model->setAttributes($data);
        $log_id = $model->save();

        if ($log_id) {
            if (!empty($data['email']))
                static::addEmail($model->id, $data['email'], $log_id);

            if (!empty($data['phone']))
                static::addPhone($model->id, $data['phone'], $log_id);

            return $model->id;
        }

        return false;
    }

    /**
     * @param $contact_id
     * @param $email
     * @param $log_id
     */
    private static function addEmail($contact_id, $email, $log_id)
    {
        $default_email_type = EmailsTypes::findOne(['default' => true]);

        if (null === $default_email_type)
            $default_email_type = EmailsTypes::find()->one();

        $model = new ContactsEmails();
        $model->contact_id = $contact_id;
        $model->type_id = (null !== $default_email_type ? $default_email_type->id : null);
        $model->value = $email;

        $email_log_id = $model->save();

        if ($email_log_id)
            LogHelper::setGroupId($email_log_id, $log_id);
    }

    /**
     * @param $contact_id
     * @param $phone
     * @param $log_id
     */
    private static function addPhone($contact_id, $phone, $log_id)
    {
        $default_phone_type = PhonesTypes::findOne(['default' => true]);

        if (null === $default_phone_type)
            $default_phone_type = PhonesTypes::find()->one();

        $model = new ContactsPhones();
        $model->contact_id = $contact_id;
        $model->type_id = (null !== $default_phone_type ? $default_phone_type->id : null);
        $model->value = $phone;

        $phone_log_id = $model->save();

        if ($phone_log_id)
            LogHelper::setGroupId($phone_log_id, $log_id);
    }
}