<?php

namespace app\helpers;


use app\components\twig\FinderExtension;

class PreviewShortcodesHelper
{
    /**
     * @param      $scheme
     * @param      $text
     * @param bool $strict
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function render($scheme, $text, $strict = false)
    {
        preg_match_all('/{{([^}{]+)}}/m', $text, $placeholders);

        foreach ($scheme as $key => $item) {
            $text = str_replace('{{' . $key . '}}','{{' . $key.'|finder("'.$key.'")|raw' . '}}', $text);
        }

        $loader = new \Twig_Loader_Array(['text' => $text]);
        $twig = new \Twig_Environment($loader, ['strict_variables' => $strict]);
        $filter = new FinderExtension();

        $twig->addExtension($filter);

        $placeholders = array_combine($placeholders[1], $placeholders[0]);
        $scheme = array_merge($placeholders, $scheme);

        return $twig->render('text', $scheme);
    }

    /**
     * @param      $scheme
     * @param      $text
     * @param bool $strict
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function renderCommercialProposals($scheme, $text, $strict = false)
    {
        preg_match_all('/{{([^}{]+)}}/m', $text, $placeholders);

        $text = preg_replace('/<span style="background-color:(#ff0)">/m', '<span>', $text);

        $loader = new \Twig_Loader_Array(['text' => $text]);
        $twig = new \Twig_Environment($loader, ['strict_variables' => $strict]);

        $placeholders = array_combine($placeholders[1], $placeholders[0]);
        $scheme = array_merge($placeholders, $scheme);

        return $twig->render('text', $scheme);
    }
}