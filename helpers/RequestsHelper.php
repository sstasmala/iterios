<?php
/**
 * RequestsHelper.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;

use app\models\Contacts;
use app\models\RequestCities;
use app\models\RequestCountries;
use app\models\RequestHotelCategories;
use app\models\Requests;
use app\models\RequestStatuses;
use yii\web\BadRequestHttpException;

class RequestsHelper
{
    /**
     * @param $data
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public static function create(array $data)
    {
        if (!empty($data['contact_id'])) {
            $contact = Contacts::find()
                ->where(['id' => $data['contact_id']])
                ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->one();

            if (null === $contact)
                throw new BadRequestHttpException('Contact not found');

            $contact_id = $contact->id;
        } else {
            $contact_id = ContactsHelper::create($data['Contacts']);
        }

        if (empty($contact_id))
            throw new BadRequestHttpException('contact_id is empty');

        $request_id = static::createRequest($data['Requests'], $contact_id);

        if (empty($request_id))
            throw new BadRequestHttpException('request_id is empty');

        if (!empty($data['Requests']['countries']))
            static::addCountries($request_id, $data['Requests']['countries']);

        if (!empty($data['Requests']['cities']))
            static::addCities($request_id, $data['Requests']['cities']);

        if (!empty($data['Requests']['hotel_category']))
            static::addHotelCategories($request_id, $data['Requests']['hotel_category']);

        return $request_id;
    }

    /**
     * @param $data
     * @param $contact_id
     *
     * @return bool
     */
    private static function createRequest($data, $contact_id)
    {
        $request = new Requests();
        $request->contact_id = $contact_id;
        $request->departure_date_start = !empty($data['departure_date_start']) ? static::convert_date($data['departure_date_start']) : '';
        $request->departure_date_end = !empty($data['departure_date_end']) ? static::convert_date($data['departure_date_end']) : '';
        $request->trip_duration_start = !empty($data['trip_days_min']) ? $data['trip_days_min'] : '';
        $request->trip_duration_end = !empty($data['trip_days_max']) ? $data['trip_days_max'] : '';
        $request->budget_currency = !empty($data['currency_id']) ? $data['currency_id'] : '';
        $request->budget_from = !empty($data['currency_value_min']) ? $data['currency_value_min'] : '';
        $request->budget_to = !empty($data['currency_value_max']) ? $data['currency_value_max'] : '';
        $request->tourists_adult_count = !empty($data['adults_person_count']) ? $data['adults_person_count'] : '';
        $request->tourists_children_count = !empty($data['children_count']) ? $data['children_count'] : '';
        $request->tourists_children_age = empty($data['children_age']) ? '' : json_encode($data['children_age']);
        $request->request_type_id = !empty($data['source']) ? $data['source'] : '';
        $request->request_description = !empty($data['description']) ? $data['description'] : '';

        $request->responsible_id = \Yii::$app->user->id;
        $request->tenant_id = \Yii::$app->user->identity->tenant->id;

        $status = RequestStatuses::findOne(['type' => RequestStatuses::TYPE_WORKING, 'is_default' => true]);

        if (is_null($status))
            $status = RequestStatuses::findOne(['type' => RequestStatuses::TYPE_WORKING]);

        $request->request_status_id = (!is_null($status) ? $status->id : '');

        return $request->save() ? $request->id : false;
    }

    /**
     * @param $data
     *
     * @return bool|int
     */
    // private static function addContact(array $data)
    // {
    //     $query = Contacts::find();
    //     $query->joinWith('contactsEmails')->joinWith('contactsPhones');
    //
    //     if (!empty($data['email'])) {
    //         $query->andWhere(['like', 'contacts_emails.value', $data['email']])
    //             ->orWhere(['like', 'contacts_emails.value', strtolower($data['email'])]);
    //     }
    //
    //     if (!empty($data['phone'])) {
    //         $query->andWhere(['like', 'contacts_phones.value', $data['phone']])
    //             ->orWhere(['like', 'contacts_phones.value', str_replace(['(', ')', '+', '-'], '', $data['phone'])]);
    //     }
    //
    //     $query->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
    //
    //     $contact = $query->one();
    //
    //     if (!empty($contact))
    //         return $contact->id;
    //
    //     return ContactsHelper::createContacts($data);
    // }

    /**
     * @param $req_id
     * @param $countries
     */
    public static function addCountries($req_id, $countries)
    {
        if (!is_array($countries))
            $countries = [$countries];

        foreach ($countries as $country) {
            $model = new RequestCountries();
            $model->request_id = $req_id;
            $model->country = $country;
            $model->save();
        }
    }

    /**
     * @param $req_id
     * @param $cities
     */
    public static function addCities($req_id, $cities)
    {
        if (!is_array($cities))
            $cities = [$cities];

        foreach ($cities as $city) {
            $model = new RequestCities();
            $model->request_id = $req_id;
            $model->city = $city;
            $model->save();
        }
    }

    public static function addHotelCategories($req_id, $hotels)
    {
        if (!is_array($hotels))
            $hotels = [$hotels];

        foreach ($hotels as $hotel) {
            $model = new RequestHotelCategories();
            $model->request_id = $req_id;
            $model->hotel_category = $hotel;
            $model->save();
        }
    }

    /**
     * @param $date
     *
     * @return false|int
     */
    private static function convert_date($date)
    {
        return strtotime($date);
    }
}