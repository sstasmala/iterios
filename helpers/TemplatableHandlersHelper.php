<?php
/**
 * TemplatableHandlersHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;

use app\components\base\EventEmitter;
use app\components\base\BaseTemplatableHandler;
use app\components\base\TemplatableHandlerInterface;

class TemplatableHandlersHelper
{
    /**
     * @param $event_emitter EventEmitter
     *
     * @return array
     */
    public static function getHandlers($event_emitter)
    {
        $ev_handlers = $event_emitter->getHandlers();
        $handlers = [];
        foreach ($ev_handlers as $h) {
            if(!class_exists($h))
                continue;
            $tmp = new $h();
            if($tmp instanceof TemplatableHandlerInterface)
                $handlers[] = $h;
        }
        return $handlers;
    }
}