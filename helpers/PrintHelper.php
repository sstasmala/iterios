<?php
/**
 * PrintHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


use yii\helpers\BaseConsole;

class PrintHelper
{
    const DEFAULT_TYPE = 0;
    const INFO_TYPE = 1;
    const ERROR_TYPE = 2;
    const WARNING_TYPE = 3;
    const SUCCESS_TYPE = 4;

    public static function printMessage($msg, $type = PrintHelper::DEFAULT_TYPE)
    {
        $date = new \DateTime();
        $p_msg = '['.$date->format('Y-m-d H:i:s').']: '.$msg . PHP_EOL;

        switch ($type) {
            case PrintHelper::DEFAULT_TYPE:
                $p_msg = '%n'.$p_msg;
                $p_msg = BaseConsole::renderColoredString($p_msg);
                break;
            case PrintHelper::INFO_TYPE:
                $p_msg = '%b'.$p_msg;
                $p_msg = BaseConsole::renderColoredString($p_msg);
                break;
            case PrintHelper::ERROR_TYPE:
                $p_msg = '%r'.$p_msg;
                $p_msg = BaseConsole::renderColoredString($p_msg);
                break;
            case PrintHelper::SUCCESS_TYPE:
                $p_msg = '%g'.$p_msg;
                $p_msg = BaseConsole::renderColoredString($p_msg);
                break;
            case PrintHelper::WARNING_TYPE:
                $p_msg = '%y'.$p_msg;
                $p_msg = BaseConsole::renderColoredString($p_msg);
                break;
        };

        echo $p_msg;
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}