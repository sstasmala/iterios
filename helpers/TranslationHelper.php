<?php
/**
 * TranslationHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


use app\models\UiTranslations;
use yii\web\HttpException;

class TranslationHelper
{
    const TRANSLATIONS_MAP = [
        'ita/orders/view' => ['ita/requests/view','ita/orders/index']
    ];


    public static function getPageTranslation($page,$lang = 'en')
    {
        if(\Yii::$app->hasProperty('session'))
            $session = \Yii::$app->session;
        else
            $session = null;
        $translations = [];
//        if(\Yii::$app->params['stage'] == 'prot')
//        {
//            $data = require \Yii::$app->basePath.'/data/translations.map.php';
//            if(!isset($data['pages'][$page]))
//                return [];
//            $module = $data['pages'][$page];
//            if(is_null($module))
//                throw new HttpException(500,'Translations for this page not found');
//
//            $general = $data['general'];
//            $translations_data = array_merge_recursive($general,$module);
//            foreach ($translations_data as $k => $t)
//            {
//                if(isset($t[$lang]))
//                    $translations[$k] = $t[$lang];
//                else
//                    $translations[$k] = '['.$k.']';
//            }
//        }
//        else
        if(!is_null($session) && $session->has('translations')) {
            $translations = $session->get('translations');
            $time = $session->get('translations_time',0);
            if((time()-$time)<10)
                $translations = [];
            if(isset($translations['pages:'.$page])) {
                $page_tr = $translations['pages:' . $page];
            }
            else {
                $page_tr = UiTranslations::getTranslationsByGroup('pages:'.$page)->translate($lang)->asArray()->all();
                $translations['pages:'.$page] = $page_tr;
                $map = static::TRANSLATIONS_MAP;
                if(isset($map[$page]))
                {
                    foreach ($map[$page] as $pg) {
                        $page_tr = UiTranslations::getTranslationsByGroup('pages:'.$pg)->translate($lang)->asArray()->all();
                        $translations['pages:'.$pg] = $page_tr;
                    }

                }

            }

            if(isset($translations['general'])) {
                $general = $translations['general'];
            }
            else {
                $general = UiTranslations::getTranslationsByGroup('general')->translate($lang)->asArray()->all();
                $translations['general'] = $general;
            }
            $translations_data = [];

            foreach ($translations as $tr)
            {
                $data = array_column($tr,'value','code');
                $translations_data = array_merge_recursive($translations_data,$data);
            }
//            $page_tr = array_column($page_tr,'value','code');
//            $general = array_column($general,'value','code');
//            $translations_data = array_merge_recursive($general,$page_tr);

            $session->set('translations',$translations);
            $session->set('translations_time',time());
            foreach ($translations_data as $k=>$v)
                if(is_null($v))
                    $translations_data[$k] = '['.$k.']';
            $translations = $translations_data;
            return $translations;
        }
        else {
            $module = UiTranslations::getTranslationsByGroup('pages:'.$page)->translate($lang)->asArray()->all();
            $general = UiTranslations::getTranslationsByGroup('general')->translate($lang)->asArray()->all();
            $module = array_column($module,'value','code');
            $general = array_column($general,'value','code');
            $translations_data = array_merge_recursive($general,$module);
            foreach ($translations_data as $k=>$v)
                if(is_null($v))
                    $translations_data[$k] = '['.$k.']';
            $translations = $translations_data;
            return $translations;
        }
    }

    public static function getTranslation($code, $lang = 'en', $default = null)
    {
        if(is_null($default))
            $default = '['.$code.']';
//        if(\Yii::$app->params['stage'] == 'prot')
//        {
//            $data = require \Yii::$app->basePath.'/data/translations.map.php';
//            $pages = $data['pages'];
//            foreach ($pages as $page)
//                foreach ($page as $k => $item)
//                {
//                    if($k == $code)
//                        if(isset($item[$lang]))
//                            return $item[$lang];
//                        else
//                            return $default;
//                }
//            $general = $data['general'];
//            foreach ($general as $k => $item)
//                if($k == $code)
//                    if(isset($item[$lang]))
//                        return $item[$lang];
//                    else
//                        return $default;
//            return $default;
//        }
//        else
        if(\Yii::$app->hasProperty('session'))// && YII_ENV_PROD)
        {
            $time = \Yii::$app->session->get('translations_time',0);
            if(\Yii::$app->session->has('translations') && (time()-$time)<10)
                $trans = \Yii::$app->session->get('translations');
            else
                $trans = [];
            $trs = [];
            foreach ($trans as $v) {
                if(!is_array($v))
                    $v = [];
                $trs = array_merge_recursive($trs, array_column($v, 'value', 'code'));
            }
            if(isset($trs[$code]) && (time()-$time)<10)
                return $trs[$code];
            else
            {
                $tr = UiTranslations::find()->where(['code'=>$code])->translate($lang)->one();
                if(is_null($tr) || is_null($tr->value))
                    return $default;
                $group = $tr->group;
                $module = UiTranslations::getTranslationsByGroup($group)->translate($lang)->asArray()->all();
                if(!empty($module)) {
                    $trans[$group] =  $module;
                    \Yii::$app->session->set('translations',$trans);
                }
                return $tr->value;
            }
        }
        $tr = UiTranslations::find()->where(['code'=>$code])->translate($lang)->one();
        if(is_null($tr) || is_null($tr->value))
            return $default;

        return $tr->value;

    }

    public static function getGroups()
    {
        $groups = ['general'];
        $data = require \Yii::$app->basePath.'/data/translations.map.php';
        $pages = $data['pages'];
        foreach ($pages as $k=>$page)
            $groups[] = 'pages:'.$k;
        return $groups;
    }
}