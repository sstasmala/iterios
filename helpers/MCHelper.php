<?php
/**
 * MCHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


use app\models\Settings;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use yii\web\HttpException;

/**
 * Class MCHelper
 * @package app\helpers
 *
 * @method searchCountry($search,$lang) static
 * @method searchCity($search,$lang,$country = null) static
 * @method searchDepartureCity($search,$lang) static
 * @method searchHotelCategory($search,$lang) static
 * @method getHotelCategoryById($id, $lang) static
 * @method getCountryById($id,$lang) static
 * @method getCityById($id, $lang) static
 * @method getDepartureCityById($id, $lang) static
 * @method getCurrency($lang = 'en') static
 * @method getCurrencyById($id,$lang) static
 * @method getAirportsById($id,$lang) static
 * @method getAirports($search = '', $lang = 'en') static
 * @method getFoodOptions($search = '', $lang = 'en') static
 * @method getFoodOptionsById($id,$lang) static
 * @method getHotelsOptions($search = '', $lang = 'en',$country = null) static
 * @method getHotelsOptionsById($id,$lang) static
 */
class MCHelper
{

    /**
     * @param $name
     * @param $arguments
     * @return array
     */
    private static function prepareQuery($name,$arguments)
    {
        $query = [];
        if(isset($arguments[0]))
            $query['search'] = $arguments[0];
        if(isset($arguments[1]))
            $query['lang'] = $arguments[1];
        switch ($name)
        {
            case 'getFoodOptionsById':
            case 'getAirportsById':
            case 'getCurrencyById':
            case 'getDepartureCityById':
            case 'getCityById':
            case 'getCountryById':
            case 'getHotelCategoryById':
            case 'getHotelsOptionsById':
                unset($query['search']);
                if(is_array($arguments[0]) && !empty($arguments[0])) {
                    $arguments[0] = implode(',', $arguments[0]);
                    $query['id'] = $arguments[0];
                }
                else
                {
                    if(!empty($arguments[0]))
                        $query['id'] = $arguments[0];
                    else
                        if(is_null( $arguments[0]))
                            $query['id'] = $arguments[0];
                        else
                            return [];
                }
                break;
            case 'searchCity':
                if(isset($arguments[2]))
                    $query['country'] = $arguments[2];
                break;
            case 'getCurrency':
                unset($query['search']);
                if(isset($arguments[0]))
                    $query['lang'] = $arguments[0];
                else
                    $query['lang'] = 'en';
                break;
            case 'getHotelsOptions':
                if(isset($arguments[2]) && $arguments[2] !== null)
                    $query['country'] = $arguments[2];
                break;
        }
        return $query;
    }

    /**
     * @param $name
     * @param $arguments
     * @return array
     * @throws HttpException
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        $url = null;
        $query = self::prepareQuery($name,$arguments);
        if(count($query) == 0)
            return [];
        switch ($name)
        {
            case 'getHotelsOptionsById' :if(!isset($query['id'])) return [];
            case 'getHotelsOptions' : $url = 'reference/hotel';break;

            case 'getCountryById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'searchCountry':$url = 'reference/country';break;

            case 'getCityById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'searchCity':$url = 'reference/city';break;

            case 'getDepartureCityById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'searchDepartureCity':$url = 'reference/depCity';break;


            case 'getHotelCategoryById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'searchHotelCategory':$url = 'reference/hotel-category';break;

            case 'getCurrencyById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'getCurrency':$url = 'reference/currency';break;

            case 'getAirportsById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'getAirports':$url = 'reference/airport';break;

            case 'getFoodOptionsById':if(!isset($query['id']) && !is_null($query['id'])) return [];
            case 'getFoodOptions':$url = 'reference/meal';break;
        }


        $key = serialize($query);

        //TODO Caching MC results ???
//        if(\Yii::$app->hasProperty('session'))
//        {
//            $session = \Yii::$app->getSession();
//            $data = [];
//            if($session->has('mc_cache'))
//                $data = $session->get('mc_cache');
//
//            if(isset($data[$name]))
//                if(isset($data[$name][$key]) && $data[$name][$key]['time']>(time()-60*60*24*7))
//                    return $data[$name][$key]['data'];
//        }

        if(is_null($url))
            return [];
        $client = new Client([
            'base_uri'=>Settings::getByKey(Settings::MCHELPER_BASE_URI)
        ]);

        try
        {
            $response = $client->request('GET',$url,[
                'headers' => ['X-Iterios-Api-Key' => Settings::getByKey(Settings::MCHELPER_KEY)],
                'query' => $query,
            ]);

            $response = \GuzzleHttp\json_decode($response->getBody()->getContents(),true);
            if($response['status'] == 'success')
            {
                //TODO Caching MC results ???
//                if(\Yii::$app->hasProperty('session')) {
//                    $session = \Yii::$app->getSession();
//                    $data = [];
//                    if ($session->has('mc_cache'))
//                        $data = $session->get('mc_cache');
//                    $data[$name][$key]['data'] = $response['data'];
//                    $data[$name][$key]['time'] = time();
//                    $session->set('mc_cache',$data);
//                }
                return $response['data'];
            }
            else
                throw new \Exception($response['error']['message']);
        }
        catch (RequestException $e)
        {
            if(YII_ENV_DEV)
                return [];
            else
                throw new HttpException(500,$e->getMessage());
        }

    }
}