<?php
/**
 * SmsHelper.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;

use app\components\mailers_sms\MailerSmsProviderException;
use app\components\mailers_sms\smsclub\SmsMailer;
use app\models\Jobs;
use app\models\LogsSms;
use app\models\SmsAlphaNames;
use app\models\SmsCountries;
use app\models\SmsSystemProviders;

class SmsHelper
{
    /**
     * @param      $phone_number
     * @param      $text
     * @param null $alpha_name_type
     *
     * @param null $tenant_id
     *
     * @return bool
     */
    public static function send($phone_number, $text, $alpha_name_type = null, $tenant_id = null)
    {
        $log = new LogsSms();
        $log->text = $text;
        $log->recipient = $phone_number;
        $log->alpha_name_type = $alpha_name_type;

        try {
            $country = self::getPhoneCountry($phone_number);

            if (empty($country['provider_id']))
                throw new \RuntimeException('The provider is not affiliated for this country ('.$country['name'].').');

            $log->country_id = $country['id'];

            $provider = SmsSystemProviders::findOne($country['provider_id']);

            if (null === $provider)
                throw new \RuntimeException('Provider does not exist for this country ('.$country['name'].').');

            $log->provider_id = $provider->id;

            if ($alpha_name_type !== SmsSystemProviders::ANAME_PERSONAL_TYPE) {
                $from = (null === $alpha_name_type || $alpha_name_type === SmsSystemProviders::ANAME_SYSTEM_TYPE ? $provider->system_aname : $provider->public_aname);
            } else {
                $alpha_name = SmsAlphaNames::findOne(['tenant_id' => $tenant_id, 'provider_id' => $provider->id]);

                if (null === $alpha_name)
                    throw new \RuntimeException('Alpha name does not exist for this tenant (ID #'.$tenant_id.') and provider ('.$provider->name.').');

                $log->alpha_name_id = $alpha_name->id;

                $from = $alpha_name['value'];
            }

            $mailer = new SmsMailer(json_decode($provider->credentials_config, true));

            $message = $mailer->compose();
            $message->setText($text)
                ->setFrom($from)
                ->setTo($phone_number);

            $sms_id = $message->send();

            $log->status = LogsSms::STATUS_SENT;
            $log->sms_id = $sms_id;
            $log->save();

            if (null === Jobs::findOne(['executor' => 'app\components\executors\UpdateSmsStatuses'])) {
                $job = new Jobs();
                $job->executor = 'app\components\executors\UpdateSmsStatuses';
                $job->save();
            }
        } catch (\Exception $e) {
            $log->status = LogsSms::STATUS_ERROR;
            $log->error_text = $e->getMessage();
            $log->save();

            return $e->getMessage();
        }

        return true;
    }

    /**
     * @param $phone_number
     *
     * @return bool|string
     */
    private static function getPhoneCountry($phone_number)
    {
        $countries = SmsCountries::find()->asArray()->all();

        $phone_number = str_replace('+', '', $phone_number);

        $country = array_filter($countries, function ($country) use ($phone_number) {
            return 0 === strpos($phone_number, $country['dial_code']);
        });

        return array_shift($country);
    }
}