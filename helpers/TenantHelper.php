<?php
/**
 * helpers/TenantHelper.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\helpers;

use app\models\BaseNotifications;
use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\ContactsMessengers;
use app\models\ContactsPhones;
use app\models\DemoData;
use app\models\Languages;
use app\models\TenantNotifications;
use app\models\Tenants;
use app\models\User;
use Yii;

class TenantHelper
{
    /**
     * @param \app\models\User $owner
     *
     * @param                  $name
     * @param                  $lang_iso
     *
     * @return bool
     * @throws \Exception
     */
    public static function createTenant(User $owner, $name, $lang_iso)
    {
        if (!Yii::$app->user->identity)
            Yii::$app->user->setIdentity($owner);

        $post = \Yii::$app->request->post();
        $tenant = new Tenants();
        $tenant->name = $name;
        $tenant->language_id = Languages::getIdFromIso($lang_iso);
        $tenant->owner_id = $owner->id;
        $tenant->country = $post['country'];

        if ($tenant->save()) {
            $tenant->link('users', $owner);
            self::setDemoData($tenant->id);
            //self::setBasteNotifications($tenant->id);

            return $tenant->id;
        }

        return false;
    }

    /**
     * @param $tenant_id
     */
    private static function setDemoData($tenant_id)
    {
        $model = DemoData::find()->where(['type' => DemoData::TYPE_CONTACT])->asArray()->all();

        foreach ($model as $demo_data) {
            $data = json_decode($demo_data['data'], true);

            $contact = new Contacts();
            $contact->load(['Contacts' => $data]);
            $contact->tenant_id = $tenant_id;

            if ($contact->save()) {
                if (isset($data['ContactsEmails']))
                    self::setDemoContacts(new ContactsEmails(), $contact->id, $data['ContactsEmails']);

                if (isset($data['ContactsPhones']))
                    self::setDemoContacts(new ContactsPhones(), $contact->id, $data['ContactsPhones']);

                if (isset($data['ContactsMessengers']))
                    self::setDemoContacts(new ContactsMessengers(), $contact->id, $data['ContactsMessengers']);
            }
        }
    }

    /**
     * @param \yii\db\ActiveRecord $model
     * @param $contact_id
     * @param $contact_data
     */
    private static function setDemoContacts(\yii\db\ActiveRecord $model, $contact_id, $contact_data)
    {
        foreach ($contact_data as $data) {
            $model->type_id = $data['type_id'];
            $model->value = $data['value'];
            $model->contact_id = $contact_id;
            $model->save();
        }
    }

    /**
     * @param Tenants $model
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public static function setTenantRbac($model)
    {
        $permissions = self::getPermissionsScheme();

        foreach ($permissions as $group)
        {
            foreach($group as $module)
            {
                $mod_name = $module['permission_name'];
                foreach ($module as $act => $pr)
                    if(is_array($pr))
                        foreach ($pr as $permission)
                        {
                            $permission_name = $model->id.'.'.$mod_name.'.'.$act.'.'.$permission['value'];
                            $perm = RbacHelper::getPermission($permission_name);
                            if(is_null($perm))
                            {
                                RbacHelper::createPermission($permission_name);
                            }
                            if(isset($permission['checked']) && $permission['checked'])
                            {
                                if(!RbacHelper::isAssigned($admin_role,$permission_name))
                                    RbacHelper::assignPermission($admin_role,$permission_name);
                            }
                            else{
                                if(RbacHelper::isAssigned($admin_role,$permission_name))
                                    RbacHelper::unassignPermission($admin_role,$permission_name);
                            }
                        }
            }
        }
    }

    public static function getPermissionsScheme($lang = null)
    {
        if(is_null($lang))
            $lang = Languages::getDefault()->iso;
        $scheme = [
            TranslationHelper::getTranslation('create_roles_table_modules', $lang, 'Module') => [
                TranslationHelper::getTranslation('create_roles_table_tasks', $lang, 'Tasks') => [
                    'permission_name' => 'tasks',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                ],
                TranslationHelper::getTranslation('create_roles_table_tour_search', $lang, 'Tour search') => [
                    'permission_name' => 'tour_search',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ]
                ],
                TranslationHelper::getTranslation('create_roles_table_requests', $lang, 'Requests') => [
                    'permission_name' => 'requests',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                ]
            ],
            TranslationHelper::getTranslation('create_roles_table_sales', $lang, 'Sales') => [
                TranslationHelper::getTranslation('create_roles_table_sales_overviews', $lang, 'Sales overviews') => [
                    'permission_name' => 'sales_overviews',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ]
                ],
                TranslationHelper::getTranslation('create_roles_table_deals', $lang, 'Deals') => [
                    'permission_name' => 'deals',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ]
                ],
                TranslationHelper::getTranslation('create_roles_table_reservations', $lang, 'Reservations') => [
                    'permission_name' => 'reservations',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ]
                ],
            ],
            TranslationHelper::getTranslation('create_roles_table_contractors', $lang, 'Contractors') => [
                TranslationHelper::getTranslation('create_roles_table_contacts', $lang, 'Contacts') => [
                    'permission_name' => 'contacts',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ]
                ],
                TranslationHelper::getTranslation('create_roles_table_companies', $lang, 'Companies') => [
                    'permission_name' => 'companies',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'create' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'update' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ]
                    ],
                    'delete' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_full_access', $lang, 'Full access'),
                            'value' => 'full',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_own', $lang, 'Only own'),
                            'value' => 'only_own',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_only_my_role', $lang, 'Only my role'),
                            'value' => 'only_own_role',
                            'color_scheme' => 'warning'
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ]
                ]
            ],
            TranslationHelper::getTranslation('create_roles_table_marketing', $lang, 'Marketing') => [
                TranslationHelper::getTranslation('create_roles_table_mailing', $lang, 'Mailing') => [
                    'permission_name' => 'mailing',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ],
                TranslationHelper::getTranslation('create_roles_table_segments', $lang, 'Segments') => [
                    'permission_name' => 'segments',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ],
                TranslationHelper::getTranslation('create_roles_table_reminders', $lang, 'Reminders') => [
                    'permission_name' => 'reminders',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ]
            ],
            TranslationHelper::getTranslation('create_roles_table_reports', $lang, 'Reports') => [
                TranslationHelper::getTranslation('create_roles_table_reports', $lang, 'Reports') => [
                    'permission_name' => 'reports',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ]
            ],
            TranslationHelper::getTranslation('create_roles_table_settings', $lang, 'Settings') => [
                TranslationHelper::getTranslation('create_roles_table_settings', $lang, 'Settings') => [
                    'permission_name' => 'settings',
                    'modules' => [
                        TranslationHelper::getTranslation('account', $lang, 'Account'),
                        TranslationHelper::getTranslation('roles_page_title', $lang, 'Roles'),
                        TranslationHelper::getTranslation('create_roles_page_integration', $lang, 'Integration'),
                        TranslationHelper::getTranslation('create_roles_page_widgets', $lang, 'Widgets'),
                    ],
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ],
                TranslationHelper::getTranslation('create_roles_table_users', $lang, 'Users') => [
                    'permission_name' => 'users',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ],
                TranslationHelper::getTranslation('create_roles_table_constructors', $lang, 'Constructors') => [
                    'permission_name' => 'constructors',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ],
                TranslationHelper::getTranslation('create_roles_table_accounts_and_payments', $lang, 'Accounts and payments') => [
                    'permission_name' => 'accounts_and_payments',
                    'read' => [
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_allowed', $lang, 'Allowed'),
                            'value' => 'access',
                            'color_scheme' => 'success',
                            'checked' => true
                        ],
                        [
                            'title' => TranslationHelper::getTranslation('create_roles_table_var_forbidden', $lang, 'Forbidden'),
                            'value' => 'forbidden',
                            'color_scheme' => 'danger'
                        ]
                    ],
                    'create' => [],
                    'update' => [],
                    'delete' => [],
                ]
            ]
        ];
        return $scheme;
    }

    public static function setBasteNotifications($id)
    {
        $id = intval($id);
        $tenant = Tenants::findOne(['id'=>$id]);
        if(is_null($tenant))
            return;
        $base  = BaseNotifications::find()->all();

        $tenant = TenantNotifications::find()->where('base_notification_id IS NOT NULL')->andWhere(['tenant_id'=>$id])->asArray()->all();
        $ids =array_combine(array_column($tenant,'id'), array_column($tenant,'base_notification_id'));
        /**
         * @var $b BaseNotifications
         */
        foreach ( $base as $k=>$b ) {
            $exist = in_array($b->id, $ids);
            if ($exist === false)
                $model = new TenantNotifications();
            else
                $model = TenantNotifications::findOne(['id' => $k]);

            $model->base_notification_id = $b->id;
            $model->repeat = true;
            $model->is_email = $b->is_email;
            $model->is_system_notification = $b->is_system_notification;
            $model->is_task = $b->is_task;
            $model->tenant_id = $id;
            $model->save();
        }
    }
}