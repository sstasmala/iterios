<?php
/**
 * BuilderHelper.php
 * @copyright ©iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


class BuilderHelper
{
    public static function unformat($item,$adm = false)
    {
        if (empty($item))
            return $item;

        foreach ($item as $k=>$v)
        {
            if($k == 'rules')
                $item[$k]= self::unformat($v,$adm);

            if(isset($v['id']) && ($v['id'] == 'contacts.created_at')) {
                if(is_array($v['value'])){
                    foreach ($v['value'] as $i=>$it)
                        if($adm)
                            $v['value'][$i] = DateTimeHelper::convertDateToTimestamp($it,'d.m.Y', date_default_timezone_get());
                        else
                            $v['value'][$i] = DateTimeHelper::convertDateToTimestamp($it);
                }
                else
                    if($adm)
                        $v['value'] = DateTimeHelper::convertDateToTimestamp($v['value'],'d.m.Y', date_default_timezone_get());
                    else
                        $v['value'] = DateTimeHelper::convertDateToTimestamp($v['value']);
                $item[$k] = $v;
            }
        }
        return $item;
    }

    public static function format($item,$adm = false)
    {   
        if (empty($item)) return false;
        
        foreach ($item as $k=>$v)
        {
            if($k == 'rules')
                $item[$k]= self::format($v,$adm);

            if(isset($v['id']) && ($v['id'] == 'contacts.created_at')) {
                if(is_array($v['value'])){
                    foreach ($v['value'] as $i=>$it)
                        if($adm)
                            $v['value'][$i] = DateTimeHelper::convertTimestampToDate($it,'d.m.Y', date_default_timezone_get());
                        else
                            $v['value'][$i] = DateTimeHelper::convertTimestampToDate($it);
                }
                else
                    if($adm)
                        $v['value'] = DateTimeHelper::convertTimestampToDate($v['value'],'d.m.Y', date_default_timezone_get());
                    else
                        $v['value'] = DateTimeHelper::convertTimestampToDate($v['value']);
                $item[$k] = $v;
            }
        }
        return $item;
    }

    public static function formatConfig($config,$adm = false)
    {
        $cnf = json_decode($config,true);
        $cnf = self::format($cnf,$adm);
        return json_encode($cnf);
    }

    public static function unformatConfig($config,$adm = false)
    {
        $cnf = json_decode($config,true);
        $cnf = self::unformat($cnf,$adm);
        return json_encode($cnf);
    }

    public static function filterConditionRules($condition, $post) {
        $rules_count = 0;
        foreach($post as $f => $v)
            if (strpos($f, 'builder_rule_') !== false)
                $rules_count++;

        return $rules_count ? $condition : '';
    }
}