<?php
/**
 * DateTimeHelper.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\helpers;


class DateTimeHelper
{

    const DEFAULT_FORMAT = 'd.m.Y';
    const DEFAULT_TIME_FORMAT = 'H:i:s';
    const DEFAULT_TIMEZONE = 'UTC';

    public static function convertFormat($format)
    {
        $chars = str_split($format);
        $result = '';
        foreach ($chars as $k=>$v)
        {
            switch ($v)
            {
                case 'd': $result.='DD'; break;
                case 'D': $result.='ddd'; break;
                case 'j': ($chars[$k+1]=='S')?$result.='D':$result.='M'; break;
                case 'l': $result.='dddd'; break;
                case 'N': $result.='E'; break;
                case 'S': $result.='o'; break;
                case 'w': $result.='e'; break;
                case 'z': $result.='DDD'; break;
                case 'W': $result.='GG'; break;
                case 'F': $result.='MMMM'; break;
                case 'm': $result.='MM'; break;
                case 'M': $result.='MMM'; break;
                case 'n': $result.='M'; break;
                case 't': $result.=''; break;//case 't': $result.='DD'; break;
                case 'L': $result.=''; break;//case 'L': $result.='GG'; break;
                case 'o': $result.='YYYY'; break;
                case 'Y': $result.='YYYY'; break;
                case 'y': $result.='YY'; break;
                case 'a': $result.='a'; break;
                case 'A': $result.='A'; break;
                case 'B': $result.='SSS'; break;
                case 'g': $result.='h'; break;
                case 'G': $result.='H'; break;
                case 'h': $result.='hh'; break;
                case 'H': $result.='HH'; break;
                case 'i': $result.='mm'; break;
                case 's': $result.='ss'; break;
                case 'u': $result.=''; break;
                case 'v': $result.=''; break;
                case 'e': $result.=''; break;
                case 'I': $result.=''; break;
                case 'O': $result.=''; break;
                case 'P': $result.=''; break;
                case 'T': $result.=''; break;
                case 'Z': $result.=''; break;
                case 'c': $result.=''; break;
                case 'r': $result.=''; break;
                case 'U': $result.=''; break;
                default: $result.=$v;
            }
        }
        return $result;

    }
    public static function convertFormat2($format)
    {
        $chars = str_split($format);
        $result = '';
        foreach ($chars as $k=>$v)
        {
            switch ($v)
            {
                case 'd': $result.='dd'; break;
                case 'D': $result.='D'; break;
                case 'j': $result.='d'; break;
                case 'l': $result.=''; break;
                case 'N': $result.=''; break;
                case 'S': $result.=''; break;
                case 'w': $result.=''; break;
                case 'z': $result.='o'; break;
                case 'W': $result.=''; break;
                case 'F': $result.='MM'; break;
                case 'm': $result.='mm'; break;
                case 'M': $result.='M'; break;
                case 'n': $result.='m'; break;
                case 't': $result.=''; break;//case 't': $result.='DD'; break;
                case 'L': $result.=''; break;//case 'L': $result.='GG'; break;
                case 'o': $result.='yyyy'; break;
                case 'Y': $result.='yyyy'; break;
                case 'y': $result.='y'; break;
                case 'a': $result.=''; break;
                case 'A': $result.=''; break;
                case 'B': $result.=''; break;
                case 'g': $result.=''; break;
                case 'G': $result.=''; break;
                case 'h': $result.=''; break;
                case 'H': $result.=''; break;
                case 'i': $result.=''; break;
                case 's': $result.=''; break;
                case 'u': $result.=''; break;
                case 'v': $result.=''; break;
                case 'e': $result.=''; break;
                case 'I': $result.=''; break;
                case 'O': $result.=''; break;
                case 'P': $result.=''; break;
                case 'T': $result.=''; break;
                case 'Z': $result.=''; break;
                case 'c': $result.=''; break;
                case 'r': $result.=''; break;
                case 'U': $result.=''; break;
                default: $result.=$v;
            }
        }
        return $result;

    }


    public static function convertTimestampToDate($timestamp,$format=null,$timezone=null)
    {
        if(is_null($timestamp))
            return null;
        if(is_null($format))
            $format = \Yii::$app->user->identity->tenant->date_format;

        if(is_null($format))
            $format = static::DEFAULT_FORMAT;

        if(is_null($timestamp) || empty($timestamp))
            return $timestamp;
        $date = static::createFromTimestamp($timestamp,$timezone);
        return $date->format($format);
    }

    public static function convertTimestampToDateTime($timestamp,$format=null,$timezone=null)
    {
        if(is_null($timestamp))
            return null;
        if(is_null($format))
            $format = \Yii::$app->user->identity->tenant->date_format.' '.\Yii::$app->user->identity->tenant->time_format;

        if(is_null($format))
            $format = static::DEFAULT_FORMAT.' '.static::DEFAULT_TIME_FORMAT;


        $date = static::createFromTimestamp($timestamp,$timezone);
        return $date->format($format);
    }

    public static function convertDateToTimestamp($date,$format=null,$timezone=null)
    {
        if(is_null($date) || empty($date))
            return null;
        $date = static::createFromDate($date,$format,$timezone);
        return $date->getTimestamp();
    }

    public static function convertDateTimeToTimestamp($date,$format=null,$timezone=null)
    {
        if(is_null($date) || empty($date))
            return null;
        $date = static::createFromDateTime($date,$format,$timezone);
        return $date->getTimestamp();
    }

    public static function createFromDate($date,$format=null,$timezone=null)
    {
        if(is_null($date) || empty($date))
            return null;
        if(is_null($format))
            $format = \Yii::$app->user->identity->tenant->date_format;

        if(is_null($timezone))
            $timezone = \Yii::$app->user->identity->tenant->timezone;

        if(is_null($format))
            $format = static::DEFAULT_FORMAT;

        if(is_null($timezone))
            $timezone = static::DEFAULT_TIMEZONE;

        $date = \DateTime::createFromFormat($format,$date,new \DateTimeZone($timezone));
        $date = $date->setTime(12,0,0);
        return $date;
    }

    public static function createFromDateTime($date,$format=null,$timezone=null)
    {
        if(is_null($date) || empty($date))
            return null;
        if(is_null($format))
            $format = \Yii::$app->user->identity->tenant->date_format.' '.\Yii::$app->user->identity->tenant->time_format;

        if(is_null($timezone))
            $timezone = \Yii::$app->user->identity->tenant->timezone;

        if(is_null($format))
            $format = static::DEFAULT_FORMAT.' '.static::DEFAULT_TIME_FORMAT;

        if(is_null($timezone))
            $timezone = static::DEFAULT_TIMEZONE;

        $date = \DateTime::createFromFormat($format,$date,new \DateTimeZone($timezone));
        return $date;
    }

    public static function createFromTimestamp($timestamp,$timezone=null)
    {
        if(is_null($timestamp))
            return null;
        if(is_null($timezone))
            $timezone = \Yii::$app->user->identity->tenant->timezone;
        if(is_null($timezone))
            $timezone = static::DEFAULT_TIMEZONE;
        $date = new \DateTime();
        $date->setTimestamp($timestamp);
        $date->setTimezone(new \DateTimeZone($timezone));
        return $date;
    }

}