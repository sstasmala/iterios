/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
var translated_models = {};

$(document).ready(function () {
    $('#lang-list').select2({
        ajax:{
            url: baseUrl + translatable_controller_path + '/get-languages',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['iso'], name: data[i]['name'], original_name: data[i]['original_name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose language',
        allowClear: false,
        width: '50%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });

    $('select#lang-list[data-action="create"]').change(function() {
        switchFormLanguage(translatable_attributes, $(this).val(), default_language, translatable_model_name, null);
    });

    $('select#lang-list[data-action="update"]').change(function() {
        var new_language = $(this).val();
        if (translatable_model_id && !translated_models[new_language]) {
            $.ajax({
                url: baseUrl + translatable_controller_path + '/get-translate?id=' + translatable_model_id + '&lang=' + new_language,
                dataType: 'json',
                data: {},
                success: function (data) {
                    translated_models[new_language] = data;
                    switchFormLanguage(translatable_attributes, new_language, default_language, translatable_model_name, data);
                }
            });
        } else {
            switchFormLanguage(translatable_attributes, new_language, default_language, translatable_model_name, null);
        }
    });

    $('select#lang-list[data-action="view"]').add('select#lang-list[data-action="index"]').change(function() {
        var params = (new URL(document.location)).searchParams;
        params.set('lang', $(this).val());
        location.search = params.toString();
    });

    // mark translatable fields by changing their names
    translatable_attributes.forEach(function(attribute_name) {
        var field_name_without_lang = translatable_model_name + '[' + attribute_name + ']';
        var field_name_with_default_lang = field_name_without_lang + '[' + default_language + ']';
        var field = $('[name^="' + field_name_without_lang + '"]');
        if ( field.length ) {
            field.attr('name', field_name_with_default_lang)
                .siblings('label').after(' <small>(translatable)</small>');
        }
    });
});

function switchFormLanguage (translatable_attributes, new_language, default_language, translatable_model_name, data) {
    translatable_attributes.forEach(function(attribute_name) {
        var field_name_without_lang = translatable_model_name + '[' + attribute_name + ']';
        var field_name_with_default_lang = field_name_without_lang + '[' + default_language + ']';
        var field_name_with_selected_lang = field_name_without_lang + '[' + new_language + ']';

        $('[name^="' + field_name_without_lang + '"]').hide();
        $('[name^="' + field_name_without_lang + '"]').closest('div').find('div.cke').hide();

        if ( $(buildSelectorByName(field_name_with_selected_lang)).length ) {
            if (typeof CKEDITOR !== "undefined" && CKEDITOR.instances[$(buildSelectorByName(field_name_with_selected_lang)).attr('id')] !== undefined) {
                $('div#cke_'+$(buildSelectorByName(field_name_with_selected_lang)).attr('id')).show();
            } else {
                $(buildSelectorByName(field_name_with_selected_lang)).show();
            }
        } else {
            var old_element = $(buildSelectorByName(field_name_with_default_lang));
            var new_element,
                new_id = old_element.attr('id') + '_' + new_language;

            if (typeof CKEDITOR !== "undefined" && CKEDITOR.instances[old_element.attr('id')] === undefined) {
                new_element = old_element.clone().attr('name', field_name_with_selected_lang).val(null).text(null).show();
            } else {
                new_element = old_element.clone().attr('id', new_id).attr('name', field_name_with_selected_lang).val(null).text(null).show();
            }

            old_element.after(new_element);

            if (data) {
                new_element.val(data[attribute_name]);
            }

            if (typeof CKEDITOR !== "undefined" && CKEDITOR.instances[old_element.attr('id')] !== undefined) {
                var config = CKEDITOR.instances[old_element.attr('id')].config;

                CKEDITOR.replace(new_id, config);
            }
        }
    });
}

function buildSelectorByName(name) {
    return '[name="' + name + '"]';
}

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    markup += '<div style="font-size: small">Original: '+perm.original_name+' Code: <b>'+perm.id+'</b></div>';
    return markup;
}

function formatPermSel (perm) {
    if (perm.name == undefined)
        return perm.text;

    return perm.name + '('+perm.original_name+')';
}