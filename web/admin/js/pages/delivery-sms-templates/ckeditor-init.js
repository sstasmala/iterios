var config = {
    // toolbar: [
    //     {name: 'document', items: ['Print','mode']},
    //     {name: 'clipboard', items: ['Undo', 'Redo']},
    //     {name: 'styles', items: ['Format', 'Font', 'FontSize']},
    //     {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
    //     {name: 'colors', items: ['TextColor', 'BGColor']},
    //     {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
    //     {name: 'links', items: ['Link', 'Unlink']},
    //     {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
    //     {name: 'insert', items: ['Image', 'Table']},
    //     {name: 'tools', items: ['Maximize']},
    //     {name: 'editing', items: ['Scayt']},
    //     {name: 'placeholder', items: ['placeholder_select']},
    //     // {name: 'sourcedialog', items: ['source']},
    // ],
    customConfig: '',
    disallowedContent: 'img{width,height,float}',
    extraAllowedContent: 'img[width,height,align]',
    extraPlugins: 'placeholder_select',
    height: 800,
    contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css'],
    bodyClass: 'document-editor',
    format_tags: 'p;h1;h2;h3;pre',
    removeDialogTabs: 'image:advanced;link:advanced',
    stylesSet: [
        {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
        {name: 'Cited Work', element: 'cite'},
        {name: 'Inline Quotation', element: 'q'},
        {
            name: 'Special Container',
            element: 'div',
            styles: {
                padding: '5px 10px',
                background: '#eee',
                border: '1px solid #ccc'
            }
        },
        {
            name: 'Compact table',
            element: 'table',
            attributes: {
                cellpadding: '5',
                cellspacing: '0',
                border: '1',
                bordercolor: '#ccc'
            },
            styles: {
                'border-collapse': 'collapse'
            }
        },
        {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
        {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
    ]
};
config.toolbarGroups = [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'forms', groups: [ 'forms' ] },
    // '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'insert', groups: [ 'insert' ] },
    // '/',
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },
    { name: 'about', groups: [ 'about' ] }
];

config.removeButtons = 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak';


function set_placeholders(type) {
    $.ajax({
        url: baseUrl + '/ia/delivery-sms-templates-system/get-placeholders',
        dataType: 'json',
        data: {
            type: type,
        },
        success: function (data) {
            config.placeholder_select = {
                placeholders: data,
                format: '<span style="background-color:#ff0;">{{%placeholder%}}</span>'
            };
            config.extraPlugins = 'sourcedialog';
            config.removePlugins = 'sourcearea';

            CKEDITOR.replace('deliverysmstemplates-body', config);

        },
        error: function () {
            console.log('Error!');
        }
    });
}

$('select#deliverysmstemplates-delivery_type_id').change(function() {
    $.each(CKEDITOR.instances, function(ins_name, instance) {
        instance.destroy();
    });

    var type = $(this).val();
    set_placeholders(type);
});

$('document').ready(function () {
    var type = $('select#deliverysmstemplates-delivery_type_id').val();

    set_placeholders(type.length > 0 ? type : '');
});