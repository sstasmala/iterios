$('document').ready(function () {
    var table_hidden = $('.delivery-placeholders-index');
    if($('select#deliverysmstemplates-delivery_type_id').val() === "") {
        table_hidden.hide();
    } else {
        table_hidden.show();
    }

    $('select#deliverysmstemplates-delivery_type_id').change(function() {
        var table_hidden = $('.delivery-placeholders-index');
        var template_id = $('.delivery-sms-templates-form').data('template_id');

        if($('select#deliverysmstemplates-delivery_type_id').val() === "") {
            table_hidden.hide();
        } else {
            table_hidden.show();
        }
        var type = $(this).val();
        $.pjax({url: ($(template_id).length > 0 ? 'update?id='+template_id+'&' : 'create?') + 'Placeholders[delivery_type]='+type, container: '#dynagrid-placeholders-pjax'});

    });

    $('body').on('click', '.copy_button', function () {
        var pl_id = $(this).parent('a').data('pl_id');
        var root = document.getElementById('placeholder_'+pl_id);
        var range = document.createRange();
        range.setStart(root, 0);
        range.setEnd(root, 1);
        window.getSelection().empty();
        window.getSelection().addRange(range);
        try {
            var successful = document.execCommand('copy');
            window.getSelection().empty();
        } catch (err) {
            alert(err);
        }
    });

});