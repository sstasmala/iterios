$(document).ready(function () {
    function getFields() {
        $('#placeholders-field').select2({
            ajax:{
                url: baseUrl + '/ia/placeholders/load-fields',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        module: $('#placeholders-module').val()
                    };
                    return query;
                },
                processResults: function (data)
                {
                    var array = [];

                    for (var i in data){
                        array.push({id: data[i].name, text: data[i].text});
                    }
                    return {
                        results: array
                    };
                }
            },
            width:'100%',
            placeholder: 'Choose field'
        });
    }

    function typePlaceholder(selector) {
        if ($(selector).val() === 'additional_template') {
            $('#placeholders-field').closest('div.form-group').hide();
            $('#placeholders-default').closest('div.form-group').hide();
        } else {
            $('#placeholders-field').closest('div.form-group').show();
            $('#placeholders-default').closest('div.form-group').show();
        }
    }

    $('#placeholders-module').on('change', function () {
        $('#placeholders-field').val(null).trigger('change');

        getFields();
    });

    getFields();

    var type = $('select[name="Placeholders[type_placeholder]"]');
    var module = $('select[name="Placeholders[module]"]');
    var path = $('select[name="Placeholders[field]"]');
    var default_field = $('input[name="Placeholders[default]"]');
    var desc = $('textarea[name="Placeholders[description]"]');

    if ($('input[name="Placeholders[fake]"]').is(':checked')) {
        type.attr('disabled', 'disabled');
        module.attr('disabled', 'disabled');
        path.attr('disabled', 'disabled');
        default_field.attr('disabled', 'disabled');
        desc.attr('disabled', 'disabled');
    }

    $('input[name="Placeholders[fake]"]').on('click', function (e) {
        if ($(this).prop('checked')) {
            type.attr('disabled', 'disabled');
            module.attr('disabled', 'disabled');
            path.attr('disabled', 'disabled');
            default_field.attr('disabled', 'disabled');
            desc.attr('disabled', 'disabled');
        } else {
            type.removeAttr('disabled');
            module.removeAttr('disabled');
            path.removeAttr('disabled');
            default_field.removeAttr('disabled');
            desc.removeAttr('disabled');
        }
    });

    $('#placeholders-type_placeholder').on('change', function () {
        typePlaceholder(this);
    });

    typePlaceholder($('#placeholders-type_placeholder'));
});