$(document).ready(function () {
    function getPath() {
        $('#deliveryplaceholders-path').select2({
            ajax:{
                url: baseUrl + '/ia/delivery-placeholders/load-path',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        module: $('#deliveryplaceholders-module').val(),
                    };
                    return query;
                },
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: i, text: data[i]});
                    }
                    return {
                        results: array
                    };
                }
            },
            delay: 1000,
            width:'100%',
            placeholder: 'Choose path'
        });
    }

    $('#deliveryplaceholders-module').on('change', function () {
        getPath();
    });

    getPath();
});