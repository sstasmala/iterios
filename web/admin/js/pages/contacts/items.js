/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.text+'</b></div>';
    // markup += '<div style="font-size: small">Original: '+perm.original_name+' Code:<b>'+perm.id+'</b></div>';
    return markup;
}

function formatPermSel (perm) {
    window.fieldLeng = perm.id;
    if (perm.text == undefined)
        return perm.text;

    return perm.text ;//+ '('+perm.original_name+')';
}

$(document).ready(function () {
    function setEditors() {
        $('.delete-item').off('click');
        $('.delete-item').on('click',function (e) {
            e.preventDefault();
            $(this).parent().parent().remove();
        });
    }
    setEditors();

    additionals.forEach(function (item,i,array) {
        $('#'+item.id+'_type_id').select2({
            ajax:{
                url: baseUrl + '/ia/demo-contacts/search-type',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        name: params.term,
                        type:item.id
                    };
                    return query;
                },
                delay:1000,
                minimumInputLength: 0,
                processResults: function (data)
                {
                    var array = [];

                    for(var i in data)
                        array.push({id: data[i]['id'], text: data[i]['value']})

                    return {
                        results: array
                    }
                }
            },
            placeholder: 'Select type',
            allowClear: false,
            width: 'resolve',
            // tags: true,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatPerm,
            templateSelection: formatPermSel,
            dropdownParent: $('#'+item.id+'s-modal')
        });

        $('#'+item.id+'s-modal').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            if(button.data('index')==undefined)
            {
                $('#'+item.id+'_type_id').val('');
                $('#'+item.id+'_value').val('');
                $('#'+item.id+'_type_id').empty().trigger('change');
            }
            else
            {
                var row = button.parents('.row[data-index="'+button.data('index')+'"]');
                $('#'+item.id+'_type_id').val(row.find('.'+item.id+'_type_id').val());
                $('#'+item.id+'_value').val(row.find('.'+item.id+'_value').val());
                console.log(row);
                var dt = {
                    id:row.find('.'+item.id+'_type_id').val(),
                    value:row.find('.'+item.id+'-type').text()
                };
                var newOption = new Option(dt.value,dt.id, false, false);
                $('#'+item.id+'_type_id').val(row.find('.'+item.id+'_type_id').val());
                $('#'+item.id+'_type_id').append(newOption).trigger('change');
            }
            console.log("Hello");
            $('#'+item.id+'_confirm').off('click');
            $('#'+item.id+'_confirm').on('click',function () {
                var data = '';
                if(button.data('index')==undefined)
                {
                    var block = $('.'+item.id+'s');
                    data = '<div class="row" data-index="'+window[item.id+'s_index']+'"><input class="'+item.id+'_type_id" type="hidden" value="'+$('#'+item.id+'_type_id').val()+'" name="'+item.name+'['+window[item.id+'s_index']+'][type_id]">' +
                        '<input class="'+item.id+'_type_value" type="hidden" value="'+$('#'+item.id+'_type_id option:selected').data().data.text+'" name="'+item.name+'['+window[item.id+'s_index']+'][type_value]">' +
                        '<input class="'+item.id+'_id" type="hidden" value="" name="'+item.name+'['+window[item.id+'s_index']+'][id]">' +
                        '<input class="'+item.id+'_value" type="hidden" value="'+$('#'+item.id+'_value').val()+'" name="'+item.name+'['+window[item.id+'s_index']+'][value]">' +
                        '<div class="col-md-4 text-left '+item.id+'-type">'+$('#'+item.id+'_type_id option:selected').data().data.text+'</div>' +
                        '<div class="col-md-4 text-center '+item.id+'-value">'+$('#'+item.id+'_value').val()+'</div>' +
                        '<div class="col-md-4 text-right">' +
                        '<a data-index="'+window[item.id+'s_index']+'" class="edit-'+item.id+' btn btn-warning" data-toggle="modal" data-target="#'+item.id+'s-modal"><i class="fa fa-edit"></i></a> ' +
                        '<button data-id="" class="delete-item btn btn-danger"><i class="fa fa-trash"></i></button>' +
                        '</div></div>';
                    block.append(data);
                    setEditors();
                }
                else
                {
                    data = $('.'+item.id+'s').find('.row[data-index="'+button.data('index')+'"]');
                    data.find('.'+item.id+'_value').val($('#'+item.id+'_value').val());
                    data.find('.'+item.id+'_type_id').val($('#'+item.id+'_type_id').val());
                    data.find('.'+item.id+'_type_value').val($('#'+item.id+'_type_id option:selected').data().data.text);
                    data.find('.'+item.id+'-type').text($('#'+item.id+'_type_id option:selected').data().data.text);
                    data.find('.'+item.id+'-value').text($('#'+item.id+'_value').val());
                }
                $('#'+item.id+'s-modal').modal('hide');
                window[item.id+'s_index']++;
            });
        });
    });
});