/**
 * Created by zapleo on 20.12.17.
 */
$('document').ready(function () {

    // $('#templateshandlers-template_id').on('change',function () {
    //     var hdsel = $('#templateshandlers-handler');
    //     hdsel.find('option').remove();
    //     hdsel.val(null).trigger('change');
    //     $('#preview-header').remove();
    //     $('#preview-body').remove();
    //     $.ajax({
    //         url: baseUrl + '/templates-handlers/get-type?id='+$(this).val(),
    //         method:'get',
    //         success:function ($response) {
    //             if($response)
    //                 $.each(handlers[$response], function(index, value) {
    //                     var option = new Option(value, index, false, true);
    //                     hdsel.append(option).val(null);
    //                 });
    //         }
    //     })
    // });

    $('#templateshandlers-handler').on('change',function () {
       $.ajax({
           url: baseUrl + '/ia/templates-handlers/get-preview',
           method:'post',
           data:{
               handler:$(this).val(),
               template: $('#templateshandlers-template_id').val()
           },
           success:function ($response) {
               if($response)
               {
                   //console.log($response);
                   var warning = $('#warning');
                   warning.empty();
                   if($response['error'])
                       warning.append('<div class="alert alert-warning alert-dismissible">'+
                           '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                           '<h4><i class="icon fa fa-warning"></i> Warning!</h4>'+
                            $response['error']+
                        '</div>');
                   $('#preview-header').remove();
                   $('#preview-body').remove();
                   warning.parent()
                       .append('<iframe id="preview-header" style="width: 100%" height="50" sandbox="" srcdoc="'+
                           $response['data']['subject']
                           +'"></iframe>')
                       .append('<iframe id="preview-body" style="width: 100%" height="500" sandbox="" srcdoc="'+$response['data']['body']+'"></iframe>');
               }
           }
       });
    });
});

