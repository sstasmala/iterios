
function get_shortcodes(data) {
    $.ajax({
        url: baseUrl + '/ia/documents-template/get-placeholders',
        dataType: 'json',
        data: {
            lang: $('#documentstemplate-languages').val(),
        },
        success: function (data) {
            set_placeholders(data);

            $.each(CKEDITOR.instances, function(ins_name, instance) {
                instance.destroy();
                initCKEditor(ins_name);
            });
        },
        error: function () {
            alert('Неудалось получить данные!');
        }
    });

}

$('document').ready(function () {
    initCKEditor('documentstemplate-body');
    var data = $('#documentstemplate-languages').val();
    get_shortcodes(data);
});

$('select#documentstemplate-languages').change(function() {
    var data = $(this).select2('data')[0];
    get_shortcodes(data.id);
});