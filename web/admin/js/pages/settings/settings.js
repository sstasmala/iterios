function settings_save(data) {
    $.ajax({
        url: baseUrl + '/ia/settings/settings-save',
        dataType: 'json',
        type: 'POST',
        data: data,
        success: function (res) {
            alertify.logPosition("bottom right").success("Settings save");

            setTimeout(function() {
                location.reload();
            }, 1000);
        },
        error: function () {
            console.log('Неудалось получить данные!');
        }
    });
}

$('document').ready(function () {
    alertify.closeLogOnClick(true);

    $('button#edit_email_providers').on('click',function () {
        if ($('table#email_providers_read').is(':visible')) {
            $('table#email_providers_read').hide();
            $('table#email_providers_edit').show();

            $('button#edit_email_providers').hide();
            $('button#save_email_providers, button#read_email_providers').show();
        }
    });

    $('button#read_email_providers').on('click',function () {
        if (!$('table#email_providers_read').is(':visible')) {
            $('table#email_providers_read').show();
            $('table#email_providers_edit').hide();

            $('button#edit_email_providers').show();
            $('button#save_email_providers, button#read_email_providers').hide();
        }
    });

    $('button#save_email_providers').on('click',function () {
        var data = {
            'provider_system': $('table#email_providers_edit select[name="provider_system"]').val(),
            'provider_public': $('table#email_providers_edit select[name="provider_public"]').val(),
            'email_from_name': $('table#email_providers_edit input#email_from_name').val(),
            'email_from': $('table#email_providers_edit input#email_from').val(),
            'email_reply_to': $('table#email_providers_edit input#email_reply_to').val()
        };

        settings_save(data);
    });


    $('button#edit_MCHelper').on('click',function () {
        if ($('table#MCHelper_read').is(':visible')) {
            $('table#MCHelper_read').hide();
            $('table#MCHelper_edit').show();

            $('button#edit_MCHelper').hide();
            $('button#save_MCHelper, button#read_MCHelper').show();
        }
    });

    $('button#read_MCHelper').on('click',function () {
        if (!$('table#MCHelper_read').is(':visible')) {
            $('table#MCHelper_read').show();
            $('table#MCHelper_edit').hide();

            $('button#edit_MCHelper').show();
            $('button#save_MCHelper, button#read_MCHelper').hide();
        }
    });

    $('button#save_MCHelper').on('click',function () {
        var data = {
            'MCHelper_base_uri': $('table#MCHelper_edit input#MCHelper_base_uri').val(),
            'MCHelper_Key': $('table#MCHelper_edit input#MCHelper_Key').val()
        };

        settings_save(data);
    });

});