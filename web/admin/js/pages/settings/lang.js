/**
 * Created by zapleo on 31.10.17.
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }

    var markup = '<div style="font-size: large"><b>'+(perm.name != null ? perm.name : '(not set)')+'</b></div>';
    markup += '<div style="font-size: small">';

    if (perm.original_name != null)
        markup += 'Original: ' + perm.original_name + ' ';

    markup += 'Code: <b>' + perm.iso + '</b></div>';
    // markup += '<div style="font-size: small">Methods: '+perm.method+'</div>';
    return markup;
}

function formatPermSel (perm) {
    return perm.name || perm.text;
}

function assign(elements, action) {
    var ids = [];

    elements.each(function () {
        ids.push($(this).val());
    });

    $.ajax({
        url:baseUrl + '/ia/settings/' + action,
        data: {
                ids: ids
        },
        method: 'post',
        success: function (response) {
            location.reload();
            // alertify.logPosition("top right").success("Permissions assigned");
        },
        error: function (response) {
            console.log(response);
            alertify.logPosition("top right").error(response.responseText);
        }
    });
}

function select(id, action) {
    $('#' + id).select2({
        ajax:{
            url: baseUrl + '/ia/settings/' + action,
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };

                return query;
            },
            delay:500,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data) {
                    array.push({
                        id: data[i]['id'],
                        name: data[i]['name'],
                        original_name: (data[i]['original_name'] ? data[i]['original_name'] : null),
                        iso: (data[i]['iso'] ? data[i]['iso'] : (data[i]['code'] ? data[i]['code'] : null))
                    });
                }

                return {
                    results: array
                }
            }
        },
        width: '100%',
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });
}

$('document').ready(function () {
    alertify.closeLogOnClick(true);
    var url = window.location;

    $('#language-enable').on('click',function (e) {
        e.preventDefault();
        $('#select-lang').modal('show');
    });

    $('#assign-lang').on('click',function () {
        $('#select-lang').modal('hide');
        var elements = $('#select-lang option:selected');

        assign(elements, 'activate-languages');
    });

    select('lang-list', 'get-languages-list');

    alertify.logPosition("bottom right");
});