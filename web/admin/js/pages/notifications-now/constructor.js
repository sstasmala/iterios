function isChecked($input, $class) {
	var $redactor = $($class);
	if ($input.is(':checked')) {
		$redactor.show();
	} else {
		$redactor.hide();
	}
}

$('document').ready(function () {

    initCKEditor('notificationsnow-text_email');
    //initCKEditor('notificationsnow-text_notice');

    isChecked($('.field-notificationsnow-is_email input'), '.field-notificationsnow-text_email,.field-notificationsnow-title_email');
    isChecked($('.field-notificationsnow-is_notice input'), '.field-notificationsnow-text_notice');

    $('.field-notificationsnow-is_email input').on('change', function(){
    	isChecked($(this), '.field-notificationsnow-text_email,.field-notificationsnow-title_email');
    });

    $('.field-notificationsnow-is_notice input').on('change', function(){
		isChecked($(this), '.field-notificationsnow-text_notice');
	});
});