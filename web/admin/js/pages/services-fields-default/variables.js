/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
// function renderDefault() {
//     $('.variants-box').empty();
// }
//
// function readVariables()
// {
//     var data = $('#servicesfieldsdefault-variables').val();
//     if(data == '' || data == '{}')
//         return;
//
//     data = JSON.parse(data);
//     data.forEach(function (item) {
//         var elem = '<div>';
//         elem += '<input class="form-control" value="'+item+'">';
//         elem += '<button class="btn btn-danger">-</button>';
//         elem += '</div>';
//         $('#variables').empty().append(elem);
//     });
//
// }
//
// $('document').ready(function () {
//     $('#servicesfieldsdefault-type').on('change',function () {
//         console.log($(this).val());
//         if($(this).val() == 'dropdown' || $(this).val() == 'multiselect')
//             $('#add-variable').show();
//         else
//             $('#add-variable').hide();
//     });
//
//     $('#add-variable').on('click',function (e) {
//         e.stopPropagation();
//         e.preventDefault();
//         var elem = '<div class="row">';
//         elem += '<input class="form-control col-md-10">';
//         elem += '<button class="btn btn-danger col-md-2">-</button>';
//         elem += '</div>';
//         $('#variables').append(elem);
//     });
// });



function RenderType(){
    this.type = undefined;
    this.variants_input = undefined;
    this.container = undefined;
    this.lang = undefined;
    this.parent = undefined;

    var Item = this;

    this.removeElement = function (index,count) {
        $(Item.variants_input).each(function (){
            var data = [];
            if($(this).val()!=undefined && $(this).val()!='')
                data = JSON.parse($(this).val());
            if(data.length >=count)
                data.splice(index,1);
            $(this).val(JSON.stringify(data));
        });
    };

    this.updateVariant = function () {

        var data = [];
        // console.log("Container: ");console.log(Item.container);
        Item.container.find('.selection-variant').each(function () {
            data.push($(this).val())
        });
        console.log("Data: ");console.log(data);
        Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val(JSON.stringify(data));
    };

    this.renderSelection = function () {

        var overlay = parent.find('overlay');
        if(overlay.length>0)
            overlay.show();
        var max_count = 0;
        Item.parent.find(Item.variants_input).each(function () {
            var value = $(this).val();
            if(value != '')
                value = JSON.parse(value);
            else
                value = [];
            if(value.length>max_count)
                max_count = value.length;
        });
        // console.log(max_count);
        var input_value  = Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val();

        if(input_value != '' && input_value != undefined)
            input_value = JSON.parse(input_value);
        else
            input_value = [];

        var container = Item.container;
        container.empty();

        var template = '<div class="form-group">'+
            '<label>Variants</label>'+
            '<div class="variants-list">';

        var diff = max_count - input_value.length;
        // console.log(input_value.length);
        if(diff>0){
            for(var j = 0; j < diff; j++)
                input_value.push('');
            Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val(JSON.stringify(input_value));
        }


        for(var i = 0; i < input_value.length; i++)
        {
            template += '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value="'+input_value[i]+'"'+
                '></div>'+'<div class="col-sm-1"><button class="btn btn-danger remove-element">-</button></div>'+'</div>';
        }

        template += '</div>'+'<button class="btn btn-success add-element">+</button>';
        template += '</div>';

        container.append(template);

        if(overlay.length>0)
            overlay.hide();

        //container events

        var component = Item;
        $('.add-element').on('click',function (e) {
            e.stopPropagation();
            e.preventDefault();
            var tmp = '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value=""></div>'+'<div class="col-sm-1"><button class="btn btn-danger remove-element">-</button></div>'+'</div>';
            Item.container.find('.variants-list').append(tmp);

            var elements = Item.container.find('.remove-element');
            elements.off('click');
            elements.on('click',function (e) {
                console.log('remove');
                e.stopPropagation();
                e.preventDefault();
                var item = component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val();
                var count = 0;
                if(item != undefined && item != '')
                    count = JSON.parse(component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val()).lenght;
                component.removeElement($(this).closest('.variants-element').index(),count);
                $(this).closest('.variants-element').remove();
                component.updateVariant();
            });

            var inputs = $('.selection-variant');
            inputs.off('keyup');
            inputs.on('keyup',function () {
                console.log('changes');
                component.updateVariant();
            });
            component.updateVariant();
        });

        var elements = $('.remove-element');
        elements.off('click');
        elements.on('click',function (e) {
            console.log('remove');
            e.stopPropagation();
            e.preventDefault();
            var item = component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val();
            var count = 0;
            if(item != undefined && item != '')
                count = JSON.parse(component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val()).lenght;
            component.removeElement($(this).closest('.variants-element').index(),count);
            $(this).closest('.variants-element').remove();
            component.updateVariant();
        });

        var inputs = $('.variants-element input');
        inputs.off('keyup');
        inputs.on('keyup',function () {
            component.updateVariant();
        });
    };

    this.renderSwitch = function () {
        var input_value  =  Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val();
        if(input_value != '' && input_value != undefined)
            input_value = JSON.parse(input_value);
        else
            input_value = ['',''];

        var container = Item.container;
        container.empty();

        var template = '<div class="form-group">'+
            '<label>Variants</label>'+
            '<div class="variants-list">';

        for(var i = 0; i < 2; i++)
        {
            template += '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value="'+input_value[i]+'"'+
                '></div></div>';
        }

        template += '</div>';
        template += '</div>';

        container.append(template);
        var component = Item;
        var inputs = container.find('.variants-element input');
        inputs.off('keyup');
        inputs.on('keyup',function () {
            console.log('changes');
            component.updateVariant();
        });
    }

    this.renderDefault = function () {
        this.container.empty();
    };

    this.renderField = function () {
        switch (Item.type)
        {
            case 'switch':Item.renderSwitch();break;
            case 'radio':
            case 'dropdown':
            case 'multiselect': Item.renderSelection(); break;
            default: Item.renderDefault();break;
        }
    };

    return function (type,variants_input,container,parent,lang) {
        Item.container = parent.find(container);
        Item.lang = lang;
        Item.type = type;
        Item.variants_input = variants_input;
        Item.parent = parent;
        Item.renderField();
        console.log('render');
        console.log(parent.find(container));
    }
};


$('document').ready(function () {

    var types = $('#servicesfieldsdefault-type');
    types.on('change',function () {
        (new RenderType())($(this).val(),'.variables-input','.variants-box',$('.services-fields-default-form'),$('#lang-list').val());
        // renderVariants($(this).val());
    });
    (new RenderType())(types.val(),'.variables-input','.variants-box',$('.services-fields-default-form'),$('#lang-list').val());

    $('select#lang-list[data-action="create"]').change(function() {
        switchFormLanguage(translatable_attributes, $(this).val(), default_language, translatable_model_name, null);
        (new RenderType())(types.val(),'.variables-input','.variants-box',$('.services-fields-default-form'),$('#lang-list').val());
    });

    $('select#lang-list[data-action="update"]').off().change(function() {
        var new_language = $(this).val();
        if (translatable_model_id && !translated_models[new_language]) {
            $.ajax({
                url: baseUrl + translatable_controller_path + '/get-translate?id=' + translatable_model_id + '&lang=' + new_language,
                dataType: 'json',
                data: {},
                success: function (data) {
                    translated_models[new_language] = data;
                    switchFormLanguage(translatable_attributes, new_language, default_language, translatable_model_name, data);
                    (new RenderType())(types.val(),'.variables-input','.variants-box',$('.services-fields-default-form'),$('#lang-list').val());
                }
            });
        } else {
            switchFormLanguage(translatable_attributes, new_language, default_language, translatable_model_name, null);
            (new RenderType())(types.val(),'.variables-input','.variants-box',$('.services-fields-default-form'),$('#lang-list').val());
        }
    });
    // renderVariants(types.val());

    // $('#lang-list').on('change',function ()
});