/**
 * index.js
 * @copyright © Iterios
 * @author Vlad Savelyev vladsova777@gmail.com
 */

$('document').ready(function () {
    $('#country-select').each(function(){
        var parentElement = $(this).parent();
        $('#country-select').select2({
            ajax: {
                url: baseUrl + '/ita/' + 1 + '/contacts/get-countries-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public',
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    };
                    return query;
                },
                delay: 1000,
                minimumInputLength: 0,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['title']});
                    }
                    return {
                        results: array
                    }
                }
            },
            width: '100%',
            dropdownParent: parentElement,
            placeholder: 'Select Country'
        });
    });
})


