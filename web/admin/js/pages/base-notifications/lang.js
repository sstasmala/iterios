/**
 * Created by zapleo on 31.10.17.
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    markup += '<div style="font-size: small">Original: '+perm.original_name+' Code: <b>'+perm.id+'</b></div>';
    return markup;
}

function formatPermSel (perm) {
    if (perm.name == undefined)
        return perm.text;

    return perm.name + '('+perm.original_name+')';
}

$('document').ready(function () {

    $('#lang-list').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-languages',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['iso'], name: data[i]['name'], original_name: data[i]['original_name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose language',
        allowClear: false,
        width: '50%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });

    $('select#lang-list[data-action="view"]').change(function() {
        location.href = baseUrl + '/ia/base-notifications/view?id='+template_id+'&lang='+$(this).val();
    });

    $('select#lang-list[data-action="index"]').change(function() {
        location.href = baseUrl + '/ia/base-notifications/index?lang='+$(this).val();
    });

});