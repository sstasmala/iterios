
var selectTags = [];
var selectStatuses = [];
var selectServices = [];
var selectHolidays = [];

var configurationAction =
    {
        'contact':[
            {
                id: 'contacts.date_of_birth',
                label: 'Contacts: birth date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'contacts.passport_due_date',
                label: 'Contacts: passport due date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'contacts.visa_due_date',
                label: 'Contacts: visa due date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'requests.days_complete',
                label: 'Requests: days complete',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type="hidden" value="0"  name="' + input_name + '">';
                },
                default_value:0
            },
            {
                id: 'orders.departure_date_start',
                label: 'Orders: departure date starting',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'orders.departure_date_end',
                label: 'Orders: departure date ending',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'tasks.overdue',
                label: 'Task: Overdue',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type="hidden" value="1"  name="' + input_name + '">';
                },
                default_value: 0
            },
            {
                id: 'holidays.system',
                label: 'System Holidays',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
        ],
        'agent':[
            {
                id: 'contacts.date_of_birth',
                label: 'Contacts: Birth date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'contacts.passport_due_date',
                label: 'Contacts: passport due date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'contacts.visa_due_date',
                label: 'Contacts: visa due date',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'requests.days_complete',
                label: 'Requests: days complete',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type="hidden" value="0"  name="' + input_name + '">';
                },
                default_value:0
            },
            
            {
                id: 'orders.departure_date_start',
                label: 'Orders: departure date starting',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'orders.departure_date_end',
                label: 'Orders: departure date ending',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
            {
                id: 'tasks.days_complete',
                label: 'Task: days complete',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type="hidden" value="0"  name="' + input_name + '">';
                },
                default_value: 0
            },
            {
                id: 'tasks.overdue',
                label: 'Task: Overdue',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type="hidden" value="1"  name="' + input_name + '">';
                },
                default_value: 0
            },
            {
                id: 'holidays.system',
                label: 'System Holidays',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },
        ]
    };


var configuration = {
        'contacts':[
            {
                id: 'contacts.tag_id',
                label: 'Contact: Tag',
                type: 'integer',
                operators: ['equal', 'not_equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger option[value=2]').prop('disabled', true).hide();
                    selectTags.push({'rule': rule, 'name': input_name});
                    return '<select class="select2-tags" name="' + input_name + '"></select>';
                }
            },
        ],

        'requests':[
            {
                id: 'requests.status_id',
                label: 'Requests: Change status',
                type: 'integer',
                operators: ['equal', 'not_equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', true).hide();
                    selectStatuses.push({'rule': rule, 'name': input_name});
                    return '<select class="select2-statuses" name="' + input_name + '"></select>';
                },
                default_value:0
            },
        ],

        'holidays':[
            {
                id: 'contacts.tag_id',
                label: 'Contact: Tag',
                type: 'integer',
                operators: ['equal', 'not_equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger option[value=2]').prop('disabled', true).hide();
                    selectTags.push({'rule': rule, 'name': input_name});
                    return '<select class="select2-tags" name="' + input_name + '"></select>';
                }
            },
            {
                id: 'holidays.system_id',
                label: 'Holidays System List:',
                type: 'integer',
                operators: ['equal'],
                multiple : true,
                input: function (rule, input_name) {
                    selectHolidays.push({'rule': rule, 'name': input_name});
                    return '<select style="width:100px;" class="select2-holidays" name="' + input_name + '"></select>';
                }
            },

        ],

        'orders':[
            {
                id: 'orders.services',
                label: 'Orders Services:',
                type: 'integer',
                operators: ['equal'],
                multiple : true,
                input: function (rule, input_name) {
                    selectServices.push({'rule': rule, 'name': input_name});
                    return '<select style="width:100px;" class="select2-services" name="' + input_name + '"></select>';
                }
            },
        ],
    };


function setSelectHolidays() {
    //select2('refresh')
    $('.select2-holidays').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-holidays',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], text: data[i]['name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose holidays',
        allowClear: false,
        width: '100%',
        // escapeMarkup: function (markup) { return markup; },
        // templateResult: formatPerm,
        // templateSelection: formatPermSel
    });

    //$('.select2-services').closest('.rule-value-container').find('.select2-container').css('min-width', '120px');


    selectHolidays.forEach(function (item) {
        
        var select = $('select[name="'+item.name+'"]');
        if(item.rule.value !== undefined)
        {
            $.when($.ajax({
                        url: baseUrl + '/ia/base-notifications/get-holiday-by-id?id='+item.rule.value,
                        method: 'get'
                    })).then(function (result) {
                        //if (result.length == 0)
                        //    return;

                        //if (result)
                        //console.log(result.length);
                         var newOption = new Option(result.name, result.id, false, true);
                         //console.log(newOption);
                        select.append(newOption);

                        //select.change();
                     })
        }
        //console.log(item.rule.value);
    });

}

function setServicesSelects() {
    $('.select2-services').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-services',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], text: data[i]['name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose services',
        allowClear: false,
        width: '100%',
        // escapeMarkup: function (markup) { return markup; },
        // templateResult: formatPerm,
        // templateSelection: formatPermSel
    });

    //$('.select2-services').closest('.rule-value-container').find('.select2-container').css('min-width', '120px');


    selectServices.forEach(function (item) {

        var select = $('select[name="'+item.name+'"]');
        if(item.rule.value !== undefined)
        {
            $.when($.ajax({
                        url: baseUrl + '/ia/base-notifications/get-service-by-id?id='+item.rule.value,
                        method: 'get'
                    })).then(function (result) {
                        //if (result.length == 0)
                        //    return;

                        //if (result)
                        //console.log(result.length);
                         var newOption = new Option(result.name, result.id, false, true);
                         //console.log(newOption);
                        select.append(newOption);

                        //select.change();
                     })
        }
        //console.log(item.rule.value);
    });

}

function setStatusesSelects() {
    $('.select2-statuses').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-statuses',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], text: data[i]['name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose status',
        allowClear: false,
        width: '100%',
        // escapeMarkup: function (markup) { return markup; },
        // templateResult: formatPerm,
        // templateSelection: formatPermSel
    });

    
    selectStatuses.forEach(function (item) {
        
        var select = $('select[name="'+item.name+'"]');
        if(item.rule.value !== undefined)
        {
            //console.log(item.rule.value + '=============');
            $.when($.ajax({
                        url: baseUrl + '/ia/base-notifications/get-status-by-id?id='+item.rule.value,
                        method: 'get'
                    })).then(function (result) {
                        //onsole.log(result);
                         var newOption = new Option(result.name, result.id, false, false);
                        select.append(newOption).trigger('change');
                     })
        }
        //console.log(item.rule.value);
    });
}

function setTagsSelects() {

    $('.select2-tags').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-tags',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], text: data[i]['value']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose tag',
        allowClear: false,
        width: '100%',
        // escapeMarkup: function (markup) { return markup; },
        // templateResult: formatPerm,
        // templateSelection: formatPermSel
    });
    selectTags.forEach(function (item) {
        var select = $('select[name="'+item.name+'"]');
        if(item.rule.value !== undefined)
        {
            $.when($.ajax({
                        url: baseUrl + '/ia/base-notifications/get-tag-by-id?id='+item.rule.value,
                        method: 'get'
                    })).then(function (result) {
                        console.log(result);
                         var newOption = new Option(result.value, result.id, false, false);
                        select.append(newOption).trigger('change');
                     })
        }
        console.log(item.rule.value);
    })
}
var builder = $('#builder');
var builder_trigger = $('#builder_trigger');

function builderInit(conf,rules,conditions) {

    builder.queryBuilder({
        plugins: ['bt-tooltip-errors'],
        filters: conf,
        conditions:conditions,
        default_condition:'OR',
        allow_groups:0
    });

    builder.on('afterUpdateRuleFilter.queryBuilder',setTagsSelects);
    builder.on('afterSetRules.queryBuilder',setTagsSelects);

    builder.on('afterUpdateRuleFilter.queryBuilder',setStatusesSelects);
    builder.on('afterSetRules.queryBuilder',setStatusesSelects);

    builder.on('afterUpdateRuleFilter.queryBuilder',setServicesSelects);
    builder.on('afterSetRules.queryBuilder',setServicesSelects);

    builder.on('afterUpdateRuleFilter.queryBuilder',setSelectHolidays);
    builder.on('afterSetRules.queryBuilder',setSelectHolidays);

    builder.on('afterCreateRuleInput.queryBuilder', function(e, rule) {
        if (rule.filter.id === 'created_at' || rule.filter.id === 'contacts.created_at') {
            var $input = rule.$el.find('.rule-value-container [name*=_value_]');
            $input.on('dp.change', function() {
                $input.trigger('change');
            });
        }
        if (rule.filter.id === 'date_of_birth') {
            var $input = rule.$el.find('.rule-value-container [name*=_value_]');
            $input.val(2);
            $input.trigger('change');
        }
    });

    if(rules !== undefined && rules !== '')
        builder.queryBuilder('setRules', JSON.parse(rules));

    builder.on('rulesChanged.queryBuilder',function (rule) {
        var result = $(this).queryBuilder('getRules');
        if (!$.isEmptyObject(result)) {
            $('#basenotifications-condition').val(JSON.stringify(result, null, 2));
        }
    });
    builder.on('afterUpdateRuleValue.queryBuilder', function(e, rule) {
        if (rule.filter.plugin === 'datepicker') {
            rule.$el.find('.rule-value-container input').datepicker('update');
        }
    });

    builder.on('afterDeleteRule.queryBuilder',function (rule) {
        $('#builder .group-conditions .btn-primary').removeClass('disabled');
        $('#builder .group-conditions input').prop('disabled', false);
    });
}

var prev_id = false;
function builderTriggerInit(conf,rules) {

    builder_trigger.queryBuilder({
        plugins: ['bt-tooltip-errors'],
        filters: conf,
        default_group_flags: {
            no_add_rule: true
        }
    });

    builder_trigger.on('afterUpdateRuleFilter.queryBuilder', setTagsSelects);
    builder_trigger.on('afterSetRules.queryBuilder', setTagsSelects);

    builder_trigger.on('afterUpdateRuleFilter.queryBuilder', setStatusesSelects);
    builder_trigger.on('afterSetRules.queryBuilder', setStatusesSelects);

    builder_trigger.on('afterUpdateRuleFilter.queryBuilder', setServicesSelects);
    builder_trigger.on('afterSetRules.queryBuilder', setServicesSelects);

    builder.on('afterUpdateRuleFilter.queryBuilder',setSelectHolidays);
    builder.on('afterSetRules.queryBuilder',setSelectHolidays);

    builder_trigger.on('afterCreateRuleInput.queryBuilder', function (e, rule) {
        if (rule.filter.id === 'created_at' || rule.filter.id === 'contacts.created_at') {
            var $input = rule.$el.find('.rule-value-container [name*=_value_]');
            $input.on('dp.change', function () {
                $input.trigger('change');
            });
        }
        if (rule.filter.id === 'date_of_birth') {
            var $input = rule.$el.find('.rule-value-container [name*=_value_]');
            $input.val(2);
            $input.trigger('change');
        }
    });


    if (rules !== undefined && rules !== '') {
        rules = JSON.parse(rules);
        console.log(rules);
        var rule_id = rules.rules[0].id.replace('.', '-');

        $('.placeholders').hide();
        $('.' + rule_id).show();


        builder_trigger.queryBuilder('setRules', rules);


        var rule_group = rules.rules[0]['id'].split('.');
        rule_group = rule_group[0];
        var rules = $('#basenotifications-condition').val();


        if (typeof configuration[rule_group] != 'undefined' && rules != 'false' && rules != '') {

            rules = JSON.parse(rules);

            // filter rules by rule_group
            for (key in rules['rules']) {
                var rule_group_curent = rules['rules'][key]['id'].split('.');
                rule_group_curent = rule_group_curent[0];
                //console.log(rule_group_curent);
                if (rule_group != rule_group_curent) {
                    delete rules['rules'][key];
                }
            }

            if ($.isEmptyObject(rules['rules'])) {
                rules = '';
            } else {
                rules = JSON.stringify(rules);
            }

            builderInit(configuration[rule_group], rules, ['AND', 'OR']);

            $('#builder .group-conditions .btn-primary').removeClass('disabled');
            $('#builder .group-conditions input').prop('disabled', false);


            //if (rule.filter.plugin === 'datepicker') {
            //    rule.$el.find('.rule-value-container input').datepicker('update');
            //}
        } else {
            removeBuilder();
            $('#basenotifications-condition').val('');
        }

    }


    builder_trigger.on('rulesChanged.queryBuilder', function (rule) {
        var result = $(this).queryBuilder('getRules');
        if (!$.isEmptyObject(result)) {
            $('#basenotifications-trigger_action').val(JSON.stringify(result, null, 2));
        }
    });


    builder_trigger.on('afterUpdateRuleValue.queryBuilder', function (e, rule) {


        if (rule.filter.id == 'tasks.overdue') {
            prev_id = $('#basenotifications-trigger').val();

            $('#basenotifications-trigger option').prop('disabled', true).hide();
            $('#basenotifications-trigger option[value=1]').prop('disabled', false).prop('selected', true).show();
            $('#basenotifications-trigger').change();
        } else {
            if (prev_id !== false) {
                $('#basenotifications-trigger option').prop('disabled', false).prop('selected', false).show();
                $('#basenotifications-trigger option[value=' + prev_id + ']').prop('selected', true);
                $('#basenotifications-trigger').change();
            }
        }

        var rule_id = rule.filter.id.replace('.', '-');

        $('.placeholders').hide();
        $('.' + rule_id).show();

        removeBuilder();
        var rule_group = rule.filter.id.split('.');
        rule_group = rule_group[0];


        if (typeof configuration[rule_group] != 'undefined') {
            builderInit(configuration[rule_group], '', ['AND', 'OR']);

            $('#builder .group-conditions .btn-primary').removeClass('disabled');
            $('#builder .group-conditions input').prop('disabled', false);

            if (rule.filter.plugin === 'datepicker') {
                rule.$el.find('.rule-value-container input').datepicker('update');
            }
        } else {
            removeBuilder();
            $('#basenotifications-condition').val('');
        }
    });

    if ($('#builder').html() == '') {
        $('#builder_trigger_rule_0 select').change();
    }
}

function removeBuilder() {
    builder.off('afterUpdateRuleFilter.queryBuilder');
    builder.off('afterSetRules.queryBuilder');
    builder.off('rulesChanged.queryBuilder');
    builder.off('afterUpdateRuleValue.queryBuilder');
    builder.off('afterCreateRuleInput.queryBuilder');
    builder.queryBuilder('destroy');
    // builder.off();
}

function removeBuilderTrigger() {
    builder_trigger.off('afterUpdateRuleFilter.queryBuilder');
    builder_trigger.off('afterSetRules.queryBuilder');
    builder_trigger.off('rulesChanged.queryBuilder');
    builder_trigger.off('afterUpdateRuleValue.queryBuilder');
    builder_trigger.off('afterCreateRuleInput.queryBuilder');
    builder_trigger.queryBuilder('destroy');
    // builder.off();
}

function check_checked($obj, clases) {
    var input = $obj.find('input[type=checkbox]');
   // console.log(input);

    if (input.is(':checked')) {
        $(clases).show();
    } else {
        $(clases).hide();
    }
}



function initBuilderBlock(lang) {
    //alert(lang);
    var rules_trigger_action = $('#basenotifications-trigger_action').val();

    if($('#basenotifications-module').val() === 'agents')
    {
        //builderInit(configuration.agent,rules,['AND','OR']);
        builderTriggerInit(configurationAction.agent, rules_trigger_action);

        $('.field-basenotifications-subscribers').show();
        $('.field-basenotifications-is_system_notification').show();
        $('.field-basenotifications-is_task').show();

        $('#basenotifications-trigger_action option[value="tasks.days_complete"]').prop('disabled', false).show();
        //$('#basenotifications-trigger_action').val('');
    }
    else
    {
        //builderInit(configuration.contact,rules,['AND','OR']);
        builderTriggerInit(configurationAction.contact, rules_trigger_action);

        $('.field-basenotifications-subscribers').hide();
        $('.field-basenotifications-is_system_notification').hide();
        $('.field-basenotifications-is_task').hide();

        $('#basenotifications-trigger_action option[value="tasks.days_complete"]').prop('disabled', true).hide();
        //$('#basenotifications-trigger_action').val('');
    }

    $('#basenotifications-module').on('change',function () {
        removeBuilderTrigger();
        removeBuilder();

        if ($(this).val() === 'agents') {
            //builderInit(configuration.agent,'',['AND','OR']);
            builderTriggerInit(configurationAction.agent, rules_trigger_action);

            $('.field-basenotifications-subscribers').show();
            $('.field-basenotifications-is_system_notification').show();
            $('.field-basenotifications-is_task').show();

            //$('#basenotifications-trigger_action option[value="tasks.days_complete"]').prop('disabled', false).show();
            //$('#basenotifications-trigger_action').val('');
        }
        else {
            //builderInit(configuration.contact,'',['AND','OR']);
            builderTriggerInit(configurationAction.contact, rules_trigger_action);

            $('.field-basenotifications-subscribers').hide();
            $('.field-basenotifications-is_system_notification').hide();
            $('.field-basenotifications-is_task').hide();

            //$('#basenotifications-trigger_action option[value="tasks.days_complete"]').prop('disabled', true).hide();
            //$('#basenotifications-trigger_action').val('');
        }

        $('#builder_trigger .group-actions').hide();
        $('#builder_trigger .rules-group-header').hide();
        $('#builder_trigger .rule-actions').hide();
    });

    if ($('.field-basenotifications-is_email').is(':checked')) {

    }

    // init and change type send
    check_checked($('.field-basenotifications-is_email'), '.field-base-notifications-subject,.field-base-notifications-body');
    check_checked($('.field-basenotifications-is_system_notification'), '.field-base-notifications-system_notification');
    check_checked($('.field-basenotifications-is_task'), '.field-base-notifications-task_name,.field-base-notifications-task_body');
    $('.field-basenotifications-is_email').on('change', function(){ check_checked($(this), '.field-base-notifications-subject,.field-base-notifications-body'); });
    $('.field-basenotifications-is_system_notification').on('change', function(){ check_checked($(this), '.field-base-notifications-system_notification'); });
    $('.field-basenotifications-is_task').on('change', function(){ check_checked($(this), '.field-base-notifications-task_name,.field-base-notifications-task_body'); });



    $('#builder_trigger .group-actions').hide();
    $('#builder_trigger .rules-group-header').hide();
    $('#builder_trigger .rule-actions').hide();

    /*$('#builder .group-conditions .btn-primary').removeClass('disabled');
    $('#builder .group-conditions input').prop('disabled', false);*/


    if($('#basenotifications-trigger').val() == 0) {
        $('#basenotifications-trigger_time').hide();
        $('#basenotifications-trigger_day').hide();
    } else if($('#basenotifications-trigger').val() == 1) {
        $('#basenotifications-trigger_time').show();
        $('#basenotifications-trigger_day').hide();
    } else if($('#basenotifications-trigger').val() == 2) {
        $('#basenotifications-trigger_time').show();
        $('#basenotifications-trigger_day').show();
    }


    $('#basenotifications-trigger_time').datetimepicker({format:'HH:mm:00'});
    $('#basenotifications-trigger').on('change',function () {
        //console.log($(this).val());

        if($(this).val() == 0) {
            $('#basenotifications-trigger_time').hide();
            $('#basenotifications-trigger_day').hide();
        } else if($(this).val() == 1) {
            $('#basenotifications-trigger_time').show();
            $('#basenotifications-trigger_day').hide();
        } else if($(this).val() == 2) {
            $('#basenotifications-trigger_time').show();
            $('#basenotifications-trigger_day').show();
        }
    });
}

var lang_default;
$(document).ready(function () {
    lang_default =  $('select#lang-list').val();
    //configuration.orders = $('.condition-orders').attr('data-condition-orders'); 
    //console.log(configuration);

    initBuilderBlock(lang_default);
});


