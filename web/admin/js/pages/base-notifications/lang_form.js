/**
 * Created by zapleo on 31.10.17.
 */

function formatPerm(perm)
{
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    markup += '<div style="font-size: small">Original: '+perm.original_name+' Code: <b>'+perm.id+'</b></div>';
    return markup;
}

function formatPermSel (perm)
{
    if (perm.name == undefined)
        return perm.text;

    return perm.name + '('+perm.original_name+')';
}

function get_field_value(lang_iso)
{
    return $.when($.ajax({
        url: baseUrl + '/ia/base-notifications/get-translate',
        method: 'get',
        data: {
            id: template_id,
            lang: lang_iso
        }
    }));
}

function field_lang_render(lang_iso, value)
{
    if (value == undefined)
        value = false;


    /*var div = '';

    div += '<div class="lang" data-lang="'+lang_iso+'"><div class="form-group field-base-notifications-name">';
    div += '<label class="control-label" for="base-notifications-name">Name</label>';
    div += '<input id="base-notifications-name_'+lang_iso+'" class="form-control" name="BaseNotifications[name]['+lang_iso+']" placeholder="Name...">';
    div += '<div class="help-block"></div></div>';

    div += '<div class="form-group field-base-notifications-subject">';
    div += '<label class="control-label" for="base-notifications-subject">Subject</label>';
    div += '<textarea id="base-notifications-subject_'+lang_iso+'" class="form-control" name="BaseNotifications[subject]['+lang_iso+']" rows="2" placeholder="Subject..."></textarea>';
    div += '<div class="help-block"></div></div>';

    div += '<div class="form-group field-templates-body">';
    div += '<label class="control-label" for="base-notifications-body">Body</label>';
    div += '<textarea id="base-notifications-body_'+lang_iso+'" class="form-control" name="BaseNotifications[body]['+lang_iso+']" rows="6" placeholder="Body..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';

    div += '<div class="form-group field-templates-system_notification">';
    div += '<label class="control-label" for="base-notifications-system_notification">System notification</label>';
    div += '<textarea id="base-notifications-system_notification'+lang_iso+'" class="form-control" ' +
        'name="BaseNotifications[system_notification]['+lang_iso+']" rows="6" placeholder="System notification..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';

    div += '<div class="form-group field-templates-sms">';
    div += '<label class="control-label" for="base-notifications-sms">Sms</label>';
    div += '<textarea id="base-notifications-sms'+lang_iso+'" class="form-control" name="BaseNotifications[sms]['+lang_iso+']" rows="6" placeholder="Sms..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';

    div += '<div class="form-group field-templates-task_name">';
    div += '<label class="control-label" for="base-notifications-task_name">Task Name</label>';
    div += '<textarea id="base-notifications-task_name'+lang_iso+'" class="form-control" name="BaseNotifications[task_name]['+lang_iso+']" rows="1" placeholder="Task Name..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';

    div += '<div class="form-group field-templates-task_body">';
    div += '<label class="control-label" for="base-notifications-task_body">Task Body</label>';
    div += '<textarea id="base-notifications-task_body'+lang_iso+'" class="form-control" name="BaseNotifications[task_body]['+lang_iso+']" rows="6" placeholder="Task Body..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';*/

    var $template = $('div.lang:first').clone();
    $template.attr('data-lang', lang_iso);


    var editors = [
        'BaseNotifications[subject]',
        'BaseNotifications[body]',
        'BaseNotifications[system_notification]'
    ];

    $template.find('.form-group').each(function(){
        var $block = $(this);

        $block.find('input,textarea').each(function(){

            var $field = $(this);
            var attr_name = $field.attr('name');
            var new_attr_name = attr_name.replace('[' + lang_default + ']', '[' + lang_iso + ']');
            var id = $field.attr('id');

            if (typeof id != 'undefined' && id.indexOf('__') != -1) {
                id = id.split('__');
                id = id[0];
            }

            var lang_id = id + '__' + lang_iso;
            $field.attr('id', lang_id);

            if (new_attr_name != attr_name) {
                $field.attr('name', new_attr_name).val('');
            }

            // для textarea почистить лишнее и переинициализировать
            $.each(editors, function(ind, val){
                if (attr_name.indexOf(val) == 0) {
                    var id = $field.attr('id');

                    if (id.indexOf('__') != -1) {
                        id = id.split('__');
                        id = id[0];
                    }

                    $block.find('#cke_' + id).remove();
                    $field.html('');
                    $field.attr('style', '');

                    var lang_id = id + '__' + lang_iso;
                    $field.attr('id', lang_id);
                }
            });
        });
    });


    $('div.lang').hide();
    var $cur_lang = $('div.lang[data-lang='+lang_iso+']');

    if ($cur_lang.length == 0) {
        $('div.lang:last').after($template);
        $cur_lang = $('div.lang[data-lang='+lang_iso+']');
    }

    $cur_lang.show();

    if (lang_default == lang_iso)
        return;

    var field_value_promise = false;

    if (value)
    {
        field_value_promise = get_field_value(lang_iso);

        get_field_value(lang_iso).then(function ($result) {
            $.each($result, function(field, val){
               var id =  'base-notifications-' + field + '__'+lang_iso;
               if ($('#' + id).length == 0) return;

                $('#' + id).val(val);

               if (id.indexOf('-subject__') == -1 && id.indexOf('-system_notification__') == -1 && id.indexOf('-body__') == -1)
                   return;

                initCKEditor(id);
            });
        })
    }
}

function get_shortcodes(handler, info) {
    $.ajax({
        url: baseUrl + '/ia/base-notifications/get-shortcodes' + (info ? '&info=1' : ''),
        dataType: 'json',
        data: {
            handler: handler
        },
        success: function (data) {
            set_placeholders(data);

            $.each(CKEDITOR.instances, function(ins_name, instance) {
                instance.destroy();
                initCKEditor(ins_name);
            });
        },
        error: function () {
            alert('Неудалось получить данные!');
        }
    });
}

$('document').ready(function () {

    initCKEditor('base-notifications-subject');
    initCKEditor('base-notifications-body');
    initCKEditor('base-notifications-system_notification');

    $('#lang-list').select2({
        ajax:{
            url: baseUrl + '/ia/base-notifications/get-languages',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['iso'], name: data[i]['name'], original_name: data[i]['original_name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose language',
        allowClear: false,
        width: '50%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });

    $('select#lang-list[data-action="create"]').change(function() {
        if ($(this).val() != null && $('.lang[data-lang="'+$(this).val()+'"]').length == 0)
            field_lang_render($(this).val());

        if ($('.lang[data-lang="'+$(this).val()+'"]').length > 0) {
            $('div.lang').hide();
            $('.lang[data-lang="'+$(this).val()+'"]').show();
        }
    });

    $('select#lang-list[data-action="update"]').change(function() {
        if ($(this).val() != null && $('.lang[data-lang="'+$(this).val()+'"]').length == 0)
            field_lang_render($(this).val(), 1);

        if ($('.lang[data-lang="'+$(this).val()+'"]').length > 0) {
            $('div.lang').hide();
            $('.lang[data-lang="'+$(this).val()+'"]').show();
        }
    });

    $('select#handlers').change(function() {
        var data = $(this).select2('data')[0];

        get_shortcodes(data.id);
    });

});