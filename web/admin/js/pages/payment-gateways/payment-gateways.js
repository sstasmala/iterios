/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$('#country-select').each(function(){
    let parentElement = $(this).parent();
    $('#country-select').select2({
        ajax: {
            url: baseUrl + '/ia/payment-gateways/get-countries-options',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                let query = {
                    search: params.term,
                    type: 'public',
                    _csrf: $('meta[name="csrf-token"]').attr('content')
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                let array = [];
                for (let i in data){
                    array.push({id: data[i]['id'], text: data[i]['title']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Country'
    });
}).on('change',function () {
    let val = $(this).val();
    $('input[name="PaymentGateways[countries]"]').val(JSON.stringify(val));
});
$('#currency-select').each(function(){
    let parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/payment-gateways/get-currencies',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                let query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                let array = [];
                console.log(data);
                for (let i in data){
                    array.push({id: data[i]['id'], text: data[i]['iso_code']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Currency'
    });
}).on('change',function () {
    let val = $(this).val();
    $('input[name="PaymentGateways[currencies]"]').val(JSON.stringify(val));
});


function loadConfig() {
    $.ajax({
        url: baseUrl + '/ia/payment-gateways/get-provider-config',
        method: 'GET',
        data: {
            provider: $('#paymentgateways-provider').val()
        },
        success:function (response) {
           let editor =  $('#paymentgateways-provider_config').data('json-editor-name');
           window[editor].set(response);
        }
    })

}

function changeType(val) {
    val += '';
    switch (val) {
        case '0':
            $('#paymentgateways-provider').closest('.row').show();
            $('#paymentgateways-provider_config').closest('.field-paymentgateways-provider_config').show();
            $('#document-select').closest('.row').hide();
            if($('#paymentgateways-provider_config').val()=='')
                loadConfig();
        break;
        case '1':
        case '2':
            $('#paymentgateways-provider').closest('.row').hide();
            $('#paymentgateways-provider_config').closest('.field-paymentgateways-provider_config').hide();
            $('#document-select').closest('.row').show();
            break;
    }
}



changeType($('#paymentgateways-type').val());
$('#paymentgateways-type').on('change',function () {
    console.log($(this).val());
    changeType($(this).val());
});

$('#paymentgateways-provider').on('change',function () {
    loadConfig();
});

$('#document-select').each(function(){
    let parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/payment-gateways/get-documents-templates',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                let query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                let array = [];
                console.log(data);
                for (let i in data){
                    array.push({id: data[i]['id'], text: data[i]['title']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select template'
    });
})