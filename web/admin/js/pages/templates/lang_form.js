/**
 * Created by zapleo on 31.10.17.
 */

function formatPerm(perm)
{
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    markup += '<div style="font-size: small">Original: '+perm.original_name+' Code: <b>'+perm.id+'</b></div>';
    return markup;
}

function formatPermSel (perm)
{
    if (perm.name == undefined)
        return perm.text;

    return perm.name + '('+perm.original_name+')';
}

function get_field_value(lang_iso)
{
    return $.when($.ajax({
        url: baseUrl + '/ia/templates/get-translate',
        method: 'get',
        data: {
            id: template_id,
            lang: lang_iso
        }
    }));
}

function field_lang_render(lang_iso, value)
{
    if (value == undefined)
        value = false;

    var div = '';

    div += '<div class="lang" data-lang="'+lang_iso+'"><div class="form-group field-templates-name">';
    div += '<label class="control-label" for="templates-name">Name</label>';
    div += '<input id="templates-name_'+lang_iso+'" class="form-control" name="Templates[name]['+lang_iso+']" placeholder="Name...">';
    div += '<div class="help-block"></div></div>';

    div += '<div class="form-group field-templates-subject">';
    div += '<label class="control-label" for="templates-subject">Subject</label>';
    div += '<textarea id="templates-subject_'+lang_iso+'" class="form-control" name="Templates[subject]['+lang_iso+']" rows="2" placeholder="Subject..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '<div class="form-group field-templates-body">';
    div += '<label class="control-label" for="templates-body">Body</label>';
    div += '<textarea id="templates-body_'+lang_iso+'" class="form-control" name="Templates[body]['+lang_iso+']" rows="6" placeholder="Template..."></textarea>';
    div += '<div class="help-block"></div></div>';
    div += '</div>';

    $('div.lang:last').hide().after(div);

    var field_value_promise = false;

    if (value)
    {
        field_value_promise = get_field_value(lang_iso);
        get_field_value(lang_iso).then(function ($result) {
            $('#templates-name_'+lang_iso).val($result['name']);
        })
    }


    initCKEditor('templates-subject_'+lang_iso, field_value_promise);
    initCKEditor('templates-body_'+lang_iso, field_value_promise);
}

function get_shortcodes(handler, info) {
    $.ajax({
        url: baseUrl + '/ia/templates/get-shortcodes' + (info ? '&info=1' : ''),
        dataType: 'json',
        data: {
            handler: handler
        },
        success: function (data) {
            set_placeholders(data);

            $.each(CKEDITOR.instances, function(ins_name, instance) {
                instance.destroy();
                initCKEditor(ins_name);
            });
        },
        error: function () {
            alert('Неудалось получить данные!');
        }
    });
}

$('document').ready(function () {

    initCKEditor('templates-subject');
    initCKEditor('templates-body');

    $('#lang-list').select2({
        ajax:{
            url: baseUrl + '/ia/templates/get-languages',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['iso'], name: data[i]['name'], original_name: data[i]['original_name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose language',
        allowClear: false,
        width: '50%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });

    $('select#lang-list[data-action="create"]').change(function() {
        if ($(this).val() != null && $('.lang[data-lang="'+$(this).val()+'"]').length == 0)
            field_lang_render($(this).val());

        if ($('.lang[data-lang="'+$(this).val()+'"]').length > 0) {
            $('div.lang').hide();
            $('.lang[data-lang="'+$(this).val()+'"]').show();
        }
    });

    $('select#lang-list[data-action="update"]').change(function() {
        if ($(this).val() != null && $('.lang[data-lang="'+$(this).val()+'"]').length == 0)
            field_lang_render($(this).val(), 1);

        if ($('.lang[data-lang="'+$(this).val()+'"]').length > 0) {
            $('div.lang').hide();
            $('.lang[data-lang="'+$(this).val()+'"]').show();
        }
    });

    $('select#handlers').change(function() {
        var data = $(this).select2('data')[0];

        get_shortcodes(data.id);
    });

});