function preview(data) {
    var iWidth = 640,
        // 800 * 0.8,
        iHeight = 420,
        // 600 * 0.7,
        iLeft = 80; // (800 - 0.8 * 800) /2 = 800 * 0.1.
    try {
        var screen = window.screen;
        iWidth = Math.round( screen.width * 0.8 );
        iHeight = Math.round( screen.height * 0.7 );
        iLeft = Math.round( screen.width * 0.1 );
    } catch ( e ) {}

    var sOpenUrl = '';

    var oWindow = window.open( sOpenUrl, null, 'toolbar=yes,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=' +
        iWidth + ',height=' + iHeight + ',left=' + iLeft );

    var doc = oWindow.document;
    doc.open();
    doc.write( data );
    doc.close();
}

$('document').ready(function () {
    $('a#view_body').click(function (e) {
        e.preventDefault();
        console.log(baseUrl + '/ia/templates/get-template-body');
        $.ajax({
            url: baseUrl + '/ia/templates/get-template-body',
            dataType: 'json',
            data: {
                id: template_id,
                lang: $('select#lang-list').val()
            },
            success: function (data) {
                preview(data);
            },
            error: function () {
                alert('Неудалось получить данные!');
            }
        });
    });
});