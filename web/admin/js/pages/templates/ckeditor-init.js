/**
 * Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
var default_placeholders = [];

function set_placeholders(data) {
    CKEDITOR.config.placeholder_select = {
        placeholders: data,
        format: '{{%placeholder%}}'
    };
}

if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
    CKEDITOR.tools.enableHtml5Elements(document);

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 400;
CKEDITOR.config.width = 'auto';
CKEDITOR.config.extraPlugins = 'preview,docprops';
CKEDITOR.config.fullPage = true;
CKEDITOR.config.allowedContent = true;

set_placeholders(default_placeholders);

CKEDITOR.config.removeButtons = 'Save';

function initCKEditor(id, field_value_promise) {
    var config = CKEDITOR.tools.prototypedCopy(CKEDITOR.config);
        config.field_value_promise = false;
        config.editor_system = true;

    if (id.indexOf('subject') >= 0) {
        config.height = 50;
        config.toolbar = [['Preview'], ['placeholder_select']];
        config.basicEntities = false;
        config.editor_system = false;
    }

    if (field_value_promise)
        config.field_value_promise = field_value_promise;

    init(id, config)();

    CKEDITOR.config.promisePreview = function (editor) {
        return $.when($.ajax({
            url: baseUrl + '/ia/templates/get-preview',
            method: 'POST',
            data: {
                text: editor.getData(),
                handler: $('select#handlers').val()
            }
        }));
    }
}

function init(id, config) {
    var wysiwygareaAvailable = isWysiwygareaAvailable();

    return function () {
        var editorElement = CKEDITOR.document.getById(id);

        if (config.field_value_promise) {
            config.field_value_promise.done(function (data) {
                editorElement.setHtml((config.editor_system ? data.body : data.subject));
                editor_replace(editorElement);
            });
        } else {
            editor_replace(editorElement);
        }
    };

    function editor_replace(editorElement) {
        // Depending on the wysiwygare plugin availability initialize classic or inline editor.
        if (wysiwygareaAvailable) {
            CKEDITOR.replace(id, config);
        } else {
            editorElement.setAttribute('contenteditable', 'true');
            CKEDITOR.inline(id, config);
        }
    }

    function isWysiwygareaAvailable () {
        // If in development mode, then the wysiwygarea must be available.
        // Split REV into two strings so builder does not replace it :D.
        if (CKEDITOR.revision == ('%RE' + 'V%')) {
            return true;
        }

        return !!CKEDITOR.plugins.get( 'wysiwygarea' );
    }
}