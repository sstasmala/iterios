/**
 * index.js
 * @copyright © Iterios
 * @author Vlad Savelyev vladsova777@gmail.com
 */

$('document').ready(function () {

    $('#requisites-field-id').select2({
        ajax:{
            url: baseUrl + '/ia/requisites-sets/get-tenants',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], name: data[i]['name'], type: data[i]['name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose Tenant',
        allowClear: false,
        width: '50%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatField,
        templateSelection: formatFieldSel
    });

});

function formatField(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    return markup;
}

function formatFieldSel (perm) {
    if (perm.name == undefined)
        return perm.text;


    return 'Tenant name: '+ perm.name;
}

