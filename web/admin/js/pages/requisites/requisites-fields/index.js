/**
 * index.js
 * @copyright © Iterios
 * @author Vlad Savelyev vladsova777@gmail.com
 */
var link_template = '';
var select_template = '';


/**
 *
 */
function renderDefault() {
    $('.variants-box').empty();
}

/**
 *
 */
function renderLink()
{
    var input_value  = $('#requisitesfields-variants').val();

    console.log(input_value);
    if(input_value != '')
        input_value = JSON.parse(input_value);
    else
        input_value = {table:''};

    $.ajax({
        url: baseUrl + '/tariff-fields/get-tables-list',
        method: 'get',
        success: function (response) {
            var container = $('.variants-box');
            container.empty();

            var selected;

            if(input_value.table=='')
                selected = response[0];
            else
                selected = input_value.table;

            console.log(selected);
            var template =
                '<div class="form-group">'+
                '<label for="tarifffields-variant-link" class="control-label">Link to reference</label>'+
                '<select id="tarifffields-variant-link" class="form-control">';
            for(var i = 0; i < response.length; i++)
            {
                template += '<option value="'+response[i] +'"';
                if(response[i] == selected)
                    template += ' selected';
                template += '>'+response[i]+'</option>'
            }

            template += '</select></div>';

            container.append(template);

            var selector = $('#tarifffields-variant-link');
            selector.off('change');
            selector.on('change',function () {
                $('#requisitesfields-variants').val(JSON.stringify({table:$(this).val()}));
            });
        }
    })
}

function updateVariant() {

    var data = [];
    $('.variants-element>input').each(function () {
        data.push($(this).val())
    });

    $('#requisitesfields-variants').val(JSON.stringify(data));
}

/**
 *
 */
function renderSelection() {
    var input_value  = $('#requisitesfields-variants').val();

    if(input_value != '')
        input_value = JSON.parse(input_value);
    else
        input_value = [];

    var container = $('.variants-box');
    container.empty();

    var template = '<div class="form-group">'+
        '<label>Variants</label>'+
        '<div class="variants-list">';

    for(var i = 0; i < input_value.length; i++)
    {
        template += '<div class="variants-element">'+
            '<input  type="text" placeholder="Value" class="form-control .selection-variant"'+
            ' value="'+input_value[i]+'"'+
            '>'+'<button class="btn btn-danger remove-element">-</button>'+'</div>'
    }

    template += '</div>'+'<button class="btn btn-success add-element">+</button>';
    template += '</div>';

    container.append(template);

    //container events

    $('.add-element').on('click',function (e) {
        e.stopPropagation();
        e.preventDefault();
        var tmp = '<div class="variants-element">'+
            '<input  type="text" placeholder="Value" class="form-control .selection-variant"'+
            ' value="">'+'<button class="btn btn-danger remove-element">-</button>'+'</div>';
        $('.variants-list').append(tmp);

        var elements = $('.remove-element');
        elements.off('click');
        elements.on('click',function (e) {
            console.log('remove');
            e.stopPropagation();
            e.preventDefault();
            $(this).parent().remove();
        });

        var inputs = $('.variants-element>input');
        inputs.off('keyup');
        inputs.on('keyup',function () {
            updateVariant();
        });
    });

    var elements = $('.remove-element');
    elements.off('click');
    elements.on('click',function (e) {
        console.log('remove');
        e.stopPropagation();
        e.preventDefault();
        $(this).parent().remove();
        updateVariant();
    });

    var inputs = $('.variants-element>input');
    inputs.off('keyup');
    inputs.on('keyup',function () {
        updateVariant();
    });
}

function renderVariants(variant) {

    switch (variant)
    {
        // case '3': renderLink();break;
        case '5':
        case '9':
        case '6': renderSelection();break;
        default: renderDefault();break;
    }
}

$('document').ready(function () {

    var types = $('.type-selection');
    types.on('change',function () {
        $('#requisitesfields-variants').val('[]');
        renderVariants($(this).val());
    });

    renderVariants(types.val());
});