/**
 * index.js
 * @copyright © Iterios
 * @author Vlad Savelyev vladsova777@gmail.com
 */

$('document').ready(function () {
    $( function() {
        $( "#sortable" ).sortable({items: "> ul"});
        $( "#sortable" ).disableSelection();
    } );
    var sortableId = 0;

    for (id in sortableIds) {
        var str = '#sortable' + id;
        $( str ).sortable({items: "> li"});
        sortableId = id;
    }
    var addFieldBlock = $('#sortable');
    sortableId++;

    $('.remove-group').off('click').on('click',function () {
        $(this).parent().parent().remove();
    });

    $('#add-country').click(function () {
        $('#add-country').attr('disabled',true);
        var ulVal = Math.round(Math.random() * (777777 - 1 )) + 1;

        var groupValue = $('#requisites-field-id :selected').val();
        var groupName = $('#select2-requisites-field-id-container').text();

        if(groupValue=== undefined) return;
        if($('.ui-sortable[data-group-id="'+groupValue+'"]').length>0) {
            $('#add-country').attr('disabled',false);
            return;
        }
        var groupNameFormat = groupName.replace(' ','')
        var selectBlock = '<ul id="sortable'+sortableId+'"' +' data-group-id="'+groupValue+'" class="ui-sortable ui-state-default ui-sortable-'+ulVal+'">' +
            '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' +
            '<input type="hidden" name="Requisites[requisites_group_id][]" value="'+groupValue+'">' +
            '<span class="group-name"> Group Name: '+groupName+' </span>' +
            '<span style="float: right"><button class="btn btn-danger remove-group">X</button></span>'+
            '</ul>';


        var str = '#sortable' + sortableId; // id for sortable group (ul)
        $( str ).sortable({items: "> li"});

        sortableId++;
        $.ajax({
            url: baseUrl + '/ia/requisites/get-fields',
            dataType: 'json',
            type : "POST",
            data : {id : groupValue},
            success: function (data) {
                fields = '';
                data.forEach(function (item) {
                    fields += '<li class="nested-field"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+item.name+'';
                    fields += '<input type="hidden" name="Requisites[requisites_field_id]['+groupValue+'][]" value="'+item.id+'">' +
                        '<input type="hidden" name="Requisites[in_header]['+groupValue+']['+item.id+']" value="0"><label>' +
                        '<input type="checkbox" name="Requisites[in_header]['+groupValue+']['+item.id+']" value="1" aria-invalid="false"> In Header</label>';
                    fields += '</li>';
                });
                addFieldBlock.append(selectBlock);
                $('.remove-group').off('click').on('click',function () {
                   $(this).parent().parent().remove();
                });
                $('.ui-sortable-'+ulVal+'').append(fields).sortable({items: "> li"});
                $('#add-country').attr('disabled',false);
            },
            error: function (err) {
                console.log(err);
                $('#add-country').attr('disabled',false);
            }
        });
    });

    $('#form-submit').click(function () {
        $('#to-form').append($('#add-field-block'));
    });

    $('#country-select').each(function(){
        var parentElement = $(this).parent();
        $('#country-select').select2({
            ajax: {
                url: baseUrl + '/ita/' + 1 + '/contacts/get-countries-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public',
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    };
                    return query;
                },
                delay: 1000,
                minimumInputLength: 0,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['title']});
                    }
                    return {
                        results: array
                    }
                }
            },
            width: '100%',
            dropdownParent: parentElement,
            placeholder: 'Select Country'
        });
    });

    $('#requisites-field-id').select2({
        ajax:{
            url: baseUrl + '/ia/requisites/get-groups',
            dataType: 'json',
            data: function (params) {
                var query = {
                    name: params.term
                };
                return query;
            },
            delay:1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data)
                    array.push({id: data[i]['id'], name: data[i]['name'], type: data[i]['name']})

                return {
                    results: array
                }
            }
        },
        placeholder: 'Choose Group',
        width: '100%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatField,
        templateSelection: formatFieldSel
    });
});

function formatField(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '<div style="font-size: large"><b>'+perm.name+'</b></div>';
    return markup;
}

function formatFieldSel (perm) {
    if (perm.name == undefined) {
        return perm.text;
    }

    return perm.name;
}


