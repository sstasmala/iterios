/**
 * add_to_tenant.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }

    var markup = '<div style="font-size: large"><b>' + perm.name + '</b></div>';
    markup += '<div style="font-size: small">';
    markup += perm.email + '</div>';

    return markup;
}

function formatPermSel (perm) {
    return perm.name || perm.text;
}

function add_user(tenant_id, user_id) {
    $.ajax({
        url: baseUrl + '/ia/users/add-to-tenant?id=' + tenant_id + '&user_id=' + user_id,
        method: 'get',
        success: function (response) {
            setTimeout(function() {
                location.reload();
            }, 1000);
        },
        error: function (response) {
            console.log('User do not add to tenant!');
        }
    });
}

$('document').ready(function () {
    $('#users-list').select2({
        ajax:{
            url: baseUrl + '/ia/tenants/get-users',
            dataType: 'json',
            data: function (params) {
                var query = {
                    tenant_id: $('#addUserModal').find('input[name="tenant_id"]').val(),
                    name: params.term
                };

                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data) {
                    array.push({
                        id: data[i]['id'],
                        name: data[i]['name'],
                        email: data[i]['email']
                    });
                }

                return {
                    results: array
                }
            }
        },
        width: '100%',
        allowClear: false,
        escapeMarkup: function (markup) {
            return markup;
        },
        placeholder: 'Choose user',
        templateResult: formatPerm,
        templateSelection: formatPermSel,
        dropdownParent: $('#addUserModal')
    });
    $('#addUserModal button#addToTenant').on('click',function () {
        $('#addUserModal').modal('hide');

        var user = $('#users-list').select2('data');
        var user_id = (user.length > 0 ? user[0].id : 0);
        var tenant_id = $('#addUserModal').find('input[name="tenant_id"]').val();

        if (user_id != 0)
            add_user(tenant_id, user_id);
    });

});