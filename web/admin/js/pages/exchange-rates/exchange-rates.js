/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$('#currency-select').each(function(){
    var parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/exchange-rates/get-currencies',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];
                console.log(data);
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['iso_code']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Currency'
    });
}).on('change',function () {
    var val = $(this).val();
    // console.log(val);
    // console.log($(this).find('option:selected').text());
    $('input[name="ExchangeRates[currency_id]"]').val(val);
    $('input[name="ExchangeRates[currency_iso]"]').val($(this).find('option:selected').text());
    // $('input[name="Tariffs[country]"]').val(JSON.stringify(val));
});

$('#exchangerates-rate').inputmask({
    // "mask": "9{+}.99"
    "regex": '^\\d+(\\.\\d{1,2})?$'
});