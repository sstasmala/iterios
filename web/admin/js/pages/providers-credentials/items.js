$(document).ready(function () {

    $('#providerscredentials-provider_id').on('change', function () {
        var request = $.ajax({
            url: baseUrl + '/ia/providers-credentials/load-params',
            dataType: 'json',
            method: "GET",
            data: {
                provider_id: $(this).val(),
            },
            success: function (data) {
                var variant = JSON.parse(data['additional_config']);
                // if (variant == null) {
                //     $('.set_changed').html('');
                //     return [];
                // }
                var UserPass = '' +
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="providerscredentials-params-username">Username</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="providerscredentials-params-username" class="form-control" name="ProvidersCredentials[credentialParams][username]">' +
                    '</div>\n' +
                    '</div>\n' +
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="providerscredentials-params-password">Password</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="providerscredentials-params-password" class="form-control" name="ProvidersCredentials[credentialParams][password]">' +
                    '</div>\n' +
                    '</div>\n';

                var api_token = '' +
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="providerscredentials-params-apitoken">Api token</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="providerscredentials-params-apitoken" class="form-control" name="ProvidersCredentials[credentialParams][apiKey]">' +
                    '</div>\n' +
                    '</div>\n';

                if (data['authorization_type'] == 'login&pass') {
                    $('#params-body').html(UserPass);
                } else if (data['authorization_type'] == 'api-token') {
                    $('#params-body').html(api_token);
                } else if (data['authorization_type'] == '') {
                    $('#params-body').html('');
                }

                $('.set_changed').html('');
                var alpha_name,ip,val;
                for (prop in variant) {
                    val = variant[prop];

                    alpha_name = '' +
                        '<div class="form-group">\n' +
                        '<label class="control-label col-md-2" for="providerscredentials-params-alphaname">Alpha name</label>\n' +
                        '<div class="col-md-10">\n' +
                        '<input type="text" id="providerscredentials-params-alphaname" class="form-control" name="ProvidersCredentials[credentialParams][alphaName]">' +
                        '</div>\n' +
                        '</div>\n';

                    ip = '' +
                        '<div class="form-group">\n' +
                        '<label class="control-label col-md-2" for="providerscredentials-params-ip">Ip</label>\n' +
                        '<div class="col-md-10">\n' +
                        '<input type="text" id="providerscredentials-params-ip" class="form-control" name="ProvidersCredentials[credentialParams][ip]">' +
                        '</div>\n' +
                        '</div>\n';

                    if (val == 'alphaName') {
                        $('#params-body2').addClass('set_changed').html(alpha_name);
                    } else if (val == '') {
                        $('#params-body2').addClass('set_changed').html('');
                    }

                    if (val == 'ip') {
                        $('#params-body3').addClass('set_changed').html(ip);
                    } else if (val == '') {
                        $('#params-body3').addClass('set_changed').html('');
                    }
                }
            },
        });
    });

});