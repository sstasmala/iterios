/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$('#customer-select').each(function(){
    var parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/financial-operations/get-customer',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];
                console.log(data);
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['name']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Customer'
    });
});

$('#currency-select').each(function() {
    var parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/exchange-rates/get-currencies',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data) {
                var array = [];
                console.log(data);
                for (var i in data) {
                    array.push({id: data[i]['id'], text: data[i]['iso_code']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Currency'
    });
}).on('change',function () {
    calculateSum();
});


function calculateSum() {
    var value = $('#financialoperations-sum').val();
    var iso = $('#currency-select').find('option:selected').text();

    if(value === undefined || value == '')
        value = 0;
    var data = {};
    if(iso === undefined || iso == '')
        data = {value:value};
    else
        data = {value:value,iso:iso};

    $.ajax({
        url:baseUrl + '/ia/financial-operations/exchange-rate',
        method:'GET',
        data:data,
        success:function (response) {
            $('.field-financialoperations-sum_usd').find('.form-control-static').html(parseFloat(response).toFixed(2));
        }
    });
}


$('#payment-gateways-select').each(function(){
    var parentElement = $(this).parent();
    $(this).select2({
        ajax: {
            url: baseUrl + '/ia/financial-operations/get-payment-gateways',
            method: 'Get',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                };
                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];
                console.log(data);
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['name']});
                }
                return {
                    results: array
                }
            }
        },
        width: '100%',
        dropdownParent: parentElement,
        placeholder: 'Select Payment Gateway'
    });
});
$('#financialoperations-sum').inputmask({
    "regex": '^\\d+(\\.\\d{1,2})?$'
}).on('keyup',function () {
    calculateSum();
});

function checkMethod(id)
{
    var tr = $('#financialoperations-transaction_number');
    var acc = $('#financialoperations-account_number');
    tr.closest('.row').hide();
    acc.closest('.row').hide();

    switch (id)
    {
        case '0':acc.closest('.row').show();tr.val('');break;
        case '1':tr.closest('.row').show();acc.val('');break;
        default:tr.val('');acc.val('');break;
    }
}
var method = $('#financialoperations-payment_method');
checkMethod(method.val());
method.on('change',function () {
    checkMethod($(this).val());
});