
$('document').ready(function () {


    $('#HolidaysMounts').on('change', function() {
        $('#HolidaysDay').find('option').remove();
        for (var i = 1; i <= dayMounth[$(this).val()-1]; i++)
            $('#HolidaysDay').append('<option value="' + i + '">' + i + '</option>');
    });

    $('#search_country').select2({
        ajax: {
            url: baseUrl + '/ia/system-holidays/get-countries-options',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            delay: 1000,
            processResults: function (data)
            {
                var array = [];
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['title']});
                }
                return {
                    results: array
                }
            }
        },
        language: tenant_lang,
        width: '100%',
        placeholder: 'Select country',
        tags: true
    });

})


