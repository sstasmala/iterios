/**
 * add_to_tenant.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }

    var markup = '<div style="font-size: large"><b>' + perm.name + '</b></div>';
    markup += '<div style="font-size: small">';
    markup += '<b>Owner:</b> ' + perm.owner + ' ';
    markup += '<b>Language:</b> ' + perm.language + '</div>';

    return markup;
}

function formatPermSel (perm) {
    return perm.name || perm.text;
}

function add_to_tenant(tenant_id, user_id) {
    $.ajax({
        url: baseUrl + '/ia/users/add-to-tenant?id=' + tenant_id + '&user_id=' + user_id,
        method: 'get',
        success: function (response) {
            setTimeout(function() {
                location.reload();
            }, 1000);
        },
        error: function (response) {
            console.log('User do not add to tenant!');
        }
    });
}

$('document').ready(function () {

    $('#tenants-list').select2({
        ajax:{
            url: baseUrl + '/ia/users/get-tenants',
            dataType: 'json',
            data: function (params) {
                var query = {
                    user_id: $('#addTenantModal').find('input[name="user_id"]').val(),
                    name: params.term
                };

                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data) {
                    array.push({
                        id: data[i]['id'],
                        name: data[i]['name'],
                        language: data[i]['language'],
                        owner: data[i]['owner']
                    });
                }

                return {
                    results: array
                }
            }
        },
        width: '100%',
        allowClear: false,
        escapeMarkup: function (markup) {
            return markup;
        },
        placeholder: 'Choose tenant',
        templateResult: formatPerm,
        templateSelection: formatPermSel,
        dropdownParent: $('#addTenantModal')
    });

    $('#addTenantModal button#addToTenant').on('click',function () {
        $('#addTenantModal').modal('hide');

        var tenant = $('#tenants-list').select2('data');
        var tenant_id = (tenant.length > 0 ? tenant[0].id : 0);
        var user_id = $('#addTenantModal').find('input[name="user_id"]').val();

        if (tenant_id != 0)
            add_to_tenant(tenant_id, user_id);
    });

});