/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

var index = 0;
var attr = [{field:"name",model:"ServicesFieldsDefault",array:true},{field:"variables",model:"ServicesFieldsDefault",array:true}];



function RenderType(){
    this.type = undefined;
    this.variants_input = undefined;
    this.container = undefined;
    this.lang = undefined;
    this.parent = undefined;

    var Item = this;

    this.removeElement = function (index,count) {
        $(Item.variants_input).each(function (){
            var data = [];
            if($(this).val()!=undefined && $(this).val()!='')
                data = JSON.parse($(this).val());
            if(data.length >=count)
                data.splice(index,1);
            $(this).val(JSON.stringify(data));
        });
    };

    this.updateVariant = function () {

        var data = [];
        // console.log("Container: ");console.log(Item.container);
        Item.container.find('.selection-variant').each(function () {
            data.push($(this).val())
        });
        console.log("Data: ");console.log(data);
        Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val(JSON.stringify(data));
    };

    this.renderSelection = function () {

        var overlay = parent.find('overlay');
        if(overlay.length>0)
            overlay.show();
        var max_count = 0;
        Item.parent.find(Item.variants_input).each(function () {
            var value = $(this).val();
            if(value != '')
                value = JSON.parse(value);
            else
                value = [];
            if(value.length>max_count)
                max_count = value.length;
        });
        // console.log(max_count);
        var input_value  = Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val();

        if(input_value != '' && input_value != undefined)
            input_value = JSON.parse(input_value);
        else
            input_value = [];

        var container = Item.container;
        container.empty();

        var template = '<div class="form-group">'+
            '<label>Variants</label>'+
            '<div class="variants-list">';

        var diff = max_count - input_value.length;
        // console.log(input_value.length);
        if(diff>0){
            for(var j = 0; j < diff; j++)
                input_value.push('');
            Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val(JSON.stringify(input_value));
        }


        for(var i = 0; i < input_value.length; i++)
        {
            template += '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value="'+input_value[i]+'"'+
                '></div>'+'<div class="col-sm-1"><button class="btn btn-danger remove-element">-</button></div>'+'</div>';
        }

        template += '</div>'+'<button class="btn btn-success add-element">+</button>';
        template += '</div>';

        container.append(template);

        if(overlay.length>0)
            overlay.hide();

        //container events

        var component = Item;
        Item.container.find('.add-element').on('click',function (e) {
            e.stopPropagation();
            e.preventDefault();
            var tmp = '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value=""></div>'+'<div class="col-sm-1"><button class="btn btn-danger remove-element">-</button></div>'+'</div>';
            Item.container.find('.variants-list').append(tmp);

            var elements = Item.container.find('.remove-element');
            elements.off('click');
            elements.on('click',function (e) {
                console.log('remove');
                e.stopPropagation();
                e.preventDefault();
                var item = component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val();
                var count = 0;
                if(item != undefined && item != '')
                    count = JSON.parse(component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val()).lenght;
                component.removeElement($(this).closest('.variants-element').index(),count);
                $(this).closest('.variants-element').remove();
                component.updateVariant();
            });

            var inputs = Item.container.find('.selection-variant');
            inputs.off('keyup');
            inputs.on('keyup',function () {
                console.log('changes');
                component.updateVariant();
            });
            component.updateVariant();
        });

        var elements = $('.remove-element');
        elements.off('click');
        elements.on('click',function (e) {
            console.log('remove');
            e.stopPropagation();
            e.preventDefault();
            var item = component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val();
            var count = 0;
            if(item != undefined && item != '')
                count = JSON.parse(component.parent.find(component.variants_input+'[name*="['+component.lang+']"]').val()).lenght;
            component.removeElement($(this).closest('.variants-element').index(),count);
            $(this).closest('.variants-element').remove();
            component.updateVariant();
        });

        var inputs = Item.container.find('.selection-variant');
        inputs.off('keyup');
        inputs.on('keyup',function () {
            component.updateVariant();
        });
    };

    this.renderSwitch = function () {
        var input_value  =  Item.parent.find(Item.variants_input+'[name*="['+Item.lang+']"]').val();
        if(input_value != '' && input_value != undefined)
            input_value = JSON.parse(input_value);
        else
            input_value = ['',''];

        var container = Item.container;
        container.empty();

        var template = '<div class="form-group">'+
            '<label>Variants</label>'+
            '<div class="variants-list">';

        for(var i = 0; i < 2; i++)
        {
            template += '<div class="variants-element row">'+
                '<div class="col-sm-10"><input  type="text" placeholder="Value" class="form-control selection-variant"'+
                ' value="'+input_value[i]+'"'+
                '></div></div>';
        }

        template += '</div>';
        template += '</div>';

        container.append(template);
        var component = Item;
        var inputs = container.find('.variants-element input');
        inputs.off('keyup');
        inputs.on('keyup',function () {
            console.log('changes');
            component.updateVariant();
        });
    }

    this.renderDefault = function () {
        this.container.empty();
    };

    this.renderField = function () {
        switch (Item.type)
        {
            case 'switch':Item.renderSwitch();break;
            case 'radio':
            case 'dropdown':
            case 'multiselect': Item.renderSelection(); break;
            default: Item.renderDefault();break;
        }
    };

    return function (type,variants_input,container,parent,lang) {
        Item.container = parent.find(container);
        Item.lang = lang;
        Item.type = type;
        Item.variants_input = variants_input;
        Item.parent = parent;
        Item.renderField();
        console.log('render');
        console.log(Item.container);
        if(type == 'radio')
            console.log(parent.find(container));
    }
};

function renderField()
{
    var field = '<li class="service-field callout" style="border: 5px solid #eee;background-color: white;display:none"><div class="row">' +
        '<div class="col-md-1"><label class="control-label">Name</label></div><div class="col-md-3">' +
        '<input type="text" class="form-control" name="ServicesFieldsDefault['+index+'][name]" value=""></div>' +
        '<div class="col-md-1"><label class="control-label">Code</label></div><div class="col-md-3">' +
        '<input type="text" class="form-control" name="ServicesFieldsDefault['+index+'][code]" value=""></div>' +
        '<div class="col-md-1"><label class="control-label">Type</label></div><div class="col-md-3">' +
        '<select class="form-control select-type" name="ServicesFieldsDefault['+index+'][type]">';
    $.each(types, function(index, value) {
        field += '<option value="'+index+'">'+value+'</option>';
    });
    field += '</select></div></div><br><div class="row">' +
        '<input type="hidden" name="ServicesFieldsDefault['+index+'][in_header]" value="0"><label class="col-md-2">' +
        '<input type="checkbox" name="ServicesFieldsDefault['+index+'][in_header]" value="1"  aria-invalid="false"> In Header</label>' +
        '<input type="hidden" name="ServicesFieldsDefault['+index+'][is_additional]" value="0"><label class="col-md-2">' +
        '<input type="checkbox" name="ServicesFieldsDefault['+index+'][is_additional]" value="1" aria-invalid="false"> Is Additional</label>' +
        '<input type="hidden" name="ServicesFieldsDefault['+index+'][only_for_order]" value="0"><label class="col-md-2">' +
        '<input type="checkbox" name="ServicesFieldsDefault['+index+'][only_for_order]" value="1" aria-invalid="false"> Only for order</label>' +
        '<input type="hidden" name="ServicesFieldsDefault['+index+'][in_creation_view]" value="0"><label class="col-md-2">' +
        '<input type="checkbox" name="ServicesFieldsDefault['+index+'][in_creation_view]" value="1" aria-invalid="false"> In creation view</label>' +
        '<input type="hidden" name="ServicesFieldsDefault['+index+'][is_bold]" value="0"><label class="col-md-2">' +
        '<input type="checkbox" name="ServicesFieldsDefault['+index+'][is_bold]" value="1" aria-invalid="false"> Is bold</label>' +
        '</div><div class="row">' +
        '<div class="col-md-1"><label class="control-label">Size</label></div><div class="col-md-3">' +
        '<select class="form-control" name="ServicesFieldsDefault['+index+'][size]" placeholder="Enter Size..." aria-invalid="false">' +
        '<option value="1" selected>1</option>' +
        '<option value="2" >2</option>' +
        '<option value="3" >3</option>' +
        '<option value="4" >4</option>' +
        '<option value="5" >5</option>' +
        '<option value="6" >6</option>' +
        '<option value="7" >7</option>' +
        '<option value="8" >8</option>' +
        '<option value="9" >9</option>' +
        '<option value="10" >10</option>' +
        '<option value="11" >11</option>' +
        '<option value="12" >12</option>' +
        '</select></div>' +
        '<div class="col-md-8"><button class="remove-field btn btn-danger pull-right">Remove</button></div>' +
        '<div class="variants-box col-md-offset-2 col-md-10" style="margin-top: 15px;"></div></div>' +
        '<input type="hidden" class="variables-input" name="ServicesFieldsDefault['+index+'][variables]" value="">' +
        '<input type="hidden" name="ServicesFields['+index+'][position]" value="0">' +
        '</li>';
    index++;
    return field;
}


function setHandlers() {
    $('.remove-field').off('click').on('click',function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().hide(500,function () {
            $(this).remove();
        });

    });
    $('.service-field').each(function (){
        field = $(this);
        field.find('.select-type').off('click').on('click',function (e) {
            (new RenderType())($(this).val(),'.variables-input','.variants-box',$(this).closest('.service-field'),$('#lang-list').val());
            // renderVariants($(this).val(),$(this).parent().parent().parent())
        });
        field.find('.select-type').each(function () {
            // console.log($(this).closest('.service-field'));
            (new RenderType())($(this).val(),'.variables-input','.variants-box',$(this).closest('.service-field'),$('#lang-list').val());
            // renderType.render($(this).val(),'.variables-input','.variants-box',$(this).closest('.service-field'),$('#lang-list').val());
            // renderVariants($(this).val(),$(this).parent().parent().parent());
        })
    });
}

function updateLanguages($attributes,$lang)
{
    $attributes.forEach(function(elem) {
        var selector = 'input';
        if(elem.array)
            selector += '[name*="'+elem.model+'["]'+'[name$="]['+elem.field+']"]';
        else
            selector += '[name="'+elem.model+'['+elem.field+']"]';
        $(selector).each(function () {
            var old = $(this).attr('name');
            $(this).attr('name',old+'['+$lang+']')
        })
    });
}
function changeLang($attributes,$lang) {
    $attributes.forEach(function(elem) {
        var selector = 'input';
        if(elem.array)
            selector += '[name*="'+elem.model+'["]'+'[name*="]['+elem.field+']"]';
        else
            selector += '[name*="'+elem.model+'['+elem.field+']"]';
        $(selector).each(function () {
            // var old = $(this).attr('name');
            // console.log($(this));
            $(this).hide();
            var selector_w_l = 'input';
            if(elem.array)
                selector_w_l += '[name*="'+elem.model+'["]'+'[name*="]['+elem.field+']['+$lang+']"]';
            else
                selector_w_l += '[name="'+elem.model+'['+elem.field+']['+$lang+']"]';

            if($(this).parent().find(selector_w_l).length>0){
                $(this).parent().find(selector_w_l).show();
                (new RenderType())($(this).closest('.service-field').find('.select-type').val(),'.variables-input','.variants-box'
                    ,$(this).closest('.service-field'),$('#lang-list').val());
            }
            else
            {
                var orig = $(this).attr('name');
                var parts = orig.split('[');
                parts.forEach(function (item,i,array) {
                    array[i] = item.split(']',1);
                });
                var new_name = '';
                if(parts[parts.length-1][0]!=elem.field)
                    parts.forEach(function (item,i) {
                        if(i == 0)
                            new_name += item;
                        else
                            if(i == parts.length-1)
                                new_name += '['+$lang+']';
                            else
                                new_name += '['+item+']';

                    });

                var field = $(this).clone().attr('name',new_name).appendTo($(this).parent()).show();
                field.val('');
                // console.log(field);
                var id = $(this).closest('.service-field').attr('data-field-id');
                var element = $(this);
                element.closest('.service-field').find('.overlay').show();
                if(id!=undefined)
                {
                    $.ajax({
                        url: baseUrl + '/ia/services/get-field-translation',
                        data: {
                            id: id,
                            lang: $lang
                        },
                        method:'get',
                        success:function (response) {
                            if(response!=null)
                            {
                                // field.attr('value',response[elem.field]);
                                field.val(response[elem.field]);
                                if(elem.field=='variables'){
                                    (new RenderType())(element.closest('.service-field').find('.select-type').val(),'.variables-input','.variants-box'
                                        ,element.closest('.service-field'),$lang);
                                    element.closest('.service-field').find('.overlay').hide();
                                }

                            }

                        }
                    });
                }
                else
                {
                    (new RenderType())(element.closest('.service-field').find('.select-type').val(),'.variables-input','.variants-box'
                        ,element.closest('.service-field'),$lang);
                    element.closest('.service-field').find('.overlay').hide();
                }
            }
        })
    });
}


function formatPermI(perm) {
    if (perm.loading) {
        return perm.text;
    }
    var markup = '';
    markup += '<div style="font-size: large"><i class="'+perm.text+'"></i></div><div style="font-size: small"><b>'+perm.text+'</b></div>';
    return markup;
}

function formatPermSelI (perm) {
    if (perm.name == undefined)
        return perm.text;

    return perm.text;
}

$('document').ready(function () {

    $('#add-field').on('click',function (e) {
        e.preventDefault();
        index = $('.service-field').length;
        $('#services-fields>.sortable').append(renderField()).find('.service-field').show(500);
        setHandlers();
        updateLanguages(attr,default_language);
    });
    updateLanguages(attr,default_language);
    setHandlers();

    $( ".sortable" ).sortable({
        stop:function (event, ui) {
            $('.service-field ').each(function (index) {
                $(this).find('input[name*="[position]"]').val(index);
            });
        }
    });

    $('#services-icon').select2({
        // data:icons,
        placeholder: 'Select icon',
        allowClear: false,
        width: '100%',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPermI,
        templateSelection: formatPermSelI
    });
    // $('#available').select2({
    //     width: '100%',
    //     allowClear: true,
    //     placeholder: 'Choose'
    // });

     $('.select-services').select2({
            width: '100%',
            allowClear: true,
            placeholder: 'Select additional services'
        });

    $('#lang-list').on('change',function () {
        changeLang(attr,$(this).val());
    });

});

