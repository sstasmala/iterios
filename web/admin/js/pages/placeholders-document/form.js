$(document).ready(function () {
    function getModuleFields(module_class, module) {
        $.ajax({
            url: baseUrl + '/ia/placeholders/load-fields',
            method: 'POST',
            dataType: 'json',
            data: {
                _csrf: $('meta[name="csrf-token"]').attr('content'),
                module: module_class
            },
            success: function (data) {
                $('div.mfield table tbody').empty();

                if (data) {
                    $.each(data, function (i, field) {
                        var template = '<tr data-key="1">\n' +
                            '<td class="skip-export kv-align-center kv-align-middle" style="width:50px;" data-col-seq="0">\n' +
                            '<a href="#" title="Copy" aria-label="Copy" data-pjax="0" data-pl_id="'+i+'">\n' +
                            '<span class="btn btn-success btn-sm copy_mark_btn copy_button">\n' +
                            '<span class="fa fa-copy"></span>\n' +
                            '</span>\n' +
                            '</a>\n' +
                            '</td>\n' +
                            '<td data-col-seq="1"><span id="placeholder_'+i+'">{{item.' + field.name + '}}</span></td>\n' +
                            '<td data-col-seq="2">' + field.text + '</td>\n' +
                            '<td data-col-seq="3">' + module + '</td>\n' +
                            '</tr>';

                        $('div.mfield table tbody').append(template);
                    });
                }
            },
            error: function () {
                alert('Error with data getting!');
            }
        });
    }

    function formatPerm(perm) {
        if (perm.loading) {
            return perm.text;
        }

        var markup = '';

        if (perm.fake != true) {
            markup = '<b>'+perm.name+'</b> - '+perm.type+'';
            markup += '<div style="font-size: small">Module: <b>' + perm.module + '</b>' + (perm.type_ph == 'field' ? ' Field: <b>' + perm.field + '</b>' : '')+'</div>';
        } else {
            markup = '<b>'+perm.name+'</b> (Fake)';
        }

        return markup;
    }

    function formatPermSel (perm) {
        if (perm.name == undefined)
            return perm.text;

        if (perm.fake == true)
            return perm.name + ' (Fake)';

        return perm.name + ' - '+perm.type;
    }

    $('#placeholders_doc-id').select2({
        ajax: {
            url: baseUrl + '/ia/placeholders-document/get-placeholders',
            dataType: 'json',

            data: function (params) {
                var query = {
                    search: params.term
                };
                return query;
            },
            processResults: function (data)
            {
                var array = [];

                for (var i in data){
                    array.push({id: data[i].id, name: data[i].name, fake: data[i].fake, module: data[i].module, module_class: data[i].module_class, field: data[i].field, type: data[i].type, type_ph: data[i].type_ph});
                }

                return {
                    results: array
                };
            }
        },
        width: '100%',
        placeholder: 'Choose placeholder',
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatPerm,
        templateSelection: formatPermSel
    });

    $('#placeholders_doc-id').on('change', function (e) {
        var data = $(this).select2('data')[0];

        if (data.type_ph === 'additional_template') {
            $('#placeholdersdocument-template').closest('div').show();
            getModuleFields(data.module_class, data.module);
            $('div.mfield').show();
        } else {
            var ph = $('#placeholders_doc-id option:selected');

            if (ph.length != 0 && ph.data('type') === 'additional_template') {
                $('#placeholdersdocument-template').closest('div').show();
                getModuleFields(ph.data('module_class'), ph.data('module'));
                $('div.mfield').show();
            } else {
                $('#placeholdersdocument-template').closest('div').hide();
                $('div.mfield').hide();
            }
        }
    });

    $('div.mfield').on('click', '.copy_button', function () {

        var pl_id = $(this).parent('a').data('pl_id');
        var root = document.getElementById('placeholder_'+pl_id);
        // var root = document.getElementById('placeholder_'+pl_id).innerHTML;
        var range = document.createRange();
        range.setStart(root, 0);
        range.setEnd(root, 1);
        window.getSelection().empty();
        window.getSelection().addRange(range);
        try {
            var successful = document.execCommand('copy');
            window.getSelection().empty();
        } catch (err) {
            alert(err);
        }

    });

    CKEDITOR.replace('placeholdersdocument-template', {
        height: 300,
        extraPlugins: 'placeholder_mini',
        removePlugins: 'placeholder_select'
    });

    var ph = $('#placeholders_doc-id option:selected');

    if (ph.length != 0 && ph.data('type') === 'additional_template') {
        $('#placeholdersdocument-template').closest('div').show();
        getModuleFields(ph.data('module_class'), ph.data('module'));
        $('div.mfield').show();
    }
});