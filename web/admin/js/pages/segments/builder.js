
function loadFilters(data) {
    $.ajax({
        url: baseUrl + '/ia/segments/load-filter',
        dataType: 'json',
        type: 'POST',
        data: {
            module: data,
            _csrf: $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            console.log(data);

            $('#builder-widgets').queryBuilder({
                plugins: ['bt-tooltip-errors'],
                filters: data,
                rules: rules,
            });
        },
        error: function () {
            alert('Filters is not loaded!');
        }
    });
}

$(document).ready(function () {
    var modules = $('#segments-modules').val();
    if(modules!="") {
        loadFilters(modules);
    }
});

$('#segments-modules').on('change',function(){
    var modules = $('#segments-modules').val();
    loadFilters(modules);
});


// bux fix if datetimepicker data is empty
$('#builder-widgets').on('afterCreateRuleInput.queryBuilder', function(e, rule) {
    if (rule.filter.plugin === 'datetimepicker') {
        var $input = rule.$el.find('.rule-value-container [name*=_value_]');
        $input.on('dp.change', function() {
            $input.trigger('change');
        });
    }
});

$('#segments-form-submit').click(function() {
    var getDataRules = $('#builder-widgets').queryBuilder('getRules');
    $('#segments-constructor input[name="Segments[rules]"]').val(JSON.stringify(getDataRules));
    $('#segments-constructor').submit();
});
