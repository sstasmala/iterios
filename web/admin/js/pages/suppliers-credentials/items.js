$(document).ready(function () {

    $('#supplierscredentials-supplier_id').on('change', function () {
        var request = $.ajax({
            url: baseUrl + '/ia/suppliers-credentials/load-params',
            dataType: 'json',
            method: "GET",
            data: {
                supplier_id: $(this).val(),
            },
            success: function (data) {
                console.log(data);
                var UserPass = ''+
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="supplierscredentials-params-username">Username</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="supplierscredentials-params-username" class="form-control" name="SuppliersCredentials[credentialParams][username]">' +
                    '</div>\n' +
                    '</div>\n'+
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="supplierscredentials-params-password">Password</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="supplierscredentials-params-password" class="form-control" name="SuppliersCredentials[credentialParams][password]">' +
                    '</div>\n' +
                    '</div>\n';

                var api_token = ''+
                    '<div class="form-group">\n' +
                    '<label class="control-label col-md-2" for="supplierscredentials-params-apitoken">Api token</label>\n' +
                    '<div class="col-md-10">\n' +
                    '<input type="text" id="supplierscredentials-params-apitoken" class="form-control" name="SuppliersCredentials[credentialParams][apiKey]">' +
                    '</div>\n' +
                    '</div>\n';

                if(data == 'login&pass'){
                    $('#params-body').html(UserPass);
                }else{
                    $('#params-body').html(api_token);
                }
            },
        });

    });

});