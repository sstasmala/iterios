/**
 * add_to_tenant.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

$('document').ready(function () {
    $('#sendtestsmsform-text').countSms('#sms-counter');

    $('#sendtestsmsform-type_alpha_name input').on('click', function (e) {
        var aname_type = $(this).val();

        if (aname_type == 'personal') {
            $('#tenant_id').show();
        } else {
            $('#sendtestsmsform-tenant_id').val(null).trigger('change');
            $('#tenant_id').hide();
        }
    });

});