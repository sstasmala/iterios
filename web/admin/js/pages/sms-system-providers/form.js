/**
 * form.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function getConfig(provider) {
    $.ajax({
        url: baseUrl + '/ia/sms-system-providers/get-config',
        dataType: 'json',
        data: {
            'provider': provider
        },
        success: function (data) {
            if (data) {
                var jsonEditor = window[$('#credentials_config').data('json-editor-name')];
                jsonEditor.set(data);
            }
        },
        error: function () {
            alert('Неудалось получить данные!');
        }
    });
}

$('document').ready(function () {
    $('#smssystemproviders-provider').on('change', function (e) {
       var provider = $('#smssystemproviders-provider').val();

       getConfig(provider);
    });
});