/**
 * add_to_tenant.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function formatPerm(perm) {
    if (perm.loading) {
        return perm.text;
    }

    var markup = '<div style="font-size: large"><b>' + perm.name + '</b></div>';
    markup += '<div style="font-size: small">';
    markup += perm.owner + '</div>';

    return markup;
}

function formatPermSel (perm) {
    return perm.name || perm.text;
}

$('document').ready(function () {
    var tenant_id = null;

    $('#tenants-list-create').select2({
        ajax:{
            url: baseUrl + '/ia/sms-system-providers/get-tenants',
            dataType: 'json',
            data: function (params) {
                var query = {
                    provider_id: $('#createAlphaNameModal').find('input[name="SmsAlphaNames[provider_id]"]').val(),
                    name: params.term
                };

                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data) {
                    array.push({
                        id: data[i]['id'],
                        name: data[i]['name'],
                        owner: data[i]['owner']
                    });
                }

                return {
                    results: array
                }
            }
        },
        width: '100%',
        allowClear: false,
        escapeMarkup: function (markup) {
            return markup;
        },
        placeholder: 'Choose tenant',
        templateResult: formatPerm,
        templateSelection: formatPermSel,
        dropdownParent: $('#createAlphaNameModal')
    });

    $('#tenants-list-update').select2({
        ajax:{
            url: baseUrl + '/ia/sms-system-providers/get-tenants',
            dataType: 'json',
            data: function (params) {
                var query = {
                    provider_id: $('#updateAlphaNameModal').find('input[name="SmsAlphaNames[provider_id]"]').val(),
                    tenant_id: tenant_id,
                    name: params.term
                };

                return query;
            },
            delay: 1000,
            minimumInputLength: 0,
            processResults: function (data)
            {
                var array = [];

                for(var i in data) {
                    array.push({
                        id: data[i]['id'],
                        name: data[i]['name'],
                        owner: data[i]['owner']
                    });
                }

                return {
                    results: array
                }
            }
        },
        width: '100%',
        allowClear: false,
        escapeMarkup: function (markup) {
            return markup;
        },
        placeholder: 'Choose tenant',
        templateResult: formatPerm,
        templateSelection: formatPermSel,
        dropdownParent: $('#updateAlphaNameModal')
    });

    $('.button_update').click(function () {
        var name_id = $(this).data('nid');
        tenant_id = $(this).data('tid');
        var tenant_name = $(this).data('tname');
        var value = $(this).data('name');

        $('#updateAlphaNameModal').find('.modal-title').text('Update personal alpha name');
        $('#updateAlphaNameModal').find('form').attr('action', '/ia/sms-system-providers/update-alpha-name');
        $('#updateAlphaNameModal').find('button[type="submit"]').text('Update');

        $('#updateAlphaNameModal').find('input[name="id"]').val(name_id);
        $('#tenants-list-update').append('<option value="'+tenant_id+'" selected="selected">'+tenant_name+'</option>').trigger('change');
        $('#updateAlphaNameModal').find('input[name="SmsAlphaNames[value]"]').val(value);

        $('#updateAlphaNameModal').modal('show');
    });

});