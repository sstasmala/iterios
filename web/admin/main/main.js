/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

function datetime_convert()
{
    $('.to-datetime-convert').each(function () {
        if ($(this).text().length > 1)
            $(this).text(moment(parseInt($(this).text()) * 1000).tz(serverTimezone).format("DD-MM-YYYY HH:mm:ss"));
    });
}

function datetime_convert_val()
{
    $('.to-datetime-convert-val').each(function () {
        if ($(this).val().length > 1)
            $(this).val(moment(parseInt($(this).val()) * 1000).tz(serverTimezone).format("DD-MM-YYYY HH:mm:ss"));
    });
}

function phone_convert()
{
    $('.to-phone-convert').each(function () {
        var val = $(this).text();

        if (val.length > 1 && val.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
            var result = val.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );

            $(this).text('+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4]);
        }
    });
}

function selectMenu() {
    var path = window.location.pathname;
    path.split('/').forEach(function (item,i,array) {
        if(item=='ia')
        {
            var elem = $('#'+array[i+1]);
            if(elem.length>0)
            {
                elem.parentsUntil('ul.sidebar-menu', 'ul.treeview-menu').show();
                elem.addClass('active');
                elem.parentsUntil('ul.sidebar-menu', 'li.treeview').addClass('active menu-open');
            }
        }
    });
}

function countDiff()
{
    $('.calc-diff').each(function () {
        var time = $(this).attr('data-time');
       $(this).text(moment(time,'X').fromNow());
    });
}

$(document).ready(function () {
    $('[data-toggle="control-sidebar"]').controlSidebar();
    $('[data-toggle="push-menu"]').pushMenu();

    var $pushMenu       = $('[data-toggle="push-menu"]').data('lte.pushmenu');
    var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar');
    var $layout         = $('body').data('lte.layout');

    $pushMenu.expandOnHover();

    if (!$('body').hasClass('sidebar-collapse'))
        $('[data-layout="sidebar-collapse"]').click();

    datetime_convert();
    phone_convert();
    selectMenu();
    countDiff();

    $('form').attr('autocomplete','off');
});
