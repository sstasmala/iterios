﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @fileOverview The "placeholder" plugin.
 *
 */

'use strict';

( function() {
	CKEDITOR.plugins.add( 'placeholder_mini', {

		onLoad: function() {
			// Register styles for placeholder widget frame.
			CKEDITOR.addCss( '.cke_placeholder{background-color:#ff0}' );
		},

		afterInit: function( editor ) {
			var placeholderReplaceRegex = /\{\{([^\{\}])+\}\}/g;

			editor.dataProcessor.dataFilter.addRules( {
				text: function( text, node ) {
					var dtd = node.parent && CKEDITOR.dtd[ node.parent.name ];

					// Skip the case when placeholder is in elements like <title> or <textarea>
					// but upcast placeholder in custom elements (no DTD).
					if ( dtd && !dtd.span )
						return;

                    if (typeof node.parent.attributes != 'undefined' && 'style' in node.parent.attributes && node.parent.attributes.style == 'background-color:#ff0')
                        return;

					return text.replace( placeholderReplaceRegex, function( match ) {
						return '<span style="background-color:#ff0">' + match + '</span>';
					} );
				}
			} );
		}
	} );

} )();
