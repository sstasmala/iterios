/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.plugins.setLang( 'placeholder_edit', 'en', {
	title: 'Edit Value',
	toolbar: 'Placeholder',
	name: 'Value',
	invalidName: 'The placeholder can not be empty and can not contain any of following characters: {, }, <, >',
	pathName: 'placeholder_edit'
} );
