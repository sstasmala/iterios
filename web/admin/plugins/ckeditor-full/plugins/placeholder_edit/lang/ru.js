/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.plugins.setLang( 'placeholder_edit', 'ru', {
	title: 'Редактирование значения',
	toolbar: 'Создать плейсхолдер',
	name: 'Значение',
	invalidName: 'Плейсхолдер не может быть пустым и содержать один из следующих символов: "[, ], <, >"',
	pathName: 'плейсхолдер_эдит'
} );
