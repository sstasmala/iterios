﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @fileOverview The "placeholder" plugin.
 *
 */

'use strict';

( function() {
	CKEDITOR.plugins.add('placeholder_edit', {
		requires: 'widget,dialog',
		lang: 'en,ru',
		icons: 'placeholder', // %REMOVE_LINE_CORE%
		hidpi: true, // %REMOVE_LINE_CORE%

		onLoad: function() {
			// Register styles for placeholder widget frame.
			CKEDITOR.addCss( '.cke_edit_placeholder{background-color:#ef909f}' );
            CKEDITOR.addCss( '.cke_edit_placeholder_yellow{background-color:#ff0}' );
		},

		init: function( editor ) {
            var lang = editor.lang.placeholder_edit;

            // Register dialog.
            CKEDITOR.dialog.add( 'placeholder_edit', this.path + 'dialogs/placeholder.js' );

			// Put ur init code here.
			editor.widgets.add( 'placeholder_edit', {
				// Widget code.
				dialog: 'placeholder_edit',
                pathName: lang.pathName,
				// We need to have wrapping element, otherwise there are issues in
				// add dialog.
				template: '<span class="cke_edit_placeholder">[[]]</span>',

				downcast: function() {
					return new CKEDITOR.htmlParser.text( '' + this.data.name + '' );
				},

				init: function() {
					// Note that placeholder markup characters are stripped for the name.
					this.setData( 'name', this.element.getText() );
				},

				data: function() {
					//console.log(this.element);

					if (this.data.name.indexOf('{{') === -1) {
                        this.element.$.parentElement.parentElement.innerHTML = '<span class="cke_edit_placeholder_yellow">' + this.data.name + '</span>';
                    } else {
                        this.element.setText(this.data.name);
                    }
				},

				getLabel: function() {
					return this.editor.lang.widget.label.replace( /%1/, this.data.name + ' ' + this.pathName );
				}
			} );
		},

		afterInit: function( editor ) {
			var placeholderReplaceRegex = /\{\{([^\{\}])+\}\}/g;

            editor.dataProcessor.dataFilter.addRules( {
                text: function( text, node ) {
                    var dtd = node.parent && CKEDITOR.dtd[ node.parent.name ];

                    // Skip the case when placeholder is in elements like <title> or <textarea>
                    // but upcast placeholder in custom elements (no DTD).
                    if ( dtd && !dtd.span )
                        return;

                    return text.replace( placeholderReplaceRegex, function( match ) {
                        // Creating widget code.
                        var widgetWrapper = null,
                            innerElement = new CKEDITOR.htmlParser.element( 'span', {
                                'class': 'cke_edit_placeholder'
                            } );

                        // Adds placeholder identifier as innertext.
                        innerElement.add( new CKEDITOR.htmlParser.text( match ) );
                        widgetWrapper = editor.widgets.wrapElement( innerElement, 'placeholder_edit' );

                        // Return outerhtml of widget wrapper so it will be placed
                        // as replacement.
                        return widgetWrapper.getOuterHtml();
                    } );
                }
            } );
		}
	} );

} )();
