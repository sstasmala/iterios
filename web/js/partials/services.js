(function($){
    $(document).ready(function() {
        var portletTitle = $('#m_modal_service_details').find('#portlet-title');

        $(".select2-elems").select2();
        initSelect2();
        setServiceName();

        $('#m_modal_service_details').find('.btn-secondary').on('click', function(){
            hideFields();
        });
        $('#m_modal_service_details').find('.close').on('click', function(){
            hideFields();
        });

        function initSelect2() {
            $('#m_modal_service_details').on('shown.bs.modal', function(){
                $(".m-select2").select2();
            });
        }
        /*
        function getType(){
            var url = baseUrl + '/ita/' + tenantId + '/services/get-service';
            $.ajax({
                url: url,
                success: function (data) {
                    console.log(data);
                    console.log(GetTranslation('1'));
                }
            });
        }*/

        function getDataFromService() {
            var url = baseUrl + '/ita/' + tenantId + '/services/get-service';
            $.ajax({
                url: url,
                success: function (data) {
                    var serviceTypes = JSON.parse(data);
                }
            });
        }

        function setServiceName() {
            $('#m_modal_select_service').on('click', '.btn-lg', function(){
                portletTitle.text(this.dataset.name);
                $('#m_modal_service_details').find('#' + this.dataset.name).css("display", "flex");
            });
        }

        function hideFields() {
            $('#m_modal_service_details').find('.service-fields').each(function(index) {
                this.style = "display: none";
            });
        }
    });
})(jQuery);

