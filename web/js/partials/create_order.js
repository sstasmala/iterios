(function($){
    $(document).ready(function(){
        searchContact();

        $('#find_contact_form')
            .find('[valid="phoneNumber"]')
            .intlTelInput({
                utilsScript: "/plugins/phone-validator/build/js/utils.js",
                autoPlaceholder: true,
                preferredCountries: ['ua', 'ru'],
                allowExtensions: false,
                autoFormat: true,
                autoHideDialCode: true,
                customPlaceholder: null,
                defaultCountry: "",
                geoIpLookup: null,
                nationalMode: false,
                numberType: "MOBILE"
            }).then(function (data) {

            // var inputFormat = $("#order_create__contact_phone_input")[0].placeholder;
            // var formated = inputFormat.replace(/[0-9]/g, 9);
            var formated = '+999 99 999 9999';
            $("#order_create__contact_phone_input").inputmask(formated);

            $("#order_create__contact_phone_input").on("countrychange", function (e, countryData) {
                var inputFormat = $("#order_create__contact_phone_input")[0].placeholder;
                var formated = inputFormat.replace(/[0-9]/g, 9);
                $("#order_create__contact_phone_input").inputmask(formated);
            });
        });

        function searchContact() {
            var email = $('#order_create__contact_email_input');
            var phone = $('#order_create__contact_phone_input');
            var email_value, phone_value;

            var searchDelay = (function(){
                var timer = 0;
                return function(callback, ms){
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            })();

            /* jQuery Validate Emails with Regex */
            function validateEmail(email) {
                var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                return $.trim(email).match(pattern) ? true : false;
            }

            var showContacts = function (contacts) {
                var el = $('#m_modal_create_order #clients').slideDown('slow').find('.m-widget4');

                el.empty();
                $('button.btn-primary').prop('disabled', 'disabled');

                $.each( contacts, function( key, contact ) {
                    var template = $('<!--begin::Widget 14 Item-->\
                        <div class="m-widget4__item">\
                            <div class="m-widget4__info"><div class="contact-name-block">\
                                <span class="m-widget4__title"></span><br>\
                                <span class="m-widget4__sub"></span>\
                            </div></div>\
                            <div class="m-widget4__ext">\
                                <a href="#" id="select-contact" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" data-index="'+key+'">'+GetTranslation('select')+'</a>\
                            </div>\
                        </div>\
                    <!--end::Widget 14 Item-->');
                    //template.find('a').attr('href', baseUrl + '/ita/' + tenantId + '/contacts/view?id='+contact.id);
                    template.find('.m-widget4__title').text(contact.name);
                    template.find('.m-widget4__sub').html(contact.email + '<br>' + contact.phone);

                    el.append(template);
                });

                $('body').on('click', '#select-contact', function (event) {
                    var key = $(this).data('index');

                    $('#m_modal_create_order input[name="contact_id"]').val(contacts[key].id);
                    $('#m_modal_create_order input[name="Contacts[first_name]"]').val(contacts[key].first_name);
                    $('#m_modal_create_order input[name="Contacts[last_name]"]').val(contacts[key].last_name);

                    $('.contact-fields').slideUp('slow');
                    $('#m_modal_create_order').find('#create_company_submit').removeAttr('disabled');

                    $(this).text(GetTranslation('clear'))
                        .attr('id', 'unselect-contact')
                        .blur();
                    event.preventDefault();
                });

                $('body').on('click', '#unselect-contact', function (event) {
                    clearModal(true);
                    clearFields();
                    $('#m_modal_create_order input[name="Contacts[email]"], #m_modal_create_order input[name="Contacts[phone]"]').val('');

                    $(this).text(GetTranslation('select'))
                        .attr('id', 'select-contact')
                        .blur();
                    event.preventDefault();
                });
            };

            var clearModal = function (disable) {
                $('#m_modal_create_order #clients').slideUp('slow', function () {
                    $(this).find('.m-widget4').empty();
                });

                $('.contact-fields').slideDown('slow');

                $('#m_modal_create_order input[name="contact_id"]').val('');

                if (disable == true)
                    $('#create_company_submit').prop('disabled', 'disabled');
            };

            var showFields = function () {
                $('#m_modal_create_order #clients').slideUp('slow', function () {
                    $(this).find('.m-widget4').empty();
                });

                $('#m_modal_create_order').find('button.btn-primary').removeAttr('disabled');
            };

            var clearFields = function () {
                $('#m_modal_create_order input[name="contact_id"]').val('');

                $('#m_modal_create_order input[name="Contacts[first_name]"], #m_modal_create_order input[name="Contacts[last_name]"]').val('');
            };

            var searchContacts = function (email, phone) {
                var data = {
                    email: email,
                    phone: phone
                };

                $.get(baseUrl + '/ita/' + tenantId + '/contacts/get-contacts', data, function(res) {
                    mApp.block('#m_modal_create_order .modal-body', {});

                    setTimeout(function() {
                        mApp.unblock('#m_modal_create_order .modal-body');

                        if (res.length > 0) {
                            $('.contact-fields').slideDown('slow');

                            showContacts(res);
                        } else {
                            $('#m_modal_create_order input[name="contact_id"]').val('');
                            showFields();
                        }
                    }, 1000);
                });
            };

            email.on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    email_value = input.val().replace(/^_+|[@_.]+$/g, '');
                    phone_value = phone.val();

                    if (validateEmail(email_value) || phone.inputmask('unmaskedvalue').length >= 12) {
                        $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                        searchContacts(email_value, phone_value);
                    } else {
                        if ($('input[name="Contacts[first_name]"]').val().length < 1 && $('input[name="Contacts[last_name]"]').val().length < 1)
                            clearModal(true);
                    }
                }, 1000);
            });

            phone.on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    phone_value = input.val();
                    email_value = email.val().replace(/^_+|[@_.]+$/g, '');

                    if (validateEmail(email_value) || input.inputmask('unmaskedvalue').length >= 12) {
                        $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                        searchContacts(email_value, phone_value);
                    } else {
                        if ($('input[name="Contacts[first_name]"]').val().length < 1 && $('input[name="Contacts[last_name]"]').val().length < 1)
                            clearModal(true);
                    }
                }, 1000);
            });

            $('input[name="Contacts[first_name]"], input[name="Contacts[last_name]"]').on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    var value = input.val();

                    if (value.length >= 1 && !$('m_modal_create_order #clients').is(':visible')) {
                        $('#m_modal_create_order').find('button.btn-primary').removeAttr('disabled');
                    }
                }, 1000);
            });
        }
    });
})(jQuery);