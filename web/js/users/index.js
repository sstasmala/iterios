var datatable = null;

(function($){
    $(document).ready(function(){
        initValidate();
        initTasksDataTable();
        initPageWidgets();
        
        function initTasksDataTable() {
            function switchUserAccess(uid, status) {
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/users/' + (status ? 'unblock' : 'block'),
                    data: {
                        id: uid
                    },
                    success: function (data) {
                        if (data)
                            datatable.reload();
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            }

            datatable = $('#users_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/users/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                }
                                // custom query params
                            }
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init bootstrap switches
                        $(row).find('[data-switch=true]').each(function(){
                            var switcher = this;
                            $(switcher).bootstrapSwitch();
                            $(switcher).on('switchChange.bootstrapSwitch', function (event, state) {
                                if (state) {
                                    switchUserAccess(data.id, true);
                                    console.log('Access enabled');
                                } else {
                                    switchUserAccess(data.id, false);
                                    console.log('Access disabled');
                                }
                            });
                        });
                        // User resend inventation email button
                        $(row).find('.resend-invite').on('click', function(){
                            var button = $(this);
                            var user_id = button.data('user-id');

                            if(user_id !== undefined && user_id !== null) {
                                swal({
                                    title: GetTranslation('delete_question'),
                                    text: GetTranslation('user_repeat_email_notification_hint'),
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: GetTranslation('submit'),
                                    cancelButtonText: GetTranslation('cancel'),
                                    reverseButtons: true
                                }).then(function(result) {
                                    button.blur();

                                    if (result.value) {
                                        $.ajax({
                                            url: baseUrl + '/ita/' + tenantId + '/users/repeat-invite',
                                            data: {
                                                id: user_id
                                            },
                                            success: function (data) {
                                                var content = {
                                                    title: GetTranslation('good_job'),
                                                    message: GetTranslation('user_repeat_notification_success')
                                                };

                                                $.notify(content, {
                                                    placement: {
                                                        from: "top",
                                                        align: "right"
                                                    },
                                                    z_index: 1031,
                                                    type: "success"
                                                });
                                            },
                                            error: function () {
                                                var content = {
                                                    title: 'Error',
                                                    message: 'Invite don\'t send again!'
                                                };

                                                $.notify(content, {
                                                    placement: {
                                                        from: "top",
                                                        align: "right"
                                                    },
                                                    type: "danger",
                                                    z_index: 1031
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                        // Init tooltip
                        $(row).find('[data-toggle="m-popover"]').each(function() {
                            mApp.initPopover($(this));
                        });
                        // Delete not confirm user
                        $(row).find('#delete_not_confirm').on('click', function(){
                            var button = $(this);
                            var user_id = button.data('user-id');

                            if(user_id !== undefined && user_id !== null) {
                                swal({
                                    title: GetTranslation('delete_question'),
                                    text: GetTranslation('delete_not_confirm_text'),
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: GetTranslation('delete'),
                                    cancelButtonText: GetTranslation('cancel'),
                                    reverseButtons: true
                                }).then(function(result) {
                                    button.blur();

                                    if (result.value) {
                                        $.ajax({
                                            url: baseUrl + '/ita/' + tenantId + '/users/delete-not-confirm',
                                            data: {
                                                id: user_id
                                            },
                                            success: function (data) {
                                                datatable.reload();

                                                var content = {
                                                    title: GetTranslation('delete_user_success'),
                                                    message: ''
                                                };

                                                $.notify(content,{
                                                    placement: {
                                                        from: "top",
                                                        align: "right"
                                                    },
                                                    z_index: 1031,
                                                    type: "success"
                                                });
                                            },
                                            error: function () {
                                                var content = {
                                                    title: 'Error',
                                                    message: 'User don\'t delete!'
                                                };

                                                $.notify(content,{
                                                    placement: {
                                                        from: "top",
                                                        align: "right"
                                                    },
                                                    type: "danger",
                                                    z_index: 1031
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: [
                    {
                        field: 'first_name',
                        title: GetTranslation('users_datatable_column_fio_title', 'Name'),
                        template: function (data) {
                            var admin_icon = data.is_tenant_owner ? ' <i class="la la-user-secret m--font-danger" data-toggle="m-popover" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('icon_tenant_owner', 'Owner') + '"></i>' : '';
                            var not_confirm_icon = (data.not_confirm ? ' <i class="la la-user-times m--font-brand" data-toggle="m-popover" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('icon_not_confirm_user', 'Not confirm user') + '"></i>' : '');
                            var user_name = (data.last_name && data.last_name != null ? data.last_name + ' ' : '') + data.first_name + (data.middle_name && data.middle_name != null ? ' ' + data.middle_name : '');
                            return user_name + admin_icon + not_confirm_icon;
                        }
                    },
                    {
                        field: 'role',
                        title: GetTranslation('users_datatable_column_post_title', 'Post')
                    },
                    {
                        field: 'phone',
                        title: GetTranslation('users_datatable_column_phone_title', 'Phone'),
                        template: function (data) {
                            if (data.phone != null && data.phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
                                var result = data.phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );

                                return '+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4];
                            }

                            return (data.phone == null || (data.phone.indexOf('+') + 1) ? data.phone : '+' + data.phone);
                        }
                    },
                    {
                        field: 'email',
                        title: GetTranslation('email_title', 'Email')
                    },
                    {
                        field: 'access',
                        title: GetTranslation('users_datatable_column_access_title', 'Access'),
                        width: '150px',
                        sortable: false,
                        template: function (row, index, datatable) {
                            var template;
                            if(row.not_confirm){
                                template = '<button type="button"\
                                            class="resend-invite btn btn-outline-brand btn-sm"\
                                            data-user-id="' + row.id + '"\
                                            data-toggle="m-popover"\
                                            data-container="body"\
                                            data-toggle="m-popover"\
                                            data-placement="top"\
                                            data-content="' + (GetTranslation('user_repeat_email_notification_hint', 'Resend user invitation email')) + '">\
                                            ' + (GetTranslation('user_repeat_email_notification', 'Send again')) + '\
                                            </button>';
                            } else {
                                template = '<input data-switch="true" ' + 
                                                (row.is_tenant_owner || userId == row.id || row.not_confirm ? 'disabled="disabled"' : '') +
                                                ' type="checkbox" ' +
                                                (row.status == 10 ? 'checked="checked"' : '') +
                                                ' data-on-text="' +
                                                GetTranslation('yes', 'Yes') +
                                                '" data-off-text="' +
                                                GetTranslation('no', 'No') +
                                                '" data-on-color="success" data-off-color="danger">';
                            }
                            return template;
                        }
                    },
                    {
                        field: 'Actions',
                        title: GetTranslation('users_datatable_column_operations_title', 'Operations'),
                        sortable: false,
                        width: '80px',
                        template: function (row, index, datatable) {
                            var template = '<button '+ (row.is_tenant_owner || userId == row.id || row.not_confirm ? 'disabled ' : '') +'class="edit-user m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('edit', 'Edit') + '" data-user-id="'+row.id+'" data-toggle="modal" data-target="#edit_user_popup"><i class="la la-edit"></i></button>\
                                            <button '+ (row.is_tenant_owner || userId == row.id ? 'disabled ' : '') +'class="delete-user m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('user_delete_dismiss', 'Dismiss') + '" data-user-id="'+row.id+'" '+(row.not_confirm ? 'id="delete_not_confirm"' : 'data-toggle="modal" data-target="#delete_user_popup"')+'><i class="la la-trash"></i></button>';
                            return template;
                        }
                    }
                ],
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
        }

        function initValidate(){
            $('#edit_user_form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                var inputFormat = $('#user_edit_phone_input')[0].placeholder;
                // var formated = inputFormat.replace(/[0-9]/g, 9);
                var formated = '+999 99 999 9999';
                $('#user_edit_phone_input').inputmask(formated);
                $('#user_edit_phone_input').on("countrychange", function (e, countryData) {
                    var formated = $('#user_edit_phone_input')[0].placeholder.replace(/[0-9]/g, 9);
                    $('#user_edit_phone_input').inputmask(formated)
                });
            });
        }

        function initPageWidgets() {
            /* jQuery Validate Emails with Regex */
            function validateEmail(email) {
                var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                return $.trim(email).match(pattern) ? true : false;
            }

            var showUsers = function (users) {
                var el = $('#ita_users').slideDown('slow').find('.m-widget4');

                $('#user_info, #access').hide();
                $('#create_user_popup_submit').prop('disabled', 'disabled');
                $('body #user-exists-notification').remove();

                el.empty();

                $.each(users, function( key, user ) {
                    var template = $('<!--begin::Widget 14 Item-->\
                <div class="m-widget4__item">\
                    <div class="m-widget4__img m-widget4__img--pic">\
                        <img src="" alt="">\
                    </div>\
                    <div class="m-widget4__info">\
                        <span class="m-widget4__title"></span><br>\
                        <span class="m-widget4__sub"></span>\
                    </div>\
                    <div class="m-widget4__ext">\
                        <a href="#" id="select_user" data-url="" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary">'+GetTranslation('select')+'</a>\
                    </div>\
                </div>\
            <!--end::Widget 14 Item-->');
                    template.find('img').prop('src', baseUrl + '/' + (user.photo != null && user.photo.length > 0 ? user.photo : 'img/profile_default.png'));
                    template.find('a').attr('data-url', baseUrl + '/ita/' + tenantId + '/users/add-user?id='+user.id);
                    template.find('.m-widget4__title').text(user.first_name + (user.last_name && user.last_name != 'null' ? ' ' + user.last_name : ''));
                    template.find('.m-widget4__sub').html(user.email + (user.phone != null && user.phone.length > 0 ? ', ' + user.phone : ''));

                    el.append(template);
                });
            };

            var clearModal = function (disable) {
                var form = $('#create_user_popup').find('form');

                $('#ita_users').slideUp('slow', function () {
                    $(this).find('.m-widget4').empty();
                });

                $('body #user-exists-notification').remove();
                $('#user_info, #access').slideUp('slow');

                if (disable == true)
                    $('#create_user_popup_submit').prop('disabled', 'disabled');
            };

            var searchUsers = function (email) {
                var data = {
                    search: email
                };

                $.get(baseUrl + '/ita/' + tenantId + '/users/search-users', data, function(res) {
                    mApp.block('#create_user_popup .modal-body', {});

                    setTimeout(function() {
                        mApp.unblock('#create_user_popup .modal-body');

                        if (!res.user_tenant && res.length > 0) {
                            showUsers(res);
                        } else {
                            clearModal(true);

                            if (res.user_tenant) {
                                $('#ita_users').before('<div id="user-exists-notification" class="m-alert m-alert--outline m-alert--outline-2x alert alert-danger alert-dismissible fade show" style="display:none;" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>' +
                                    '<strong>'+GetTranslation('user_create_notification_oops')+'</strong> '+GetTranslation('user_create_notification_user_exist')+'</div>');

                                $('body #user-exists-notification').show('slow');
                            } else {
                                $('#user_info, #access').slideDown('slow');
                                $('#create_user_popup_submit').removeAttr('disabled');
                            }
                        }
                    }, 1000);
                });
            };

            var searchDelay = (function(){
                var timer = 0;
                return function(callback, ms){
                    clearTimeout (timer);
                    timer = setTimeout(callback, ms);
                };
            })();

            $('#user_create_email_input, #user_email_edit').inputmask({ alias: 'email'});
            // $('#user_create_phone_input, #user_edit_phone_input').inputmask({ 'mask': '99(999)999-9999'});

            $('#add_exist_user_form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {
                $('.extra_user_create_block').find('.validate-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated);
                    });
                });
            });

            var create_form = null;

            $('#create_user_popup_submit').on('click', function (e) {
                var email = $('#user_create_email_input').inputmask('unmaskedvalue');

                $('#user_create_email_input').val(email);

                create_form = $(this).closest('div.modal-content').find('form');

                create_form.validate({
                    rules: {
                        'User[email]': {
                            required: function () {
                                return $('#ita_users').is(':visible') ? false : true;
                            },
                            email: function () {
                                var email = $('#user_create_email_input').val().replace(/^_+|[@_.]+$/g, '');

                                return (email.length > 0 ? true : false);
                            }
                        },
                        password: {
                            required: function () {
                                return $('#ita_users').is(':visible') ? false : true;
                            },
                            minlength: 6
                        },
                        'User[phone]': {
                            required: false
                        },
                        'User[first_name]': {
                            required: function () {
                                return $('#ita_users').is(':visible') ? false : true;
                            }
                        },
                        'User[last_name]': {
                            required: function () {
                                return $('#ita_users').is(':visible') ? false : true;
                            }
                        },
                        'role': {
                            required: true
                        }
                    }
                });

                if (!create_form.valid()) {
                    return;
                }
            });

            $('#edit_user_popup_submit').on('click', function (e) {
                var form = $(this).closest('div.modal-content').find('form');

                form.validate({
                    rules: {
                        'User[email]': {
                            required: true,
                            email: function () {
                                var email = $('#user_email_edit').val().replace(/^_+|[@_.]+$/g, '');

                                return (email.length > 0 ? true : false);
                            }
                        },
                        password: {
                            required: false,
                            minlength: 6
                        },
                        'User[phone]': {
                            required: false
                        },
                        'User[first_name]': {
                            required: true
                        },
                        'User[last_name]': {
                            required: true
                        },
                        'role': {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }
            });

            $('#create_user_popup #user_create_email_input').on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    var email = input.val();

                    if (validateEmail(email)) {
                        searchUsers(email);

                        // clearModal(true);
                        //
                        // $('#user_info').slideDown('slow');
                        // $('#create_user_popup_submit').removeAttr('disabled');
                    } else {
                        if ($('#user_info').is(':visible'))
                            clearModal(true);
                    }
                }, 1000);
            });

            $('body').on('click', '#select_user', function () {
                if (create_form != null)
                    create_form.validate().resetForm();

                if ($('#access').is(':visible')) {
                    $('form#add_exist_user_form').attr('action', baseUrl + '/ita/' + tenantId + '/users/create');
                    $('#access').slideUp('slow');
                    $('#create_user_popup_submit').prop('disabled', 'disabled');
                    $(this).blur();
                } else {
                    $('form#add_exist_user_form').attr('action', $(this).attr('data-url'));
                    $('#access').slideDown('slow');
                    $('#create_user_popup_submit').removeAttr('disabled');
                }
            });

            //var timer_notif = 0;

            // $('body').on('click', '#select_user', function () {
            //     $('body #user-notification').remove();
            //     $('#ita_users').before('<div id="user-notification" class="m-alert m-alert--outline m-alert--outline-2x alert alert-success alert-dismissible fade show" style="display:none;" role="alert">' +
            //         '<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>' +
            //         '<strong>'+GetTranslation('user_create_notification_well_done')+'</strong> '+GetTranslation('user_create_notification_user_added')+'</div>');
            //
            //     $('body #user-notification').show('slow');
            //
            //     var link = $(this).attr('data-url');
            //
            //     clearTimeout(timer_notif);
            //     timer_notif = setTimeout(function() {
            //         window.location.replace(link);
            //     }, 2500);
            // });

            // Password input with generator
            $('.password_input_with_generator').each(function(){
                var password_input = this;
                var gen_btn = $(password_input).parents('.input-group').find('.pass_gen_btn')[0];
                $(gen_btn).on('click', function(){
                    var generated_pass = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    for (var i = 0; i < 12; i++){
                        generated_pass += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    $(password_input).val(generated_pass);
                });
            });

            $('body').on('click', 'button.edit-user', function (e) {
                var user_id = $(this).data('user-id');
                var form = $('#edit_user_popup form#edit_user_form');

                form.prop('action', baseUrl + '/ita/' + tenantId + '/users/update?id=' + user_id);
                form.validate().destroy();

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/users/get-user',
                    data: {
                        id: user_id
                    },
                    success: function (data) {
                        if (data) {
                            form.find('input[name="User[email]"]').val(data.email);
                            form.find('input[name="User[first_name]"]').val(data.first_name);
                            form.find('input[name="User[last_name]"]').val(data.last_name);
                            form.find('input[name="User[phone]"]').val(data.phone);

                            form.find('#user_edit_role_select').empty();
                            form.find('#user_edit_role_select').append('<option data-hidden="true"></option>');

                            $.each(data.roles, function( index, value ) {
                                var data_select = {
                                    value: value.name,
                                    text: value.name.split('.')[1]
                                };

                                if (value.name == data.role)
                                    data_select.selected = 'selected';

                                form.find('#user_edit_role_select').append($('<option>', data_select));
                            });

                            form.find('#user_edit_role_select').selectpicker('refresh');
                        }
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            });

            $('body').on('click', 'button.delete-user', function (e) {
                var user_id = $(this).data('user-id');
                var modal = $('#delete_user_popup');
                $('button#dismiss_user').data('user-id', user_id);

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/users/get-user',
                    data: {
                        id: user_id
                    },
                    success: function (data) {
                        if (data) {
                            var user_name = data.first_name + ' ' + data.last_name;

                            modal.find('.m-widget4__img img')
                                .attr('src', baseUrl + '/' + (data.photo != null && data.photo.length > 0 ? data.photo : 'img/profile_default.png'))
                                .attr('alt', user_name)
                                .attr('title', user_name);
                            modal.find('.m-widget4__title').text(user_name);
                            modal.find('.m-widget4__sub').text(data.email);

                            modal.find('.m--font-brand').text(data.count_tourist);
                        }
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });

                //User delete - Transfer all data to another user select
                $("#user_delete_transfer_to_select").select2({
                    language: tenant_lang,
                    placeholder: GetTranslation('change_user', 'Сhange user'),
                    allowClear: true,
                    tags: false,
                    width: '100%',
                    dropdownParent: $('#user_delete_transfer_to_select').parent(),
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/users/search-users',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                tenant_user: user_id
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data) {
                            var array = [];
                            for (var i in data) {
                                array.push({
                                    id: data[i]['id'],
                                    text: data[i]['email'] + ' (' + data[i].first_name + ' ' + data[i].last_name + ')'
                                });
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }
                });

                $('#user_delete_tags_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['name']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation('delete_modal_bulk_select_tags', 'Select tags'),
                    width: '100%',
                    dropdownParent: $('#user_delete_transfer_to_select').parent()
                });
            });

            $('#not_transfer_data').on('click', function () {
                if ($(this).prop('checked'))
                    $('select#user_delete_transfer_to_select').val(null).trigger('change');

                $('select#user_delete_transfer_to_select').prop('disabled', function(i, v) { return !v; });
            });

            $('button#dismiss_user').on('click', function (e) {
                var user_id = $(this).data('user-id');

                var data = {
                    id: user_id
                };
                var assign_to = $('select#user_delete_transfer_to_select').val();
                var tags = $('select#user_delete_tags_select').val();
                var not_transfer_data = $('#not_transfer_data').prop('checked');

                if (!not_transfer_data) {
                    data.assign_to = assign_to;
                } else {
                    data.not_transfer_data = 1;
                }

                data.tags = tags;

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/users/delete',
                    data: data,
                    success: function (data) {
                        if (data) {
                            $('#delete_user_popup').modal('hide');
                            datatable.reload();
                        }
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            });

            $('#create_user_popup_submit').on('click', function (e) {
                var phone = $('#user_create_phone_input').inputmask('unmaskedvalue');

                $('#user_create_phone_input').val(phone);
            });

            $('#edit_user_popup_submit').on('click', function (e) {
                var phone = $('#user_edit_phone_input').inputmask('unmaskedvalue');

                $('#user_edit_phone_input').val(phone);
            });
        }
    });
})(jQuery);