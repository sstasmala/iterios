var segments_datatable = null;
var segments_contacts_datatable = null;
var segments_type = 'all';
var rules_config = {
    rules: [
        {
            id:'contacts_emails.value',
            label:'Email',
            type:'string',
            operators:['not_contains', 'contains', 'begins_with', 'ends_with', 'equal']
        },
        {
            id:'contacts_phones.value',
            label:'Phone',
            type:'string',
            operators:['not_contains', 'contains', 'begins_with', 'ends_with', 'equal']
        },
        {
            id:'contacts.created_at',
            label:'Date added',
            type:'datetime',
            plugin:'datetimepicker',
            plugin_config:{format:'YYYY-MM-DD'},
            operators:['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
        },
        {
            id:'contacts.updated_at',
            label:'Date updated',
            type:'datetime',
            plugin:'datetimepicker',
            plugin_config:{format:'YYYY-MM-DD'},
            operators:['equal','not_equal','less','greater','less_or_equal','greater_or_equal']
        },
        {
            id:'count_date_added',
            field:'contacts.created_at',
            label:'Now - Date added (days)',
            type:'integer',
            operators:['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
        },
        {
            id:'count_date_updated',
            field:'contacts.updated_at',
            label:'Now - Date updated (days)',
            type:'integer',
            operators:['equal','not_equal','less','greater','less_or_equal','greater_or_equal'],
        }
    ],
    rulesTitle:GetTranslation('segment_create_modal_tag_placeholder'),
    operatorsTitle:GetTranslation('segment_create_modal_operator_placeholder'),
    addRuleTitle:GetTranslation('add'),
    removeRuleTitle:GetTranslation('delete')
};

(function($){
    $(document).ready(function(){
        initSegmentsTable();
        initPageWidgets();
        initEditSegmentModal();
        initRemindersTableFilter();
        initSegmentContactsModal();

        $('#segments-filter>button').on('click',function () {
            segments_datatable.options.data.source.read.params.query.type = $(this).attr('data-filter');
            segments_datatable.reload();
        });
        
        function initSegmentsTable(){
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'segment_name',
                    title: GetTranslation('sidt_col_name_title', 'Name'),
                    overflow: 'visible',
                    sortable: true,
                    template: function(row, index, datatable) {
                        var segment_id = row.segment.id;
                        var out = '<a href="#" class="m-link m--font-bold" data-segment-id="' + segment_id + '" data-toggle="modal" data-target="#update_segment_modal">'+row.segment.name+'</a>';
                        out += (row.segment.type=='system') ? '<i class="la la-exclamation-circle system-status-icon ml-1" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('sidt_sys_segment_notification', 'System segment, editing is disabled') + '"></i>' : '';
                        return out;
                    }
                },
                {
                    field: 'segment_contacts_count',
                    title: GetTranslation('sidt_col_contacts_count_title', 'Contacts'),
                    overflow: 'visible',
                    sortable: true,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        var segment_id = row.id;
                        var segment_name = row.segment.name;
                        return '<a href="#" class="m-link m--font-bold" data-segment-id="' + segment_id + '" data-segment-name="' + segment_name + '" data-toggle="modal" data-target="#segment_contacts_modal">'+row.count_contact+'</a>';
                    }
                },
                {
                    field: 'segment_emails_count',
                    title: GetTranslation('sidt_col_emails_count_title', 'Emails'),
                    overflow: 'visible',
                    sortable: true,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        return row.count_emails;
                    }
                },
                {
                    field: 'segment_phones_count',
                    title: GetTranslation('sidt_col_phones_count_title', 'Phones'),
                    overflow: 'visible',
                    sortable: true,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        return row.count_phones;
                    }
                },
                {
                    field: 'segment_integration',
                    title: GetTranslation('sidt_col_integration_title', 'Integration'),
                    overflow: 'visible',
                    sortable: false,
                    template: function(row, index, datatable) {
                        // Это для имитации разных условий, для того что бы 
                        // показать на фронте все варианы верстки ячейки.
                        // Удалить после подстановки реальных данных!
                        // var randomizer = parseInt(Math.random()*100)%2;
                        var has_integrations = 0;//(randomizer===1);
                        if (has_integrations){
                            return '\
                                <table class="integrations-table">\
                                    <tbody>\
                                        <tr>\
                                            <td class="logo-cell">\
                                                <div class="logo" style="background-image: url( https://awdee.ru/wp-content/uploads/2014/07/WsxhcOQ9yhk.jpg );" title="System name"></div>\
                                            </td>\
                                            <td class="status-cell">\
                                                <i class="la la-toggle-on enabled" title="' + GetTranslation('sidt_enabled', 'Enabled') + '"></i>\
                                            </td>\
                                        </tr>\
                                        <tr>\
                                            <td class="logo-cell">\
                                                <div class="logo-wrap">\
                                                    <div class="logo" style="background-image: url( https://i1.wp.com/awdee.ru/wp-content/uploads/2014/07/ktjxZQGgZRk.jpg );" title="System name"></div>\
                                                </div>\
                                            </td>\
                                            <td class="status-cell">\
                                                <i class="la la-toggle-on enabled" title="' + GetTranslation('sidt_enabled', 'Enabled') + '"></i>\
                                            </td>\
                                        </tr>\
                                        <tr>\
                                            <td class="logo-cell">\
                                                <div class="logo-wrap">\
                                                    <div class="logo" style="background-image: url( https://i2.wp.com/awdee.ru/wp-content/uploads/2014/07/BgNjz_Avjuk.jpg );" title="System name"></div>\
                                                </div>\
                                            </td>\
                                            <td class="status-cell">\
                                                <i class="la la-toggle-on disabled" title="' + GetTranslation('sidt_disabled', 'Disabled') + '"></i>\
                                            </td>\
                                        </tr>\
                                    </tbody>\
                                </table>\
                                ';
                        } else {
                            return  '<div class="row">\
                                        <div class="col-12 m--font-bold">' + GetTranslation('sidt_no_integrations', 'No integrations') + '</div>\
                                    </div>';
                        }
                    }
                },
                {
                    field: 'segment_actions',
                    title: GetTranslation('sidt_col_actions_title', 'Actions'),
                    overflow: 'visible',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                        var html = '\
                            <div class="dropdown ' + dropup + '">\
                                <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                    <i class="la la-ellipsis-h"></i>\
                                </a>\
                                <div class="dropdown-menu dropdown-menu-right">\
                                    <a class="dropdown-item" href="#"><i class="la la-refresh"></i> ' + GetTranslation('sidt_refresh', 'Refresh') + '</a>\
                                    <a class="dropdown-item" href="#"><i class="la la-random"></i> ' + GetTranslation('sidt_synchronize', 'Synchronize') + '</a>\
                                    <a class="dropdown-item" href="#"><i class="la la-truck"></i> ' + GetTranslation('sidt_export', 'Export') + '</a>\
                                </div>\
                            </div>';
                        console.log(row);
                        if(row.segment.type != 'system')
                            html += '<a href="#" data-id="'+row.segment.id+'" class="remove-segment m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '">\
                                <i class="la la-trash"></i>\
                            </a>';
                        return html;
                    }
                }
            ];
            
            segments_datatable = $('#segments_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
//                            url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-index',
                            url: baseUrl + '/ita/' + tenantId + '/segments/get-data',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {
                                    type:segments_type
                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                        $('.remove-segment').on('click',function () {
                            var id = $(this).attr('data-id');
                            swal({
                                title: GetTranslation('delete_question','Are you sure?'),
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                                cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                                reverseButtons: true
                            }).then(function(result){
                                if(result.value){
                                    $.ajax({
                                        url: baseUrl + '/ita/' + tenantId + '/segments/delete-segment',
                                        dataType: 'json',
                                        type: 'POST',
                                        data: {
                                            id: id,
                                            _csrf: $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success: function (data) {
                                            segments_datatable.reload();

                                        },
                                        error: function () {
                                            alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                                        }
                                    });
                                }
                            });
                        })

                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(segments_datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(segments_datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = segments_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_segments_buttons').css({display : "flex"});
                }else{
                    $('#bulk_segments_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-segment-btn').on('click', function(){
                var selected_records = segments_datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('segments_index_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('segments_index_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/delivery-emal/del-index',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                segments_datatable.reload();

                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
        
        function initSegmentContactsModal(){
            var modal = document.getElementById('segment_contacts_modal');
            $(modal).on('shown.bs.modal', function(event){
                var segment_id = parseInt($(event.relatedTarget).data('segment-id'));
                if(segment_id !== null && segment_id !== undefined){
                    // Change modal name
                    $(modal).find('.segment-name').text($(event.relatedTarget).data('segment-name'));
                    // Load datatable
                    initSegmentContactsTable(segment_id);
                } else {
                    alert('Invalid segment id!');
                }
            });
            $(modal).on('hidden.bs.modal', function(event){
                segments_contacts_datatable.destroy();
                segments_contacts_datatable = null;
            });
            
            function initSegmentContactsTable(segment_id){
                var columns = [
                    {
                        field: 'contact_name',
                        title: GetTranslation('sidt_contact_col_name_title', 'Name'),
                        overflow: 'visible',
                        sortable: true,
                        template: function(row, index, datatable) {
                            if(row.last_name == null)
                                row.last_name = '';
                            if(row.first_name == null)
                                row.first_name = '';
                            if(row.middle_name == null)
                                row.middle_name = '';

                            return '<a href="'+baseUrl + '/ita/' + tenantId + '/contacts/view?id='+row.id+'" target="_blank">'+row.last_name + ' ' + row.first_name + ' ' + row.middle_name + '</a>';
                        }
                    },
                    {
                        field: 'segment_contacts_contacts',
                        title: GetTranslation('sidt_contact_col_contacts_title', 'Contacts'),
                        overflow: 'visible',
                        sortable: true,
//                        width: 250,
                        template: function (row, index, datatable) {
                            var contacts = [];
                            var phones = [];
                            if(row.phones != null) {
                                for (P in row.phones) {
                                    phones.push(row.phones[P]);
                                }
                            }
                            var emails = [];
                            if(row.emails != null) {
                                for (E in row.emails) {
                                    emails.push(row.emails[E]);
                                }
                            }
                            if(phones != null && phones.length >0)
                                contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(phones[0].value))+'</div>';
                            if(emails != null &&  emails.length >0)
                                contacts.push( '<div class="contact-row"><i class="far fa-envelope"></i> '+emails[0].value)+'</div>';


                            phones.forEach(function (item,i) {
                                if(i!=0)
                                    contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(item.value))+'</div>';
                            });

                            emails.forEach(function (item,i) {
                                if(i!=0)
                                    contacts.push( '<div class="contact-row"><i class="far fa-envelope"></i> '+item.value)+'</div>';
                            });

                            var first = '';
                            var additional = '';
                            console.log(contacts);
                            contacts.forEach(function (item,i) {
                                if(i<2) {
                                    if (i < 1)
                                        item+='</div>';
                                    first += '<div class="">'+item;
                                }
                                else
                                    additional+='<li class="dropdown-item">'+item+'</li>';
                            });

                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            var toggle = '<div class="dropdown ' + dropup + '">' +
                                '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"> ' +
                                '<i class="la la-ellipsis-h"></i>' +
                                '</a>' +
                                '<ul class="dropdown-menu additional-list">'
                                +additional +
                                '</ul>' +
                                '</div>';
                            if(additional != '')
                                first += toggle;
                            first += '</div>';
                            return first;
                        }
                    },
                    {
                        field: 'segment_contacts_add_date',
                        title: GetTranslation('sidt_contact_col_add_date_title', 'Add date'),
                        overflow: 'visible',
                        sortable: true,
                        template: function(row, index, datatable) {
                            return '23.06.2018';
                        }
                    }
                ];

                segments_contacts_datatable = $('#segments_contacts_datatable').mDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
    //                            url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-index',
                                url: baseUrl + '/ita/' + tenantId + '/segments/get-segment-data',
                                method: 'GET',
                                params: {
                                    _csrf: $('meta[name="csrf-token"]').attr('content'),
                                    query: {
                                        segment_id: segment_id
                                    },
                                    search: {
                                        filter_responsible: userId
                                    }
                                    // custom query params
                                },
                                map: function (raw) {

                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                }
                            }
                        },
                        pageSize: 10,
                        saveState: {
                            cookie: false,
                            webstorage: false
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    layout: {
                        theme: 'default',
                        class: '',
                        scroll: false,
                        footer: false
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 400
                    },
                    rows: {
                        afterTemplate: function (row, data, index) {
                            // Init popovers
                            mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                        },
                        callback: function () {

                        },
                        // auto hide columns, if rows overflow. work on non locked columns
                        autoHide: false
                    },
                    columns: columns,
                    toolbar: {
                        layout: ['pagination', 'info'],
                        placement: ['bottom'],  //'top', 'bottom'
                        items: {
                            pagination: {
                                type: 'default',

                                pages: {
                                    desktop: {
                                        layout: 'default',
                                        pagesNumber: 6
                                    },
                                    tablet: {
                                        layout: 'default',
                                        pagesNumber: 3
                                    },
                                    mobile: {
                                        layout: 'compact'
                                    }
                                },

                                navigation: {
                                    prev: true,
                                    next: true,
                                    first: true,
                                    last: true
                                },

                                pageSizeSelect: [10, 20, 30, 50, 100]
                            },

                            info: true
                        }
                    },
                    translate: {
                        records: {
                            processing: GetTranslation('datatable_data_processiong','Please wait...'),
                            noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                        },
                        toolbar: {
                            pagination: {
                                items: {
                                    default: {
                                        first: GetTranslation('pagination_first','First'),
                                        prev: GetTranslation('pagination_previous','Previous'),
                                        next: GetTranslation('pagination_next','Next'),
                                        last: GetTranslation('pagination_last','Last'),
                                        more: GetTranslation('pagination_more','More pages'),
                                        input: GetTranslation('pagination_page_number','Page number'),
                                        select: GetTranslation('pagination_page_size','Select page size')
                                    },
                                    info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                                }
                            }
                        }
                    }
                });

                /* DATATABLE EVENTS */

                // Init event
                $(segments_contacts_datatable).on('m-datatable--on-init', function(e){

                });
                // Layout updated event
                $(segments_contacts_datatable).on('m-datatable--on-layout-updated', function(e){

                });
            }
            
            function beautiful_phone(phone) {
                if (phone != null && phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
                    var result = phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );
                    return '+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4];
                }
                return (phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone);
            }
        }
        
        function initPageWidgets(){
            // Segment module select
            var select = $('#segment_module_select2').get()[0];
            $(select).select2({
                placeholder: GetTranslation('segment_create_modal_module_placeholder', 'Select module'),
                language: tenant_lang,
                width: '100%',
                dropdownParent: $(select).parent()
            });
            
            // New segment rules repeater
            $('#segment_rules_repeater').repeater({
                initEmpty: false,
                show: function() {
                    // Init select2
                    $(this).find('.m_selectpicker').selectpicker({
                        width: '100%'
                    });
                    // Show row
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    if(confirm(GetTranslation('delete_element_confirmation_text', 'Are you sure you want to delete this element?'))) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
            // Update segment rules repeater
            $('#update_segment_rules_repeater').repeater({
                initEmpty: false,
                show: function() {
                    // Init select2
                    $(this).find('.m_selectpicker').selectpicker({
                        width: '100%'
                    });
                    // Show row
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    if(confirm(GetTranslation('delete_element_confirmation_text', 'Are you sure you want to delete this element?'))) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
        }
        function initEditSegmentModal(){
            var modal = $('#update_segment_modal').get()[0];
            
            $(modal).on('show.bs.modal', function (event) {
                var segment_id = parseInt($(event.relatedTarget).data('segmentId'));
                if(segment_id !== null && segment_id !== undefined){
                    loadAllFields(segment_id);
                } else {
                    alert('Invalid segment id!');
                }
            });
            
            $(modal).on('hidden.bs.modal', function (event) {
                showPreloader();
                clearAllFields();
            });
            
            function hidePreloader(){
                $(modal).find('.preloader-wrap').each(function(){
                    $(this).addClass('d-none');
                });
                $(modal).find('#segment_update_form').each(function(){
                    $(this).removeClass('d-none');
                });
            }
            
            function showPreloader(){
                $(modal).find('#segment_update_form').each(function(){
                    $(this).addClass('d-none');
                });
                $(modal).find('.preloader-wrap').each(function(){
                    $(this).removeClass('d-none');
                });
            }
            
            function loadAllFields(segment_id){
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/segments/get-segment',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        id: segment_id,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                        // Fill id input
                        $(modal).find("input[name='Segments[id]']").val(data.id);
                        // Fill name input
                        $(modal).find("input[name='Segments[name]']").each(function(){
                            $(this).val(data.name);
                        });

                        $(modal).find("input[name='Segments[rules]']").val(data.rules);
                        // Fill module select2
                        $(modal).find("select[name='Segments[modules]']").each(function(){
                            $(this).find('option[value="'+data.modules+'"]').attr('selected',true);
                            var select = this;

                            $(select).select2({
                                placeholder: GetTranslation('segment_create_modal_module_placeholder', 'Select module'),
                                language: tenant_lang,
                                width: '100%',
                                dropdownParent: $(select).parent()
                            });
                            if($(this).attr('data-inited')==1)
                                return;
                            $(this).attr('data-inited',1);
                            var constructor = undefined;
                            $('#update_segment_rules_repeater').empty();
                            $(this).on('change',function () {
                                $('#update_segment_rules_repeater').empty();
                                constructor = ConditionsConstructor($('#update_segment_rules_repeater'),
                                    $(modal).find('input[name="Segments[rules]"]'),rules_config);
                                $(modal).off('submit').on('submit',function (e) {
                                    constructor.updateValue();
                                });
                            })
                            $(select).val(data.modules).trigger('change');
                            if(constructor!=undefined)
                                constructor.renderFromRules();
                        });
                        if(data.type=='system') {
                            $(modal).find("input[name='Segments[id]']").prop('disabled',true);
                            $(modal).find("input[name='Segments[name]']").prop('disabled',true);
                            $(modal).find("input[name='Segments[rules]']").prop('disabled',true);
                            $(modal).find("select[name='Segments[modules]']").prop('disabled',true);
                            // $(modal).find('.segment-rules-block').off('click').on('click',function (e) {
                            //     console.log('click');
                            //     e.stopPropagation();
                            // });
                            var fake_div = document.createElement('div');
                            $(fake_div).css({
                                "position" : "absolute",
                                "top" : "0",
                                "left" : "0",
                                "width" : "100%",
                                "height" : "100%",
                                "background" : "#FFF",
                                "z-index" : "1",
                                "opacity" : "0.2"
                            });

                            $(fake_div).attr('id', 'segment_form_fake_div');
                            $(modal).find('.segment-rules-block').css({"position" : "relative"}).append(fake_div);
                            $(modal).find('#segment_update_submit').hide();
                        } else {
                            console.log('Enable');
                            $(modal).find("input[name='Segments[id]']").prop('disabled',false);
                            $(modal).find("input[name='Segments[name]']").prop('disabled',false);
                            $(modal).find("input[name='Segments[rules]']").prop('disabled',false);
                            $(modal).find("select[name='Segments[modules]']").prop('disabled',false);
                            $(modal).find('#segment_form_fake_div').remove();
                            $(modal).find('#segment_update_submit').show();
                        }


                        // TODO: Fill repeater
                        
                        // Show form
                        hidePreloader();
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            }
            
            function clearAllFields(){
                // Clear id input
                $(modal).find("input[name='Segment[id]']").val('');
                // Clear name input
                $(modal).find("input[name='Segment[name]']").each(function(){
                    $(this).val('');
                });
                // Clear module select
                $(modal).find("select[name='Segment[module]']").each(function(){
                    $(this).val('');
                    $(this).select2('destroy');
                });
                // TODO: Clear repeater
            }
        }
        
        function initRemindersTableFilter(){
            var filter = $('#segments-filter').get()[0];
            var portlet = $('#segments_datatable_portlet').get()[0];
            if((filter !== undefined) && (filter !== null)){
                $(filter).find('button').on('click', function(){
                    var clicked_btn = this;
                    var filter_type = $(clicked_btn).data('filter');
                    $(filter).find('button').removeClass('btn-brand').addClass('btn-secondary');
                    $(clicked_btn).removeClass('btn-secondary').addClass('btn-brand');
                    // Show/hide content parts
                    // $(portlet).find('.reminders-index-portlet-content').removeClass('active');
                    // $(portlet).find('.reminders-index-portlet-content[data-content-part-'+filter_type+'="true"]').addClass('active');
                });
            }
        }

        $('#create_segment_modal').on('show.bs.modal',function () {
            $('#segment_rules_repeater').empty();
            $(this).find('input[name="Segments[rules]"]').val('');
            $('#segment_module_select2').each(function () {
               if($(this).attr('data-inited')==1)
                   return;
               $(this).attr('data-inited',1);
               $(this).on('change',function () {
                   $('#segment_rules_repeater').empty();
                   $('#create_segment_modal').find('input[name="Segments[rules]"]').val('');
                   var constructor = ConditionsConstructor($('#segment_rules_repeater'),
                       $('#create_segment_modal').find('input[name="Segments[rules]"]'),rules_config);
                   $('#segment_create_form').off('submit').on('submit',function (e) {
                       constructor.updateValue();
                   });
               })
            });
        });
    });
})(jQuery);