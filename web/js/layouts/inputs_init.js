/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
var add_count = 0;


var inputsInitor = {

    initOpeners:function () {
        $('[data-opener="true"]').each(function () {
            var opener = this;
            var is_already_inited = $(opener).data('inited');
            if (is_already_inited !== 'true') {
                var target_selector = $(opener).data('target');
                $(opener).on('click', function () {
                    activateTogleEvent(opener, target_selector);
                });
                activateTogleEvent(opener, target_selector);
                $(opener).data('inited', 'true');
            }
        });

        function makeOpen(opener, target) {
            $(target).stop().fadeIn();
            $(target).removeClass('close-by-opener').addClass('open-by-opener');
            $(opener).find('[data-show_decorator="for-close"]').hide();
            $(opener).find('[data-show_decorator="for-open"]').show();
            $(opener).data('status', 'open');
        }

        function makeClose(opener, target) {
            $(target).stop().fadeOut();
            $(target).removeClass('open-by-opener').addClass('close-by-opener');
            $(opener).find('[data-show_decorator="for-close"]').show();
            $(opener).find('[data-show_decorator="for-open"]').hide();
            $(opener).data('status', 'close');
        }

        function activateTogleEvent(opener, target_selector) {
            var status = $(opener).data('status');
            $(target_selector).each(function () {
                var target = this;
                switch (status) {
                    case 'close':
                        makeOpen(opener, target);
                        break;
                    case 'open':
                        makeClose(opener, target);
                        break;
                }
            });
        }

    },
    initSelect2:function () {
        if(this.modal === undefined)
            return;
        var modal = this.modal;
        //==============================================================================================================
        //Airport
        $(modal).find('.select2-airport').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-airports',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            var value = data[i];
                            value.text = value.title;
                            array.push(value);
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("rv_page_airport_select_placeholder", "Select airport");
                    }
                    if(data.iata_code === undefined)
                        return data.text;
                    return data.text + ' (' + data.iata_code + ') - <i>'+data.city_title+', '+data.country_title+'</i>';
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 2,
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (perm) {
                    // if (perm.loading) {
                    //     return perm.text;
                    // }
                    var markup = '';
                    if(perm.iata_code === undefined)
                        markup = '<div>' + perm.text + '</div>';
                    else
                        markup = '<div>' + perm.text + ' (' + perm.iata_code + ') - <i>'+perm.city_title+', '+perm.country_title+'</i></div>';
                    return markup;
                },
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                placeholder: GetTranslation("rv_page_airport_select_placeholder", "Select airport")
            });
        });
        //==============================================================================================================
        //City
        $(modal).find('.select2-city').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-cityes-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data,params) {
                        var array = [];
                        params.page = params.page || 1;
                        for (var i in data.results) {
                            array.push({id: data.results[i]['id'], text: data.results[i]['title']});
                        }
                        // Make Add new hotel button if no results
                        if(array.length === 0){
                            array = [{
                                new_city: true
                            }];
                        }
                        return {
                            results: array,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("rv_page_city_select_placeholder", "Select city");
                    }
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (perm) {
                    if(perm.new_city){
                        return '<a href="#" class="btn btn-secondary btn-sm m-btn m-btn m-btn--icon mb-2" data-toggle="modal" data-target="#m_modal_create_new_city">\
                                    <span>\
                                        <i class="fa fa-plus"></i>\
                                        <span>' + GetTranslation('add_new', 'Add new') + '</span>\
                                    </span>\
                                </a>';
                    }
                    return perm.text;;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                placeholder: GetTranslation("rv_page_city_select_placeholder", "Select city")
            });
        });
        //==============================================================================================================
        // Country
        $(modal).find('.select2-country').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-countries-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            array.push({id: data[i]['id'], text: data[i]['title']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("rv_page_country_select_placeholder", "Select country");
                    }
                    return data.text;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                placeholder: GetTranslation("rv_page_country_select_placeholder", "Select country")
            });
        });
        //==============================================================================================================
        //Supplier
        $(modal).find('.select2-supplier').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-suppliers-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation('rv_page_suppliers_placeholder', 'Enter suppliers');
                    }
                    return data.text;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 0,
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                placeholder: GetTranslation('rv_page_suppliers_placeholder', 'Enter suppliers')
            });
        });
        //==============================================================================================================
        // Departure city
        $(modal).find('.select2-departure-city').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-departure-cityes-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            if (array[data[i]['country_id']] === undefined)
                                array[data[i]['country_id']] = {text: data[i]['country_title'], children: []};
                            array[data[i]['country_id']]['children'].push({
                                id: data[i]['id'],
                                text: data[i]['title']
                            });
                        }
                        var res_data = [];
                        for (var i in array) {
                            res_data.push(array[i]);
                        }
                        return {
                            results: res_data
                        };
                    }
                },
                // templateSelection: function (data) {
                //     if (data.id === '') {
                //         return GetTranslation("rv_page_departure_city_select_placeholder", "Select departure city");
                //     }
                //     return data.text;
                // },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                placeholder: GetTranslation("rv_page_departure_city_select_placeholder", "Select departure city")
            });
        });
        //==============================================================================================================
        // Hotel
        $(modal).find('.select2-hotel').each(function () {
            if ($(this).attr('data-inited') !== undefined || $(this).prop('localName') != 'select')
                return;
            $(this).attr('data-inited', 1);
            var select = this;
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-hotels-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            country:($(modal).find('.select2-country').val()!=undefined
                            && $(modal).find('.select2-country').val()!=''
                                ?$(modal).find('.select2-country').val():null),
                            page: params.page || 1,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data,params) {
                        var array = [];
                        hotels_data = data.results;
                        params.page = params.page || 1;
                        for (var i in data.results) {
                            array.push({
                                id: data.results[i]['id'],
                                text: data.results[i]['title'],
                                city_title: data.results[i]['city_title'],
                                country_title: data.results[i]['country_title'],
                                hotel_category_id:data.results[i]['hotel_category_id'],
                                hotel_category_title:data.results[i]['hotel_category_title']
                            });
                        }
                        // Make Add new hotel button if no results
                        if(array.length === 0){
                            array = [{
                                new_hotel: true
                            }];
                        }
                        return {
                            results: array,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("rv_page_hotel_select_placeholder", "Select hotel");
                    }
                    if(data.country_title !== undefined)
                        return data.text+' *'+data.hotel_category_title+', '+data.country_title+', '+data.city_title;
                    return data.text;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 4,
                escapeMarkup: function (markup) {
                    return markup;
                },
                width: '100%',
                dropdownParent: $(select).closest('.modal-body'),
                templateResult: function (perm) {
                    if (perm.loading) {
                        return perm.text;
                    }
                    if(perm.new_hotel){
                        return '<a href="#" class="btn btn-secondary btn-sm m-btn m-btn m-btn--icon mb-2" data-toggle="modal" data-target="#m_modal_create_new_hotel">\
                                    <span>\
                                        <i class="fa fa-plus"></i>\
                                        <span>' + GetTranslation('add_new', 'Add new') + '</span>\
                                    </span>\
                                </a>';
                    }
                    var markup = '<div><b>' + perm.text + '</b> <i class="fa fa-star-o"></i>'+perm.hotel_category_title+'</div>';
                    markup += '<div style="font-size: small"><i>' + perm.country_title + ', ' + perm.city_title + '</i></div>';
                    return markup;
                },
                placeholder: GetTranslation("rv_page_hotel_select_placeholder", "Select hotel")
            });
        });
    },
    initAjaxes:function(){
        if(this.modal === undefined)
            return;
        var modal = this.modal;
        // Food
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/requests/get-food-options',
            method: 'POST',
            dataType: 'json',
            data: {
                search: '',
                type: 'public',
                _csrf: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                for (var i in data) {
                    $(modal).find('select.food-select').each(function () {
                        if ($(this).find('option[value="' + data[i]['id'] + '"]').length == 0)
                            $(this).append('<option value="' + data[i]['id'] + '">' + data[i]['title'] + '</option>');
                    })
                }
                $(modal).find('select.food-select').selectpicker('refresh');
            }
        });
        // Currency
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/ajax/get-usage-currency',
            method: 'POST',
            dataType: 'json',
            data: {
                type: 'public',
                _csrf: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                var selected = '';
                var select = $(modal).find('select.currency-select');
                for (var i in data.usage_currencies) {
                    select.each(function () {
                        // if($(this).attr('data-inited')==1)
                        //     return;
                        // $(this).attr('data-inited',1);
                        if ($(this).val() != '')
                            selected = $(this).val();
                        if ($(this).val() != data.usage_currencies[i]['id'])
                            $(this).append('<option value="' + data.usage_currencies[i]['id'] + '">' + data.usage_currencies[i]['iso_code'] + '</option>');
                    });

                }
                if (data.main_currency !== undefined && data.main_currency !== null) {
                    $(modal).find('select.currency-select').each(function () {
                        if ($(this).val() == '') {
                            if (selected != '')
                                $(this).val(selected);
                            else
                                $(this).val(data.main_currency.id);
                        }

                    });

                }

                $(modal).find('select.currency-select').selectpicker('refresh');
                if (select.length > 0) {
                    $(modal).find('.currency_indicator').text($(select[0]).find('option:selected').text());
                }


                // Change currency in anyone select event
                $(modal).find('select.currency-select').on('change', function () {
                    var changed_select = this;

                    var select_val = parseInt($(this).val());
                    // Change currncy code in inputs groups with currency indicator
                    var val_text = $(this).find('option[value="' + select_val + '"]').text();

                    $(modal).find('.currency_indicator').text(val_text);
                    // Change other currency selects in modal
                    $(modal).find('select.currency-select').each(function () {
                        if (changed_select !== this) {
                            $(this).val(select_val).selectpicker('refresh');
                        }
                    });
                });
            }
        });
        // Hotel category
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/requests/get-hotel-category',
            method: 'POST',
            dataType: 'json',
            data: {
                search: '',
                type: 'public',
                _csrf: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                for (var i in data) {
                    $(modal).find('select.star_rating-select').each(function () {
                        if ($(this).find('option[value="' + data[i]['id'] + '"]').length == 0)
                            $(this).append('<option value="' + data[i]['id'] + '">' + data[i]['title'] + '</option>');
                    });
                }
                $(modal).find('select.star_rating-select').selectpicker('refresh');
            }
        });
    },
    initOtherInputs:function () {
        if(this.modal === undefined)
            return;
        var modal = this.modal;
        // Date input
        $(modal).find('.datepicker-input').each(function () {
            var picker = this;
            $(picker).datepicker({
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
        });

        // $(modal).find('select.dropdown-select').each(function () {
        //     if($(this).attr('data-inited')!=1) {
        //         $(this).on('change',function () {
        //             console.log($(this));
        //             var input = $(this).closest('.service-item').find('input[type="hidden"]');
        //             input.val($(this).find('option:selected').index());
        //         });
        //         $(this).attr('data-inited',1);
        //     }
        // });

        // Multiselect
        $(modal).find('.m_selectpicker').each(function () {
            $(this).selectpicker({
                width: '100%'
            });
        });
        // Text
        $(modal).find('.text-textarea').each(function () {
            autosize(this);
        });

        $(modal).find('.text_req_type').each(function () {
            placeholder($this);
        });

        var saveTouristsCount =  this.saveTouristsCount;

        // Children select
        $('.init-children-ages').each(function () {
            var wraper = this;
            var element = $(this);
            saveTouristsCount(element);
            var inputs = $(wraper).find('.children-age-input').length;
            if (inputs > 0) {
                $(wraper).find('.children-age-input').off('keyup').on('keyup', function () {
                    saveTouristsCount(element);
                });
            }
            else
                $(wraper).find('.children-age').slideUp(0);  // Hide on start
            $(wraper).find('select.request_create__adult_count_select').on('change', function () {
                saveTouristsCount(element);
            });
            $(wraper).find('select.request_create__children_count_select').on('change', function () {
                var select = this;
                var chidlen_count = parseInt($(select).val());
                var inputs_row = $(wraper).find('.children-age').get()[0];
                var inputs_block = $(inputs_row).find('.generated_inputs_block').get()[0];
                if (chidlen_count > 0) {
                    // Delete old inputs
                    $(inputs_block).find('.child-age-col').remove();
                    // Generate inputs
                    var inputs_html = '';
                    for (var i = 0; i < chidlen_count; i++) {
                        inputs_html += '<div class="col-auto child-age-col mb-3" style="min-width: 50px;max-width: 75px;">\
                                            <input type="text"  class="children-age-input form-control m-input age_mask" maxlength="2">\
                                        </div>';
                    }

                    // Show
                    $(inputs_block).append(inputs_html);
                    $(inputs_row).stop().slideDown(400);
                    $(inputs_block).find(".age_mask").inputmask({
                        "mask": "9{1,2}"
                    });
                    $(wraper).find('.children-age-input').off('keyup').on('keyup', function () {
                        saveTouristsCount(element);
                    });
                    saveTouristsCount(element);
                } else {
                    // Close
                    $(inputs_row).stop().slideUp(400);
                    // Delete inputs
                    $(inputs_block).find('.child-age-col').slideUp(200, function () {
                        $(this).remove();
                    });
                    saveTouristsCount(element);
                }
            });
            $(wraper).removeClass('init-children-ages');
        });
        // Time
        var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
        $(modal).find('.time-picker').timepicker({
            minuteStep: 5,
            showSeconds: false,
            showMeridian: format_AM_PM,
            defaultTime: ''
        });
        // Bootstrap switch
        $('.need-switcher-init').each(function () {
            var on = $(this).data('on');
            var off = $(this).data('off');

            $(this).bootstrapSwitch();
            $(this).removeClass('need-switcher-init');
        });

        // Payment to supplier
        $(modal).find('.payment_to_supplier-input').inputmask({
            // "mask": "9{+}.99"
            "regex": '^-?\\d+(\\.\\d{1,2})?$'
        });

        // Services price
        $(modal).find('.services_price-input').inputmask({
            "regex": '^-?\\d+(\\.\\d{1,2})?$'
        });
        var Component = this;
        // Init commission input with dropdown
        $(modal).find('.extra-dropdown-option').each(function(){
            var group = this;
            var hidden_input_selector = $(group).data('for-input-selector');
            var hidden_input = $(group).find(hidden_input_selector)[0];
            var drop_down_btn = $(group).find('button.btn')[0];
            $(this).find('.value_input').inputmask({
                // "mask": "9{+}.99"
                "regex": '^-?\\d+(\\.\\d{1,2})?$'
            });
            $(group).find('.dropdown-menu a').each(function(){
                var variant = this;
                $(variant).unbind('click');
                $(variant).on('click', function(){
                    var variant_name = $(this).text();
                    var variant_val = $(this).data('value');
                    $(modal).find('.extra-dropdown-option').each(function () {
                        $(this).find('.type_input').val(variant_val);
                        var btn = $(this).find('button.btn');
                        btn.text(variant_name);
                        if(variant_val == 'cur')
                            btn.addClass('currency_indicator');
                        else
                            btn.removeClass('currency_indicator');
                        Component.updateCommissionValue($(group));
                    }).find('.type_input').trigger('change');

                    // $(hidden_input).val(variant_val).trigger('change');
                    // $(drop_down_btn).text(variant_name);
                    // if(variant_val == 'cur')sas
                    //     $(drop_down_btn).addClass('currency_indicator');
                    // else
                    //     $(drop_down_btn).removeClass('currency_indicator');
                });
            });
        });

        mApp.initPopover($(modal).find('[data-toggle="m-popover"]'));
    },
    initChangers:function () {
        if(this.modal === undefined)
            return;
        var modal = this.modal;
        function countryChange() {
            if ($(this).find('option:selected').length > 0) {
                if ($(this).val() != '')
                    main_country = $(this).clone();
                var selector = $('select.select2-country');
                var self = this;
                var value = $(this).val();
                var selected = $(this).find('option:selected').clone();
                selector.each(function () {
                    if (this == self)
                        return;
                    if ($(this).find('option[value="' + value + '"]').length > 0)
                        $(this).val(value).selectpicker('refresh');
                    else {
                        $(this).append(selected).val(value).selectpicker('refresh');
                    }
                })

            }
        }

        function hotelsChange() {
            var id = $(this).val();

            hotel = hotels_data.find(function (element, index, array) {
                return element.id == id;
            });
            if (hotel !== undefined) {
                $(this).closest('.row').find('select.select2-country').each(function () {
                    $(this).append('<option selected value="' + hotel.country_id + '">' + hotel.country_title + '</option>').trigger('change')
                });
                $(this).closest('.row').find('select.select2-city').each(function () {
                    $(this).append('<option selected value="' + hotel.city_id + '">' + hotel.city_title + '</option>')
                });
                $(this).closest('.row').find('select.star_rating-select').each(function () {
                    $(this).find('option[value="' + hotel.hotel_category_id + '"]').attr('selected', true).trigger('change');
                });

            }
        }

        function dayChange() {
            var start = $(this).closest('.row').find('.date-start-input').val();
            var end = $(this).closest('.row').find('.date-end-input').val();
            start = moment(start, date_format);
            end = moment(end, date_format);
            var nights = end.diff(start, 'days');
            $(this).closest('.row').find('.nigths-count-input').val(nights);
        }

        function dayStartEndChange() {
            var self = this;
            var value = $(this).val();
            $(modal).find('.date-end-input').each(function () {
                if (this !== self)
                    $(this).val(value).trigger('change');
            });
        }

        //Country selection
        $('#service-default-details-block,#service-custom-details-block').find('select.select2-country')
            .off('change', countryChange).on('change', countryChange);

        $(modal).find('.date-start-input,.date-end-input').off('change', dayChange).on('change', dayChange);

        $('#service-default-details-block,#service-custom-details-block').find('.date-start-input,.date-end-input')
            .off('change', dayStartEndChange).on('change', dayStartEndChange);

        //hotel selection
        $('.select2-hotel').off('change', hotelsChange).on('change', hotelsChange);

        var Component = this;

        function calculateFromCommission(elem)
        {
            var commission = elem;
            var price = $(modal).find('.services_price-input');
            var payment = $(modal).find('.payment_to_supplier-input');
            var profit_input = $(modal).find('input.profit-total');
            var profit_text = $(modal).find('span.profit-total');


            commission = JSON.parse(commission.val());
            if(commission.type == 'per')
                var pr = parseFloat(price.val())/100 * parseFloat(commission.value);
            else
                var pr =  parseFloat(commission.value);
            var pm =  parseFloat(price.val()) - pr;


            if(!isNaN(pr.toFixed(2))) {
                profit_text.text(pr.toFixed(2));
                profit_input.val(pr.toFixed(2));
            } else {
                profit_text.text(0.00);
                profit_input.val(0.00);
            }
            payment.val(pm.toFixed(2));
        }

        function calculateFromPayment(elem)
        {
            var commission = $(modal).find('.commission-input');
            var price = $(modal).find('.services_price-input');
            var payment = elem;
            var profit_input = $(modal).find('input.profit-total');
            var profit_text = $(modal).find('span.profit-total');

            var co = JSON.parse(commission.val());

            var pr = parseFloat(price.val()) - parseFloat(payment.val());
            if(co.type == 'per')
                co.value = parseFloat(payment.val())/(pr);
            else
                co.value = parseFloat(price.val()) - parseFloat(payment.val());

            if(!isNaN(pr.toFixed(2))) {
                profit_text.text(pr.toFixed(2));
                profit_input.val(pr.toFixed(2));
            } else {
                profit_text.text(0.00);
                profit_input.val(0.00);
            }
            commission.parent().find('.value_input').val(co.value);
            commission.val(JSON.stringify(co));
        }

        function calculateFromPrice(elem)
        {
            var commission = $(modal).find('.commission-input');
            var price = elem;
            var payment = $(modal).find('.payment_to_supplier-input');
            var profit_input = $(modal).find('input.profit-total');
            var profit_text = $(modal).find('span.profit-total');

            commission = JSON.parse(commission.val());
            if(commission.type == 'per')
                var pr = parseFloat(price.val())/100 * parseFloat(commission.value);
            else
                var pr = parseFloat(commission.value);
            var pm =  parseFloat(price.val()) - pr;

            if(!isNaN(pr.toFixed(2))) {
                profit_text.text(pr.toFixed(2));
                profit_input.val(pr.toFixed(2));
            } else {
                profit_text.text(0.00);
                profit_input.val(0.00);
            }

            payment.val(pm.toFixed(2));
        }



        $(modal).find('.commission-input').parent().find('.value_input').off('keyup').on('keyup',function () {
            var value = $(this).val();
            $(modal).find('.commission-input').each(function () {
                $(this).parent().find('.value_input').val(value);
                Component.updateCommissionValue($(this).parent());
            });
            calculateFromCommission($(this).parent().find('.commission-input'));
        });

        $(modal).find('.commission-input').parent().find('.type_input').off('change').on('change',function () {
            $(modal).find('.commission-input').each(function () {
                Component.updateCommissionValue($(this).parent());
            });
            calculateFromCommission($(this).parent().find('.commission-input'));
        });

        $(modal).find('.payment_to_supplier-input').off('keyup').on('keyup',function () {
            calculateFromPayment($(this));
        });
        $(modal).find('.services_price-input').off('keyup').on('keyup',function () {
            calculateFromPrice($(this));
        });

        var inputs = $('#service-default-details-block').find('select.select2-country');
        if (inputs.length > 0 && inputs.find('option:selected').length > 0)
            Component.main_country = inputs.find('option:selected').clone();
        else {
            inputs = $('#service-custom-details-block').find('select.select2-country');
            if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                Component.main_country = inputs.find('option:selected').clone();
        }

    },
    updateCommissionValue:function(elem) {
        var value = elem.find('.value_input').val();
        if(value == '' || value === undefined)
            value = 0;
        var type = elem.find('.type_input').val();
        elem.find('.commission-input').val(JSON.stringify({value:value,type:type}));
    },
    saveTouristsCount:function (element) {
        var data = {adults_count: 0, children_count: 0, children_age: []};
        data.adults_count = element.find('select.request_create__adult_count_select').val();
        data.children_count = element.find('select.request_create__children_count_select').val();
        element.find('.children-age-input').each(function () {
            data.children_age.push($(this).val());
        });
        element.find('.tourists-count').val(JSON.stringify(data));
    },
    main_country:undefined,
    initInputs:function (modal) {
        this.modal = modal;
        this.initSelect2();
        this.initAjaxes();
        this.initOtherInputs();
        this.initChangers();
        this.initOpeners();
    }
};

function setDefault(element) {
    var select = element.find('select.select2-country');
    select.each(function () {
        if (($(this).val() == '' || $(this).val() == null) && inputsInitor.main_country != undefined)
            $(this).append(inputsInitor.main_country).trigger('change');
    });
}

function initDynamicInputs(modal) {


    inputsInitor.initInputs(modal);


    // function dayEndChange()
    // {
    //     $(modal).find('.date-end-input').val($(this).val());
    // }
    // $(modal).find('.date-end-input').off('change',dayEndChange).on('change',dayEndChange);
}