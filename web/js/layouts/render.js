/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

function service_renderAirport(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<select name="' + name + '[' + field['id'] + ']" class="form-control m-select2 select2-airport">' +
            ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '') +
            '</select>';
}
//======================================================================================================================
function service_renderCity(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<select name="' + name + '[' + field['id'] + ']" class="form-control m-select2 select2-city">' +
            ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '') +
            '</select>';
}
//======================================================================================================================
function service_renderCountry(field,name) {
    return  '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<select name="' + name + '[' + field['id'] + ']" class="form-control m-select2 select2-country">' +
            ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '') +
            '</select>';
}
//======================================================================================================================
function service_renderDate(field,name) {
    return  '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input datepicker-input" placeholder="'
        + GetTranslation('rv_page_date_placeholder', 'Select date') + '">';
}
//======================================================================================================================
function service_renderDateEnding(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control date-end-input m-input datepicker-input" placeholder="'
        + GetTranslation('rv_page_date_ending_placeholder', 'Select ending date') + '">';
}
//======================================================================================================================
function service_renderDateStarting(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control date-start-input m-input datepicker-input" placeholder="'
        + GetTranslation('rv_page_date_starting_placeholder', 'Select starting date') + '">';
}
//======================================================================================================================
function service_renderMultiselect(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    html += '<select name="' + name + '[' + field['id'] + '][]" multiple class="form-control m-bootstrap-select m_selectpicker" title="' + GetTranslation('rv_page_multiselect_placeholder', 'Select value') + '">';
    var variables = [];
    try {
        variables = JSON.parse(field.variables);
    }
    catch(err) {
        // values.push(field['value']);
    }
    var values = [];
    if (field['value'] !== undefined && field['value'] !== null && field['value'] !== ''){
        try {
            values = JSON.parse(field['value']);
        }
        catch(err) {
            values.push(field['value']);
        }

    }
    if(!Array.isArray(values))
        values = [values];
    var i = 0;
    for (key in variables) {
        html += '<option value="' + i + '" ' + ((values.indexOf(i+"") >= 0) ? 'selected' : '') + '>'
            + variables[key] + '</option>';
        i++;
    }
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderRadio(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    var variables = [];
    try {
        variables = JSON.parse(field.variables);
    }
    catch(err) {
        // values.push(field['value']);
    }
    var i = 0;
    for (key in variables) {
        html += '<label class="m-radio mr-3">\
                                            <input type="radio" name="' + name + '[' + field['id'] + ']" value="' + i + '" '
            + ((field['value'] !== undefined && field['value'] == i) ? 'checked' : '') + '>'
            + variables[key] + '\
                                            <span></span>\
                                        </label>';
        i++;
    }
    return html;
}
//======================================================================================================================
function service_renderDepartureCity(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
        '<select name="' + name + '[' + field['id'] + ']" class="form-control m-select2 select2-departure-city">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '') +
        '</select>';
}
//======================================================================================================================
function service_renderDropdown(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    // html += '<input type="hidden" name="' + name + '[' + field['id'] + ']" value="'+(field['value'] !== undefined?field['value']:'')+'">';
    html += '<select name="' + name + '[' + field['id'] + ']" class="form-control m-bootstrap-select m_selectpicker" title="' + GetTranslation('rv_page_dropdown_placeholder', 'Select value') + '">';

    var variables = [];
        try {
            variables = JSON.parse(field.variables);
        }
        catch(err) {
            // values.push(field['value']);
        }
        var i = 0
    for (key in variables) {
        html += '<option value="' + i + '" '
            + ((field['value'] !== undefined && field['value'] == i) ? 'selected' : '') + '>' + variables[key] + '</option>';
        i++;
    }
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderFood(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    html += '<select name="' + name + '[' + field['id'] + ']" class="form-control m-bootstrap-select m_selectpicker food-select" title="' + GetTranslation('rv_page_food_select_placeholder', 'Select value') + '">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '');
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderNumber(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="number" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input" placeholder="'
        + GetTranslation('rv_page_number_placholder', 'Enter number') + '">';
}
//======================================================================================================================
function service_renderHotel(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
        '<select name="' + name + '[' + field['id'] + ']" class="form-control m-select2 select2-hotel">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '') +
        '</select>';
}
//======================================================================================================================
function service_renderText(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<textarea name="' + name + '[' + field['id'] + ']" rows="1" class="form-control text-textarea" placeholder="'
        + GetTranslation('rv_page_text_placeholder', 'Enter text') + '">' +
        ((field['value'] !== undefined) ? field['value'] : '') +
        '</textarea>';
}
//======================================================================================================================
function service_renderLink(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input" placeholder="'
        + GetTranslation('rv_page_link_placeholder', 'Enter link') + '">';
}
//======================================================================================================================
function service_renderReservationUrl(field,name) {
    return '<div class="one-line-label">' +
        '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
        '</div>' +
        '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input" placeholder="'
        + GetTranslation('rv_page_link_placeholder', 'Enter link') + '">';
}
//======================================================================================================================
function service_renderStarRating(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    html += '<select name="' + name + '[' + field['id'] + ']" class="form-control m-bootstrap-select m_selectpicker star_rating-select" title="' + GetTranslation('rv_page_star_rating_select_placeholder', 'Select star rating') + '">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '');
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderNightsCount(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="number" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input nigths-count-input" placeholder="'
        + GetTranslation('rv_page_nights_count_placholder', 'Enter nights count') + '">';
}
//======================================================================================================================
function service_renderVarchar(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input" placeholder="'
        + GetTranslation('rv_page_varchar_placeholder', 'Enter varchar') + '">';
}
//======================================================================================================================
function service_renderReservation(field,name) {
    return '<div class="one-line-label">' +
        '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
        '</div>' +
        '<input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input" placeholder="'
        + GetTranslation('rv_page_varchar_placeholder', 'Enter varchar') + '">';
}
//======================================================================================================================
function service_renderTime(field,name) {
    return  '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<input type="text" name="' + name + '[' + field['id']
        + ']" class="form-control m-input time-picker" value="' + ((field['value'] !== undefined) ? field['value'] : '')
        + '" placeholder="' + GetTranslation('rv_page_time_placeholder', 'Enter time') + '">';
}
//======================================================================================================================
function service_renderSuppliers(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<select name="' + name + '[' + field['id'] + ']" class="form-control m-bootstrap-select m_selectpicker select2-supplier" title="' + GetTranslation('rv_page_suppliers_placeholder', 'Enter suppliers') + '">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '');
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderCurrency(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    html += '<select name="' + name + '[' + field['id'] + ']" class="form-control m-bootstrap-select m_selectpicker currency-select" title="' + GetTranslation('rv_page_currency_placeholder', 'Select currency') + '">' +
        ((field['value'] !== undefined) ? '<option selected value="' + field['value']['id'] + '">' + field['value']['text'] + '</option>' : '');
    html += '</select>';
    return html;
}
//======================================================================================================================
function service_renderPaymentToSupplier(field,name) {
    return '<div class="one-line-label">\
                <label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>\
            </div>\
            <div class="input-group m-input-group">\
                <input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
                + name + '[' + field['id'] + ']" class="form-control m-input payment_to_supplier-input" placeholder="'
                + GetTranslation('rv_page_payment_to_supplier_placeholder', 'Enter payment') + '" aria-describedby="payment_to_supplier-addon1">\
                <div class="input-group-append">\
                    <span class="input-group-text currency_indicator" id="payment_to_supplier-addon1">\
                        <div class="m-loader" style="width: 20px; display: inline-block;"></div>\
                    </span>\
                </div>\
            </div>';
}
//======================================================================================================================
function service_renderCommission(field,name) {
    var values = field['value'];

    // if (field['value'] !== undefined && field['value'] !== null && field['value'] !== ''){
    //     try {
    //         values = JSON.parse(field['value']);
    //         if(values.value === undefined)
    //             values = {'value':parseFloat(field['value']),'type':'per'};
    //     }
    //     catch(err) {
    //         console.log(field['value'] );
    //         values = {'value':field['value'],'type':'per'};
    //     }
    //
    // }
    if(values === undefined || values.value === undefined)
        values = {'value':0,'type':'per'};
    var html = '<div><div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<div class="input-group extra-dropdown-option" data-for-input-selector=".service_currency_input_with_dropdown">' +
                '<input type="text" class="form-control m-input value_input" aria-label="Text input with dropdown button" value="'+values.value+'">' +
        '<input value="" type="hidden" name="'
        + name + '[' + field['id'] + ']" class="form-control m-input commission-input" placeholder="'
        + GetTranslation('rv_page_commission_placeholder', 'Commission') + '" aria-describedby="commission_number-addon1" max="100" maxlength="3">' +
                '<input type="hidden" class="service_currency_input_with_dropdown type_input" value="'+values.type+'">' +
                '<div class="input-group-append">' +
                    '<button type="button" class="btn btn-secondary dropdown-toggle '
                    +(values.type=='cur'?'currency_indicator':'')
                    +'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                        '%' +
                    '</button>' +
                    '<div class="dropdown-menu">' +
                        '<a class="dropdown-item" href="#" data-value="per">%</a>' +
                        '<a class="dropdown-item currency_indicator" href="#" data-value="cur"></a>' +
                    '</div>' +
                '</div>' +
            '</div></div>';
    var data = $.parseHTML(html);
    $(data).find('.commission-input').val(JSON.stringify(values));
    return $(data).html();
}

//======================================================================================================================
function service_renderServicesPrice(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<div class="input-group m-input-group">\
                <input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" type="text" name="'
                + name + '[' + field['id'] + ']" class="form-control m-input services_price-input" placeholder="'
                + GetTranslation('rv_page_services_price_placeholder', 'Price') + '" aria-describedby="services_price-addon1">\
                <div class="input-group-append">\
                    <span class="input-group-text services_price-input currency_indicator" id="services_price-addon1">\
                        <div class="m-loader" style="width: 20px; display: inline-block;"></div>\
                    </span>\
                </div>\
            </div>';
}
//======================================================================================================================
function service_renderProfit(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<div class="row align-items-center">\
                <div class="col-sm-3">\
                    <div class="income-money income-money__icon fa flaticon-clipboard" style="font-size: 2.5rem;"></div>\
                </div>\
                <div class="col-sm-9">\
                    <input value="' + ((field['value'] !== undefined) ? field['value'] : '') + '" class="profit-total" type="hidden" name="' + name + '[' + field['id'] + ']">\
                    <div class="income-money income-money__info-holder">\
                        <span class="profit-total">' + ((field['value'] !== undefined) ? field['value'] : '0.00') + '</span>\
                        &nbsp;<span class="profit-currency currency_indicator"></span>\
                    </div>\
                </div>\
            </div>';
}
//======================================================================================================================
function service_renderCheckbox(field,name) {
    return '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>' +
            '<label class="m-checkbox mr-2">' +
            '<input type="hidden" name="' + name + '[' + field['id'] + ']" value="">' +
            '<input ' + ((field['value'] !== undefined && field['value']) ? 'checked' : '') + ' type="checkbox" name="' + name + '[' + field['id'] + ']" value="1">' +
            field['name'] +
            '<span></span>' +
            '</label>';
}
//======================================================================================================================
function service_renderSwitch(field,name) {
    var html = '';
    html += '<div class="one-line-label">' +
                '<label class="m-input__title one-line m--font-bolder" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + field['name'] + '">' + field['name'] + '</label>' +
            '</div>';
    var data = ['ON','OFF'];
    try {
        data = JSON.parse(field.variables);
    }
    catch(err) {}
    // if(field.variables !== undefined && field.variables !== '' && field.variables !== null)
    //     data = JSON.parse(field.variables);
    // if(data[0]!==undefined)
    //     html += '<input type="hidden" name="' + name + '[' + field['id'] + ']" value="0">';
    // else
        html += '<input type="hidden" name="' + name + '[' + field['id'] + ']" value="0">';
    // if(data[1]!==undefined)
    //     html += '<input ' + ((field['value'] !== undefined && field['value'] == 1) ? 'checked' : '') + ' data-off-text="'+data[0]+'" data-on-text="'+data[1]+'" data-toggle="toggle" class="need-switcher-init" type="checkbox" value="1" name="' + name + '[' + field['id'] + ']">';
    // else
        html += '<input ' + ((field['value'] !== undefined && field['value'] == 1) ? 'checked' : '') + ' data-off-text="'+data[0]+'" data-on-text="'+data[1]+'" data-toggle="toggle" class="need-switcher-init" type="checkbox" value="1" name="' + name + '[' + field['id'] + ']">';
    return html;
}
//======================================================================================================================
function service_renderTouristsCount(field,name) {
    var html = '';
    var data = undefined;
    if (field['value'] !== undefined && field['value'] !== '' && field['value'] !== null)
        data = JSON.parse(field['value']);
    html += '<div class="init-children-ages tr-count">' +
        '<input type="hidden" class="tourists-count" name="' + name + '[' + field['id'] + ']"  value="' + ((field['value'] !== undefined) ? field['value'] : '') + '">' +
        '<div class="row">' +
        '<div class="col-md-6 mb-3">' +
        '<label>' + GetTranslation('mcr_adults_person_count_label') + ':</label><br>' +
        '<select class="request_create__adult_count_select form-control m-bootstrap-select m_selectpicker" >';
    for (var i = 1; i <= 10; i++)
        if (data !== undefined && data.adults_count !== undefined && i == data.adults_count)
            html += '<option selected value="' + i + '">' + i + '</option>';
        else
            html += '<option value="' + i + '">' + i + '</option>';
    html += '</select>' +
        '</div><div class="col-md-6 mb-3">' +
        '<label>' + GetTranslation('mcr_children_count_label') + ':</label><br>' +
        '<select class="request_create__children_count_select form-control m-bootstrap-select m_selectpicker">';
    for (var i = 0; i <= 10; i++)
        if (data !== undefined && data.children_count !== undefined && i == data.children_count)
            html += '<option selected value="' + i + '">' + i + '</option>';
        else
            html += '<option value="' + i + '">' + i + '</option>';
    html += '</select>' +
        '</div><div class="col-12">' +
        '<div class="row children-age">' +
        '<div class="col-12">' +
        '<label>' + GetTranslation('mcr_children_age_label') + ':</label>' +
        '</div>' +
        '<div class="col-12">' +
        '<div class="row generated_inputs_block">';
    if (data !== undefined && data.children_age !== undefined)
        for (X in data.children_age)
            html += '<div class="col-auto child-age-col mb-3" style="min-width: 50px;max-width: 75px;">' +
                '<input type="text"  class="children-age-input form-control m-input age_mask" maxlength="2" value="' + data.children_age[X] + '">' +
                '</div>';

    html += '</div></div></div></div></div></div>';

    return html;
}
//======================================================================================================================
function renderField(field, name) {
    var html = '';
    var size = field['size'];
    html += '<div class="col-sm-' + size + ' col-lg-' + size + ' mb-3 service-item">';

    switch (field.type) {
        case 'airport':
            html += service_renderAirport(field,name);
            break;
        case 'city':
            html += service_renderCity(field,name);
            break;
        case 'country':
            html += service_renderCountry(field,name);
            break;
        case 'date':
            html += service_renderDate(field,name);
            break;
        case 'date_ending':
            html += service_renderDateEnding(field,name);
            break;
        case 'date_starting':
            html += service_renderDateStarting(field,name);
            break;
        case 'multiselect':
            html += service_renderMultiselect(field,name);
            break;
        case 'radio':
            html += service_renderRadio(field,name);
            break;
        case 'departure_city':
            html += service_renderDepartureCity(field,name);
            break;
        case 'dropdown':
            html += service_renderDropdown(field,name);
            break;
        case 'food':
            html += service_renderFood(field,name);
            break;
        case 'number':
            html += service_renderNumber(field,name);
            break;
        case 'hotel':
            html += service_renderHotel(field,name);
            break;
        case 'text':
            html += service_renderText(field,name);
            break;
        case 'link':
            html += service_renderLink(field,name);
            break;
        case 'star_rating':
            html += service_renderStarRating(field,name);
            break;
        case 'nights_count':
            html += service_renderNightsCount(field,name);
            break;
        case 'varchar':
            html += service_renderVarchar(field,name);
            break;
        case 'time':
            html += service_renderTime(field,name);
            break;
        case 'time_starting':
            html += service_renderTime(field,name);
            break;
        case 'time_ending':
            html += service_renderTime(field,name);
            break;
        case 'suppliers':
            html += service_renderSuppliers(field,name);
            break;
        case 'currency':
            html += service_renderCurrency(field,name);
            break;
        case 'payment_to_supplier':
            html += service_renderPaymentToSupplier(field,name);
            break;
        case 'commission':
            html += service_renderCommission(field,name);
            break;
        case 'services_price':
            html += service_renderServicesPrice(field,name);
            break;
        case 'profit':
            html += service_renderProfit(field,name);
            break;
        case 'checkbox':
            html += service_renderCheckbox(field,name);
            break;
        case 'switch':
            html += service_renderSwitch(field,name);
            break;
        case 'tourists_count':
            html += service_renderTouristsCount(field,name);
            break;
        case 'reservation':
            html += service_renderReservation(field,name);
            break;
        case 'reservation_url':
            html += service_renderReservationUrl(field,name);
            break;
        case 'exchange_rate':
            html += service_renderNumber(field,name);
            break;
    }
    html += '</div>';
    return html;
}
//======================================================================================================================
function renderAdditional(service, link_id,count) {
    var html = '';
    html +=
        '<div class="m-portlet m-portlet--mobile additional-filed" data-id="' + count + 1
        + '" data-service-id="' + service.id + '" m-portlet="true">' +
        '<div class="m-portlet__head">' +
        '<div class="m-portlet__head-caption">' +
        '<div class="m-portlet__head-title">' +
        '<span class="m-portlet__head-icon">' +
        '<i class="' + service.icon + ' m--font-success"></i>' +
        '</span>' +
        '<h3 class="m-portlet__head-text m--font-success">' +
        service.name +
        '</h3>' +
        '</div>' +
        '</div>' +
        '<div class="m-portlet__head-tools">' +
        '<ul class="m-portlet__nav">' +
        '<li class="m-portlet__nav-item">' +
        '<a href="#" m-portlet-tool="remove" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">' +
        '<i class="la la-close"></i>' +
        '</a>' +
        '</li>' +
        '<li class="m-portlet__nav-item">' +
        '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill copy-service">' +
        '<i class="la la-copy"></i>' +
        '</a>' +
        '</li>' +
        '<li class="m-portlet__nav-item">' +
        '<a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">' +
        '<i class="la la-angle-down"></i>' +
        '</a>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '<div class="m-portlet__body">' +
        getInputsHtmlTemplate(service.id, service.fields, count + 1, link_id) +
        '</div>';
    return html;
}
//======================================================================================================================
function renderAddServicesList(services) {
    var html = '';
    html += ' <li class="m-portlet__nav-item">' +
        '<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" data-dropdown-toggle="click">' +
        '<a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">' +
        GetTranslation('rv_select_service_modal_partlet_2_title') +
        '</a>' +
        '<div class="m-dropdown__wrapper">' +
        '<span class="m-dropdown__arrow m-dropdown__arrow--right"></span>' +
        '<div class="m-dropdown__inner">' +
        '<div class="m-dropdown__body">' +
        '<div class="m-dropdown__content">' +
        '<ul class="m-nav">';
    for (key in services) {
        html += '<li class="m-nav__item">' +
            '<a href="#" class="m-nav__link add-service" data-service-id="' + services[key].id + '">' +
            '<i class="m-nav__link-icon ' + services[key].icon + '"></i>' +
            '<span class="m-nav__link-text">' +
            services[key].name +
            '</span>' +
            '</a>' +
            '</li>';
    }
    html += '</ul>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</li>';
    return html;
}
//======================================================================================================================
function getInputsHtmlTemplate(service_id, fields, additional, ad_link,type) {
    var html = '<div class="row">';
    var name = 'Service';
    if (additional !== undefined && additional) {
        name = 'ServiceAdditionals[' + additional + ']';
    }
    if (ad_link !== undefined)
        html += '<input type="hidden" name="' + name + '[link_id]" value="' + ad_link + '">';
    html += '<input type="hidden" name="' + name + '[id]" value="' + service_id + '">';

    if(type !== undefined)
        html += '<input type="hidden" name="' + name + '[type]" value="' + type + '">';
    var ad = '';
    for (key in fields) {
        var field = fields[key];
        if (!field['is_additional'])
            html += renderField(field, name);
        else
            ad += renderField(field, name);
    }
    if (ad != '') {
        html += '<div class="col-sm-6 col-lg-6 mb-3" style="display: flex; align-items: flex-end;">'; // START OPENER COL
        html += '<a href="#" class="m-link mb-2" data-opener="true" data-target=".opener-target-' + service_id + '' + additional + '" data-status="open">';
        html += '<i class="la la-caret-down" data-show_decorator="for-open"></i>';
        html += '<i class="la la-caret-right" data-show_decorator="for-close"></i>';
        html += '<span>' + GetTranslation('mcr_additional_parameters') + '</span>';
        html += '</a>';
        html += '</div>'; // END OPENER COL
        html += '<div class="col-12">';
        html += '<div class="opener-target-' + service_id + '' + additional + '">'; // START ADDITIONL SERVICES BLOCK
        html += '<div class="row">';

        html += ad;
        html += '</div>'; // END ADDITIONL SERVICES ROW
        html += '</div>'; // END ADDITIONL SERVICES BLOCK
        html += '</div>';
    }
    html += '</div>';

    return html;
}
//======================================================================================================================
function getAdditionalServicesHtmlTemplate(service_id, additional_services) {
    var html = '';
    if ((additional_services !== undefined) && (additional_services.length > 0)) {
        html = '<div class="row services-set">';
        for (key in additional_services) {
            var additional_service_elem = additional_services[key];
            html += '<div class="col-sm-6 col-lg-3 mb-4 additional-service-item">\
                                        <div class="checkbox-block">\
                                            <label class="m-checkbox">\
                                                    <input type="checkbox" class="select-additional" value="' + additional_service_elem.id + '">\
                                                    <span></span>\
                                            </label>\
                                        </div>\
                                        <div class="info-block">\
                                            <div class="icon-wrap">\
                                                <i class="' + additional_service_elem.icon + '"></i>\
                                            </div>\
                                            <div class="text-wrap">\
                                                <span class="btn-lg__text m--font-bold">' + additional_service_elem.name + '</span>\
                                            </div>\
                                        </div>\
                                    </div>';

        }
        html += '</div>';
    }
    return html;
}
//======================================================================================================================
function renderBaseAdditionalService(service,count,link_id)
{
    var html = '';

    html += '<div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm mb-4" id="service-custom-details-portlet">' +
        '<div class="m-portlet__head">' +
        '<div class="m-portlet__head-caption">' +
        '<div class="m-portlet__head-title">' +
        '<h3 class="m-portlet__head-text" >' +
            service.name +
        '</h3>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="m-portlet__body">' +
        '<div class="container">' +
        getInputsHtmlTemplate(service.id, service.fields, count + 1, link_id,1) +
        '</div>' +
        '</div>' +
        '</div>';
    return html;
}