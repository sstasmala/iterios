function initMomentJS(){
//            EXAMPLE
//            <span class="moment-js invisible" data-format="{{FORMAT}}">{{DATE}}</span>
//            {{FORMAT}} = 'DD.MM.YYYY' | 'unix'
//            {{DATE}} = '22.05.2016' | '1514472593'
    $('.moment-js').each(function(){
        var moment_block = this;
        var is_already_inited = $(moment_block).data("moment-js-inited");
        var format = $(moment_block).data('format');
        var moment_date = $(moment_block).text();
        var moment_text;
        var moment_tip;

        if(!is_already_inited){
            moment.locale(tenant_lang);
            if(format === 'unix'){
                moment_text = moment(parseInt(moment_date * 1000)).fromNow();
                moment_tip = moment(parseInt(moment_date * 1000)).format(date_format+' '+time_format);
            }else{
                moment_text = moment(moment_date, format).fromNow();
                moment_tip = moment(moment_date, format).format(date_format+' '+time_format);
            }
            $(moment_block).text(moment_text);
            $(moment_block).popover({
                content: moment_tip,
                placement: 'top',
                trigger: 'hover'
            });
            $(moment_block).removeClass("invisible");
            $(moment_block).data("moment-js-inited", true);
            moment.locale('en'); // default the locale to English
        }
    });
}

var SnippetCreateCompany = function() {
    var searchDelay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    /* jQuery Validate Emails with Regex */
    function validateEmail(email) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        return $.trim(email).match(pattern) ? true : false;
    }

    var showNotification = function(text) {
        var el = $('#m_modal_create_company').find('form');
        var notification = $('<div class="notification_block">\
        </div>');

        $('#m_modal_create_company').find('.notification_block').remove();
        notification.insertAfter(el);
        notification.html(text);
    };

    var showClients = function (companies, validate_mail) {
        var el = $('#companies').slideDown('slow').find('.m-widget4');
        var notification_text = GetTranslation('ci_search_notification');

        if (validate_mail == true) {
            notification_text = GetTranslation('ci_search__contact_notification1') +
                '<strong id="email"> ' +
                $('form#create_company_details').find('[name="email"]').val() +
                ' </strong>' +
                GetTranslation('ci_search__contact_notification2');
        }

        // Fixed form after pressing button
        if ($('div').is('#email-error:visible') == true || $('div').is('#phone-error:visible') == true) {
            var email = $('form#create_company_details').find('[name="email"]').inputmask('unmaskedvalue');
            var phone = $('form#create_company_details').find('[name="phone"]').inputmask('unmaskedvalue');

            $('#m_modal_create_company').find('form').validate().destroy();
            $('form#create_company_details').find('[name="email"]').val(email);
            $('form#create_company_details').find('[name="phone"]').val(phone);
        }
        // End fixed

        $('#m_modal_create_company').find('#create_company_submit').attr('disabled', 'disabled');
        showNotification(notification_text);
        el.empty();

        $.each( companies, function( key, client ) {
            var template = $('<!--begin::Widget 14 Item-->\
                <div class="m-widget4__item">\
                    <div class="m-widget4__info">\
                        <span class="m-widget4__title"></span><br>\
                        <span class="m-widget4__sub"></span>\
                    </div>\
                    <div class="m-widget4__ext">\
                        <a href="#" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" target="_blank">'+GetTranslation('select')+'</a>\
                    </div>\
                </div>\
            <!--end::Widget 14 Item-->');
            //template.find('img').attr('src', client.photo);
            template.find('a').attr('href', baseUrl + '/ita/' + tenantId + '/companies/view?id='+client.id);
            template.find('.m-widget4__title').text(client.name);
            template.find('.m-widget4__sub').html(client.email + '<br>' + client.phone);

            el.append(template);
        });
    };

    var clearModal = function (input_clear, undisable) {
        var form = $('#m_modal_create_company').find('form');

        if (input_clear == true) {
            //form.validate().destroy();
        }

        $('#companies').slideUp('slow', function () {
            $(this).find('.m-widget4').empty();
        });

        $('#m_modal_create_company').find('.notification_block').fadeOut('slow');

        if (undisable == true)
            $('#m_modal_create_company').find('#create_company_submit').removeAttr('disabled');
    };

    var searchClients = function (email, phone) {
        var data = {
            email: email,
            phone: phone
        };

        $.get(baseUrl + '/ita/' + tenantId + '/companies/get-companies', data, function(res) {
            mApp.block('#m_modal_create_company .modal-body', {});

            setTimeout(function() {
                mApp.unblock('#m_modal_create_company .modal-body');

                if (res.length > 0) {
                    var validate_mail = validateEmail($('form#create_company_details').find('[name="email"]').val());
                    showClients(res, validate_mail);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });
    };

    var inputListener = function () {
        var email = $('form#create_company_details').find('[name="email"]');
        var phone = $('form#create_company_details').find('[name="phone"]');
        var email_value, phone_value;

        email.on('input', function (e) {
            var input = $(this);

            searchDelay(function() {
                email_value = input.val().replace(/^_+|[@_.]+$/g, '');
                phone_value = phone.val();

                if (email_value.length > 3 || phone_value.length > 3) {
                    $('#m_modal_create_company').find('#create_company_submit').attr('disabled', 'disabled');

                    searchClients(email_value, phone_value);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });

        phone.on('input', function (e) {
            var input = $(this);

            searchDelay(function() {
                phone_value = input.val();
                email_value = email.val().replace(/^_+|[@_.]+$/g, '');

                if (phone_value.length > 3 || email_value.length > 3) {
                    $('#m_modal_create_company').find('#create_company_submit').attr('disabled', 'disabled');

                    searchClients(email_value, phone_value);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });
    };

    var handleCreateContactFormSubmit = function() {
        //$('#m_modal_create_company').find('#create_company_submit').attr('disabled', 'disabled');

        $('#create_company_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('div.modal-content').find('form');

            form.validate({
                rules: {
                    email: {
                        required: false,
                        email: function () {
                            var email = $('form#create_company_details').find('[name="email"]').val().replace(/^_+|[@_.]+$/g, '');

                            return (email.length > 0 ? true : false);
                        }
                    },
                    phone: {
                        required: false
                    },
                    name: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/ita/' + tenantId + '/companies/create',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.id)
                            location.href = baseUrl + '/ita/' + tenantId + '/companies/view?id=' + response.id;
                    }, 1000);
                }
            });
        });
    };

    //== Public Functions
    return {
        // public functions
        init: function() {
            inputListener();
            handleCreateContactFormSubmit();
        },
        clear: function() {
            clearModal(true, false)
        }
    };
}();

var SnippetCreateContact = function() {
    var searchDelay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    /* jQuery Validate Emails with Regex */
    function validateEmail(email) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        return $.trim(email).match(pattern) ? true : false;
    }

    var showNotification = function(text) {
        var el = $('#m_modal_create_contact').find('form');
        var notification = $('<div class="notification_block">\
        </div>');

        $('#m_modal_create_contact').find('.notification_block').remove();
        notification.insertAfter(el);
        notification.html(text);
    };

    var showClients = function (clients, validate_mail) {
        var el = $('#clients').slideDown('slow').find('.m-widget4');
        var notification_text = GetTranslation('contacts_search_notification');

        if (validate_mail == true) {
            notification_text = GetTranslation('contacts_search__contact_notification1') +
                '<strong id="email"> ' +
                $('form#create_contact_details').find('[name="email"]').val() +
                ' </strong>' +
                GetTranslation('contacts_search__contact_notification2');
        }

        // Fixed form after pressing button
        if ($('div').is('#email-error:visible') == true || $('div').is('#phone-error:visible') == true) {
            var email = $('form#create_contact_details').find('[name="email"]').inputmask('unmaskedvalue');
            var phone = $('form#create_contact_details').find('[name="phone"]').inputmask('unmaskedvalue');

            $('#m_modal_create_contact').find('form').validate().destroy();
            $('form#create_contact_details').find('[name="email"]').val(email);
            $('form#create_contact_details').find('[name="phone"]').val(phone);
        }
        // End fixed

        $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');
        showNotification(notification_text);
        el.empty();

        $.each( clients, function( key, client ) {
            var template = $('<!--begin::Widget 14 Item-->\
                <div class="m-widget4__item">\
                    <div class="m-widget4__info">\
                        <span class="m-widget4__title"></span><br>\
                        <span class="m-widget4__sub"></span>\
                    </div>\
                    <div class="m-widget4__ext">\
                        <a href="#" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" target="_blank">'+GetTranslation('select')+'</a>\
                    </div>\
                </div>\
            <!--end::Widget 14 Item-->');
            //template.find('img').attr('src', client.photo);
            template.find('a').attr('href', baseUrl + '/ita/' + tenantId + '/contacts/view?id='+client.id);
            template.find('.m-widget4__title').text(client.name);
            template.find('.m-widget4__sub').html(client.email + '<br>' + client.phone);

            el.append(template);
        });
    };

    var clearModal = function (input_clear, undisable) {
        var form = $('#m_modal_create_contact').find('form');

        if (input_clear == true) {
            //form.validate().destroy();
        }

        $('#clients').slideUp('slow', function () {
            $(this).find('.m-widget4').empty();
        });

        $('#m_modal_create_contact').find('.notification_block').fadeOut('slow');

        if (undisable == true)
            $('#m_modal_create_contact').find('#create_contact_submit').removeAttr('disabled');
    };

    var searchClients = function (email, phone) {
        var data = {
            email: email,
            phone: phone
        };

        $.get(baseUrl + '/ita/' + tenantId + '/contacts/get-contacts', data, function(res) {
            mApp.block('#m_modal_create_contact .modal-body', {});

            setTimeout(function() {
                mApp.unblock('#m_modal_create_contact .modal-body');

                if (res.length > 0) {
                    var validate_mail = validateEmail($('form#create_contact_details').find('[name="email"]').val());
                    showClients(res, validate_mail);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });
    };

    var inputListener = function () {
        var email = $('form#create_contact_details').find('[name="email"]');
        var phone = $('form#create_contact_details').find('[name="phone"]');
        var email_value, phone_value;

        email.on('input', function (e) {
            var input = $(this);

            searchDelay(function() {
                email_value = input.val().replace(/^_+|[@_.]+$/g, '');
                phone_value = phone.val();

                if (email_value.length > 3 || phone_value.length > 3) {
                    $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                    searchClients(email_value, phone_value);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });

        phone.on('input', function (e) {
            var input = $(this);

            searchDelay(function() {
                phone_value = input.val();
                email_value = email.val().replace(/^_+|[@_.]+$/g, '');

                if (phone_value.length > 3 || email_value.length > 3) {
                    $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                    searchClients(email_value, phone_value);
                } else {
                    clearModal(false, true);
                }
            }, 1000);
        });
    };

    var handleCreateContactFormSubmit = function() {
        //$('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

        $('#create_contact_submit').click(function(e) {
            // e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('div.modal-content').find('form');

            form.validate({
                rules: {
                    email: {
                        required: false,
                        email: function () {
                            var email = $('form#create_contact_details').find('[name="email"]').val().replace(/^_+|[@_.]+$/g, '');

                            return (email.length > 0 ? true : false);
                        }
                    },
                    phone: {
                        required: false
                    },
                    first_name: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/ita/' + tenantId + '/contacts/create',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.id)
                            location.href = baseUrl + '/ita/' + tenantId + '/contacts/view?id=' + response.id;
                    }, 1000);
                }
            });
        });
    };

    //== Public Functions
    return {
        // public functions
        init: function() {
            inputListener();
            handleCreateContactFormSubmit();
        },
        clear: function() {
            clearModal(true, false)
        }
    };
}();

(function($){

    $(document).ready(function(){
        
        /* Functions init */
        searchDropdownCore();
        notifiationMessagesCore();
        initMomentJS();
        initCountrySelects();
        initCitiesSelects();
        initBootstrapSelects();
        initMaskedInputs();
        overdueTaskCount();
        setMomentJS();
        initCreateCompanyModal();
        initCreateContactModal();
        initCreateRequestModal();
        initCreateHotelModal();
        initCreateCityModal();
        searchContactCore();
        initHeaderRequestsModal();
        hideBodySchrollbarWhenModalOpen();
        disableAutoFormComplete();
        initEditableModalTitles();
        tooltip();

        function tooltip(){
            $('[data-title="m-tooltip"]').each(function() {
                mApp.initTooltip($(this));
            });
        }
        /* Functions declarate */
        function searchDropdownCore(){
            // VARS
            var search_dropdown = document.getElementById('search-dropdown');
            var search_panel = document.getElementById('search-panel');
            var quick_search_input = document.getElementById('quick-search-input');
            
            // EVENTS
            if(search_dropdown !== null){
                $(quick_search_input).on('focus', function(){
                    openSearchDropdown();
                });
                $('.m-page>.m-body, footer.m-footer, header.m-header').on('click', function(){
                    if(!$(search_panel).is(':hover')){
                        closeSearchDropdown();
                    }
                });
                $('#reset_search_input_button').on('click', function(){
                    $('#quick-search-input').val('');
                });
            }
            
            // FUNCTIONS-HELPERS
            function closeSearchDropdown(){
                $(search_dropdown).stop().slideUp(300, function(){
                    $(this).removeClass('open');
                });
            }
            
            function openSearchDropdown(){
                $(search_dropdown).stop().slideDown(300, function(){
                    $(this).addClass('open');
                });
            }
        }
        
        function notifiationMessagesCore(){
            var notifications_wrap = document.getElementById('notifications-wrap');
            var notifications_list = document.getElementById('notifications-list');
            var items_per_load = 20;

            var notification_template = '<div class="m-list-timeline__item">\
                                            <div class="read_marker" title="Mark as read"></div>\
                                            <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>\
                                            <div class="m-list-timeline__text">\
                                                <div class="notification-content"></div>\
                                            </div>\
                                            <span class="m-list-timeline__time moment-js" data-format="unix"></span>\
                                        </div>';
            
            if (notifications_list !== null){
                loadUserNotifications(items_per_load, 0);
            }

            function not_read () {
                var icon = $('#notifications_block').find('a.m-nav__link');

                if (icon.attr('id') != 'm_topbar_notification_icon') {
                    $('#notifications_block').find('.m-nav__link-badge').show();
                    icon.attr('id', 'm_topbar_notification_icon');
                }
            }
            
            function loadUserNotifications(items_per_load, offset) {
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/ajax/notifications',
                    method: 'POST',
                    data: {
                        items_per_load: items_per_load,
                        offset: offset
                    },
                    dataType: 'json',
                    success: function (response){
                        if((response.notifications_count == 0) && (offset === 0)){
                            //It's first load and no results.
                            $(notifications_list).parent().append('<div>No notifications for you!</div>');
                        } else {
                            document.getElementById('notifications_count').innerHTML = response.notifications_count;

                            removeLoadMoreSpiner();

                            response.notifications.forEach(function (item, i) {
                                var notification = $(notification_template);

                                if (!item.read) {
                                    not_read();
                                } else {
                                    notification.addClass('m-list-timeline__item--read');
                                    notification.find('div.read_marker').attr('title', 'Mark as unread');
                                }

                                notification.attr('data-notification-id', item.id);
                                notification.find('div.notification-content').append((i + 1 + offset) + '. ' + item.text);
                                notification.find('span.m-list-timeline__time').append(item.created_at);
                                $(notifications_list).append(notification);
                            });

                            initMomentJS();

                            if(response.stack_finished === false){
                                addLoadMoreSpiner(items_per_load, response.new_offset);
                            }
                            /* Add click event to markers */
                            $('#notifications-list .read_marker').each(function(){
                                var marker = this;
                                if($(marker).data('click_evented') !== 'evented'){
                                    $(marker).on('click', function(){
                                        var notification = $(this).parents('.m-list-timeline__item')[0];
                                        var notif_id = $(notification).data('notification-id');

                                        if (notif_id) {
                                            if ($(notification).hasClass('m-list-timeline__item--read')) {
                                                $(notification).removeClass('m-list-timeline__item--read');
                                                $(this).attr('title', 'Mark as read');
                                            } else {
                                                $(notification).addClass('m-list-timeline__item--read');
                                                $(this).attr('title', 'Mark as unread');
                                            }

                                            $.get(baseUrl + '/ita/' + tenantId + '/ajax/read-notification', {id: notif_id}, function (res) {

                                            });
                                        }
                                    });
                                    $(marker).data('click_evented', 'evented');
                                }
                            });
                        }
                    }
                });
            }
            
            function removeLoadMoreSpiner()
            {
                $('#load-more-spinner').remove();
            }
            
            function addLoadMoreSpiner(items_per_load, offset)
            {
                var spinner = document.createElement('div');
                spinner.id = 'load-more-spinner';
                spinner.innerHTML = '<div class="m-loader m-loader--brand"></div>';
                $(spinner).data('offset', offset);
                $(notifications_list).append(spinner);
            }
            
            /* Init scrollable notifications dropdown */
//            $(notifications_wrap).mCustomScrollbar({
//                scrollInertia: 0,
//                autoDraggerLength: true,
//                autoHideScrollbar: true,
//                autoExpandScrollbar: false,
//                alwaysShowScrollbar: 0,
//                axis: 'y',
//                mouseWheel: {
//                    scrollAmount: 80,
//                    preventDefault: true
//                },
//                setHeight: 300,
//                theme:"minimal-dark",
//                callbacks:{
//                    onTotalScrollOffset: 10,
//                    onTotalScroll: function(){
//                        var load_more_spinner = document.getElementById('load-more-spinner');
//                        /* Load more notifications */
//                        if(load_more_spinner !== null){
//                            var spinner_offset = parseInt($(load_more_spinner).data('offset'));
//                            var offset = (spinner_offset) ? spinner_offset : 0;
//                            loadUserNotifications(items_per_load, offset);
//                        }
//                    },
//                    onScroll: function(){
//                        checkAutoRead(this);
//                    },
//                    onInit: function(){
//                        checkAutoRead(this);
//                    }
//                }
//            });
            
            function checkAutoRead(content_block){
                /**
                 * This function makes the notifications automatically read,
                 * if user scroll to this notification and see it during time
                 */
                var config = {
                    autoread_delay : 2000
                };
                var scrolTop = -content_block.mcs.top;
                var scopeHeight = $(content_block).height();
                var notifications = $(content_block).find('.m-list-timeline__item');
                $(notifications).each(function(){
                    var notification = this;
                    var notification_id = $(notification).data('notification-id');
                    var item_center_y_position = notification.offsetTop + ($(notification).height() / 2);
                    if(!$(notification).hasClass('m-list-timeline__item--read')){
                        // Work only with unread notifications
                        if((item_center_y_position > scrolTop) && (item_center_y_position < (scrolTop + scopeHeight))){
                            // In scope! Start autoread timeout
                            var timeout_id = $(notification).data('autoread-timeout-id');
                            if(!timeout_id){
                                timeout_id = window.setTimeout(function(){
                                    $(notification).animate({"opacity" : "0.5"}, 300, 'swing', function(){
                                        $(notification).find('.read_marker').click();
                                        $(notification).attr('style', '');
                                    });
                                }, config.autoread_delay);
                                $(notification).data('autoread-timeout-id', timeout_id);
                            }
                        } else {
                            // Not in scope! Stop autoread timeout
                            var timeout_id = $(notification).data('autoread-timeout-id');
                            if(timeout_id){
                                clearTimeout(timeout_id);
                                $(notification).data('autoread-timeout-id', null);
                            }
                        }
                    }
                });
            }
        }
        
        function initCountrySelects(){
            /**
             * Add "country-select-ajax" for select tag for create select2 with country ajax search
             * Snippet: <select class="form-control m-bootstrap-select country-select-ajax" name="country"></select>
             */
            $('.country-select-ajax').each(function(){
                var select = this;
                var parentElement = $(this).parent();
                $(select).select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    minimumInputLength: 2,
                    allowClear: true,
                    width: '100%',
                    dropdownParent: parentElement,
                    placeholder: GetTranslation("select_country_placeholder", "Select country")
                });
            });
        }
        
        function initCitiesSelects(){
            /**
             * Add "city-select-ajax" for select tag for create select2 with country ajax search
             * Snippet: <select class="form-control m-bootstrap-select city-select-ajax" name="city"></select>
             */
            $('.city-select-ajax').each(function(){
                var select = this;
                var parentElement = $(this).parent();
                $(select).select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-cityes-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    minimumInputLength: 2,
                    allowClear: true,
                    width: '100%',
                    dropdownParent: parentElement,
                    placeholder: GetTranslation("select_city_placeholder", "Select city")
                });
            });
        }
        
        function initBootstrapSelects(){
            $('.m_selectpicker').selectpicker({
                width: '100%'
            });
        }
        
        function initMaskedInputs(){
            $('.masked-input').inputmask();
        }

        function overdueTaskCount() {
            $.get(baseUrl + '/ita/' + tenantId + '/tasks/get-count-overdue-task', {}, function(res) {
                if (res.count && $('span').is('#overdue_task_badge')) {
                    $('#overdue_task_badge').find('.m-badge').text(res.count);
                    $('#overdue_task_badge').show();
                }
            });
        }
        
        function setMomentJS() {
            moment.updateLocale(tenant_lang, {
                week: { dow: +week_start } // Set first day of the week
            });
        }
        
        function initCreateCompanyModal() {
            $('#create_company_details')
                .find('[valid="companyPhoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                // var inputFormat = $("#company_create")[0].placeholder;
                // var formated = inputFormat.replace(/[0-9]/g, 9);
                var formated = '+999 99 999 9999';
                $("#company_create").inputmask(formated);
                    $("#company_create").on("countrychange", function (e, countryData) {
                        var inputFormat = $("#company_create")[0].placeholder;
                        var formated = inputFormat.replace(/[0-9]/g, 9);
                        $("#company_create").inputmask(formated);
                    });
                });
            SnippetCreateCompany.init();

            $('#m_modal_create_company').on('hidden.bs.modal', function (e) {
                SnippetCreateContact.clear();
            });
        }
        
        function initCreateContactModal(){
            $('#create_contact_details')
                .find('[valid="phoneNum"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
            }).then(function (data) {

                // var inputFormat = $("#contact_create__contact_phone_input")[0].placeholder;
                // var formated = inputFormat.replace(/[0-9]/g, 9);
                var formated = '+999 99 999 9999';
                $("#contact_create__contact_phone_input").inputmask(formated);

                $("#contact_create__contact_phone_input").on("countrychange", function (e, countryData) {
                    var inputFormat = $("#contact_create__contact_phone_input")[0].placeholder;
                    var formated = inputFormat.replace(/[0-9]/g, 9);
                    $("#contact_create__contact_phone_input").inputmask(formated);
                });
            });

            SnippetCreateContact.init();

            $('#m_modal_create_contact').on('hidden.bs.modal', function (e) {
                SnippetCreateContact.clear();
            });
        }
        
        function initCreateRequestModal(){
            $('#m_modal_create_request_form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                // var inputFormat = $("#request_create__contact_phone_input")[0].placeholder;
                // var formated = inputFormat.replace(/[0-9]/g, 9);
                var formated = '+999 99 999 9999';
                $("#request_create__contact_phone_input").inputmask(formated);

                $("#request_create__contact_phone_input").on("countrychange", function (e, countryData) {
                    var inputFormat = $("#request_create__contact_phone_input")[0].placeholder;
                    var formated = inputFormat.replace(/[0-9]/g, 9);
                    $("#request_create__contact_phone_input").inputmask(formated);
                });
            });
            // Countries
            var select = $('#request_create__contact_countries_select').get()[0];
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['title']});
                        }
                        return {
                            results: array
                        }
                    },
                },
                templateSelection: function(data){
                    if(data.id === ''){
                        return GetTranslation("mcr_countries_select_placeholder", "Select country");
                    }
                    return data.text;
                },
                language: tenant_lang,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(select).closest('#m_modal_create_request'),
                placeholder: GetTranslation("mcr_countries_select_placeholder", "Select country")
            });
            
            // City
            var select = $('#request_create__contact_city_select').get()[0];
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-departure-cityes-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data) {
                            array.push({id: data[i]['id'], text: data[i]['title']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                language: tenant_lang,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(select).closest('#m_modal_create_request'),
                placeholder: GetTranslation("mcr_city_select_placeholder", "Enter city of departure")
            });
            
            // Departure date
            var datepicker = $('#request_create__contact_departure_date_picker').get()[0];
            $(datepicker).daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                dropdownParent: $(datepicker).closest('#m_modal_create_request'),
                minDate: moment(new Date()).format(date_format),
                maxDate: moment(new Date()).add(20, 'years').format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                    applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                    cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                    fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                    toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function(e){
                $('.daterangepicker').on('click', function(e){
                    // It is fix for daterangepicker. It disble closing 
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });
            $(datepicker).val(''); // make datepicker empty
            $(datepicker).on('change', function(){
                var startDate = $(this).data('daterangepicker').startDate.format(date_format);
                var endDate = $(this).data('daterangepicker').endDate.format(date_format);
                $('#request_create__contact_departure_date_picker_start').val(startDate);
                $('#request_create__contact_departure_date_picker_end').val(endDate);
            });
            
            // Budget inputs
            $('#m_modal_create_request_form .extra-dropdown-option').each(function(){
                var group = this;
                var hidden_input_selector = $(group).data('for-input-selector');
                var hidden_input = $(group).find(hidden_input_selector)[0];
                var drop_down_btn = $(group).find('button.btn')[0];
                $(group).find('.dropdown-menu a').each(function(){
                    var variant = this;
                    var variant_name = $(this).text();
                    var variant_val = $(this).data('value');
                    $(variant).unbind('click');
                    $(variant).on('click', function(){
                       $(hidden_input).val(variant_val);
                       $(drop_down_btn).text(variant_name);
                    });
                });
            });
            
            // Children select
            $('#m_modal_create_request .children-age').slideUp(0);  // Hide on start
            $('#request_create__children_count_select').on('change', function(){
                var chidlen_count = parseInt($(this).val());
                var inputs_row = $('#m_modal_create_request .children-age').get()[0];
                var inputs_block = $(inputs_row).find('.generated_inputs_block').get()[0];
                if(chidlen_count>0){
                    // Delete old inputs
                    $(inputs_block).find('.child-age-col').remove();
                    // Generate inputs
                    var inputs_html = '';
                    for(var i=0; i<chidlen_count; i++){
                        inputs_html += '<div class="col-1 child-age-col mb-3">\
                                        <input type="text" name="Requests[children_age][' + i + ']" class="form-control m-input age_mask" maxlength="2">\
                                    </div>';
                    }
                    // Show
                    $(inputs_block).append(inputs_html);
                    $(inputs_row).stop().slideDown(400);
                }else{
                    // Close
                    $(inputs_row).stop().slideUp(400);
                    // Delete inputs
                    $(inputs_block).find('.child-age-col').slideUp(200, function(){
                        $(this).remove();
                    });
                }
            });
                        
            // Mask input for currency
            $(".currency_mask").inputmask({
                "mask": "9{+}"
            });
            
            // Mask input for age
            $(".age_mask").inputmask({
                "mask": "9{1,2}"
            });
            
            // Description textarea
            var textarea_jq_selector = $('#request_create__description_textarea');
            autosize(textarea_jq_selector);
        }
        
        function initCreateHotelModal(){
            var form = $('#add_new_hotel_form').get()[0];
            var category_select = $('#add_new_hotel_category_input').get()[0];
            
            // Init hotel category select
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/requests/get-hotel-category',
                method: 'POST',
                dataType: 'json',
                data: {
                    search: '',
                    type: 'public',
                    _csrf: $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    for (var i in data) {
                        if ($(category_select).find('option[value="' + data[i]['id'] + '"]').length == 0){
                            $(category_select).append('<option value="' + data[i]['id'] + '">' + data[i]['title'] + '</option>');
                        }
                    }
                    $('#add_new_hotel_category_input_preloader').hide();
                    $(category_select).selectpicker('refresh');
                }
            });
            
            // Init hotel city select
            var city_select = $('#add_new_hotel_city_input').get()[0];
            $(city_select).select2({
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(city_select).closest('.modal-content'),
                placeholder: GetTranslation("hotel_city_input_placeholder", "Select city"),
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-cityes-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data,params) {
                        var array = [];
                        params.page = params.page || 1;
                        for (var i in data.results) {
                            array.push({id: data.results[i]['id'], text: data.results[i]['title']});
                        }
                        return {
                            results: array,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("hotel_city_input_placeholder", "Select city");
                    }
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (perm) {
                    return perm.text;;
                }
            });
            
            // Init hotel country select
            var coutry_select = $('#add_new_hotel_country_input').get()[0];
            $(coutry_select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['title']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                minimumInputLength: 2,
                allowClear: true,
                width: '100%',
                dropdownParent: $(coutry_select).closest('.modal-content'),
                placeholder: GetTranslation("hotel_country_input_placeholder", "Select hotel country")
            });
            
            // Add validate rules
            $(form).validate({
                rules: {
                    hotel_name: {
                        required: true,
                        minlength: 2
                    },
                    hotel_category: {
                        required: true
                    },
                    hotel_city: {
                        required: true
                    },
                    hotel_country: {
                        required: true
                    }
                }
            });
            
            // Proccess submit event
            $(form).submit(function(event){
                event.preventDefault();
                if($(form).valid()){
                    var formDataArray = $(form).serializeArray();
                    var formDataObject = {};
                    formDataArray.forEach(function(value){
                        formDataObject[value.name] = value.value;
                    });
                    alert('Create hotel from: ' + JSON.stringify(formDataObject));
                }
            });
        }
        
        function initCreateCityModal(){
            var form = $('#add_new_city_form').get()[0];
            $(form).validate({
                rules: {
                    city_name: {
                        required: true,
                        minlength: 2
                    }
                }
            });
            $(form).submit(function(event){
                event.preventDefault();
                if($(form).valid()){
                    var formDataArray = $(form).serializeArray();
                    var formDataObject = {};
                    formDataArray.forEach(function(value){
                        formDataObject[value.name] = value.value;
                    });
                    alert('Create city from: ' + JSON.stringify(formDataObject));
                }
            });
        }
        
        function searchContactCore() {
            var email = $('#request_create__contact_email_input');
            var phone = $('#request_create__contact_phone_input');
            var email_value, phone_value;

            var searchDelay = (function(){
                var timer = 0;
                return function(callback, ms){
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            })();

            /* jQuery Validate Emails with Regex */
            function validateEmail(email) {
                var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                return $.trim(email).match(pattern) ? true : false;
            }


            var showContacts = function (contacts) {
                var el = $('#contacts').slideDown('slow').find('.m-widget4');

                el.empty();
                $('button.btn-primary').prop('disabled', 'disabled');

                $.each( contacts, function( key, contact ) {
                    var template = $('<!--begin::Widget 14 Item-->\
                        <div class="m-widget4__item">\
                            <div class="m-widget4__info">\
                                <span class="m-widget4__title"></span><br>\
                                <span class="m-widget4__sub"></span>\
                            </div>\
                            <div class="m-widget4__ext">\
                                <a href="#" id="select-contact" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" data-index="'+key+'">'+GetTranslation('select')+'</a>\
                            </div>\
                        </div>\
                    <!--end::Widget 14 Item-->');
                    //template.find('a').attr('href', baseUrl + '/ita/' + tenantId + '/contacts/view?id='+contact.id);
                    template.find('.m-widget4__title').text(contact.name);
                    template.find('.m-widget4__sub').html(contact.email + '<br>' + contact.phone);

                    el.append(template);
                });

                $('body').on('click', '#select-contact', function (event) {
                    var key = $(this).data('index');

                    $('#m_modal_create_request input[name="contact_id"]').val(contacts[key].id);
                    $('#m_modal_create_request input[name="Contacts[first_name]"]').val(contacts[key].first_name);
                    $('#m_modal_create_request input[name="Contacts[last_name]"]').val(contacts[key].last_name);

                    $('.customer_fields').slideUp('slow');
                    $('#request_fields').slideDown('slow');
                    $('#m_modal_create_request').find('button.btn-primary').removeAttr('disabled');

                    $(this).text(GetTranslation('clear'))
                        .attr('id', 'unselect-contact')
                        .blur();
                    event.preventDefault();
                });

                $('body').on('click', '#unselect-contact', function (event) {
                    clearModal(true);
                    clearFields();
                    $('#m_modal_create_request input[name="Contacts[email]"], #m_modal_create_request input[name="Contacts[phone]"]').val('');

                    $(this).text(GetTranslation('select'))
                        .attr('id', 'select-contact')
                        .blur();
                    event.preventDefault();
                });
            };

            var clearModal = function (disable) {
                $('#contacts').slideUp('slow', function () {
                    $(this).find('.m-widget4').empty();
                });

                $('.customer_fields').slideDown('slow');
                $('#request_fields').slideUp('slow');

                $('#m_modal_create_request input[name="contact_id"]').val('');

                if (disable == true)
                    $('button.btn-primary').prop('disabled', 'disabled');
            };

            var showFields = function () {
                $('#contacts').slideUp('slow', function () {
                    $(this).find('.m-widget4').empty();
                });

                //$('.customer_fields').slideUp('slow');
                $('#request_fields').slideDown('slow');
                $('#m_modal_create_request').find('button.btn-primary').removeAttr('disabled');
            };

            var clearFields = function () {
                $('#m_modal_create_request input[name="contact_id"]').val('');

                $('#m_modal_create_request input[name="Contacts[first_name]"], #m_modal_create_request input[name="Contacts[last_name]"]').val('');
            };

            var searchContacts = function (email, phone) {
                var data = {
                    email: email,
                    phone: phone
                };

                $.get(baseUrl + '/ita/' + tenantId + '/contacts/get-contacts', data, function(res) {
                    mApp.block('#m_modal_create_request .modal-body', {});

                    setTimeout(function() {
                        mApp.unblock('#m_modal_create_request .modal-body');

                        if (res.length > 0) {
                            $('.customer_fields').slideDown('slow');
                            $('#request_fields').slideUp('slow');

                            showContacts(res);
                        } else {
                            $('#m_modal_create_request input[name="contact_id"]').val('');
                            showFields();
                        }
                    }, 1000);
                });
            };

            email.on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    email_value = input.val().replace(/^_+|[@_.]+$/g, '');
                    phone_value = phone.val();
                    if (validateEmail(email_value) || phone.inputmask('unmaskedvalue').length >= 12) {
                        $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                        searchContacts(email_value, phone_value);
                    } else {
                        if ($('input[name="Contacts[first_name]"]').val().length < 1 && $('input[name="Contacts[last_name]"]').val().length < 1)
                            clearModal(true);
                    }
                }, 1000);
            });

            phone.on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    phone_value = input.val();
                    email_value = email.val().replace(/^_+|[@_.]+$/g, '');
                    if (validateEmail(email_value) || input.inputmask('unmaskedvalue').length >= 12) {
                        $('#m_modal_create_contact').find('#create_contact_submit').attr('disabled', 'disabled');

                        searchContacts(email_value, phone_value);
                    } else {
                        if ($('input[name="Contacts[first_name]"]').val().length < 1 && $('input[name="Contacts[last_name]"]').val().length < 1)
                            clearModal(true);
                    }
                }, 1000);
            });

            $('input[name="Contacts[first_name]"], input[name="Contacts[last_name]"]').on('input', function (e) {
                var input = $(this);

                searchDelay(function() {
                    var value = input.val();

                    if (value.length >= 1 && !$('#contacts').is(':visible')) {
                        $('#request_fields').slideDown('slow');
                        $('#m_modal_create_request').find('button.btn-primary').removeAttr('disabled');
                    }
                }, 1000);
            });
        }
        
        function initHeaderRequestsModal(){
            
            initRequestServicesPortlet();
            initServicesPopupWidgets();
            initCPModalsWidgets();
            
            function initRequestServicesPortlet(){
                var checkboxes = $('#header-request-services-list-partlet .serv_cbx_tag').get();
                var cp_change_buttons = $('#header_change_cp_method_modal .cm-item').get();
                var remove_service_buttons = $('#header-request-services-list-partlet .remove-service').get();
                var add_cp_btn = $('#add_header_commercial_proposal_btn').get();
                
                // Checkbox change event
                $(checkboxes).each(function(){
                    $(this).on('change', function(){
                        var checked_ids = getCheckedIds();
                        if(checked_ids.length>0){
                            $(add_cp_btn).stop().fadeIn(300);
                        }else{
                            $(add_cp_btn).stop().fadeOut(200);
                        }
                    });
                });
                
                // Check all checkbox
                $('#check_all_header_serv_cbx_tag').on('change', function(){
                    var check_all_box = this;
                    $(checkboxes).each(function(){
                        this.checked = check_all_box.checked;
                        $(this).trigger('change');
                    });
                });
                
                // Add CP button click event
                $(add_cp_btn).on('click', function(event){
                    event.preventDefault();
                    var checked_ids = getCheckedIds();
                    if(checked_ids.length>0){
                        $('#header_change_cp_method_modal').modal('show');
                    }else{
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(GetTranslation('request_cpp_no_changed_services_error', 'You dont change any services!'));
                    }
                });

                // Click on CP send variant event
                $(cp_change_buttons).each(function(){
                    var variant_button = this;
                    $(variant_button).on('click', function(){
                        var button = this;
                        var checked_ids = getCheckedIds();
                        var method = $(button).data('cp-method');
                        $('#header_change_cp_method_modal').modal('hide');
                        setTimeout(function(){
                            $('#header_cp_method_' + method + '_modal').modal('show');
                        }, 370);
                    });
                });
                
                // Collect ids from changed checkboxes
                function getCheckedIds(){
                    var ids = [];
                    $(checkboxes).each(function(){
                        if(this.checked){
                            ids.push(parseInt($(this).val()));
                        }
                    });
                    return ids;
                }
                
                // Remove service buttons init
                $(remove_service_buttons).on('click', function () {
                    var service_id = $(this).attr('data-id');
                    var item = $(this).closest('.service-item');
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        // text: button.data('message'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/requests/delete-service?id=' + service_id,
                                method: 'GET',
                                success: function (response) {
                                    var content = {
                                        title: GetTranslation('success_message', 'Success!'),
                                        message: ''
                                    };
                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        z_index: 1031,
                                        type: "success"
                                    });
                                    item.remove();
                                },
                                error: function (response) {
                                    var mes = response;
                                    if (response.error != undefined)
                                        mes = response.error;
                                    if (response.message != undefined)
                                        mes = response.message;
                                    if (response.responseText != undefined)
                                        mes = response.responseText;
                                    if (response.text != undefined)
                                        mes = response.text;
                                    var content = {
                                        title: 'Error',
                                        message: mes
                                    };
                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        type: "danger",
                                        z_index: 1031
                                    });
                                }
                            });
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                });
            }
            
            function initServicesPopupWidgets() {
                var modal = $('#m_header_modal_service_details').get()[0];
                var custom_portlet_title = $('#header-service-custom-details-portlet-title').get()[0];
                var additional_services_portlet = $('#header-service-additional-services-portlet').get()[0];
                var default_inputs_block = $('#header-service-default-details-block').get()[0];
                var custom_inputs_block = $('#header-service-custom-details-block').get()[0];
                var additional_services_block = $('#header-service-additional-services-portlet-block').get()[0];
                $(modal).on('show.bs.modal', function (event) {
                    $(modal).find('#hsm-btn-duplicate').hide();
                    if (event.relatedTarget !== undefined) {
                        clearDefaultPortlet();
                        clearCustomPortlet();
                        clearAdditionalServicesPortlet();
                        clearBaseAdditionalServicesPortlet();
                        var button = $(event.relatedTarget);

                        if (button.data('serv-id') !== undefined) {
                            var service_id = parseInt(button.data('serv-id'));
                            $('#hsm-create-label').show();
                            $('#hsm-update-label').hide();
                            $('#hsm-btn-back').show();

                            // Get service data
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                data: {
                                    service_id: service_id,
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                },
                                method: 'POST',
                                success: function (response) {
                                    var service = response;
                                    cur_service = service;
                                    var default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def);
                                    var custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields);

                                    var additional_services = response.links;
                                    var additional_services_html = getAdditionalServicesHtmlTemplate(service.id, additional_services);
                                    $(custom_portlet_title).text(service.name);
                                    $(default_inputs_block).html(default_inputs_html);
                                    $(custom_inputs_block).html(custom_inputs_html);

                                    // var inputs = $('#service-default-details-block').find('select.select2-country');
                                    // if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                    //     main_country = inputs.find('option:selected').clone();
                                    // else {
                                    //     inputs = $('#service-custom-details-block').find('select.select2-country');
                                    //     if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                    //         main_country = inputs.find('option:selected').clone();
                                    // }

                                    // Additional services partlet status
                                    if (additional_services_html.length > 0) {
                                        $(additional_services_block).html(additional_services_html).show();
                                        $(additional_services_portlet).show();
                                        $('#hsm-btn-next').attr('data-service-id', service_id);
                                    } else {
                                        $(additional_services_portlet).hide();
                                        $('#hsm-btn-next').hide();
                                        $('#hsm-btn-save').show().attr('data-type', 'create');
                                    }

                                    if(service.multiservices) {
                                        console.log(modal);
                                        var btn =  $(modal).find('#hsm-btn-duplicate');
                                        btn.show();
                                        btn.text(service.multiservices_name);
                                        btn.off('click').on('click',function () {
                                            $('#header-service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                            add_count ++;
                                            initDynamicInputs(modal);
                                            initCopyBtn(modal);
                                            initNextBtn(modal);
                                            initSaveBtn(modal)
                                        });
                                    }
                                    else {
                                        $(modal).find('#hsm-btn-duplicate').hide();
                                    }

                                    initDynamicInputs(modal);
                                    initCopyBtn(modal);
                                    initNextBtn(modal);
                                    initSaveBtn(modal)
                                },
                                error:function (response) {
                                    cur_service = undefined;
                                }
                            });
                        }
                        else if (button.data('id') !== undefined) {
                            var link_id = parseInt(button.data('id'));
                            $('#hsm-create-label').hide();
                            $('#hsm-update-label').show();
                            $('#hsm-btn-back').hide();
                            $('#hsm-btn-next').hide();
                            $('#hsm-btn-save').show().attr('data-type', 'update');
                            $(additional_services_block).hide();
                            $('#header-service-additional-services-details').html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                            // Get service data
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-link-id',
                                data: {
                                    link_id: link_id,
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                },
                                method: 'POST',
                                success: function (response) {
                                    var service = response;
                                    cur_service = service;

                                    var default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def, undefined, link_id);
                                    var custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields, undefined, link_id);
                                    var additional_services = response.links;

                                    $(custom_portlet_title).text(service.name);
                                    $(default_inputs_block).html(default_inputs_html);
                                    $(custom_inputs_block).html(custom_inputs_html);

                                    if(service.multiservices) {
                                        var btn =  $(modal).find('#hsm-btn-duplicate');
                                        btn.show();
                                        btn.text(service.multiservices_name);
                                        btn.off('click').on('click',function () {
                                            $('#header-service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                            add_count ++;
                                            initDynamicInputs(modal);
                                            initCopyBtn(modal);
                                            initNextBtn(modal);
                                            initSaveBtn(modal)
                                        });
                                    }
                                    else {
                                        $(modal).find('#hsm-btn-duplicate').hide();
                                    }

                                    $('#header-service-additional-services-details').empty();
                                    // var inputs = $('#service-default-details-block').find('select.select2-country');
                                    // if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                    //     main_country = inputs.find('option:selected').clone();
                                    // else {
                                    //     inputs = $('#service-custom-details-block').find('select.select2-country');
                                    //     if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                    //         main_country = inputs.find('option:selected').clone();
                                    // }
                                    for (key in additional_services) {
                                        if(!additional_services[key].type) {
                                            $('#header-service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                            $('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                                var portlet = new mPortlet(this);
                                                portlet.collapse();
                                            });
                                        } else {
                                            $('#header-service-duplicates').append(renderBaseAdditionalService(additional_services[key],add_count,key));
                                            add_count ++;
                                        }

                                        add_count++;
                                    }
                                    $('#hsm-add-srv-btn').empty();
                                    $.ajax({
                                        url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                        data: {
                                            service_id: service.id,
                                            _csrf: $('meta[name="csrf-token"]').attr('content')
                                        },
                                        method: 'POST',
                                        success: function (response) {
                                            $('#hsm-add-srv-btn').append(renderAddServicesList(response['links']));
                                            if (response['links'] == undefined || response['links'].length == 0)
                                                $('#header-service-additional-services-portlet').hide();
                                            else
                                                $('#header-service-additional-services-portlet').show();
                                            $('.add-service').on('click', function () {
                                                var service_id = $(this).attr('data-service-id');
                                                $.ajax({
                                                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                                    data: {
                                                        service_id: service_id,
                                                        _csrf: $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    method: 'POST',
                                                    success: function (response) {
                                                        $('#header-service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                                        $('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                                            var portlet = new mPortlet(this);
                                                        });
                                                        setDefault($('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]'));
                                                        add_count++;
                                                        initDynamicInputs(modal);
                                                        initCopyBtn(modal);
                                                        initNextBtn(modal);
                                                        initSaveBtn(modal)
                                                    }
                                                });
                                            });
                                        }
                                    });
                                    initDynamicInputs(modal);
                                    initCopyBtn(modal);
                                    initNextBtn(modal);
                                    initSaveBtn(modal);
                                },
                                error:function (response) {
                                    cur_service = undefined;
                                }
                            });
                        }
                    }
                });

                function clearDefaultPortlet() {
                    $(default_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                }

                function clearCustomPortlet() {
                    $(custom_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                    $(custom_portlet_title).text('');
                }

                function clearBaseAdditionalServicesPortlet() {
                    $('#header-service-duplicates').empty();
                }

                function clearAdditionalServicesPortlet() {
                    add_count = 0;
                    $('#hsm-add-srv-btn').empty();
                    $('#header-service-additional-services-details').empty();
                    $('#hsm-btn-next').show();
                    $('#hsm-btn-save').hide().attr('data-type', 'create');
                    $(additional_services_block).show();
                    $(additional_services_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                }
                
                function initCopyBtn(modal){
                    $('#m_header_modal_service_details .copy-service').off('click').on('click', function () {
                        var element = $(this).closest('.additional-filed');
                        var service_id = element.attr('data-service-id');
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: service_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                $('#header-service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                $('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                    var portlet = new mPortlet(this);
                                    portlet.expand();
                                });
                                add_count++;
                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal);
                                initSaveBtn(modal)
                            }
                        });
                    });
                }
                
                function initNextBtn(modal) {
                    $('#hsm-btn-next').off('click').on('click', function () {
                        $(modal).find('.select-additional').each(function () {
                            if ($(this).prop('checked')) {
                                var service_id = $(this).val();
                                $.ajax({
                                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                    data: {
                                        service_id: service_id,
                                        _csrf: $('meta[name="csrf-token"]').attr('content')
                                    },
                                    method: 'POST',
                                    success: function (response) {
                                        $('#header-service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                        $('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                            var portlet = new mPortlet(this);
                                        });
                                        add_count++;
                                        initDynamicInputs(modal);
                                        initCopyBtn(modal);
                                        initNextBtn(modal);
                                        initSaveBtn(modal)
                                    }
                                });
                            }
                        });
                        $(this).hide();
                        $('#header-service-additional-services-portlet-block').hide();
                        $('#hsm-btn-save').show();
                        $('#hsm-add-srv-btn').empty();
                        // Get service data
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: $(this).attr('data-service-id'),
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                $('#hsm-add-srv-btn').append(renderAddServicesList(response['links']));
                                $(modal).find('.add-service').on('click', function () {
                                    var service_id = $(this).attr('data-service-id');
                                    $.ajax({
                                        url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                        data: {
                                            service_id: service_id,
                                            _csrf: $('meta[name="csrf-token"]').attr('content')
                                        },
                                        method: 'POST',
                                        success: function (response) {
                                            $('#header-service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                            $('#header-service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                                var portlet = new mPortlet(this);
                                                portlet.expand();
                                            });
                                            add_count++;
                                            initDynamicInputs(modal);
                                            initCopyBtn(modal);
                                            initNextBtn(modal);
                                            initSaveBtn(modal)
                                        }
                                    });
                                });
                            }
                        });
                    });
                }
                
                function initSaveBtn(modal){
                    $('#hsm-btn-save').off('click').on('click', function () {
                        var form = $(this).closest('.modal-content').find('form');
                        var formData = new FormData(form[0]);
                        $(this).attr('disabled', true);
                        if ($(this).attr('data-type') == 'create') {
                            $(modal).find('.modal-body')
                                .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/requests/add-service',
                                method: 'POST',
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    $('#hsm-btn-save').attr('disabled', false);
                                    window.location.reload();
                                },
                                error: function (data) {
                                    $('#hsm-btn-save').attr('disabled', false);
                                    console.log(data);
                                }
                            });
                        } else {
                            $(modal).find('.modal-body')
                                .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/requests/update-service',
                                method: 'POST',
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    $('#hsm-btn-save').attr('disabled', false);
                                    // $('#m_modal_service_details').modal('hide');
                                    window.location.reload();
                                },
                                error: function (data) {
                                    $('#hsm-btn-save').attr('disabled', false);
                                    console.log(data);
                                    // window.location.reload();
                                }
                            });
                        }
                    });
                }
            }
            
            function initCPModalsWidgets(){
                // Select of sms contact phone
                var phone_select = $('#header_cp_for_who_select').get()[0];
                var new_phone_input = $('#header_cp_for_who_input').get()[0];
                $(phone_select).on('change', function(){
                    var select_val = $(phone_select).val();
                    if(select_val === 'CREATE_NEW_CONTACT'){
                        phone_select.disabled = true;
                        $(phone_select).val('').selectpicker('refresh').selectpicker('hide');
                        $(new_phone_input).show();
                    }
                });

                // Select of email contact phone
                var phone_select = $('#header_cp_email_for_who_select').get()[0];
                var new_phone_input = $('#header_cp_email_for_who_input').get()[0];
                $(phone_select).on('change', function(){
                    var select_val = $(phone_select).val();
                    if(select_val === 'CREATE_NEW_CONTACT'){
                        phone_select.disabled = true;
                        $(phone_select).val('').selectpicker('refresh').selectpicker('hide');
                        $(new_phone_input).show();
                    }
                });

                // Sms boby textarea with sms-counter
                autosize($('#header_cp_sms_body_textarea'));
                $('#header_cp_sms_body_textarea').countSms('#header-sms-counter');

                // Copy link button
                var copy_btn = $('#header_copy_link_button').get()[0];
                var link_input = $('#header_copy_link_input').get()[0];
                var copy_bufer = $('#header_copy_bufer').get()[0];
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                $(copy_btn).on('click', function(){
                    var value = $(link_input).val();
                    if(value.length>0){
                        $(copy_bufer).text(value);
                        copy_bufer.select();
                        try {
                            var successful = document.execCommand('copy');
                            if(successful === true){
                                toastr.success(GetTranslation('request_cp_link_modal_bufer_copy_success', 'Link was copy in buffer'));
                                $('#header_cp_method_link_modal').modal('hide');
                            }else{
                                toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                            }
                        } catch (err) {
                            console.log(err);
                            toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                        }
                    }else{
                        toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_no_data_error', 'No data for copy'));
                    }
                });
                
                // Email body Summernote init
                $('#header_cp_method_email_modal').on('shown.bs.modal', function(){
                    if($('#header_cp_email_body_editor').data('was-inited') !== true){
                        // init only when modal was shown (this is for summernote editable area bug slove)
                        $('#header_cp_email_body_editor').summernote({
                            height: 300,
                            toolbar: [
                                // [groupName, [list of button]]
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough', 'superscript', 'subscript']],
                                ['fontsize', ['fontsize']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['height', ['height']]
                            ]
                        });
                        $('#header_cp_email_body_editor').data('was-inited', true);
                    }
                });
            }
        }
        
        function hideBodySchrollbarWhenModalOpen(){
            $('.modal').each(function(){
                var modal = this;
                $(modal).on('shown.bs.modal', function(){
                    $('body').addClass('no-overflow');
                });
                $(modal).on('hidden.bs.modal', function(){
                    if($('body').find('.modal.show').length === 0) {
                        // There are no any more opened modals on the page
                        $('body').removeClass('no-overflow');
                    } else {
                        // There are opened modals on the page yet , so body need class 'modal-open'
                        $('body').addClass('modal-open');
                    }
                });
            });
        }
        
        function disableAutoFormComplete(){
            $('form').attr('autocomplete','off');
        }
        
        function initEditableModalTitles(){
            $('.editable-title').each(function(){
                var title_block = this;
                if(!$(title_block).data('inited')){
                    var span = $(title_block).find('>span')[0];
                    var input = $(title_block).find('>input')[0];
                    
                    if(!$(input).prop('disabled')){
                        $(span).on('click', function () {
                            $(this).hide();
                            $(input).show().focus();
                        });
                    } else {
                        $(span).css('cursor', 'not-allowed');
                        $(span).find('i').hide();
                    }
                    $(input).on('focusout change', function () {
                        $(span).find('>.text').text($(input).val());
                        $(span).show();
                        $(input).hide();
                    });
                    $(input).on('keyup', function (e) {
                        // Enter pressed event
                        if(e.which === 13) {
                            $(span).find('>.text').text($(input).val());
                            $(span).show();
                            $(input).blur().hide();
                        }
                    });
                    $(title_block).data('inited', true);
                }
            });
        }
        
    });
})(jQuery);