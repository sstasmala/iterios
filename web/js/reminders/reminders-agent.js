(function($){

    /* Vars */

    /* Events */
    $(document).ready(function(){
        initSwitchers();
        initEditRemindersModal();
        initRemindersTriggers();
    });

    /* Functions */
    function initSwitchers(){
        $('.status_switcher').on('change', function(){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if(this.checked){
                toastr.success('Access enabled!');
                console.log('Access enabled');
            }else{
                toastr.warning('Access disabled!');
                console.log('Access disabled');
            }
            // Check if all checkboxes disabled
            var row = $(this).closest('tr').get()[0];
            var checked_count = 0;
            $(row).find('input.status_switcher').each(function(){
                if(this.checked){
                    checked_count++;
                }
            });
            if(checked_count > 0){
                $(row).removeClass('all-dissabled');
            }else{
                $(row).addClass('all-dissabled');
            }
        });
    }

    function initEditRemindersModal(){
        
        // Agent - Edit modal
        $('#edit_agent_reminder_popup').on('shown.bs.modal', function (event) {
            initAgentEditModal(this, 'edit', event);
        });
        $('#edit_agent_reminder_popup').on('hidden.bs.modal', function (event) {
            proccessAgentEditModalClose(this, 'edit', event);
        });
        // Agent - Read modal
        $('#edit_agent_reminder_readonly_popup').on('shown.bs.modal', function (event) {
            initAgentReadModal(this, 'read', event);
        });
        $('#edit_agent_reminder_readonly_popup').on('hidden.bs.modal', function (event) {
            proccessAgentReadModalClose(this, 'read', event);
        });
        
        var format_24 = (time_format === 'hh:mm:ss A') ? true : false;
        $('#notification_period_time').timepicker({
            minuteStep: 15,
            showSeconds: false,
            showMeridian: format_24,
            defaultTime: '12:00 AM'
        });
        
        function initAgentEditModal(modal, mode, event){
            if(event.relatedTarget !== undefined){
                var button = $(event.relatedTarget);
                if (button.data('reminder-id') !== undefined) {
                    var reminder_id = button.data('reminder-id');
                    // Get reminder data via ajax
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/reminders/get-reminder',
                        data: {
                            reminder_id: reminder_id,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'POST',
                        success:function (reminder) {
                            // Update fields
                            $(modal).find('.notification_title_input').val(reminder.name).trigger('change');
                            $(modal).find('#reminder_id_hidden_input').val(reminder.id);
                            $(modal).find('#notification_activate_mode_select').val(reminder.notification_activate_mode);
                            // Functions helpers
                            updateChannelsCheckboxes(modal, mode, reminder.channels);
                            initCopyButton(modal, reminder.id);
                            initNotificationActivateModeSelect(modal, mode);
                            // Hide preloader
                            $(modal).find('.preloader').css({"display":"none"});
                        }
                    });
                }
            }
        }
        
        function proccessAgentEditModalClose(modal, mode, event){
            //Show preloader
            $(modal).find('.preloader').css({"display":"flex"});
            //Clear fields
            clearAllFields(modal, mode);
        }
        
        function initAgentReadModal(modal, mode, event){
            if(event.relatedTarget !== undefined){
                var button = $(event.relatedTarget);
                if (button.data('reminder-id') !== undefined) {
                    var reminder_id = button.data('reminder-id');
                    // Get reminder data via ajax
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/reminders/get-reminder',
                        data: {
                            reminder_id: reminder_id,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'POST',
                        success:function (reminder) {
                            // Update fields
                            $(modal).find('.notification_title_input').val(reminder.name).trigger('change');
                            $(modal).find('#reminder_id_hidden_input').val(reminder.id);
                            $(modal).find('#notification_activate_mode_select').val(reminder.notification_activate_mode);
                            // Functions helpers
                            updateChannelsCheckboxes(modal, mode, reminder.channels);
                            initCopyButton(modal, reminder.id);
                            initNotificationActivateModeSelect(modal, mode);
                            // Hide preloader
                            $(modal).find('.preloader').css({"display":"none"});
                        }
                    });
                }
            }
        }
        
        function proccessAgentReadModalClose(modal, mode, event){
            //Show preloader
            $(modal).find('.preloader').css({"display":"flex"});
            //Clear fields
            clearAllFields(modal, mode);
        }
        
        function clearAllFields(modal, mode){
            // Reset inputs
            $(modal).find('#notification_period_date_display').datepicker("setDate", new Date()).val('');
            $(modal).find('#notification_period_date_value').val('');
            $(modal).find('#notification_period_time').timepicker('setTime', '12:00 AM');
            $(modal).find('#notification_subscribers_select').val(1).selectpicker('refresh');
            // Reset repeater
            if(mode !== 'read') {
                $(modal).find('#source_sets_repeater [data-repeater-list]').empty();
                $(modal).find('#source_sets_repeater [data-repeater-create]').click();
            }
        }
        
        function updateChannelsCheckboxes(modal, modal_mode, channels){
            var html = '';
            for (var i=0; i<channels.length; i++){
                html += '<label class="m-checkbox mr-3">\
                            <input type="checkbox" name="Channel[]"' + ((channels[i].checked) ? ' checked' : '') + ' data-editor-channel="' + channels[i].name + '">\
                            ' + channels[i].name + '\
                            <span></span>\
                        </label>';
            }
            $(modal).find('.channels-check-block').html(html);
            
            // First poccess function call
            poccessCheckboxes();
            // Add events to checkboxes
            $(modal).find('.channels-check-block input[type="checkbox"]').on('change', function(){
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success('Настройки сохранены!');
                poccessCheckboxes();
            });
            
            function poccessCheckboxes(){
                $(modal).find('.channels-check-block input[type="checkbox"]').each(function(){
                    var channel = $(this).data('editor-channel');
                    switch(channel){
                        case 'Email':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="email_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="email_text"]').prop('disabled', true);
                            }
                        break;
                        case 'Notice':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="notice_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="notice_text"]').prop('disabled', true);
                            }
                        break;
                        case 'Task':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="task_name"]').prop('disabled', false);
                                    $(modal).find('[name="task_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="task_name"]').prop('disabled', true);
                                $(modal).find('[name="task_text"]').prop('disabled', true);
                            }
                        break;
                    }
                });
            }
        }
        
        function initNotificationActivateModeSelect(modal, mode){
            var select = $(modal).find('#notification_activate_mode_select').get()[0];
            var time_select = $(modal).find('#notification_period_time').get()[0];
            var current_value = $(select).val();
            checkTimeInput(current_value);
            //Event change
            $(select).off('change').on('change', function(){
                var value = $(select).val();
                checkTimeInput(value);
            });
            //Reinit selectpicker
            $(select).selectpicker('destroy');
            $(select).selectpicker();
            
            function checkTimeInput(val){
                // Show or hide time input
                switch(val){
                    case '1':
                        // Hide
                        time_select.disabled = true;
                        $(time_select).fadeOut(200);
                        $(modal).find('h6.notification_period_time_heading').fadeOut(200);
                        break;
                    case '2':
                        // Show
                        if(mode !== 'read'){
                            time_select.disabled = false;
                        }
                        $(time_select).fadeIn(200);
                        $(modal).find('h6.notification_period_time_heading').fadeIn(200);
                        break;
                }
            }
        }
        
        function initCopyButton(modal, reminder_id){
            $(modal).find('.copy_reminder_btn').off('click').on('click', function(){
                alert('Start copy reminder id=' + reminder_id);
            });
        }
    }

    function initRemindersTriggers(){
        // Init source repeater functionality
        $('.source_sets_repeater').each(function(){
            var repeater_block = this;
            
            $(repeater_block).repeater({
                initEmpty: false,
                defaultValues: {
                    'input-name': 'foo'
                },
                show: function() {
                    // Init select2
                    $(this).find('.repeater_m_selectpicker').selectpicker({
                        width: '100%'
                    });
                    // Init functionality for source inputs work
                    initSourceTriggersLogic(this);  //TODO!
                    // Show row
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    swal({
                        title: GetTranslation('delete_question','Are you sure?'),
                        text: GetTranslation('delete_this_source_question','Delete this source?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                }      
            });
            $(this).find('.repeater_m_selectpicker').selectpicker({
                width: '100%'
            });
        });
        
        function initSourceTriggersLogic(node){
            // node - page element where we must find source inputs and 
            // add events for them
        }
    }
})(jQuery);
