var reminders_marks_datatable = null;

(function($){

    /* Vars */

    /* Events */
    $(document).ready(function(){
        initSwitchers();
        initEditRemindersModal();
        initRemindersTriggers();
        initRemindersTableFilter();
        initMarksDatatable();
    });

    /* Functions */
    function initSwitchers(){
        $('.status_switcher').on('change', function(){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if(this.checked){
                toastr.success('Access enabled!');
                console.log('Access enabled');
            }else{
                toastr.warning('Access disabled!');
                console.log('Access disabled');
            }
            // Check if all checkboxes disabled
            var row = $(this).closest('tr').get()[0];
            var checked_count = 0;
            $(row).find('input.status_switcher').each(function(){
                if(this.checked){
                    checked_count++;
                }
            });
            if(checked_count > 0){
                $(row).removeClass('all-dissabled');
            }else{
                $(row).addClass('all-dissabled');
            }
        });
    }

    function initEditRemindersModal(){
        
        // Tourist - Edit modal
        $('#edit_tourist_reminder_popup').on('shown.bs.modal', function (event) {
            initTouristEditModal(this, 'edit', event);
        });
        $('#edit_tourist_reminder_popup').on('hidden.bs.modal', function (event) {
            proccessTouristEditModalClose(this, 'edit', event);
        });
        // Tourist - Read modal
        $('#edit_tourist_reminder_readonly_popup').on('shown.bs.modal', function (event) {
            initTouristReadModal(this, 'read', event);
        });
        $('#edit_tourist_reminder_readonly_popup').on('hidden.bs.modal', function (event) {
            proccessTouristReadModalClose(this, 'read', event);
        });
        
        var format_24 = (time_format === 'hh:mm:ss A') ? true : false;
        $('#notification_period_time').timepicker({
            minuteStep: 15,
            showSeconds: false,
            showMeridian: format_24,
            defaultTime: '12:00 AM'
        });
        
        function initTouristEditModal(modal, mode, event){
            if(event.relatedTarget !== undefined){
                var button = $(event.relatedTarget);
                
                if (button.data('reminder-id') !== undefined) {
                    var reminder_id = button.data('reminder-id');
                    
                    // Init notification mode select logic
                    initNotificationActivateModeSelect(modal, mode);
                    
                    // Get reminder data via ajax
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/reminders/get-reminder',
                        data: {
                            reminder_id: reminder_id,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'POST',
                        success:function (reminder) {
                            // Update fields
                            $(modal).find('.notification_title_input').val(reminder.name).trigger('change');
                            $(modal).find('#reminder_id_hidden_input').val(reminder.id);
                            CKEDITOR.instances.trme_email_text_ckeditor.setData('Reminder email body content...');
                            // Functions helpers
                            initCopyButton(modal, reminder.id);
                            // Hide preloader
                            $(modal).find('.preloader').css({"display":"none"});
                        }
                    });
                }
            }
        }
        
        function proccessTouristEditModalClose(modal, mode, event){
            //Show preloader
            $(modal).find('.preloader').css({"display":"flex"});
            // Reset inputs
            $(modal).find('#trme_email_theme_input').val('');
            $(modal).find('#notification_period_time').timepicker('setTime', '12:00 AM');
            // Reset repeater
            $(modal).find('.source_sets_repeater [data-repeater-list]').empty();
            $(modal).find('.source_sets_repeater [data-repeater-create]').click();
            // Reset email CKEDITOR
            CKEDITOR.instances.trme_email_text_ckeditor.setData('');
        }
        
        function initTouristReadModal(modal, mode, event){
            if(event.relatedTarget !== undefined){
                var button = $(event.relatedTarget);
                
                if (button.data('reminder-id') !== undefined) {
                    var reminder_id = button.data('reminder-id');
                    
                    // Init notification mode select logic
                    initNotificationActivateModeSelect(modal, mode);
                    
                    // Get reminder data via ajax
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/reminders/get-reminder',
                        data: {
                            reminder_id: reminder_id,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'POST',
                        success:function (reminder) {
                            // Update fields
                            $(modal).find('.notification_title_input').val(reminder.name).trigger('change');
                            $(modal).find('#reminder_id_hidden_input').val(reminder.id);
                            CKEDITOR.instances.trmr_email_text_ckeditor.setData('Reminder email body content...');
                            // Functions helpers
                            initCopyButton(modal, reminder.id);
                            // Hide preloader
                            $(modal).find('.preloader').css({"display":"none"});
                        }
                    });
                }
            }
        }
        
        function proccessTouristReadModalClose(modal, mode, event){
            //Show preloader
            $(modal).find('.preloader').css({"display":"flex"});
            // Reset inputs
            $(modal).find('#notification_period_time').timepicker('setTime', '12:00 AM');
            // Reset email CKEDITOR
            CKEDITOR.instances.trmr_email_text_ckeditor.setData('');
        }
        
        function updateChannelsCheckboxes(modal, modal_mode, channels){
            var html = '';
            for (var i=0; i<channels.length; i++){
                html += '<label class="m-checkbox mr-3">\
                            <input type="checkbox" name="Channel[]"' + ((channels[i].checked) ? ' checked' : '') + ((modal_mode === 'read') ? ' disabled' : '') + ' data-editor-channel="' + channels[i].name + '">\
                            ' + channels[i].name + '\
                            <span></span>\
                        </label>';
            }
            $(modal).find('.channels-check-block').html(html);
            
            // First poccess function call
            poccessCheckboxes();
            // Add events to checkboxes
            $(modal).find('.channels-check-block input[type="checkbox"]').on('change', function(){
                poccessCheckboxes();
            });
            
            function poccessCheckboxes(){
                $(modal).find('.channels-check-block input[type="checkbox"]').each(function(){
                    var channel = $(this).data('editor-channel');
                    switch(channel){
                        case 'Email':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="email_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="email_text"]').prop('disabled', true);
                            }
                        break;
                        case 'Notice':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="notice_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="notice_text"]').prop('disabled', true);
                            }
                        break;
                        case 'Task':
                            if(this.checked){
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideDown(200);
                                if(modal_mode !== 'read'){
                                    $(modal).find('[name="task_name"]').prop('disabled', false);
                                    $(modal).find('[name="task_text"]').prop('disabled', false);
                                }
                            }else{
                                $(modal).find('[data-channel="' + channel + '"]').stop().slideUp(200);
                                $(modal).find('[name="task_name"]').prop('disabled', true);
                                $(modal).find('[name="task_text"]').prop('disabled', true);
                            }
                        break;
                    }
                });
            }
        }
        
        function initNotificationActivateModeSelect(modal, mode){
            var trigger_constructor_block = $(modal).find('.trigger-constructor').get()[0];
            var select = $(modal).find('#notification_activate_mode_select').get()[0];
            var days_select = $(modal).find('#notification_period_days').get()[0];
            var time_select = $(modal).find('#notification_period_time').get()[0];
            var current_value = $(select).val();
            
            checkInputsStatus(current_value);
            //Event change
            $(select).off('change').on('change', function(){
                var value = $(select).val();
                checkInputsStatus(value);
            });
            //Reinit selectpicker
            $(select).selectpicker('destroy');
            $(select).selectpicker();
            
            function checkInputsStatus(val){
                // Show or hide time input
                switch(val){
                    case 'in_event_moment':
                        // Hide days
                        days_select.disabled = true;
                        $(trigger_constructor_block).addClass('no-days');
                        // Hide time
                        time_select.disabled = true;
                        $(trigger_constructor_block).addClass('no-time');
                        break;
                    case 'one_time':
                        // Hide days
                        days_select.disabled = true;
                        $(trigger_constructor_block).addClass('no-days');
                        // Show time
                        time_select.disabled = (mode === 'read') ? true : false;
                        $(trigger_constructor_block).removeClass('no-time');
                        break;
                    case 'days_before':
                        // Show days
                        days_select.disabled = (mode === 'read') ? true : false;
                        $(trigger_constructor_block).removeClass('no-days');
                        // Show time
                        time_select.disabled = (mode === 'read') ? true : false;
                        $(trigger_constructor_block).removeClass('no-time');
                        break;
                    case 'days_after':
                        // Show days
                        days_select.disabled = (mode === 'read') ? true : false;
                        $(trigger_constructor_block).removeClass('no-days');
                        // Show time
                        time_select.disabled = (mode === 'read') ? true : false;
                        $(trigger_constructor_block).removeClass('no-time');
                        break;
                }
            }
        }
        
        function initCopyButton(modal, reminder_id){
            $(modal).find('.copy_reminder_btn').off('click').on('click', function(){
                alert('Start copy reminder id=' + reminder_id);
            });
        }
        
        // Init Email edit body CKEditor init
        CKEDITOR.replace('trme_email_text_ckeditor', {
            customConfig: '',
            allowedContent: true,
            disallowedContent: 'img{width,height,float}',
            extraAllowedContent: 'img[width,height,align]',
            extraPlugins: 'placeholder_edit',
            height: 500,
            contentsCss: [
                baseUrl + '/admin/plugins/ckeditor-full/contents.css',
//                baseUrl + '/admin/plugins/ckeditor-full/document-style.css'
            ],
            bodyClass: 'document-editor',
            format_tags: 'p;h1;h2;h3;pre',
            removeDialogTabs: 'image:advanced;link:advanced',
            stylesSet: [
                {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                {name: 'Cited Work', element: 'cite'},
                {name: 'Inline Quotation', element: 'q'},
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
            ],
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']}
            ],
            removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
        });
        
        // Init Email read body CKEditor init
        CKEDITOR.replace('trmr_email_text_ckeditor', {
            customConfig: '',
            allowedContent: true,
            disallowedContent: 'img{width,height,float}',
            extraAllowedContent: 'img[width,height,align]',
            extraPlugins: 'placeholder_edit',
            readOnly: true,
            height: 500,
            contentsCss: [
                baseUrl + '/admin/plugins/ckeditor-full/contents.css',
//                baseUrl + '/admin/plugins/ckeditor-full/document-style.css'
            ],
            bodyClass: 'document-editor',
            format_tags: 'p;h1;h2;h3;pre',
            removeDialogTabs: 'image:advanced;link:advanced',
            stylesSet: [
                {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                {name: 'Cited Work', element: 'cite'},
                {name: 'Inline Quotation', element: 'q'},
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
            ],
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']}
            ],
            removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
        });
    }

    function initRemindersTriggers(){
        // Init source repeater functionality
        $('.source_sets_repeater').each(function(){
            var repeater_block = this;
            
            $(repeater_block).repeater({
                initEmpty: false,
                defaultValues: {
                    'input-name': 'foo'
                },
                show: function() {
                    // Init select2
                    $(this).find('.repeater_m_selectpicker').selectpicker({
                        width: '100%'
                    });
                    // Init functionality for source inputs work
                    initSourceTriggersLogic(this);  //TODO!
                    // Show row
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    swal({
                        title: GetTranslation('delete_question','Are you sure?'),
                        text: GetTranslation('delete_this_source_question','Delete this source?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                }      
            });
            $(this).find('.repeater_m_selectpicker').selectpicker({
                width: '100%'
            });
        });
        
        function initSourceTriggersLogic(node){
            // node - page element where we must find source inputs and 
            // add events for them
        }
    }
    
    function initRemindersTableFilter(){
        var filter = $('#reminders-filter').get()[0];
        var portlet = $('#reminders-index-portlet').get()[0];
        if((filter !== undefined) && (filter !== null)){
            $(filter).find('button').on('click', function(){
                var clicked_btn = this;
                var filter_type = $(clicked_btn).data('filter');
                $(filter).find('button').removeClass('btn-brand').addClass('btn-secondary');
                $(clicked_btn).removeClass('btn-secondary').addClass('btn-brand');
                // Show/hide content parts
                $(portlet).find('.reminders-index-portlet-content').removeClass('active');
                $(portlet).find('.reminders-index-portlet-content[data-content-part-'+filter_type+'="true"]').addClass('active');
            });
        }
    }
    function initMarksDatatable(){
        var columns = [
            {
                field: 'short_code',
                title: GetTranslation('tdi_marks_col_title_name', 'Mark'),
                template: function(row, index, datatable) {
                    return '<button type="button" class="btn btn-success btn-sm copy_mark_btn mr-3" title="' + GetTranslation('tdi_copy_mark_btn', 'Copy mark') + '" data-pl_id="'+row.id+'">'+
                               '<i class="fa fa-copy"></i>'+
                            '</button>'
                        + '<span id="placeholder_text_'+row.id+'">{{'
                        + row.short_code + '}}</span>';    // Return random name from stack
                }
            },
            {
                field: 'description',
                title: GetTranslation('tdi_marks_col_title_description', 'Description'),
                template: function(row, index, datatable) {
                    return '<span data-toggle="m-popover" data-placement="top" data-content="' + (row.default == null ? '' : row.default) + '">' + row.description + '</span>';
                }
            }
        ];

        reminders_marks_datatable = $('#reminders_marks_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: baseUrl + '/ita/' + tenantId + '/templates/get-email-and-sms-placeholders',
                        method: 'GET',
                        params: {
                            query: {
                                // generalSearch: ''
                            },
                            search: {
                            }
                            // custom query params
                        },
                        map: function (raw) {

                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 400
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    // Init popovers
                    mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    initCopyMarkButtons(row, data, index);
                },
                callback: function () {

                },
                // auto hide columns, if rows overflow. work on non locked columns
                autoHide: false
            },
            columns: columns,
            toolbar: {
                layout: ['pagination', 'info'],
                placement: ['bottom'],  //'top', 'bottom'
                items: {
                    pagination: {
                        type: 'default',
                        pages: {
                            desktop: {
                                layout: 'default',
                                pagesNumber: 6
                            },
                            tablet: {
                                layout: 'default',
                                pagesNumber: 3
                            },
                            mobile: {
                                layout: 'compact'
                            }
                        },
                        navigation: {
                            prev: true,
                            next: true,
                            first: true,
                            last: true
                        },
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    },
                    info: true
                }
            },
            translate: {
                records: {
                    processing: GetTranslation('datatable_data_processiong','Please wait...'),
                    noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: GetTranslation('pagination_first','First'),
                                prev: GetTranslation('pagination_previous','Previous'),
                                next: GetTranslation('pagination_next','Next'),
                                last: GetTranslation('pagination_last','Last'),
                                more: GetTranslation('pagination_more','More pages'),
                                input: GetTranslation('pagination_page_number','Page number'),
                                select: GetTranslation('pagination_page_size','Select page size')
                            },
                            info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                        }
                    }
                }
            }
        });

        /* DATATABLE EVENTS */

        // Init event
        $(reminders_marks_datatable).on('m-datatable--on-init', function(e){

        });
        // Layout updated event
        $(reminders_marks_datatable).on('m-datatable--on-layout-updated', function(e){

        });

        function initCopyMarkButtons(row, data, index){
            $(row).find('.copy_mark_btn').each(function(){
                var button = this;
                $(button).on('click', function(){
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    var pl_id = $(this).data('pl_id');
                    var root = document.getElementById('placeholder_text_' + pl_id);
                    var range = document.createRange();

                    range.setStart(root, 0);
                    range.setEnd(root, 1);
                    window.getSelection().empty();
                    window.getSelection().addRange(range);

                    try {
                        var successful = document.execCommand('copy');
                        window.getSelection().empty();

                        if (successful === true)
                            toastr.success(GetTranslation('tdi_mark_copy_success_message', 'Mark was copied'));
                    } catch (err) {
                        alert(err);
                    }

                });
            });
        }

        $('#reminders_marks_search').on('keydown', function (e) {
            var search = $(this).val();
            if(e.which == 13) {
                reminders_marks_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: $.extend(reminders_marks_datatable.options.data.source.read.params.search, {search_text: search})
                };
                reminders_marks_datatable.reload();
            }
        });
    }

})(jQuery);
