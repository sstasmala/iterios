//== Class definition
var EmailDeliveryCreateWizard = function () {
    //== Base elements
    var formEl = $('#delivery_email_form');
    var validator;
    var wizard;
    
    //== Private functions
    var initWizard = function () {
        //== Initialize form wizard
        wizard = new mWizard('delivery_email_wizard', {
            startStep: 1,
            manualStepForward: true
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function (wizard) {
            if (validator.form() !== true) {
                wizard.stop();  // don't go to the next step
            }
            var current_step = wizard.getStep();
            switch (current_step) {
                case 1:break;
            }
        });

        //== Change event
        wizard.on('change', function(wizard) {
            mUtil.scrollTop();
        });
    };

    var initValidation = function() {
        validator = formEl.validate({
            ignore: ":hidden",
            rules: {
                'EmailsDispatch[segments][]': {
                    required: true
                },
                'EmailsDispatch[email]': {
                    required: true
                },
                'EmailsDispatch[email_type]': {
                    required: true
                }
            },
            messages: {
                
            },
            
            invalidHandler: function(event, validator) {
                mUtil.scrollTop();
                swal({
                    "title": "",
                    "text": GetTranslation("dess_email_create_wizard_form_has_error_message", "There are some errors in your submission. Please correct them."),
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {
                console.log(form);
            }
        });   
    };

    var initSubmit = function() {
        var btn = formEl.find('[data-wizard-action="submit"]');

        btn.on('click', function(e) {
            e.preventDefault();


            if (validator.form()) {
                //== See: src\js\framework\base\app.js
                mApp.progress(btn);
                //mApp.block(formEl); 

                //== See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/delivery-email/create-dispatch',
                    success: function(response) {
                        mApp.unprogress(btn);
                        //mApp.unblock(formEl);
                        var id = response;
                        swal({
                            "title": "", 
                            "text": GetTranslation('dess_email_create_form_success', "The form has been successfully submitted!"),
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                        }).then( function(isConfirm) {
                           window.location.href = 'view?id='+id;
                        });
                    }
                });
            }
        });
    };

    return {
        // public functions
        init: function() {
            formEl = $('#delivery_email_form');
            initWizard(); 
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function() {
    EmailDeliveryCreateWizard.init();
});