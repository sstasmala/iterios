(function ($) {
    $(document).ready(function () {

        initPageWidgets();
        daterangepickerFilterInit();
        initEmailBodyTemplateModal();
        initEmailBodyDesignerModal();
        initEmailBodyRawModal();
        initSegmentsPreviewDatatable();
        
        function initPageWidgets() {
            // Delivery name and theme inputs with copy functional
            var name_input = $('#deliv_name_input').get()[0];
            var theme_input = $('#deliv_theme_input').get()[0];
            if(name_input !== undefined && name_input !== null && theme_input !== undefined && theme_input !== null){
                $(name_input).on('keyup', function(){
                    var name_val = $(name_input).val();
                    var theme_val = $(theme_input).val();
                    var manual_mode = $(theme_input).data('manual-mode');
                    if(!manual_mode || theme_val === ''){
                        $(theme_input).data('manual-mode', false);
                        $(theme_input).val(name_val);
                    }
                });
                $(theme_input).on('keyup', function(){
                    $(theme_input).data('manual-mode', true);
                });
            }

            $('select[name="EmailsDispatch[provider_id]"]').on('change',function () {
                $('.provider-placeholeder').hide();
                $('.provider-data').hide();
                var provider_id = $(this).val();
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-provider-info?id='+provider_id,
                    dataType: 'json',
                    type: 'get',
                    success: function (response) {
                        $('#provider_name').text(response.name);
                        $('#provider_description').text(response.description);
                        $('#provider_website').text(response.site).attr('href',response.site);
                        if(response.total_sub != undefined)
                            $('#sub_count').text(response.total_sub);

                        $('#contacts_count').text(response.total_contacts);
                        $('#dispatch_count').text(response.dispatches);
                        $('.provider-data').show();
                    }
                });
            });
        }
        
        function daterangepickerFilterInit() {
            var picker = $('#delivery_email_statistic_date_filter');
            var start = moment();
            var end = moment();

            function cb(start, end, label) {
                var title = '';
                var range = '';

                if ((end - start) < 100) {
                    title = 'Today:';
                    range = start.format('MMM D');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D');
                } else {
                    range = start.format('MMM D') + ' - ' + end.format('MMM D');
                }

                picker.find('.m-subheader__daterange-date').html(range);
                picker.find('.m-subheader__daterange-title').html(title);
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        }
        
        function initEmailBodyTemplateModal() {
            // Email body - template CK Editor
            var editor_height = (window.innerHeight - 340) < 400 ? 400 : window.innerHeight - 340;
            var config = {
                customConfig: '',
                allowedContent: true,
                disallowedContent: 'img{width,height,float}',
                extraAllowedContent: 'img[width,height,align]',
                extraPlugins: 'placeholder_edit',
                height: editor_height,
                contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css', baseUrl + '/admin/plugins/ckeditor-full/document-style.css'],
                bodyClass: 'document-editor',
                format_tags: 'p;h1;h2;h3;pre',
                removeDialogTabs: 'image:advanced;link:advanced',
                stylesSet: [
                    {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                    {name: 'Cited Work', element: 'cite'},
                    {name: 'Inline Quotation', element: 'q'},
                    {
                        name: 'Special Container',
                        element: 'div',
                        styles: {
                            padding: '5px 10px',
                            background: '#eee',
                            border: '1px solid #ccc'
                        }
                    },
                    {
                        name: 'Compact table',
                        element: 'table',
                        attributes: {
                            cellpadding: '5',
                            cellspacing: '0',
                            border: '1',
                            bordercolor: '#ccc'
                        },
                        styles: {
                            'border-collapse': 'collapse'
                        }
                    },
                    {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                    {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
                ],
                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    // '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    // '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] }
                ],
                removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
            };
            CKEDITOR.replace('email-body-template-editor-block', config);
            
            // Save button
            $('#templates_save_btn').on('click', function(){
                var editor_html = CKEDITOR.instances['email-body-template-editor-block'].getData();
                console.log(editor_html);
            });
            
            // Back button
            $('#my_templates_editor_block__back_btn').on('click', function(){
                $('#my_templates_editor_block').addClass('d-none');
                $('#my_templates_select_block').removeClass('d-none');
            });
            
            // Template select links
            $('.template-selector').on('click', function(event){
                event.preventDefault();
                // Получаем html выбранного шаблона в переменную html и встромляем в CKEditor
                var html = "<p>Пример HTML шаблона</p>";
                var editor = CKEDITOR.instances['email-body-template-editor-block'].setData(html);
                $('#my_templates_select_block').addClass('d-none');
                $('#my_templates_editor_block').removeClass('d-none');
            });
            
            // Hide modal event
            $('#m_modal_email_body_templates').on('hidden.bs.modal', function(){
                CKEDITOR.instances['email-body-template-editor-block'].setData('');
                $('#my_templates_editor_block').addClass('d-none');
                $('#my_templates_select_block').removeClass('d-none');
            });
        }
        
        function initEmailBodyDesignerModal() {
            
        }
        
        function initEmailBodyRawModal() {
            // Email body - template CK Editor
            var editor_height = (window.innerHeight - 400) < 380 ? 400 : window.innerHeight - 380;
            var config = {
                customConfig: '',
                allowedContent: true,
                disallowedContent: 'img{width,height,float}',
                extraAllowedContent: 'img[width,height,align]',
                extraPlugins: 'placeholder_edit',
                height: editor_height,
                contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css', baseUrl + '/admin/plugins/ckeditor-full/document-style.css'],
                bodyClass: 'document-editor',
                format_tags: 'p;h1;h2;h3;pre',
                removeDialogTabs: 'image:advanced;link:advanced',
                stylesSet: [
                    {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                    {name: 'Cited Work', element: 'cite'},
                    {name: 'Inline Quotation', element: 'q'},
                    {
                        name: 'Special Container',
                        element: 'div',
                        styles: {
                            padding: '5px 10px',
                            background: '#eee',
                            border: '1px solid #ccc'
                        }
                    },
                    {
                        name: 'Compact table',
                        element: 'table',
                        attributes: {
                            cellpadding: '5',
                            cellspacing: '0',
                            border: '1',
                            bordercolor: '#ccc'
                        },
                        styles: {
                            'border-collapse': 'collapse'
                        }
                    },
                    {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                    {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
                ],
                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    // '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    // '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] }
                ],
                removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
            };
            CKEDITOR.replace('email-body-raw-editor-block', config);
            
            // Modal raw html shown event
            $('#m_modal_email_body_raw_html').on('shown.bs.modal', function (e) {
                // Focus on editor
                CKEDITOR.instances['email-body-raw-editor-block'].focus();
                var modal = $(this);
                modal.find('button.save-raw-html').off('click').on('click',function () {
                    $('#delivery_email_wizard_form_step_2').find('input[name="EmailsDispatch[email_type]"]').val(0);
                    $('#delivery_email_wizard_form_step_2').find('input[name="EmailsDispatch[email]"]').val(CKEDITOR.instances['email-body-raw-editor-block'].getData());
                    modal.modal('hide');
                });
            });
        }

        function initSegmentsPreviewDatatable(){
            var checkboxes = $('#delivery_email_wizard_form_step_3 .segment_checkbox').get();
            var segments_contacts_datatable = null;

            var segments = [];
            
            $(checkboxes).on('change', function(){
                segments = [];
                $('#delivery_email_wizard_form_step_3 .segment_checkbox:checked').each(function () {
                    segments.push($(this).val());
                });
                actionsBeforeRefresh();
                if(segments_contacts_datatable === null){
                    initDatatable();
                } else {
                    segments_contacts_datatable.options.data.source.read.params.query.segments = segments;
                    refreshDatatable();
                }
            });
            
            function initDatatable(){
                var columns = [
                    {
                        field: 'Full_name',
                        title: GetTranslation('dess_email_contacts_field_title_full_name', 'Full name'),
                        sortable: false,
                        template: function (row, index, datatable) {
                            if(row.last_name == null)
                                row.last_name = '';
                            if(row.first_name == null)
                                row.first_name = '';
                            if(row.middle_name == null)
                                row.middle_name = '';

                            return '<a href="'+baseUrl + '/ita/' + tenantId + '/contacts/view?id='+row.id+'">'+row.first_name + ' ' + row.middle_name + ' ' + row.last_name  + '</a>';
                        }
                    },
                    {
                        field: 'Email_field',
                        title: GetTranslation('dess_email_contacts_field_title_email', 'Email'),
                        sortable: false,
                        template: function(row, index, datatable) {
                            var demo_email = row.emails[0];//'userpost@mail.com'; // TODO: CHANGE ON REAL!
                            return maskEmail(demo_email);
                        }
                    }
                ];
                segments_contacts_datatable = $('#segments_preview_datatable').mDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-segments-info',
                                method: 'GET',
                                params: {
                                    query: {
                                        segments:segments
                                        // generalSearch: ''
                                    },
                                    search: {
                                        // filter_responsible: userId
                                    }
                                    // custom query params
                                },
                                map: function (raw) {

                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                }
                            }
                        },
                        pageSize: 10,
                        saveState: {
                            cookie: false,
                            webstorage: false
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    layout: {
                        theme: 'default',
                        class: '',
                        scroll: false,
                        footer: false
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 400
                    },
                    rows: {
                        afterTemplate: function (row, data, index) {
                            
                        },
                        callback: function () {
                            
                        },
                        // auto hide columns, if rows overflow. work on non locked columns
                        autoHide: false
                    },
                    columns: columns,
                    toolbar: {
                        layout: ['pagination', 'info'],
                        placement: ['bottom'],  //'top', 'bottom'
                        items: {
                            pagination: {
                                type: 'default',
                                pages: {
                                    desktop: {
                                        layout: 'default',
                                        pagesNumber: 6
                                    },
                                    tablet: {
                                        layout: 'default',
                                        pagesNumber: 3
                                    },
                                    mobile: {
                                        layout: 'compact'
                                    }
                                },
                                navigation: {
                                    prev: true,
                                    next: true,
                                    first: true,
                                    last: true
                                },

                                pageSizeSelect: [10, 20, 30, 50, 100]
                            },
                            info: true
                        }
                    },
                    translate: {
                        records: {
                            processing: GetTranslation('datatable_data_processiong','Please wait...'),
                            noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                        },
                        toolbar: {
                            pagination: {
                                items: {
                                    default: {
                                        first: GetTranslation('pagination_first','First'),
                                        prev: GetTranslation('pagination_previous','Previous'),
                                        next: GetTranslation('pagination_next','Next'),
                                        last: GetTranslation('pagination_last','Last'),
                                        more: GetTranslation('pagination_more','More pages'),
                                        input: GetTranslation('pagination_page_number','Page number'),
                                        select: GetTranslation('pagination_page_size','Select page size')
                                    },
                                    info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                                }
                            }
                        }
                    }
                });
                
                /* DATATABLE EVENTS */
                
                // Init event
                $(segments_contacts_datatable).on('m-datatable--on-init', function(e){
                    $('#segment-contacts-search-block').removeClass('d-none');
                    actionsAfterRefresh();
                });
                // Layout updated event
                $(segments_contacts_datatable).on('m-datatable--on-layout-updated', function(e){
                    actionsAfterRefresh();
                });
                $(segments_contacts_datatable).on('m-datatable--on-ajax-done', function(e){
                    actionsAfterRefresh();
                });
            }
            
            function refreshDatatable(){
                // TODO: Make datatable source filtering
                segments_contacts_datatable.reload();   // Reload datatable with new data
            }
            
            function actionsBeforeRefresh(){
                $('#segments-contacts-search-input').prop('disabled', true);
                $('#segments-contacts-search-submit').prop('disabled', true);
                $('#segments-contacts-search-submit').addClass('disabled');
                $(checkboxes).each(function(){
                    $(this).prop('disabled', true);
                    $(this).closest('.m-checkbox').addClass('m-checkbox--disabled');
                });
            }
            
            function actionsAfterRefresh(){
                $('#segments-contacts-search-input').prop('disabled', false);
                $('#segments-contacts-search-submit').prop('disabled', false);
                $('#segments-contacts-search-submit').removeClass('disabled');
                $(checkboxes).each(function(){
                    $(this).prop('disabled', false);
                    $(this).closest('.m-checkbox').removeClass('m-checkbox--disabled');
                });
            }
            
            function maskEmail(email){
                var masked_email = '';
                var email_parts = email.split('@');
                if(email_parts.length === 2){
                    for(i in email_parts){
                        if(parseInt(i) === 0){
                            email_parts[i] = email_parts[i].substr(0, (email_parts[i].length - 1)) + '*' + email_parts[i].substr(email_parts[i].length);
                            email_parts[i] = email_parts[i].substr(0, (email_parts[i].length - 2)) + '*' + email_parts[i].substr(email_parts[i].length - 1);
                        }
                        masked_email += email_parts[i];
                        if((parseInt(i) + 1) !== email_parts.length){
                            masked_email += '@';
                        }
                    }
                } else {
                    console.log('Error! ' + email + ' - incorrect email format!');
                    return '';
                }
                return masked_email;
            }
        }
    });
})(jQuery);