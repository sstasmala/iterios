var dess_email_recievers_datatable = null;

(function($){
    $(document).ready(function(){
        initMessEmailDataTable();
        initClicksGraphic();
        initClicksWorldMap();

        function initMessEmailDataTable(){
            var columns = [
                {
                    field: 'col_data_1',
                    title: GetTranslation('dess_email_report_datatable_col_title_1', 'Name'),
                    textAlign: 'center',
                    sortable: true,
                    template: function(row, index, datatable) {
                        return row.name;
                    }
                },
                {
                    field: 'col_data_2',
                    title: GetTranslation('dess_email_report_datatable_col_title_2', 'Email'),
                    textAlign: 'center',
                    sortable: true,
                    template: function(row, index, datatable) {
                        return row.email;
                    }
                },
                {
                    field: 'col_data_3',
                    title: GetTranslation('dess_email_report_datatable_col_title_3', 'Contact segments'),
                    textAlign: 'center',
                    sortable: true,
                    template: function(row, index, datatable) {
                        return '<span class="text-nowrap mr-1">'+row.segments+'</span>';

                    }
                },
                {
                    field: 'col_data_4',
                    title: GetTranslation('dess_email_report_datatable_col_title_4', 'Status'),
                    textAlign: 'center',
                    sortable: true,
                    template: function(row, index, datatable) {
                        var data = {
                            status : row.status
                        };
                        var className;
                        switch (data.status){
                            case 'bounce':className='danger';
                            break;
                            case 'send':className='success';
                            break;
                            case 'delivered':className='primary';
                            break;
                        };

                        return '<span class="m-badge m-badge--'+className+' m-badge--dot"></span><span class="m--font-bold m--font-'+className+' ml-1">'+
                            GetTranslation(data.status +'_status', data.status) +
                            '</span>';
                    }
                }
            ];

            dess_email_recievers_datatable = $('#email-delivery-report-datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-users-info?id='+dispatch_id,
                            method: 'GET',
                            params: {
                                // _csrf: $('meta[name="csrf-token"]').attr('content'),
                                send_email_id: send_email_id,
                                query: {

                                },
                                // search: {
                                //     filter_responsible: userId
                                // }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {

                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
        }
        
        function initClicksGraphic(){
            if ($('#clicks_chartjs').length == 0) {
                return;
            }
            var ctx = document.getElementById("clicks_chartjs").getContext("2d");
            var clicks_gradient = ctx.createLinearGradient(0, 0, 0, 240);
                clicks_gradient.addColorStop(0, Chart.helpers.color(mApp.getColor('brand')).alpha(0.7).rgbString());
                clicks_gradient.addColorStop(1, Chart.helpers.color(mApp.getColor('brand')).alpha(0.2).rgbString());
            var opens_gradient = ctx.createLinearGradient(0, 0, 0, 240);
                opens_gradient.addColorStop(0, Chart.helpers.color(mApp.getColor('info')).alpha(0.3).rgbString());
                opens_gradient.addColorStop(1, Chart.helpers.color(mApp.getColor('info')).alpha(0.1).rgbString());
            var config = {
                type: 'line',
                data: {
                    labels: times,
                    datasets: [
                        {
                            label: GetTranslation('dess_email_view_chartjs_opens_label', 'Opens'),
                            backgroundColor: opens_gradient,
                            borderColor: mApp.getColor('info'),
                            pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointHoverBackgroundColor: mApp.getColor('info'),
                            pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                            //fill: 'start',
                            data: opens
                        },
                        {
                            label: GetTranslation('dess_email_view_chartjs_clicks_label', 'Clicks'),
                            backgroundColor: clicks_gradient,
                            borderColor: mApp.getColor('brand'),
                            pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointHoverBackgroundColor: mApp.getColor('brand'),
                            pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                            //fill: 'start',
                            data: clicks
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                // steps: 10,
                                stepSize: 10,
                                // max: 10,
                                beginAtZero: true   // minimum value will be 0.
                            }
                        }]
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    elements: {
                        line: {
                            tension: 0.01
                        },
                        point: {
                            radius: 4,
                            borderWidth: 12
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 10,
                            bottom: 0
                        }
                    }
                }
            };
            var chart = new Chart(ctx, config);
        }
        
        function initClicksWorldMap(){
            var sample_data = {
                "us": "1",
            };

            var data = {
                map: 'world_en',
                backgroundColor: null,
                color: '#f2f1ff',
                hoverOpacity: 0.7,
                selectedColor: '#34bfa3',
                enableZoom: true,
                showTooltip: true,
                values: locations,
                scaleColors: ['#f2f1ff', '#5f57c3'],
                normalizeFunction: 'polynomial',
                onRegionOver: function(event, code) {

                },
                onRegionClick: function(element, code, region) {
                    //sample to interact with map
                    var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                    console.log(message);
                }
            };
            var map = jQuery('#m_jqvmap_world');
            map.width(map.parent().width());
            map.vectorMap(data);
        }
    });
})(jQuery);
