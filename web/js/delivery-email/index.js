var dess_email_datatable = null;

(function($){
    $(document).ready(function(){
        initMessEmailDataTable();
        
        function initMessEmailDataTable(){
            // Responsive columns default sizes
//            var col_sizes = {
//                statistic: 280
//            };
//            // Small
//            if(window.innerWidth >= 540){
//                col_sizes.statistic = 280;
//            }
//            // Medium
//            if(window.innerWidth >= 720){
//                col_sizes.statistic = 300;
//            }
//            // Lage
//            if(window.innerWidth >= 960){
//                col_sizes.statistic = 400;
//            }
//            // Extra Lage
//            if(window.innerWidth >= 1140){
//                col_sizes.statistic = 400;
//            }
//            // Super Lage
//            if(window.innerWidth >= 1500){
//                col_sizes.statistic = 550;
//            }
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'name',
                    title: GetTranslation('ds_email_datatable_col_name_title', 'Name'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        var delivery_name = row.name;
                        return '<a href="'+baseUrl + '/ita/' + tenantId + '/delivery-email/view?id='+row.id+'" class="m-link m--font-bold">'+delivery_name+'</a>';
                    }
                },
                {
                    field: 'date',
                    title: GetTranslation('ds_email_datatable_col_date_title', 'Date'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        var date = row.date;
                        return '<span>'+ date +'</span>';
                    }
                },
                {
                    field: 'status',
                    title: GetTranslation('ds_email_datatable_col_status_title', 'Status'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        var status = row.status;  // Отправлено, Черновик, Доставлено, Ошибка
                        var color = 'success';// success|danger|warning
                        var info = '';
                        var errors = '';
                        for(X in row.errors_data)
                        {
                            if(typeof row.errors_data[X] === 'string')
                                errors += row.errors_data[X]+'\n';
                            else
                                errors += JSON.stringify(row.errors_data[X]).replace(/[{"}]/g,'')+'\n';
                        }

                        switch (row.status) {
                            case 0: status = 'В очереди';color = 'default';break;
                            case 1: status = 'Отправка';color = 'info';break;
                            case 2: status = 'Отправлено';color = 'success'; break;
                            case 3: status = 'Ошибка';color = 'danger'; info = '<i class="far fa-question-circle status-hint-icon m--font-danger ml-1" data-toggle="m-popover" data-content="'+errors+'"></i>';break;
                        }

                        return '<div class="text-nowrap" >\
                                    <span class="m-badge m-badge--' + color + ' m-badge--dot"></span>\
                                    <span class="m--font-bold m--font-' + color + ' ml-1">' + status + '</span>\
                                '+info+'</div>';
                    }
                },
                {
                    field: 'receivers',
                    title: GetTranslation('ds_email_datatable_col_receivers_title', 'Receivers'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        var receivers_count = row.receivers;
                        return '<span class="m--font-bold">' + receivers_count + '</span>';
                    }
                },
                {
                    field: 'opened',
                    title: GetTranslation('ds_email_datatable_col_opened_title', 'Opened'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        var opened_count = row.opens;
                        return '<span class="m--font-bold m--font-success">' + opened_count + '</span>';
                    }
                },
                {
                    field: 'folowed',
                    title: GetTranslation('ds_email_datatable_col_folowed_title', 'Folowed'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        var folowed_count = row.clicks;
                        return '<span class="m--font-bold m--font-success">' + folowed_count + '</span>';
                    }
                },
                {
                    field: 'errors',
                    title: GetTranslation('ds_email_datatable_col_errors_title', 'Errors'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        var errors_count = row.errors;
                        return '<span class="m--font-bold m--font-danger">' + errors_count + '</span>';
                    }
                },
                {
                    field: 'responsible',
                    title: GetTranslation('ds_email_datatable_col_responsible_title', 'Responsible'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        var photo;
                        photo = row.owner.photo;//'/img/profile_default.png';
                        name = row.owner.first_name+' '+row.owner.last_name;//'Somebody name';
                        return  '<div class="m-card-user m-card-user--sm">\
                                    <div class="m-card-user__pic">\
                                        <img src="/'+ photo +'" class="m--img-rounded m--marginless m--img-centered" alt="'+ name + '" title="'+ name + '">\
                                    </div>\
                                </div>';
                    }
                }
            ];
            
            dess_email_datatable = $('#dess_email_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/delivery-email/get-index',
                            // url: baseUrl + '/ita/' + tenantId + '/tasks/get-data',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {

                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(dess_email_datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(dess_email_datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = dess_email_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_dess_email_buttons').css({display : "flex"});
                }else{
                    $('#bulk_dess_email_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-mess-email').on('click', function(){
                var selected_records = dess_email_datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('dess_email_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('dess_email_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/delivery-emal/del-index',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                dess_email_datatable.reload();

                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
    });
})(jQuery);