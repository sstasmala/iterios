function initPageWidgets(){
    $('#transactions_list').empty().append('<div class="m-loader m-loader--brand" style="width: 100%;text-align: center;"></div>');
    $('#orders_body').empty().append('<tr><td colspan="6"><div class="m-loader m-loader--brand" style="width: 100%;height: 30px;padding:25px 0;"></div></td></tr>');
    $('#transactions_pagination').empty();

    // Add payment sum input mask
    $('#payment_summ_input').inputmask({
        regex: '^-?\\d+(\\.\\d{1,2})?$'
    });

    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/billing/get-account-info',
        method: 'GET',
        success: function (response) {
            $('#tenant_balance').text(DisplayLocaleString(parseFloat(response.balance),'USD'));
        }
    });

    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/billing/load-orders',
        method: 'GET',
        success: function (response) {
            $('#orders_body').empty();
            let html = '';
            for(let X in response) {
                html = '';
                html += ' <tr>' +
                    '<td>' +
                    // '<label class="m-checkbox m-checkbox--solid m-checkbox--single m-checkbox--brand">' +
                    // '<input type="checkbox">' +
                    '<span>'+response[X].id+'</span>' +
                    // '</label>' +
                    '</td>';
                html += ' <td>' +
                    '<span class="m-widget11__title">' +
                    response[X].name +
                    '</span>' +
                    '<span class="m-widget11__sub">' +
                    '<span class="moment-js invisible" data-format="unix">'+response[X].created_at+'</span>' +
                    '</span>' +
                    '</td>';
                switch (response[X].status) {
                    case 0: html += '<td>' +
                        '<span class="m-badge m-badge--default m-badge--dot mr-1"></span>' +
                        '<span class="m--font-bold m--font-default">Not active</span>' +
                        '</td>';break;
                    case 1: html += '<td>' +
                        '<span class="m-badge m-badge--success m-badge--dot mr-1"></span>' +
                        '<span class="m--font-bold m--font-success">Active</span>' +
                        '</td>';break;
                    case 2: html += '<td>' +
                        '<span class="m-badge m-badge--warning m-badge--dot mr-1"></span>' +
                        '<span class="m--font-bold m--font-warning">Suspended</span>' +
                        '</td>';break;
                }

                html += ' <td>' +
                    response[X].actual_date +
                    '</td>';
                html += '<td>' +
                    response[X].days_left +
                    '</td>';
                html += '<td class="m--font-brand">' +
                    DisplayLocaleString(parseFloat(response[X].price),'USD') +
                    '</td>';
                $('#orders_body').append(html);
            }
            updateDates();
        }
    });
}

function getTransactions(page) {
    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/billing/get-transactions',
        method: 'GET',
        data: {
            page:page,
            offset:5,
        },
        success:function (response) {
            $('#transactions_list').empty();
            let html = '';
            for(let I in response.data) {
                console.log(response.data[I]);
                html = '';
                switch (response.data[I].action) {
                    case 0:
                    case 2:html += '<div class="m-widget4__item">' +
                        '<div class="m-widget4__ext">' +
                        '<a href="#" class="m-widget4__icon m--font-brand">' +
                        '<i class="fa fa-cart-plus"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="m-widget4__info">' +
                        '<span class="m-widget4__text">' +
                        'Пополнение счета' +
                        '</span>' +
                        '<span class="moment-js invisible" data-format="unix">1526905382</span>' +
                        '</div>' +
                        '<div class="m-widget4__ext text-nowrap">' +
                        '<span class="m-widget4__number m--font-success">' +
                        '+' +  DisplayLocaleString(parseFloat(response.data[I].sum),'USD') +
                        '</span>' +
                        '</div>' +
                        '</div>';break;
                    case 1: html += '<div class="m-widget4__item">' +
                        '<div class="m-widget4__ext">' +
                        '<a href="#" class="m-widget4__icon m--font-brand">' +
                        '<i class="fa fa-coins"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="m-widget4__info">' +
                        '<span class="m-widget4__text">';
                        if(response.data[I].tariff_order_id == undefined || response.data[I].tariff_order_id == null)
                            html += ' Возврат денег со счета ' ;
                        else
                            html += ' Списание со счета для заказа <b>#'+response.data[I].tariff_order_id+'</b>' ;
                        html += '</span>' +
                        '<span class="moment-js invisible" data-format="unix">'+response.data[I].updated_at+'</span>' +
                        '</div>' +
                        '<div class="m-widget4__ext text-nowrap">' +
                        '<span class="m-widget4__number m--font-danger">' +
                        '-' + DisplayLocaleString(parseFloat(response.data[I].sum),'USD') +
                        '</span>' +
                        '</div>' +
                        '</div>';break;
                }


                $('#transactions_list').append(html);
            }

            $('#transactions_pagination').empty();
            if(response.meta.page>1)
            {
                html = '<li>' +
                    '<a title="' +
                    GetTranslation('pagination_first', tenant_lang) +
                    '" class="m-datatable__pager-link m-datatable__pager-link--first" data-page="1">' +
                    '<i class="la la-angle-double-left"></i>' +
                    '</a>' +
                    '</li>' +
                    '<li>' +
                    '<a title="' +
                    GetTranslation('pagination_previous', tenant_lang) +
                    '" class="m-datatable__pager-link m-datatable__pager-link--prev " data-page="'+(response.meta.page-1)+'"> ' +
                    '<i class="la la-angle-left"></i>' +
                    '</a>' +
                    '</li>';
                $('#transactions_pagination').append(html);
            }
            let total = Math.ceil(response.meta.total/response.meta.offset);
            console.log(total);
            if(total>1)
            for(let i = 1;i<=total;i++){
                html = '<li>' +
                    '<a class="m-datatable__pager-link m-datatable__pager-link-number '
                    +((i==response.meta.page)?'m-datatable__pager-link--active':'')
                    +'" data-page="'+i+'" title="2">'+i+'</a>' +
                    '</li>';
                $('#transactions_pagination').append(html);
            }
            if(response.meta.page<total)
            {
                html = '<li>' +
                    '<a title="' +
                    GetTranslation('pagination_next', tenant_lang) +
                    '" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="'+(response.meta.page+1)+'">' +
                    '<i class="la la-angle-right"></i>' +
                    '</a>' +
                    '</li>' +
                    '<li>' +
                    '<a title="' +
                    GetTranslation('pagination_last', tenant_lang) +
                    '" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="'+total+'"> ' +
                    '<i class="la la-angle-double-right"></i>' +
                    '</a>' +
                    '</li>';
                $('#transactions_pagination').append(html);
            }
            $('#transactions_pagination').find('a').off('click').on('click',function () {
                getTransactions($(this).data('page'));
            });
            updateDates();
        }
    });
}

function initPayButton(){
    var pay_btn = $('#pay_btn').get()[0];
    var payment_method_radios = $('input[name="payment_method"]').get();
    var payment_summ_input = $('#payment_summ_input').get()[0];
    
    $(payment_method_radios).on('change', function(){
        checkPayButtonDisable();
    });
    
    $(payment_summ_input).on('keyup', function(){
        checkPayButtonDisable();
    });
    
    $(pay_btn).on('click', function(){
        var payment_method = getPaymentMethod();
        $('#modal_payment_' + payment_method).modal('show');
    });
    
    function getPaymentMethod(){
        var payment_method = null;
        $(payment_method_radios).each(function(){
            var radio = this;
            if(radio.checked){
                payment_method = $(radio).val();
            }
        });
        return payment_method;
    }
    
    function getSumValue(){
        var sum = $(payment_summ_input).val();
        return sum;
    }
    
    function checkPayButtonDisable(){
        var payment_method = getPaymentMethod();
        var sum = getSumValue();
        
        if( payment_method !== undefined &&
            payment_method !== null &&
            payment_method !== '' &&
            sum !== undefined &&
            sum !== null &&
            sum !== '') {
                pay_btn.disabled = false;
            } else {
                pay_btn.disabled = true;
            }
    }
}

function updateDates() {
    $('.moment-js').each(function(){
        var moment_block = this;
        var is_already_inited = $(moment_block).data("moment-js-inited");
        var format = $(moment_block).data('format');
        var moment_date = $(moment_block).text();
        var moment_text;
        var moment_tip;

        if(!is_already_inited){
            moment.locale(tenant_lang);
            if(format === 'unix'){
                moment_text = moment(parseInt(moment_date * 1000)).fromNow();
                moment_tip = moment(parseInt(moment_date * 1000)).format(date_format+' '+time_format);
            }else{
                moment_text = moment(moment_date, format).fromNow();
                moment_tip = moment(moment_date, format).format(date_format+' '+time_format);
            }
            $(moment_block).text(moment_text);
            $(moment_block).popover({
                content: moment_tip,
                placement: 'top',
                trigger: 'hover'
            });
            $(moment_block).removeClass("invisible");
            $(moment_block).data("moment-js-inited", true);
            moment.locale('en'); // default the locale to English
        }
    });
}

function initCardPayment(){
    var modal = $('#modal_payment_card').get()[0];
    $(modal).on('shown.bs.modal', function(){
        console.log(modal);
    });
}

function initBillPayment(){
    var modal = $('#modal_payment_bill').get()[0];
    $(modal).on('shown.bs.modal', function(){
        console.log(modal);
    });
}

function initInvoicePayment(){
    var modal = $('#modal_payment_invoice').get()[0];
    $(modal).on('shown.bs.modal', function(){
        console.log(modal);
    });
}

$(document).ready(function(){
    initPageWidgets();
    getTransactions(1);
    initPayButton();
    initCardPayment();
    initBillPayment();
    initInvoicePayment();
});