/**
 * timeline.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

(function($) {
    $(document).ready(function () {
        add_note();
        edit_note();
        delete_note();
        load_more();
        search();
        add_task();

        if ($('#history_tab_activity').find('.m-timeline-1__items .m-timeline-1__item').length === 0)
            $('#search-input').attr('disabled', 'disabled');

        // fix summer note
        $('.note-editable').on('click', function(){
            $('.note-toolbar-wrapper').css('height', '50px');
        });

        $('#contact-edit-note-popup').on('show.bs.modal', function (e) {
            var init = $('#edit-note-from_timeline-redactor').data('init');

            if(event.relatedTarget !== undefined && !init) {
                $('#edit-note-from_timeline-redactor').summernote({height: 150});
                $('#edit-note-from_timeline-redactor').data('init', true);
            }
        });

        function add_task() {
            var btn = $('button[form="add-task-from_timeline_form"]');
            var form = $('#add-task-from_timeline_form');

            $(btn).click(function(e) {
                form.validate({
                    rules: {
                        'Tasks[title]': {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    method: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/contacts/add-task?contact_id=' + form.data('contact_id'),
                    success: function (response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if ($('#history_tab_activity').find('.m-timeline-1__items .m-timeline-1__item').length > 0) {
                                $('#history_tab_activity .m-timeline-1__items .m-timeline-1__item--first').removeClass('m-timeline-1__item--first');
                                $('#history_tab_activity .m-timeline-1__items .m-timeline-1__marker').after(response);
                            } else {
                                var template = '<div class="m-timeline-1 m-timeline-1--fixed ita-custom">' +
                                    '<div class="m-timeline-1__items">' +
                                    '<div class="m-timeline-1__marker"></div>' + response + '</div></div>';

                                $('#history_tab_activity').append(template);
                            }

                            if ($('#history_tab_tasks').find('.m-timeline-1__items .m-timeline-1__item').length > 0) {
                                $('#history_tab_tasks .m-timeline-1__items .m-timeline-1__item--first').removeClass('m-timeline-1__item--first');
                                $('#history_tab_tasks .m-timeline-1__items .m-timeline-1__marker').after(response);
                            } else {
                                var template = '<div class="m-timeline-1 m-timeline-1--fixed ita-custom">' +
                                    '<div class="m-timeline-1__items">' +
                                    '<div class="m-timeline-1__marker"></div>' + response + '</div></div>';

                                $('#history_tab_tasks').append(template);
                            }

                            $('#history_tab_activity .m-timeline-1__items').show();
                            $('#no_result').remove();
                            $('#search-input').attr('disabled', false);

                            initMomentJS();

                            $('#create_task_input__title, #timeline-create_task_note_textarea').val('');
                            $('.task_due_date_timestamp, .email_reminder_timestamp').val('');
                            $('#timeline-create_task_type, #timeline-create_task_assigned_to_id_select').val(null).trigger('change');
                            $('#timeline_new_task_due_date, #timeline_new_task_due_time, #timeline_new_task_email_reminder_date, #timeline_new_task_email_reminder_time').val(null);

                            var content = {
                                title: GetTranslation('success_message', 'Success!'),
                                message: ''
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1031,
                                type: "success"
                            });
                        }, 1000);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }

        function add_note() {
            var btn = $('button[form="add-note-from_timeline_form"]');
            var form = $('#add-note-from_timeline_form');

            $(btn).click(function(e) {
                form.validate({
                    rules: {
                        value: {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    method: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/contacts/add-note?contact_id=' + form.data('contact_id'),
                    success: function (response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if ($('#history_tab_activity').find('.m-timeline-1__items .m-timeline-1__item').length > 0) {
                                $('#history_tab_activity .m-timeline-1__items .m-timeline-1__item--first').removeClass('m-timeline-1__item--first');
                                $('#history_tab_activity .m-timeline-1__items .m-timeline-1__marker').after(response);
                            } else {
                                var template = '<div class="m-timeline-1 m-timeline-1--fixed ita-custom">' +
                                    '<div class="m-timeline-1__items">' +
                                    '<div class="m-timeline-1__marker"></div>' + response + '</div></div>';

                                $('#history_tab_activity').append(template);
                            }

                            if ($('#history_tab_notes').find('.m-timeline-1__items .m-timeline-1__item').length > 0) {
                                $('#history_tab_notes .m-timeline-1__items .m-timeline-1__item--first').removeClass('m-timeline-1__item--first');
                                $('#history_tab_notes .m-timeline-1__items .m-timeline-1__marker').after(response);
                            } else {
                                var template = '<div class="m-timeline-1 m-timeline-1--fixed ita-custom">' +
                                    '<div class="m-timeline-1__items">' +
                                    '<div class="m-timeline-1__marker"></div>' + response + '</div></div>';

                                $('#history_tab_notes').append(template);
                            }

                            $('#history_tab_activity .m-timeline-1__items').show();
                            $('#no_result').remove();
                            $('#search-input').attr('disabled', false);

                            $('#add-note-from_timeline-redactor').summernote('code', '');

                            initMomentJS();

                            var content = {
                                title: GetTranslation('success_message', 'Success!'),
                                message: ''
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1031,
                                type: "success"
                            });
                        }, 1000);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }

        function edit_note() {
            var id;
            var type;

            $('body').on('click', '.edit_btn', function(e) {
                id = $(this).data('id');
                type = $(this).data('type');

                $('.note-toolbar-wrapper').css('height', '50px');

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-note?id=' + id,
                    method: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        type: type
                    },
                    success: function (response) {
                        $('#edit-note-from_timeline-redactor').summernote('code', response.value);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });

            $('#edit-note-save-button').click(function (e) {
                e.preventDefault();

                var btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $('#edit-note-from_timeline_form').ajaxSubmit({
                    method: 'POST',
                    data: {
                        type: type
                    },
                    url: baseUrl + '/ita/' + tenantId + '/contacts/edit-note?id=' + id,
                    success: function (response, status, xhr, $form) {
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            $('#contact-edit-note-popup').modal('hide');

                            if (response.id) {
                                $('#history_tab_notes .m-timeline-1__item[data-id="' + id + '"]').find('.m-timeline-1__item-body').empty().html(response.value);
                                $('#history_tab_activity .m-timeline-1__item[data-id="' + id + '"]').find('.m-timeline-1__item-body').empty().html(response.value);

                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                            }
                        }, 1000);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }

        function delete_note() {
            $('body').on('click', '.remove_btn', function(e) {
                var id = $(this).data('id');
                var type = $(this).data('type');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/contacts/delete-note?id=' + id,
                            method: 'POST',
                            data: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                type: type
                            },
                            success: function (response) {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });

                                if ($('#history_tab_notes .m-timeline-1__item[data-id="'+id+'"]').hasClass('m-timeline-1__item--first')) {
                                    var history_elem = $('#history_tab_notes .m-timeline-1__items .m-timeline-1__item').get(1);
                                    $(history_elem).addClass('m-timeline-1__item--first');
                                }

                                $('#history_tab_notes .m-timeline-1__item[data-id="'+id+'"]').remove();

                                if ($('#history_tab_activity .m-timeline-1__item[data-id="'+id+'"]').hasClass('m-timeline-1__item--first')) {
                                    var note_elem = $('#history_tab_activity .m-timeline-1__items .m-timeline-1__item').get(1);
                                    $(note_elem).addClass('m-timeline-1__item--first');
                                }

                                $('#history_tab_activity .m-timeline-1__item[data-id="'+id+'"]').remove();
                            },
                            error: function (response) {
                                var content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {

                    }
                });
            });
        }

        function load_more() {
            $('.load-more').click(function (e) {
                var id = $(this).data('cid');
                var type = $(this).data('type');

                var btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                var offset = btn.closest('.tab-pane').find('.m-timeline-1__items .m-timeline-1__item').length;

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/contacts/time-line-load-more?id=' + id,
                    method: 'GET',
                    data: {
                        type: type,
                        offset: offset,
                        search: $('#search-input').val()
                    },
                    success: function (response) {
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response) {
                                btn.closest('.tab-pane').find('.m-timeline-1__items').append(response);

                                initMomentJS();
                            }

                            if (btn.closest('.tab-pane').find('.m-timeline-1__items .m-timeline-1__item').length % 10 !== 0 || response.length === 0)
                                btn.attr('disabled', true);
                        }, 1000);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }
        
        function search() {
            $('#search-input').on('input', function (e) {
                if ($(this).val().length > 0) {
                    if ($('#search-btn').hasClass('btn-metal'))
                        $('#search-btn').removeClass('btn-metal').addClass('btn-brand');
                } else {
                    $('#search-btn').removeClass('btn-brand').addClass('btn-metal');
                }
            });

            $('#search-btn').on('click', function (e) {
                var id = $(this).data('cid');

                var btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                var due_date_val = $('#timeline-task-create-block').find('.task_input__due_date').val();
                var due_time_val = $('#timeline-task-create-block').find('.task_input__due_time').val();
                var reminder_date_val = $('#timeline-task-create-block').find('.task_input__email_reminder_date').val();
                var reminder_time_val = $('#timeline-task-create-block').find('.task_input__email_reminder_time').val();
                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                if(!due_date_val){
                    $('#timeline-task-create-block').find('.task_input__due_date').datepicker('setDate', tomorrow);
                }
                if(!due_time_val){
                    $('#timeline-task-create-block').find('.task_input__due_time').timepicker('setTime', '09:00');
                }
                if(reminder_date_val && !reminder_time_val){
                    $('#timeline-task-create-block').find('.task_input__email_reminder_time').timepicker('setTime', '09:00');
                }
                if(!reminder_date_val && reminder_time_val){
                    $('#timeline-task-create-block').find('.task_input__email_reminder_date').datepicker('setDate', due_date_val);
                }

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/contacts/time-line-search?id=' + id,
                    method: 'GET',
                    data: {
                        search: $('#search-input').val()
                    },
                    success: function (response) {
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response) {
                                var template = '<div class="m-timeline-1__marker"></div>' + response;

                                $('#no_result').remove();

                                $('#history_tab_activity .m-timeline-1__items').fadeOut(500, function() {
                                    $('#history_tab_activity .m-timeline-1__items').empty().append(template).fadeIn();

                                    if ($('#history_tab_activity').find('.m-timeline-1__items .m-timeline-1__item').length % 10 !== 0 || response.length === 0)
                                        $('#history_tab_activity .load-more').closest('.row').hide();
                                });

                                initMomentJS();
                            } else {
                                var no_result = '<div class="row" id="no_result"><div class="col-12" style="text-align: center;">' +
                                    '<span>' + GetTranslation('no_result') + '</span></div></div>';

                                $('#no_result').remove();

                                $('#history_tab_activity .m-timeline-1__items').fadeOut(500, function() {
                                    $('#history_tab_activity .m-timeline-1__items').empty();
                                    $('#history_tab_activity .m-timeline-1').before(no_result);
                                    $('#history_tab_activity .load-more').closest('.row').hide();
                                });
                            }
                        }, 1000);
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }
    });
})(jQuery);