(function($){
    $(document).ready(function(){
        initTagDataTable();
        initAddTagDropdownForm();
        
        function initTagDataTable(){
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/ajax/get-tags-list',
                method: 'POST',
                data: {
                    search: 'default'
                },
                dataType: 'json',
                success: function (response){
                    var dataJSONArray = response;
                    var datatable = $('#m_datatable-tags').mDatatable({
                        data: {
                            type: 'local',
                            source: dataJSONArray,
                            pageSize: 10
                        },
                        layout: {
                            theme: 'default',
                            class: 'tags-data-table-wrap',
                            scroll: false,
                            footer: false
                        },
                        rows: {
                            afterTemplate: function (row, data, index) {
                                /* Init delete button functional */
                                $(row).find('.delete-tag-button').each(function(){
                                    var delete_button = this;
                                    $(delete_button).on('click', function(){
                                        swal({
                                            title: GetTranslation('delete_question', 'Are you sure?'),
                                            text: GetTranslation('tag_delete_question', 'Delete this tag?'),
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                                            cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                                            reverseButtons: true
                                        }).then(function (result) {
                                            if(result.value === true){
                                                $.ajax({
                                                    url: baseUrl + '/ita/' + tenantId + '/ajax/delete-tag',
                                                    data: {
                                                        tag_id: data.id
                                                    },
                                                    method: 'POST',
                                                    success: function (response){
                                                        if (response.message) {
                                                            toastr.options = {
                                                                "closeButton": false,
                                                                "debug": false,
                                                                "newestOnTop": false,
                                                                "progressBar": false,
                                                                "positionClass": "toast-top-right",
                                                                "preventDuplicates": false,
                                                                "onclick": null,
                                                                "showDuration": "300",
                                                                "hideDuration": "1000",
                                                                "timeOut": "5000",
                                                                "extendedTimeOut": "1000",
                                                                "showEasing": "swing",
                                                                "hideEasing": "linear",
                                                                "showMethod": "fadeIn",
                                                                "hideMethod": "fadeOut"
                                                            };
                                                            if(response.result === 'success'){
                                                                /* Delete row from table and table source array */
                                                                toastr.success(response.message);
                                                                var datatable = $('#m_datatable-tags').mDatatable();
                                                                var source = datatable.options.data.source;
                                                                for (var key in source) {
                                                                    if(data.id === source[key].id){
                                                                        source.splice(key, 1);
                                                                    }
                                                                }
                                                                /* Reload after row delete */
                                                                $(row).slideUp(400, function(){
                                                                    datatable.reload();
                                                                });
                                                            }
                                                            if(response.result === 'error'){
                                                                toastr.error(response.message);
                                                            }
                                                        }
                                                    }
                                                });
                                            } else {
                                                return false;
                                            }
                                        });
                                    });
                                });

                                $(row).find('[data-toggle="m-tooltip"]').each(function() {
                                    mApp.initTooltip($(this));
                                });
                            }
                        },
                        sortable: true,
                        pagination: true,
                        search: false,
                        translate: {
                            records: {
                                processing: GetTranslation('datatable_data_processiong','Please wait...'),
                                noRecords: GetTranslation('datatable_records_not_found','No records found')
                            },
                            toolbar: {
                                pagination: {
                                    items: {
                                        default: {
                                            first: GetTranslation('pagination_first','First'),
                                            prev: GetTranslation('pagination_previous','Previous'),
                                            next: GetTranslation('pagination_next','Next'),
                                            last: GetTranslation('pagination_last','Last'),
                                            more: GetTranslation('pagination_more','More pages'),
                                            input: GetTranslation('pagination_page_number','Page number'),
                                            select: GetTranslation('pagination_page_size','Select page size')
                                        },
                                        info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                                    }
                                }
                            }
                        },
                        columns: [
                            {
                                field: "name",
                                title: GetTranslation('tags_table_name_title', "Name"),
                                overflow: 'visible',
                                template: function (row, index, datatable) {
                                    /* Show last 2 row with upper style dropdown */
                                    var page = datatable.API.params.pagination.page;
                                    var pages = datatable.API.params.pagination.pages;
                                    var perpage = datatable.API.params.pagination.perpage;
                                    var total = datatable.API.params.pagination.total;
                                    var items_count = perpage;
                                    if(page === pages){
                                        //Last page...
                                        var items_count = total - ((page*perpage)-perpage);
                                    }
                                    var is_upper_drop_style = false;
                                    var item_order = index+1;
                                    if(item_order > (items_count-2)){
                                        is_upper_drop_style = true;
                                    }
                                    /* Render row */
                                    return '<div class="m-dropdown ' + ((is_upper_drop_style) ? 'm-dropdown--up ' : '') + 'etitable-form-dropdown m-dropdown--arrow m-dropdown--large"'+(row.type != 'system' ? ' data-dropdown-toggle="click" data-dropdown-persistent="true"' : '')+'>\
                                                <a href="#" class="m-dropdown__toggle" style="'+(row.type == 'system' ? 'color: gray; pointer-events: none;' : 'text-decoration:underline; text-decoration-style:dotted;')+'">' + row.name + '</a>\
                                                '+ (row.type == 'system' ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-tooltip" data-placement="top" title="' + GetTranslation('system_tag_tooltip', 'System tag, edit disabled') + '"></i>' : '') + '\
                                                <div class="m-dropdown__wrapper">\
                                                    '+ ((is_upper_drop_style) ? '' : '<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>') + '\
                                                    <div class="m-dropdown__inner">\
                                                        <div class="m-dropdown__body">\
                                                            <div class="m-dropdown__content">\
                                                                <form class="etitable-form">\
                                                                    <div class="row align-items-center">\
                                                                        <div class="col-8 inputs">\
                                                                            <input name="ajax_url" type="hidden" value="/ajax/update-tag">\
                                                                            <input name="id" type="hidden" value="' + row.id + '">\
                                                                            <input name="property" type="hidden" value="name" required>\
                                                                            <input name="value" class="form-control m-input" type="text" placeholder="' + GetTranslation('tags_table_tag_name_placeholder', "Tag name") + '" value="' + row.name + '" required>\
                                                                        </div>\
                                                                        <div class="col-4 buttons text-right">\
                                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                                        </div>\
                                                                    </div>\
                                                                </form>\
                                                                <div class="preloader" style="display:none;text-align:center;">\
                                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    '+ ((is_upper_drop_style) ? '<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>' : '') + '\
                                                </div>\
                                            </div>';
                                }
                            },
                            {
                                field: "contact_uses",
                                title: GetTranslation('contacts_contacts', "Contacts"),
                                textAlign: 'center'
                            },
                            {
                                field: "company_uses",
                                title: GetTranslation('tags_table_сompanies_title', "Companies"),
                                textAlign: 'center'
                            },
                            {
                                field: "created_at",
                                title: GetTranslation('tags_table_created_at_title', "Created")
                            },
                            {
                                field: "actions",
                                title: GetTranslation('delete', "Delete"),
                                sortable: false,
                                textAlign: 'center',
                                overflow: 'visible',
                                template: function (row, index, datatable) {
                                    return  '<a href="#" class="'+(row.type == 'system' ? 'disabled ' : '')+'delete-tag-button m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', "Delete") + '">\
                                                <i class="la la-trash"></i>\
                                            </a>';
                                },
                            }
                        ]
                    });
                    /* Init editable inputs after table page was changed */
                    $(datatable).on('m-datatable--on-goto-page', function(){
                        initEditableForms('#m_datatable-tags');
                    });
                    /* Init editable inputs after table rendered */
                    initEditableForms('#m_datatable-tags');
                }
            });
        }
        
        function initAddTagDropdownForm(){
            initEditableForms('.add-tag-form-column', function(data){
                /* Success add tag */
                var datatable = $('#m_datatable-tags').mDatatable();
                datatable.destroy();
                initTagDataTable();
                $('.add-tag-form-column .etitable-form input[name="tag_name"]').val('');
            });
        }
        
    });
})(jQuery);