/** contacts/filters.js
 *
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

function search() {
    var search_text = $('input[name="quick-search"]').val();
    var filter_email = $('#search-dropdown input[name="email"]').val();
    var filter_phone = $('#search-dropdown input[name="phone"]').val();
    var filter_tags = $('#search-dropdown #tags_search').val();

    var option_count = 0;

    if (search_text.length > 0)
        option_count++;

    if (filter_email.length > 0)
        option_count++;

    if (filter_phone.length > 0)
        option_count++;

    if (filter_tags.length > 0)
        option_count++;

    filters_inactive(false);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (option_count > 0) {
        $('#search-panel #filters_indicator').text(option_count + ' ' + declOfNum(option_count, ['опция', 'опции', 'опций']));
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    datatable.options.data.source.read.params = {
        query: {
            // generalSearch: ''
        },
        search: {
            search_text: search_text,
            filter_email: filter_email,
            filter_phone: filter_phone,
            filter_tags: filter_tags
        }
    };
    datatable.reload();

    //setTimeout(function () {
    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
    //}, 1000);
}

function reset(reset_all, reload, filters) {
    var params = {
        query: {
            // generalSearch: ''
        },
        'search': {
        }
    };

    if (filters)
        filters_inactive(false);

    //if (!$('#search-panel #reset_search_input_button').is(':visible'))
        //$('#search-panel #reset_search_input_button').show();

    $('#search-panel #filters_indicator').parent().removeClass('show');

    if (reset_all)
        $('input[name="quick-search"]').val('');

    if (!reset_all)
        params.search.search_text = $('input[name="quick-search"]').val();

    $('#search-dropdown input[name="email"]').val('');
    $('#search-dropdown input[name="phone"]').val('');
    $('#tags_search').val(null).trigger('change');

    if (reload) {
        datatable.options.data.source.read.params = params;
        datatable.reload();
    }

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
}

function filters_inactive(filter_id) {
    $('.filter-item a').each(function (key, el) {
        if (!filter_id || filter_id != $(el).attr('id')) {
            if ($(el).hasClass('active'))
                $(el).removeClass('active');
        }
    });
}

$('#apply_filter').on('click', function () {
    search();
});

$('input#quick-search-input').keyup(function(event) {
    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (event.keyCode == 13 && $('input[name="quick-search"]').val().length > 0) {
        search();
        $(this).blur();

        if ($('input[name="quick-search"]').val().length > 0) {
            $('#search-panel #filters_indicator').text('1 опция');
            $('#search-panel #filters_indicator').parent().addClass('show');
        } else {
            $('#search-panel #filters_indicator').parent().removeClass('show');
        }
    }
});

$('#reset_filter').on('click', function () {
    reset(false, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible') && $('input[name="quick-search"]').val().length == 0) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('#reset_search_input_button').on('click', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('body').on('click', 'a#show_all_link', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('.filter-item').on('click', function () {
    var filter = ($(this).find('a').hasClass('active') ? '' : $(this).find('a').attr('id'));

    reset(true, false, false);
    $(this).find('a').toggleClass('active');
    filters_inactive(filter);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter.length > 0) {
        $('#search-panel #filters_indicator').text('1 опция');
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    datatable.options.data.source.read.params = {
        query: {
            // generalSearch: ''
        },
        'search': {
            'filter': filter
        }
    };
    datatable.reload();

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
});

$('input#quick-search-input').on('click', function () {
    var filter_tags = $('#search-dropdown #tags_search').val();

    if (filter_tags.length == 0)
        $('#tags_search').val(null).trigger('change');
});

$(document).ready(function () {
    $('#tags_search').select2({
        ajax: {
            url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'system'
                }
                return query;
            },
            delay: 1000,
            processResults: function (data)
            {
                var array = [];
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['name']});
                }
                return {
                    results: array
                }
            }
        },
        language: tenant_lang,
        placeholder: GetTranslation('modal_bulk_select_tags'),
        width: '100%',
        allowClear: true,
        dropdownParent: $('#search-dropdown')
    });

    datatable.on('m-datatable--on-reloaded', function (e) {
        datatable.on('m-datatable--on-layout-updated', function (e, arg) {
            $('#search-panel #summary_indicator').text(datatable.getTotalRows() + ' ' +
                declOfNum(datatable.getTotalRows(), ['запись', 'записи', 'записей']));

            if ($('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #summary_indicator').parent().show();
        });
    });
});