/**
 * datatable.js
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
function beautiful_phone(phone) {
    if (phone != null && phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
        var result = phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );

        return '+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4];
    }

    return (phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone);
}

//Columns settings
var settings = window.localStorage.getItem('contacts_settings');
var def_set = ['id','Full_name','contacts','type','tags'];

if(settings == null) {
    settings = def_set;
} else {
    settings = settings.split(',');
}

var additional_selected = [];
//Columns
var columns_set = [
    {
        field: "id",
        title: "#",
        width: 50,
        sortable: false,
        textAlign: 'center',
        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
    },
    {
        field: "Full_name",
        title: GetTranslation('contacts_field_title_full_name',"Full name"),
        width: 160,
        template: function (row) {
            if(row.last_name == null)
                row.last_name = '';
            if(row.first_name == null)
                row.first_name = '';
            if(row.middle_name == null)
                row.middle_name = '';

            return '<div class="full_name-wraper">\
                        <div class="content-data-block" data-contact-id="'+row.id+'">\
                            <a href="'+baseUrl + '/ita/' + tenantId + '/contacts/view?id='+row.id+'" data-container="#contacts_t .content-data-block[data-contact-id='+row.id+']" data-toggle="m-popover" data-placement="top" data-content="' + row.last_name + ' ' + row.first_name + ' ' + row.middle_name + '">\
                                ' + row.last_name + ' ' + row.first_name + ' ' + row.middle_name + '\
                            </a>\
                        </div>\
                        <div class="preview-link-block">\
                            <button type="button" class="btn btn-secondary btn-sm ml-3" data-toggle="modal" data-target="#contact_quick_edit_modal" data-contact-id="'+row.id+'">\
                                ' + GetTranslation('contacts_datatable_preview_btn', 'Preview') + '\
                            </button>\
                        </div>\
                    </div>';
        }
    },
    {
        field: "gender",
        title: GetTranslation('contacts_field_title_gender',"Gender"),
        template: function (row) {
            if(row.gender)
                return GetTranslation('gender_male');
            else
                return GetTranslation('gender_female');
        }
    },
    {
        field: "contacts",
        title: GetTranslation('contacts_field_title_contacts',"Contacts"),
        // sortable: 'asc',
        // filterable: false,
        width: 250,
        overflow: 'visible',
        template: function (row, index, datatable) {
            var contacts = [];
            if(row.phones != null && row.phones.length >0)
                contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(row.phones[0].value))+'</div>';
            if(row.emails != null &&  row.emails.length >0)
                contacts.push( '<div class="contact-row"><i class="fa fa-envelope-o"></i> '+row.emails[0].value)+'</div>';
            if(row.phones != null)
            row.phones.forEach(function (item,i) {
                if(i!=0)
                    contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(item.value))+'</div>';
            });
            if(row.emails != null)
            row.emails.forEach(function (item,i) {
                if(i!=0)
                    contacts.push( '<div class="contact-row"><i class="fa fa-envelope-o"></i> '+item.value)+'</div>';
            });
            var first = '';
            var additional = '';
            contacts.forEach(function (item,i) {
                if(i<2) {
                    if (i < 1)
                        item+='</div>';
                    first += '<div class="">'+item;
                }
                else
                    additional+='<li class="dropdown-item">'+item+'</li>';
            });

            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            var toggle = '<div class="dropdown ' + dropup + '">' +
                '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"> ' +
                '<i class="la la-ellipsis-h"></i>' +
                '</a>' +
                '<ul class="dropdown-menu additional-list">'
                +additional +
                '</ul>' +
                '</div>';
            if(additional != '')
                first += toggle;
            first += '</div>';
            return first;
        }
    },
    {
        field: "type",
        title: GetTranslation('contacts_field_title_type',"Type"),
        template:function (row, index,datatable) {
            var value ='';
            var style ='primary';
            switch (row.type)
            {
                case 'customer':style = 'primary';value = GetTranslation('contacts_types_customer');break;
                case 'tourist':style = 'success';value = GetTranslation('contacts_types_tourist');break;
                case 'lid':style = 'warning';value = GetTranslation('contacts_types_lid');break;
                case 'new':style = 'danger';value = GetTranslation('contacts_types_new');break;

            }
            return '<span class="m-badge m-badge--'+style+' m-badge--dot"></span><span class="m--font-bold m--font-'+style+'"> '+value+'</span>';
        }
    },
    {
        field: "date_of_birth",
        title: GetTranslation('contacts_field_title_birth_date',"Birth date")
    },
    {
        field: "Age",
        title: GetTranslation('contacts_field_title_age',"Age"),
        template: function (row) {
            if(row.date_of_birth==null)
                return null;
            var date = moment(row.date_of_birth,date_format);
            var curDate =moment();
            var diff =curDate.diff(date,'years');
            return diff;
        }
    },
    {
        field: "nationality",
        title: GetTranslation('contacts_field_nationality',"Nationality"),
    },
    {
        field: "company",
        
        title: GetTranslation('contacts_field_company',"Company"),
        template: function (row) {
            if(row.company!=null && row.company.name!=undefined)
                return '<a href="'+ baseUrl + '/ita/' + tenantId + '/companies?id'+ row.company.id+'">' + row.company.name + '</a>';
        }
    },
    {
        field: "tags",
        title: GetTranslation('contacts_field_tags',"Tags"),
        overflow: 'visible',
        template: function (row, index, datatable) {
            var tags = [];
            if(row.tags == null)
                return;
            row.tags.forEach(function (item,i) {
                tags.push(item.name);
            });
            var first = '';
            var additional = '';
            tags.forEach(function (item,i) {
                if(i<2)
                    if(i<1)
                        first+='<div><span class="m-badge m-badge--brand m-badge--wide mb-1 mr-1">'+item+'</span></div>';
                    else
                        first+='<div><span class="m-badge m-badge--brand m-badge--wide mb-1 mr-1">'+item+'</span>';
                else
                    additional+='<li class="dropdown-item"><span class="m-badge m-badge--brand m-badge--wide">'+item+'</span></li>';
            });
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

            var toggle =    '<div class="dropdown ' + dropup + '">\
				<a href="#" class="btn btn-sm m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill mb-1 mr-1" data-toggle="dropdown" style="width: 25px;height: 21px;">\
                                    <i class="la la-ellipsis-h"></i>\
                                </a>\
                                <ul class="dropdown-menu additional-list">'
                                    +additional+
                                '</ul>\
                            </div>';
            if(additional != '')
                first += toggle;
            first += '</div>';
            return first;
        }
    },
    {
        field: "created_at",
        title: GetTranslation('contacts_field_created',"Created"),
        template: function (row) {
            return row.created_at;//moment(row.created_at*1000).format('YYYY-MM-DD HH:mm:ss');
        }
    },
    {
        field: "updated_at",
        title: GetTranslation('contacts_field_updated',"Updated"),
        template: function (row) {
            return row.updated_at;//moment(row.updated_at*1000).format('YYYY-MM-DD HH:mm:ss');
        }
    },
    {
        field: "responsible",
        title: GetTranslation('responsible',"Responsible"),
        template: function (row) {
            var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
            if(row.responsible !== null && row.responsible.id !== undefined)
            {
                html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                if(row.responsible.photo !== null){
                    html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                } else {
                    html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                }
                html += '</a>';
            }
            return html;
        }
    },
    {
        field: "Actions",
        width: 70,
        title: GetTranslation('contacts_field_actions',"Actions"),
        sortable: false,
        overflow: 'visible',
        visible: false,
        textAlign: 'center',
        template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            var actions = '<button data-id="'+row.id+'" class="remove-contact delete-contact m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="la la-trash"></i></button>';
            return actions;
        }
    }
];
var columns = [];

columns_set.forEach(function (item, i, array) {
    if(settings.indexOf(item.field)>=0)
        columns.push(item);
});
// columns = columns_set;

//datatable options
var options = {
    data: {
        type: 'remote',
        source: {
            read: {
                url: baseUrl + '/ita/' + tenantId + '/contacts/get-data',
                method: 'GET',
                params: {
                    query: {
                        // generalSearch: ''
                    }
                    // custom query params
                },
                map: function (raw) {

                    // sample data mapping
                    var dataSet = raw;
                    if (typeof raw.data !== 'undefined') {
                        dataSet = raw.data;
                    }
                    return dataSet;
                }
            }
        },
        pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    },
    layout: {
        theme: 'default',
        class: '',
        scroll: false,
        // height: null,
        footer: false,
        // header: true,

        smoothScroll: {
            scrollbarShown: true
        },

        spinner: {
            overlayColor: '#000000',
            opacity: 0,
            type: 'loader',
            state: 'brand',
            message: true
        },

        icons: {
            sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
            pagination: {
                next: 'la la-angle-right',
                prev: 'la la-angle-left',
                first: 'la la-angle-double-left',
                last: 'la la-angle-double-right',
                more: 'la la-ellipsis-h'
            },
            rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
    },

    sortable: true,

    pagination: true,

    search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
    },

    rows: {
        afterTemplate: function (row,data,index) {

            //block dropdown click
            $('.additional-list').on('click',function (e) {
                e.preventDefault();
                e.stopPropagation();
            });

            //check selected rows
            // checkNodes();
            
            // Init popovers
            mApp.initPopover($(row).find('[data-toggle="m-popover"]'));

            //remove contact action
            $('.remove-contact').on('click',function (e) {
                var button = $(this);
                swal({
                    title:  GetTranslation('delete_question','Are you sure?'),
                    text: button.data('message'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if (result.value) {
                        $.ajax({
                            url:baseUrl + '/ita/' + tenantId + '/contacts/delete?id='+button.data('id')+'&redirect=0',
                            method:'GET',
                            success:function (response) {
                                datatable.reload();
                                var content = {
                                    title: GetTranslation('contacts_delete_success','Success!'),
                                    message: ''
                                };
                                $.notify(content,{
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                            },
                            error:function (response) {
                                var mes = response;
                                if(response.error != undefined)
                                    mes = response.error;
                                if(response.message != undefined)
                                    mes = response.message;
                                if(response.responseText != undefined)
                                    mes = response.responseText;
                                if(response.text != undefined)
                                    mes = response.text;
                                var content = {
                                    title: 'Error',
                                    message: mes
                                };
                                $.notify(content,{
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            })
        },
        callback: function () {
        },
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false
    },

    // columns definition
    columns: columns,

    toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
            pagination: {
                type: 'default',

                pages: {
                    desktop: {
                        layout: 'default',
                        pagesNumber: 6
                    },
                    tablet: {
                        layout: 'default',
                        pagesNumber: 3
                    },
                    mobile: {
                        layout: 'compact'
                    }
                },

                navigation: {
                    prev: true,
                    next: true,
                    first: true,
                    last: true
                },

                pageSizeSelect: [10, 20, 30, 50, 100]
            },

            info: true
        }
    },

    translate: {
        records: {
            processing: GetTranslation('datatable_data_processiong','Please wait...'),
            noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">'+GetTranslation('contacts_records_show_all','Show all')+'</a>'
        },
        toolbar: {
            pagination: {
                items: {
                    default: {
                        first: GetTranslation('pagination_first','First'),
                        prev: GetTranslation('pagination_previous','Previous'),
                        next: GetTranslation('pagination_next','Next'),
                        last: GetTranslation('pagination_last','Last'),
                        more: GetTranslation('pagination_more','More pages'),
                        input: GetTranslation('pagination_page_number','Page number'),
                        select: GetTranslation('pagination_page_size','Select page size')
                    },
                    info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                }
            }
        }
    },
    extensions:{
        checkbox: {}
    }
};
var datatable = $('#contacts_t').mDatatable(options);
// var query = datatable.getDataSourceQuery();

$('#select-all-contacts').on('click',function (e) {
    e.preventDefault();
    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/contacts/select-all',
        type:'get',
        method: 'get',
        dataType: 'json',
        data:{
            search:datatable.options.data.source.read.params.search
        },
        success:function (response) {
            additional_selected = response;
            $('#m_datatable_selected_number').html(additional_selected.length);
            datatable.rows('.m-datatable__row').
            nodes().
            find('.m-checkbox--single > [type="checkbox"]').each(function () {
               $(this).prop('checked',true);
            });
        }
    })
});

function checkNodes() {
    var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
    var i;
    var count = checkedNodes.length;
    datatable.rows('.m-datatable__row').
    nodes().
    find('.m-checkbox--single > [type="checkbox"]').each(function () {
        if(!$(this).prop('checked')) {
            i = additional_selected.indexOf(parseInt($(this).val()));
            // console.log($(this).val());
            if(i>=0)
                additional_selected.splice(i,1);
        }
        else{
            i = additional_selected.indexOf(parseInt($(this).val()));
            if(i<0)
                additional_selected.push(parseInt($(this).val()));
        }
    });
    // console.log(datatable.rows('.m-datatable__row--active').nodes().length);
    // console.log(additional_selected);
    if (count > 0) {
        if(additional_selected.length>0)
            $('#m_datatable_selected_number').html(additional_selected.length);
        else
            $('#m_datatable_selected_number').html(count);
            $('#selection-row').css({display: 'inline-block'});
            $('#contacts-datatable .tools__left_section').addClass('space_between_items');
            $('#m_datatable_selection').collapse('show');
            $('#m_datatable_total_number').html(datatable.API.params.pagination.total);
    } else {
        additional_selected = [];
        $('#selection-row').css({display: 'none'});
        $('#contacts-datatable .tools__left_section').removeClass('space_between_items');
        $('#m_datatable_selection').collapse('hide');
    }
}

datatable.on('m-datatable--on-ajax-done', function(e) {
    // datatable.checkbox() access to extension methods
    // console.log('Reloaded');
    setTimeout(checkNodes,1000);
    // checkNodes();
});

datatable.on('m-datatable--on-click-checkbox', function(e) {
    // datatable.checkbox() access to extension methods
    // datatable.on('m-datatable--on-layout-updated', function(e) {
    //     console.log('Reload');
    //     // datatable.checkbox() access to extension methods
    //     console.log(datatable.rows('.m-datatable__row--active').nodes().length);
    //     checkNodes();
    // });
    checkNodes();
});

// datatable.reload();

//
$('#remove-contacts').on('click',function () {
    var ids = [];
    datatable.rows('.m-datatable__row--active').
    nodes().
    find('.m-checkbox--single > [type="checkbox"]').
    map(function(i, chk) {
        ids.push($(chk).val());
    });
    if(ids.length>0)
    {
        var p_ids = ids.join(',');
        swal({
            title: GetTranslation('delete_question','Are you sure?'),
            text: GetTranslation('contacts_delete_message1','Are you sure to delete this ')+ ids.length
                +GetTranslation('contacts_delete_message2',' items?'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
            cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
                $.ajax({
                    url:baseUrl + '/ita/' + tenantId + '/contacts/bulk-delete',
                    method: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        ids: p_ids
                    },
                    success: function (response) {
                        datatable.reload();
                        var content = {
                            title: GetTranslation('contacts_delete_success','Success!'),
                            message: ''
                        };
                        $.notify(content,{
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            z_index: 1031,
                            type: "success"
                        });
                    },
                    error:function (response) {
                        var mes = response;
                        if(response.error != undefined)
                            mes = response.error;
                        if(response.message != undefined)
                            mes = response.message;
                        if(response.responseText != undefined)
                            mes = response.responseText;
                        if(response.text != undefined)
                            mes = response.text;
                        var content = {
                            title: 'Error',
                            message: mes
                        };
                        $.notify(content,{
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            }
        });
    }
});


$('#contacts_settings_modal').on('show.bs.modal', function (event) {
    var st = window.localStorage.getItem('contacts_settings');
    if(st == null) {
        st = def_set;
    }
    else {
        st = st.split(',');
    }
    $(this).find('input[type="checkbox"]').each(function () {
       var field = $(this).data('field');
       if(st.indexOf(field)>=0)
           $(this).prop('checked',true);
       else
           $(this).prop('checked',false);
    });
});

$('#save-settings').on('click',function () {
    var st= window.localStorage.getItem('contacts_settings');
    if(st === null) {
        st = def_set;
    }
    else {
        st = st.split(',');
    }
    $('#contacts_settings_modal').find('input[type="checkbox"]').each(function () {
        var field = $(this).data('field');
        if($(this).prop('checked')){
            if(st.indexOf(field)<0)
                st.push(field);
        }
        else{
            if(st.indexOf(field)>=0)
                st.splice(st.indexOf(field),1);
        }
    });

    window.localStorage.setItem('contacts_settings',st.join(','));
    window.location.reload();
});

/* BULK ACTIONS FUNCTIONS */
window.initBulckActions = function(){
    var bulk_action_type_picker = $('#bulk_action_type_picker').get()[0];
    var bulk_form = $('#bulk-action-form').get()[0];

    function initSelectTags() {
        $('#tags_select').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation('modal_bulk_select_tags'),
            width: '100%',
            dropdownParent: $('#m_modal_contacts_bulk_actions')
        });
    }

    function initSelectResponsible() {
        $('#responsible_select').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['first_name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation('modal_bulk_select_responsible'),
            width: '100%',
            dropdownParent: $('#m_modal_contacts_bulk_actions')
        });
    }

    function loadPropertyTemplate(){
        var action_id = $(bulk_action_type_picker).selectpicker().val();
        var new_template = $('#bulk-update-block-' + action_id).get()[0];

        $(bulk_form).find('#form_select').html('').append($(new_template).clone());

        var select_id = $(new_template).find('select').attr('id');

        if (select_id == 'tags_select')
            initSelectTags();

        if (select_id == 'responsible_select')
            initSelectResponsible();
    }

    $('#m_modal_contacts_bulk_actions').on('shown.bs.modal', function (e) {
        $('#tags_select').val(null).trigger('change');
        $('#responsible_select').val(null).trigger('change');

        var ids = [];

        datatable.rows('.m-datatable__row--active')
            .nodes()
            .find('.m-checkbox--single > [type="checkbox"]')
            .map(function(i, chk) {
                ids.push($(chk).val());
            });

        if (ids.length > 0) {
            $('#ids_input').val(ids);
        }
    });

    $('#m_modal_contacts_bulk_actions button[type="submit"][form="bulk-action-form"]').on('click', function (e) {
        localStorage.setItem('contacts_t-1-checkbox', '{"selectedRows":[],"unselectedRows":[]}');

        var meta = $.parseJSON(localStorage.getItem('contacts_t-1-meta'));
        meta.selectedAllRows = false;
        localStorage.setItem('contacts_t-1-meta', JSON.stringify(meta));
    });
    
    $(bulk_action_type_picker).on('change', function () {
        loadPropertyTemplate();
    });
    
    //First load
    loadPropertyTemplate();
};

(function($){
    $(document).ready(function(){
        
        initBulckActions();
        makeContactsSettingsModalSchrollable();
        makeContactsEditSettingsModalSchrollable();
        initContactQuickEditModal();
        initMergeContactsModal();
        
        function makeContactsSettingsModalSchrollable() {
            var scrollable_element = $('#contacts_settings_modal .custom-m-scrollable').get()[0];
            // Calculate needle height of scrollable element
            $(scrollable_element).css({
                "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px",
                "padding-right" : "10px"
            });
            var Scrollable = new PerfectScrollbar(scrollable_element, {
                suppressScrollX: true,
                wheelPropagation: true
            });
            // Update scrollable block size on shown event
            $('#contacts_settings_modal').on('shown.bs.modal', function(){
                $(scrollable_element).css({
                    "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px"
                });
                Scrollable.update();
            });
            // Update scrollable block size on window resize event
            $(window).resize(function() {
                $(scrollable_element).css({
                    "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px"
                });
                Scrollable.update();
            });
        }
        
        function makeContactsEditSettingsModalSchrollable() {
            var scrollable_element = $('#contacts_edit_settings_modal .custom-m-scrollable').get()[0];
            // Calculate needle height of scrollable element
            $(scrollable_element).css({
                "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px",
                "padding-right" : "10px"
            });
            var Scrollable = new PerfectScrollbar(scrollable_element, {
                suppressScrollX: true,
                wheelPropagation: true
            });
            // Update scrollable block size on shown event
            $('#contacts_edit_settings_modal').on('shown.bs.modal', function(){
                $(scrollable_element).css({
                    "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px"
                });
                Scrollable.update();
            });
            // Update scrollable block size on window resize event
            $(window).resize(function() {
                $(scrollable_element).css({
                    "height" : ((window.innerHeight - 262) >= 200) ? ((window.innerHeight - 262) + "px") : "200px"
                });
                Scrollable.update();
            });
        }
        
        function initContactQuickEditModal(){
            class QuickEditModal
            {
                constructor(){
                    this.contact_id = null;
                    this.body = $('#contact_quick_edit_modal .modal-body').get()[0];
                    this.scrollable_body = null;
                    this.initScrollableBody();
                    this.initTaskFormWidgets();
                }
                updateContactIdInForm(contact_id){
                    this.contact_id = parseInt(contact_id);
                    $('#contact_id_quick_edit_modal_field').val(this.contact_id);
                }
                clearContactIdInForm(){
                    this.contact_id = null;
                    $('#contact_id_quick_edit_modal_field').val(this.contact_id);
                }
                initScrollableBody(){
                    $(this.body).css({
                        "height" : ((window.innerHeight - 121) >= 200) ? ((window.innerHeight - 121) + "px") : "200px"
                    });
                    this.scrollable_body = new PerfectScrollbar(this.body, {
                        suppressScrollX: true,
                        wheelPropagation: true
                    });
                }
                updateModalScrollableBodyHeight(){
                    $(this.body).css({
                        "height" : ((window.innerHeight - 121) >= 200) ? ((window.innerHeight - 121) + "px") : "200px"
                    });
                    this.scrollable_body.update();
                }
                initTaskFormWidgets(){
                    var self = this;
                    // Due date picker init
                    $(self.body).find('.task_input__due_date').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        orientation: "bottom left",
                        format: date_format.toLowerCase(),
                        language: tenant_lang,
                        autoclose: true,
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    // Due time picker init
                    var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
                    $(self.body).find('.task_input__due_time').timepicker({
                        minuteStep: 5,
                        showSeconds: false,
                        showMeridian: format_AM_PM,
                        defaultTime: ''
                    });
                    // Reminder date picker init
                    $(self.body).find('.task_input__email_reminder_date').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        orientation: "bottom left",
                        format: date_format.toLowerCase(),
                        language: tenant_lang,
                        autoclose: true,
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    // Reminder time picker init
                    var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
                    $(self.body).find('.task_input__email_reminder_time').timepicker({
                        minuteStep: 5,
                        showSeconds: false,
                        showMeridian: format_AM_PM,
                        defaultTime: ''
                    });
                    // Notes textarea
                    autosize($('#create_task_note_textarea'));
                    // Event - change due date
                    $(self.body).find('.task_input__due_date').on('change', function(){
                        updateTaskDates(true);
                    });
                    // Event - change due time
                    $(self.body).find('.task_input__due_time').timepicker().on('changeTime.timepicker', function (e) {
                        updateTaskDates(true);
                    });
                    // Event - open due time
                    $(self.body).find('.task_input__due_time').timepicker().on('show.timepicker', function (e) {
                        if(!e.time.value){
                            $(this).timepicker('setTime', '09:00');
                        }
                    });
                    // Event - change email reminder date
                    $(self.body).find('.task_input__email_reminder_date').on('change', function(){
                        updateTaskDates();
                    });
                    // Event - change email reminder time
                    $(self.body).find('.task_input__email_reminder_time').timepicker().on('changeTime.timepicker', function (e) {
                        updateTaskDates();
                    });
                    // Event - open reminder time
                    $(self.body).find('.task_input__email_reminder_time').timepicker().on('show.timepicker', function (e) {
                        if(!e.time.value){
                            $(this).timepicker('setTime', '09:00');
                        }
                    });
                    function updateTaskDates(calculate_reminder_time){
                        var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
                        var due_date = moment($(self.body).find('.task_input__due_date').val(), date_format).format('X');
                        var due_time = moment($(self.body).find('.task_input__due_time').val(), remove_seconds_from_format(time_format)).format('X');
                        var reminder_date = moment($(self.body).find('.task_input__email_reminder_date').val(), date_format).format('X');
                        var reminder_time = moment($(self.body).find('.task_input__email_reminder_time').val(), remove_seconds_from_format(time_format)).format('X');

                        if(due_date !== 'Invalid date' && due_time !== 'Invalid date'){
                            // Get full date timestamp
                            var date = new Date(due_date * 1000);
                            var time = new Date(due_time * 1000);
                            var years = date.getFullYear();
                            var months = date.getMonth()+1;
                            var days = date.getDate();
                            var hours = time.getHours();
                            var minutes = time.getMinutes();
                            var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                            var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                            // Add timestamp in hidden input
                            $(self.body).find('.task_due_date_timestamp').val(full_date_timestamp);
                            // Auto genetare email reminder time
                            if(calculate_reminder_time === true){
                                var new_reminder_date = new Date((full_date_timestamp - (60 * 60 * 3)) * 1000);  // 3 hours ago
                                $(self.body).find('.task_input__email_reminder_date').datepicker('setDate', new_reminder_date);
                                $(self.body).find('.task_input__email_reminder_time').timepicker('setTime', new_reminder_date);
                            }
                        }else{
                            // Clear hidden timestamp input if no data
                            $(self.body).find('.task_due_date_timestamp').val('');
                        }
                        if(reminder_date !== 'Invalid date' && reminder_time !== 'Invalid date'){
                            // Get full date timestamp
                            var date = new Date(reminder_date * 1000);
                            var time = new Date(reminder_time * 1000);
                            var years = date.getFullYear();
                            var months = date.getMonth()+1;
                            var days = date.getDate();
                            var hours = time.getHours();
                            var minutes = time.getMinutes();
                            var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                            var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                            // Add timestamp in hidden input
                            $(self.body).find('.email_reminder_timestamp').val(full_date_timestamp);
                        }else{
                            // Clear hidden timestamp input if no data
                            $(self.body).find('.email_reminder_timestamp').val('');
                        }
                    }
                    function remove_seconds_from_format(format){
                        var new_format = format.replace(':ss', '');
                        return new_format;
                    }
                }
            }
            
            var modal = new QuickEditModal();
            
            // Shown event
            $('#contact_quick_edit_modal').on('shown.bs.modal', function(event){
                modal.updateContactIdInForm($(event.relatedTarget).data('contact-id'));
                modal.updateModalScrollableBodyHeight();
            });
            
            // Hidden event
            $('#contact_quick_edit_modal').on('hidden.bs.modal', function(){
                modal.clearContactIdInForm();
            });
            
            // Window resize event
            $(window).resize(function() {
                modal.clearContactIdInForm();
            });
        }
        
        function initMergeContactsModal(){
            
            class MergeModal
            {
                constructor(){
                    this.modal = $('#contacts_merge_modal').get()[0];
                    this.merge_all_link = $('#merge-all-link').get()[0];
                    this.initElements();
                }
                initElements(){
                    // Start to init modal widgets and add event listeners
                    this.initMergeAllButton();
                    this.initMergeOneButton();
                    this.initMergeDismissButons();
                }
                initMergeAllButton(){
                    // Click on Merge All button event
                    $(this.merge_all_link).on('click', function(){
                        swal({
                            title: GetTranslation('merge_all_alert_message_title', 'Contacts merge'),
                            text: GetTranslation('merge_all_alert_message_question', 'Are you sure you want to merge all these contacts?'),
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: GetTranslation('yes', 'Yes'),
                            cancelButtonText: GetTranslation('no', 'No'),
                            reverseButtons: true,
                            showCloseButton: true,
                            focusConfirm: false
                        }).then( function(isConfirm) {
                            if(isConfirm.value) {
                                alert('Стартуем процесс обеденения контактов!');
                            }
                        });
                    });
                }
                initMergeOneButton(){
                    $(this.modal).find('.merge-one-link').each(function(){
                        var button = this;
                        $(button).on('click', function(){
                            swal({
                                title: GetTranslation('merge_this_contacts_message_title', 'Merge contacts'),
                                text: GetTranslation('merge_this_contacts_message_question', 'Do you realy want to merge this contacts?'),
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonText: GetTranslation('yes', 'Yes'),
                                cancelButtonText: GetTranslation('no', 'No'),
                                reverseButtons: true,
                                showCloseButton: true,
                                focusConfirm: false
                            }).then( function(isConfirm) {
                                if(isConfirm.value) {
                                    alert('Стартуем процесс объединения текущей комбинации контактов!');
                                }
                            });
                        });
                    });
                }
                initMergeDismissButons(){
                    $(this.modal).find('.dismiss-merge-combination').each(function(){
                        var button = this;
                        $(button).on('click', function(){
                            swal({
                                title: GetTranslation('merge_dismiss_message_title', 'Merge dismiss'),
                                text: GetTranslation('merge_dismiss_message_question', "Do you want to exclude this combination of contacts and don't offer their merge in the future?"),
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonText: GetTranslation('yes', 'Yes'),
                                cancelButtonText: GetTranslation('no', 'No'),
                                reverseButtons: true,
                                showCloseButton: true,
                                focusConfirm: false
                            }).then( function(isConfirm) {
                                if(isConfirm.value) {
                                    alert('Стартуем процесс отклонения данной комбинации!');
                                }
                            });
                        });
                    });
                }
            }
            
            var modal = new MergeModal();
        }
    });
})(jQuery);
