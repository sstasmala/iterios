/**
 * profile/dropzone.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

//== Class definition

var DropzoneDemo = function () {
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div id="alert" class="form-group m-form__group">\
            <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div></div>');

        $('#upload_file_block').find('#alert').remove();

        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    };

    //== Private functions
    var document_upload = function () {
        // file type validation
        Dropzone.options.mDropzone = {
            url: baseUrl + '/ita/' + tenantId + '/contacts/upload-documents?order_id='+$('#upload_btn').data('contact_id'),
            params: {},
            paramName: 'documents', // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 25, // MB
            addRemoveLinks: true,
            acceptedFiles: 'image/*,application/pdf,text/plain,application/msword,application/docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/rtf,text/rtf,text/richtext',
            // Prevents Dropzone from uploading dropped files immediately
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 10,
            dictInvalidFileType: GetTranslation('order_document_upload_wrong_file_ext_error'),
            dictDefaultMessage: GetTranslation('order_document_upload_not_upl_error'),
            dictFileTooBig: GetTranslation('order_document_upload_too_big_error_js'),
            dictMaxFilesExceeded: GetTranslation('order_document_upload_too_many_error_js'),
            init: function () {
                var submitButton = document.querySelector("#upload_btn");
                var pictureDropzone = this; // closure
                var upload_btn = $('#upload_btn');

                submitButton.addEventListener('click', function () {
                    upload_btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    $(pictureDropzone.element).find('.dz-progress').show();
                    pictureDropzone.processQueue(); // Tell Dropzone to process all queued files.
                });

                this.on('addedfile', function (file) {
                    $(this.element).find('.dz-progress').hide();
                    upload_btn.removeAttr('disabled').css('cursor', 'pointer');
                });

                this.on('maxfilesexceeded', function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('sending', function (file, xhr, formData) {
                    var csrf = upload_btn.closest('form').find('input[name="_csrf"]').val();
                    formData.append('_csrf', csrf);
                });

                this.on('successmultiple', function (file, response) {
                    if (response) {
                        // similate 2s delay
                        setTimeout(function() {
                            pictureDropzone.removeAllFiles();
                            upload_btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            var content = {
                                title: GetTranslation('order_document_upload_success'),
                                message: ''
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1055,
                                type: "success"
                            });

                            $('#file_add_modal').modal('hide');

//                            $.each(response, function( index, file ) {
//                                var template = '<li>' +
//                                    '<div class="name">' +
//                                    '<a href="#" class="m-link m--font-bold file-link" data-toggle="modal" data-target="#file_upload_view_modal" data-order_id="' + file.order_id + '" data-contact_file_id="' + file.id + '">' + file.title + '</a>' +
//                                    '<div class="date">' +
//                                    '<span class="moment-js invisible" data-format="unix">' + file.updated_at + '</span>' +
//                                    '</div></div><div class="actions">' +
//                                    '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill other-doc-delete-link" title="' + GetTranslation('delete') + '" data-contact_file_id="' + file.id + '">' +
//                                    '<i class="la la-trash"></i></a></div></li>';
//
//                                $('.order-docs-block .other-docs').prepend(template);
//                                $('.order-documents-portlet div.text-center').hide();
//                            });

                            initMomentJS();
                        }, 1000);
                    }
                });

                this.on('error', function (file, errorMessage) {
                    console.log(errorMessage);

                    pictureDropzone.removeAllFiles();
                    upload_btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    upload_btn.attr('disabled', 'disabled').css('cursor', 'not-allowed');
                    showErrorMsg(upload_btn.closest('form'), 'danger', GetTranslation('order_document_upload_error') + '<br/>' + (errorMessage.message != undefined ? errorMessage.message : errorMessage));
                });
            }
        };
    };

    var delete_file = function () {
        $('body').on('click', '.contact-file-delete-link', function (e) {
            var contact_file_id = $(this).data('contact_file_id');

            swal({
                title: GetTranslation('delete_question', 'Are you sure?'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/delete-upload-document?document_id=' + contact_file_id,
                        type: 'GET',
                        success: function (response) {
                            var content = {
                                title: GetTranslation('success_message', 'Success!'),
                                message: ''
                            };
                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1055,
                                type: "success"
                            });

                            $('#file_upload_view_modal').modal('hide');

                            $('li a[data-contact_file_id="' + contact_file_id + '"]').closest('li').remove();

                            if ($('.other-docs li').length === 0)
                                $('#other_doc_block').hide();

                            if ($('.other-docs li').length === 0 && $('.our-docs li').length === 0)
                                $('.order-documents-portlet div.text-center').show();
                        },
                        error: function (response) {
                            var content = {
                                title: 'Error',
                                message: response.responseText.replace(':', '')
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                type: "danger",
                                z_index: 1055
                            });
                        }
                    });
                } else if (result.dismiss === 'cancel') {
                }
            });
        });
    };

   var file_preview =  function () {
        $('body').on('click', '.file-link', function (e) {
            $('#file_upload_view_modal .file_title').val('').trigger('change');

            var file_title = $(this).text();
            $('#file_upload_view_modal .file_title').val($.trim(file_title)).trigger('change');

            var order_id = $(this).data('order_id');
            var contact_file_id = $(this).data('contact_file_id');

            $('#file_upload_view_modal #file_preview').empty();

            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/get-upload-document?document_id=' + contact_file_id,
                type: 'GET',
                success: function (response) {
                    if (response) {
                        $('#file_upload_view_modal .file_title').val(response.title).trigger('change');
                        $('#file_upload_view_modal #file_delete_btn').data('contact_file_id', contact_file_id);
                        $('#file_upload_view_modal #other-file-title').data('contact_file_id', contact_file_id);
                        $('#file_upload_view_modal #download-file').data('contact_file_id', contact_file_id);

                        var file = '';
                        var img_type = ['image/png', 'image/jpeg', 'image/gif', 'image/bmp'];

                        var editor_height = (window.innerHeight - 255) < 500 ? 500 : window.innerHeight - 255;

                        if ($.inArray(response.file_type, img_type) !== -1) {
                            file = '<img class="img-thumbnail" style="max-width: 100%;" src="' + baseUrl + '/' + response.file_path + '" alt="' + response.title + '">';
                        } else if (response.file_type === 'application/pdf') {
                            file = '<iframe src="' + baseUrl + '/' + response.file_path + '" width="100%" height="'+editor_height+'px"></iframe>';
                        } else {
                            file = '<iframe src="https://docs.google.com/gview?url=' + baseUrl + '/' + response.file_path + '&embedded=true" width="100%" height="'+editor_height+'px"></iframe>';
                        }

                        $('#file_upload_view_modal #file_preview').append(file);
                    }
                }
            });
        });
   };

    var document_title_update = function () {
        var searchDelay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $('#file_upload_view_modal #other-file-title').on('input', function (e) {
            var input = $(this);

            searchDelay(function () {
                var value = input.val();

                if (value.length > 1) {
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/save-upload-document?document_id='+input.data('contact_file_id'),
                        method: 'POST',
                        data: {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            title: value
                        },
                        success: function (response) {
                            if (response) {
                                setTimeout(function () {
                                    var content = {
                                        title: GetTranslation('success_message', 'Success!'),
                                        message: ''
                                    };

                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        z_index: 1055,
                                        type: "success"
                                    });

                                    input.blur();
                                    $('#file_upload_view_modal .file_title').val(response.title).trigger('change');
                                    $('li a[data-contact_file_id="'+response.id+'"]').closest('li').remove();

                                    var template = '<li>' +
                                        '<div class="name">' +
                                        '<a href="#" class="m-link m--font-bold file-link" data-toggle="modal" data-target="#file_upload_view_modal" data-order_id="' + response.order_id + '" data-contact_file_id="' + response.id + '">' + response.title + '</a>' +
                                        '<div class="date">' +
                                        '<span class="moment-js invisible" data-format="unix">' + response.updated_at + '</span>' +
                                        '</div></div><div class="actions">' +
                                        '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill other-doc-delete-link" title="' + GetTranslation('delete') + '" data-contact_file_id="' + response.id + '">' +
                                        '<i class="la la-trash"></i></a></div></li>';

                                    $('.order-docs-block .other-docs').prepend(template);

                                    initMomentJS();
                                }, 500);
                            }
                        },
                        error: function (response) {
                            var content = {
                                title: 'Error',
                                message: response.responseText.replace(':', '')
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                type: "danger",
                                z_index: 1055
                            });
                        }
                    });
                }
            }, 1000);
        });
    };

    var document_download = function () {
        $('#file_upload_view_modal #download-file').on('click', function (e) {
            var contact_file_id = $(this).data('contact_file_id');
            $(this).blur();

            setTimeout(function () {
                var content = {
                    title: GetTranslation('success_message', 'Success!'),
                    message: ''
                };

                $.notify(content, {
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    z_index: 1055,
                    type: "success"
                });

                location.href = baseUrl + '/ita/' + tenantId + '/orders/download-document?document_id=' + contact_file_id;
            }, 500);
        });
    };

    return {
        // public functions
        init: function() {
            document_upload();
            delete_file();
            file_preview();
            document_title_update();
            document_download();
        }
    };
}();

DropzoneDemo.init();