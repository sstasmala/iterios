(function($){
    $(document).ready(function(){
        initValidate();
        initPageWidgets();
        initPassportInputsMask();
        initExtraDropdownOptionInputs();
        initPassportPopup();
        initVisaPopup();
        initDeleteForm();
        initDeletePassportForm();
        initDeleteVisaForm();
        initPostForm();
        initContactRelationsForm();
        initContactsManualMerge();

        $('body').find('form').attr('autocomplete', 'off');
        $('#submit-date-contact').click(function() {
            var date_of_birth_contact =  $('#contact-info-table-date_of_birth').val();
            $('[data-date]').attr('data-date',date_of_birth_contact);
        });

        function initPostForm() {

            $('#modal-add-passport-save').on('click', function () {
                var form = $('#add_passport_form');
                form.validate({
                    ignore: 'input[type=hidden], .select2-input, .select2-focusser',
                    rules: {
                        'ContactPassport[first_name]': {
                            required: true
                        },
                        'ContactPassport[last_name]': {
                            required: true
                        },
                        'ContactPassport[type]': {
                            required: true
                        },
                        'ContactPassport[birth_date]': {
                            required: true
                        },
                        'ContactPassport[serial]': {
                            required: true
                        },
                        'ContactPassport[date_limit]': {
                            required: true
                        },
                        // 'ContactPassport[issued_date]': {
                        //     required: true
                        // },
                        // 'ContactPassport[issued_owner]': {
                        //     required: true
                        // }
                    }
                });

                if (!form.valid()) {
                    $(form).find('div.form-control-feedback').css('color', 'red');
                    return;
                }

                var date_of_birth_contact =  $('#contact-info-table-date_of_birth').val();
                var date_of_birth_passport = $('#passport_form_birth_date_datepicker').val();
                var data_date = $('[data-date]').attr('data-date');

                var id_contact = $("[name='id']").eq(0).val();

                if(date_of_birth_passport !== "" && typeof date_of_birth_passport !== "undefined") {

                    if(date_of_birth_contact !== date_of_birth_passport && data_date !== '') {
                        swal({
                            title: GetTranslation("contacts_passport_birth_notsame", "Birth dates do not match"),
                            text: GetTranslation("contacts_passport_update_birth", "Update birth date in contact card?"),
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonText: GetTranslation('yes', 'Yes'),
                            cancelButtonText: GetTranslation('no', 'No'),
                            reverseButtons: true,
                            showCloseButton: true,
                            customClass: 'noneShow1',
                            focusConfirm: false
                        }).then( function(isConfirm) {
                            if((isConfirm.dismiss === 'cancel') || (isConfirm.value)) {
                                var url = baseUrl + '/ita/' + tenantId + '/contacts/add-passport?id='+id_contact;
                                var helper = 'true';
                                if(isConfirm.dismiss === 'cancel'){
                                    helper = 'cancel';
                                }
                                var action = 'not_same';
                                $.ajax({
                                    url: url,
                                    type: 'post',
                                    dataType: 'json',
                                    data: {
                                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                                        data : $('#add_passport_form').serialize(),
                                        action: action,
                                        helper: helper
                                    },
                                    success: function (response) { //Данные отправлены успешно
                                        if(typeof response.ok !== 'undefined' && response.ok == true){
                                            location.reload();
                                        } else {
                                            swal({
                                                text: response['error'],
                                                type: "warning"
                                            });
                                            $('.modal-backdrop.show').eq(1).remove();
                                        }
                                    }
                                });
                            }
                        });
                    } else if (date_of_birth_contact !== date_of_birth_passport && data_date === '') {
                        var url = baseUrl + '/ita/' + tenantId + '/contacts/add-passport?id='+id_contact;
                        var action = 'not_same';
                        var helper = 'true';
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                data : $('#add_passport_form').serialize(),
                                action: action,
                                helper: helper
                            },
                            success: function (response) { //Данные отправлены успешно
                                if(typeof response.ok !== 'undefined' && response.ok == true) {
                                    location.reload();
                                } else {
                                    swal({
                                        text: response['error'],
                                        type: "warning",
                                        customClass: 'noneShow2'
                                    });
                                    $('.modal-backdrop.show').eq(1).remove();
                                }
                            }
                        });
                    } else if(data_date === date_of_birth_passport) {
                        var url = baseUrl + '/ita/' + tenantId + '/contacts/add-passport?id='+id_contact;
                        var action = 'same';
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                data : $('#add_passport_form').serialize(),
                                action: action
                            },
                            success: function (response) { //Данные отправлены успешно
                                if(typeof response.ok !== 'undefined' && response.ok == true) {
                                    location.reload();
                                } else {
                                    swal({
                                        text: response['error'],
                                        type: "warning",
                                        customClass: 'noneShow2'
                                    });
                                    $('.modal-backdrop.show').eq(1).remove();
                                }
                            }
                        });
                    }
                }
            });

        }

        function initPageWidgets(){
            $('#contact-info-table-departure_date_first').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(new Date()).subtract(100, 'years').format(date_format),
                maxDate: moment(new Date()).format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function(e){
                $('.daterangepicker').on('click', function(e){
                    // It is fix for daterangepicker. It disble closing 
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });
            $('#company_select_input').select2({
                placeholder: "Select a company"
            });
            $('#passport_form_date_limit_datepicker').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            $('#passport_form_birth_date_datepicker').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(new Date()).subtract(100, 'years').format(date_format),
                maxDate: moment(new Date()).format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function(e){
                $('.daterangepicker').on('click', function(e){
                    // It is fix for daterangepicker. It disble closing 
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });
            $('body').find('form').attr('autocomplete', 'off');
            $('#passport_form_issued_date_datepicker').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            $('#contact_visa_begin_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            $('#contact_visa_end_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            $('#contact-info-table-date_of_birth').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(new Date()).subtract(100, 'years').format(date_format),
                maxDate: moment(new Date()).format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function(e){
                $('.daterangepicker').on('click', function(e){
                    // It is fix for daterangepicker. It disble closing
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });
            $('#conctact-add-note-popup').on('shown.bs.modal', function () {
                $('#add-note-preload').hide();
                $('#add-note-redactor').summernote({
                    height: 300,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ],
                });
            }); 
            $('#contacts-modal-tags-search-select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public'
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("conctact-edit-popup-add-tag-plaseholder", "Add a tag"),
                tags: true
            });
            $('#contact-info-table-company_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-companies-options',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                dropdownParent: $('#contact-info-table-company_select').parent(),
                width: '100%',
                placeholder: GetTranslation("info-editable-table-select_company_placeholder", "Select company")
            });
            $('#select2-responsible-ajax-search').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/contacts/get-responsible-options',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            var full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("contact-settings-partlet-responsible_select_placeholder", "Select responsible")
            });
            $('#select2-responsible-ajax-search').on('change', function(){
                var contact_id = $('#select2-responsible-ajax-search-contact-id-hint').val();
                var selected = $(this).find("option:selected").val();
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/contacts/change-responsible',
                    data: {
                        contact_id: contact_id,
                        responsible_id: selected,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method:'POST',
                    success:function (response) {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if(response.result === 'success'){
                            toastr.success(response.message);
                        }
                        if(response.result === 'error'){
                            toastr.error(response.message);
                        }
                    }
                });
            });
            
            $('#copy-living-address').on('click', function (e) {
                var data = {
                    id: $('.etitable-form select[name="living_address[country]"]').val(),
                    text: $('.etitable-form select[name="living_address[country]"] option:selected').text()
                };
                var newOption = new Option(data.text, data.id, true, true);
                // Append it to the select
                $('.etitable-form select[name="residence_address[country]"]').append(newOption).trigger('change');

                $('.etitable-form input[name="residence_address[region]"]').val($('.etitable-form input[name="living_address[region]"]').val());
                $('.etitable-form input[name="residence_address[city]"]').val($('.etitable-form input[name="living_address[city]"]').val());
                $('.etitable-form input[name="residence_address[street]"]').val($('.etitable-form input[name="living_address[street]"]').val());
                $('.etitable-form input[name="residence_address[house]"]').val($('.etitable-form input[name="living_address[house]"]').val());
                $('.etitable-form input[name="residence_address[flat]"]').val($('.etitable-form input[name="living_address[flat]"]').val());
                $('.etitable-form input[name="residence_address[postcode]"]').val($('.etitable-form input[name="living_address[postcode]"]').val());
            });

            $('#contact-phones-repeater, #contact-emails-repeater, #contact-socials-repeater, #contact-messengers-repeater, #contact-sites-repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'text-input': 'foo'
                },
                show: function () {
                    $(this).slideDown();
                    //Init maskinput

                    $('#conctact-edit-popup-form')
                        .find('[valid="phoneNumber_add"]')
                        .intlTelInput({
                            utilsScript: "/plugins/phone-validator/build/js/utils.js",
                            autoPlaceholder: true,
                            preferredCountries: ['ua', 'ru'],
                            allowExtensions: false,
                            autoFormat: true,
                            autoHideDialCode: true,
                            customPlaceholder: null,
                            defaultCountry: "ua",
                            geoIpLookup: null,
                            nationalMode: false,
                            numberType: "MOBILE"
                        }).then(function (data) {

                        $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                            el = $(this);
                            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                            var formated = '+999 99 999 9999';
                            $(el).inputmask(formated);
                            $(el).on("countrychange", function (e, countryData) {
                                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                $(el).inputmask(formated)
                            });
                        });
                    });

                    $(this).find('input').each(function () {
                        var mask_data = $(this).data('inputmask');
                        if (mask_data) {
                            $(this).inputmask();
                        }
                    });
                    //Init dropdown option
                    initExtraDropdownOptionInputs(this);
                },
                hide: function (deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
                
            });

            var phones = $('a.phones');
            phones.each(function (i) {
                var phone = $(this).text();
                if (phone != null && phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
                    var result = phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );
                    $(this).text('+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4]);
                } else {
                    $(this).text((phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone));
                }
            });

            // Timeline history - create note redactor
            $('#add-note-from_timeline-redactor').summernote({
                height: 150,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            
            // Timeline history - create task due date picker init
            $('#timeline_new_task_due_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            
            // Timeline history - create task due time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $('#timeline_new_task_due_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: ''
            });
            
            // Timeline history - create task type picker init
            $('#timeline-create_task_type').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-task-types',
                    method: 'POST',
                    dataType: 'json',
                    data: {},
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['value']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("contacts_timeline_task_type_select_placeholder", "Select type"),
                minimumResultsForSearch: -1, //Disable search input
                width: '100%',
                dropdownParent: $('#timeline-create_task_type').parents('#history_tab_tasks')
            });
            
            // Timeline history - create task textarea init
            autosize($('#timeline-create_task_note_textarea'));
            
            // Timeline history - create task assigned to id init
            $('#timeline-create_task_assigned_to_id_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['first_name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("contacts_timeline_task_assigned_to_select_placeholder", "Select responsible"),
                width: '100%',
                dropdownParent: $('#timeline-create_task_assigned_to_id_select').parents('#history_tab_tasks')
            });
            
            // Timeline history - create task email date picker init
            $('#timeline_new_task_email_reminder_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            
            // Timeline history - create task email time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $('#timeline_new_task_email_reminder_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: '',
                placeholder: 'time'
            });

            $('#contact-info-table-company_select').on('change', function (e) {
                var data = $('#contact-info-table-company_select').select2('data');

                $('button[type="submit"]').on('click', function (e) {
                    $('#company_link').attr('href', baseUrl + '/ita/' + tenantId + '/companies/view?id=' + data[0].id);
                });
            });

            // Event - change due date
            $('#timeline-task-create-block').find('.task_input__due_date').on('change', function(){
                updateTaskDates(true);
            });
            // Event - change due time
            $('#timeline-task-create-block').find('.task_input__due_time').timepicker().on('changeTime.timepicker', function (e) {
                updateTaskDates(true);
            });
            // Event - open due time
            $('#timeline-task-create-block').find('.task_input__due_time').timepicker().on('show.timepicker', function (e) {
                if(!e.time.value){
                    $(this).timepicker('setTime', '09:00');
                }
            });
            // Event - change email reminder date
            $('#timeline-task-create-block').find('.task_input__email_reminder_date').on('change', function(){
                updateTaskDates();
            });
            // Event - change email reminder time
            $('#timeline-task-create-block').find('.task_input__email_reminder_time').timepicker().on('changeTime.timepicker', function (e) {
                updateTaskDates();
            });
            // Event - open reminder time
            $('#timeline-task-create-block').find('.task_input__email_reminder_time').timepicker().on('show.timepicker', function (e) {
                if(!e.time.value){
                    $(this).timepicker('setTime', '09:00');
                }
            });

            function updateTaskDates(calculate_reminder_time){
                /**
                 * @var calculate_reminder_time : true|null - set true if need to auto calculate email reminder date
                 */
                var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
                var due_date = moment($('#timeline-task-create-block').find('.task_input__due_date').val(), date_format).format('X');
                var due_time = moment($('#timeline-task-create-block').find('.task_input__due_time').val(), remove_seconds_from_format(time_format)).format('X');
                var reminder_date = moment($('#timeline-task-create-block').find('.task_input__email_reminder_date').val(), date_format).format('X');
                var reminder_time = moment($('#timeline-task-create-block').find('.task_input__email_reminder_time').val(), remove_seconds_from_format(time_format)).format('X');

                if(due_date !== 'Invalid date' && due_time !== 'Invalid date'){
                    // Get full date timestamp
                    var date = new Date(due_date * 1000);
                    var time = new Date(due_time * 1000);
                    var years = date.getFullYear();
                    var months = date.getMonth()+1;
                    var days = date.getDate();
                    var hours = time.getHours();
                    var minutes = time.getMinutes();
                    var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                    var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                    // Add timestamp in hidden input
                    $('#timeline-task-create-block').find('.task_due_date_timestamp').val(full_date_timestamp);
                    // Auto genetare email reminder time
                    if(calculate_reminder_time === true){
                        var new_reminder_date = new Date((full_date_timestamp - (60 * 60 * 3)) * 1000);  // 3 hours ago
                        $('#timeline-task-create-block').find('.task_input__email_reminder_date').datepicker('setDate', new_reminder_date);
                        $('#timeline-task-create-block').find('.task_input__email_reminder_time').timepicker('setTime', new_reminder_date);
                    }
                }else{
                    // Clear hidden timestamp input if no data
                    $('#timeline-task-create-block').find('.task_due_date_timestamp').val('');
                }

                if(reminder_date !== 'Invalid date' && reminder_time !== 'Invalid date'){
                    // Get full date timestamp
                    var date = new Date(reminder_date * 1000);
                    var time = new Date(reminder_time * 1000);
                    var years = date.getFullYear();
                    var months = date.getMonth()+1;
                    var days = date.getDate();
                    var hours = time.getHours();
                    var minutes = time.getMinutes();
                    var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                    var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                    // Add timestamp in hidden input
                    $('#timeline-task-create-block').find('.email_reminder_timestamp').val(full_date_timestamp);
                }else{
                    // Clear hidden timestamp input if no data
                    $('#timeline-task-create-block').find('.email_reminder_timestamp').val('');
                }

            }

            function remove_seconds_from_format(format){
                var new_format = format.replace(':ss', '');
                return new_format;
            }
        }

        function initValidate(){
            $('#conctact-edit-popup-form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {
                $('#contact-phones-repeater').find('.validate-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated)
                    });
                });
            });


            $('#conctact-edit-popup-form')
                .find('[valid="phoneNumber_add"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated);
                    });
                });
            });
        }

        function initPassportInputsMask(){
            Inputmask.extendAliases({
                'english_uppercae': {
                    mask: "e{*}",
                    definitions: {
                        'e': {
                            validator: "[Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m]",
                            casing: "upper"
                        }
                    }
                }
            });
            
            $('.bank-name-input').each(function(){
                $(this).inputmask();
            });
        }
        
        function initExtraDropdownOptionInputs(added_row){
            if (added_row){
                //Init default dropdown value
                var group = $(added_row).find('.extra-dropdown-option')[0];
                var hidden_input_selector = $(group).data('for-input-selector');
                var hidden_input = $(group).find(hidden_input_selector)[0];
                var default_val = $(hidden_input).data('default-value');
                $(hidden_input).val(default_val).removeAttr('data-default-value');
            }
            $('.extra-dropdown-option').each(function(){
                var group = this;
                var hidden_input_selector = $(group).data('for-input-selector');
                var hidden_input = $(group).find(hidden_input_selector)[0];
                var drop_down_btn = $(group).find('button.btn')[0];
                $(group).find('.dropdown-menu a').each(function(){
                    var variant = this;
                    var variant_name = $(this).text();
                    var variant_val = $(this).data('value');
                    $(variant).unbind('click');
                    $(variant).on('click', function(){
                       $(hidden_input).val(variant_val);
                       $(drop_down_btn).text(variant_name);
                    });
                });
            });
        }

        function initPassportPopup() {
            var popup = $('#conctact-add-passport-popup').get()[0];
            $(popup).on('show.bs.modal', function (event) {
                if (event.relatedTarget !== undefined) {
                    clearAllFields();
                    var button = $(event.relatedTarget);
                    if (button.data('passport-data') !== undefined) {
                        $('#add-passport-modal-title').text(GetTranslation('modal_edit_passport_title', 'Edit passport'));
                        $('#modal-add-passport-passport-type').text(GetTranslation('modal_add_passport_passport_type', 'Passport type'));
                        $('#modal-add-passport-first-name').text(GetTranslation('modal_add_passport_first_name', 'First name'));
                        $('#modal-add-passport-last-name').text(GetTranslation('modal_add_passport_last_name', 'Last name'));
                        $('#modal-add-passport-birth-date').text(GetTranslation('modal_add_passport_birth_date', 'Birth date'));
                        $('#modal-add-passport-passport-serial').text(GetTranslation('modal_add_passport_passport_serial', 'Passport serial'));
                        $('#modal-add-passport-limit-date').text(GetTranslation('modal_add_passport_limit_date', 'Limit date'));
                        $('#modal-add-passport-issued-date').text(GetTranslation('modal_add_passport_issued_date', 'Issued date'));
                        $('#modal-add-passport-issued-owner').text(GetTranslation('modal_add_passport_issued_owner', 'Issued Owner'));
                        $('#modal-add-passport-nationality').text(GetTranslation('modal_add_passport_nationality', 'Nationality'));
                        $('#modal-add-passport-country').text(GetTranslation('modal_add_passport_country', 'Country'));
                        $('#modal-add-passport-close').text(GetTranslation('modal_add_passport_close', 'Close'));
                        $('#modal-add-passport-save').text(GetTranslation('modal_add_passport_save', 'Save'));
                        var passport_data = button.data('passport-data');
                        //id
                        if (passport_data.id !== undefined && passport_data.id !== null) {
                            $('#ContactPassport_passport_id').val(passport_data.id);
                        }
                        // Birth date
                        if (passport_data.birth_date !== undefined && passport_data.birth_date !== null) {
                            $("#passport_form_birth_date_datepicker").data('daterangepicker').setStartDate(passport_data.birth_date);
                            $("#passport_form_birth_date_datepicker").data('daterangepicker').setEndDate(passport_data.birth_date);
                        }
                        //Country
                        if (
                            passport_data.country_id !== undefined &&
                            passport_data.country_id !== null &&
                            passport_data.country_name !== undefined &&
                            passport_data.country_name !== null
                        ) {
                            $('#ContactPassport_country').html('<option value="' + passport_data.country_id + '" selected>' + passport_data.country_name + '</option>');
                            $('#ContactPassport_country').selectpicker('val', passport_data.country_id);
                        }
                        //Nationality
                        if (
                            passport_data.nationality_id !== undefined &&
                            passport_data.nationality_id !== null &&
                            passport_data.nationality_name !== undefined &&
                            passport_data.nationality_name !== null
                        ) {
                            $('#ContactPassport_nationality').html('<option value="' + passport_data.nationality_id + '" selected>' + passport_data.nationality_name + '</option>');
                            $('#ContactPassport_nationality').selectpicker('val', passport_data.nationality_id);
                        }
                        //Date limit
                        if (passport_data.date_limit !== undefined && passport_data.date_limit !== null) {
                            var splited_data = passport_data.date_limit.split('.');
                            var date = new Date(splited_data[2] + '-' + splited_data[1] + '-' + splited_data[0]);
                            $('#passport_form_date_limit_datepicker').datepicker('setDate', date);
                        }
                        //First name
                        if (passport_data.first_name !== undefined && passport_data.first_name !== null) {
                            $('#ContactPassport_first_name').val(passport_data.first_name.toUpperCase());
                        }
                        //Last name
                        if (passport_data.last_name !== undefined && passport_data.last_name !== null) {
                            $('#ContactPassport_last_name').val(passport_data.last_name.toUpperCase());
                        }
                        //Issued date
                        if (passport_data.issued_date !== undefined && passport_data.issued_date !== null) {
                            var splited_data = passport_data.issued_date.split('.');
                            var date = new Date(splited_data[2] + '-' + splited_data[1] + '-' + splited_data[0]);
                            $('#passport_form_issued_date_datepicker').datepicker('setDate', date);
                        }
                        //Issued owner
                        if (passport_data.issued_owner !== undefined && passport_data.issued_owner !== null) {
                            $('#passport_form_issued_owner').val(passport_data.issued_owner);
                        }
                        //Serial
                        if (passport_data.serial !== undefined && passport_data.serial !== null) {
                            $('#ContactPassport_serial').val(passport_data.serial);
                        }
                        //Type
                        if (passport_data.type !== undefined && passport_data.type !== null) {
                            $('#ContactPassport_type_id').selectpicker('val', passport_data.type);
                        }
                    } else {
                        $('#add-passport-modal-title').text(GetTranslation('modal_add_passport_title', 'Add passport'));
                        $('#modal-add-passport-passport-type').text(GetTranslation('modal_add_passport_passport_type', 'Passport type'));
                        $('#modal-add-passport-first-name').text(GetTranslation('modal_add_passport_first_name', 'First name'));
                        $('#modal-add-passport-last-name').text(GetTranslation('modal_add_passport_last_name', 'Last name'));
                        $('#modal-add-passport-birth-date').text(GetTranslation('modal_add_passport_birth_date', 'Birth date'));
                        $('#modal-add-passport-passport-serial').text(GetTranslation('modal_add_passport_passport_serial', 'Passport serial'));
                        $('#modal-add-passport-limit-date').text(GetTranslation('modal_add_passport_limit_date', 'Limit date'));
                        $('#modal-add-passport-issued-date').text(GetTranslation('modal_add_passport_issued_date', 'Issued date'));
                        $('#modal-add-passport-issued-owner').text(GetTranslation('modal_add_passport_issued_owner', 'Issued Owner'));
                        $('#modal-add-passport-nationality').text(GetTranslation('modal_add_passport_nationality', 'Nationality'));
                        $('#modal-add-passport-country').text(GetTranslation('modal_add_passport_country', 'Country'));
                        $('#modal-add-passport-close').text(GetTranslation('close_button', 'Close'));
                        $('#modal-add-passport-save').text(GetTranslation('save_changes_button', 'Save'));
                    }
                }
            });
            function clearAllFields() {
                //id
                $('#ContactPassport_passport_id').val('');
                // Birth date
                var date_birthday = $('#contact-info-table-date_of_birth').val();
                var data_date = $('table.table').find('a.m-dropdown__toggle[data-date]').attr('data-date');

                if(typeof data_date !== '') {
                    $("#passport_form_birth_date_datepicker").data('daterangepicker').setStartDate(date_birthday);
                    $("#passport_form_birth_date_datepicker").data('daterangepicker').setEndDate(date_birthday);
                    $("#passport_form_birth_date_datepicker").val(data_date);
                } else {
                    $("#passport_form_birth_date_datepicker").data('daterangepicker').setStartDate(new Date());
                    $("#passport_form_birth_date_datepicker").data('daterangepicker').setEndDate(new Date());
                    $("#passport_form_birth_date_datepicker").val('');
                }
                //Country
                $('#ContactPassport_country').html('');
                $('#ContactPassport_country').selectpicker('val', '');
                //Nationality
                $('#ContactPassport_nationality').html('');
                $('#ContactPassport_nationality').selectpicker('val', '');
                //Date limit
                $('#passport_form_date_limit_datepicker').datepicker("setDate", new Date()).val('');
                //First name
                $('#ContactPassport_first_name').val('');
                //Last name
                $('#ContactPassport_last_name').val('');
                //Issued date
                $('#passport_form_issued_date_datepicker').datepicker("setDate", new Date()).val('');
                //Issued owner
                $('#passport_form_issued_owner').val('');
                //Serial
                $('#ContactPassport_serial').val('');
                //Type
                $('#ContactPassport_type_id').selectpicker('val', '');
            }
        }
        function initVisaPopup(){
            var popup = $('#conctact-add-visa-popup').get()[0];
            $(popup).on('show.bs.modal', function (event) {
                if(event.relatedTarget !== undefined){
                    clearAllFields();
                    var button = $(event.relatedTarget);
                    if (button.data('visa-data') !== undefined) {
                        $('#add-visa-modal-title').text(GetTranslation('modal_edit_visa_title', 'Edit visa'));
                        var visa_data = button.data('visa-data');
                        //id
                        if(visa_data.id !== undefined){
                            $('#ContactVisa_visa_id').val(visa_data.id);
                        }
                        //Begin date
                        if(visa_data.begin_date !== undefined){
                            var splited_data = visa_data.begin_date.split('.');
                            var date = new Date(splited_data[2] + '-' + splited_data[1] + '-' + splited_data[0]);
                            $('#contact_visa_begin_date').datepicker('setDate', date);
                        }
                        //Country
                        if(visa_data.country_id !== undefined && visa_data.country_name !== null){
                            $('#contact_visa_country').html('<option value="' + visa_data.country_id + '" selected>'  + visa_data.country_name + '</option>');
                            $('#contact_visa_country').selectpicker('val', visa_data.country_id);
                        }
                        //Description
                        if(visa_data.description !== undefined){
                            $('#contact_visa_description').val(visa_data.description);
                        }
                        //End date
                        if(visa_data.end_date !== undefined){
                            var splited_data = visa_data.end_date.split('.');
                            var date = new Date(splited_data[2] + '-' + splited_data[1] + '-' + splited_data[0]);
                            $('#contact_visa_end_date').datepicker('setDate', date);
                        }
                        //Type
                        if(visa_data.type !== undefined){
                            $('#contact_visa_type').selectpicker('val', visa_data.type);
                        }
                    }else{
                        $('#add-visa-modal-title').text(GetTranslation('modal_add_visa_title', 'Add visa'));
                    }
                }
            });
            function clearAllFields(){
                //id
                $('#ContactVisa_visa_id').val('');
                //Begin date
                $('#contact_visa_begin_date').datepicker("setDate", new Date()).val('');
                //Country
                $('#contact_visa_country').selectpicker('val', '');
                $('#contact_visa_country').html('');
                //Description
                $('#contact_visa_description').val('');
                //End date
                $('#contact_visa_end_date').datepicker("setDate", new Date()).val('');
                //Type
                $('#contact_visa_type').selectpicker('val', '');
            }
        }
        
        function initDeleteForm(){
            var form = $('#contact-view-delete-form').get()[0];
            $(form).on('submit', function (event) {
                if(!$(form).data('confirmed')){
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('contact_delete_question', 'Delete this contact?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if(result.value === true){
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }
        
        function initDeletePassportForm(){
            $('.delete-passport_form').on('submit', function (event) {
                var form = this;
                if(!$(form).data('confirmed')){
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('passport_delete_question', 'Delete this passport?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if(result.value === true){
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }
        function initDeleteVisaForm(){
            $('.delete-visa_form').on('submit', function (event) {
                var form = this;
                if(!$(form).data('confirmed')){
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('visa_delete_question', 'Delete this visa?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if(result.value === true){
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }
        function initContactRelationsForm(){
            // Contact relation select
            var contact_relation_type_select = $('#relation_contact_type_select').get()[0];
            var contact_relation_contact_select = $('#relation_contact_search_select').get()[0];
            var submit = $('#add_contact_relation_submit').get()[0];
            
            $(contact_relation_contact_select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            var full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        };
                    }
                },
                language: tenant_lang,
                dropdownParent: $(contact_relation_contact_select).parent(),
                width: '100%',
                placeholder: GetTranslation("contact_relation_contact_select_placeholder", "Select contact")
            });
            $(contact_relation_contact_select).on('change', function(){
                checkSubmitStatus();
            });
            $(contact_relation_type_select).on('change', function(){
                checkSubmitStatus();
            });
            function checkSubmitStatus(){
                var relation_type = $(contact_relation_type_select).val();
                var relation_contact = $(contact_relation_contact_select).val();
                if(relation_type !== undefined && relation_type !== null && relation_type !== '' && relation_contact !== undefined && relation_contact !== null && relation_contact !== ''){
                    submit.disabled = false;
                    $(submit).removeClass('disabled');
                } else {
                    submit.disabled = true;
                    $(submit).addClass('disabled');
                }
            }
        }
        
        function initContactsManualMerge(){
            var contact_for_merge_select = $('#contact_merge_object_select').get()[0];
            $(contact_for_merge_select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            var full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        };
                    }
                },
                language: tenant_lang,
                dropdownParent: $(contact_for_merge_select).parent(),
                width: '100%',
                placeholder: GetTranslation("manual_contacts_merge_select_placeholder", "Contact search")
            });
            $(contact_for_merge_select).on('change', function(){
                var changed_contact_id = $(contact_for_merge_select).val();
                var changed_contact_name = $(contact_for_merge_select).find('option[value="'+changed_contact_id+'"]').text();
                $('#merge-block .merge-contacts-names .additional-contact-name').text(changed_contact_name);
                $('#merge-block .merge-contacts-names').stop().slideDown(400);
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success('Контакт для объединения был выбран');
            });
        }
    });
})(jQuery);