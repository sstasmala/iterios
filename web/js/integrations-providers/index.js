(function($){
    $(document).ready(function(){
        
        initPageWidgets();
        initAllIntegrations();
        initMyIntegrations();
        
        function initPagination(selector, count_all, per_page, page) {
            per_page = 12;

            $(selector).find('ul.m-datatable__pager-nav').empty();

            if (count_all > per_page) {
                var last_page = Math.ceil(count_all / per_page);
                var template_prev = '<li>' +
                    '<a title="'+GetTranslation('pagination_first')+'" class="m-datatable__pager-link m-datatable__pager-link--first" data-page="1">\n' +
                    '<i class="la la-angle-double-left"></i>\n' +
                    '</a>\n' +
                    '</li>\n' +
                    '<li>\n' +
                    '<a title="'+GetTranslation('pagination_previous')+'" class="m-datatable__pager-link m-datatable__pager-link--prev" data-page="'+(page - 1)+'">\n' +
                    '<i class="la la-angle-left"></i>\n' +
                    '</a>\n' +
                    '</li>';
                var template_last = '<li>\n' +
                    '<a title="'+GetTranslation('pagination_next')+'" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="'+(page + 1)+'">\n' +
                    '<i class="la la-angle-right"></i>\n' +
                    '</a>\n' +
                    '</li>\n' +
                    '<li>\n' +
                    '<a title="'+GetTranslation('pagination_last')+'" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="'+last_page+'">\n' +
                    '<i class="la la-angle-double-right"></i>\n' +
                    '</a>\n' +
                    '</li>';

                if (page != 1)
                    $(selector).find('ul.m-datatable__pager-nav').append(template_prev);

                for (var i = 0; i < last_page; i++) {
                    var page_template = '<li>\n' +
                        '<a class="m-datatable__pager-link m-datatable__pager-link-number'+((i+1) == page ? ' m-datatable__pager-link--active' : '')+'" data-page="'+(i+1)+'" title="'+(i+1)+'">'+(i+1)+'</a>\n' +
                        '</li>';

                    $(selector).find('ul.m-datatable__pager-nav').append(page_template);
                }

                if (page != last_page)
                    $(selector).find('ul.m-datatable__pager-nav').append(template_last);

                $(selector).show();

                $(selector).off('click').on('click', 'a.m-datatable__pager-link', function (e) {
                    var page = $(this).data('page');

                    if ($(this).hasClass('m-datatable__pager-link-number'))
                        $(this).addClass('m-datatable__pager-link--active');

                    getProviders({integrations: 'all', page: page});
                });
            } else {
                $(selector).hide();
            }
        }
        
        function getProviders(data) {
            if (data.integrations == 'all') {
                mApp.block('#ilp_tab_1_2 .items-wrap', {
                    // overlayColor: "#000000",
                    // type: "loader",
                    // state: "success",
                    message: GetTranslation('datatable_data_processiong')
                });
            } else {
                mApp.block('#ilp_tab_1_1 .items-wrap', {
                    // overlayColor: "#000000",
                    // type: "loader",
                    // state: "success",
                    message: GetTranslation('datatable_data_processiong')
                });
            }

            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/integrations-providers/get-data',
                dataType: 'json',
                type: 'POST',
                data: {
                    _csrf: $('meta[name="csrf-token"]').attr('content'),
                    providers: data
                },
                success: function (res) {
                    if (data.integrations == 'all') {
                        var selector = $('#ilp_tab_1_2');
                    } else {
                        var selector = $('#ilp_tab_1_1');
                    }

                    $(selector).find('.items-wrap').empty();

                    if ($.isEmptyObject(res.providers)) {
                        var empty_div = '<div class="text-center" style="width: 100%;padding: 20px;">'+
                            GetTranslation('datatable_records_not_found') +
                            '. <a id="ilp_show_all" href="#">'+GetTranslation('datatable_show_all_message')+'</a>'
                            +'</div>';

                        $(selector).find('.items-wrap').append(empty_div);
                        $(selector).find('.paginator-wrap').hide();
                    } else {
                        $.each(res.providers, function (key, provider) {
                            var fields = $($(selector).find('.demo-items-wrap').html());

                            if (provider.logo.length > 0) {
                                $(fields).find('img').attr('alt', provider.name).attr('src', baseUrl + '/' + provider.logo);
                            } else {
                                $(fields).find('img').replaceWith('<h2>' + provider.name + '</h2>');
                            }

                            $(fields).find('a').data('intergation-id', provider.id);
                            $(fields).find('.description-block div span').text(provider.desc);

                            $(fields).show();

                            $(selector).find('.items-wrap').append(fields);
                        });

                        initPagination($(selector).find('.paginator-wrap'), res.count, 12, data.page);
                    }

                    if (data.integrations == 'all') {
                        mApp.unblock('#ilp_tab_1_2 .items-wrap');
                    } else {
                        mApp.unblock('#ilp_tab_1_1 .items-wrap');
                    }

                },
                error: function () {
                    alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                }
            });
        }

        function initAllIntegrations() {
            getProviders({integrations: 'all', page: 1});
        }

        function initMyIntegrations() {
            getProviders({integrations: 'my', page: 1});
        }
        
        function initPageWidgets(){
            $('#ilp_filter_type').on('change', function (e) {
                getProviders({integrations: 'all', type: $(this).val(), search_text: $('#ilp_filter_search').val(), page: 1});
            });

            $('#ilp_filter_search').on('keydown', function (e) {
                var search = $(this).val();

                if (e.which == 13) {
                    getProviders({integrations: 'all', type: $('#ilp_filter_type').val(), search_text: search, page: 1});
                }
            });

            $('#ilp_tab_1_2 .items-wrap').on('click', 'a#ilp_show_all', function (e) {
                $('#ilp_filter_search').val('');
                $('#ilp_filter_type').val(null).trigger('change');

                getProviders({integrations: 'all', page: 1});
            });

            $('#ilp_tab_1_1 .items-wrap').on('click', 'a#ilp_show_all', function (e) {
                getProviders({integrations: 'my', page: 1});
            });

            $('#ilp_tab_1_2 .items-wrap, #ilp_tab_1_1 .items-wrap').on('click', 'a.m-link', function (e) {
                var provider_id = $(this).data('intergation-id');

                $('#integration_details_modal .btn-primary').data('provider_id', provider_id);

                if (provider_id) {
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/integrations-providers/get-provider',
                        dataType: 'json',
                        type: 'GET',
                        data: {
                            id: provider_id
                        },
                        success: function (res) {
                            $('#integration_details_modal').find('#integrationDetailsModalLabel').text(res.name);

                            if (res.logo.length > 0) {
                                $('#integration_details_modal .modal-body .description img').attr('alt', res.name).attr('src', baseUrl + '/' + res.logo).show();
                            } else {
                                $('#integration_details_modal .modal-body .description img').hide();
                            }

                            $('#integration_details_modal .modal-body .description span').text(res.description);

                            $('#integration_details_modal .modal-body ul.features').empty();

                            if ($.isEmptyObject(res.functions)) {
                                $($('#integration_details_modal .modal-body hr').get(0)).hide();
                                $('#integration_details_modal .modal-body ul.features').closest('div.row').hide();
                            } else {
                                $($('#integration_details_modal .modal-body hr').get(0)).show();
                                $('#integration_details_modal .modal-body ul.features').closest('div.row').show();

                                $.each(res.functions, function (key, func) {
                                    var template = '<li class="feature-item">' +
                                        '<i class="fa fa-check m--font-success mr-1"></i>' +
                                        '<span class="m--font-bold">' +
                                        (func.value != null ? func.value : GetTranslation('not_set')) +
                                        '</span>' +
                                        '</li>';

                                    $('#integration_details_modal .modal-body ul.features').append(template);
                                });
                            }

                            var credentials = (res.credentials != undefined ? res.credentials : {});

                            $('#integration_details_modal #provider-credentials-repeater').find('div.col-12').empty();

                            if (res.authorization_type == 'api-token') {
                                var input_template = '<input type="text" class="form-control m-input" name="apiKey" value="'+(credentials.hasOwnProperty('apiKey') ? credentials.apiKey : '')+'" placeholder="'+GetTranslation('ip_inx_api_key_placeholder')+'">' +
                                    '<span class="m-form__help">'+GetTranslation('ip_inx_api_key')+'</span>';

                                $('#integration_details_modal #provider-credentials-repeater').find('div.col-12').append(input_template);
                            } else {
                                var two_inputs_template = '<div class="input-group double-inputs-customize">\n' +
                                    '<div class="input-group-prepend">\n' +
                                    '<input type="text" class="form-control m-input" name="username" value="'+(credentials.hasOwnProperty('username') ? credentials.username : '')+'" placeholder="'+GetTranslation('ip_inx_username_placeholder')+'">\n' +
                                    '</div>\n' +
                                    '<input type="text" class="form-control m-input" name="password" value="'+(credentials.hasOwnProperty('password') ? credentials.password : '')+'" placeholder="'+GetTranslation('ip_inx_password_placeholder')+'">\n' +
                                    '</div>' +
                                    '<div class="input-group double-inputs-customize"><div class="input-group-prepend"><span class="m-form__help">'+GetTranslation('ip_inx_username')+'</span></div><span class="m-form__help">'+GetTranslation('ip_inx_password')+'</span></div>';

                                $('#integration_details_modal #provider-credentials-repeater').find('div.col-12').append(two_inputs_template);
                            }

                            $('#integration_details_modal #provider-additional-config').find('div.config').empty();

                            if (res.additional_config != null) {
                                var config = $.parseJSON(res.additional_config);

                                $.each(config, function (i, value) {
                                    var input_template = '<div class="col-12 mb-3"><input type="text" class="form-control m-input" name="'+value+'" value="'+(credentials.hasOwnProperty(value) ? credentials[value] : '')+'" placeholder="'+GetTranslation('enter')+' '+GetTranslation('ip_inx_'+value)+'">' +
                                        '<span class="m-form__help">'+GetTranslation('ip_inx_'+value)+'</span></div>';

                                    $('#integration_details_modal #provider-additional-config').find('div.config').append(input_template);
                                });

                                $($('#integration_details_modal .modal-body hr').get(3)).show();
                                $('#integration_details_modal .modal-body div#provider-additional-config').closest('div.row').show();
                            } else {
                                $($('#integration_details_modal .modal-body hr').get(3)).hide();
                                $('#integration_details_modal .modal-body div#provider-additional-config').closest('div.row').hide();
                            }
                        },
                        error: function () {
                            alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                        }
                    });
                }
            });

            $('div#integration_details_modal .btn-primary').click(function (e) {
                var provider_id = $(this).data('provider_id');
                var auth = $('#integration_details_modal form#integration-detail-form').serializeArray();
                var config = $('#integration_details_modal form#additional-config-form').serializeArray();

                var btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/integrations-providers/save-provider?id='+provider_id,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        auth: auth,
                        config: config
                    },
                    success: function (res) {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (res) {
                            $('#integration_details_modal').modal('hide');

                            location.reload();
                        }
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                    }
                });
            });
        }
    });
})(jQuery);