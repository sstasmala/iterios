(function($){
    $(document).ready(function(){
        
        initTasksDataTable();

        $('body').find('form').attr('autocomplete', 'off');

        function initTasksDataTable(){
            var datatable = $('#roles_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/roles/get-data',
                            method: 'GET'
                        }
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        $(row).find('.delete-role').on('click', function (e) {
                            e.preventDefault();

                            var link = $(this).attr('href');

                            if (data.users > 0) {
                                swal(GetTranslation('roles_delete_oops'), GetTranslation('roles_delete_notification_error'), 'error');
                            } else {
                                swal({
                                    title: GetTranslation('delete_question'),
                                    text: GetTranslation('roles_delete_notification'),
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: GetTranslation('delete_confirmation')
                                }).then(function(result) {
                                    if (result.value)
                                        window.location.href = link;
                                });
                            }
                        });
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: [
                    {
                        field: 'id',
                        title: '#',
                        sortable: false,
                        width: 40,
                        textAlign: 'center',
                        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                    },
                    {
                        field: 'name',
                        title: GetTranslation('roles_table_name_column_title', 'Name'),
                        template: function(data){
                            return '<a href="'+baseUrl+'/ita/roles/update?name=' + data.id + '" class="m-link">' + data.name + '</a>';
                        }
                    },
                    {
                        field: 'users',
                        title: GetTranslation('roles_table_employees_column_title', 'Employees'),
                        width: 100,
                        textAlign: 'center',
                        sortable: true,
                        template: function(data){
                            var output = data.users;
                            return output;
                        }
                    },
                    {
                        field: 'Actions',
                        title: GetTranslation('roles_table_actions_column_title', 'Actions'),
                        sortable: false,
                        textAlign: 'right',
                        width: 100,
                        template: function (row, index, datatable) {
                            var template = '<a href="'+baseUrl+'/ita/roles/update?name=' + row.id + '" class="edit-role m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('edit', 'Edit') + '"><i class="la la-edit"></i></a>\
                                            <a href="'+baseUrl+'/ita/roles/delete?name=' + row.id + '" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';

                            return template;
                        }
                    }
                ],
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
        }
    });
})(jQuery);