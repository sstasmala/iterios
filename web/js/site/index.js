/**
 * site/index.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

var geo_country;
var data_country;
//== Class Definition
var SnippetLogin = function() {
    var login = $('#m_login');
    var getParams = function() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            vars[key] = value;
        });
        return vars;
    };

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    };

    //== Private Functions

    var displaySignUpForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        login.find('.m-login__signup').animateClass('flipInX animated');
    };

    var displaySignInForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        login.find('.m-login__signin').animateClass('flipInX animated');
    };

    var displayForgetPasswordForm = function() {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    };

    var handleFormSwitch = function() {
        $('#m_login_forget_password').click(function(e) {
            e.preventDefault();
            displayForgetPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#m_login_signup').click(function(e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#m_login_signup_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });
    };

    var handleSignInFormSubmit = function() {
        $('#m_login_signin_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/login',
                data: {
                    redirect_url: getParams()['redirect_url'] ? getParams()['redirect_url'] : 0
                },
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.login) {
                            location.href = (!response.redirect_url ? baseUrl + '/ita' : baseUrl + response.redirect_url);
                        } else {
                            showErrorMsg(form, 'danger', response.error);
                        }
                    }, 1000);
                }
            });
        });
    };

    var handleSignUpFormSubmit = function() {
        $('#m_login_signup_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    company: {
                        required: true
                    },
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        minlength: 6,
                        equalTo: '#password'
                    },
                    agree: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            var language = window.navigator ? (window.navigator.language ||
                window.navigator.systemLanguage ||
                window.navigator.userLanguage) : 'en';
            language = language.substr(0, 2).toLowerCase();

            form.find('input[name="language"]').val(language);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/sign-up',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        form.clearForm();
                        form.validate().resetForm();

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.m-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        if (response.user_id)
                            showErrorMsg(signInForm, 'success', GetTranslation('complete_registration'));

                        if (response.error)
                            showErrorMsg(signInForm, 'danger', response.error);
                    }, 1000);
                }
            });
        });
    };

    var handleForgetPasswordFormSubmit = function() {
        $('#m_login_forget_password_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/forgot-password',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.m-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        if (response.success)
                            showErrorMsg(signInForm, 'success', GetTranslation('ft_pass_success'));

                        if (response.error)
                            showErrorMsg(signInForm, 'danger', response.error);
                    }, 1000);
                }
            });
        });
    };

    var handleChangePasswordFormSubmit = function() {
        $('#m_login_change_password_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        minlength: 6,
                        equalTo: '#password'
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            var token = form.find('input[name="token"]').val();
            token = (token.length > 0 ? token : '');

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/change-password?token='+token,
                success: function(response, status, xhr, $form) {
                    if (response.success)
                        showErrorMsg(form, 'success', GetTranslation('change_pass_success'));

                    if (response.error)
                        showErrorMsg(form, 'danger', response.error);

                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        location.href = baseUrl;
                    }, 2000);
                }
            });
        });
    };

    var handleCreateTenantFormSubmit = function() {
        $('#m_login_create_tenant_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    company: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            var language = window.navigator ? (window.navigator.language ||
                window.navigator.systemLanguage ||
                window.navigator.userLanguage) : 'en';
            language = language.substr(0, 2).toLowerCase();
            form.find('input[name="language"]').val(language);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/create-tenant',
                success: function(response, status, xhr, $form) {
                    if (response.success) {
                        showErrorMsg(form, 'success', GetTranslation('create_company_success'));

                        // similate 2s delay
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove
                            form.clearForm(); // clear form
                            form.validate().resetForm(); // reset validation states

                            location.href = baseUrl + '/ita';
                        }, 2000);
                    }

                    if (response.error)
                        location.href = baseUrl + '/';
                }
            });
        });
    };

    //== Public Functions
    return {
        // public functions
        init: function() {
            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgetPasswordFormSubmit();
            handleChangePasswordFormSubmit();
            handleCreateTenantFormSubmit();
        }
    };
}();


//== Class Initialization
jQuery(document).ready(function() {

        // $.getJSON('http://www.geoplugin.net/json.gp?jsoncallback=?', function(data) {
        //     geo_country = JSON.stringify(data, null, 2);
        //     var country_name = JSON.parse(geo_country);
            $.ajax(
                {
                    type: "POST",
                    dataType: 'json',
                    url: baseUrl + '/site/get-country?title=Ukraine',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data){
                        if ($('#account_country_select').find("option[value='" + data[0].id + "']").length) {
                            $('#account_country_select').val(data[0].id).trigger('change');
                        } else {
                            // Create a DOM Option and pre-select by default
                            var newOption = new Option(data[0].title, data[0].id, true, true);
                            // Append it to the select
                            $('#account_country_select').append(newOption).trigger('change');
                        }
                        $('input[name="country"]').val(data[0].id);
                    }
                });
            SnippetLogin.init();
            var country_select = $('#account_country_select').get()[0];
            $(country_select).select2({
                ajax: {
                    url: baseUrl + '/site/get-countries-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['title']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("mcr_countries_select_placeholder", "Select country");
                    }
                    return data.text;
                },
                minimumInputLength: 2,
                width: '100%',
                dropdownParent: $(country_select).parent(),
                placeholder: GetTranslation("mcr_countries_select_placeholder", "Select country")
            });
        // });
});