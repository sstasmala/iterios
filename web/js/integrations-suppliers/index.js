(function($){
    $(document).ready(function(){
        
        initPageWidgets();
        initAllIntegrations();
        initMyIntegrations();

        function initPagination(selector, count_all, per_page, page) {
            per_page = 12;

            $(selector).find('ul.m-datatable__pager-nav').empty();

            if (count_all > per_page) {
                var last_page = Math.ceil(count_all / per_page);
                var template_prev = '<li>' +
                    '<a title="'+GetTranslation('pagination_first')+'" class="m-datatable__pager-link m-datatable__pager-link--first" data-page="1">\n' +
                    '<i class="la la-angle-double-left"></i>\n' +
                    '</a>\n' +
                    '</li>\n' +
                    '<li>\n' +
                    '<a title="'+GetTranslation('pagination_previous')+'" class="m-datatable__pager-link m-datatable__pager-link--prev" data-page="'+(page - 1)+'">\n' +
                    '<i class="la la-angle-left"></i>\n' +
                    '</a>\n' +
                    '</li>';
                var template_last = '<li>\n' +
                    '<a title="'+GetTranslation('pagination_next')+'" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="'+(page + 1)+'">\n' +
                    '<i class="la la-angle-right"></i>\n' +
                    '</a>\n' +
                    '</li>\n' +
                    '<li>\n' +
                    '<a title="'+GetTranslation('pagination_last')+'" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="'+last_page+'">\n' +
                    '<i class="la la-angle-double-right"></i>\n' +
                    '</a>\n' +
                    '</li>';

                if (page != 1)
                    $(selector).find('ul.m-datatable__pager-nav').append(template_prev);

                for (var i = 0; i < last_page; i++) {
                    var page_template = '<li>\n' +
                        '<a class="m-datatable__pager-link m-datatable__pager-link-number'+((i+1) == page ? ' m-datatable__pager-link--active' : '')+'" data-page="'+(i+1)+'" title="'+(i+1)+'">'+(i+1)+'</a>\n' +
                        '</li>';

                    $(selector).find('ul.m-datatable__pager-nav').append(page_template);
                }

                if (page != last_page)
                    $(selector).find('ul.m-datatable__pager-nav').append(template_last);

                $(selector).show();

                $(selector).off('click').on('click', 'a.m-datatable__pager-link', function (e) {
                    var page = $(this).data('page');

                    if ($(this).hasClass('m-datatable__pager-link-number'))
                        $(this).addClass('m-datatable__pager-link--active');

                    getSuppliers({integrations: 'all', page: page});
                });
            } else {
                $(selector).hide();
            }
        }

        function getSuppliers(data) {
            if (data.integrations == 'all') {
                mApp.block('#ils_tab_1_2 .items-wrap', {
                    // overlayColor: "#000000",
                    // type: "loader",
                    // state: "success",
                    message: GetTranslation('datatable_data_processiong')
                });
            } else {
                mApp.block('#ils_tab_1_1 .items-wrap', {
                    // overlayColor: "#000000",
                    // type: "loader",
                    // state: "success",
                    message: GetTranslation('datatable_data_processiong')
                });
            }

            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/integrations-suppliers/get-data',
                dataType: 'json',
                type: 'POST',
                data: {
                    _csrf: $('meta[name="csrf-token"]').attr('content'),
                    suppliers: data
                },
                success: function (res) {
                    if (data.integrations == 'all') {
                        var selector = $('#ils_tab_1_2');
                    } else {
                        var selector = $('#ils_tab_1_1');
                    }

                    $(selector).find('.items-wrap').empty();

                    if ($.isEmptyObject(res.suppliers)) {
                        var empty_div = '<div class="text-center" style="width: 100%;padding: 20px;">'+
                            GetTranslation('datatable_records_not_found') +
                            '. <a id="ils_show_all" href="#">'+GetTranslation('datatable_show_all_message')+'</a>'
                            +'</div>';

                        $(selector).find('.items-wrap').append(empty_div);
                        $(selector).find('.paginator-wrap').hide();
                    } else {
                        $.each(res.suppliers, function (key, supplier) {
                            var fields = $($(selector).find('.demo-items-wrap').html());

                            if (supplier.logo.length > 0) {
                                $(fields).find('img').attr('alt', supplier.name).attr('src', baseUrl + '/' + supplier.logo);
                            } else {
                                $(fields).find('img').replaceWith('<h2>' + supplier.name + '</h2>');
                            }

                            $(fields).find('a.connect-link').data('intergation-id', supplier.id);

                            if (data.integrations == 'all') {
                                $(fields).find('.description-block tr.country div').removeClass('ua').addClass(supplier.country.iso);
                                $(fields).find('.description-block tr.country span.country').text(supplier.country.title);
                                $(fields).find('.description-block tr.code span').text(supplier.code);
                                $(fields).find('.description-block tr.company span').text(supplier.company);
                                $(fields).find('.description-block tr.website a').text(supplier.web_site).attr('href', (supplier.web_site != null && supplier.web_site.slice(0, 4) == 'http' ? supplier.web_site : 'http://' + supplier.web_site));
                            } else {
                                $(fields).find('.description-block table.cred tbody').empty();

                                if (!$.isEmptyObject(supplier.credentials)) {
                                    if (supplier.auth_type == 'api-token') {
                                        var templ = '<tr><td>' + GetTranslation('is_inx_api_key') + ':</td>\n' +
                                            '<td>' + ('apiKey' in supplier.credentials[0] ? supplier.credentials[0].apiKey : '') + '</td></tr>';

                                        $(fields).find('.description-block table.cred tbody').append(templ);
                                    } else {
                                        var templ = '<tr><td>' + GetTranslation('is_inx_username') + ':</td>\n' +
                                            '<td>' + ('username' in supplier.credentials[0] ? supplier.credentials[0].username : '') + '</td></tr>' +
                                            '<tr><td>' + GetTranslation('is_inx_password') + ':</td>\n' +
                                            '<td>' + ('password' in supplier.credentials[0] ? supplier.credentials[0].password : '') + '</td></tr>';

                                        $(fields).find('.description-block table.cred tbody').append(templ);
                                    }

                                    $(fields).find('.description-block table.mt-2').hide();
                                    $(fields).find('.description-block table.mt-2 div.dropdown-menu').empty();

                                    $.each(supplier.credentials, function (key, cred) {
                                        if (key == 0)
                                            return;

                                        var templ = '';

                                        if (supplier.auth_type == 'api-token') {
                                            templ = '<table><tbody><tr><td>' + GetTranslation('is_inx_api_key') + ':</td>\n' +
                                                '<td>' + ('apiKey' in cred ? cred.apiKey : '') + '</td></tr></tbody></table>';
                                        } else {
                                            templ = '<table><tbody><tr><td>' + GetTranslation('is_inx_username') + ':</td>\n' +
                                                '<td>' + ('username' in cred ? cred.username : '') + '</td></tr>' +
                                                '<tr><td>' + GetTranslation('is_inx_password') + ':</td>\n' +
                                                '<td>' + ('password' in cred ? cred.password : '') + '</td></tr></tbody></table>';
                                        }

                                        templ = '<div class="dropdown-item">' + templ + '</div>' + (supplier.credentials.length != (key + 1) ? '<div class="dropdown-divider"></div>' : '');

                                        $(fields).find('.description-block table.mt-2 div.dropdown-menu').append(templ);

                                        $(fields).find('.description-block table.mt-2').show();
                                    });
                                }
                            }

                            $(fields).show();

                            $(selector).find('.items-wrap').append(fields);
                        });

                        initPagination($(selector).find('.paginator-wrap'), res.count, 12, data.page);
                    }

                    if (data.integrations == 'all') {
                        mApp.unblock('#ils_tab_1_2 .items-wrap');
                    } else {
                        mApp.unblock('#ils_tab_1_1 .items-wrap');
                    }

                },
                error: function () {
                    alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                }
            });
        }

        function initAllIntegrations() {
            getSuppliers({integrations: 'all', page: 1});
        }

        function initMyIntegrations() {
            getSuppliers({integrations: 'my', page: 1});
        }

        function initRepeater() {
            // Init suppliers credentials repeater
            $('form#integration-detail-form').find('#supplier-credentials-repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'text-input': 'foo'
                },
                show: function () {
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    swal({
                        title: GetTranslation('delete_question','Are you sure?'),
                        text: GetTranslation('delete_element_confirmation_text','Are you sure you want to delete this element?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                        reverseButtons: true
                    }).then(function(result){
                        if(result.value){
                            $(this).slideUp(deleteElement);
                        }
                    });
                }
            });
        }
        
        function initPageWidgets() {
            // Stop close dropdown if user click it
            $('.dropdown-menu.click-prevented').on('click', function(event){
                event.stopPropagation();
            });

            $('#ils_filter_type').on('change', function (e) {
                getSuppliers({integrations: 'all', type: $(this).val(), search_text: $('#ils_filter_search').val(), page: 1});
            });

            $('#ils_filter_search').on('keydown', function (e) {
                var search = $(this).val();

                if (e.which == 13) {
                    getSuppliers({integrations: 'all', type: $('#ils_filter_type').val(), search_text: search, page: 1});
                }
            });

            $('#ils_tab_1_2 .items-wrap').on('click', 'a#ils_show_all', function (e) {
                $('#ils_filter_search').val('');
                $('#ils_filter_type').val(null).trigger('change');

                getSuppliers({integrations: 'all', page: 1});
            });

            $('#ils_tab_1_1 .items-wrap').on('click', 'a#ils_show_all', function (e) {
                getSuppliers({integrations: 'my', page: 1});
            });

            $('#ils_tab_1_2 .items-wrap, #ils_tab_1_1 .items-wrap').on('click', 'a.m-link', function (e) {
                var supplier_id = $(this).data('intergation-id');

                $('#integration_details_modal .btn-primary').data('supplier_id', supplier_id);

                function credentialsTemplate(auth_type, cred) {
                    var repeater_template = '',
                        input_template = '';

                    if (auth_type == 'api-token') {
                        input_template = '<div class="col-12 mb-3" data-repeater-item>' +
                            '<div class="input-group double-inputs-customize"><input type="text" class="form-control m-input" name="apiKey" value="' + (cred.hasOwnProperty('apiKey') ? cred.apiKey : '') + '" placeholder="' + GetTranslation('is_inx_api_key_placeholder') + '">' +
                            '<div class="input-group-append"><a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon"><i class="la la-close"></i></a></div>' +
                            '</div><span class="m-form__help">'+GetTranslation('is_inx_api_key')+'</span></div>';
                    } else {
                        input_template = '<div class="col-12 mb-3" data-repeater-item><div class="input-group double-inputs-customize">\n' +
                            '<div class="input-group-prepend">\n' +
                            '<input type="text" class="form-control m-input" name="username" value="' + (cred.hasOwnProperty('username') ? cred.username : '') + '" placeholder="' + GetTranslation('is_inx_username_placeholder') + '">\n' +
                            '</div>\n' +
                            '<input type="text" class="form-control m-input" name="password" value="' + (cred.hasOwnProperty('password') ? cred.password : '') + '" placeholder="' + GetTranslation('is_inx_password_placeholder') + '">\n' +
                            '<div class="input-group-append"><a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon"><i class="la la-close"></i></a></div>' +
                            '</div>' +
                            '<div class="input-group double-inputs-customize"><div class="input-group-prepend" style="width: 45%;"><span class="m-form__help">'+GetTranslation('is_inx_username')+'</span></div><span class="m-form__help">'+GetTranslation('is_inx_password')+'</span></div></div>';
                    }

                    if ($('#integration_details_modal form#integration-detail-form').find('#supplier-credentials-repeater').length > 0) {
                        $('#integration_details_modal form#integration-detail-form').find('#supplier-credentials-repeater div.cred').append(input_template);
                    } else {
                        repeater_template = '<div id="supplier-credentials-repeater">\n' +
                            '<div data-repeater-list="Credentials" class="row cred">\n' +
                            input_template +
                            '</div>\n' +
                            '<div class="row">\n' +
                            '<div class="col-12">\n' +
                            '<span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;' + GetTranslation('add', 'Add') + '</span>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>';

                        $('#integration_details_modal form#integration-detail-form').append(repeater_template);

                        initRepeater();
                    }
                }

                if (supplier_id) {
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/integrations-suppliers/get-supplier',
                        dataType: 'json',
                        type: 'GET',
                        data: {
                            id: supplier_id
                        },
                        success: function (res) {
                            $('#integration_details_modal').find('#integrationDetailsModalLabel').text(res.name);

                            if (res.logo.length > 0) {
                                $('#integration_details_modal .modal-body .description img').attr('alt', res.name).attr('src', baseUrl + '/' + res.logo).show();
                            } else {
                                $('#integration_details_modal .modal-body .description img').hide();
                            }

                            $('#integration_details_modal .modal-body .description span').text(res.description);

                            $('#integration_details_modal .modal-body ul.features').empty();

                            if ($.isEmptyObject(res.functions)) {
                                $($('#integration_details_modal .modal-body hr').get(0)).hide();
                                $('#integration_details_modal .modal-body ul.features').closest('div.row').hide();
                            } else {
                                $($('#integration_details_modal .modal-body hr').get(0)).show();
                                $('#integration_details_modal .modal-body ul.features').closest('div.row').show();

                                $.each(res.functions, function (key, func) {
                                    var template = '<li class="feature-item">' +
                                        '<i class="fa fa-check m--font-success mr-1"></i>' +
                                        '<span class="m--font-bold">' +
                                        (func.value != null ? func.value : GetTranslation('not_set')) +
                                        '</span>' +
                                        '</li>';

                                    $('#integration_details_modal .modal-body ul.features').append(template);
                                });
                            }

                            $('#integration_details_modal .modal-body div.stats-block').find('tr.country div').removeClass('ua').addClass(res.country.iso);
                            $('#integration_details_modal .modal-body div.stats-block').find('tr.country span.country').text(res.country.title);
                            $('#integration_details_modal .modal-body div.stats-block').find('tr.code span').text(res.code);
                            $('#integration_details_modal .modal-body div.stats-block').find('tr.company span').text(res.company);
                            $('#integration_details_modal .modal-body div.stats-block').find('tr.website a').text(res.web_site).attr('href', (res.web_site != null && res.web_site.slice(0, 4) == 'http' ? res.web_site : 'http://' + res.web_site));

                            $('#integration_details_modal #supplier-credentials-repeater').remove();

                            if (res.authorization_type) {
                                if (!$.isEmptyObject(res.credentials)) {
                                    var credentials = (!$.isEmptyObject(res.credentials) ? res.credentials : {});

                                    $.each(credentials, function (key, cred) {
                                        credentialsTemplate(res.authorization_type, cred);
                                    });
                                } else {
                                    credentialsTemplate(res.authorization_type, {})
                                }
                            } else {
                                $($('#integration_details_modal .modal-body hr').get(2)).hide();
                                $('#integration_details_modal .modal-body #integration-detail-form').closest('div.row').hide();
                            }
                        },
                        error: function () {
                            alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                        }
                    });
                }
            });

            $('div#integration_details_modal .btn-primary').click(function (e) {
                var supplier_id = $(this).data('supplier_id');
                var form = $('#integration_details_modal form#integration-detail-form');

                var btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    url: baseUrl + '/ita/' + tenantId + '/integrations-suppliers/save-supplier?id='+supplier_id,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (res) {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (res) {
                            $('#integration_details_modal').modal('hide');

                            location.reload();
                        }
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                    }
                });
            });
        }
    });
})(jQuery);