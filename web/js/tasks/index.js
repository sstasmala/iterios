var datatable = null;

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}
(function($){



    $(document).ready(function(){
        initTasksDataTable();
        initPageWidgets();
        taskModalCore('#add-task-popup');
        taskModalCore('#edit-task-popup');
        
        function initTasksDataTable(){
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'title',
                    title: GetTranslation('tasks_datatable_column_title_title', 'Title'),
                    template: function(data) {
                        var output = '\
                            <div class="task_name' + ((data.status) ? ' completed' : '') + '">\
                                <div class="task_name__icon" data-task-id="' + data.id + '">\
                                    <i class="fa fa-check"></i>\
                                </div>\
                                <div class="task_name__link">\
                                    <a href="#" class="m-link" data-task-id="' + data.id + '" data-toggle="modal" data-target="#edit-task-popup">' + data.title + '</a>\
                                </div>\
                            </div>\
                            ';
                        return output;
                    }
                },
                {
                    field: 'type',
                    title: GetTranslation('tasks_datatable_column_type_title', 'Type')
                },
                {
                    field: 'assigned_to',
                    title: GetTranslation('tasks_datatable_associated_with_title', 'Аssociated with')
                },
                {
                    field: 'due_date',
                    title: GetTranslation('tasks_datatable_due_date_title', 'Due date'),
                    template: function (data) {
                        return (data.overdue ? '<span class="m--font-danger">'+data.due_date+'</span>' : data.due_date);
                    }
                },
                {
                    field: 'company_t',
                    title:  GetTranslation('tasks_company', 'Company')

                },
                {
                    field: 'contact_fn',
                    title:  GetTranslation('tasks_contact_name', 'Contact name')
                }
            ];

            datatable = $('#task_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/tasks/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            // Event - Click on task name button
            $('body').on('click', 'div.task_name__icon', function () {
                var task = $(this);
                var task_id = task.data('task-id');

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/tasks/change-status',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'task_id': task_id,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.id)
                            task.closest('.task_name').toggleClass('completed');
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_task_buttons').css({display : "flex"});
                }else{
                    $('#bulk_task_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-tasks').on('click', function(){
                var selected_records = datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('tasks_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('tasks_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/tasks/bulk-delete',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                datatable.reload();
                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
            
            // Delete task button
            $('#delete_task_button').on('click', function(){
                var edit_task_id = $(this).data('task-id');
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: 'Are you sure you want to delete this item?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/tasks/delete?id=' + edit_task_id,
                            dataType: 'json',
                            data: {},
                            success: function (data) {
                                if (data) {
                                    datatable.reload();
                                    $('#edit-task-popup').modal('hide');
                                }
                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
        
        function initPageWidgets(){
            function assoc_contact(modal) {
                // Edit task modal - Queue select2
                $('#'+modal+'_task_contact_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                search: params.term
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['first_name']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation('tasks_create_select_contact'),
                    width: '100%',
                    allowClear: true,
                    dropdownParent: $('#'+modal+'_task_contact_select').parents('.modal-body')
                });
            }

            function assoc_company(modal) {
                // Edit task modal - Queue select2
                $('#'+modal+'_task_company_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-companies-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['name']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation('tasks_create_select_contact'),
                    width: '100%',
                    allowClear: true,
                    dropdownParent: $('#'+modal+'_task_company_select').parents('.modal-body')
                });
            }

            function assoc_order(modal) {
                // Edit task modal - Queue select2
                $('#'+modal+'_task_order_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-orders-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: 'ID #' + data[i]['id'] + ' (' + data[i]['contact']['last_name'] + ' ' + data[i]['contact']['first_name'] + ')'});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation('tasks_create_select_order'),
                    width: '100%',
                    allowClear: true,
                    dropdownParent: $('#'+modal+'_task_order_select').parents('.modal-body')
                });
            }

            function task_type(modal) {
                // Edit task modal - Queue select2
                $('#'+modal+'_task_type_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/ajax/search-task-types',
                        method: 'POST',
                        dataType: 'json',
                        data: {},
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['value']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation("edit_task_type_select_placeholder", "Select type"),
                    width: '100%',
                    minimumResultsForSearch: -1, //Disable search input
                    dropdownParent: $('#'+modal+'_task_type_select').parents('.modal-body')
                });
            }

            function assigned_to(modal) {
                // Edit task modal - Assigned to select2
                $('#'+modal+'_task_assigned_to_id_select').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['first_name']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    placeholder: GetTranslation("edit_task_assigned_to_select_placeholder", "Select responsible"),
                    width: '100%',
                    dropdownParent: $('#'+modal+'_task_assigned_to_id_select').parents('.modal-body')
                });
            }
            
            function initTasksPortletFilter(){
                var filter = $('#tasks-filter').get()[0];
                var portlet = $('#tasks_datatable_portlet').get()[0];
                if((filter !== undefined) && (filter !== null)){
                    $(filter).find('button').on('click', function(){
                        var clicked_btn = this;
                        var filter_type = $(clicked_btn).data('filter');

                        setCookie('selected_page',filter_type,{ expires: 3600 * 24 * 365 * 20,
                            path: '/ita/' + tenantId});
                        $(filter).find('button').removeClass('btn-brand').addClass('btn-secondary');
                        $(clicked_btn).removeClass('btn-secondary').addClass('btn-brand');
                        // Show/hide content parts
                        $(portlet).find('.task-index-portlet-content').removeClass('active');
                        $(portlet).find('.task-index-portlet-content[data-content-part-'+filter_type+'="true"]').addClass('active');
                        switch(filter_type){
                            case 'calendar':
                               TasksPageCalendar.init();
                            break;
                        }
                    });
                }
            }

            function initCalendarFilterCheckboxes(){
                var filter_block = $('#tasks_calendar_filters').get()[0];
                $(filter_block).find('.calendar-filter-checkbox').each(function(){
                    var calendar = $('#tasks_reminders_calendar').get()[0];
                    var checkbox = this;
                    $(checkbox).change(function(){
                        if(this.checked){
                            console.log('filter enabled!');
                            if($(this).data('type')!==undefined) {
                                setCookie($(this).data('type'), '1', {
                                    expires: 3600 * 24 * 365 * 20,
                                    path: '/ita/' + tenantId
                                });
                                $(calendar).fullCalendar( 'refetchEvents' );
                            }
                            // continue logic ...
                        }else{
                            console.log('filter disabled!');
                            if($(this).data('type')!==undefined){
                                setCookie($(this).data('type'), '0', {
                                    expires: 3600 * 24 * 365 * 20,
                                    path: '/ita/' + tenantId
                                });
                                $(calendar).fullCalendar( 'refetchEvents' );
                            }
                            // continue logic ...
                        }
                    });
                });
            }

            assoc_contact('create');
            assoc_contact('edit');
            assoc_company('create');
            assoc_company('edit');
            assoc_order('create');
            assoc_order('edit');
            task_type('create');
            task_type('edit');
            assigned_to('create');
            assigned_to('edit');
            initTasksPortletFilter();
            initCalendarFilterCheckboxes();
            var page = getCookie('selected_page');
            if(page !== undefined){
                $('#tasks-filter').find('button[data-filter="'+page+'"]').trigger('click');
            }
        }
        
        function taskModalCore(modal_selector){
            var modal = $(modal_selector).get()[0];
            // Due date picker init
            $(modal).find('.task_input__due_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            // Due time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $(modal).find('.task_input__due_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: ''
            });
            // Reminder date picker init
            $(modal).find('.task_input__email_reminder_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            // Reminder time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $(modal).find('.task_input__email_reminder_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: ''
            });
            // Event - change due date
            $(modal).find('.task_input__due_date').on('change', function(){
                updateTaskDates(true);
            });
            // Event - change due time
            $(modal).find('.task_input__due_time').timepicker().on('changeTime.timepicker', function (e) {
                updateTaskDates(true);
            });
            // Event - open due time
            $(modal).find('.task_input__due_time').timepicker().on('show.timepicker', function (e) {
                if(!e.time.value){
                    $(this).timepicker('setTime', '09:00');
                }
            });
            // Event - change email reminder date
            $(modal).find('.task_input__email_reminder_date').on('change', function(){
                updateTaskDates();
            });
            // Event - change email reminder time
            $(modal).find('.task_input__email_reminder_time').timepicker().on('changeTime.timepicker', function (e) {
                updateTaskDates();
            });
            // Event - open reminder time
            $(modal).find('.task_input__email_reminder_time').timepicker().on('show.timepicker', function (e) {
                if(!e.time.value){
                    $(this).timepicker('setTime', '09:00');
                }
            });
            // Event - modal was showed
            $(modal).on('show.bs.modal', function (event) {
                if(modal.id === 'edit-task-popup'){
                    if(event.relatedTarget !== undefined){
                        var edit_task_id = $(event.relatedTarget).data('task-id');
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/tasks/get-task?id=' + edit_task_id,
                            dataType: 'json',
                            data: {},
                            success: function (data) {
                                var task = data.task;
                                $(modal).find('input[name="task_id"]').val(edit_task_id);
                                $(modal).find('#delete_task_button').data('task-id', edit_task_id);
                                $('#edit_task_input__title').val(task.title);
                                $(modal).find('.task_input__due_date').datepicker('setDate', new Date(task.due_date*1000));
                                $(modal).find('.task_input__due_time').timepicker('setTime', new Date(task.due_date*1000));
                                $(modal).find('.task_input__email_reminder_date').datepicker('setDate', new Date(task.email_reminder*1000));
                                $(modal).find('.task_input__email_reminder_time').timepicker('setTime', new Date(task.email_reminder*1000));
                                
                                $(modal).find('#edit_task_note_textarea').html(task.description);

                                if (task.type_id) {
                                    $(modal).find('#edit_task_type_select').append('<option value="' + data.type.id + '">' + data.type.value + '</option>').val(task.type_id).trigger('change');
                                } else {
                                    $(modal).find('#edit_task_type_select').val(null).trigger('change');
                                }

                                if (task.assigned_to_id) {
                                    $(modal).find('#edit_task_assigned_to_id_select').append('<option value="' + data.user.id + '">' + (data.user.last_name ? data.user.last_name + ' ' : '') + data.user.first_name + '</option>').val(task.assigned_to_id).trigger('change');
                                } else {
                                    $(modal).find('#edit_task_assigned_to_id_select').val(null).trigger('change');
                                }

                                if (task.contact_id && task.contact_id != null) {
                                    $('#edit_assoc_contact_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                                    $('#edit_assoc_contact_div').appendTo('#edit_assoc_container');
                                    $('#edit_assoc_contact_div').show();

                                    $(modal).find('#edit_task_contact_select').append('<option value="' + data.contact.id + '">' + (data.contact.last_name ? data.contact.last_name + ' ' : '') + data.contact.first_name + '</option>').val(task.contact_id).trigger('change');
                                } else {
                                    $('#edit_assoc_contact_div').hide();
                                    $('#edit_assoc_contact_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                                    $(modal).find('#edit_task_contact_select').val(null).trigger('change');
                                }

                                if (task.company_id && task.company_id != null) {
                                    $('#edit_assoc_company_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                                    $('#edit_assoc_company_div').appendTo('#edit_assoc_container');
                                    $('#edit_assoc_company_div').show();

                                    $(modal).find('#edit_task_company_select').append('<option value="' + data.company.id + '">' + data.company.name + '</option>').val(task.company_id).trigger('change');
                                } else {
                                    $('#edit_assoc_company_div').hide();
                                    $('#edit_assoc_company_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                                    $(modal).find('#edit_task_company_select').val(null).trigger('change');
                                }

                                if (task.order_id && task.order_id != null) {
                                    $('#edit_assoc_order_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                                    $('#edit_assoc_order_div').appendTo('#edit_assoc_container');
                                    $('#edit_assoc_order_div').show();

                                    $(modal).find('#edit_task_order_select').append('<option value="' + data.order.id + '">ID #' + data.order.id + ' ('+data.order.name+')</option>').val(task.order_id).trigger('change');
                                } else {
                                    $('#edit_assoc_order_div').hide();
                                    $('#edit_assoc_order_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                                    $(modal).find('#edit_task_order_select').val(null).trigger('change');
                                }
                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                }
            });
            // Event - create form submit
            if(modal.id === 'add-task-popup'){
                $('#add_task_form').submit(function(e){
                    var is_dates_valid = $(this).data('is-valid');
                    if(is_dates_valid !== 'true'){
                        e.preventDefault();
                        var due_date_val = $(modal).find('.task_input__due_date').val();
                        var due_time_val = $(modal).find('.task_input__due_time').val();
                        var reminder_date_val = $(modal).find('.task_input__email_reminder_date').val();
                        var reminder_time_val = $(modal).find('.task_input__email_reminder_time').val();
                        var tomorrow = new Date();
                        tomorrow.setDate(tomorrow.getDate() + 1); 
                        if(!due_date_val){
                            $(modal).find('.task_input__due_date').datepicker('setDate', tomorrow);
                        }
                        if(!due_time_val){
                            $(modal).find('.task_input__due_time').timepicker('setTime', '09:00');
                        }
                        if(reminder_date_val && !reminder_time_val){
                            $(modal).find('.task_input__email_reminder_time').timepicker('setTime', '09:00');
                        }
                        if(!reminder_date_val && reminder_time_val){
                            $(modal).find('.task_input__email_reminder_date').datepicker('setDate', due_date_val);
                        }
                        $(this).data('is-valid', 'true');
                        $(this).submit();
                        return false;
                    }
                });
            }
            
            function updateTaskDates(calculate_reminder_time){
                /**
                 * @var calculate_reminder_time : true|null - set true if need to auto calculate email reminder date
                 */
                var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
                var due_date = moment($(modal).find('.task_input__due_date').val(), date_format).format('X');
                var due_time = moment($(modal).find('.task_input__due_time').val(), remove_seconds_from_format(time_format)).format('X');
                var reminder_date = moment($(modal).find('.task_input__email_reminder_date').val(), date_format).format('X');
                var reminder_time = moment($(modal).find('.task_input__email_reminder_time').val(), remove_seconds_from_format(time_format)).format('X');
                
                if(due_date !== 'Invalid date' && due_time !== 'Invalid date'){
                    // Get full date timestamp
                    var date = new Date(due_date * 1000);
                    var time = new Date(due_time * 1000);
                    var years = date.getFullYear();
                    var months = date.getMonth()+1;
                    var days = date.getDate();
                    var hours = time.getHours();
                    var minutes = time.getMinutes();
                    var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                    var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                    // Add timestamp in hidden input
                    $(modal).find('.task_due_date_timestamp').val(full_date_timestamp);
                    // Auto genetare email reminder time
                    if(calculate_reminder_time === true){
                        var new_reminder_date = new Date((full_date_timestamp - (60 * 60 * 3)) * 1000);  // 3 hours ago
                        $(modal).find('.task_input__email_reminder_date').datepicker('setDate', new_reminder_date);
                        $(modal).find('.task_input__email_reminder_time').timepicker('setTime', new_reminder_date);
                    }
                }else{
                    // Clear hidden timestamp input if no data
                    $(modal).find('.task_due_date_timestamp').val('');
                }
                
                if(reminder_date !== 'Invalid date' && reminder_time !== 'Invalid date'){
                    // Get full date timestamp
                    var date = new Date(reminder_date * 1000);
                    var time = new Date(reminder_time * 1000);
                    var years = date.getFullYear();
                    var months = date.getMonth()+1;
                    var days = date.getDate();
                    var hours = time.getHours();
                    var minutes = time.getMinutes();
                    var string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                    var full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                    // Add timestamp in hidden input
                    $(modal).find('.email_reminder_timestamp').val(full_date_timestamp);
                }else{
                    // Clear hidden timestamp input if no data
                    $(modal).find('.email_reminder_timestamp').val('');
                }
                
            }
            
            function remove_seconds_from_format(format){
                var new_format = format.replace(':ss', '');
                return new_format;
            }
        }

        function init_assoc(type) {
            $('#'+type+'_assoc_contact').on('click', function (e) {
                if ($('#'+type+'_assoc_contact_div').is(':visible')) {
                    //$('#assoc_contact').closest('span').show(500);
                    $('#'+type+'_assoc_contact_div').slideUp(500);
                    //$('#'+type+'_task_contact_select').val('').trigger('change');

                    $('#'+type+'_assoc_contact_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                } else {
                    $('#'+type+'_assoc_contact_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                    $('#'+type+'_assoc_contact_div').appendTo('#'+type+'_assoc_container');
                    $('#'+type+'_assoc_contact_div').slideDown(500);
                    //$(this).closest('span').hide(500);
                }
            });

            $('#'+type+'_assoc_company').on('click', function (e) {
                if ($('#'+type+'_assoc_company_div').is(':visible')) {
                    //$('#assoc_company').closest('span').show(500);
                    $('#'+type+'_assoc_company_div').slideUp(500);
                    //$('#'+type+'_task_company_select').val('').trigger('change');

                    $('#'+type+'_assoc_company_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                } else {
                    $('#'+type+'_assoc_company_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                    $('#'+type+'_assoc_company_div').appendTo('#'+type+'_assoc_container');
                    $('#'+type+'_assoc_company_div').slideDown(500);
                    //$(this).closest('span').hide(500);
                }
            });

            $('#'+type+'_assoc_order').on('click', function (e) {
                if ($('#'+type+'_assoc_order_div').is(':visible')) {
                    //$('#assoc_order').closest('span').show(500);
                    $('#'+type+'_assoc_order_div').slideUp(500);
                    //$('#'+type+'_task_order_select').val('').trigger('change');

                    $('#'+type+'_assoc_order_link i.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
                } else {
                    $('#'+type+'_assoc_order_link i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
                    $('#'+type+'_assoc_order_div').appendTo('#'+type+'_assoc_container');
                    $('#'+type+'_assoc_order_div').slideDown(500);
                    //$(this).closest('span').hide(500);
                }
            });
        }

        init_assoc('create');
        init_assoc('edit');
    });
})(jQuery);

var TasksPageCalendar = function () {
    return {

        //main function to initiate the module
        init: function () {

            $('.calendar-filter-checkbox').each(function() {
                var type = $(this).data('type');
                if(type!==undefined && getCookie(type)!=undefined && getCookie(type)=='1')
                    $(this).prop('checked',true);
            });

            var calendar = $('#tasks_reminders_calendar').get()[0];
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? 'h A' : 'H:mm';

            $(calendar).fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                titleFormat:date_format,
                timeFormat: format_AM_PM,
                locale:tenant_lang,
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                buttonText: {
                    month: GetTranslation('tasks_month', 'month'),
                    agendaWeek: GetTranslation('tasks_week', 'week'),
                    agendaDay: GetTranslation('tasks_day', 'day'),
                    listWeek: GetTranslation('tasks_list', 'list')
                },
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: baseUrl+'/ita/'+tenantId+'/tasks/events-calendar',
                        method:'get',
                        data: {
                            // our hypothetical feed requires UNIX timestamps
                            start: start.unix(),
                            end: end.unix(),
                            options:{
                                birthday:getCookie('filter_birthday'),
                                task:getCookie('filter_task'),
                                request:getCookie('filter_request')
                            }
                        },
                        success: function(response) {
                            var events = [];

                            for(i in response)
                            {
                                events.push({
                                    title: response[i].title,
                                    start: response[i].start,
                                    description:response[i].description,
                                    className:response[i].className,
                                    iconClass:response[i].iconClass,
                                    url:response[i].url
                                });
                            }
                            callback(events);
                        }
                    });
                },
                // events: [
                //     {
                //         title: 'All Day Event',
                //         start: YM + '-01',
                //         description: 'Lorem ipsum dolor sit incid idunt ut',
                //         className: "m-fc-event--danger m-fc-event--solid-warning",
                //         iconClass: "la la-angellist"
                //     },
                //     {
                //         title: 'Reporting',
                //         start: YM + '-14T13:30:00',
                //         description: 'Lorem ipsum dolor incid idunt ut labore',
                //         end: YM + '-14',
                //         className: "m-fc-event--accent",
                //         iconClass: "la la-android"
                //     },
                //     {
                //         title: 'Company Trip',
                //         start: YM + '-02',
                //         description: 'Lorem ipsum dolor sit tempor incid',
                //         end: YM + '-03',
                //         className: "m-fc-event--primary",
                //         iconClass: "la la-apple"
                //     },
                //     {
                //         title: 'ICT Expo 2017 - Product Release',
                //         start: YM + '-03',
                //         description: 'Lorem ipsum dolor sit tempor inci',
                //         end: YM + '-05',
                //         className: "m-fc-event--light m-fc-event--solid-primary",
                //         iconClass: "la la-bank"
                //     },
                //     {
                //         title: 'Dinner',
                //         start: YM + '-12',
                //         description: 'Lorem ipsum dolor sit amet, conse ctetur',
                //         end: YM + '-10',
                //         iconClass: "la la-angellist"
                //     },
                //     {
                //         id: 999,
                //         title: 'Repeating Event',
                //         start: YM + '-09T16:00:00',
                //         description: 'Lorem ipsum dolor sit ncididunt ut labore',
                //         className: "m-fc-event--danger",
                //         iconClass: "la la-battery-4"
                //     },
                //     {
                //         id: 1000,
                //         title: 'Repeating Event',
                //         description: 'Lorem ipsum dolor sit amet, labore',
                //         start: YM + '-16T16:00:00',
                //         iconClass: "la la-birthday-cake"
                //     },
                //     {
                //         title: 'Conference',
                //         start: YESTERDAY,
                //         end: TOMORROW,
                //         description: 'Lorem ipsum dolor eius mod tempor labore',
                //         className: "m-fc-event--accent",
                //         iconClass: "la la-bomb"
                //     },
                //     {
                //         title: 'Meeting',
                //         start: TODAY + 'T10:30:00',
                //         end: TODAY + 'T12:30:00',
                //         description: 'Lorem ipsum dolor eiu idunt ut labore',
                //         iconClass: ""
                //     },
                //     {
                //         title: 'Lunch',
                //         start: TODAY + 'T12:00:00',
                //         className: "m-fc-event--info",
                //         description: 'Lorem ipsum dolor sit amet, ut labore',
                //         iconClass: "la la-bus"
                //     },
                //     {
                //         title: 'Meeting',
                //         start: TODAY + 'T14:30:00',
                //         className: "m-fc-event--warning",
                //         description: 'Lorem ipsum conse ctetur adipi scing',
                //         iconClass: "la la-chrome"
                //     },
                //     {
                //         title: 'Happy Hour',
                //         start: TODAY + 'T17:30:00',
                //         className: "m-fc-event--metal",
                //         description: 'Lorem ipsum dolor sit amet, conse ctetur',
                //         iconClass: "la la-cloud"
                //     },
                //     {
                //         title: 'Dinner',
                //         start: TODAY + 'T20:00:00',
                //         className: "m-fc-event--solid-focus m-fc-event--light",
                //         description: 'Lorem ipsum dolor sit ctetur adipi scing',
                //         iconClass: "la la-crosshairs"
                //     },
                //     {
                //         title: 'Birthday Party',
                //         start: TOMORROW + 'T07:00:00',
                //         className: "m-fc-event--primary",
                //         description: 'Lorem ipsum dolor sit amet, scing',
                //         iconClass: "la la-dribbble"
                //     },
                //     {
                //         title: 'Click for Google',
                //         url: 'http://google.com/',
                //         start: YM + '-28',
                //         className: "m-fc-event--solid-info m-fc-event--light",
                //         description: 'Lorem ipsum dolor sit amet, labore',
                //         iconClass: "la la-gamepad"
                //     }
                // ],

                eventRender: function (event, element) {
                    if (element.hasClass('fc-day-grid-event')) {
                        element.data('content', event.description);
                        element.data('placement', 'top');
                        mApp.initPopover(element);
                    } else if (element.hasClass('fc-time-grid-event')) {
                        element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                    } else if (element.find('.fc-list-item-title').lenght !== 0) {
                        element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                    }
                    if(event.iconClass !== undefined && event.iconClass !== null && event.iconClass.length>0){
                        element.find('.fc-content').prepend('<i class="' + event.iconClass + ' mr-1"></i>');
                    }
                }
            });
        }
    };
}();
