/** contacts/filters.js
 *
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

function search(filter) {
    var search_text = $('input[name="quick-search"]').val();
    var filter_title = $('#search-dropdown input[name="title"]').val();
    var filter_author = $('#search-dropdown #select_author').val();
    var filter_type = $('#search-dropdown #select_type').val();
    var filter_responsible = $('#search-dropdown #select_responsible').val();
    var completion_start = $('#search-dropdown input[name="completion_start"]').val();
    var completion_end = $('#search-dropdown input[name="completion_end"]').val();
    var create_start = $('#search-dropdown input[name="create_start"]').val();
    var create_end = $('#search-dropdown input[name="create_end"]').val();
    var update_start = $('#search-dropdown input[name="update_start"]').val();
    var update_end = $('#search-dropdown input[name="update_end"]').val();

    var option_count = 0;

    if (search_text.length > 0)
        option_count++;

    if (filter_title.length > 0)
        option_count++;

    if (filter_author != null && filter_author.length > 0)
        option_count++;

    if (filter_type != null && filter_type.length > 0)
        option_count++;

    if (filter_responsible != null && filter_responsible.length > 0)
        option_count++;

    if (completion_start.length > 0 && completion_end.length > 0)
        option_count++;

    if (create_start.length > 0 && create_end.length > 0)
        option_count++;

    if (update_start.length > 0 && update_end.length > 0)
        option_count++;

    //filters_inactive(false);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    var params = {
        search_text: search_text,
        filter_title: filter_title,
        filter_author: filter_author,
        filter_type: filter_type,
        filter_responsible: filter_responsible,
        completion: {
            start: completion_start,
            end: completion_end
        },
        create: {
            start: create_start,
            end: create_end
        },
        update: {
            start: update_start,
            end: update_end
        }
    };

    if (filter) {
        params.filter = filter;

        if (filter != 'false')
            option_count++;
    } else {
        if (datatable.options.data.source.read.params.search.filter != undefined) {
            params.filter = datatable.options.data.source.read.params.search.filter;

            option_count++;
        }
    }

    if (option_count > 0) {
        $('#search-panel #filters_indicator').text(option_count + ' ' + declOfNum(option_count, ['опция', 'опции', 'опций']));
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    datatable.options.data.source.read.params = {
        query: {
            // generalSearch: ''
        },
        search: params
    };
    datatable.reload();

    //setTimeout(function () {
    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
    //}, 1000);
}

function reset(reset_all, reload, filters) {
    var params = {
        query: {
            // generalSearch: ''
        },
        search: {
            //filter_responsible: userId
        }
    };

    if (filters)
        filters_inactive(false);

    //if (!$('#search-panel #reset_search_input_button').is(':visible'))
        //$('#search-panel #reset_search_input_button').show();

    $('#search-panel #filters_indicator').parent().removeClass('show');

    if (reset_all)
        $('input[name="quick-search"]').val('');

    if (!reset_all)
        params.search.search_text = $('input[name="quick-search"]').val();

    $('#search-dropdown input[name="title"]').val('');
    $('#select_responsible').val(null).trigger('change');
    $('#search-dropdown #select_author').val(null).trigger('change');
    $('#search-dropdown #select_type').val(null).trigger('change');
    $('#search-dropdown input[name="completion_start"]').val();
    $('#search-dropdown input[name="completion_end"]').val();
    $('#search-dropdown input[name="create_start"]').val();
    $('#search-dropdown input[name="create_end"]').val();
    $('#search-dropdown input[name="update_start"]').val();
    $('#search-dropdown input[name="update_end"]').val();
    $('#m_daterangepicker_completion input, #m_daterangepicker_create input, #m_daterangepicker_update input').val(null);

    //$('.filter-item #task-open').addClass('active');

    if (reload) {
        datatable.options.data.source.read.params = params;
        datatable.reload();
    }

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
}

function filters_inactive(filter_id) {
    $('.filter-item a').each(function (key, el) {
        if (!filter_id || filter_id != $(el).attr('id')) {
            if ($(el).hasClass('active'))
                $(el).removeClass('active');
        }
    });
}

$('#apply_filter').on('click', function () {
    search(false);
});

$('input#quick-search-input').keyup(function(event) {
    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (event.keyCode == 13 && $('input[name="quick-search"]').val().length > 0) {
        search(false);
        $(this).blur();

        if ($('input[name="quick-search"]').val().length > 0) {
            $('#search-panel #filters_indicator').text('1 опция');
            $('#search-panel #filters_indicator').parent().addClass('show');
        } else {
            $('#search-panel #filters_indicator').parent().removeClass('show');
        }
    }
});

$('#reset_filter').on('click', function () {
    reset(false, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible') && $('input[name="quick-search"]').val().length == 0) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('#reset_search_input_button').on('click', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('body').on('click', 'a#show_all_link', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('.filter-item').on('click', function () {
    var filter = ($(this).find('a').hasClass('active') ? '' : $(this).find('a').attr('id'));

    //reset(true, false, false);
    $(this).find('a').toggleClass('active');
    filters_inactive(filter);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter.length > 0) {
        $('#search-panel #filters_indicator').text('1 опция');
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    search((filter.length == 0 ? 'false' : filter));

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
});

// $('input#quick-search-input').on('click', function () {
//     var filter_tags = $('#search-dropdown #tags_search').val();
//
//     if (filter_tags.length == 0)
//         $('#tags_search').val(null).trigger('change');
// });

$(document).ready(function () {
    $('#select_responsible').select2({
        ajax: {
            url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            delay: 1000,
            processResults: function (data)
            {
                var array = [];
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['first_name']});
                }
                return {
                    results: array
                }
            }
        },
        language: tenant_lang,
        placeholder: GetTranslation('filter_task_responsible'),
        width: '100%',
        allowClear: true,
        dropdownParent: $('#search-dropdown')
    });

    //$('#select_responsible').val(userId).trigger('change');

    $('#select_author').select2({
        ajax: {
            url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            delay: 1000,
            processResults: function (data)
            {
                var array = [];
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['first_name']});
                }
                return {
                    results: array
                }
            }
        },
        language: tenant_lang,
        placeholder: GetTranslation('filter_task_author'),
        width: '100%',
        allowClear: true,
        dropdownParent: $('#search-dropdown')
    });

    $('#select_type').select2({
        ajax: {
            url: baseUrl + '/ita/' + tenantId + '/ajax/search-task-types',
            method: 'POST',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            delay: 1000,
            processResults: function (data)
            {
                var array = [];
                for (var i in data){
                    array.push({id: data[i]['id'], text: data[i]['value']});
                }
                return {
                    results: array
                }
            }
        },
        language: tenant_lang,
        placeholder: GetTranslation('filter_task_type'),
        width: '100%',
        allowClear: true,
        dropdownParent: $('#search-dropdown')
    });

    $('#m_daterangepicker_completion').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        locale: {
            format: date_format,
            daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
            monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
            applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
            cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
            fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
            toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
        }
    }, function (start, end, label) {
        $('#search-dropdown input[name="completion_start"]').val(start / 1000);
        $('#search-dropdown input[name="completion_end"]').val(end / 1000);
        $('#m_daterangepicker_completion .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });

    $('#m_daterangepicker_create').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        locale: {
            format: date_format,
            daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
            monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
            applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
            cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
            fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
            toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
        }
    }, function (start, end, label) {
        $('#search-dropdown input[name="create_start"]').val(start / 1000);
        $('#search-dropdown input[name="create_end"]').val(end / 1000);
        $('#m_daterangepicker_create .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });

    $('#m_daterangepicker_update').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        locale: {
            format: date_format,
            daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
            monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
            applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
            cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
            fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
            toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
        }
    }, function (start, end, label) {
        $('#search-dropdown input[name="update_start"]').val(start / 1000);
        $('#search-dropdown input[name="update_end"]').val(end / 1000);
        $('#m_daterangepicker_update .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });

    datatable.on('m-datatable--on-reloaded', function (e) {
        datatable.on('m-datatable--on-layout-updated', function (e, arg) {
            $('#search-panel #summary_indicator').text(datatable.getTotalRows() + ' ' +
                declOfNum(datatable.getTotalRows(), ['запись', 'записи', 'записей']));

            if ($('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #summary_indicator').parent().show();
        });
    });
});