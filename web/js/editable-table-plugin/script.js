window.initEditableForms = function(parent_selector, successCallback, errorCallback){
    /**
     * parent_selector - Select where to search etitable forms
     * successCallback - call when ajax success result
     * errorCallback -  call when ajax error result
     */
    $(parent_selector).find('.etitable-form').each(function(){
        var form = this;
        var dropdown = $(form).parents('.etitable-form-dropdown')[0];
        var dropdown_content = $(form).parents('.m-dropdown__content')[0];
        var preloader = $(dropdown_content).find('.preloader')[0];
        $(form).off('submit');
        $(form).on('submit', function (event) {
            event.preventDefault();
            var form_data = {};

            $.each($(form).serializeArray(), function(index, item) {
                if (!form_data[item.name]) {
                    form_data[item.name] = item.value;
                } else {
                    var item_array = form_data[item.name];
                    var item_new_array = [];

                    if (Array.isArray(item_array)) {
                        item_new_array = item_new_array.concat(item_array);
                    } else {
                        item_new_array.push(item_array);
                    }

                    item_new_array.push(item.value);

                    form_data[item.name] = item_new_array;
                }
            });
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + form_data.ajax_url,
                data: form_data,
                method: 'POST',
                success: function (response){
                    if (response.message) {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if(response.result === 'success'){
                            toastr.success(response.message);
                            $(dropdown).find('.m-dropdown__toggle').text(response.new_value);

                            if (response.color)
                                $(dropdown).find('.m-dropdown__toggle').removeClass('btn-success btn-warning btn-danger btn-info btn-primary btn-default').addClass('btn-'+response.color);

                            /* Success callback */
                            if(successCallback !== undefined && successCallback !== null){
                                successCallback(response,parent_selector);
                            }
                        }
                        if(response.result === 'error'){
                            toastr.error(response.message);
                            /* Error callback */
                            if(errorCallback !== undefined && errorCallback !== null){
                                errorCallback(response);
                            }
                        }
                    }
                    closeDropDown();
                },
                error: function (response){
                    console.log('ERROR!');
                    console.log(response);
                },
                beforeSend: function (){
                    $(form).hide();
                    $(dropdown_content).find('.result-message').hide();
                    $(preloader).show();
                },
                complete: function(){
                    $(form).show();
                    $(preloader).hide();
                }
            });
        });
    });
    
    function initCloseButtons(){
        $('.etitable-form-cancel').each(function(){
            $(this).on('click', function(){
                closeDropDown();
            });
        });
    }

    function closeDropDown(){
        $('body, html').click();
    }
    
    initCloseButtons();
};

(function ($) {
    $(document).ready(function () {
        initEditableForms('body');
    });
})(jQuery);