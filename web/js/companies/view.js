(function($){
    $(document).ready(function(){
        initValidate()
        initPageWidgets();
        initExtraDropdownOptionInputs();
        initDeleteForm();
        initRemoveContact();

        $('body').find('form').attr('autocomplete', 'off');

        function initPageWidgets(){
            // Timeline history - create note redactor
            $('#add-note-from_timeline-redactor').summernote({
                height: 150,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            
            // Timeline history - create task due date picker init
            $('#timeline_new_task_due_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            
            // Timeline history - create task due time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $('#timeline_new_task_due_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: ''
            });
            
            // Timeline history - create task type picker init
            $('#timeline-create_task_type').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-task-types',
                    method: 'POST',
                    dataType: 'json',
                    data: {},
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['value']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("contacts_timeline_task_type_select_placeholder", "Select type"),
                minimumResultsForSearch: -1, //Disable search input
                width: '100%',
                dropdownParent: $('#timeline-create_task_type').parents('#history_tab_tasks')
            });
            
            // Timeline history - create task textarea init
            autosize($('#timeline-create_task_note_textarea'));
            
            // Timeline history - create task assigned to id init
            $('#timeline-create_task_assigned_to_id_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['first_name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("contacts_timeline_task_assigned_to_select_placeholder", "Select responsible"),
                width: '100%',
                dropdownParent: $('#timeline-create_task_assigned_to_id_select').parents('#history_tab_tasks')
            });
            
            // Timeline history - create task email date picker init
            $('#timeline_new_task_email_reminder_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            
            // Timeline history - create task email time picker init
            var format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $('#timeline_new_task_email_reminder_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: '',
                placeholder: 'time'
            });
            
            // Company contacts repeaters
            $('#contact-phones-repeater, #contact-emails-repeater, #contact-socials-repeater, #contact-messengers-repeater, #contact-sites-repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'text-input': 'foo'
                },
                show: function () {
                    $(this).slideDown();
                    //Init maskinput
                    $('#company-edit-popup-form')
                        .find('[valid="phoneNumber_add"]')
                        .intlTelInput({
                            utilsScript: "/plugins/phone-validator/build/js/utils.js",
                            autoPlaceholder: true,
                            preferredCountries: ['ua', 'ru'],
                            allowExtensions: false,
                            autoFormat: true,
                            autoHideDialCode: true,
                            customPlaceholder: null,
                            defaultCountry: "ua",
                            geoIpLookup: null,
                            nationalMode: false,
                            numberType: "MOBILE"
                        }).then(function (data) {

                            $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                                el = $(this);
                                // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                var formated = '+999 99 999 9999';
                                $(el).inputmask(formated);
                                $(el).on("countrychange", function (e, countryData) {
                                    var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                    $(el).inputmask(formated)
                                });
                            });
                        });
                    $(this).find('input').each(function () {
                        var mask_data = $(this).data('inputmask');
                        if (mask_data) {
                            $(this).inputmask();
                        }
                    });
                    //Init dropdown option
                    initExtraDropdownOptionInputs(this);
                },
                hide: function (deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
            $('#contacts-modal-tags-search-select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public'
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("company-edit-popup-add-tag-placeholder", "Add a tag"),
                tags: true
            });
            $('#company-info-table-company_type_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-types-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['value']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("field_company_type_placeholder"),
                width: '100%',
                dropdownParent: $('#company-info-table-company_type_select').parent()
            });
            $('#company-info-table-company_kind_activity_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-kind-activities-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['value']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("field_company_kind_activity_placeholder"),
                width: '100%',
                dropdownParent: $('#company-info-table-company_kind_activity_select').parent()
            });
            $('#company-info-table-company_status_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-statuses-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['value']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("field_company_status_placeholder"),
                width: '100%',
                dropdownParent: $('#company-info-table-company_status_select').parent()
            });
            $('#select2-responsible-ajax-search').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/companies/get-responsible-options',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            var full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("company-settings-partlet-responsible_select_placeholder", "Select responsible")
            });
            $('#select2-responsible-ajax-search').on('change', function(){
                var company_id = $('#select2-responsible-ajax-search-company-id-hint').val();
                var selected = $(this).find("option:selected").val();
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/companies/change-responsible',
                    data: {
                        company_id: company_id,
                        responsible_id: selected,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method:'POST',
                    success:function (response) {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if(response.result === 'success'){
                            toastr.success(response.message);
                        }
                        if(response.result === 'error'){
                            toastr.error(response.message);
                        }
                    }
                });
            });
            $('#company_contact_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['first_name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation("company_added_contact_placeholder"),
                width: '100%',
                dropdownParent: $('#company_contact_select').parent()
            });

            var phones = $('a.phones');

            phones.each(function (i) {
                var phone = $(this).text();

                if (phone != null && phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
                    var result = phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );

                    $(this).text('+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4]);
                } else {
                    $(this).text((phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone));
                }
            });
        }

        function initValidate(){
            $('#company-edit-popup-form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                $('#contact-phones-repeater').find('.validate-phone').each(function(index, el) {
                    el = $(this);

                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated)
                    });
                });
            });


            $('#company-edit-popup-form')
                .find('[valid="phoneNumber_add"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function(data){

                $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated);
                    });
                });
            });
        }

        function initExtraDropdownOptionInputs(added_row){
            if (added_row){
                //Init default dropdown value
                var group = $(added_row).find('.extra-dropdown-option')[0];
                var hidden_input_selector = $(group).data('for-input-selector');
                var hidden_input = $(group).find(hidden_input_selector)[0];
                var default_val = $(hidden_input).data('default-value');
                $(hidden_input).val(default_val).removeAttr('data-default-value');
            }
            $('.extra-dropdown-option').each(function(){
                var group = this;
                var hidden_input_selector = $(group).data('for-input-selector');
                var hidden_input = $(group).find(hidden_input_selector)[0];
                var drop_down_btn = $(group).find('button.btn')[0];
                $(group).find('.dropdown-menu a').each(function(){
                    var variant = this;
                    var variant_name = $(this).text();
                    var variant_val = $(this).data('value');
                    $(variant).unbind('click');
                    $(variant).on('click', function(){
                        $(hidden_input).val(variant_val);
                        $(drop_down_btn).text(variant_name);
                    });
                });
            });
        }

        function initDeleteForm(){
            var form = $('#company-view-delete-form').get()[0];
            $(form).on('submit', function (event) {
                if(!$(form).data('confirmed')){
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('company_delete_question'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if(result.value === true){
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }

        function initRemoveContact(){
            $('.remove_contact').on('click', function (event) {
                event.preventDefault();

                var delete_link = $(this).attr('href');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    text: GetTranslation('company_contact_delete_question'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value === true){
                        window.location.href = delete_link;
                    }
                });
            });
        }
    });
})(jQuery);