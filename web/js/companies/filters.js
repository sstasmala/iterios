/** contacts/filters.js
 *
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

var search_text = $('input[name="quick-search"]');
var contact_passport = $('input#company_contact_filter_input_passport');
var contact_last_name = $('input#company_contact_filter_input_last_name');
var contact_first_name = $('input#company_contact_filter_input_first_name');
var contact_phone = $('input#company_contact_filter_input_phone');
var contact_email = $('input#company_contact_filter_input_email');
var contact_tags = $('select#contacts-filter-tags-search-select');
var company_name = $('input#company_filter_input_name');
var company_phone = $('input#company_filter_input_phone');
var company_email = $('input#company_filter_input_email');
var company_status = $('select#company_filter_input_status');
var company_type = $('select#company_filter_input_type');
var company_activities_type = $('select#company_filter_input_activities_type');
var company_responsible = $('select#filter-select2-responsible-ajax-search');

function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

function count_filters(filters) {
    var option_count = 0;

    $.each(filters, function( index, value ) {
        if ($.isPlainObject(value)) {
            option_count += count_filters(value);
        } else {
            if (value && value.length > 0 )
                option_count++;
        }
    });

    return option_count;
}

function search(filter) {
    var params = {
        search_text: search_text.val(),
        contact: {
            passport: contact_passport.val(),
            first_name: contact_first_name.val(),
            last_name: contact_last_name.val(),
            phone: contact_phone.val().replace(/^_+|[_]+$/g, ''),
            email: contact_email.val().replace(/^_+|[@_.]+$/g, ''),
            tags: contact_tags.val()
        },
        name: company_name.val(),
        phone: company_phone.val(),
        email: company_email.val(),
        status: company_status.val(),
        type: company_type.val(),
        activities_type: company_activities_type.val(),
        responsible: company_responsible.val()
    };

    var option_count = count_filters(params);

    //filters_inactive(false);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter) {
        params.filter = filter;

        if (filter != 'false')
            option_count++;
    } else {
        if (companies_datatable.options.data.source.read.params.search.filter != undefined) {
            params.filter = companies_datatable.options.data.source.read.params.search.filter;

            option_count++;
        }
    }

    if (option_count > 0) {
        $('#search-panel #filters_indicator').text(option_count + ' ' + declOfNum(option_count, ['опция', 'опции', 'опций']));
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    companies_datatable.options.data.source.read.params = {
        query: {
            // generalSearch: ''
        },
        search: params
    };
    companies_datatable.reload();

    //setTimeout(function () {
    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
    //}, 1000);
}

function reset(reset_all, reload, filters) {
    var params = {
        query: {
            // generalSearch: ''
        },
        search: {
        }
    };

    if (filters)
        filters_inactive(false);

    //if (!$('#search-panel #reset_search_input_button').is(':visible'))
        //$('#search-panel #reset_search_input_button').show();

    $('#search-panel #filters_indicator').parent().removeClass('show');

    if (reset_all)
        search_text.val('');

    if (!reset_all)
        params.search.search_text = search_text.val();

    contact_passport.val('');
    contact_last_name.val('');
    contact_first_name.val('');
    contact_phone.val('');
    contact_email.val('');
    contact_tags.val(null).trigger('change');

    company_name.val('');
    company_phone.val('');
    company_email.val('');
    company_status.val(null).trigger('change');
    company_type.val(null).trigger('change');
    company_activities_type.val(null).trigger('change');
    company_responsible.val(null).trigger('change');

    if (reload) {
        companies_datatable.options.data.source.read.params = params;
        companies_datatable.reload();
    }

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
}

function filters_inactive(filter_id) {
    $('.filter-item a').each(function (key, el) {
        if (!filter_id || filter_id != $(el).attr('id')) {
            if ($(el).hasClass('active'))
                $(el).removeClass('active');
        }
    });
}

$('#apply_filter').on('click', function () {
    search(false);
});

$('input#quick-search-input').keyup(function(event) {
    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (event.keyCode == 13 && $('input[name="quick-search"]').val().length > 0) {
        search(false);
        $(this).blur();

        if ($('input[name="quick-search"]').val().length > 0) {
            $('#search-panel #filters_indicator').text('1 опция');
            $('#search-panel #filters_indicator').parent().addClass('show');
        } else {
            $('#search-panel #filters_indicator').parent().removeClass('show');
        }
    }
});

$('#reset_filter').on('click', function () {
    reset(false, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible') && $('input[name="quick-search"]').val().length == 0) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('#reset_search_input_button').on('click', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('body').on('click', 'a#show_all_link', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('.filter-item').on('click', function () {
    var filter = ($(this).find('a').hasClass('active') ? '' : $(this).find('a').attr('id'));

    //reset(true, false, false);
    $(this).find('a').toggleClass('active');
    filters_inactive(filter);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter.length > 0) {
        $('#search-panel #filters_indicator').text('1 опция');
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    search((filter.length == 0 ? 'false' : filter));

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
});

// $('input#quick-search-input').on('click', function () {
//     var filter_tags = $('#search-dropdown #tags_search').val();
//
//     if (filter_tags.length == 0)
//         $('#tags_search').val(null).trigger('change');
// });

$(document).ready(function () {
    companies_datatable.on('m-datatable--on-reloaded', function (e) {
        companies_datatable.on('m-datatable--on-layout-updated', function (e, arg) {
            $('#search-panel #summary_indicator').text(companies_datatable.getTotalRows() + ' ' +
                declOfNum(companies_datatable.getTotalRows(), ['запись', 'записи', 'записей']));

            if ($('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #summary_indicator').parent().show();
        });
    });
});