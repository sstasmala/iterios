var companies_datatable = null;

function beautiful_phone(phone) {
    if (phone != null && phone.search( /(\d{2})(\d{3})(\d{3})(\d{4})/ ) >= 0) {
        var result = phone.match( /(\d{2})(\d{3})(\d{3})(\d{4})/ );

        return '+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4];
    }

    return (phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone);
}

var CompanyIndexPage = function(){

    var companiesDatatable = function(){
        var columns = [
            {
                field: 'id',
                title: '#',
                sortable: false,
                width: 40,
                textAlign: 'center',
                selector: {class: 'm-checkbox--solid m-checkbox--brand'}
            },
            {
                // Название
                field: 'name',
                title: GetTranslation('ci_datatable_col_title_1', ''),
                sortable: false,
                template: function(data) {
                    return '<a href="'+baseUrl + '/ita/' + tenantId + '/companies/view?id='+data.id+'">'+data.name+'</a>';
                }
            },
            {
                // Контакты
                field: 'contacts',
                title: GetTranslation('ci_datatable_col_title_2', ''),
                sortable: false,
                width: 250,
                overflow: 'visible',
                template: function(data, index, datatable) {
                    var contacts = [];
                    if(data.phones != null && data.phones.length >0)
                        contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(data.phones[0].value))+'</div>';
                    if(data.emails != null &&  data.emails.length >0)
                        contacts.push( '<div class="contact-row"><i class="fa fa-envelope-o"></i> '+data.emails[0].value)+'</div>';
                    if(data.phones != null)
                        data.phones.forEach(function (item,i) {
                            if(i!=0)
                                contacts.push( '<div class="contact-row"><i class="fa fa-phone"></i> '+beautiful_phone(item.value))+'</div>';
                        });
                    if(data.emails != null)
                        data.emails.forEach(function (item,i) {
                            if(i!=0)
                                contacts.push( '<div class="contact-row"><i class="fa fa-envelope-o"></i> '+item.value)+'</div>';
                        });
                    var first = '';
                    var additional = '';
                    contacts.forEach(function (item,i) {
                        if(i<2) {
                            if (i < 1)
                                item+='</div>';
                            first += '<div class="">'+item;
                        }
                        else
                            additional+='<li class="dropdown-item">'+item+'</li>';
                    });

                    var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    var toggle = '<div class="dropdown ' + dropup + '">' +
                        '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"> ' +
                        '<i class="la la-ellipsis-h"></i>' +
                        '</a>' +
                        '<ul class="dropdown-menu additional-list">'
                        +additional +
                        '</ul>' +
                        '</div>';
                    if(additional != '')
                        first += toggle;
                    first += '</div>';
                    return first;
                }
            },
            {
                // Статус
                field: 'status',
                title: GetTranslation('ci_datatable_col_title_3', ''),
                sortable: false,
                template: function(data) {
                    return data.company_status_id ? '<span class="m-badge m-badge--'+data.status.color+' m-badge--dot"></span><span class="m--font-bold m--font-'+data.status.color+'"> '+(data.status.value ? data.status.value : '-')+'</span>' : '';
                }
            },
            {
                // Тип
                field: 'type',
                title: GetTranslation('ci_datatable_col_title_4', ''),
                sortable: false,
                template: function(data) {
                    return data.type.value;
                }
            },
            {
                // Вид деятельности
                field: 'kind_activity',
                title: GetTranslation('ci_datatable_col_title_5', ''),
                sortable: false,
                template: function(data) {
                    return data.kind_activity.value;
                }
            },
            {
                // Ответственный
                field: 'responsible',
                width: 90,
                title: GetTranslation('ci_datatable_col_title_6', ''),
                sortable: false,
                template: function(data) {
                    return  '<div class="m-card-user m-card-user--sm">\
                                <div class="m-card-user__pic">\
                                    <img src="'+baseUrl+'/'+data.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered" alt="'+data.responsible.full_name+'" title="'+data.responsible.full_name+'">\
                                </div>\
                            </div>';
                }
            }
        ];

        companies_datatable = $('#companies_data_table').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: baseUrl + '/ita/' + tenantId + '/companies/get-data',
                        method: 'GET',
                        params: {
                            query: {
                                // generalSearch: ''
                            },
                            search: {
                                filter_responsible: userId
                            }
                            // custom query params
                        },
                        map: function (raw) {

                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 400
            },
            rows: {
                afterTemplate: function (row, data, index) {

                },
                callback: function () {

                },
                // auto hide columns, if rows overflow. work on non locked columns
                autoHide: false
            },
            columns: columns,
            toolbar: {
                layout: ['pagination', 'info'],
                placement: ['bottom'],  //'top', 'bottom'
                items: {
                    pagination: {
                        type: 'default',

                        pages: {
                            desktop: {
                                layout: 'default',
                                pagesNumber: 6
                            },
                            tablet: {
                                layout: 'default',
                                pagesNumber: 3
                            },
                            mobile: {
                                layout: 'compact'
                            }
                        },

                        navigation: {
                            prev: true,
                            next: true,
                            first: true,
                            last: true
                        },

                        pageSizeSelect: [10, 20, 30, 50, 100]
                    },

                    info: true
                }
            },
            translate: {
                records: {
                    processing: GetTranslation('datatable_data_processiong','Please wait...'),
                    noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: GetTranslation('pagination_first','First'),
                                prev: GetTranslation('pagination_previous','Previous'),
                                next: GetTranslation('pagination_next','Next'),
                                last: GetTranslation('pagination_last','Last'),
                                more: GetTranslation('pagination_more','More pages'),
                                input: GetTranslation('pagination_page_number','Page number'),
                                select: GetTranslation('pagination_page_size','Select page size')
                            },
                            info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                        }
                    }
                }
            }
        });

        /* DATATABLE EVENTS */

        // Init event
        $(companies_datatable).on('m-datatable--on-init', function(e){
            initTableCheckboxes();
        });
        // Layout updated event
        $(companies_datatable).on('m-datatable--on-layout-updated', function(e){
            initTableCheckboxes();
            checkBulkButtonsVisibility();
        });

        // Add event for checkboxes
        function initTableCheckboxes(){
            $('[data-field="id"] input[type="checkbox"]').each(function(){
                var checkbox = this;
                $(checkbox).off('change');
                $(checkbox).on('change', function(){
                    checkBulkButtonsVisibility();
                });
            });
        }

        // Show/hide bulk buttons
        function checkBulkButtonsVisibility(){
            var changed_records = companies_datatable.getSelectedRecords();
            var changed_count = changed_records.length;
            if(changed_count>0){
                $('#bulk_companies_buttons').css({display : "flex"});
            }else{
                $('#bulk_companies_buttons').hide();
            }
        }

        // Bulk delete button
        $('#remove-companies').on('click', function(){
            var selected_records = companies_datatable.getSelectedRecords();
            var ids_to_delete = [];
            $(selected_records).each(function(){
                var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                ids_to_delete.push(id);
            });
            swal({
                title: GetTranslation('delete_question','Are you sure?'),
                text: GetTranslation('ci_delete_message1','Are you sure to delete this') +
                        ' ' +
                        ids_to_delete.length +
                        ' ' +
                        GetTranslation('ci_delete_message2','items?'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                reverseButtons: true
            }).then(function(result){
                if(result.value){
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/companies/bulk-delete',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            ids: ids_to_delete,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            companies_datatable.reload();
                        },
                        error: function () {
                            alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                        }
                    });
                }
            });
        });
    };
    
    var filterCompaniesCore = function() {
        $('#company_filter_input_type').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-types-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        search: params.term
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['value']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation("ci_filter_type_input_placeholder"),
            width: '100%',
            dropdownParent: $('#company_filter_input_type').parent()
        });

        $('#company_filter_input_activities_type').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-kind-activities-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        search: params.term
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['value']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation("ci_filter_activities_types_input_placeholder"),
            width: '100%',
            dropdownParent: $('#company_filter_input_activities_type').parent()
        });

        $('#company_filter_input_status').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/companies/get-companies-statuses-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        search: params.term
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['value']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation("ci_filter_status_input_placeholder"),
            width: '100%',
            dropdownParent: $('#company_filter_input_status').parent()
        });

        // Tags filter select
        var tags_select = $('#contacts-filter-tags-search-select').get()[0];
        $(tags_select).select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                method: 'POST',
                dataType: 'json',
                    data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            dropdownParent: $(tags_select).parent(),
            width: '100%',
            placeholder: GetTranslation("ci_filter_contact_tags_input_placeholder", "Contact tag"),
            tags: true
        });
        
        // Responsible select
        var responsible_select = $('#filter-select2-responsible-ajax-search').get()[0];
        $(responsible_select).select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/companies/get-responsible-options',
                method: 'POST',
                dataType: 'json',
                    data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public',
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        var full_name = '';
                        full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                        full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                        full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                        array.push({id: data[i]['id'], text: full_name.trim()});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            dropdownParent: $(responsible_select).parent(),
            width: '100%',
            placeholder: GetTranslation("ci_filter_responsible_input_placeholder", "Select responsible")
        });
    };
    
    return {
        init: function () {
            companiesDatatable();
            filterCompaniesCore();
        }
    };
}();

jQuery(document).ready(function() {
    CompanyIndexPage.init();
});