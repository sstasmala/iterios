/**
 *
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */


function ConditionsConstructor(container, input, config) {


    function init() {
        this.container = undefined;
        this.config = {
            rules: {},
            rulesTitle:'Rule',
            operatorsTitle:'Operator',
            addRuleTitle:'Add',
            removeRuleTitle:'Remove'
        };

        this.operators = {
            equal:            { type: 'equal',            nb_inputs: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'] },
            not_equal:        { type: 'not_equal',        nb_inputs: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'] },
            in:               { type: 'in',               nb_inputs: 1, multiple: true,  apply_to: ['string', 'number', 'datetime'] },
            not_in:           { type: 'not_in',           nb_inputs: 1, multiple: true,  apply_to: ['string', 'number', 'datetime'] },
            less:             { type: 'less',             nb_inputs: 1, multiple: false, apply_to: ['number', 'datetime'] },
            less_or_equal:    { type: 'less_or_equal',    nb_inputs: 1, multiple: false, apply_to: ['number', 'datetime'] },
            greater:          { type: 'greater',          nb_inputs: 1, multiple: false, apply_to: ['number', 'datetime'] },
            greater_or_equal: { type: 'greater_or_equal', nb_inputs: 1, multiple: false, apply_to: ['number', 'datetime'] },
            between:          { type: 'between',          nb_inputs: 2, multiple: false, apply_to: ['number', 'datetime'] },
            not_between:      { type: 'not_between',      nb_inputs: 2, multiple: false, apply_to: ['number', 'datetime'] },
            begins_with:      { type: 'begins_with',      nb_inputs: 1, multiple: false, apply_to: ['string'] },
            not_begins_with:  { type: 'not_begins_with',  nb_inputs: 1, multiple: false, apply_to: ['string'] },
            contains:         { type: 'contains',         nb_inputs: 1, multiple: false, apply_to: ['string'] },
            not_contains:     { type: 'not_contains',     nb_inputs: 1, multiple: false, apply_to: ['string'] },
            ends_with:        { type: 'ends_with',        nb_inputs: 1, multiple: false, apply_to: ['string'] },
            not_ends_with:    { type: 'not_ends_with',    nb_inputs: 1, multiple: false, apply_to: ['string'] },
            is_empty:         { type: 'is_empty',         nb_inputs: 0, multiple: false, apply_to: ['string'] },
            is_not_empty:     { type: 'is_not_empty',     nb_inputs: 0, multiple: false, apply_to: ['string'] },
            is_null:          { type: 'is_null',          nb_inputs: 0, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'] },
            is_not_null:      { type: 'is_not_null',      nb_inputs: 0, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'] }
        };
        this.input = undefined;

        var Component = this;

        this.buildRuleSelector = function (rule) {

            var html = '<select class="form-control m-bootstrap-select m_selectpicker rules-select" title="'
                + (Component.config.rulesTitle!=undefined?Component.config.rulesTitle:'Rule') +'">';
            for(rl in Component.config.rules) {
                var title = (Component.config.rules[rl]!=undefined
                && Component.config.rules[rl].label!=undefined?Component.config.rules[rl].label:Component.config.rules[rl].id);
                html += '<option value="'
                    +Component.config.rules[rl].id+'" '
                    +(Component.config.rules[rl].id==rule?'selected':'')
                    +'>' + title + '</option>';
            }
            html += '</select>';
            return html;
        };

        this.buildOperators = function (operators,value) {
            console.log(value);
            var html = '<select class="form-control m-bootstrap-select m_selectpicker operators-select" title="'
                + (Component.config.operatorsTitle!=undefined?Component.config.operatorsTitle:'Operator') +'">';
            if(operators != undefined)
                for(op in operators) {
                    if(typeof operators[op] === 'object') {
                        if(operators[op].type === undefined || Component.operators[operators[op].type] === undefined)
                            continue;

                        var title = operators[op].type;
                        if(operators[op].tite != undefined)
                            title = operators[op].title;
                        html += '<option value="'+operators[op].type+'" '+(operators[op].type==value?'selected':'')+'>' + title + '</option>';
                    } else {
                        var title = operators[op];
                        html += '<option value="'+operators[op]+'" '+(operators[op]==value?'selected':'')+'>' + title + '</option>';
                    }
                }
            html += '</select>';
            return html;
        };
        
        this.getValue = function () {
            var data = {};
            data.condition = Component.container.find('.condition-type').val();
            data.rules = [];

            Component.container.find('.condition-row').each(function () {
                var row = $(this);

                var rule = row.find('.condition-rule').find('select.rules-select');
                var operator = row.find('.condition-operator').find('select.operators-select');
                if(rule == undefined || operator == undefined)
                    return;
                rule = rule.val();
                operator = operator.val();
                var base_config = Component.searchRule(rule);
                if(base_config == undefined)
                    return;
                var config = {};
                var value = row.find('.condition-value').find('[name="'+base_config.id+'"]');
                if(value == undefined)
                    return;
                value = value.val();
                config.id = base_config.id;
                if(base_config.field != undefined)
                    config.field = base_config.field;
                else
                    config.field = base_config.id;
                config.type = base_config.type;
                config.value = value;
                config.operator = operator;
                data.rules.push(config);
            });

            data.valid = true;
            return data;
        };

        this.updateValue = function () {
            var data = Component.getValue();
            console.log('input');
            console.log(Component.input);
            if(Component.input != undefined)
                Component.input.val(JSON.stringify(data));
        };

        this.buildNumber = function (name,value) {
            return '<input type="number" name="'+(name!=undefined?name:'')+'" class="form-control m-input" value="'+(value!=undefined?value:'')+'">';
        };

        this.buildString = function (name,value) {
            return '<input type="text" name="'+(name!=undefined?name:'')+'" class="form-control m-input" value="'+(value!=undefined?value:'')+'">';
        };
        
        this.buildDate = function (name,value) {
            return '<input type="text" name="'+(name!=undefined?name:'')+'" class="form-control m-input date-range" value="'+(value!=undefined?value:'')+'">';
        } 

        this.searchRule = function (rule_id) {
            for(I in Component.config.rules)
                if(Component.config.rules[I].id == rule_id)
                    return Component.config.rules[I];
            return undefined
        };

        this.buildConditionForm = function (config) {
            var data = {
                html:'',
                init:function () {}
            };
            if(config == undefined)
                config = {
                    rule:undefined,
                    operator:undefined,
                    value:undefined
                };
            var condition = '<div class="row mb-3 mb-md-0 condition-row"><div class="col-12 col-md-10 col-lg-11"><div class="row">';

            condition += '<div class="col-12 col-md-4 mb-3 condition-rule">';
            condition += Component.buildRuleSelector(config.id);
            condition += '</div>';

            condition += '<div class="col-12 col-md-4 mb-3 condition-operator">';
            // console.log(Component.config.rules);
            // console.log(config.rule);
            if(Component.searchRule(config.id) !== undefined)
                condition += Component.buildOperators(Component.searchRule(config.id).operators,config.operator);
            condition += '</div>';

            condition += '<div class="col-12 col-md-4 mb-3 condition-value">';
            if(config.value!=undefined) {
                var input = Component.buildInput(config);
                condition += input.input;
                data.init = input.init;
            }

            condition += '</div></div>';

            condition += '</div>' +
                '<div class="col-12 col-md-2 col-lg-1">' +
                '<a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only remove-condition" title="">' +
                '<i class="la la-remove"></i>' +
                '</a>' +
                '</div>' +
                '</div>';

            data.html = condition;
            return data;
        }

        this.build = function () {
            Component.container.empty();
            var html = '<div class="row mb-2 mb-md-0">' +
                '<input type="hidden" class="condition-type" value="AND">' +
                '<div class="col-12 conditions-container">' +
                '</div>' +
                '<div class="col-12"> ' +
                '<a href="#" class="m-link add-condition"> +&nbsp;'+ Component.config.addRuleTitle +'</a></div>';
            Component.container.append(html);
            Component.initInputs();
        };

        this.initSelectpicker = function () {
            Component.container.find('.m_selectpicker').selectpicker({
                width: '100%'
            });
        };

        this.buildInput = function (config) {
            var data = {};
            data.input = '';
            data.init = function () {
            };

            if(config == undefined || config.id == undefined )
                return data;

            if(config.type == undefined) {
                data.input = Component.buildString(config.id,config.value);
                return data;
            }

            switch (config.type) {
                case 'string':data.input = Component.buildString(config.id,config.value);break;
                case 'integer':data.input = Component.buildNumber(config.id,config.value);break;
                case 'datetime':{
                    data.input = Component.buildDate(config.id,config.value);
                    data.init = function (input) {
                        input.daterangepicker({
                            buttonClasses: 'm-btn btn',
                            applyClass: 'btn-primary',
                            cancelClass: 'btn-secondary',
                            singleDatePicker: true,
                            showDropdowns: true,
                            locale: {
                                format: date_format,
                                daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                                monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                            },
                            templates: {
                                leftArrow: '<i class="la la-angle-left"></i>',
                                rightArrow: '<i class="la la-angle-right"></i>'
                            }
                        }).click(function(e){
                            $('.daterangepicker').on('click', function(e){
                                e.stopPropagation();
                            });
                        });
                    };
                    break;
                }
                default:data.input = Component.buildString(config.value);
            }
            return data;
        }

        this.initInputs = function () {
            Component.container.find('.add-condition').off('click').on('click', function () {
                var container = Component.container.find('.conditions-container');
                var html = Component.buildConditionForm().html;
                container.append(html);
                Component.initInputs();
                // Component.updateValue();
            });
            Component.initSelectpicker();
            Component.container.find('select.rules-select').each(function () {
                if($(this).attr('data-inited')==1)
                    return;
                $(this).attr('data-inited',1);
                $(this).on('change',function () {
                    // console.log($(this).val());
                    var operator = $(this).closest('.condition-row').find('.condition-operator');
                    var input = $(this).closest('.condition-row').find('.condition-value');
                    input.empty();
                    operator.empty();
                    operator.append(Component.buildOperators(Component.searchRule($(this).val()).operators));
                    Component.initSelectpicker();
                    Component.initInputs();
                })
            });
            Component.container.find('select.operators-select').each(function () {
                if($(this).attr('data-inited')==1)
                    return;
                $(this).attr('data-inited',1);
                $(this).on('change',function () {
                    // console.log($(this).val());
                    var input = $(this).closest('.condition-row').find('.condition-value');
                    var rule = $(this).closest('.condition-row').find('select.rules-select').val();
                    input.empty();
                    var config = Component.searchRule(rule);
                    var data = Component.buildInput(config);

                    input.append(data.input);
                    data.init(input.find('[name="'+config.id+'"]'));
                    // Component.initSelectpicker();
                })
            });
            Component.container.find('.remove-condition').off('click').on('click',function () {
               $(this).closest('.condition-row').remove();
            });
        };

        this.renderFromRules = function () {

            try {
                var rules = Component.input.val();
                rules = JSON.parse(rules);
            } catch (e) {
                return;
            }

            var condition = rules.condition;
            rules = rules.rules;
            if(rules == undefined)
                return;

            var config = undefined;
            var container = Component.container.find('.conditions-container');
            for(X in rules) {
                config = Component.searchRule(rules[X].id);
                console.log(config);
                if(config == undefined)
                    continue;
                config.value = rules[X].value;
                config.operator = rules[X].operator;
                var rule = Component.buildConditionForm(config);
                container.append(rule.html);
                rule.init(container.find('[name="'+rules[X].id+'"]'));
            }
            Component.initSelectpicker();
            Component.initInputs();
        };

        return function (container, input, config) {
            console.log(input);

            if(config!=undefined)
                Component.config = config;
            Component.container = container;
            Component.input = input;
            Component.build();
            return Component;
        }
    }

    return init()(container, input, config);
}