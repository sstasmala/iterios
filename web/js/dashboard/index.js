(function ($) {
    $(document).ready(function () {
        initFormWidgets();
        daterangepickerInit();
        initDraggablePortlets();

        function initFormWidgets() {
            $('#select_stage_select2').select2({
                language: tenant_lang,
                placeholder: "Select stage"
            });
            $('#select_crated_by_select2').select2({
                language: tenant_lang,
                placeholder: "Select crated by"
            });
            $('#select_update_by_select2').select2({
                language: tenant_lang,
                placeholder: "Select update by"
            });
            $('#select_changed_by_select2').select2({
                language: tenant_lang,
                placeholder: "Select changed by"
            });
            $('#m_daterangepicker_1').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                    applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                    cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                    fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                    toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
                }
            }, function (start, end, label) {
                $('#m_daterangepicker_1 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
            });
            $('#m_daterangepicker_2').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                    applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                    cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                    fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                    toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
                }
            }, function (start, end, label) {
                $('#m_daterangepicker_2 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
            });
            $('#m_daterangepicker_3').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                    applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                    cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                    fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                    toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
                }
            }, function (start, end, label) {
                $('#m_daterangepicker_3 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
            });
            initSearchFormBudgetSlider();
        }

        function initSearchFormBudgetSlider() {
            var slider = document.getElementById('budget_range_nouislider');
            noUiSlider.create(slider, {
                start: [1000, 5000],
                connect: true,
                range: {
                    'min': 0,
                    'max': 10000
                }
            });
            var sliderInput0 = document.getElementById('budget_range_min');
            var sliderInput1 = document.getElementById('budget_range_max');
            var sliderInputs = [sliderInput0, sliderInput1];
            slider.noUiSlider.on('update', function (values, handle) {
                sliderInputs[handle].value = values[handle];
            });
        }
        
        function daterangepickerInit() {
            if ($('#m_dashboard_daterangepicker').length == 0) {
                return;
            }

            var picker = $('#m_dashboard_daterangepicker');
            var start = moment();
            var end = moment();

            function cb(start, end, label) {
                var title = '';
                var range = '';

                if ((end - start) < 100) {
                    title = 'Today:';
                    range = start.format('MMM D');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D');
                } else {
                    range = start.format('MMM D') + ' - ' + end.format('MMM D');
                }

                picker.find('.m-subheader__daterange-date').html(range);
                picker.find('.m-subheader__daterange-title').html(title);
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        }
        
        function initDraggablePortlets(){
            // Init sortable
            $("#dashboard_sortable_portlets").sortable({
                connectWith: ".m-portlet__head",
                items: ".m-portlet", 
                opacity: 0.8,
                handle : '.m-portlet__head',
                scroll: true,
                scrollSensitivity: 50,
                tolerance: "pointer",
                placeholder: "portlet-placeholder ui-corner-all",
                forceHelperSize: true,
                helper: "clone",
                cancel: ".m-portlet--sortable-empty", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    if (c.item.prev().hasClass("m-portlet--sortable-empty")) {
                        c.item.prev().before(c.item);
                    }                    
                },
                start(event, ui){
                    // Took a full size portlet event
                    console.log(ui.item.outerHeight());
                    if($(ui.item).hasClass('full-mode')){
                        ui.placeholder.css('width', '100%');
                    }
                    // Took a half size portlet event
                    if($(ui.item).hasClass('half-mode')){
                        ui.placeholder.css('width', 'calc(50% - 15px)');
                    }
                    ui.placeholder.css('height', ui.item.outerHeight()+'px');
                }
            });
            // Init resizable
            $("#dashboard_sortable_portlets .m-portlet").resizable({
                handles: "e",
                stop: function(event, ui){
                    var portlet_percent_width = (ui.originalElement.outerWidth()/ui.originalElement.parent().outerWidth())*100;
                    if(portlet_percent_width >= 75){
                        ui.originalElement.css('width', '100%');
                        ui.originalElement.addClass('full-mode');
                        ui.originalElement.removeClass('half-mode');
                    } else {
                        ui.originalElement.css('width', 'calc(50% - 15px)');
                        ui.originalElement.addClass('half-mode');
                        ui.originalElement.removeClass('full-mode');
                    }
                }
            });
        }
    });
})(jQuery);
