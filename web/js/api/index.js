var api_datatable = null;

(function($){
    $(document).ready(function(){
        initApiDatatable();
        initKeyGenProcess();
        
        function initApiDatatable(){
            var columns = [
                {
                    field: 'date',
                    title: GetTranslation('api_index_datatable_date_col', 'Date'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return '04.06.2018';
                    }
                },
                {
                    field: 'method',
                    title: GetTranslation('api_index_datatable_method_col', 'Method'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return 'POST';
                    }
                },
                {
                    field: 'status',
                    title: GetTranslation('api_index_datatable_status_col', 'Status'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return 'Active';
                    }
                },
                {
                    field: 'user',
                    title: GetTranslation('api_index_datatable_user_col', 'User'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return 'User Name';
                    }
                },
                {
                    field: 'url',
                    title: GetTranslation('api_index_datatable_url_col', 'url'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return 'https://urldaomain.com';
                    }
                },
                {
                    field: 'key',
                    title: GetTranslation('api_index_datatable_key_col', 'Key'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return '1cba31**-****-****-****-**********';
                    }
                },
                {
                    field: 'ip',
                    title: GetTranslation('api_index_datatable_ip_col', 'IP'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return '127.0.0.1';
                    }
                }
            ];
            
            api_datatable = $('#api_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/orders/get-data',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {
                                    
                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(api_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(api_datatable).on('m-datatable--on-layout-updated', function(e){
                
            });
        }
        
        function initKeyGenProcess(){
            var keygen_btn = $('#keygen_start').get()[0];
            var keygen_preloader = $('#keygen_preloader').get()[0];
            var generated_key = $('#generated_key').get()[0];
            var generated_key_input = $('#generated_key_input').get()[0];
            
            // Click on keygen button event
            $(keygen_btn).on('click', function(){
                $(keygen_btn).hide();
                $(keygen_preloader).find('.m-loader').show();
                console.log('Иммитация паузы при генерации ключа'); 
                setTimeout(function(){  // Delete this timeout
                    var new_key = '1234-123456-098765-abcdefg'; // Demo key
                    $(generated_key_input).val(new_key);
                    $(keygen_preloader).find('.m-loader').hide();
                    $(generated_key).removeClass('d-none');
                    api_datatable.reload();
                }, 1500);
            });
            
            // Click on key copy button
            $('#copy_key_button').on('click', function(){
                $('#generated_key_input').select();
                try {
                    var successful = document.execCommand('copy');
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    if (successful === true) {
                        toastr.success(GetTranslation('api_index_key_copy_succsess_message', 'Key was copyed!'));
                    } else {
                        toastr.error(GetTranslation('api_index_key_copy_error_message', 'Key copy error! Try to copy key manually'));
                    }
                } catch (err) {
                    toastr.error(GetTranslation('api_index_key_copy_error_message', 'Key copy error! Try to copy key manually'));
                }
                $('#generated_key_input').trigger('blur');
            });
            
            // Click Deactivate key button event
            $('#key_deactivate').on('click', function(){
                swal({
                    title: GetTranslation('api_index_deactivation_proccess_title', 'API key deactivation'),
                    text: GetTranslation('api_index_deactivation_proccess_description', 'Are you sure you want to deactivate this key?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('yes', 'Yes'),
                    cancelButtonText: GetTranslation('no', 'No'),
                    reverseButtons: true,
                    showCloseButton: true,
                    focusConfirm: false
                }).then( function(isConfirm) {
                    if(isConfirm.value) {
                        $(generated_key_input).val('');
                        alert('Деактивация ключа...');
                    }
                    api_datatable.reload();
                });
            });
        }
    });
})(jQuery);