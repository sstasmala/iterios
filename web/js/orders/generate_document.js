(function($){
    $(document).ready(function(){
        initGenerateDocPopUp();
        initEditDocPopUp();
        saveDocument();
        deleteDocument();
        document_not_save_alert();
        saveTitleDocument();

        var open_doc_add_modal = true;
        var save_document = false;
        var auto_save = false;
        var event_set_data = false;

        var saveDelay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $('#document_add_modal').on('show.bs.modal', function (e) {
            $('#upload_doc_tab').find('#alert').remove();
        });

        $('#document_view_modal').on('shown.bs.modal', function (e) {
            $('body').addClass('modal-open');
        });

        function document_not_save_alert() {
            $('#document_view_modal').on('hide.bs.modal', function(e) {
                if (!save_document) {
                    e.preventDefault();

                    swal({
                        title: GetTranslation('document_not_save_alert_title'),
                        text: GetTranslation('document_not_save_alert_text'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('document_not_save_alert_btn'),
                        cancelButtonText: GetTranslation('document_not_save_alert_btn_cancel'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            save_document = true;
                            $('#document_view_modal').modal('hide');

                            if (open_doc_add_modal)
                                $('#document_add_modal').modal('show');
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                } else if (open_doc_add_modal) {
                    $('#document_add_modal').modal('show');
                }

                $('#document_view_modal #gener_doc_input').data('gener_doc_id', 0);
            });
        }

        function initGenerateDocPopUp() {
            $('.document-link').on('click', function (e) {
                $('#document_view_modal .editable-title input').val('').trigger('change');
                CKEDITOR.instances['doc-editor-block'].setData('');

                $('#doc_delete_btn').hide();

                var doc_title = $(this).find('.btn-lg__text').text();
                $('#document_view_modal .editable-title input').val($.trim(doc_title)).trigger('change');

                var order_id = $(this).data('order_id');
                var doc_id = $(this).data('doc_id');

                save_document = true;
                open_doc_add_modal = true;
                auto_save = false;

                $('#doc_save_btn').show();

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-document-preview?order_id='+order_id+'&tid='+doc_id,
                    type: 'GET',
                    success:function (response) {
                        if (response) {
                            $('#document_view_modal .editable-title input').val(response.title).trigger('change');
                            CKEDITOR.instances['doc-editor-block'].setData(response.body);

                            $('#doc_save_btn').data('order_id', order_id).data('doc_id', doc_id).data('order_doc_id', null);
                        }
                    }
                });
            });

            CKEDITOR.instances['doc-editor-block'].on('setData', function() {
                event_set_data = true;
            });

            CKEDITOR.instances['doc-editor-block'].on('change', function(e) {
                if (!event_set_data) {
                    if (auto_save) {
                        saveDelay(function () {
                            save_doc_request($('#doc_save_btn'), 0);
                        }, 2000);
                    } else {
                        save_document = false;
                    }
                }

                event_set_data = false;
            });
        }

        function initEditDocPopUp() {
            $('body').on('click', '.our-doc-link', function (e) {
                $('#document_view_modal .editable-title input').val('').trigger('change');
                CKEDITOR.instances['doc-editor-block'].setData('');

                open_doc_add_modal = false;
                save_document = true;
                auto_save = true;

                $('#doc_delete_btn').show();

                var doc_title = $(this).text();
                $('#document_view_modal .editable-title input').val($.trim(doc_title)).trigger('change');

                var order_id = $(this).data('order_id');
                var doc_id = $(this).data('doc_id');
                var order_doc_id = $(this).data('order_doc_id');

                $('#doc_save_btn').hide();

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-document?document_id=' + $(this).data('order_doc_id'),
                    type: 'GET',
                    success: function (response) {
                        if (response) {
                            $('#document_view_modal #gener_doc_input').data('gener_doc_id', order_doc_id);
                            $('#document_view_modal .editable-title input').val(response.title).trigger('change');
                            CKEDITOR.instances['doc-editor-block'].setData(response.body);

                            $('#doc_save_btn').data('order_id', order_id).data('doc_id', doc_id).data('order_doc_id', order_doc_id);
                            $('#doc_delete_btn').data('order_doc_id', order_doc_id);
                        }
                    }
                });
            });
        }

        function saveTitleDocument() {
            $('#document_view_modal #gener_doc_input').on('input', function (e) {
                var input = $(this);
                var order_doc_id = input.data('gener_doc_id');

                saveDelay(function () {
                    var value = input.val();

                    if (value.length > 1 && order_doc_id) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/save-title-document?document_id='+order_doc_id,
                            method: 'POST',
                            data: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                title: value
                            },
                            success: function (response) {
                                if (response) {

                                    var template = '<li>' +
                                        '<div class="name">' +
                                        '<a href="#" class="m-link m--font-bold our-doc-link" data-toggle="modal" data-target="#document_view_modal" data-doc_id="' + response.template_id + '" data-order_id="' + response.order_id + '" data-order_doc_id="' + response.id + '">' + response.title + '</a>' +
                                        '<div class="date">' +
                                        '<span class="moment-js invisible" data-format="unix">' + response.updated_at + '</span>' +
                                        '</div></div><div class="actions">' +
                                        '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill our-doc-delete-link" title="' + GetTranslation('delete') + '" data-order_doc_id="' + response.id + '">' +
                                        '<i class="la la-trash"></i></a></div></li>';

                                    setTimeout(function () {
                                        var content = {
                                            title: GetTranslation('success_message', 'Success!'),
                                            message: ''
                                        };

                                        $.notify(content, {
                                            placement: {
                                                from: "top",
                                                align: "right"
                                            },
                                            z_index: 1055,
                                            type: "success"
                                        });

                                        //input.blur();
                                        $('#document_upload_view_modal .editable-title input').val(response.title).trigger('change');

                                        if (!$('.our-docs li').is(':visible')) {
                                            $('#our_doc_block').show();
                                        }

                                        if (order_doc_id)
                                            $('li a[data-order_doc_id="'+order_doc_id+'"]').closest('li').remove();

                                        $('.order-docs-block .our-docs').prepend(template);
                                        $('.order-documents-portlet div.text-center').hide();
                                        initMomentJS();
                                    }, 1000);
                                }
                            },
                            error: function (response) {
                                var content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1055
                                });
                            }
                        });
                    }
                }, 1000);
            });
        }

        function save_doc_request(data, close_modal) {
            var order_id = $(data).data('order_id');
            var doc_id = $(data).data('doc_id');
            var order_doc_id = $(data).data('order_doc_id');

            if (order_id && doc_id) {
                var data = CKEDITOR.instances['doc-editor-block'].getData();

                event_set_data = true;

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/save-document?order_id=' + order_id + '&tid=' + doc_id + (order_doc_id ? '&document_id='+order_doc_id : ''),
                    method: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        title: close_modal ? $('#document_view_modal .editable-title input').val() : null,
                        body: data,
                        body_hash: $.md5(data)
                    },
                    success: function (response) {
                        if (response) {
                            save_document = true;

                            if (close_modal) {
                                open_doc_add_modal = false;
                                $('#document_view_modal').modal('hide');
                            }

                            var template = '<li>' +
                                '<div class="name">' +
                                '<a href="#" class="m-link m--font-bold our-doc-link" data-toggle="modal" data-target="#document_view_modal" data-doc_id="' + response.template_id + '" data-order_id="' + response.order_id + '" data-order_doc_id="' + response.id + '">' + response.title + '</a>' +
                                '<div class="date">' +
                                '<span class="moment-js invisible" data-format="unix">' + response.updated_at + '</span>' +
                                '</div></div><div class="actions">' +
                                '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill our-doc-delete-link" title="' + GetTranslation('delete') + '" data-order_doc_id="' + response.id + '">' +
                                '<i class="la la-trash"></i></a></div></li>';

                            setTimeout(function () {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1055,
                                    type: "success"
                                });

                                if (!$('.our-docs li').is(':visible')) {
                                    $('#our_doc_block').show();
                                }

                                if (order_doc_id)
                                    $('li a[data-order_doc_id="'+order_doc_id+'"]').closest('li').remove();

                                $('.order-docs-block .our-docs').prepend(template);
                                $('.order-documents-portlet div.text-center').hide();
                                initMomentJS();
                            }, 1000);

                            $('#document_view_modal .editable-title input').val('').trigger('change')
                        }
                    },
                    error: function (response) {
                        var content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1055
                        });
                    }
                });
            }
        }

        function saveDocument() {
            $('#doc_save_btn').on('click', function (e) {
                save_doc_request($(this), 1);
            });
        }

        function deleteDocument() {
            $('body').on('click', '.our-doc-delete-link', function (e) {
                var order_doc_id = $(this).data('order_doc_id');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/delete-document?document_id=' + order_doc_id,
                            type: 'GET',
                            success: function (response) {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1055,
                                    type: "success"
                                });

                                open_doc_add_modal = false;
                                $('#document_view_modal').modal('hide');

                                $('li a[data-order_doc_id="' + order_doc_id + '"]').closest('li').remove();

                                if ($('.our-docs li').length === 0)
                                    $('#our_doc_block').hide();

                                if ($('.other-docs li').length === 0 && $('.our-docs li').length === 0)
                                    $('.order-documents-portlet div.text-center').show();
                            },
                            error: function (response) {
                                var content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1055
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });
        }
    });
})(jQuery);