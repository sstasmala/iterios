/** contacts/filters.js
 *
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

var search_text = $('input[name="quick-search"]');
var contact_passport = $('input#order_contact_filter_input_passport_serial');
var contact_last_name = $('input#order_contact_filter_input_last_name');
var contact_first_name = $('input#order_contact_filter_input_first_name');
var contact_phone = $('input#order_contact_filter_input_phone');
var contact_email = $('input#order_contact_filter_input_email');
var contact_tags = $('select#contacts-filter-tags-search-select');
var create_date = $('input#oi_filter_create_date_picker');
var update_date = $('input#oi_filter_update_date_picker');
var responsible = $('select#oi_filter_select_responsible');
var countries = $('select#oi_filter_select_country');
var reservation_number = $('input#oi_filter_reservation_number');
var departure_date = $('input#oi_filter_select_departure_date_picker');
var budget_from = $('input#oi_filter_input_currency_min');
var budget_to = $('input#oi_filter_input_currency_max');
var request_number = $('input#oi_filter_request_number');

function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

function count_filters(filters) {
    var option_count = 0;

    $.each(filters, function( index, value ) {
        if (index == 'create_date_end' || index == 'update_date_end' || index == 'departure_date_end')
            return;

        if ($.isPlainObject(value)) {
            option_count += count_filters(value);
        } else {
            if (value && value.length > 0 )
                option_count++;
        }
    });

    return option_count;
}

function search(filter) {
    var params = {
        search_text: search_text.val(),
        contact: {
            passport: contact_passport.val(),
            first_name: contact_first_name.val(),
            last_name: contact_last_name.val(),
            phone: contact_phone.val().replace(/^_+|[_]+$/g, ''),
            email: contact_email.val().replace(/^_+|[@_.]+$/g, ''),
            tags: contact_tags.val()
        },
        create_date_start: $('input#oi_filter_create_date_picker_start').val(),
        create_date_end: $('input#oi_filter_create_date_picker_end').val(),
        update_date_start: $('input#oi_filter_update_date_picker_start').val(),
        update_date_end: $('input#oi_filter_update_date_picker_end').val(),
        responsible: responsible.val(),
        reservation_number: reservation_number.val(),
        countries: countries.val(),
        departure_date_start: $('input#oi_filter_select_departure_date_picker_start').val(),
        departure_date_end: $('input#oi_filter_select_departure_date_picker_end').val(),
        budget_from: budget_from.val(),
        budget_to: budget_to.val(),
        request_number: request_number.val()
    };

    var option_count = count_filters(params);

    //filters_inactive(false);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter) {
        params.filter = filter;

        if (filter != 'false')
            option_count++;
    } else {
        if (orders_datatable.options.data.source.read.params.search.filter != undefined) {
            params.filter = orders_datatable.options.data.source.read.params.search.filter;

            option_count++;
        }
    }

    if (option_count > 0) {
        $('#search-panel #filters_indicator').text(option_count + ' ' + declOfNum(option_count, ['опция', 'опции', 'опций']));
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    orders_datatable.options.data.source.read.params = {
        query: {
            // generalSearch: ''
        },
        search: params
    };
    orders_datatable.reload();

    //setTimeout(function () {
    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
    //}, 1000);
}

function reset(reset_all, reload, filters) {
    var params = {
        query: {
            // generalSearch: ''
        },
        search: {
        }
    };

    if (filters)
        filters_inactive(false);

    //if (!$('#search-panel #reset_search_input_button').is(':visible'))
    //$('#search-panel #reset_search_input_button').show();

    $('#search-panel #filters_indicator').parent().removeClass('show');

    if (reset_all)
        search_text.val('');

    if (!reset_all)
        params.search.search_text = search_text.val();

    contact_passport.val('');
    contact_last_name.val('');
    contact_first_name.val('');
    contact_phone.val('');
    contact_email.val('');
    contact_tags.val(null).trigger('change');
    create_date.val('');
    update_date.val('');
    responsible.val(null).trigger('change');
    countries.val(null).trigger('change');
    reservation_number.val('');
    departure_date.val(null);
    budget_from.val('');
    budget_to.val('');
    request_number.val('');

    $('input#oi_filter_create_date_picker_start').val('');
    $('input#oi_filter_create_date_picker_end').val('');
    $('input#oi_filter_update_date_picker_start').val('');
    $('input#oi_filter_update_date_picker_end').val('');
    $('input#oi_filter_select_departure_date_picker_start').val('');
    $('input#oi_filter_select_departure_date_picker_end').val('');

    if (reload) {
        orders_datatable.options.data.source.read.params = params;
        orders_datatable.reload();
    }

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
}

function filters_inactive(filter_id) {
    $('.filter-item a').each(function (key, el) {
        if (!filter_id || filter_id != $(el).attr('id')) {
            if ($(el).hasClass('active'))
                $(el).removeClass('active');
        }
    });
}

$('#apply_filter').on('click', function () {
    search(false);
});

$('input#quick-search-input').keyup(function(event) {
    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (event.keyCode == 13 && $('input[name="quick-search"]').val().length > 0) {
        search(false);
        $(this).blur();

        if ($('input[name="quick-search"]').val().length > 0) {
            $('#search-panel #filters_indicator').text('1 опция');
            $('#search-panel #filters_indicator').parent().addClass('show');
        } else {
            $('#search-panel #filters_indicator').parent().removeClass('show');
        }
    }
});

$('#reset_filter').on('click', function () {
    reset(false, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible') && $('input[name="quick-search"]').val().length == 0) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('#reset_search_input_button').on('click', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('body').on('click', 'a#show_all_link', function () {
    reset(true, true, true);

    if ($('#search-panel #reset_search_input_button').is(':visible')) {
        $('#search-panel #reset_search_input_button').hide();
        $('#search-panel #summary_indicator').parent().hide();
    }
});

$('.filter-item').on('click', function () {
    var filter = ($(this).find('a').hasClass('active') ? '' : $(this).find('a').attr('id'));

    //reset(true, false, false);
    $(this).find('a').toggleClass('active');
    filters_inactive(filter);

    if (!$('#search-panel #reset_search_input_button').is(':visible'))
        $('#search-panel #reset_search_input_button').show();

    if (filter.length > 0) {
        $('#search-panel #filters_indicator').text('1 опция');
        $('#search-panel #filters_indicator').parent().addClass('show');
    } else {
        $('#search-panel #filters_indicator').parent().removeClass('show');
    }

    search((filter.length == 0 ? 'false' : filter));

    $(document.getElementById('search-dropdown')).stop().slideUp(300, function(){
        $(this).removeClass('open');
    });
});

$(document).ready(function () {
    initFilterInputs();

    orders_datatable.on('m-datatable--on-reloaded', function (e) {
        orders_datatable.on('m-datatable--on-layout-updated', function (e, arg) {
            $('#search-panel #summary_indicator').text(orders_datatable.getTotalRows() + ' ' +
                declOfNum(orders_datatable.getTotalRows(), ['запись', 'записи', 'записей']));

            if ($('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #summary_indicator').parent().show();
        });
    });

    function initFilterInputs(){
        // Tags
        $('#contacts-filter-tags-search-select').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            width: '100%',
            placeholder: GetTranslation("oi_filter_contact_tags_input_placeholder", "Change tags"),
            tags: false
        });

        // Departure date
        var datepicker = $('#oi_filter_select_departure_date_picker').get()[0];
        $(datepicker).daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            parentEl: $(datepicker).closest('#search-dropdown'),
            opens: 'left',
            locale: {
                format: date_format,
                daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
            },
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }).click(function(e){
            $('.daterangepicker').on('click', function(e){
                // It is fix for daterangepicker. It disble closing
                // dropdown, when next and prev arrows was clicked
                e.stopPropagation();
            });
        });
        $(datepicker).val(''); // make datepicker empty
        $(datepicker).on('change', function(){
            var startDate = $(this).data('daterangepicker').startDate.format(date_format);
            var endDate = $(this).data('daterangepicker').endDate.format(date_format);
            $('#oi_filter_select_departure_date_picker_start').val(startDate);
            $('#oi_filter_select_departure_date_picker_end').val(endDate);
        });

        // Budget inputs
        $('.extra-dropdown-option').each(function(){
            var group = this;
            var hidden_input_selector = $(group).data('for-input-selector');
            var hidden_input = $(group).find(hidden_input_selector)[0];
            var drop_down_btn = $(group).find('button.btn')[0];
            $(group).find('.dropdown-menu a').each(function(){
                var variant = this;
                var variant_name = $(this).text();
                var variant_val = $(this).data('value');
                $(variant).unbind('click');
                $(variant).on('click', function(){
                    $(hidden_input).val(variant_val);
                    $(drop_down_btn).text(variant_name);
                });
            });
        });

        // Create dates
        var datepicker = $('#oi_filter_create_date_picker').get()[0];
        $(datepicker).daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            parentEl: $(datepicker).closest('#search-dropdown'),
            opens: 'center',
            locale: {
                format: date_format,
                daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
            },
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }).click(function(e){
            $('.daterangepicker').on('click', function(e){
                // It is fix for daterangepicker. It disble closing
                // dropdown, when next and prev arrows was clicked
                e.stopPropagation();
            });
        });
        $(datepicker).val(''); // make datepicker empty
        $(datepicker).on('change', function(){
            var startDate = $(this).data('daterangepicker').startDate.format(date_format);
            var endDate = $(this).data('daterangepicker').endDate.format(date_format);
            $('#oi_filter_create_date_picker_start').val(startDate);
            $('#oi_filter_create_date_picker_end').val(endDate);
        });

        // Update dates
        var datepicker = $('#oi_filter_update_date_picker').get()[0];
        $(datepicker).daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            parentEl: $(datepicker).closest('#search-dropdown'),
            opens: 'center',
            locale: {
                format: date_format,
                daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
            },
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }).click(function(e){
            $('.daterangepicker').on('click', function(e){
                // It is fix for daterangepicker. It disble closing
                // dropdown, when next and prev arrows was clicked
                e.stopPropagation();
            });
        });
        $(datepicker).val(''); // make datepicker empty
        $(datepicker).on('change', function(){
            var startDate = $(this).data('daterangepicker').startDate.format(date_format);
            var endDate = $(this).data('daterangepicker').endDate.format(date_format);
            $('#oi_filter_update_date_picker_start').val(startDate);
            $('#oi_filter_update_date_picker_end').val(endDate);
        });

        // Responsible
        $('#oi_filter_select_responsible').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/contacts/get-responsible-options',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public',
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        var full_name = '';
                        full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                        full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                        full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                        array.push({id: data[i]['id'], text: full_name.trim()});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            width: '100%',
            placeholder: GetTranslation("oi_filter_order_responsible_placeholder", "Responsible")
        });
    }
});