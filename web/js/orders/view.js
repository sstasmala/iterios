(function($){
    $(document).ready(function(){

        initValidate();
        initOpeners();
        initPageWidgets();
        initExtraDropdownOptionInputs();
        initRemindersPartlet();
        initDocumentViewModal();
        initAddPaymentModal();
        initEditTouristsModal();
        initDeleteForm();
        delete_note();
        edit_note();
        reminder_setdate();
        add_reminder_modal();
        initServicesPopupWidgets();
        initReminderSource();
        initTourists();
        initReservationModal();
        initCheckTouristSerial();

        $('body').find('form').attr('autocomplete', 'off');
        var i = null;


        function initCheckTouristSerial() {
            let timeDelay = (function(){
                let timer = 0;
                return function(callback, ms){
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            })();

            $('body').on('input', 'input.serial_passport_input', function () {
                let value = $(this).val();
                let passport_id = $(this).data('passport_id');
                let input = $(this);

                if ($(this).val().length > 0) {
                    timeDelay(function () {
                        let data = {
                            serial: value,
                            passport_id: (passport_id ? passport_id : '')
                        };

                        $.get(baseUrl + '/ita/' + tenantId + '/orders/check-passport-serial', data, function (res) {
                            if (res.error_msg) {
                                swal({
                                    text: res.error_msg,
                                    type: "warning"
                                });

                                input.closest('div.modal-content').find('.btn-primary').attr('disabled', 'disabled').css('cursor', 'not-allowed');
                            } else {
                                input.closest('div.modal-content').find('.btn-primary').removeAttr('disabled').css('cursor', 'pointer');
                            }
                        });
                    }, 500);
                }
            });
        }

        function initTourists() {
            let tourist;

            $('#add_tourist_tourist_select').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/orders/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        let query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        tourist = data;

                        let array = [];
                        for (let i in data) {
                            let value = data[i];
                            value.text = value.first_name+' '+value.last_name;
                            array.push(value);
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation("ov_services_tourists_contact_search_select_placeholder", "Select contact");
                    }
                    return data.text;
                },
                language: tenant_lang,
                //allowClear: true,
                minimumInputLength: 2,
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (perm) {
                    if (perm.loading) {
                        return perm.text;
                    }
                    let markup = '<div>' + perm.text + '</div>';

                    if (perm.passport && perm.passport.id) {
                        markup += '<div>' +
                            '<p style="margin:0"><b>Date limit: </b><small>'+(perm.passport.date_limit ? perm.passport.date_limit : '-')+'</small></p>' +
                            '<p style="margin:0"><b>Serial: </b><small>' + (perm.passport.serial ? perm.passport.serial : '-') + '</small></p>' +
                            '</div>';
                    }

                    return markup;
                },
                width: '100%',
                dropdownParent: $('#add_tourist_tourist_select').closest('.modal-body'),
                placeholder: GetTranslation("ov_services_tourists_contact_search_select_placeholder", "Select contact")
            });

            $('#add_tourist_tourist_select').on('change', function () {
                if ($(this).val() in tourist) {
                    tourist = tourist[$(this).val()];

                    if (tourist.passport && tourist.passport.id)
                        $('#m_modal_add_tourist #add_tourist_form input[name="passport_id"]').val(tourist.passport.id);
                }
            });

            $('span#add_tourist_link').on('click', function () {
                $('#m_modal_add_tourist #add_tourist_form input[name="order_tourist_id"]').val($(this).data('ot_id'));
            });

            $('span#add_tourist_passport_link').on('click', function () {
                $('#m_modal_edit_tourists #tourist_passport_form input[name="order_tourist_id"]').val($(this).data('ot_id'));
                $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[first_name]"]').val($(this).data('fname')).attr('disabled', 'disabled');
                $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[last_name]"]').val($(this).data('lname')).attr('disabled', 'disabled');
            });

            $('.tourist-table-partlet button.remove-tourist').on('click', function (e) {
                let id_tourist = $(this).data('tourist_id');
                let item = $(this).closest('tr');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/delete-tourist?tourist_id=' + id_tourist,
                            method: 'GET',
                            success: function (response) {
                                let content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                                item.remove();
                            },
                            error: function (response) {
                                let content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });

            $('#m_modal_add_tourist .btn-primary').click(function(e) {
                e.preventDefault();
                let btn = $(this);
                let form = $(this).closest('.modal-content').find('form');

                form.validate({
                    rules: {
                        'tourist_type': {
                            required: true
                        },
                        'tourist_id': {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/orders/add-tourist',
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response) {
                                $('#m_modal_add_tourist').modal('hide');

                                location.reload();
                            }
                        }, 1000);
                    }
                });
            });

            $('#m_modal_add_tourist').on('hide.bs.modal', function (e) {
                let validator = $('#m_modal_add_tourist #add_tourist_form').validate();
                validator.resetForm();
            });

            function clear_tourist_fields(fields) {
                $(fields).find('input.serial_passport_input').data('passport_id', '');

                $(fields).find('.edit_tourist_passport_pid').val('');
                $(fields).find('.edit_tourist_passport_first_name').val('');
                $(fields).find('.edit_tourist_passport_last_name').val('');
                $(fields).find('.edit_tourist_passport_serial').val('');
                $(fields).find('.edit_tourist_passport_date_limit_picker').val('');
                $(fields).find('.edit_tourist_passport_birth_date_picker').val('');
                $(fields).find('.edit_passport_form_issued_owner').val('');
                $(fields).find('.edit_tourist_passport_type_select').val(null).selectpicker('refresh');

                $(fields).find('.edit_contact_passport_nationality').select2().select2('destroy');
            }

            function set_tourist_fields(data, fields, i) {
                if (data.id) {
                    $(fields).find('input.serial_passport_input').data('passport_id', data.id);

                    // Passport date limit datepicker
                    $(fields).find('.edit_tourist_passport_date_limit_picker').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        format: date_format_datepicker,
                        language: tenant_lang,
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    $(fields).find('.edit_tourist_passport_birth_date_picker').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        format: date_format_datepicker,
                        language: tenant_lang,
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    $(fields).find('#passport_form_issued_date_datepicker').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        format: date_format_datepicker,
                        language: tenant_lang,
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    $(fields).find('.edit_passport_form_issued_date_datepicker').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        format: date_format_datepicker,
                        language: tenant_lang,
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    $(fields).find('.edit_tourist_passport_issued_date').datepicker({
                        weekStart: +week_start,
                        todayHighlight: true,
                        format: date_format_datepicker,
                        language: tenant_lang,
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });

                    $(fields).find('.edit_tourist_passport_pid').val(data.id);
                    $(fields).find('.edit_tourist_passport_type_select').val(data.type.id).selectpicker('refresh');
                    $(fields).find('.edit_tourist_passport_first_name').val(data.first_name);
                    $(fields).find('.edit_tourist_passport_last_name').val(data.last_name);
                    $(fields).find('.edit_tourist_passport_serial').val(data.serial);
                    $(fields).find('.edit_passport_form_issued_owner').val(data.issued_owner);
                    $(fields).find('.edit_tourist_passport_date_limit_picker').datepicker('setDate', data.date_limit);
                    $(fields).find('.edit_tourist_passport_birth_date_picker').datepicker('setDate', data.birth_date);
                    $(fields).find('.edit_tourist_passport_issued_date').datepicker('setDate', data.issued_date);
                    $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/get-country-name?country_name=' + data.nationality,
                            success: function(result){
                                var newOption = new Option(result, data.nationality, true, true);
                                $('#edit_passport_nationality_'+i).append(newOption).trigger('change');
                            }
                        })
                }
            }

            $('.tourist-table-partlet button.edit-tourist').on('click', function (e) {
                let id_tourist = $(this).data('tourist_id');
                let form = $('#m_modal_edit_tourist_passports form#tourist_passport_form');

                $('#m_modal_edit_tourist_passports .btn-primary').data('tourist_id', id_tourist);

                $('#m_modal_edit_tourist_passports #passport_data_title').hide();

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-tourist-passports?tourist_id=' + id_tourist,
                    method: 'GET',
                    success: function (response) {
                        clear_tourist_fields($(form).find('#edit_tourist_fields'));
                        $('#m_modal_edit_tourist_passports input[name="Contact[first_name]"]').val(response.contact.first_name);
                        $('#m_modal_edit_tourist_passports input[name="Contact[last_name]"]').val(response.contact.last_name);
                        function ajaxCall() {
                            $('.country-select').each(function(){
                                var select = this;

                                var parentElement = $(this).parent();
                                $(select).select2({
                                    ajax: {
                                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                                        method: 'POST',
                                        dataType: 'json',
                                        data: function (params) {
                                            var query = {
                                                search: params.term,
                                                type: 'public',
                                                _csrf: $('meta[name="csrf-token"]').attr('content')
                                            }
                                            return query;
                                        },
                                        delay: 1000,
                                        processResults: function (data)
                                        {
                                            var array = [];
                                            for (var i in data){
                                                array.push({id: data[i]['id'], text: data[i]['title']});
                                            }
                                            return {
                                                results: array
                                            }
                                        }
                                    },
                                    minimumInputLength: 2,
                                    allowClear: true,
                                    width: '100%',
                                    dropdownParent: parentElement,
                                    placeholder: GetTranslation("select_country_placeholder", "Select country")
                                });
                            });
                        }
                        if (response.passports.length <= 1) {
                            $('.country-select').each(function(){
                            var select = this;
                            var parentElement = $(this).parent();

                        });
                            if (response.passports.length != 0) {
                                var count_form = ($(form).find('.edit_contact_passport_nationality')).length;
                                for(var y = 0; y<=count_form; y++){
                                    $(form).find('.edit_contact_passport_nationality').eq(y).attr('data-select2-id', 'edit_passport_nationality_'+y);
                                    $(form).find('.edit_contact_passport_nationality').eq(y).attr('id', 'edit_passport_nationality_'+y);
                                }
                                ajaxCall();
                                set_tourist_fields(response.passports[0], $(form).find('#edit_tourist_fields'), 0);
                                $('#m_modal_edit_tourist_passports #passport_data_title').show();
                                $(form).find('#edit_tourist_fields').slideDown();
                            }
                        } else {
                            let accordion = $(form).find('#m_accordion_tourist_passports');

                            $(accordion).empty();
                            $.each(response.passports, function( i, data ) {
                                let accordion_element = $('<div class="m-accordion__item">' +
                                    '<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_tourist_passports_head_item_'+i+'" data-toggle="collapse" href="#m_accordion_tourist_passports_body_item_'+i+'" aria-expanded="false">'+
                                    '<span class="m-accordion__item-icon"><label class="m-checkbox"><input type="checkbox" name="Tourist[selected_passport]" value="'+data.id+'"'+(data.id === response.tourist_passport_id ? ' checked="checked"' : '')+'>' +
                                    '<span></span></label></span><span class="m-accordion__item-title"></span><span class="m-accordion__item-mode"></span></div>'+
                                    '<div class="m-accordion__item-body collapse" id="m_accordion_tourist_passports_body_item_'+i+'" role="tabpanel" aria-labelledby="m_accordion_tourist_passports_head_item_'+i+'" data-parent="#m_accordion_tourist_passports">'+
                                    '<div class="m-accordion__item-content"></div></div></div>');
                                let tourist_fields = $($(form).find('#edit_tourist_fields').html());

                                $(tourist_fields).find('.edit_tourist_passport_type_select').closest('div.form-group').append($(tourist_fields).find('.edit_tourist_passport_type_select'));
                                $(tourist_fields).find('div.bootstrap-select').remove();
                                clear_tourist_fields(tourist_fields);
                                set_tourist_fields(data, tourist_fields,i);
                                $(accordion_element).find('.m-accordion__item-title').text(data.serial);

                                $(accordion_element).find('.m-accordion__item-content').append(tourist_fields);
                                $('#m_modal_edit_tourist_passports #passport_data_title').show();

                                $(accordion).append(accordion_element);
                            });
                            var count_form = ($(form).find('.edit_contact_passport_nationality')).length;
                            for(var y = 0; y<=count_form; y++){
                                $(form).find('.edit_contact_passport_nationality').eq(y).attr('data-select2-id', 'edit_passport_nationality_'+y);
                                $(form).find('.edit_contact_passport_nationality').eq(y).attr('id', 'edit_passport_nationality_'+y);
                            }
                            ajaxCall();
                            $(accordion).show();
                        }
                    },
                    error: function (response) {
                        let content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });

            $('#m_modal_edit_tourist_passports').on('hidden.bs.modal', function (e) {
                let form = $('#m_modal_edit_tourist_passports form#tourist_passport_form');
                $(form).find('#edit_tourist_fields').hide();
                $(form).find('#m_accordion_tourist_passports').hide();
            });

            $('#m_modal_edit_tourist_passports .btn-primary').click(function(e) {
                e.preventDefault();
                let btn = $(this);
                let form = $(this).closest('.modal-content').find('form');
                let id_tourist = $(this).data('tourist_id');

                function check_required_tourist() {
                    if ($('#m_modal_edit_tourist_passports input[name="Tourist[first_name][]"]').val().length > 0
                        || $('#m_modal_edit_tourist_passports input[name="Tourist[last_name][]"]').val().length > 0
                        || $('#m_modal_edit_tourist_passports input[name="Tourist[serial][]"]').val().length > 0
                        || $('#m_modal_edit_tourist_passports input[name="Tourist[date_limit][]"]').val().length > 0
                        || $('#m_modal_edit_tourist_passports input[name="Tourist[birth_date][]"]').val().length > 0)
                        return true;

                    return false;
                }

                // form.validate({
                //     rules: {
                //         'Contact[first_name]': {
                //             required: true
                //         },
                //         'Contact[last_name]': {
                //             required: true
                //         },
                //         'Tourist[passport_type][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         },
                //         'Tourist[first_name][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         },
                //         'Tourist[last_name][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         },
                //         'Tourist[serial][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         },
                //         'Tourist[date_limit][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         },
                //         'Tourist[birth_date][]': {
                //             required: function () {
                //                 return check_required_tourist();
                //             }
                //         }
                //     }
                // });
                //
                // if (!form.valid()) {
                //     return;
                // }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/orders/update-tourist-passports?tourist_id='+id_tourist,
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response) {
                                $('#m_modal_edit_tourist_passports').modal('hide');

                                location.reload();
                            }
                        }, 1000);
                    }
                });
            });

            $('#m_modal_edit_tourist_passports').on('click', '.m-accordion__item-icon', function(e) {
                if ($(this).find('input').prop('checked') == false) {
                    $('#m_modal_edit_tourist_passports .m-accordion__item-icon input').prop('checked', false);

                    $(this).find('input').prop('checked', true);
                } else {
                    $(this).find('input').prop('checked', true);
                }
            });
        }

        function initReminderSource() {
            function setRequestSource() {
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/set-request-source',
                    data: $('form.request_source').serializeArray(),
                    method: 'POST',
                    success: function (response){
                        $('#order_request_source .preloader').show();
                        $('#order_request_source .request_source').hide();

                        // similate 2s delay
                        setTimeout(function() {
                            if (response) {
                                let content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });

                                let value = response.value;

                                if (response.system_type === 'from_request') {
                                    value = GetTranslation('order_request') + ' #' + response.request_id;

                                    $('#order_select_request').empty().append('<option value="' + response.request_id + '">' + value + ' (' + response.request_contact_name + ')' + '</option>');
                                    $('#order_request_source #unlink_request_button').show();
                                } else {
                                    $('#order_select_request').empty();
                                    $('#order_request_source #unlink_request_button').hide();
                                }

                                $('#order_request_source a.m-dropdown__toggle').html(value);
                                $('#order_request_source .preloader').hide();
                                $('#order_request_source .request_source').show();

                                $('body, html').click();
                            }
                        }, 1500);
                    },
                    error: function (response) {
                        let content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            }

            $('#orders_request_sources').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/get-order-request-sources',
                    method: 'POST',
                    dataType: 'json',
                    delay: 1000,
                    processResults: function (data) {
                        let array = [];
                        for (let i in data) {
                            array.push({id: data[i]['id'], text: data[i]['value'], system_type: data[i]['system_type']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                minimumResultsForSearch: -1,
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation('order_select_request_source'),
                dropdownParent: $('#orders_request_sources').parents('.m-dropdown__inner')
            });

            $('#order_select_request').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-request',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        let query = {
                            search: params.term,
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        let array = [];
                        for (let i in data) {
                            array.push({id: data[i]['id'], text: data[i]['request_description']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation('order_select_request'),
                dropdownParent: $('#order_select_request').parents('.modal')
            });

            $('#order_request_source .editable-submit').on('click', function (e) {
                e.preventDefault();

                let selected_source_data = $('#orders_request_sources').select2('data')[0];

                $('.request_source input[name="Orders[request_id]"]').val('');

                if (selected_source_data.system_type === 'from_request' || $('#orders_request_sources').select2('data')[0].element.dataset.system_type === 'from_request') {
                    $('body, html').click();
                    $('#contact-request-source').modal('show');
                } else {
                    setRequestSource();
                }
            });

            $('#contact-request-source .btn-primary').on('click', function () {
                let request_id = $('#order_select_request').val();

                $('.request_source input[name="Orders[request_id]"]').val(request_id);
                $('#contact-request-source').modal('hide');
                setRequestSource();
            });

            $('#order_request_source #unlink_request_button').on('click', function (e) {
                e.preventDefault();
                $('#contact-request-source').modal('show');
            });
        }
        
        function initReservationModal(){
            $('#m_modal_create_order_reservation').on('shown.bs.modal', function(e){
                let modal = this;
                let button = e.relatedTarget;
                let reservation_url = $(e.relatedTarget).data('reservation-url');
                if(reservation_url !== undefined && reservation_url !== '' && reservation_url !== null){
                    let iframe =    '<iframe src="' + reservation_url + '" width="700" height="500">\
                                        ' + GetTranslation('iframe_browser_error', 'Your browser does not support iframe!') + '\
                                    </iframe>';
                    $(modal).find('.iframe-wrap').html(iframe);
                }
                $(modal).find('button.remove-tourist').on('click', function (e) {
                    let id_tourist = $(this).data('tourist_id');
                    let item = $(this).closest('tr');

                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/orders/delete-tourist?tourist_id=' + id_tourist,
                                method: 'GET',
                                success: function () {
                                    let content = {
                                        title: GetTranslation('success_message', 'Success!'),
                                        message: ''
                                    };
                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        z_index: 1031,
                                        type: "success"
                                    });
                                    item.remove();
                                },
                                error: function (response) {
                                    let content = {
                                        title: 'Error',
                                        message: response.responseText.replace(':', '')
                                    };

                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        type: "danger",
                                        z_index: 1031
                                    });
                                }
                            });
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                });
                $(modal).find('.copy-icon').on('click', function () {
                    let source_selector = $(this).data('copy-target-id');
                    let root = document.getElementById(source_selector);
                    let range = document.createRange();
                    range.setStart(root, 0);
                    range.setEnd(root, 1);
                    window.getSelection().empty();
                    window.getSelection().addRange(range);
                    try {
                        let successful = document.execCommand('copy');
                        window.getSelection().empty();
                        if(successful === true){
                            toastr.success(GetTranslation('ov_page_copy_credential_success_message', 'Text was copy in buffer'));
                        }else{
                            toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                        }
                    } catch (err) {
                        console.log(err);
                        toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                    }
                });
            });
            $('#m_modal_create_order_reservation').on('hidden.bs.modal', function(e){
                let modal = this;
                $(modal).find('.iframe-wrap').html('');
            });
        }

        function reminder_setdate() {

            $('#order-add-note-popup').on('show.bs.modal', function (e) {
                $('.note-toolbar-wrapper').css('height', '50px');
            });

            //$('#add-note-preload').hide();
            $('#add-note-redactor').summernote({
                height: 300,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            //$('#add-note-preload').hide();
            $('#edit-note-redactor').summernote({
                height: 300,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            // Due date picker init
            $('.reminder-date').find('.reminder_input__due_date').datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                orientation: "bottom left",
                format: date_format.toLowerCase(),
                language: tenant_lang,
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            // Due time picker init
            let format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            $('.reminder-date').find('.reminder_input__due_time').timepicker({
                minuteStep: 5,
                showSeconds: false,
                showMeridian: format_AM_PM,
                defaultTime: ''
            });
            // Event - change due date
            $('.reminder-date').find('.reminder_input__due_date').on('change', function(){
                updateDates($(this).closest('.reminder-date'));
            });
            // Event - change due time
            $('.reminder-date').find('.reminder_input__due_time').timepicker().on('changeTime.timepicker', function (e) {
                updateDates($(this).closest('.reminder-date'));
            });
            // Event - open due time
            $('.reminder-date').find('.reminder_input__due_time').timepicker().on('show.timepicker', function (e) {
                if(!e.time.value){
                    $(this).timepicker('setTime', '09:00');
                }
            });
            $('.btn-setdate-submit').on('click', function (e) {
                let reminder_date = $(this).closest('.reminder-date');
                let due_date_val = reminder_date.find('.reminder_input__due_date').val();
                let due_time_val = reminder_date.find('.reminder_input__due_time').val();
                let tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                if(!due_date_val){
                    reminder_date.find('.reminder_input__due_date').datepicker('setDate', tomorrow);
                }

                if(!due_time_val){
                    reminder_date.find('.reminder_input__due_time').timepicker('setTime', '09:00');
                }

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/reminder-set-date?id='+$(this).data('rid'),
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        due_date: reminder_date.find('.reminder_due_date_timestamp').val()

                    },
                    success:function (response) {
                        if (response) {
                            let content = {
                                title: GetTranslation('success_message', 'Success!'),
                                message: ''
                            };
                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1031,
                                type: "success"
                            });

                            $(reminder_date).find('.etitable-form-dropdown .la-calendar').attr('title', response.due_date_str);
                            $('body, html').click();
                        }
                    },
                    complete: function () {
                        // Toggle css calss 'active' for reminder buttons table cell,
                        // when it was clicked. It's need for correct dates dropdown display
                        $('#order-reminders-table').find('td.reminder-date').each(function(){
                            let table_cell = this;
                            $(table_cell).closest('tr').removeClass('active');
                        });
                    }
                });
            });
        }

        function initValidate(){
            // Phones
            let phones = $('a.phones');
            phones.each(function (i) {
                let phone = $(this).text();
                if (phone != null && phone.search(/(\d{2})(\d{3})(\d{3})(\d{4})/) >= 0) {
                    let result = phone.match(/(\d{2})(\d{3})(\d{3})(\d{4})/);
                    $(this).text('+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4]);
                } else {
                    $(this).text((phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone));
                }
            });

            $('#conctact-edit-popup-form')
                .find('[valid="phoneNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function(data){

                $('#contact-phones-repeater').find('.validate-phone').each(function(index, el) {
                    el = $(this);

                    // let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    let formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function () {
                        let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated)
                    });
                });
            });


            $('#conctact-edit-popup-form')
                .find('[valid="phoneNumber_add"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function(data){

                $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                    el = $(this);
                    // let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    let formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated);
                    });
                });
            });

        }

        function updateDates(selector) {
            /**
             * @let calculate_reminder_time : true|null - set true if need to auto calculate email reminder date
             */
            let format_AM_PM = (time_format === 'hh:mm:ss A') ? true : false;
            let due_date = moment(selector.find('.reminder_input__due_date').val(), date_format).format('X');
            let due_time = moment(selector.find('.reminder_input__due_time').val(), remove_seconds_from_format(time_format)).format('X');

            if (due_date !== 'Invalid date' && due_time !== 'Invalid date') {
                // Get full date timestamp
                let date = new Date(due_date * 1000);
                let time = new Date(due_time * 1000);
                let years = date.getFullYear();
                let months = date.getMonth()+1;
                let days = date.getDate();
                let hours = time.getHours();
                let minutes = time.getMinutes();
                let string_date = years + '.' + months + '.' + days + ' ' + hours + ':' + minutes;
                let full_date_timestamp = moment(string_date, 'YYYY.MM.DD HH:mm').format('X');
                // Add timestamp in hidden input
                selector.find('.reminder_due_date_timestamp').val(full_date_timestamp);
            } else {
                // Clear hidden timestamp input if no data
                selector.find('.reminder_due_date_timestamp').val('');
            }
        }

        function remove_seconds_from_format(format){
            let new_format = format.replace(':ss', '');
            return new_format;
        }

        function add_reminder_modal() {
            $('#add_reminder_btn').click(function(e) {
                e.preventDefault();
                let btn = $(this);
                let form = $(this).closest('form');

                let due_date_val = form.find('.reminder_input__due_date').val();
                let due_time_val = form.find('.reminder_input__due_time').val();
                let tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);

                if(!due_date_val){
                    form.find('.reminder_input__due_date').datepicker('setDate', tomorrow);
                }

                if(!due_time_val){
                    form.find('.reminder_input__due_time').timepicker('setTime', '09:00');
                }

                updateDates(form);

                form.validate({
                    rules: {
                        'Reminder[name]': {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/orders/add-reminder?id='+$(this).data('oid'),
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            if (response.name && response.due_date) {
                                let template = '<tr class="">' +
                                    '<td class="text-center checkbox-cell">' +
                                    '<label class="m-checkbox"><input type="checkbox" data-rid="' + response.id + '"><span></span></label></td>' +
                                    '<td class="reminder-text">' + response.name + '</td>' +
                                    '<td class="reminder-date">' +
                                    '<div><span>' + response.due_date_str + '</span></div></td></tr>';

                                $('#order-reminders-table tbody tr').last().before(template);
                                $('#order-add-reminder-popup').modal('hide');

                                $('#create_reminder_input__title, .reminder_due_date_timestamp, .reminder_input__due_date, .reminder_input__due_time').val(null);

                                initRemindersPartlet();
                            }
                        }, 1000);
                    }
                });
            });
        }

        function delete_note() {
            $('body').on('click', 'a[data-action="delete"]', function (e) {
                let id_note = $(this).data('id_note');
                let item = $(this).closest('.m-widget3__item');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/delete-note?id_note=' + id_note,
                            method: 'GET',
                            success: function (response) {
                                let content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                                item.remove();
                            },
                            error: function (response) {
                                let content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });
        }

        function edit_note() {
            let item, note_id;

            $('body').on('click', 'a[data-action="edit"]', function (e) {
                item = $(this).closest('.m-widget3__item');
                note_id = $(this).data('id_note');

                $('.note-toolbar-wrapper').css('height', '50px');

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-note?id_note=' + note_id,
                    method: 'GET',
                    success: function (response) {
                        $('#edit-note-redactor').summernote('code', response.value);
                    },
                    error: function (response) {
                        let content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });

            $('#edit-note-save-button').click(function (e) {
                e.preventDefault();

                let btn = $(this);
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $('#order-edit-note-popup form').ajaxSubmit({
                    method: 'POST',
                    url: baseUrl + '/ita/' + tenantId + '/orders/edit-note?id_note=' + note_id,
                    success: function (response, status, xhr, $form) {
                        setTimeout(function () {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            $('#order-edit-note-popup').modal('hide');

                            if (response.id) {
                                item.find('.m-widget3__body').empty().html(response.value);

                                let content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                            }
                        }, 1000);
                    },
                    error: function (response) {
                        let content = {
                            title: 'Error',
                            message: response.responseText.replace(':', '')
                        };

                        $.notify(content, {
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            type: "danger",
                            z_index: 1031
                        });
                    }
                });
            });
        }
        
        function initPageWidgets(){
            
            // Contact modal repeaters
            $('#contact-phones-repeater, #contact-emails-repeater, #contact-socials-repeater, #contact-messengers-repeater, #contact-sites-repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'text-input': 'foo'
                },
                show: function () {
                    $(this).slideDown();
                    //Init maskinput
                    $('#conctact-edit-popup-form')
                        .find('[valid="phoneNumber_add"]')
                        .intlTelInput({
                            utilsScript: "/plugins/phone-validator/build/js/utils.js",
                            autoPlaceholder: true,
                            preferredCountries: ['ua', 'ru'],
                            allowExtensions: false,
                            autoFormat: true,
                            autoHideDialCode: true,
                            customPlaceholder: null,
                            defaultCountry: "ua",
                            geoIpLookup: null,
                            nationalMode: false,
                            numberType: "MOBILE"
                        }).then(function(data){

                        $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                            el = $(this);
                            // let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                            let formated = '+999 99 999 9999';
                            $(el).inputmask(formated);
                            $(el).on("countrychange", function (e, countryData) {
                                let formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                $(el).inputmask(formated)
                            });
                        });
                    });
                    $(this).find('input').each(function () {
                        let mask_data = $(this).data('inputmask');
                        if (mask_data) {
                            $(this).inputmask();
                        }
                    });
                    //Init dropdown option
                    initExtraDropdownOptionInputs(this);
                },
                hide: function (deleteElement) {
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('delete_element_confirmation_text', 'Are you sure you want to delete this element?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                }
            });
            
            // Tags select
            $('#contacts-modal-tags-search-select-req').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        let query = {
                            search: params.term,
                            type: 'public'
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        let array = [];
                        for (let i in data) {
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("conctact-edit-popup-add-tag-plaseholder-req", "Add a tag"),
                tags: true
            });
            
            // Responsible select
            $('#select2-responsible-ajax-search').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-responsible-options',
                    method: 'POST',
                    dataType: 'json',
                        data: function (params) {
                        let query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        let array = [];
                        for (let i in data){
                            let full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("ov_page_settings-partlet-responsible_select_placeholder", "Select responsible")
            });

            $('#select2-responsible-ajax-search').on('change', function(){
                let order_id = $('#select2-responsible-ajax-search-order-id-hint').val();
                let selected = $(this).find("option:selected").val();
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/change-responsible',
                    data: {
                        order_id: order_id,
                        responsible_id: selected,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method:'POST',
                    success:function (response) {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if(response.result === 'success'){
                            toastr.success(response.message);
                        }
                        if(response.result === 'error'){
                            toastr.error(response.message);
                        }
                    }
                });
            });
            
            // Unlink order connection button
            let unlink_btn = $('#unlink_button').get()[0];
            $(unlink_btn).on('click', function(){
                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    text: GetTranslation('ov_page_unlink_connection_question', 'Unlink this connection?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('ov_page_unlink_confirmation', 'Yes, unlink it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        // Make something
                    }
                });
            });

            $('#order_change_contact').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        let query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        let array = [];
                        for (let i in data){
                            array.push({id: data[i]['id'], text: data[i]['first_name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation('order_select_new_contact'),
                width: '100%',
                dropdownParent: $('#order_change_contact').parents('.modal-body')
            });
            
            // Set exchange reate inputs
            $('.exchange_rate_summ_mask').inputmask({
                "regex": '^-?\\d+(\\.\\d{1,2})?$'
            });
            
        }

        function initDeleteForm() {
            let form = $('#order-view-delete-form').get()[0];
            $(form).on('submit', function (event) {
                if (!$(form).data('confirmed')) {
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('order_delete_question', 'Delete this order?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value === true) {
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }
        
        function initExtraDropdownOptionInputs(added_row){
            if (added_row){
                //Init default dropdown value
                let group = $(added_row).find('.extra-dropdown-option')[0];
                let hidden_input_selector = $(group).data('for-input-selector');
                let hidden_input = $(group).find(hidden_input_selector)[0];
                let default_val = $(hidden_input).data('default-value');
                $(hidden_input).val(default_val).removeAttr('data-default-value');
            }
            $('.extra-dropdown-option').each(function(){
                let group = this;
                let hidden_input_selector = $(group).data('for-input-selector');
                let hidden_input = $(group).find(hidden_input_selector)[0];
                let drop_down_btn = $(group).find('button.btn')[0];
                $(group).find('.dropdown-menu a').each(function(){
                    let variant = this;
                    let variant_name = $(this).text();
                    let variant_val = $(this).data('value');
                    $(variant).unbind('click');
                    $(variant).on('click', function(event){
                        event.preventDefault();
                        $(hidden_input).val(variant_val);
                        $(drop_down_btn).text(variant_name);
                    });
                });
            });
        }
        
        function initRemindersPartlet(){
            // Init vars
            let table = $('#order-reminders-table').get()[0];
            // Setup toast messages
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "600",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            // Add events for checkboxes
            $(table).find('td.checkbox-cell input[type=checkbox]').each(function(){
                let checkbox = this;

                $(checkbox).off('change');
                $(checkbox).on('change', function(){
                    let row = $(this).closest('tr')[0];

                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/reminder-checked?id='+$(this).data('rid'),
                        method: 'GET',
                        success:function (response) {
                            if(checkbox.checked == true) {
                                $(row).addClass('checked-row');
                                $('body, html').click();
                                $(row).find('.new-data-set').css('cursor', 'not-allowed').removeClass('m-dropdown__toggle');
                                toastr.success(GetTranslation('order_closed_reminder'));     // ADD YOUR LOGIC
                            } else {
                                $(row).removeClass('checked-row');
                                $(row).find('.new-data-set').css('cursor', 'pointer').addClass('m-dropdown__toggle');
                                toastr.success(GetTranslation('order_opened_reminder'));   // ADD YOUR LOGIC
                            }
                        }
                    });
                });
            });
            
            // Init delete button
            $(table).find('.delete-reminder-icon').on('click', function(){
                let link = this;
                let reminder_id = $(link).data('rid');
                if(reminder_id !== undefined && reminder_id !== null){
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/orders/delete-reminder?reminder_id=' + reminder_id,
                                method: 'GET',
                                success: function (response) {
                                    
                                },
                                error: function (response) {
                                    
                                }
                            });
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                }
            });
            
            // Toggle css calss 'active' for reminder buttons table cell,
            // when it was clicked. It's need for correct dates dropdown display
            $(table).find('td.reminder-date').each(function(){
                let table_cell = this;
                $(table_cell).find('.etitable-form-dropdown').on('click', function(){
                    $(table).find('tr').removeClass('active');
                    $(this).closest('tr').addClass('active');
                });
                $('body').on('click', function(){
                    if(!$(table_cell).find('.etitable-form-dropdown').is(':hover')){
                        $(table_cell).closest('tr').removeClass('active');
                    }
                });
            });
        }
        
        function initOpeners() {
            $('[data-opener="true"]').each(function () {
                let opener = this;
                let is_already_inited = $(opener).data('inited');
                if (is_already_inited !== 'true') {
                    let target_selector = $(opener).data('target');
                    $(opener).on('click', function (event) {
                        event.preventDefault();
                        activateTogleEvent(opener, target_selector);
                    });
                    activateTogleEvent(opener, target_selector);
                    $(opener).data('inited', 'true');
                }
            });
            function activateTogleEvent(opener, target_selector) {
                let status = $(opener).data('status');
                $(target_selector).each(function () {
                    let target = this;
                    switch (status) {
                        case 'close':
                            makeOpen(opener, target);
                            break;
                        case 'open':
                            makeClose(opener, target);
                            break;
                    }
                });
            }

            function makeOpen(opener, target) {
                $(target).stop().fadeIn();
                $(target).removeClass('close-by-opener').addClass('open-by-opener');
                $(opener).find('[data-show_decorator="for-close"]').hide();
                $(opener).find('[data-show_decorator="for-open"]').show();
                $(opener).data('status', 'open');
            }

            function makeClose(opener, target) {
                $(target).stop().fadeOut();
                $(target).removeClass('open-by-opener').addClass('close-by-opener');
                $(opener).find('[data-show_decorator="for-close"]').show();
                $(opener).find('[data-show_decorator="for-open"]').hide();
                $(opener).data('status', 'close');
            }
        }
        
        function initDocumentViewModal(){
            let editor_height = (window.innerHeight - 255) < 500 ? 500 : window.innerHeight - 255;

            let config = {
                customConfig: '',
                allowedContent: true,
                disallowedContent: 'img{width,height,float}',
                extraAllowedContent: 'img[width,height,align]',
                extraPlugins: 'placeholder_edit',
                height: editor_height,
                contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css', baseUrl + '/admin/plugins/ckeditor-full/document-style.css'],
                bodyClass: 'document-editor',
                format_tags: 'p;h1;h2;h3;pre',
                removeDialogTabs: 'image:advanced;link:advanced',
                stylesSet: [
                    {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                    {name: 'Cited Work', element: 'cite'},
                    {name: 'Inline Quotation', element: 'q'},
                    {
                        name: 'Special Container',
                        element: 'div',
                        styles: {
                            padding: '5px 10px',
                            background: '#eee',
                            border: '1px solid #ccc'
                        }
                    },
                    {
                        name: 'Compact table',
                        element: 'table',
                        attributes: {
                            cellpadding: '5',
                            cellspacing: '0',
                            border: '1',
                            bordercolor: '#ccc'
                        },
                        styles: {
                            'border-collapse': 'collapse'
                        }
                    },
                    {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                    {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
                ],
                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    // '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    // '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },

                ],
                removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
            };
            
            CKEDITOR.replace('doc-editor-block', config);
        }
        
        function initAddPaymentModal(){
            // Payment date picker
            let payment_date_select = $('#add_payment_form_date').get()[0];
            $(payment_date_select).datepicker({
                weekStart: +week_start,
                todayHighlight: true,
                format: date_format_datepicker,
                language: tenant_lang,
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });
            $(payment_date_select).datepicker("setDate", new Date());   // Set today date
            
            // Currency mask input
            $('.currency_mask').inputmask({
                "regex": '^-?\\d+(\\.\\d{1,2})?$'
            });
            
            // Payment note textarea
            let textarea_jq_selector = $('#payment_note_form_textarea');
            autosize(textarea_jq_selector);
        }
        
        function initEditTouristsModal(){
            function initDatePicker(edit) {
                let date_limit = $('#tourist_passport_date_limit_picker');
                let birth_date = $('#tourist_passport_birth_date_picker');
                let issued = $('#passport_form_issued_date_datepicker');

                if (edit === true) {
                    date_limit = $('.edit_tourist_passport_date_limit_picker');
                    birth_date = $('.edit_tourist_passport_birth_date_picker');
                    issued = $('#edit_passport_form_issued_date_datepicker');
                }

                // Passport date limit datepicker
                date_limit.datepicker({
                    weekStart: +week_start,
                    todayHighlight: true,
                    format: date_format_datepicker,
                    language: tenant_lang,
                    autoclose: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });
                birth_date.datepicker({
                    weekStart: +week_start,
                    todayHighlight: true,
                    format: date_format_datepicker,
                    language: tenant_lang,
                    autoclose: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });
                issued.datepicker({
                    weekStart: +week_start,
                    todayHighlight: true,
                    format: date_format_datepicker,
                    language: tenant_lang,
                    autoclose: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });
            }

            initDatePicker(false);
            initDatePicker(true);
        }

        function initCopyBtn(modal){
            $('.copy-service').off('click').on('click', function () {
                let element = $(this).closest('.additional-filed');

                let service_id = element.attr('data-service-id');
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                    data: {
                        service_id: service_id,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    success: function (response) {
                        $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                        $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                            let portlet = new mPortlet(this);
                            portlet.expand();
                        });
                        add_count++;
                        initDynamicInputs(modal);
                        initCopyBtn(modal);
                        initNextBtn(modal);
                    }
                });
            });
        }

        function initNextBtn(modal) {
            $('#btn-next').off('click').on('click', function () {
                $(modal).find('.select-additional').each(function () {
                    if ($(this).prop('checked')) {
                        let service_id = $(this).val();
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: service_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                    let portlet = new mPortlet(this);
                                });
                                add_count++;
                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal)
                            }
                        });
                    }
                });
                $(this).hide();
                $('#service-additional-services-portlet-block').hide();
                $('#btn-save').show();
                $('#add-srv-btn').empty();
                // Get service data
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                    data: {
                        service_id: $(this).attr('data-service-id'),
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    success: function (response) {
                        $('#add-srv-btn').append(renderAddServicesList(response['links']));
                        $('.add-service').on('click', function () {
                            let service_id = $(this).attr('data-service-id');
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                data: {
                                    service_id: service_id,
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                },
                                method: 'POST',
                                success: function (response) {
                                    $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                    $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                        let portlet = new mPortlet(this);
                                        portlet.expand();
                                    });
                                    add_count++;
                                    initDynamicInputs(modal);
                                    initCopyBtn(modal);
                                    initNextBtn(modal);
                                }
                            });
                        });
                    }
                });
            });
        }
        
        function initServicesPopupWidgets() {
            $('#btn-save').off('click').on('click', function () {
                let form = $(this).closest('.modal-content').find('form');
                let formData = new FormData(form[0]);

                $(this).attr('disabled', true);
                if ($(this).attr('data-type') == 'create') {
                    $('#m_modal_service_details').find('.modal-body')
                        .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/add-service',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            // $('#btn-save').attr('disabled', false);
                            window.location.reload();
                        },
                        error: function (data) {
                            $('#btn-save').attr('disabled', false);
                            let content = {
                                title: 'Error',
                                message: 'Error'
                            };
                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1031,
                                type: "success"
                            });
                        }
                    });
                } else {
                    $('#m_modal_service_details').find('.modal-body')
                        .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/update-service',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            // $('#btn-save').attr('disabled', false);
                            // $('#m_modal_service_details').modal('hide');
                            window.location.reload();
                        },
                        error: function (data) {
                            $('#btn-save').attr('disabled', false);
                            let content = {
                                title: 'Error',
                                message: 'Error'
                            };
                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                z_index: 1031,
                                type: "success"
                            });
                            // window.location.reload();
                        }
                    });
                }
            });

            $('.remove-service').on('click', function () {
                let service_id = $(this).attr('data-id');
                let item = $(this).closest('.product-item');
                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    // text: button.data('message'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/delete-service?id=' + service_id,
                            method: 'GET',
                            success: function (response) {
                                let content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                                item.remove();
                            },
                            error: function (response) {
                                let mes = response;
                                if (response.error != undefined)
                                    mes = response.error;
                                if (response.message != undefined)
                                    mes = response.message;
                                if (response.responseText != undefined)
                                    mes = response.responseText;
                                if (response.text != undefined)
                                    mes = response.text;
                                let content = {
                                    title: 'Error',
                                    message: mes
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });

            let count = 0;
            let tourist_passports = {};

            function renderTourist(tourist) {
                let html = '';
                count++;

                tourist_passports[tourist.id] = tourist;

                if (tourist.empty_tourist !== undefined) {
                    html += renderEmptyTourist(tourist.type);
                } else {
                    if (tourist.passport !== undefined && tourist.passport.serial !== undefined) {
                        html += '<div class="row mb-3"> ' +
                            '<div class="tourist-item col-12"> ' +
                            '<span class="tourist-item-text"> ' +
                            '<i class="fa fa-check m--font-success"></i>' +
                            '<input type="hidden" class="tid" name="Tourists[' + count + '][id]" value="' + tourist.id + '">' +
                            (tourist.order_tourist_id ? '<input type="hidden" name="Tourists[' + count + '][order_tourist_id]" value="' + tourist.order_tourist_id + '">' : '') +
                            '<input type="hidden" name="Tourists[' + count + '][passport_id]" value="' + tourist.passport.id + '">' +
                            '<input type="hidden" name="Tourists[' + count + '][type]" value="' + tourist.type + '">' +
                            '<span class="name m--font-brand m--font-bold ml-2 edit-tourist" data-toggle="modal" data-source="tourist" data-target="#m_modal_edit_tourists" data-type="' + tourist.type + '" style="cursor: pointer;">' + tourist.passport.first_name + ' ' + tourist.passport.last_name + ' ' +
                            tourist.passport.serial + ' ' + GetTranslation('order_tourists_passport_limit') + ' ' + tourist.passport.date_limit + '</span> ' +
                            '<i class="fa fa-times m--font-danger ml-2 delete_contact" data-type="' + tourist.type + '" title="Delete"></i> ' +
                            '</span> ' +
                            '</div> ' +
                            '</div>';
                    } else {
                        html += '<div class="row mb-3"> ' +
                            '<div class="tourist-item col-12"> ' +
                            '<span class="tourist-item-text"> ' +
                            '<i class="fa fa-check m--font-success"></i>' +
                            (tourist.order_tourist_id ? '<input type="hidden" name="Tourists[' + count + '][order_tourist_id]" value="' + tourist.order_tourist_id + '">' : '') +
                            '<input type="hidden" class="tid" name="Tourists[' + count + '][id]" value="' + tourist.id + '">' +
                            '<input type="hidden" name="Tourists[' + count + '][type]" value="' + tourist.type + '">' +
                            '<span class="name m--font-brand m--font-bold ml-2 edit-tourist" data-toggle="modal" data-source="tourist" data-target="#m_modal_edit_tourists" data-type="' + tourist.type + '" style="cursor: pointer;">' + tourist.first_name + ' ' + tourist.last_name + '</span> ' +
                            '<i class="fa fa-times m--font-danger ml-2 delete_contact" data-type="' + tourist.type + '" title="Delete"></i> ' +
                            '</span> ' +
                            '</div> ' +
                            '</div>';
                    }
                }

                return html;
            }

            function renderEmptyTourist(type)
            {
                let html = '';
                html += '<div class="row align-items-center mb-3"> ' +
                    '<div class="tourist-item col-5"> ' +
                    '<select class="tourist-search-select" data-type="'+type+'"></select> ' +
                    '</div> ' +
                    '<div class="col-7"> ' +
                    '<span href="#" class="m-link add_new_contact_button" data-toggle="modal" data-source="service" data-type="'+type+'" data-target="#m_modal_edit_tourists" style="cursor: pointer;"> ' +
                    '+&nbsp ' +
                    GetTranslation("order_new_contact", "New contact") +
                    '</span> ' +
                    '</div> ' +
                    '</div>';
                return html;
            }

            function renderHeader(adult_count,children_count) {
                let html = '';
                html += ' <div class="row tourists-count-wrap align-items-end mb-3">' +
                '<div class="col-5 col-lg-2 no-padding-right">' +
                '<label>' +
                    GetTranslation("ov_service_tourists_adult_persons_label", "Adults") +
                '</label> ' +
                '<select class="form-control m-bootstrap-select m_selectpicker tourist-adult-count" name="Adults_count"> ';
                for(let i = 0;i<=10;i++)
                    html += '<option value="'+i+'" '+(i==adult_count?'selected':'')+'>'+i+'</option>';
                html +='</select> ' +
                '</div> ' +
                '<div class="col-1 mb-2 text-center" style="padding: 0;"> ' +
                '<i class="fas fa-plus"></i> ' +
                '</div> ' +
                '<div class="col-5 col-lg-2 no-padding-left"> ' +
                '<label> ' +
                    GetTranslation("ov_service_tourists_children_persons_label", "Children") +
                '</label> ' +
                '<select class="form-control m-bootstrap-select m_selectpicker tourist-children-count" name="Children_count"> ';
                for(i = 0;i<=10;i++)
                    html += '<option value="'+i+'" '+(i==children_count?'selected':'')+'>'+i+'</option>';
                html +='</select> ' +
                '</div> ' +
                '</div>';
                return html;
            }

            function renderTourists(tourists,adult_count,children_count) {
               let adult = 0;
               let child = 0;
               let result = '';
               for(I in tourists) {
                   if(tourists[I].type == 0)
                       adult++;
                   else
                       child++;
                   result += (renderTourist(tourists[I]));
               }
               if((adult+child)<(adult_count+children_count)){
                   // let left = (adult_count+children_count) - (adult+child);
                   for(let i = child; i<children_count;i++)
                       result += renderEmptyTourist(1);
                   for(let i = adult; i<adult_count;i++)
                       result += renderEmptyTourist(0);
               }
               else
               {
                   adult_count = adult;
                   children_count = child;
               }
               let header = renderHeader(adult_count,children_count);
               return '<div class="container-fluid">'+header+'<div id="tourists_list">'+result+'</div></div>';

            }

            function reRenderTr(old,nw,type) {
                if(old>nw) {
                    let diff = old - nw;
                    $('#tourists_list').find('.tourist-search-select[data-type="'+type+'"]').each(function () {
                        if(diff!=0){
                            $(this).closest('.row').remove();
                            diff--;
                        }
                    });
                    $('#tourists_list').find('.delete_contact[data-type="'+type+'"]').each(function () {
                        if(diff!=0){
                            $(this).closest('.row').remove();
                            diff--;
                        }
                    })
                }
                else {
                    for(let i = old;i<nw;i++)
                        $('#tourists_list').append(renderEmptyTourist(type));
                }
            }

            function calculateTourists() {
                let n_adult = $('.tourist-adult-count>option:selected').val();
                let n_child = $('.tourist-children-count>option:selected').val();
                let o_adult = $('#tourists_list').find('.delete_contact[data-type="0"]').length;
                o_adult += $('#tourists_list').find('.tourist-search-select[data-type="0"]').length;
                let o_child = $('#tourists_list').find('.delete_contact[data-type="1"]').length;
                o_child += $('#tourists_list').find('.tourist-search-select[data-type="1"]').length;

                reRenderTr(o_adult,n_adult,0);
                reRenderTr(o_child,n_child,1);
                initTouristsBlock();
            }

            $('#service-tourists-details').on('click', '#tourists_list span.edit-tourist', function (e) {
                let tid = $(this).closest('.tourist-item').find('.tid').val();
                let data = tourist_passports[tid];
                let fields = $('#m_modal_edit_tourists form');

                $(fields).find('#tourist_contact_id').val(data.id);
                $(fields).find('input[name="Contact[first_name]"]').val(data.first_name);
                $(fields).find('input[name="Contact[last_name]"]').val(data.last_name);

                if (data.passport) {
                    $('input.serial_passport_input').data('passport_id', data.passport.id);
                    $(fields).find('#tourist_passport_pid').val(data.passport.id);
                    $(fields).find('#tourist_passport_type_select').val(data.passport.type.id).selectpicker('refresh');
                    $(fields).find('#tourist_passport_first_name_select').val(data.passport.first_name);
                    $(fields).find('#tourist_passport_last_name_select').val(data.passport.last_name);
                    $(fields).find('#tourist_passport_serial_select').val(data.passport.serial);
                    $(fields).find('#passport_form_issued_owner').val(data.passport.issued_owner);
                    $(fields).find('#tourist_passport_date_limit_picker').datepicker('setDate', data.passport.date_limit);
                    $(fields).find('#tourist_passport_birth_date_picker').datepicker('setDate', data.passport.birth_date);
                    $(fields).find('#passport_form_issued_date_datepicker').datepicker('setDate', data.passport.issued_date);
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/orders/get-country-name?country_name=' + data.passport.nationality,
                        success: function(result){
                            var newOption = new Option(result, data.passport.nationality, true, true);
                            $('#ContactPassport_nationality').append(newOption).trigger('change');
                        }})

                } else {
                    $('input.serial_passport_input').data('passport_id', '');

                    $(fields).find('#tourist_passport_pid').val();
                    $(fields).find('#tourist_passport_type_select').val().selectpicker('refresh');
                    $(fields).find('#tourist_passport_first_name_select').val();
                    $(fields).find('#tourist_passport_last_name_select').val();
                    $(fields).find('#tourist_passport_serial_select').val();
                    $(fields).find('#tourist_passport_date_limit_picker').val();
                    $(fields).find('#tourist_passport_birth_date_picker').val();
                    $(fields).find('#passport_form_issued_date_datepicker').val();
                    $(fields).find('#tourist_passport_birth_date_picker').val();
                    $(fields).find('#ContactPassport_nationality').val();
                }
            });

            $('#m_modal_edit_tourists').on('show.bs.modal', function (event){
                if (event.relatedTarget === undefined)
                    return;
                let button = $(event.relatedTarget);
                if (button.data('source') == 'service' || button.data('source') == 'tourist' || button.data('source') == 'tourist-passport'){
                    if (button.data('source') != 'tourist-passport') {
                        $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[first_name]"]').removeAttr('disabled');
                        $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[last_name]"]').removeAttr('disabled');
                    }

                    if (button.data('source') != 'tourist')
                        $('input.serial_passport_input').data('passport_id', '');


                    $(this).find('#create_tourist_submit').off('click').on('click', function (e) {
                        e.preventDefault();

                        let btn = $(this);
                        let form = $(this).closest('.modal-content').find('form');
                        let data = $('#tourist_passport_form').serializeArray().reduce(function(obj, item) {
                            obj[item.name] = item.value;
                            return obj;
                        }, {});

                        function check_required_tourist() {
                            if (button.data('source') == 'tourist-passport')
                                return true;

                            if ($('#m_modal_edit_tourists input[name="Tourist[first_name]"]').val().length > 0
                                || $('#m_modal_edit_tourists input[name="Tourist[last_name]"]').val().length > 0
                                || $('#m_modal_edit_tourists input[name="Tourist[serial]"]').val().length > 0
                                || $('#m_modal_edit_tourists input[name="Tourist[date_limit]"]').val().length > 0
                                || $('#m_modal_edit_tourists input[name="Tourist[birth_date]"]').val().length > 0)
                                return true;

                            return false;
                        }

                        // form.validate({
                        //     rules: {
                        //         'Contact[first_name]': {
                        //             required: true
                        //         },
                        //         'Contact[last_name]': {
                        //             required: true
                        //         },
                        //         'Tourist[passport_type]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         },
                        //         'Tourist[first_name]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         },
                        //         'Tourist[last_name]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         },
                        //         'Tourist[serial]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         },
                        //         'Tourist[date_limit]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         },
                        //         'Tourist[birth_date]': {
                        //             required: function () {
                        //                 return check_required_tourist();
                        //             }
                        //         }
                        //     }
                        // });
                        //
                        // if (!form.valid()) {
                        //     return;
                        // }

                        btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                        form.ajaxSubmit({
                            url: baseUrl + '/ita/' + tenantId + '/orders/' + (button.data('source') != 'tourist' ? 'create-tourist' : 'update-tourist'),
                            method: 'POST',
                            dataType: 'json',
                            data: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                Contact: {
                                    first_name: $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[first_name]"]').val(),
                                    last_name: $('#m_modal_edit_tourists #tourist_passport_form input[name="Contact[last_name]"]').val()
                                }
                            },
                            success: function(data, status, xhr, $form) {
                                // similate 2s delay
                                setTimeout(function() {
                                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                                    $('#m_modal_edit_tourists').modal('hide');
                                    $('input.serial_passport_input').data('passport_id', '');

                                    if (button.data('source') == 'tourist-passport') {
                                        location.reload();
                                    } else {
                                        if ($('#m_modal_add_tourist').is(':visible')) {
                                            $('#add_tourist_tourist_select').append('<option value="' + data.id + '">' + (data.passport && data.passport.id ? data.passport.first_name + ' ' + data.passport.last_name : data.first_name + ' ' + data.last_name) + '</option>');

                                            if (data.passport && data.passport.id)
                                                $('#m_modal_add_tourist #add_tourist_form input[name="passport_id"]').val(data.passport.id);

                                            $('#add_tourist_tourist_select').val(data.id).trigger('change');
                                        } else {
                                            element = button.closest('.row');

                                            if (data.type)
                                                data.type = button.data('type');

                                            if (button.data('source') == 'service') {
                                                $(renderTourist(data)).insertAfter(element);
                                            } else {
                                                $(element).replaceWith(renderTourist(data));
                                            }

                                            element.stop().slideUp(10, function () {
                                                $(this).remove();
                                            });

                                            initTouristsBlock();
                                        }

                                    }

                                    $('#tourist_passport_form input').each(function () {
                                        $(this).val('');
                                    });
                                }, 1000);
                            }
                        });
                    });
                }
            });

            $('#m_modal_edit_tourists').on('hidden.bs.modal', function (event) {
                let validator = $('#m_modal_edit_tourists #tourist_passport_form').validate();
                validator.resetForm();
            });

            let modal = $('#m_modal_service_details').get()[0];
            let custom_portlet_title = $('#service-custom-details-portlet-title').get()[0];
            let additional_services_portlet = $('#service-additional-services-portlet').get()[0];
            let default_inputs_block = $('#service-default-details-block').get()[0];
            let custom_inputs_block = $('#service-custom-details-block').get()[0];
            let additional_services_block = $('#service-additional-services-portlet-block').get()[0];
            $(modal).on('show.bs.modal', function (event) {
                $(modal).find('#btn-duplicate').hide();
                if (event.relatedTarget !== undefined) {
                    clearDefaultPortlet();
                    clearCustomPortlet();
                    clearAdditionalServicesPortlet();
                    clearBaseAdditionalServicesPortlet();
                    let button = $(event.relatedTarget);

                    if (button.data('serv-id') !== undefined) {
                        let service_id = parseInt(button.data('serv-id'));
                        $('#create-label').show();
                        $('#update-label').hide();
                        $('#btn-back').show();

                        // Get service data
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: service_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                let service = response;
                                cur_service = service;
                                let default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def);
                                let custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields);

                                let additional_services = response.links;
                                let additional_services_html = getAdditionalServicesHtmlTemplate(service.id, additional_services);
                                $(custom_portlet_title).text(service.name);
                                $(default_inputs_block).html(default_inputs_html);
                                $(custom_inputs_block).html(custom_inputs_html);

                                // Additional services partlet status
                                if (additional_services_html.length > 0) {
                                    $(additional_services_block).html(additional_services_html).show();
                                    $(additional_services_portlet).show();
                                    $('#btn-next').attr('data-service-id', service_id);
                                } else {
                                    $(additional_services_portlet).hide();
                                    // $('#btn-back').hide();
                                    $('#btn-next').hide();

                                    $('#btn-save').show().attr('data-type', 'create');
                                }

                                let tourists = renderTourists([],1,0);
                                $('#service-tourists-details').empty().append(tourists);
                                initTouristsBlock();

                                if(service.multiservices) {
                                    let btn =  $(modal).find('#btn-duplicate');
                                    btn.show();
                                    btn.text(service.multiservices_name);
                                    btn.off('click').on('click',function () {
                                        $('#service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                        add_count ++;
                                        initDynamicInputs(modal);
                                        initCopyBtn(modal);
                                        initNextBtn(modal);
                                    });
                                }
                                else {
                                    $(modal).find('#btn-duplicate').hide();
                                }

                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal);
                            },
                            error:function (response) {
                                cur_service = undefined;
                            }
                        });
                    }
                    else if (button.data('id') !== undefined) {
                        let link_id = parseInt(button.data('id'));
                        $('#create-label').hide();
                        $('#update-label').show();
                        $('#btn-back').hide();
                        $('#btn-next').hide();
                        $('#btn-save').show().attr('data-type', 'update');
                        $(additional_services_block).hide();
                        $('#service-additional-services-details').html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>')
                        // Get service data
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-orders-service-by-link-id',
                            data: {
                                link_id: link_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                let service = response;
                                cur_service = service;

                                let default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def, undefined, link_id);
                                let custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields, undefined, link_id);
                                let additional_services = response.links;

                                $(custom_portlet_title).text(service.name);
                                $(default_inputs_block).html(default_inputs_html);
                                $(custom_inputs_block).html(custom_inputs_html);

                                if(service.multiservices) {
                                    let btn =  $(modal).find('#btn-duplicate');
                                    btn.show();
                                    btn.text(service.multiservices_name);
                                    btn.off('click').on('click',function () {
                                        $('#service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                        add_count ++;
                                        initDynamicInputs(modal);
                                        initCopyBtn(modal);
                                        initNextBtn(modal);
                                    });
                                }
                                else {
                                    $(modal).find('#btn-duplicate').hide();
                                }
                                let tourists = renderTourists(service.tourists,service.adults_count,service.children_count);
                                $('#service-tourists-details').empty().append(tourists);
                                initTouristsBlock();

                                $('#service-additional-services-details').empty();
                                // let inputs = $('#service-default-details-block').find('select.select2-country');
                                // if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //     main_country = inputs.find('option:selected').clone();
                                // else {
                                //     inputs = $('#service-custom-details-block').find('select.select2-country');
                                //     if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //         main_country = inputs.find('option:selected').clone();
                                // }
                                for (key in additional_services) {
                                    if(!additional_services[key].type) {
                                        $('#service-additional-services-details').append(renderAdditional(additional_services[key], key,add_count));
                                        $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                            let portlet = new mPortlet(this);
                                        });
                                    } else {
                                        $('#service-duplicates').append(renderBaseAdditionalService(additional_services[key],add_count,key));
                                        add_count ++;
                                    }

                                    add_count++;
                                }
                                $('#add-srv-btn').empty();
                                $.ajax({
                                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                    data: {
                                        service_id: service.id,
                                        _csrf: $('meta[name="csrf-token"]').attr('content')
                                    },
                                    method: 'POST',
                                    success: function (response) {
                                        $('#add-srv-btn').append(renderAddServicesList(response['links']));
                                        if (response['links'] == undefined || response['links'].length == 0)
                                            $('#service-additional-services-portlet').hide();
                                        else
                                            $('#service-additional-services-portlet').show();
                                        $('.add-service').on('click', function () {
                                            let service_id = $(this).attr('data-service-id');
                                            $.ajax({
                                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                                data: {
                                                    service_id: service_id,
                                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                                },
                                                method: 'POST',
                                                success: function (response) {
                                                    $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                                    $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                                        let portlet = new mPortlet(this);
                                                    });
                                                    setDefault($('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]'));
                                                    add_count++;
                                                    initDynamicInputs(modal);
                                                    initCopyBtn(modal);
                                                    initNextBtn(modal);
                                                }
                                            });
                                        });
                                    }
                                });
                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal);
                            },
                            error:function (response) {
                                cur_service = undefined;
                            }
                        });
                    }
                }
            });

            let tourists = [];

            function initTouristsBlock() {
                let tourists_block = $('#service-tourists-details').get()[0];
                $(tourists_block).find('.tourist-search-select').each(function(){
                    initTouristsSelect2(this);
                });
                $(tourists_block).find('.delete_contact').off('click').on('click',function() {
                    let type = $(this).attr('data-type');
                    element = $(this).closest('.row');
                    $(renderEmptyTourist(type)).insertAfter(element);
                    $(this).closest('.row').stop().slideUp(10, function(){
                        $(this).remove();
                    });
                    initTouristsBlock()
                });
                // $('.add_new_contact_button').off('click').on('click', function(){
                //     $('#m_modal_edit_tourists').modal('show');
                // });

                $('.tourist-adult-count').off('change',calculateTourists)
                    .on('change',calculateTourists);
                $('.tourist-children-count').off('change',calculateTourists)
                    .on('change',calculateTourists);

                function initTouristsSelect2(select) {
                    if($(select).attr('data-inited')==1)
                        return;
                    $(select).attr('data-inited',1);
                    $(select).select2({
                        ajax: {
                            url: baseUrl + '/ita/' + tenantId + '/orders/search-contacts',
                            method: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                let query = {
                                    search: params.term,
                                    type: 'public',
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                };
                                return query;
                            },
                            delay: 1000,
                            processResults: function (data) {
                                tourists = data;
                                let array = [];
                                for (let i in data) {
                                    let value = data[i];
                                    value.text = value.first_name+' '+value.last_name;
                                    array.push(value);
                                }
                                return {
                                    results: array
                                };
                            }
                        },
                        templateSelection: function (data) {
                            if (data.id === '') {
                                return GetTranslation("ov_services_tourists_contact_search_select_placeholder", "Select contact");
                            }
                            return data.text;
                        },
                        language: tenant_lang,
                        allowClear: true,
                        minimumInputLength: 2,
                        escapeMarkup: function (markup) {
                            return markup;
                        },
                        templateResult: function (perm) {
                            if (perm.loading) {
                                return perm.text;
                            }

                            let markup = '<div>' + perm.text + '</div>';

                            if (perm.passport && perm.passport.id) {
                                markup += '<div>' +
                                    '<p style="margin:0"><b>Date limit: </b><small>'+(perm.passport.date_limit ? perm.passport.date_limit : '-')+'</small></p>' +
                                    '<p style="margin:0"><b>Serial: </b><small>' + (perm.passport.serial ? perm.passport.serial : '-') + '</small></p>' +
                                    '</div>';
                            }

                            return markup;
                        },
                        width: '100%',
                        dropdownParent: $(select).closest('.modal-body'),
                        placeholder: GetTranslation("ov_services_tourists_contact_search_select_placeholder", "Select contact")
                    });

                    $(select).on('change',function () {
                        let tourist = tourists[$(this).val()];
                        tourist.type = $(this).attr('data-type');
                        let selected = renderTourist(tourist);
                        $(selected).insertAfter($(this).closest('.row'));
                        $(this).closest('.row').stop().slideUp(10, function(){
                            $(this).remove();
                        });
                        initTouristsBlock();
                    });
                }
            }

            function clearDefaultPortlet() {
                $(default_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
            }

            function clearCustomPortlet() {
                $(custom_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                $(custom_portlet_title).text('');
            }

            function clearBaseAdditionalServicesPortlet() {
                $('#service-duplicates').empty();
            }

            function clearAdditionalServicesPortlet() {
                add_count = 0;
                $('#add-srv-btn').empty();
                $('#service-additional-services-details').empty();
                $('#btn-next').show();
                $('#btn-save').hide().attr('data-type', 'create');
                $(additional_services_block).show();
                $(additional_services_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
            }
        }

        function updatePrPaymentInfo(id)
        {
            let provider = $('.provider-payment[data-link-id="'+id+'"]');
            provider.find('.m-loader').removeClass('d-none');
            provider.find('table').hide();
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/get-provider-payment-info',
                method: 'get',
                dataType: 'json',
                data: {
                    link_id:id,
                    order_id:$('#tourist-pay-partlet').data('order-id')
                },
                success: function (data) {
                    let not_set = GetTranslation('not_set', 'Not set');
                    provider.find('.provider-total-sum').text(data.total_sum==null?not_set:DisplayLocaleString(parseFloat(data.total_sum),data.payment_currency));
                    provider.find('.provider-payed').text(data.payed==null?not_set:DisplayLocaleString(parseFloat(data.payed),data.payment_currency));
                    provider.find('.provider-duty').text(data.duty==null?not_set:DisplayLocaleString(parseFloat(data.duty),data.payment_currency));
                    provider.find('.provider-due-date').text(data.due_date==null?not_set:data.due_date);
                    // provider.find('.provider-currency').text(data.currency==null?not_set:data.currency);
                    provider.find('.provider-ex-rates').text(data.rates==null?not_set:data.rates);
                    provider.find('.provider-commission').text(data.commission==null?not_set:data.commission);
                    if(data.status==0) {
                        provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--danger m-badge--wide">'+GetTranslation('payment_status_not_payed')+'</span>');
                        provider.find('.provider-payment-indicator').html('<span class="indicator danger"></span> <span class="indicator"></span> <span class="indicator"></span>');
                    }
                    if(data.status==1){
                        provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--warning m-badge--wide">'+GetTranslation('payment_status_partial')+'</span>');
                        provider.find('.provider-payment-indicator').html('<span class="indicator warning"></span> <span class="indicator warning"></span> <span class="indicator"></span>');
                    }

                    if(data.status==2) {
                        provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--success m-badge--wide">'+GetTranslation('payment_status_payed')+'</span>');
                        provider.find('.provider-payment-indicator').html('<span class="indicator success"></span> <span class="indicator success"></span> <span class="indicator success"></span>');
                    }
                    provider.find('.m-loader').addClass('d-none');
                    provider.find('table').show();
                }
            })
        }

        function updateDiscountValue(elem) {
            let value = elem.find('.value_input').val();
            if(value == '' || value === undefined)
                value = 0;
            let type = elem.find('.type_input').val();
            elem.find('.discount-value').val(JSON.stringify({value:value,type:type}));
        }

        $('.due_date_picker').datepicker({
            // weekStart: +week_start,
            todayHighlight: true,
            format: date_format_datepicker,
            language: tenant_lang,
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            // dropdownParent: $(input).parents('.m-accordion__item-content')
        });

        // $('.provider-payment').each(function () {
        //    updatePrPaymentInfo($(this).data('link-id'));
        // });

        $('#order_discount').on('click',function () {
            $('#dicount_input').empty().addClass('m-loader m-loader--primary');
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/get-order-discount',
                method: 'get',
                dataType: 'json',
                data: {
                    id:$('#tourist-pay-partlet').data('order-id')
                },
                success: function (data) {
                    $('#dicount_input').empty().removeClass('m-loader m-loader--primary');
                    let value = JSON.parse(data.value);
                    let input = '<div class="input-group extra-dropdown-option" data-for-input-selector=".tourist_discount_type"> ' +
                        '<input type="text" class="form-control m-input value_input" aria-label="Text input with dropdown button" value="'+value.value+'"> ' +
                        '<input value="" type="hidden" name="discount" class="form-control m-input discount-value" max="100"> ' +
                        '<input type="hidden" class="tourist_discount_type type_input" value="'+value.type+'"> ' +
                        '<div class="input-group-append"> ' +
                        '<button type="button" class="btn btn-secondary dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">%</button> ' +
                        '<div class="dropdown-menu"> ' +
                        '<a class="dropdown-item" href="#" data-value="per">%</a> ' +
                        '<a class="dropdown-item currency_indicator" href="#" data-value="cur">'+data.cur+'</a> ' +
                    '</div> ' +
                    '</div> ' +
                    '</div>';
                    $('#dicount_input').append(input).find('.discount-value').val(JSON.stringify(value));
                    $('#dicount_input').find('.dropdown-menu a').each(function(){
                        let variant = this;
                        $(variant).unbind('click');
                        $(variant).on('click', function(e){
                            e.preventDefault();
                            let variant_name = $(this).text();
                            let variant_val = $(this).data('value');
                            $('#dicount_input').find('.extra-dropdown-option').each(function () {
                                $(this).find('.type_input').val(variant_val);
                                let btn = $(this).find('button.btn');
                                btn.text(variant_name);
                                if(variant_val == 'cur')
                                    btn.addClass('currency_indicator');
                                else
                                    btn.removeClass('currency_indicator');
                                updateDiscountValue($('#dicount_input'));
                            }).find('.type_input').trigger('change');
                        });
                    });
                    $('.discount-value').parent().find('.value_input').off('keyup').on('keyup',function () {
                        updateDiscountValue($(this).parent());
                    });
                    $('.discount-value').parent().find('.type_input').off('change').on('change',function () {
                        updateDiscountValue($(this).parent());
                    });
                    $('#dicount_input').find('.value_input').inputmask({
                        // "mask": "9{+}.99"
                        "regex": '^-?\\d+(\\.\\d{1,2})?$'
                    })
                }
            });
        });

        $('#order_rates').on('click',function () {
            $('#rates_inputs').empty().addClass('m-loader m-loader--primary');
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/get-order-rates',
                method: 'get',
                dataType: 'json',
                data: {
                    id:$('#tourist-pay-partlet').data('order-id')
                },
                success: function (data) {
                    $('#rates_inputs').empty().removeClass('m-loader m-loader--primary');
                    for(I in data)
                    {
                        let input = '<div class="input-group m-input-group mb-3">' +
                            '<div class="input-group-prepend">' +
                            '<span class="input-group-text">' +
                            data[I].iso_code +
                            '</span>' +
                            '</div>' +
                            '<input type="text" name="Ex_rate['+data[I].id+']" ' +
                            'class="form-control m-input exchange_rate_summ_mask" ' +
                            'placeholder="' +
                            GetTranslation('ov_payment_enter_ex_rate', 'Set rate') +
                            '" value="'+data[I].value+'">' +
                            '</div>';
                        $('#rates_inputs').append(input);
                    }
                    $('#rates_inputs').find('.exchange_rate_summ_mask').inputmask({
                        // "mask": "9{+}.99"
                        "regex": '^-?\\d+(\\.\\d{1,2})?$'
                    })
                }
            });
        });


        window.initEditableForms('.edit-tourist-payments, .edit-provider-payments',function (response) {
            updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
        });

        // window.initEditableForms('.edit-provider-payments',function (response,parent_selector) {
        //     updatePrPaymentInfo($(parent_selector).closest('.provider-payment').data('link-id'));
        // });

        // $('.editable-submit-exch').on('click',function () {
        //     updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
        // });

        updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));

        $('#m_modal_add_payment').on('hidden.bs.modal',function(){$('#add_payment_form').empty();});
        $('#m_modal_add_payment').on('shown.bs.modal',function (e) {
            let modal = $(this);
            $('#m_modal_add_payment .m-loader').removeClass('d-none');
            $('#add_payment_form').empty();


            if($(e.relatedTarget).data('type')=='provider')
            {
                (new PaymentModal($(this),'provider',e.relatedTarget,$('#tourist-pay-partlet').data('order-id'))());
            } else {
                (new PaymentModal($(this),'tourist',e.relatedTarget,$('#tourist-pay-partlet').data('order-id'))());
            }
        });

    });

    function updatePaymentInfo(data) {
        let not_set = GetTranslation('not_set', 'Not set');
        $('#order_total_sum').text(data.sum==null?not_set:DisplayLocaleString(parseFloat(data.sum),data.cur.text));
        $('#order_discount').text(data.discount==null?not_set:data.discount);
        if(data.discount == null || data.sum == data.sum_wd)
            $('#order_total_sum_wd').closest('tr').hide();
        else
            $('#order_total_sum_wd').closest('tr').show();

        $('#order_total_sum_wd').text(data.sum_wd==null?not_set:DisplayLocaleString(parseFloat(data.sum_wd),data.cur.text));
        $('#order_payed').text(data.tourist_payed==null?not_set:DisplayLocaleString(parseFloat(data.tourist_payed),data.cur.text));
        $('#order_duty').text(data.duty==null?not_set:DisplayLocaleString(parseFloat(data.duty),data.currency));
        // $('#order_due_date').text(data.due_date==null?not_set:data.due_date);
        // $('#order_due_date').closest('.etitable-form-dropdown').find('input[name="due_date"]').val(data.due_date==null?not_set:data.due_date);


        $('#order_due_date').text(data.due_date==null?not_set:data.due_date);
        $('#order_due_date').closest('.etitable-form-dropdown').find('input[name="due_date"]').val(data.due_date==null?not_set:data.due_date);

        window.initEditableForms('.edit-tourist-payments',function (response) {
            updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
        });

        // $('#order_currency').text(data.currency==null?not_set:data.currency);
        // $('#order_rates').text((data.rates==null || data.rates==[])?not_set:data.rates);
        $('#order_profit').text(data.profit==null?not_set:DisplayLocaleString(parseFloat(data.profit),data.cur.text));
        if(data.status==0) {
            $('#order_status').html('<span class="status-bage m-badge m-badge--danger m-badge--wide">'+GetTranslation('payment_status_not_payed')+'</span>');
            $('#tourist-payment-indicator').html('<span class="indicator danger"></span> <span class="indicator"></span> <span class="indicator"></span>');
        }
        else
        if(data.status==1) {
            $('#order_status').html('<span class="status-bage m-badge m-badge--warning m-badge--wide">'+GetTranslation('payment_status_partial')+'</span>');
            $('#tourist-payment-indicator').html('<span class="indicator warning"></span> <span class="indicator warning"></span> <span class="indicator"></span>');
        }
        else
        if(data.status==2) {
            $('#order_status').html('<span class="status-bage m-badge m-badge--success m-badge--wide">'+GetTranslation('payment_status_payed')+'</span>');
            $('#tourist-payment-indicator').html('<span class="indicator success"></span> <span class="indicator success"></span> <span class="indicator success"></span>');
        }
        else {
            $('#order_status').html('<span class="status-bage m-badge m-badge--danger m-badge--wide">'+GetTranslation('payment_status_not_payed')+'</span>');
            $('#tourist-payment-indicator').html('<span class="indicator danger"></span> <span class="indicator"></span> <span class="indicator"></span>');
        }
        $('.payments-history-item').remove();
        let payments = '';
        for(P in data.tourist_payments)
            payments +='<tr class="payments-history-item tourist_pay_extra_field" style="display: none"><td>' +
                DisplayLocaleString(parseFloat(data.tourist_payments[P].sum),data.cur.text) +
                '</td><td>' +
                DisplayLocaleString(parseFloat(data.tourist_payments[P].rate),data.cur.text) +
                '</td><td>' +
                data.tourist_payments[P].date +
                '</td></tr>'
        $(payments).insertAfter('.payments-history');
        $('.tourist-duty').remove();
        let duties = '';
        for(P in data.tourist_duties)
            duties += '<tr class="tourist-duty"><td><span>' +
                GetTranslation('ov_tourist_pay_tourist_duty_label') +
                '</span></td><td class="text-right m--font-bold" colspan="2">' +
                '<span>' +
                DisplayLocaleString(parseFloat(data.tourist_duties[P]),P) +
                '</span></td></tr>';
        $(duties).insertAfter('.b-duties');


        $('.provider-payment').each(function () {
            let provider = $(this);
            provider.find('.m-loader').removeClass('d-none');
            provider.find('table').hide();
            let info = data.provider_payed[$(this).data('link-id')];
            if(info == undefined)
            {
                info = {
                    price:null,
                    sum:null,
                    duty:null,
                    commission:null,
                    status:null,
                    cur:{text:data.cur.text}
                }
            }
            let not_set = GetTranslation('not_set', 'Not set');
            provider.find('.provider-total-sum').text(info.price==null?not_set:DisplayLocaleString(parseFloat(info.price),data.cur.text));
            provider.find('.provider-payed').text(info.sum==null?not_set:DisplayLocaleString(parseFloat(info.sum),data.cur.text));
            provider.find('.provider-duty').text(info.duty==null?not_set:DisplayLocaleString(parseFloat(info.duty),info.cur.text));
            // provider.find('.provider-due-date').text(data.due_date==null?not_set:data.due_date);
            // provider.find('.provider-currency').text(data.currency==null?not_set:data.currency);
            provider.find('.provider-commission').text(info.commission==null?not_set:info.commission);
            if(info.status==0) {
                provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--danger m-badge--wide">'+GetTranslation('payment_status_not_payed')+'</span>');
                provider.find('.provider-payment-indicator').html('<span class="indicator danger"></span> <span class="indicator"></span> <span class="indicator"></span>');
            }
            else
            if(info.status==1) {
                provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--warning m-badge--wide">'+GetTranslation('payment_status_partial')+'</span>');
                provider.find('.provider-payment-indicator').html('<span class="indicator warning"></span> <span class="indicator warning"></span> <span class="indicator"></span>');
            }
            else
            if(info.status==2) {
                provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--success m-badge--wide">'+GetTranslation('payment_status_payed')+'</span>');
                provider.find('.provider-payment-indicator').html('<span class="indicator success"></span> <span class="indicator success"></span> <span class="indicator success"></span>');
            }
            else {
                provider.find('.provider-status').html('<span class="status-bage m-badge m-badge--danger m-badge--wide">'+GetTranslation('payment_status_not_payed')+'</span>');
                provider.find('.provider-payment-indicator').html('<span class="indicator danger"></span> <span class="indicator"></span> <span class="indicator"></span>');
            }
            provider.find('.m-loader').addClass('d-none');
            provider.find('table').show();
        });
    }

    function updateTrPaymentInfo(id)
    {   $('#tourist-pay-partlet .m-loader').removeClass('d-none');
        $('#tourist-pay-partlet table').hide();
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/orders/get-info',
            method: 'get',
            dataType: 'json',
            data: {
                id:id
            },
            success: function (data) {
                updatePaymentInfo(data);
                $('#tourist-pay-partlet .m-loader').addClass('d-none');
                $('#tourist-pay-partlet table').show();
            }
        });
    }

    $('.set-service-status').on('click',function () {
        let item = $(this);
        function changeSatus(item) {
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/set-service-status?order_id='+$('#tourist-pay-partlet').data('order-id')+'&link_id='+item.data('link-id'),
                method: 'post',
                dataType: 'json',
                data:{status:item.data('status'), _csrf:$('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    let status = '';
                    item.closest('.order-product-status-dropdown').find('.product-status').remove();
                    switch (item.data('status'))
                    {
                        case 0: status = ' <span href="#" class="m-dropdown__toggle m--font-danger product-status"> ' +
                            '<span class="text">' +
                            GetTranslation('ov_page_products_danger_status_label')+
                            '</span> ' +
                            '<i class="la la-angle-down open_indicator"></i> ' +
                            '</span>';break;
                        case 1: status = ' <span href="#" class="m-dropdown__toggle m--font-warning product-status"> ' +
                            '<span class="text">' +
                            GetTranslation('ov_page_products_warning_status_label')+
                            '</span> ' +
                            '<i class="la la-angle-down open_indicator"></i> ' +
                            '</span>';break;
                        case 2: status = ' <span href="#" class="m-dropdown__toggle m--font-success product-status"> ' +
                            '<span class="text">' +
                            GetTranslation('ov_page_products_success_status_label')+
                            '</span> ' +
                            '<i class="la la-angle-down open_indicator"></i> ' +
                            '</span>';break;
                        case 3: status = ' <span href="#" class="m-dropdown__toggle m--font-danger product-status"> ' +
                            '<span class="text">' +
                            GetTranslation('ov_page_products_canceled_status_label')+
                            '</span> ' +
                            '<i class="la la-angle-down open_indicator"></i> ' +
                            '</span>';break;
                    }

                    item.closest('.order-product-status-dropdown').append(status);
                    let content = {
                        title: GetTranslation('success_message', 'Success!'),
                        message: ''
                    };
                    $.notify(content, {
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        z_index: 1031,
                        type: "success"
                    });
                    location.reload();
                },
                error: function (response) {
                    let content = {
                        title: 'Error',
                        message: response.responseText
                    };
                    $.notify(content, {
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        type: "danger",
                        z_index: 2031
                    });
                }
            });
        }

        if(item.data('status') == 0)
        {
            swal({
                title: GetTranslation('delete_question', 'Are you sure?'),
                // text: button.data('message'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: GetTranslation('yes', 'Yes'),
                cancelButtonText: GetTranslation('no', 'No'),
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    changeSatus(item);
                } else if (result.dismiss === 'cancel') {}
            });
        } else {
            changeSatus(item);
        }

    });


    function PaymentModal(modal,type,relatedTarget,orderId) {
        this.modal = modal;
        this.type = type;
        this.relatedTarget = relatedTarget;
        this.orderId = orderId;
        let Component = this;

        this.renderItem = function (payment, services, order_cur, count) {
            let np = false;
            if (payment == undefined) {
                np = true;
                payment = {};
            }
            let form = '<div class="m-accordion__item"> ' +
                '<div class="m-accordion__item-head ' + (np ? '' : 'collapsed') + '"  role="tab" id="add_payment_accordion_item_' + count + '_head" data-toggle="collapse" href="#add_payment_accordion_item_' + count + '_body" aria-expanded="' + (np ? 'true' : 'false') + '"> ' +
                '<span class="m-accordion__item-icon"> ' +
                '<i class="fa flaticon-cart"></i> ' +
                '</span> ' +
                '<span class="m-accordion__item-title">' +
                ((payment.sum !== undefined) ? payment.date : GetTranslation('ov_new_payment')) +
                '</span> ' +
                '<span class="m-accordion__item-mode"></span> ' +
                '</div> ' +
                '<div class="m-accordion__item-body collapse ' + (np ? 'show' : '') + '" id="add_payment_accordion_item_' + count + '_body" role="tabpanel" aria-labelledby="add_payment_accordion_item_' + count + '_head" data-parent="#add_payment_accordion"> ' +
                '<div class="m-accordion__item-content"> ' +
                '<form class="payment-form">' +
                ((payment.id !== undefined) ? '<input type="hidden"  name="Payment[id]" value="' + payment.id + '">' : '') +
                '<div class="row mb-3"> ' +
                '<div class="col-12"> ' +
                '<label class="control-label" for="add_payment_form_date"> ' +
                GetTranslation('ovapm_date_selecktpicker_label') +
                '</label> ' +
                '<div class="input-group date">' +
                '<input type="text" class="form-control m-input add_payment_form_date"  name="Payment[date]" value="' +
                ((payment.date !== undefined && payment.date !== null) ? payment.date : (moment().format(date_format)))
                + '">' +
                '<div class="input-group-append"> ' +
                '<span class="input-group-text"> ' +
                '<i class="la la-calendar-check-o"></i> ' +
                '</span> ' +
                '</div> ' +
                '</div> ' +
                '</div> ' +
                '</div> ' +

                '<div class="row mb-3"> ' +
                '<div class="col-12 mb-2">' +
                GetTranslation('ovapm_exchange_rates_label') +
                '</div>' +
                '<div class="col-12 col-sm-12 mb-3 mb-sm-0"> ' +
                '<div class="input-group date"> ' +
                '<input class="form-control exchange_rate" type="text" required name="Payment[exchange_rate]" maxlength="9" ' +
                ((payment.ext_rates !== undefined && payment.ext_rates !== null) ? 'data-old="' + payment.ext_rates + '"' : '')
                + ' value="' +
                ((payment.ext_rates !== undefined) ? payment.ext_rates : '')
                + '" class="form-control m-input currency_mask exchange_rate" placeholder="' + GetTranslation('ov_payment_enter_ex_rate') + '"> ' +
                '<div class="input-group-append"> ' +
                '<span class="input-group-text ex-rate"></span> ' +
                '</div> ' +
                '</div> ' +
                '</div>' +
                '</div>' +
                '<div class="row mb-3"> ' +
                '<div class="col-12"> ' +
                '<label class="control-label" for="add_payment_form_summ">' +
                GetTranslation('ovapm_date_summ_label') +
                '</label> ' +
                '<div class="input-group date"> ' +
                '<input type="text" required class="form-control m-input currency_mask add_payment_form_summ" ' +
                ((payment.sum !== undefined && payment.sum !== null) ? 'data-old="' + payment.sum + '"' : '')
                + ' value="' +
                ((payment.sum !== undefined) ? payment.sum : '')
                + '" name="Payment[sum]" placeholder="' + GetTranslation('ov_payment_enter_sum') + '"> ' +
                '<div class="input-group-append"> ' +
                '<span class="input-group-text">' + order_cur.iso_code + '</span> ' +
                '</div> ' +
                '</div> ' +
                '</div> ' +
                '</div> ';
            let count_sr = services.length;
            if (count_sr > 0) {
                form += '<div class="payment-progress-row row mb-3 m-radio-list">' +
                    '<div class="col-12 mb-2">' + GetTranslation('ovapm_payment_status_label') +
                    '</div>';
                for (X in services) {
                    let per = services[X].payed / services[X].sum * 100;
                    let selected = false;
                    if (payment.link_id !== undefined && services[X].id == payment.link_id)
                        selected = true;
                    if (payment.link_id == undefined && services[X] == services[0])
                        selected = true;
                    form += '<div class="col-12 ' + ((count_sr > 1) ? 'col-sm-6' : '') + ' mb-3 mb-sm-0"><label class="m-radio">' +
                        '<input class="service-radio" data-cur="' +
                        ((services[X].cur != null && services[X].cur != undefined) ? services[X].cur.text : order_cur.iso_code)
                        + '" type="radio" name="Payment[service_id]" ' + (selected ? 'checked' : '') + ' value="' + services[X].id + '"><span></span>' +
                        '<div class="payment-progress-item">' +
                        '<div class="progress-bar" style="width: ' + per + '%"></div>' +
                        '<div class="content"><div class="left">' +
                        '<div class="summ-native"><span class="service-payed-cur" data-old="' + services[X].payed_cur + '">' + services[X].payed_cur.toFixed(2) + '</span><span>&nbsp;' + order_cur.iso_code + '</span></div>' +
                        '<div class="summ-exchanged"><span class="service-payed" data-old="' + services[X].payed + '">' + services[X].payed.toFixed(2) + '</span><span>&nbsp;' +
                        ((services[X].cur != null && services[X].cur != undefined) ? services[X].cur.text : order_cur.iso_code) +
                        '</span></div>' +
                        '</div>' +
                        '<div class="right">' +
                        '<div class="full-summ-exchanged"><span class="service-sum">' + services[X].sum + '</span><span>&nbsp;' +
                        ((services[X].cur != null && services[X].cur != undefined) ? services[X].cur.text : order_cur.iso_code) +
                        '</span></div>' +
                        '</div>' +
                        '</div></div></label></div>';
                }
                form += '</div>';
            }
            form += '<div class="row mb-3"> ' +
                '<div class="col-12"> ' +
                '<label class="control-label" for="payment_note_form_textarea">' +
                GetTranslation('ovapm_payment_note_label') +
                '</label> ' +
                '<textarea name="Payment[note]" id="payment_note_form_textarea" class="form-control" rows="3">' +
                ((payment.note !== undefined) ? payment.note : '') +
                '</textarea></div> ' +
                '</div>';
            if (payment.sum !== undefined)
                form +=
                    '<div class="row"><div class="col-12">' +
                    '<button type="button" class="btn btn-danger delete-payment">' +
                    GetTranslation('ov_delete_payment') +
                    '</button></div>' +
                    '</div>';
            form += '</form>' +
                '</div> ' +
                '</div> ' +
                '</div>';
            return form;
        }

        this.removePayment = function (item) {
            swal({
                title: GetTranslation('delete_question', 'Are you sure?'),
                // text: button.data('message'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    let item_id = item.closest('.m-accordion__item').find('input[name="Payment[id]"]').val()
                    if(item_id!= undefined) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/'+(Component.type=='tourist'?'remove-tourists-payment':'remove-providers-payment'),
                            method: 'get',
                            dataType: 'json',
                            data: {
                                id: $('#tourist-pay-partlet').data('order-id'),
                                payment_id:item_id
                            },
                            success:function () {
                                item.closest('.m-accordion__item').remove();
                                item.closest('.m-accordion__item-content').find('.service-radio').each(function () {
                                    recalculate($(this));
                                    updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
                                });
                            }
                        });
                    } else {
                        item.closest('.m-accordion__item').remove();
                        item.closest('.m-accordion__item-content').find('.service-radio').each(function () {
                            recalculate($(this));
                            updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
                        });
                    }
                } else if (result.dismiss === 'cancel') {}
            });
        }

        function recalculate(item,parent) {
            let total_val = 0;
            let total_cur = 0;
            let sum = 0;
            $('#add_payment_form').find('.service-radio[value="'+item.val()+'"]:checked').each(function () {
                let input = $(this).closest('.m-accordion__item-content').find('.add_payment_form_summ');
                let er = $(this).closest('.m-accordion__item-content').find('.exchange_rate');
                if(er.val()!=0)
                {
                    let value = input.val()/er.val();
                    let value_cur = input.val();
                    sum = parseFloat($(this).parent().find('.service-sum').text());
                    if(er.val() != undefined && er.val() != '')
                    {
                        value = parseFloat(value);
                        value_cur = parseFloat(value_cur);

                        if(!isNaN(value))
                            total_val += value;
                        if(!isNaN(value_cur))
                            total_cur += value_cur;
                    }
                }
            });

            console.log("sum: "+sum);
            console.log("total: "+total_val);
            console.log("total_cur: "+total_cur);

            let percent = total_val/sum*100;
            console.log("percent: "+percent);

            if(percent > 100) {
                if (parent != undefined) {
                    let rate = parent.closest('.m-accordion__item-content').find('.exchange_rate').val();
                    console.log("rate: "+rate);
                    console.log("parent: "+parent.val());
                    if(rate != 0) {
                        let new_val = (parseFloat(sum)-(total_val - parseFloat(parent.val())/rate))*rate;
                        console.log("new_val: "+new_val);
                        percent = 100;
                        total_val = sum;
                        total_cur = total_cur - parent.val() + new_val;
                        parent.val(new_val);
                    }
                    else
                        percent = 100;
                }
                else
                    percent = 100;
            }
            Component.modal.find('#add_payment_form').find('.service-radio[value="'+item.val()+'"]').each(function () {
                let item = $(this).parent().find('.service-payed-cur');
                item.text(total_cur.toFixed(2));
                item = $(this).parent().find('.service-payed');
                item.text(total_val.toFixed(2));
                $(this).parent().find('.progress-bar').attr('style','width:'+percent+'%')
            });
        }


        this.savePayment = function (data) {
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/orders/'+(Component.type=='tourist'?'save-tourist-payments':'save-provider-payments')+'?id='+Component.orderId,
                method: 'post',
                dataType: 'json',
                data:data,
                success: function (data) {
                    modal.modal('hide');
                    $('#add_payment_form').empty();
                    if(data.message != undefined)
                    {
                        swal({
                            title: data.message,
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonText: GetTranslation('yes', 'Yes'),
                            // cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                            reverseButtons: false
                        }).then(function (result) {
                            updatePaymentInfo(data);
                        });
                    }
                    updatePaymentInfo(data);

                    // updateTrPaymentInfo($('#tourist-pay-partlet').data('order-id'));
                },
                error: function (response) {
                    let content = {
                        title: 'Error',
                        message: response.responseText
                    };
                    $.notify(content, {
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        type: "danger",
                        z_index: 2031
                    });
                }
            });
        }

        this.renderPayments = function () {
            if(Component.type == 'tourist') {
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-tourists-payments',
                    method: 'get',
                    dataType: 'json',
                    data: {
                        id: $('#tourist-pay-partlet').data('order-id')
                    },
                    success: function (data) {
                        $('#m_modal_add_payment .m-loader').addClass('d-none');

                        let count = 0;
                        let form = '';
                        for (I in data.payments) {
                            form += Component.renderItem(data.payments[I], data.services, data.cur, count);
                            count++;
                        }
                        form += Component.renderItem(undefined, data.services, data.cur, count);

                        Component.modal.find('#add_payment_form').append(form);
                        Component.modal.find('.exchange_rate').inputmask({
                            // "mask": "9{+}.99"
                            "regex": '^-?\\d+(\\.\\d{1,2})?$'
                        });
                        Component.modal.find('.add_payment_form_summ').inputmask({
                            // "mask": "9{+}.99"
                            "regex": '^-?\\d+(\\.\\d{1,2})?$'
                        });

                        Component.modal.find('.delete-payment').off('click').on('click', function () {
                            let item = $(this);
                            Component.removePayment(item);
                        });

                        Component.modal.find('#save-tourist-payments').off('click').on('click',function () {
                            let form = $('#add_payment_form').find('.m-accordion__item-head[aria-expanded="true"]')
                                .parent().find('.payment-form');
                            let validator = form.validate({
                                rules: {
                                    'Payment[exchange_rate]': {
                                        required: true
                                    },
                                    'Payment[sum]': {
                                        required: true
                                    },
                                    'tourist_id': {
                                        required: true
                                    }
                                }
                            });
                            if (!form.valid()) {
                                $(form).find('div.form-control-feedback').css('color', 'red');
                                return;
                            }

                            let data = form.serializeArray().reduce(function(obj, item) {
                                obj[item.name] = item.value;
                                return obj;
                            }, {});
                            data._csrf = $('meta[name="csrf-token"]').attr('content');
                            Component.savePayment(data);
                        });

                        Component.modal.find('#add_payment_form .exchange_rate').off('keyup').on('keyup',function () {
                            console.log('Exchange rate');
                            let sum = $(this).closest('.m-accordion__item-content').find('.add_payment_form_summ');
                            let max = parseFloat($(this).closest('.m-accordion__item-content').find('.service-radio:checked').parent().find('.service-sum').text());
                            let payed = parseFloat($(this).closest('.m-accordion__item-content').find('.service-radio:checked').parent().find('.service-payed').text());
                            if(sum.val()=='' && $(this).val()!='')
                            {
                                sum.val((max-payed)*parseFloat($(this).val()));
                            }
                            sum.trigger('keyup');
                        });



                        Component.modal.find('#add_payment_form .add_payment_form_summ').off('keyup').on('keyup',function () {
                            recalculate($(this).closest('.m-accordion__item-content').find('.service-radio:checked'),$(this));
                        });

                        Component.modal.find('#add_payment_form .service-radio').off('click').on('click',function () {
                            // let item = $(this);
                            $(this).closest('.m-accordion__item-content').find('.service-radio').each(function () {
                                recalculate($(this));
                            });
                            $(this).closest('.m-accordion__item-content').find('.add_payment_form_summ').trigger('keyup');
                        });


                        Component.modal.find(' .add_payment_form_date').each(function(){
                            let input = this;
                            $(input).datepicker({
                                // weekStart: +week_start,
                                todayHighlight: true,
                                format: date_format_datepicker,
                                language: tenant_lang,
                                autoclose: true,
                                orientation: "bottom left",
                                templates: {
                                    leftArrow: '<i class="la la-angle-left"></i>',
                                    rightArrow: '<i class="la la-angle-right"></i>'
                                },
                                // dropdownParent: $(input).parents('.m-accordion__item-content')
                            }).click(function(e){
                                $('.add_payment_form_date').on('click', function(e){
                                    // It is fix for daterangepicker. It disble closing
                                    // dropdown, when next and prev arrows was clicked
                                    e.stopPropagation();
                                });
                            });
                        });

                        Component.modal.find('#add_payment_form').find('.service-radio').off('change').on('change',function () {
                            $(this).closest('.m-accordion__item').find('.ex-rate').text($(this).data('cur'));
                        });

                        Component.modal.find('#add_payment_form').find('.service-radio').each(function () {
                            if($(this).prop('checked'))
                                $(this).trigger('change');
                        });
                    }
                });
            }
            else {
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/orders/get-provider-payments',
                    method: 'get',
                    dataType: 'json',
                    data: {
                        order_id:$(Component.relatedTarget).closest('.provider-payment').data('order-id'),
                        link_id:$(Component.relatedTarget).closest('.provider-payment').data('link-id'),
                    },
                    success: function (data) {
                        $('#m_modal_add_payment .m-loader').addClass('d-none');

                        let count = 0;
                        let form = '';
                        for (I in data.payments) {
                            form += Component.renderItem(data.payments[I], data.services, data.cur, count);
                            count++;
                        }
                        form += Component.renderItem(undefined, data.services, data.cur, count);

                        Component.modal.find('#add_payment_form').append(form);
                        Component.modal.find('.exchange_rate').inputmask({
                            // "mask": "9{+}.99"
                            "regex": '^-?\\d+(\\.\\d{1,2})?$'
                        });
                        Component.modal.find('.add_payment_form_summ').inputmask({
                            // "mask": "9{+}.99"
                            "regex": '^-?\\d+(\\.\\d{1,2})?$'
                        });

                        Component.modal.find('.delete-payment').off('click').on('click', function () {
                            let item = $(this);
                            Component.removePayment(item);
                        });

                        Component.modal.find('.ex-rate').text(data.service_cur.text);

                        Component.modal.find(' .add_payment_form_date').each(function(){
                            let input = this;
                            $(input).datepicker({
                                // weekStart: +week_start,
                                todayHighlight: true,
                                format: date_format_datepicker,
                                language: tenant_lang,
                                autoclose: true,
                                orientation: "bottom left",
                                templates: {
                                    leftArrow: '<i class="la la-angle-left"></i>',
                                    rightArrow: '<i class="la la-angle-right"></i>'
                                },
                                // dropdownParent: $(input).parents('.m-accordion__item-content')
                            }).click(function(e){
                                $('.add_payment_form_date').on('click', function(e){
                                    // It is fix for daterangepicker. It disble closing
                                    // dropdown, when next and prev arrows was clicked
                                    e.stopPropagation();
                                });
                            });
                        });

                        $('#save-tourist-payments').off('click').on('click',function () {
                            let form = $('#add_payment_form').find('.m-accordion__item-head[aria-expanded="true"]')
                                .parent().find('.payment-form');
                            let validator = form.validate({
                                rules: {
                                    'Payment[exchange_rate]': {
                                        required: true
                                    },
                                    'Payment[sum]': {
                                        required: true
                                    },
                                    'tourist_id': {
                                        required: true
                                    }
                                }
                            });
                            if (!form.valid()) {
                                $(form).find('div.form-control-feedback').css('color', 'red');
                                return;
                            }

                            let data = form.serializeArray().reduce(function(obj, item) {
                                obj[item.name] = item.value;
                                return obj;
                            }, {});
                            data._csrf = $('meta[name="csrf-token"]').attr('content');
                            data.service_id = $(Component.relatedTarget).closest('.provider-payment').data('link-id');
                            Component.savePayment(data);
                        });
                    }
                });
            }
        }
        return function () {
            Component.renderPayments();
        }
    };
})(jQuery);