var orders_datatable = null;

(function($){
    $(document).ready(function(){
        initBidsDataTable();
        initCreateOrderModal();
        
        function initBidsDataTable(){
            // Responsive columns default sizes
            var col_sizes = {
                statistic: 280
            };
            // Small
            if(window.innerWidth >= 540){
                col_sizes.statistic = 280;
            }
            // Medium
            if(window.innerWidth >= 720){
                col_sizes.statistic = 300;
            }
            // Lage
            if(window.innerWidth >= 960){
                col_sizes.statistic = 400;
            }
            // Extra Lage
            if(window.innerWidth >= 1140){
                col_sizes.statistic = 400;
            }
            // Super Lage
            if(window.innerWidth >= 1500){
                col_sizes.statistic = 550;
            }
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'full_name',
                    title: GetTranslation('oi_datatable_col_title_1', ''),
                    sortable: false,
                    template: function(data) {
                        return '<a href="'+baseUrl + '/ita/' + tenantId + '/orders/view?id='+data.id+'">'+data.full_name+'</a>';
                    }
                },
                {
                    field: 'order_info',
                    width: col_sizes.statistic,
                    overflow: 'visible',
                    title: GetTranslation('oi_datatable_col_title_2', ''),
                    sortable: false,
                    template: function(data) {
                        if(data.order_info[0]!=undefined) {
                            var item = '';
                            
                            // Render extra services
                            if(data.order_info.length > 1){
                                item += '<span class="dropdown float-right">\
                                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\
                                              <i class="la la-ellipsis-h"></i>\
                                            </a>\
                                            <div class="dropdown-menu dropdown-menu-right">';
                                                for (var i=1; i<data.order_info.length; i++) {
                                                    item += '<a class="dropdown-item" href="#"><i class="' + data.order_info[i]['service_icon'] + '"></i> ' + data.order_info[i]['service_name'] + '</a>';
                                                }
                                item +=     '</div>\
                                        </span>';
                            }
                            console.log(data.order_info);
                            // Render first service
                            item += '<div class="service-data">' +
                                        '<div class="heading m--font-bold">' +
                                            '<div class="service-item">' +
                                                '<i class="'+data.order_info[0].service_icon+' mr-2"></i>' +
                                                '<span>'+data.order_info[0].service_name+'</span>' +
                                                '<div class="status-block ml-3">';
                            switch (data.order_info[0].service_status){
                                case 0:item +=      '<span class="m-badge m-badge--danger m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-danger ml-1">'+GetTranslation('ov_page_products_danger_status_label')+'</span>';
                                    break;
                                case 1:item +=      '<span class="m-badge m-badge--warning m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-warning ml-1">'+GetTranslation('ov_page_products_warning_status_label')+'</span>';
                                    break;
                                case 2:item +=      '<span class="m-badge m-badge--success m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-success ml-1">'+GetTranslation('ov_page_products_success_status_label')+'</span>';
                                    break;
                                case 3:item +=      '<span class="m-badge m-badge--danger m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-danger ml-1">'+GetTranslation('ov_page_products_success_status_label')+'</span>';
                                    break;
                            }
                            item +=             '</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="description">';
                            if(data.order_info[0].reservation !== null)
                            item +=         '<div class="reservation-block">' +
                                                '<mark class="reservation-number mb-1">'+data.order_info[0].reservation+'</mark>' +
                                            '</div>';
                            item +=         '<span class="description-text">' +
                                                data.order_info[0].fields_list+
                                            '</span>' +
                                        '</div>' +
                                    '</div>';
                            return item;
                        }
                        return '';
                    }
                },
                {
                    field: 'order_payment',
                    title: GetTranslation('oi_datatable_col_title_3', ''),
                    sortable: false,
                    template: function(data) {
                        var info = data['order_payment'];
                        var payment = '';
                        if(info.sum == null)
                            info.sum = 0;
                        info.sum = parseFloat(info.sum);
                        payment = info.sum.toLocaleString(tenant_lang,{style: 'currency', currency: info.cur.text});

                        var status = '';
                        switch (info.status){
                            case 1: status =    '<div class="d-block mt-1">' +
                                                    '<span class="m-badge m-badge--warning m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-warning">' + GetTranslation('payment_status_partial') + '</span>' +
                                                '</div>';
                                break;
                            case 2: status =    '<div class="d-block mt-1">' +
                                                    '<span class="m-badge m-badge--success m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-success">' + GetTranslation('payment_status_payed') + '</span>' +
                                                '</div>';
                                break;
                            default: status =   '<div class="d-block mt-1">' +
                                                    '<span class="m-badge m-badge--danger m-badge--dot mr-2"></span>' +
                                                    '<span class="m--font-bold m--font-danger">' + GetTranslation('payment_status_not_payed') + '</span>' +
                                                '</div>';
                                break;
                        }
                        
                        // Money calculate
                        var income_row = '<tr><td>' + GetTranslation('oi_income_label', 'Income') + ':</td><td>' + (info.sum!==null?info.sum.toLocaleString(tenant_lang,{style: 'currency', currency: info.cur.text}):GetTranslation('not_set')) + '</td></tr>';
                        var consumption_row = '<tr><td>' + GetTranslation('oi_consumption_label', 'Consumption')+ ':</td><td>' +  (info.supplier_sum!==null?info.supplier_sum.toLocaleString(tenant_lang,{style: 'currency', currency: info.cur.text}):GetTranslation('not_set')) + '</td></tr>';
                        var profit_row = '<tr><td>' + GetTranslation('oi_profit_label', 'Profit') + ':</td><td>' +  (info.profit!==null?info.profit.toLocaleString(tenant_lang,{style: 'currency', currency: info.cur.text}):GetTranslation('not_set')) + '</td></tr>';
                        // Return template
                        return '<div data-container="body" data-toggle="m-popover" data-html="true" data-placement="bottom"\
                                        data-content="<table>' + income_row + consumption_row + profit_row + '</table>">\
                                    <span class="sum">'+payment+'</span>\
                                    <span class="status">'+status+'</span>\
                                </div>';
                    }
                },
                {
                    field: 'responsible',
                    width: 90,
                    title: GetTranslation('oi_datatable_col_title_4', ''),
                    sortable: false,
                    template: function(data) {
                        return '<div class="m-card-user m-card-user--sm"><div class="m-card-user__pic"><img src="'+baseUrl+'/'+data.responsible_photo+'" class="m--img-rounded m--marginless m--img-centered" alt="'+data.responsible+'" title="'+data.responsible+'"></div></div>';
                    }
                }
            ];
            
            orders_datatable = $('#orders_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/orders/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                    // filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(orders_datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(orders_datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = orders_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_orders_buttons').css({display : "flex"});
                }else{
                    $('#bulk_orders_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-orders').on('click', function(){
                var selected_records = orders_datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('bi_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('bi_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/orders/bulk-delete',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                orders_datatable.reload();
                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
        
        function initCreateOrderModal(){
            // Put your code here...
        }
    });
})(jQuery);