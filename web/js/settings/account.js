//== Class definition
var WizardDemo = function () {
    //== Base elements
    var formStep_1 = $('#wizard_form_1');
    var formStep_2 = $('#wizard_form_2');
    var formStep_3 = $('#wizard_form_3');
    var formStep_4 = $('#wizard_form_4');
    var validator = [];
    var wizard;

    //== Private functions
    var initWizard = function () {
        //== Initialize form wizard
        wizard = new mWizard('m_wizard', {
            startStep: 1,
            manualStepForward: true
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function(wizard) {
            var current_step = wizard.getStep();

            switch (current_step) {
                case 1:
                    var form_need_save = $('#wizard_form_1').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (current_step == 1 && tenant_country_id == 0) {
                                    tenant_country_id = $('#account_country_select').val();
                                    $('#form-wizard-submit').trigger('click');
                                }
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_1').data('need-save', false);
                                wizard.goNext();
                            }
                        });
                        return false;
                    }
                    break;

                case 2:
                    var form_need_save = $('#wizard_form_2').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_2').data('need-save', false);
                                wizard.goNext();
                            }
                        });
                        return false;
                    }
                    break;
                case 3:
                    var form_need_save = $('#wizard_form_3').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_3').data('need-save', false);
                                wizard.goNext();
                            }
                        });
                        return false;
                    }
                    break;

                case 4:
                    var form_need_save = $('#wizard_form_4').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_4').data('need-save', false);
                                wizard.goNext();
                            }
                        });
                        return false;
                    }
                    break;

                default:
                    if (validator[current_step] !== null && validator[current_step] !== undefined){
                        if (validator[current_step].form() !== true) {
                            wizard.stop();  // don't go to the next step
                        } else {
                            if (current_step == 1 && tenant_country_id == 0) {
                                tenant_country_id = $('#account_country_select').val();
                                $('#form-wizard-submit').trigger('click');
                            }
                            if (window.isGetRequisiteFields == undefined) {
                                window.isGetRequisiteFields = false;
                            }
                        }
                    }
                    break;
            }
        });

        wizard.on('beforePrev', function(wizard) {
            var current_step = wizard.getStep();

            switch (current_step) {
                case 1:
                    var form_need_save = $('#wizard_form_1').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (current_step == 1 && tenant_country_id == 0) {
                                    tenant_country_id = $('#account_country_select').val();
                                    $('#form-wizard-submit').trigger('click');
                                }
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_1').data('need-save', false);
                                wizard.goPrev();
                            }
                        });
                        return false;
                    }
                    break;

                case 2:
                    var form_need_save = $('#wizard_form_2').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_2').data('need-save', false);
                                wizard.goPrev();
                            }
                        });
                        return false;
                    }
                    break;
                case 3:
                    var form_need_save = $('#wizard_form_3').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_3').data('need-save', false);
                                wizard.goPrev();
                            }
                        });
                        return false;
                    }
                    break;

                case 4:
                    var form_need_save = $('#wizard_form_4').data('need-save');
                    if(form_need_save !== true || form_need_save === undefined){
                        if (validator[current_step] !== null && validator[current_step] !== undefined){
                            if (validator[current_step].form() !== true) {
                                wizard.stop();  // don't go to the next step
                            } else {
                                if (window.isGetRequisiteFields == undefined) {
                                    window.isGetRequisiteFields = false;
                                }
                            }
                        }
                    } else {
                        swal({
                            title: GetTranslation("acc_unsaved_changes", "You have unsaved changes"),
                            text: GetTranslation("acc_leave_now_question", "If you leave now, your changes will be lost"),
                            confirmButtonClass: "btn btn-secondary m-btn m-btn--wide leave_page",
                            confirmButtonText: GetTranslation("acc_leave", "Leave"),
                            showCancelButton: true,
                            cancelButtonText: GetTranslation("acc_stay", "Stay")
                        }).then(function(data) {
                            if(data.value) {
                                $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
                                $('#wizard_form_4').data('need-save', false);
                                wizard.goPrev();
                            }
                        });
                        return false;
                    }
                    break;
                default:
                    if (validator[current_step] !== null && validator[current_step] !== undefined){
                        if (validator[current_step].form() !== true) {
                            wizard.stop();  // don't go to the next step
                        } else {
                            if (current_step == 1 && tenant_country_id == 0) {
                                tenant_country_id = $('#account_country_select').val();
                                $('#form-wizard-submit').trigger('click');
                            }
                            if (window.isGetRequisiteFields == undefined) {
                                window.isGetRequisiteFields = false;
                            }
                        }
                    }
                    break;
            }
        });

        //== Change event
        wizard.on('change', function(wizard) {
            var current_step = wizard.getStep();
            $(wizard).attr('data-current-step', current_step);   //set currnt step data
            mUtil.scrollTop();   //scroll page on top
            
            // Requisites init
            if (current_step == 3 && window.isGetRequisiteFields == false) {
                getRequisitesFields();
                getNewRequisitesFields();
                checkForm();
                window.isGetRequisiteFields = true;
            }
        });

        $('#wizard_form_1 input, #wizard_form_1 button, #wizard_form_1 textarea').on('keyup', function () {
            $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().addClass('topopup');
            $('#wizard_form_1').data('need-save', true);
        });
        $('#wizard_form_2 input, #wizard_form_2 button, #wizard_form_2 textarea').on('keyup', function () {
            $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().addClass('topopup');
            $('#wizard_form_2').data('need-save', true);
        });
        $('#wizard_form_4 input, #wizard_form_4 button, #wizard_form_4 textarea').on('keyup', function () {
            $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().addClass('topopup');
            $('#wizard_form_4').data('need-save', true);
        });

        $('#form-wizard-submit, #m_requisites_accordion .save-requisites-set, #create_requisites_form .generated_requisites_form__submit').on('click', function(){
            $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().filter('.topopup').removeClass('topopup');
            $('#wizard_form_1').data('need-save', false);
            $('#wizard_form_2').data('need-save', false);
            $('#wizard_form_3').data('need-save', false);
            $('#wizard_form_4').data('need-save', false);
        });

    };

    var initValidation = function() {
        validator[1] = formStep_1.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                //== Main settings
                name: {
                    required: true 
                },
                country: {
                    required: true
                },
                lang: {
                    required: true
                },
                timezone: {
                    required: true
                },
                date_format: {
                    required: true
                }
            },

            //== Validation messages
            messages: {
                'name': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'country': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'lang': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'timezone': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'date_format': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                }
            },

            //== Display error  
            invalidHandler: function(event, validator) {
                mUtil.scrollTop();

                swal({
                    "title": "",
                    "text": GetTranslation("acc_set_form_has_error_message", "There are some errors in your submission. Please correct them."),
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {

            }
        });
        validator[2] = formStep_2.validate({
            //== Validate only visible fieldsignore: ":hidden",
            //== Validation rules
            rules: {//== Main settings
                "agency[phones][0][phone]": {
                    required: true
                }, "agency[emails][0][value]": {
                    required: true
                }, "agency[socials][0][value]": {
                    required: true
                }, "agency[messengers][0][value]": {
                    required: true
                }, "agency[site]": {
                    required: true
                }, "agency[city]": {
                    required: true
                }, "agency[street]": {
                    required: true
                }, "agency[house]": {
                    required: true
                }, "agency[description]": {
                    required: true
                }
            },
            //== Validation messages
            messages: {
                'agency[phones][0][phone]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[emails][0][value]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[socials][0][value]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[messengers][0][value]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[site]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[city]': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                },
                'agency[street]': {
                       required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                   },
                'agency[house]': {
                       required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                   },
                'agency[description]': {
                       required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                   }
            },
            //== Display error
            invalidHandler: function(event, validator) {mApp.scrollTop();

            swal({
                "title": "",
                "text": GetTranslation("acc_set_form_has_error_message", "There are some errors in your submission. Please correct them."),
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"});
                       },

                       //== Submit valid form
                           submitHandler: function (form) {

                           }
               });
        validator[4] = formStep_4.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                reminders_email: {
                    required: true,
                    email: true
                },
                reminders_from_who: {
                    required: true
                }
            },

            //== Validation messages
            messages: {
                'reminders_email': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required."),
                    email: GetTranslation("acc_set_email_field_error_message", "Not valid email")
                },
                'reminders_from_who': {
                    required: GetTranslation("acc_set_required_field_error_message", "This field is required.")
                }
            },

            //== Display error  
            invalidHandler: function(event, validator) {
                mUtil.scrollTop();

                swal({
                    "title": "",
                    "text": GetTranslation("acc_set_form_has_error_message", "There are some errors in your submission. Please correct them."),
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {

            }
        });
    };
    
    var initSubmit = function() {
        $('#form-wizard-submit').on('click',function () {
            var btn = this;
            var current_step = wizard.getStep();
            console.log(current_step, validator[current_step]);

            if (validator[current_step] !== null && validator[current_step] !== undefined) {

                mApp.progress(btn);
                if (validator[current_step].form() !== true) {
                    mApp.unprogress(btn);
                    wizard.stop();  // don't go to the next step
                } else {
                    var formdata = false;
                    switch (current_step) {
                        case 1:
                            formdata = formStep_1.serializeArray();
                            break;
                        case 2:
                            formdata = formStep_2.serializeArray();
                            break;
                        case 4:
                            formdata = formStep_4.serializeArray();
                            break;
                    }
                    if (formdata != false && formdata.length > 0) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/settings/update-base-settings',
                            method: 'POST',
                            data: formdata,
                            success: function () {
                                mApp.unprogress(btn);
                                //mApp.unblock(formEl);
                                swal({
                                    "title": GetTranslation("saved", "Saved"),
                                    "text": GetTranslation("acc_set_form_submit_success", "The application has been successfully submitted!"),
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            }
                        });
                    }
                }
            }
        });
    };

    return {
        // public functions
        init: function() {
            initWizard(); 
            initValidation();
            initSubmit();
        }
    };
}();

var RequisitiesForm = function(){
    var form = $('.generated_requisites_form');
    var formStep_3 = $('#wizard_form_3');

    var initForms = function() {
        form.each(function(){
            /* Vars */
            var submit = $('.generated_requisites_form__submit').get()[0];
            var formdata = false;
            /* Events */
            $(submit).on('click', function(){
                formdata = formStep_3.serializeArray();
                $('.generated_requisites_form__submit').attr('disabled',true);
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/settings/create-requisites',
                    method: 'GET',
                    data: formdata,
                    success: function (data) {
                        $('#m_requisites_accordion').append(formatAccordionRequisites(data, true, false));
                        editRequisiteSet(false);
                        initRemoveSetButton();
                        checkForm();
                        formStep_3[0].reset();
                        $('.generated_requisites_form__submit').attr('disabled',false);
                    },
                    error: function ($error) {
                        $('.generated_requisites_form__submit').attr('disabled',false);
                    }
                });
                //Собрать все данные с инпутов
                //Сохранить данные
                    //true 
                        //Сообщить об удачном сохранинеии
                        //Добавить аккордеон
                        //Очистить форму
                    //false
                        //Сообщить об ошибке
            });
        });
    };
    
    return {
        init: function() {
            initForms();
        }
    };
}();

var initPageInputs = function(){
    
    var initInputs = function(){
        $('.timezone-select-2').select2();
        $('#select_language').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/settings/get-language',
                method: 'GET',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        name: params.term
                    };
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            width: '100%',
            placeholder: GetTranslation("conctact-edit-popup-add-tag-plaseholder", "Add a tag"),
            tags: true
        });

    };
    
    return {
        init: function() {
            initInputs();
        }
    };
}();

var AgencyContactsForm = function(){
    
    var initInputs = function(){
        // Repeaters
        $('#agency-phones-repeater, #agency-emails-repeater, #agency-socials-repeater, #agency-messengers-repeater').repeater({
            initEmpty: false,
            defaultValues: {
                'text-input': 'foo'
            },
            show: function () {
                $(this).slideDown();
                //Init maskinput
                $('#wizard_form_2')
                    .find('[valid="phoneNumber"]')
                    .intlTelInput({
                        utilsScript: "/plugins/phone-validator/build/js/utils.js",
                        autoPlaceholder: true,
                        preferredCountries: ['ua', 'ru'],
                        allowExtensions: false,
                        autoFormat: true,
                        autoHideDialCode: true,
                        customPlaceholder: null,
                        defaultCountry: "ua",
                        geoIpLookup: null,
                        nationalMode: false,
                        numberType: "MOBILE"
                    }).then(function (data) {
                        $('#agency-phones-repeater').find('.input_phone_visible').each(function(index, el) {
                            el = $(this);
                            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                            var formated = '+999 99 999 9999';
                            $(el).inputmask(formated);
                            $(el).on("countrychange", function (e, countryData) {
                                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                $(el).inputmask(formated);
                            });
                        });
                    });

                $(this).find('input').each(function () {
                    var mask_data = $(this).data('inputmask');
                    if (mask_data) {
                        $(this).inputmask();
                    }
                });
                //Init dropdown option
                initExtraDropdownOptionInputs(this);
            },
            hide: function (deleteElement) {
                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    text: GetTranslation('delete_element_confirmation_text', 'Are you sure you want to delete this element?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
        });
        
        // Description textarea
        var textarea_jq_selector = $('#agency_adress__description_textarea');
        autosize(textarea_jq_selector);
        
    };
    
    var initExtraDropdownOptionInputs = function(added_row){
        if (added_row){
            //Init default dropdown value
            var group = $(added_row).find('.extra-dropdown-option')[0];
            var hidden_input_selector = $(group).data('for-input-selector');
            var hidden_input = $(group).find(hidden_input_selector)[0];
            var default_val = $(hidden_input).data('default-value');
            $(hidden_input).val(default_val).removeAttr('data-default-value');
        }
        $('.extra-dropdown-option').each(function(){
            var group = this;
            var hidden_input_selector = $(group).data('for-input-selector');
            var hidden_input = $(group).find(hidden_input_selector)[0];
            var drop_down_btn = $(group).find('button.btn')[0];
            $(group).find('.dropdown-menu a').each(function(event){
                var variant = this;
                var variant_name = $(this).text();
                var variant_val = $(this).data('value');
                $(variant).unbind('click');
                $(variant).on('click', function(event){
                   $(hidden_input).val(variant_val);
                   $(drop_down_btn).text(variant_name);
                   event.preventDefault();
                });
            });
        });
    };
    
    return {
        init: function() {
            initInputs();
            initExtraDropdownOptionInputs();
        }
    };
}();

var DropzoneDemo = function () {
    var showErrorMsg = function(form, type, msg) {
        // var alert = $('<div id="alert" class="form-group m-form__group">\
         //    <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
		// 	<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
		// 	<span></span>\
		// </div></div>');
        var alert = $('<div id="alert" class="form-group m-form__group">\
            <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div></div>');

        form.find('#alert').remove();

        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    };

    //== Private functions
    var agency_picture = function () {
        // file type validation
        Dropzone.options.mDropzone = {
            url: baseUrl + '/ita/' + tenantId + '/settings/change-agency-picture',
            params: {},
            paramName: 'agency_picture', // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            // Prevents Dropzone from uploading dropped files immediately
            autoProcessQueue: false,
            init: function () {
                var submitButton = document.querySelector("#form-wizard-submit");
                var pictureDropzone = this; // closure
                var upload_btn = $('#form-wizard-submit');

                submitButton.addEventListener('click', function () {
                    upload_btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    $(pictureDropzone.element).find('.dz-progress').show();
                    pictureDropzone.processQueue(); // Tell Dropzone to process all queued files.
                });

                this.on('addedfile', function (file) {
                    $(this.element).find('.dz-progress').hide();
                    upload_btn.removeAttr('disabled').css('cursor', 'pointer');
                });

                this.on('maxfilesexceeded', function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('sending', function (file, xhr, formData) {
                    var csrf = $('meta[name="csrf-token"]').attr('content');

                    formData.append('_csrf', csrf);
                });

                this.on('success', function (file, response) {
                    if (response) {
                        // similate 2s delay
                        setTimeout(function() {
                            pictureDropzone.removeAllFiles();
                            upload_btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response.photo) {
                                showErrorMsg($('#m_wizard_form_step_2 #agency_dropzone_div'), 'success', GetTranslation('agency_picture_upload_success'));

                            }

                            if (response.error)
                                showErrorMsg($('#m_wizard_form_step_2 #agency_dropzone_div'), 'danger', GetTranslation('agency_picture_upload_error'));
                        }, 500);
                    }
                });
            }
        };
    };

    return {
        // public functions
        init: function() {
            agency_picture();
            //handleProfilePictureFormSubmit();
        }
    };
}();

DropzoneDemo.init();

jQuery(document).ready(function() {
    initValidate();
    DropzoneDemo.init();
    window.accordionId = 1;
    WizardDemo.init();
    RequisitiesForm.init();
    initPageInputs.init();
    AgencyContactsForm.init();
    //
    $('body').find('form').attr('autocomplete', 'off');

    $('#acc_set_usage_currency').select2({
        language: tenant_lang,
        placeholder: GetTranslation("acc_set_usage_currency", 'Usage currency'),
        width: '100%',
    });
    $('#account_main_currency').select2({
        language: tenant_lang,
        placeholder: GetTranslation("acc_set_main_currency", 'Usage main currency'),
        width: '100%',
        minimumResultsForSearch: Infinity,
    });

    $('#account_currency_display').select2({
        language: tenant_lang,
        placeholder: GetTranslation("acc_set_main_currency", 'Usage main currency'),
        width: '100%',
        minimumResultsForSearch: Infinity,
    });


    var allCurrency = [];

    $("#acc_set_usage_currency option").each(function() {
        allCurrency.push({
            id: $(this).val(),
            text: $(this).text()
        });
    });

    var objCur;
    var allCurrencyFiltered = [];

    dataUsageCurr=[];

    $('#acc_set_usage_currency').change(function () {
        allCurrencyFiltered = [];

        usageCurr = $('#acc_set_usage_currency').val();
        if (usageCurr.length > 0) {
            usageCurr.forEach(function (str) {
                objCur = allCurrency.filter(function (obj) {
                    return obj.id == str;
                });
                allCurrencyFiltered.push(objCur[0]);
            });
            $('#account_main_currency').empty();

            $('#account_main_currency').select2({
                language: tenant_lang,
                data: allCurrencyFiltered
            });
        }
    });

    $('#add-set-button').on('click',function () {
        $(this).hide();
        $('#create_requisites_form').show();
    });

    $('#delete-logo').on('click', function() {
        var showErrorMsg = function(form, type, msg) {
            var alert = $('<div id="alert" class="form-group m-form__group">\
            <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div></div>');

            form.find('#alert').remove();

            alert.prependTo(form);
            //alert.animateClass('fadeIn animated');
            alert.find('span').html(msg);
        };
        swal({
            title: GetTranslation('delete_question','Are you sure?'),
            text: GetTranslation('tasks_delete_message1', 'Are you sure to delete this'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
            cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
            reverseButtons: true
        }).then(function(result){
            if(result.value){
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/settings/change-agency-picture',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        tenantId: tenantId,
                        _csrf: $('meta[name="csrf-token"]').attr('content'),
                        action: 'delete'
                    },
                    success: function (data) {
                        console.log(data);


                        if (data) {
                            // similate 2s delay
                            setTimeout(function() {
                                if (data.photo === null) {
                                    showErrorMsg($('#m_wizard_form_step_2 #agency_dropzone_div'), 'success', GetTranslation('agency_picture_delete_success'));
                                }
                                if (data.error)
                                    showErrorMsg($('#m_wizard_form_step_2 #agency_dropzone_div'), 'danger', GetTranslation('agency_picture_delete_error'));
                            }, 500);
                        }
                    },
                    error: function () {
                        console.log(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
            }
        });

    });
});

function initValidate(){
    $('#wizard_form_2')
        .find('[valid="phoneNumber"]')
        .intlTelInput({
            utilsScript: "/plugins/phone-validator/build/js/utils.js",
            autoPlaceholder: true,
            preferredCountries: ['ua', 'ru'],
            allowExtensions: false,
            autoFormat: true,
            autoHideDialCode: true,
            customPlaceholder: null,
            defaultCountry: "ua",
            geoIpLookup: null,
            nationalMode: false,
            numberType: "MOBILE"
        }).then(function (data) {
        $('#agency-phones-repeater').find('.validate-phone').each(function(index, el) {
            el = $(this);
            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
            var formated = '+999 99 999 9999';
            $(el).inputmask(formated);
            $(el).on("countrychange", function (e, countryData) {
                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                $(el).inputmask(formated)
            });
        });
    });
    $('#wizard_form_2')
        .find('[valid="phoneNumber_add"]')
        .intlTelInput({
            utilsScript: "/plugins/phone-validator/build/js/utils.js",
            autoPlaceholder: true,
            preferredCountries: ['ua', 'ru'],
            allowExtensions: false,
            autoFormat: true,
            autoHideDialCode: true,
            customPlaceholder: null,
            defaultCountry: "ua",
            geoIpLookup: null,
            nationalMode: false,
            numberType: "MOBILE"
        }).then(function (data) {
        $('#agency-phones-repeater').find('.validate-add-phone').each(function(index, el) {
            el = $(this);
            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
            var formated = '+999 99 999 9999';
            $(el).inputmask(formated);
            $(el).on("countrychange", function (e, countryData) {
                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                $(el).inputmask(formated);
            });
        });
    });
    $('#wizard_form_3')
        .find('[valid="phoneNumber_1"]')
        .intlTelInput({
            utilsScript: "/plugins/phone-validator/build/js/utils.js",
            autoPlaceholder: true,
            preferredCountries: ['ua', 'ru'],
            allowExtensions: false,
            autoFormat: true,
            autoHideDialCode: true,
            customPlaceholder: null,
            defaultCountry: "ua",
            geoIpLookup: null,
            nationalMode: false,
            numberType: "MOBILE"
        }).then(function (data) {

        $('.requisites_sets_container').find('.validate-phone').each(function(index, el) {
            el = $(this);
            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
            var formated = '+999 99 999 9999';
            $(el).inputmask(formated);
            $(el).on("countrychange", function (e, countryData) {
                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                $(el).inputmask(formated)
            });
        });
    });

    $('#wizard_form_3 input, #wizard_form_3 button, #wizard_form_3 textarea').on('keyup', function () {
        $('[data-wizard-action="prev"],[data-wizard-action="next"]').parent().addClass('topopup');
        $('#wizard_form_3').data('need-save', true);
    });

    $('#wizard_form_3')
        .find('[valid="phoneNumber"]')
        .intlTelInput({
            utilsScript: "/plugins/phone-validator/build/js/utils.js",
            autoPlaceholder: true,
            preferredCountries: ['ua', 'ru'],
            allowExtensions: false,
            autoFormat: true,
            autoHideDialCode: true,
            customPlaceholder: null,
            defaultCountry: "ua",
            geoIpLookup: null,
            nationalMode: false,
            numberType: "MOBILE"
        }).then(function (data) {

        $('#requisite-form-area').find('.validate-phone').each(function(index, el) {
            // console.log($('.requisites_sets_container').find('.validate-phone'));
            el = $(this);
            // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
            var formated = '+999 99 999 9999';
            $(el).inputmask(formated);
            $(el).on("countrychange", function (e, countryData) {
                var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                $(el).inputmask(formated)
            });
        });
    });

}

function getRequisitesFields() {
    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/settings/get-fields',
        dataType: 'json',
        type : "GET",
        success: function (data) {
            console.log(data);
            $('#m_requisites_accordion').append(formatAccordionRequisites(data, true, false));
            editRequisiteSet(false);
            initRemoveSetButton();
            checkForm();
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function editRequisiteSet(accordionId) {
    var editButton;
    if (accordionId === false) {
        editButton = $('.edit-requisites-set');
    } else {
        editButton = $('#requisite-edit-'+accordionId+'');
    }
    console.log(accordionId);
    editButton.click(function () {
        var setId = {};
        var accordionId = $(this).attr("accordion-id");
        var requisiteBody = $('#requisites_accordion_'+accordionId+'_item_body');
        setId.set_id = $(this).attr("set-id");
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/settings/get-fields-update-requisites',
            dataType: 'json',
            type : "GET",
            data: setId,
            success: function (data) {
                requisiteBody.empty();
                requisiteBody.append(formatUpdateAccordionRequisite(data,accordionId));
                saveRequisiteSet();
                initRemoveSetButton();
                checkForm();
                initInputs();
                initValidate();
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
}

function saveRequisiteSet() {
    $('.save-requisites-set').click(function () {
        var accordionId = $(this).attr("accordion-id");
        var requisiteBody = $('#requisites_accordion_'+accordionId+'_item_body');
        var requisiteForm = $('#form-requisite-accordion-'+accordionId+'');
        formData = requisiteForm.serializeArray();
        $.ajax({
            url: baseUrl + '/ita/' + tenantId + '/settings/update-requisite',
            dataType: 'json',
            type : "GET",
            data: formData,
            // data: setId,
            success: function (data) {
                requisiteBody.empty();
                console.log(data);
                requisiteBody.append(formatAccordionRequisites(data, false, accordionId));
                for (setId in data) {
                    A = data[setId];
                    setName = '';
                    for (group in A) {

                        for (field in A[group]) {
                            if (A[group][field].in_header)
                                setName += A[group][field].value + ' ';
                        }
                    }
                    $('#requisites_accordion_'+accordionId).find('.m-accordion__item-title').text(setName);
                }
                editRequisiteSet(accordionId);
                initRemoveSetButton();
                checkForm();
                initValidate();
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
}

function getNewRequisitesFields() {
    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/settings/get-generated-fields',
        dataType: 'json',
        type : "GET",
        success: function (data) {
            fields = '';
            var groupName, A,
                j, X,
                k, Q;


            var arr = Object.values(data);
            // console.log(arr);
            arr.sort(function(a, b){
                return a.count - b.count;
            });

            for (groupid in arr) {
//                 function compareReq(ReqA, ReqB) {
//                     return ReqA.count - ReqB.count;
//                 }
//                 var t = data.sort(compareReq);
//                 // for(i=0, i < )
// console.log(t);

                // var arr = Object.values(data);
                // //console.log(arr);
                // arr.sort(function(a, b){
                //     return a.count - b.count;
                // });
                // console.log(arr);
                // arr.forEach(function(el, index){
                //     console.log(i);
                // });
                A = arr[groupid];

                // console.log(A.count, data);
                groupName = A.name
                fields += '<div class=""><div class="m-form__heading"><h2 class="m-form__heading-title">';
                fields += groupName + ':';
                fields += '</h2></div></div>';

                for (j in A.fields) {
                    X = A.fields[j];
                    var requisitesId = X[0];
                    var type = X[1];
                    var fieldName = X[2];
                    var variants = X[3];


                    fields += '<div class="form-group m-form__group">';
                    // fields += '<label class="">';
                    // fields += fieldName + ':';
                    // fields += '</label>';
                    // fields += '<div class="col-xl-9 col-lg-9">';
                    fields = mappingType(type, fieldName, requisitesId, fields, variants,X[4],X[5]);
                    fields += '</div>'
                };
            };

            $('#requisite-form-area').append(fields);
            initInputs();
            initRemoveSetButton();
            initValidate();
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function mappingType(type, fieldName, requisitesId, fields, variants,placeholder,description) {
    if(placeholder == null || placeholder == undefined)
        placeholder = '';
    if(description == null || description == undefined)
        description = '';

    // fields = '<div class="form-group m-form__group">';
    switch (type) {
        case 0:
            fields += '<label>'+fieldName+'</label>'
                + '<input type="text" name="Requisites['+requisitesId+']" class="form-control m-input" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 1:
            fields += '<label>'+fieldName+'</label>'
                +'<textarea name="Requisites['+requisitesId+']" class="form-control m-input" rows="3" placeholder="'+placeholder+'"></textarea>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 2:
            fields += '<label>'+fieldName+'</label>'
                +'<input class="form-control m-input" name="Requisites['+requisitesId+']" type="number" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 3:
            fields += '<label>'+fieldName+'</label>'
                +'<input class="form-control m-input" type="url" name="Requisites['+requisitesId+']" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 4:
            fields += '<label class="">'+fieldName+'</label>'
                +'<div class="m-dropzone dropzone"><input type="hidden" required name="Requisites['+requisitesId+']" placeholder="'+placeholder+'"></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 5:
            variants = JSON.parse(variants);
            fields += '<label>'+fieldName+'</label>'
                +'<select type="text" name="Requisites['+requisitesId+']" class="form-control m-input" placeholder="'+placeholder+'">';
            variants.forEach(function(item) {
                fields += '<option name="'+item+'">'+item+'</option>';
            });
            fields += '</select>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 6:
            variants = JSON.parse(variants);
            fields += '<label>'+fieldName+'</label>'
                +'<select multiple="" name="Requisites['+requisitesId+'][]" class="form-control m-input m-bootstrap-select m_selectpicker" placeholder="'+placeholder+'">';
            variants.forEach(function(item) {
                fields += '<option name="'+item+'">'+item+'</option>';
            });
            fields += '</select>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 7:
            fields += '<label>'+fieldName+'</label>'
                +'<div class="input-group date">';
            fields += '<input type="text" class="form-control m-input date-picker-req" required name="Requisites['+requisitesId+']" placeholder="'+placeholder+'">';
            fields += '<div class="input-group-append">';
            fields += '<span class="input-group-text">';
            fields += '<i class="la la-calendar-check-o"></i>';
            fields += '</span></div></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 8:
            fields += '<label class="m-checkbox">' +
                '<input type="hidden" name="Requisites['+requisitesId+']" value="0">'
                +'<input type="checkbox" name="Requisites['+requisitesId+']" value="1">'+fieldName;
            return fields+'<span class="m-form__help">'+description+'</span></label>';
        case 9:
            variants = JSON.parse(variants);
            fields += '<label>'+fieldName+'</label>'
                +'<div class="m-radio-list">';

            variants.forEach(function(item,i) {
                fields += '<label class="m-radio">';
                fields += '<input type="radio" value="'+item+'" name="Requisites['+requisitesId+']" '+(i==0?'checked':'')+' >'+item+'';
                fields += '<span></span>';
                fields += '</label>';
            });

            return fields+'<span class="m-form__help">'+description+'</span></div>';
        case 10:
            fields += '<label>'+fieldName+'</label>'
                +'<div class="input-group m-input-group"><div class="input-group-prepend"><span class="input-group-text">@</span></div>' +
                '<input class="form-control m-input masked-input" data-inputmask="\'alias\': \'email\'" name="Requisites['+requisitesId+']" type="text" placeholder="'+placeholder+'"></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 11:
            fields += '<label>'+fieldName+'</label>'
                +'<div class="input-group m-input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>'
                // +'<input class="form-control m-input masked-input" data-inputmask="\'mask\': \'99(999)999-9999\'" name="Requisites['+requisitesId+']" type="tel" placeholder="'+placeholder+'"></div>';
                +'<input class="form-control m-input masked-input validate-phone input_phone_visible" id="account_settings_phone" name="Requisites['+requisitesId+']"  valid="phoneNumber"></div>';
        // <input class="form-control m-input masked-input validate-phone input_phone_visible" name="Contacts[phone]" id="order_edit__contact_phone_input"  valid="phoneContactNumber" value="<?= $phone['value'] ?>" />
            return fields+'<span class="m-form__help">'+description+'</span>';
    }
}

    function formatAccordionRequisites(data,init, accordionId) {
    var setName, A,
        groupName, X,
        position, Q;
    var fields = '';
    console.log(data);
    for (setId in data) {
        A = data[setId];
        setName = '';
        for(group in A)
        {

            for(field in A[group]) {
                if (A[group][field].in_header)
                    setName += A[group][field].value + ' ';
            }
        }
        if (init == true) {
            var accordionId = window.accordionId;
        }
        if (init == true) {
            fields += '<div class="m-accordion__item">';
            fields += '<div class="m-accordion__item-head collapsed" role="tab" id="requisites_accordion_'+accordionId+'" data-toggle="collapse" href="#requisites_accordion_'+accordionId+'_item_body" aria-expanded="false">';
            fields += '<span class="m-accordion__item-icon">';
            fields += '<i class="fa flaticon-list-3"></i>';
            fields += '</span>';
            fields += '<span class="m-accordion__item-title">';
            fields += setName;
            fields += '</span>';
            fields += '<span class="m-accordion__item-mode"></span>';
            fields += '</div>';
            fields += '<div class="m-accordion__item-body collapse" id="requisites_accordion_'+accordionId+'_item_body" class="" role="tabpanel" aria-labelledby="requisites_accordion_'+accordionId+'_item_head" data-parent="#m_requisites_accordion">';
        }
        fields += '<div class="tab-content m--padding-30">';
        for (groupName in A) {
            X = A[groupName];
            fields += '<div class="m-form__heading">';
            fields += '<h4 class="m-form__heading-title">';
            fields += groupName;
            fields += '</h4>';
            fields += '</div>';
            fields += '<div class="form-group m-form__group m-form__group--sm row">';
            for (position in X) {
                var requisite = X[position];
                var setId = requisite['set_id'];
                // console.log(requisite);

                fields += '<label class="col-xl-4 col-lg-4 col-form-label">';
                fields += requisite['name'];
                fields += '</label>';
                fields += '<div class="col-xl-8 col-lg-8">';
                if(requisite.type == 4 && requisite['value']!='') {
                    fields += '<img src="'+baseUrl+'/'+requisite['value']+'">';
                }
                else
                if(requisite.type == 8){
                    if(requisite['value'] == '1')
                        fields += '<i class="fa fa-check"></i>';
                    else
                        fields += '<i class="fa fa-close"></i>';
                }
                else{
                    fields += '<span class="m-form__control-static">';
                    fields += requisite['value'];
                    fields += '</span>';
                }
                fields += '</div>';

            }
            fields += '</div><div class="separator"></div>';
        }
        fields += '<div class="row">';
        fields += '<div class="col-12 text-right">';
        fields += '<button type="button" class="btn btn-danger delete-requisites-set" set-id="'+setId+'" accordion-id="'+accordionId+'" >';
        fields += GetTranslation('delete',"Delete");
        fields += '</button> ';
        fields += '<button type="button" id="requisite-edit-'+accordionId+'" class="btn btn-success edit-requisites-set" set-id="'+setId+'" accordion-id="'+accordionId+'" >';
        fields += GetTranslation('edit',"Edit");
        fields += '</button>';
        fields += '</div>';
        fields += '</div>';
        if (init == true) {
            fields += '</div>';
            fields += '</div>';
            fields += '</div>';
            window.accordionId++;
        }
    }
    return fields;
}

function formatUpdateAccordionRequisite(data,accordionId) {
    var setName, A,
        groupName, X,
        position, Q;
    var fields = '';
    for (setName in data) {
        A = data[setName];
        fields += '<div class="tab-content m--padding-30">';
        fields += '<form id="form-requisite-accordion-'+accordionId+'">';

        for (groupName in A) {
            X = A[groupName];
            fields += '<div class="m-form__heading">';
            fields += '<h4 class="m-form__heading-title">';
            fields += groupName;
            fields += '</h4>';
            fields += '</div>';
            for (position in X) {
                var requisite = X[position];
                var setId = requisite['set_id'];
                fields += '<div class="form-group m-form__group m-form__group--sm row" style="margin-bottom: 10px;">';
                fields += '<label class="col-xl-4 col-lg-4 col-form-label">';
                fields += requisite['name'];
                fields += '</label>';
                fields += '<div class="col-xl-8 col-lg-8">';
                fields = mappingTypeAccordion(requisite['type'], groupName, requisite['id'], fields, requisite['variants'], requisite['value'],requisite['placeholder']);
                fields += '</div>';
                fields += '</div>';
            }
        }
        fields += '<input hidden name="set_id" value="'+setId+'">';
        fields += '<div class="form-group m-form__group m-form__group--sm row">';
        fields += '<div class="col-12 text-right">';
        fields += '<button type="button" class="btn btn-success save-requisites-set edit_button" set-id="'+setId+'" accordion-id="'+accordionId+'" >';
        fields += GetTranslation('save',"Save");
        fields += '</button>';
        fields += '</div>';
        fields += '</div>';

        fields += '</form>';

        fields += '</div>';
    }
    return fields;
}

function mappingTypeAccordion(type, fieldName, requisitesId, fields, variants, value,placeholder,description) {
    if(placeholder == null || placeholder == undefined)
        placeholder = '';
    if(description == null || description == undefined)
        description = '';

    switch (type) {
        case 0:
            fields += '<input type="text" name="Requisites['+requisitesId+']" class="form-control m-input" value="'+value+'" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 1:
            fields += '<textarea name="Requisites['+requisitesId+']" class="form-control m-input" rows="3" placeholder="'+placeholder+'>value</textarea>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 2:
            fields += '<input class="form-control m-input" name="Requisites['+requisitesId+']" type="number" value="'+value+'" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 3:
            fields += '<input class="form-control m-input" type="url" name="Requisites['+requisitesId+']" value="'+value+'" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 4:
            fields += '<div class="m-dropzone dropzone"><input class="form-control m-input" name="Requisites['+requisitesId+']" type="hidden" required value="'+value+'" placeholder="'+placeholder+'">';
            return fields+'<span class="m-form__help">'+description+'</span></div>';
        case 5:
            variants = JSON.parse(variants);
            fields += '<select type="text" name="Requisites['+requisitesId+']" class="form-control m-input" value="'+value+'" placeholder="'+placeholder+'">';
            variants.forEach(function(item) {
                fields += '<option name="'+item+'"'+(item == value ? ' selected="selected"' : '')+'>'+item+'</option>';
            });
            fields += '</select>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 6:
            variants = JSON.parse(variants);
            var vs = JSON.parse(value);
            console.log(variants);
            fields += '<select multiple="" name="Requisites['+requisitesId+'][]"  class="form-control m-input m-bootstrap-select m_selectpicker" placeholder="'+placeholder+'">';
            variants.forEach(function(item) {
                console.log(vs.indexOf(item));
                console.log(item);
                fields += '<option name="'+item+'"'+(vs.indexOf(item)>=0 ? ' selected="selected"' : '')+'>'+item+'</option>';
            });
            fields += '</select>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 7:
            fields += '<div class="input-group date">';
            fields += '<input type="text" class="form-control m-input date-picker-req" required name="Requisites['+requisitesId+']"  value="'+value+'" placeholder="'+placeholder+'">';
            fields += '<div class="input-group-append">';
            fields += '<span class="input-group-text">';
            fields += '<i class="la la-calendar-check-o"></i>';
            fields += '</span></div></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 8:
            fields += '<label class="m-checkbox"><input type="hidden" name="Requisites['+requisitesId+']" value="0">' +
                '<input type="checkbox" name="Requisites['+requisitesId+']" value="1" '+(value == '1' ? ' checked="checked"' : '')+'>';
            return fields+'<span class="m-form__help">'+description+'</span></label>';
        case 9:
            fields += '<div class="m-radio-list">';
            variants = JSON.parse(variants);
            variants.forEach(function(item) {
                fields += '<label class="m-radio">';
                fields += '<input name="Requisites['+requisitesId+']" type="radio" value="'+item+'" '+(item == value ? ' checked="checked"' : '')+'>';
                fields += item;
                fields += '<span></span>';
                fields += '</label>';
            });
            return fields+'<span class="m-form__help">'+description+'</span></div>';
        case 10:
            fields += '<div class="input-group m-input-group"><div class="input-group-prepend"><span class="input-group-text">@</span></div>' +
                '<input class="form-control m-input masked-input" data-inputmask="\'alias\': \'email\'" name="Requisites['+requisitesId+']" type="text" value="'+value+'" placeholder="'+placeholder+'"></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
        case 11:
            fields += '<div class="input-group m-input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>' +
                '<input class="form-control m-input masked-input validate-phone input_phone_visible" id="account_settings_phone" name="Requisites['+requisitesId+']"  valid="phoneNumber_1" value="'+value+'"></div>';
            // '<input class="form-control m-input masked-input" data-inputmask="\'mask\': \'99(999)999-9999\'" name="Requisites['+requisitesId+']" type="tel" value="'+value+'" placeholder="'+placeholder+'"></div>';
            return fields+'<span class="m-form__help">'+description+'</span>';
    }
}

function initRemoveSetButton() {
    $('.delete-requisites-set').off('click').on('click',function () {
        var set_id = $(this).attr('set-id');
        var acc_id = $(this).attr('accordion-id');
        console.log(set_id);

        swal({
            title: GetTranslation('delete_question','Are you sure?'),
            text: GetTranslation('acc_set_delete_message','Are you sure to delete this set?'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
            cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
                console.log(acc_id);
                console.log(set_id);
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/settings/remove-set?id=' + set_id,
                    success: function (response) {
                        $('#requisites_accordion_'+acc_id).parent().remove();
                        checkForm();
                    }
                });
            }
        });

    });

}

function checkForm() {
    if($('#m_requisites_accordion .m-accordion__item').length > 0) {
        $('#create_requisites_form').hide();
        $('#add-set-button').show();
    }
    else {
        $('#create_requisites_form').show();
        $('#add-set-button').hide();
    }
}

function initInputs() {
    $('.date-picker-req').datepicker({
        todayHighlight: true,
        format: date_format_datepicker,
        language: tenant_lang,
        autoclose: true,
        orientation: "bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    $('.masked-input').inputmask();
}
