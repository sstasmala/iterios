var import_details_datatable;

(function($){
    $(document).ready(function(){
        initImportHistoryDatatable();
        
        function initImportHistoryDatatable(){
            var csrf_val = $('meta[name="csrf-token"]').attr('content');
            var columns = [
                {
                    field: 'col_name',
                    title: GetTranslation('impedt_datatable_col_contact_name_title', 'Name'),
                    sortable: false,
                    overflow: 'visible',
                    template: function(data) {
                        var contact_name = 'Александр';
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-left" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">' + contact_name + '</a>\
                                <div class="m-dropdown__wrapper">\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                    <div class="m-dropdown__inner">\
                                        <div class="m-dropdown__body">\
                                            <div class="m-dropdown__content">\
                                                <form class="etitable-form">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_name_label', 'Contact name') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[last_name]">\
                                                            <input type="text" name="value" class="form-control m-input" value="' + contact_name + '">\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                </div>\
                            </div>';
                    }
                },
                {
                    field: 'col_date',
                    title: GetTranslation('impedt_datatable_col_contact_last_name_title', 'Last name'),
                    sortable: false,
                    overflow: 'visible',
                    template: function(data) {
                        var contact_last_name = 'Петров';
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-left" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">' + contact_last_name + '</a>\
                                <div class="m-dropdown__wrapper">\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                    <div class="m-dropdown__inner">\
                                        <div class="m-dropdown__body">\
                                            <div class="m-dropdown__content">\
                                                <form class="etitable-form">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_last_name_label', 'Contact last name') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[last_name]">\
                                                            <input type="text" name="value" class="form-control m-input" value="' + contact_last_name + '">\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                </div>\
                            </div>';
                    }
                },
                {
                    field: 'col_type',
                    title: GetTranslation('impedt_datatable_col_contact_pnone_title', 'Phone'),
                    sortable: false,
                    overflow: 'visible',
                    template: function(data) {
                        var contact_phone = '38(067)123-4567';
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-left" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">' + contact_phone + '</a>\
                                <div class="m-dropdown__wrapper">\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                    <div class="m-dropdown__inner">\
                                        <div class="m-dropdown__body">\
                                            <div class="m-dropdown__content">\
                                                <form class="etitable-form">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_pnone_label', 'Contact phone') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[last_name]">\
                                                            <input type="text" name="value" class="form-control m-input phone-mask" value="' + contact_phone + '">\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>\
                                </div>\
                            </div>';
                    }
                },
                {
                    field: 'col_processed',
                    title: GetTranslation('impedt_datatable_col_contact_email_title', 'Email'),
                    sortable: false,
                    textAlign: 'center',
                    overflow: 'visible',
                    template: function(data) {
                        var contact_email = 'mail@to.me';
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-right" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">' + contact_email + '</a>\
                                <div class="m-dropdown__wrapper">\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>\
                                    <div class="m-dropdown__inner">\
                                        <div class="m-dropdown__body">\
                                            <div class="m-dropdown__content">\
                                                <form class="etitable-form">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_pnone_label', 'Contact phone') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[contact_email]">\
                                                            <input type="text" name="value" class="form-control m-input email-mask" value="' + contact_email + '">\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>\
                                </div>\
                            </div>';
                    }
                },
                {
                    field: 'col_added',
                    title: GetTranslation('impedt_datatable_col_contact_birthday_title', 'Birthday'),
                    sortable: false,
                    textAlign: 'center',
                    overflow: 'visible',
                    template: function(data) {
                        var upper_mode_for_last_items = 3;
                        var contact_birthday = '25.08.1988';
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-right" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">' + contact_birthday + '</a>\
                                <div class="m-dropdown__wrapper">\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>\
                                    <div class="m-dropdown__inner">\
                                        <div class="m-dropdown__body">\
                                            <div class="m-dropdown__content">\
                                                <form class="etitable-form">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_birthday_label', 'Contact birthday') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[contact_birthday]">\
                                                            <div class="input-group pull-right">\
                                                                <input type="text" name="value" value="' + contact_birthday + '" class="form-control m-input birthday_picker" placeholder="' + GetTranslation('impedt_datatable_contact_birthday_select_placeholder', 'Select contact birthday') + '">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>\
                                </div>\
                            </div>';
                    }
                },
                {
                    field: 'col_errors',
                    title: GetTranslation('impedt_datatable_col_contact_tag_title', 'Tag'),
                    sortable: false,
                    textAlign: 'center',
                    overflow: 'visible',
                    template: function(data) {
                        var contact_tag = 'VIP';
                        var contact_tag_id = 2;
                        return '\
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-right" data-last-uppers="3" data-dropdown-toggle="click" data-dropdown-persistent="true">\
                                <span class="m-dropdown__toggle m-badge m-badge--brand m-badge--wide" data-toggle="modal" data-target="#m_modal_' + data.id + '">' + contact_tag + '</span>\
                                <div class="modal fade close-after-success" id="m_modal_' + data.id + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">\
                                    <div class="modal-dialog" role="document">\
                                        <div class="modal-content">\
                                            <div class="modal-header">\
                                                <h5 class="modal-title" id="exampleModalLabel">' + GetTranslation('impedt_datatable_col_contact_tag_label', 'Contact tag') + '</h5>\
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                                                    <span aria-hidden="true">&times;</span>\
                                                </button>\
                                            </div>\
                                            <div class="modal-body m-dropdown__content">\
                                                <form class="etitable-form text-left">\
                                                    <div class="row align-items-center">\
                                                        <div class="col-12 labels">\
                                                            <h6>' + GetTranslation('impedt_datatable_col_contact_tag_label', 'Contact tag') + '</h6>\
                                                        </div>\
                                                        <div class="col-12 mb-1"></div>\
                                                        <div class="col-12 col-sm-8 mb-2 mb-sm-0 inputs">\
                                                            <input type="hidden" name="_csrf" value="' + csrf_val + '">\
                                                            <input name="ajax_url" type="hidden" value="/import/set-data">\
                                                            <input name="id" type="hidden" value="' + data.id + '">\
                                                            <input name="property" type="hidden" value="Import[contact_email]">\
                                                            <select name="value" class="form-control m-input tag-select">\
                                                                <option value="' + contact_tag_id + '" selected>' + contact_tag + '</option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-12 col-sm-4 buttons text-left text-sm-right">\
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>\
                                                            <button type="button" class="btn btn-sm etitable-form-cancel" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i></button>\
                                                        </div>\
                                                    </div>\
                                                </form>\
                                                <div class="preloader" style="display:none;text-align:center;">\
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';
                    }
                }
            ];
            
            import_details_datatable = $('#import-details-datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/tasks/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Swith last n dropdowns in UP mode
                        checkIfThisRowInUpperMode(row, data, index);
                        // Add masks for inputs
                        $(row).find('.phone-mask').inputmask("99(999)999-9999");
                        $(row).find('.email-mask').each(function(){
                            Inputmask("email").mask(this);
                        });
                        // Init birthdate pickers
                        $(row).find('.birthday_picker').each(function(){
                            $(this).daterangepicker({
                                buttonClasses: 'm-btn btn',
                                applyClass: 'btn-primary',
                                cancelClass: 'btn-secondary',
                                singleDatePicker: true,
                                showDropdowns: true,
                                minDate: moment(new Date()).subtract(100, 'years').format(date_format),
                                maxDate: moment(new Date()).format(date_format),
                                locale: {
                                    format: date_format,
                                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                                },
                                templates: {
                                    leftArrow: '<i class="la la-angle-left"></i>',
                                    rightArrow: '<i class="la la-angle-right"></i>'
                                }
                            }).click(function(e){
                                $('.daterangepicker').on('click', function(e){
                                    // It is fix for daterangepicker. It disble closing 
                                    // dropdown, when next and prev arrows was clicked
                                    e.stopPropagation();
                                });
                            });
                        });
                        // Init tag select
                        $(row).find('.tag-select').each(function(){
                            var select = this;
                            $(select).select2({
                                ajax: {
                                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                                    method: 'POST',
                                    dataType: 'json',
                                        data: function (params) {
                                        var query = {
                                            search: params.term,
                                            type: 'public'
                                        };
                                        return query;
                                    },
                                    delay: 1000,
                                    processResults: function (data)
                                    {
                                        var array = [];
                                        for (var i in data){
                                            array.push({id: data[i]['id'], text: data[i]['name']});
                                        }
                                        return {
                                            results: array
                                        };
                                    }
                                },
                                language: tenant_lang,
                                width: '100%',
                                placeholder: GetTranslation('impedt_datatable_col_contact_tag_placeholder', 'Select tag'),
                                dropdownParent: $(select).parent(),
                                tags: false
                            });
                        });
                        // Init editable dropdowns
                        initEditableForms(
                            row, 
                            function(){
                                alert('successCallback!');
                                $('.close-after-success').modal('hide');
                            },
                            function(){
                                alert('errorCallback');
                            }
                        );
                    },
                    callback : function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            function checkIfThisRowInUpperMode(row, data, index){
                $(row).find('.etitable-form-dropdown').each(function(){
                    var editable_dropdown = this;
                    if((editable_dropdown !== undefined)&&(editable_dropdown !== null)){
                        var last_uppers = $(editable_dropdown).data('last-uppers');
                        if((last_uppers !== undefined)&&(last_uppers !== null)){
                            var total_row = parseInt(import_details_datatable.dataSet.length);
                            var current_row = index+1;
                            if(((total_row - current_row) < last_uppers) && (current_row > last_uppers)){
                                $(editable_dropdown).addClass('m-dropdown--up');
                            }
                        }
                    }
                });
            }
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(import_details_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(import_details_datatable).on('m-datatable--on-layout-updated', function(e){
                // Remove dropdown shadow bug, after datatable update, when window was resized
                $('.modal-backdrop.show').fadeOut(200, function(){
                    $(this).remove();
                });
            });
        }
    });
})(jQuery);