(function($){
    $(document).ready(function(){
        ImportWizard.init();
    });
})(jQuery);

var ImportWizard = function () {
    var wizardEl = $('#import_wizard');
    var formEl = $('#import_form');
    var validator;
    var wizard;
    
    var initWizard = function () {
        wizard = new mWizard('import_wizard', {
            startStep: 1,
            manualStepForward: true
        });

        wizard.on('beforeNext', function(wizard) {
            if (validator.form() !== true) {
                wizard.stop();  // don't go to the next step
            }
        });

        wizard.on('change', function(wizard) {
            mUtil.scrollTop();
        });
    };

    var initValidation = function() {
        validator = formEl.validate({
            //== Validate only visible fields
            ignore: ".ignore",

            //== Validation rules
            rules: {
                "Import[module]": {
                    required: true
                }
            },

            //== Validation messages
            messages: {
                "Import[module]": {
                    required: GetTranslation('imp_crt_select_module_error_message', 'You must select a module')
                }
            },

            //== Display error
            invalidHandler: function(event, validator) {
                mUtil.scrollTop();

                swal({
                    "title": "",
                    "text": GetTranslation('imp_crt_form_has_error_message', 'There are some errors in your submission. Please correct them.'),
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            }
        });   
    };

    var initSubmit = function() {
        var btn = formEl.find('[data-wizard-action="submit"]');
        btn.on('click', function(e) {
            e.preventDefault();
            if (validator.form()) {
                mApp.progress(btn);
                formEl.submit();
            }
        });
    };
    
    var initFormWidgets = function() {
        // Init change module icons
        var input = $('#import-type-change-input').get()[0];
        var switchers = $('.change-item').get();
        $(switchers).each(function(){
            var switcher = this;
            $(switcher).on('click', function(){
                var module_name = $(switcher).data('module-name');
                if((module_name !== undefined) &&(module_name.length > 0)){
                    $(switchers).removeClass('selected');
                    $(switcher).addClass('selected');
                    $(input).val(module_name);
                } else {
                    console.log('Invalid module data!');
                }
            });
        });
        
        Dropzone.options.mImportDropzoneOne = {
            url: baseUrl + '/ita/' + tenantId + '/import/change-profile-picture',
            params: {},
            paramName: 'profile_picture', // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            // Prevents Dropzone from uploading dropped files immediately
            autoProcessQueue: false,
            init: function () {
//                var submitButton = document.querySelector("#change_picture_submit");
//                var pictureDropzone = this; // closure
//                var upload_btn = $('#change_picture_submit');
//
//                submitButton.addEventListener('click', function () {
//                    upload_btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
//                    $(pictureDropzone.element).find('.dz-progress').show();
//                    pictureDropzone.processQueue(); // Tell Dropzone to process all queued files.
//                });

                this.on('addedfile', function (file) {
//                    $(this.element).find('.dz-progress').hide();
//                    upload_btn.removeAttr('disabled').css('cursor', 'pointer');
                });

                this.on('maxfilesexceeded', function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('sending', function (file, xhr, formData) {
//                    var csrf = upload_btn.closest('form').find('input[name="_csrf"]').val();
//                    formData.append('_csrf', csrf);
                });

                this.on('success', function (file, response) {
//                    if (response) {
//                        // similate 2s delay
//                        setTimeout(function() {
//                            pictureDropzone.removeAllFiles();
//                            upload_btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
//
//                            if (response.photo) {
//                                showErrorMsg(upload_btn.closest('form'), 'success', GetTranslation('profile_picture_upload_success'));
//
//                                $('img.profile_picture').each(function(){ this.src = baseUrl + '/' + response.photo; });
//                            }
//
//                            if (response.error)
//                                showErrorMsg(upload_btn.closest('form'), 'danger', GetTranslation('profile_picture_upload_error'));
//                        }, 500);
//                    }
                });
            }
        };
        
        // Property select2 init
        $('.import_properties_select').select2({
            width: '100%',
            placeholder: GetTranslation('import_properties_select_placeholder', 'Select property type')
        });
        
        // Check all checkboxes
        $('.check_all_props').on('change', function(){
            var change_all_checkbox = this;
            $(this).parents('table').find('tbody .row_checkbox').each(function(){
                var row_checkbox = this;
                if(change_all_checkbox.checked){
                    row_checkbox.checked = true;
                } else {
                    row_checkbox.checked = false;
                }
            });
        });
    };

    return {
        // public functions
        init: function() {
            wizardEl = $('#import_wizard');
            formEl = $('#import_form');

            initWizard(); 
            initValidation();
            initSubmit();
            initFormWidgets();
        }
    };
}();