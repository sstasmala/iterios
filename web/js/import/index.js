var import_history_datatable;

(function($){
    $(document).ready(function(){
        initImportFilters();
        initImportHistoryDatatable();
        
        function initImportFilters(){
            var filter = $('#import-filter').get()[0];
            var filter_items = $(filter).find('.filter-item');
            $(filter_items).each(function(){
                var filter_item = this;
                $(filter_item).on('click', function(){
                    $(filter_items).removeClass('active');
                    $(filter_item).addClass('active');
                });
            });
        }
        
        function initImportHistoryDatatable(){
            var columns = [
                {
                    field: 'col_name',
                    title: GetTranslation('imphst_datatable_col_name_title', 'Name'),
                    sortable: false,
                    template: function(data) {
                        return '<a href="'+baseUrl + '/ita/' + tenantId + '/import/view?id=' + data.id + '" class="m-link m--font-bold">Import name</a>';
                    }
                },
                {
                    field: 'col_date',
                    title: GetTranslation('imphst_datatable_col_date_title', 'Date'),
                    sortable: false,
                    template: function(data) {
                        return '17.03.2018';
                    }
                },
                {
                    field: 'col_type',
                    title: GetTranslation('imphst_datatable_col_type_title', 'Type'),
                    sortable: false,
                    template: function(data) {
                        var type = "Type name";
                        return '<span class="m-badge m-badge--dot" style="background: #575962;"></span>\
                                <span class="m--font-bold ml-2">'+ type +'</span>';
                    }
                },
                {
                    field: 'col_processed',
                    title: GetTranslation('imphst_datatable_col_processed_title', 'Processed'),
                    sortable: false,
                    textAlign: 'center',
                    template: function(data) {
                        return '34';
                    }
                },
                {
                    field: 'col_added',
                    title: GetTranslation('imphst_datatable_col_added_title', 'Added'),
                    sortable: false,
                    textAlign: 'center',
                    template: function(data) {
                        return '<span class="m--font-success">28</span>';
                    }
                },
                {
                    field: 'col_errors',
                    title: GetTranslation('imphst_datatable_col_errors_title', 'Errors'),
                    sortable: false,
                    textAlign: 'center',
                    template: function(data) {
                        return '<span class="m--font-danger">6</span>';
                    }
                },
                {
                    field: 'col_actions',
                    title: GetTranslation('imphst_datatable_col_actions_title', 'Actions'),
                    sortable: false,
                    overflow: 'visible',
                    template: function(row, index, datatable) {
                        var dropup = (import_history_datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                        return '\
                        <div class="dropdown ' + dropup + '">\
                            <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="#"><i class="la la-mail-reply-all"></i> ' + GetTranslation('imphst_datatable_col_actions_rollback_label', 'Import rollback') + '</a>\
                                <a class="dropdown-item" href="#"><i class="la la-exclamation-triangle"></i> ' + GetTranslation('imphst_datatable_col_actions_error_file_label', 'Download error file') + '</a>\
                                <a class="dropdown-item" href="#"><i class="la la-file"></i> ' + GetTranslation('imphst_datatable_col_actions_original_file_label', 'Download original file') + '</a>\
                            </div>\
                        </div>\
                        <button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill ml-2" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></button>';
                    }
                }
            ];
            
            import_history_datatable = $('#import-history-datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/contacts/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                    filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(import_history_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(import_history_datatable).on('m-datatable--on-layout-updated', function(e){
                
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = import_history_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_dess_sms_buttons').css({display : "flex"});
                }else{
                    $('#bulk_dess_sms_buttons').hide();
                }
            }
        }
    });
})(jQuery);