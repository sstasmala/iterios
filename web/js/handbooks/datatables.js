class HBDatatablesMaster
{
    constructor() {
        this.datatables = {
            datatable_city: null,
            datatable_hotel: null,
            datatable_source: null,
            datatable_reminders_orders: null,
            datatable_reminders_requests: null,
            datatable_company_type: null,
            datatable_activity_type: null,
            datatable_company_status: null
        };
    }
    
    activateDatatable(datatable_name){
        var datatable = this.datatables['datatable_'+datatable_name];
        var datatable_id = 'datatable_' + datatable_name;
        
        this.changePortletTitle(datatable_id);
        
        if(datatable === null){
            this.destroyAllDatatables();
            switch(datatable_id){
                case 'datatable_city':
                    this.initDatatableCity(datatable_id);
                    break;
                case 'datatable_hotel':
                    this.initDatatableHotel(datatable_id); 
                    break;
                case 'datatable_source':
                    this.initDatatableSource(datatable_id);
                    break;
                case 'datatable_reminders_orders':
                    this.initDatatableRemindersOrders(datatable_id);
                    break;
                case 'datatable_reminders_requests':
                    this.initDatatableRemindersRequests(datatable_id);
                    break;
                case 'datatable_company_type':
                    this.initDatatableCompanyType(datatable_id);
                    break;
                case 'datatable_activity_type':
                    this.initDatatableActivityType(datatable_id);
                    break;
                case 'datatable_company_status':
                    this.initDatatableCompanyStatus(datatable_id);
                    break;
                default:
                    alert('Error! Can not find datatable with id = ' + datatable_id);
                    break;
            }
        } else {
            datatable.reload();
        }
    }
    
    // Init City datatable
    initDatatableCity(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'city_name',
                title: GetTranslation('hbi_nav_city_name_column', 'City'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Cityname</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'country_name',
                title: GetTranslation('hbi_nav_country_name_column', 'Country'),
                sortable: true,
                template: function(row, index, datatable) {
                    return 'Country';
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Hotel datatable
    initDatatableHotel(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            //        - Отель (название, звездность, город, страна аватарка кто создал)
            {
                field: 'hotel_name',
                title: GetTranslation('hbi_nav_hotel_name_column', 'Hotel'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Hotel</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'hotel_rate',
                title: GetTranslation('hbi_nav_hotel_rate_column', 'Hotel rate'),
                sortable: true,
                template: function(row, index, datatable) {
                    return '5 stars';
                }
            },
            {
                field: 'city_name',
                title: GetTranslation('hbi_nav_city_name_column', 'City'),
                sortable: true,
                template: function(row, index, datatable) {
                    return 'City';
                }
            },
            {
                field: 'country_name',
                title: GetTranslation('hbi_nav_country_name_column', 'Country'),
                sortable: true,
                template: function(row, index, datatable) {
                    return 'Country';
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Source datatable
    initDatatableSource(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'source_name',
                title: GetTranslation('hbi_nav_source_name_column', 'Source'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>TV advertising</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Reminders Orders datatable
    initDatatableRemindersOrders(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'reminder_name',
                title: GetTranslation('hbi_nav_reminder_name_column', 'Reminder name'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Reminder name</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Reminders Requests datatable
    initDatatableRemindersRequests(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'reminder_name',
                title: GetTranslation('hbi_nav_reminder_name_column', 'Reminder name'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Reminder name</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Company Type datatable
    initDatatableCompanyType(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'company_type',
                title: GetTranslation('hbi_nav_company_type_column', 'Company type'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Company type</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Activity Type datatable
    initDatatableActivityType(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'activity_type',
                title: GetTranslation('hbi_nav_activity_type_column', 'Activity type'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Activity type</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Init Company Status datatable
    initDatatableCompanyStatus(datatable_id){
        var config = this.getDefaultDatatableConfig();
        
        // Configurate datatable
        config.data.source.read.url = baseUrl + '/ita/' + tenantId + '/orders/get-data';
        config.columns = [
            {
                field: 'company_status',
                title: GetTranslation('hbi_nav_company_status_column', 'Company status'),
                sortable: true,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    var output =    '<span>Company status</span>\
                                    ' + ((is_system) ? '<i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('hbi_system_record_icon_popover_message', 'This is the system record') + '"></i>' : '');
                    return output;
                }
            },
            {
                field: 'creator_avatar',
                title: GetTranslation('hbi_nav_creator_avatar_column', 'Creator'),
                width: 140,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    // Demo row
                    row = {
                        responsible: {
                            id: 1,
                            photo: 'img/profile_default.png'
                        }
                    };
                    var html = GetTranslation('no_responsible_datatable_message', 'No responsible');
                    if(row.responsible !== null && row.responsible.id !== undefined) {
                        html = '<a href="'+ baseUrl + '/ita/' + tenantId + '">';
                        if(row.responsible.photo !== null){
                            html += '<img height="52" src="'+baseUrl+'/'+row.responsible.photo+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        } else {
                            html += '<img height="52" src="'+baseUrl+'/img/profile_default.png'+'" class="m--img-rounded m--marginless m--img-centered profile_picture">';
                        }
                        html += '</a>';
                    }
                    if(is_system){
                        html = GetTranslation('hbi_system_record_datatable_message', 'System');
                    }
                    return html;
                }
            },
            {
                field: 'actions',
                title: GetTranslation('hbi_nav_actions_column', 'Actions'),
                width: 100,
                textAlign: 'center',
                sortable: false,
                template: function(row, index, datatable) {
                    var randomizer = parseInt(Math.random()*100)%2;
                    var is_system = (randomizer===1);
                    return is_system ? '' : '<a href="'+baseUrl+'/ita/handbook/delete?id=1" class="delete-role m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="' + GetTranslation('delete', 'Delete') + '"><i class="la la-trash"></i></a>';
                }
            }
        ];
        this.datatables[datatable_id] = $('#'+datatable_id).mDatatable(config);
        
        // Init datatable event
        $(this.datatables[datatable_id]).on('m-datatable--on-init', function(e){

        });
        // Datatable layout updated event
        $(this.datatables[datatable_id]).on('m-datatable--on-layout-updated', function(e){

        });
    }
    
    // Default conpfig for all datatables
    getDefaultDatatableConfig(){
        var config = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: null,
                        method: 'GET',
                        params: {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            query: {

                            },
                            search: {
                                filter_responsible: userId
                            }
                            // custom query params
                        },
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 400
            },
            rows: {
                afterTemplate: function (row, data, index) {
                    // Init tooltip
                    $(row).find('[data-toggle="m-popover"]').each(function() {
                        mApp.initPopover($(this));
                    });
                },
                callback: function () {

                },
                // auto hide columns, if rows overflow. work on non locked columns
                autoHide: false
            },
            columns: null,
            toolbar: {
                layout: ['pagination', 'info'],
                placement: ['bottom'], //'top', 'bottom'
                items: {
                    pagination: {
                        type: 'default',
                        pages: {
                            desktop: {
                                layout: 'default',
                                pagesNumber: 6
                            },
                            tablet: {
                                layout: 'default',
                                pagesNumber: 3
                            },
                            mobile: {
                                layout: 'compact'
                            }
                        },
                        navigation: {
                            prev: true,
                            next: true,
                            first: true,
                            last: true
                        },
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    },
                    info: true
                }
            },
            translate: {
                records: {
                    processing: GetTranslation('datatable_data_processiong', 'Please wait...'),
                    noRecords: GetTranslation('datatable_records_not_found', 'No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: GetTranslation('pagination_first', 'First'),
                                prev: GetTranslation('pagination_previous', 'Previous'),
                                next: GetTranslation('pagination_next', 'Next'),
                                last: GetTranslation('pagination_last', 'Last'),
                                more: GetTranslation('pagination_more', 'More pages'),
                                input: GetTranslation('pagination_page_number', 'Page number'),
                                select: GetTranslation('pagination_page_size', 'Select page size')
                            },
                            info: GetTranslation('pagination_records_info', 'Displaying {{start}} - {{end}} of {{total}} records')
                        }
                    }
                }
            }
        };
        return config;
    }
    
    changePortletTitle(datatable_id){
        switch(datatable_id){
            case 'datatable_city':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-building');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_city', 'City'));
                break;
            case 'datatable_hotel':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-h-square');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_hotel', 'Hotel'));
                break;
            case 'datatable_source':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-server');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_source', 'Source'));
                break;
            case 'datatable_reminders_orders':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-bell');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_reminders_orders', 'Orders reminders'));
                break;
            case 'datatable_reminders_requests':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-bell');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_reminders_requests', 'Requests reminders'));
                break;
            case 'datatable_company_type':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-bookmark');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_company_type', 'Company type'));
                break;
            case 'datatable_activity_type':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-briefcase');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_activity_type', 'Activity type'));
                break;
            case 'datatable_company_status':
                $('#handbooks_data_portlet_icon').attr('class', 'fas fa-crown');
                $('#handbooks_data_portlet_title').text(GetTranslation('hbi_nav_company_status', 'Company status'));
                break;
            default:
                alert('Error! Can not find datatable with id = ' + datatable_id);
                break;
        }
    }
    
    destroyAllDatatables(){
        for(var key in this.datatables){
            if(this.datatables[key] !== null){
                this.datatables[key].destroy();
                this.datatables[key] = null;
            }
        }
    }
}

var DatatablesMaster = new HBDatatablesMaster();