(function($){
    $(document).ready(function(){
        
        initHandbooksCore();
        
        function initHandbooksCore(){
            //===== Variables declarate =====//
            var handbooks_portlet = $('#handbooks_data_portlet').get()[0];
            var nav_links_left = $('#handbooks_nav_portlet a.m-nav__link').get();
            var nav_links_grid = $('#handbooks_nav_grid a.m-nav-grid__item').get();
            
            //===== Functions calls =====//
            initNavigationLinks();
            initAddNewHandbooksItemModals();
            initCityFilters();
            initHotelFilters();
            
            //===== Functions declarate =====//
            function initNavigationLinks(){
                // Click on handbook link event
                $(nav_links_left).on('click', function(){
                    var link = this;
                    var datatable_name = $(link).data('table-name');

                    $(nav_links_left).closest('li.m-nav__item').removeClass('m-nav__item--active');
                    $(link).closest('li.m-nav__item').addClass('m-nav__item--active');
                    $(handbooks_portlet).find('.m-portlet__head-tools').removeClass('d-none');
                    $('#add_handbook_button').attr('data-target', '#m_modal_add_handbook_item_' + datatable_name);

                    if(datatable_name){
                        $(handbooks_portlet).find('.handbook-item').hide();
                        $(handbooks_portlet).find('#data_block_'+datatable_name).stop().fadeIn();
                        DatatablesMaster.activateDatatable(datatable_name);
                    } else {
                        alert('Handbooks nav menu error!');
                    }
                });

                // Click on handbook link in main datatables portlet event
                $(nav_links_grid).on('click', function(){
                    var link = this;
                    var datatable_name = $(link).data('table-name');
                    $('#handbooks_nav_portlet a.m-nav__link[data-table-name="'+ datatable_name +'"]').click();
                });
            }
            
            function initAddNewHandbooksItemModals(){
                initAddCityItemModal();
                initAddHotelItemModal();
                initAddSourceItemModal();
                initAddRemindersOrdersItemModal();
                initAddRemindersRequestsItemModal();
                initAddCompanyTypeItemModal();
                initAddActivityTypeItemModal();
                initAddCompanyStatusItemModal();
                
                function initAddCityItemModal() {
                    var modal = $('#m_modal_add_handbook_item_city').get()[0];
                    var form = $('#add_city_handbook_item_form').get()[0];
                    var name_input = $('#city_name_for_city_input').get()[0];
                    var coutry_select = $('#country_for_city_select').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            city_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create city from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    // Init country select2 for city create
                    $(coutry_select).select2({
                        ajax: {
                            url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                            method: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                    type: 'public',
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                };
                                return query;
                            },
                            delay: 1000,
                            processResults: function (data)
                            {
                                var array = [];
                                for (var i in data){
                                    array.push({id: data[i]['id'], text: data[i]['title']});
                                }
                                return {
                                    results: array
                                };
                            }
                        },
                        minimumInputLength: 2,
                        allowClear: false,
                        width: '100%',
                        dropdownParent: $(coutry_select).closest('div'),
                        placeholder: GetTranslation("hbi_form_country_select_placeholder", "Select country")
                    });
                    
                    // Shown modal event
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    
                    // Hidden modal event
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear city name input
                        $(name_input).val('');
                        
                        // Clear country select
                        $(coutry_select).val('').trigger('change');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddHotelItemModal() {
                    var modal = $('#m_modal_add_handbook_item_hotel').get()[0];
                    var form = $('#add_hotel_handbook_item_form').get()[0];
                    var hotel_name_input = $('#hotel_name_for_hotel_input').get()[0];
                    var hotel_category_select = $('#hotel_category_for_hotel_select').get()[0];
                    var city_select = $('#hotel_city_for_hotel_select').get()[0];
                    var coutry_select = $('#country_for_hotel_select').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            hotel_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create hotel from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    // Init hotel category select
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/requests/get-hotel-category',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            search: '',
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            for (var i in data) {
                                if ($(hotel_category_select).find('option[value="' + data[i]['id'] + '"]').length == 0){
                                    $(hotel_category_select).append('<option value="' + data[i]['id'] + '">' + data[i]['title'] + '</option>');
                                }
                            }
                            $(hotel_category_select).selectpicker('refresh');
                        }
                    });
                    
                    // Init city select2 for hotel create
                    $(city_select).select2({
                        ajax: {
                            url: baseUrl + '/ita/' + tenantId + '/requests/get-cityes-options',
                            method: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                    page: params.page || 1,
                                    type: 'public',
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                };
                                return query;
                            },
                            delay: 1000,
                            processResults: function (data,params) {
                                var array = [];
                                params.page = params.page || 1;
                                for (var i in data.results) {
                                    array.push({id: data.results[i]['id'], text: data.results[i]['title']});
                                }
                                return {
                                    results: array,
                                    pagination: {
                                        more: (params.page * 10) < data.total_count
                                    }
                                };
                            }
                        },
                        templateSelection: function (data) {
                            if (data.id === '') {
                                return GetTranslation("hbi_form_hotel_city_select_placeholder", "Select hotel city");
                            }
                            return data.text;
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        },
                        templateResult: function (perm) {
                            return perm.text;;
                        },
                        language: tenant_lang,
                        allowClear: false,
                        minimumInputLength: 2,
                        width: '100%',
                        dropdownParent: $(city_select).closest('div'),
                        placeholder: GetTranslation("hbi_form_hotel_city_select_placeholder", "Select hotel city")
                    });
                    
                    // Init country select2 for city create
                    $(coutry_select).select2({
                        ajax: {
                            url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                            method: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                    type: 'public',
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                };
                                return query;
                            },
                            delay: 1000,
                            processResults: function (data)
                            {
                                var array = [];
                                for (var i in data){
                                    array.push({id: data[i]['id'], text: data[i]['title']});
                                }
                                return {
                                    results: array
                                };
                            }
                        },
                        minimumInputLength: 2,
                        allowClear: false,
                        width: '100%',
                        dropdownParent: $(coutry_select).closest('div'),
                        placeholder: GetTranslation("hbi_form_country_select_placeholder", "Select country")
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear hotel name input
                        $(hotel_name_input).val('');
                        
                        // Clear hotel category select
                        $(hotel_category_select).val('');
                        $(hotel_category_select).selectpicker('refresh');
                        
                        // Clear hotel city select
                        $(city_select).val('').trigger('change');
                        
                        // Clear country select
                        $(coutry_select).val('').trigger('change');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddSourceItemModal() {
                    var modal = $('#m_modal_add_handbook_item_source').get()[0];
                    var form = $('#add_source_handbook_item_form').get()[0];
                    var source_name_input = $('#source_name_for_source_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            source_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create source from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear source name input
                        $(source_name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddRemindersOrdersItemModal() {
                    var form = $('#add_reminders_orders_handbook_item_form').get()[0];
                    var modal = $('#m_modal_add_handbook_item_reminders_orders').get()[0];
                    var name_input = $('#reminders_orders_name_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            reminders_orders_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create reminaders order from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear name input
                        $(name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddRemindersRequestsItemModal() {
                    var form = $('#add_reminders_requests_handbook_item_form').get()[0];
                    var modal = $('#m_modal_add_handbook_item_reminders_requests').get()[0];
                    var name_input = $('#reminders_requests_name_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            reminders_requests_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create reminders request from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear name input
                        $(name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddCompanyTypeItemModal() {
                    var form = $('#add_company_type_handbook_item_form').get()[0];
                    var modal = $('#m_modal_add_handbook_item_company_type').get()[0];
                    var name_input = $('#company_type_name_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            company_type_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create company type from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear name input
                        $(name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddActivityTypeItemModal() {
                    var form = $('#add_activity_type_handbook_item_form').get()[0];
                    var modal = $('#m_modal_add_handbook_item_activity_type').get()[0];
                    var name_input = $('#activity_type_name_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            activity_type_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create activity type from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear name input
                        $(name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
                
                function initAddCompanyStatusItemModal() {
                    var form = $('#add_company_status_handbook_item_form').get()[0];
                    var modal = $('#m_modal_add_handbook_item_company_status').get()[0];
                    var name_input = $('#company_status_name_input').get()[0];
                    
                    // Add validation rules to form
                    var form_validator = $(form).validate({
                        rules: {
                            company_status_name: {
                                minlength: 2
                            }
                        }
                    });

                    // Proccess submit event
                    $(form).submit(function(event){
                        event.preventDefault();
                        if($(form).valid()){
                            var formDataArray = $(form).serializeArray();
                            var formDataObject = {};
                            formDataArray.forEach(function(value){
                                formDataObject[value.name] = value.value;
                            });
                            alert('Create company status from: ' + JSON.stringify(formDataObject));
                        }
                    });
                    
                    $(modal).on('shown.bs.modal', function(){
                        // Hide preloader, show form
                        $(modal).find('.modal-body>.preloader').hide();
                        $(modal).find('.modal-body>form').fadeIn();
                    });
                    $(modal).on('hidden.bs.modal', function(){
                        // Clear form errors
                        form_validator.resetForm();
                        
                        // Clear name input
                        $(name_input).val('');
                        
                        // Hide form, show preloader
                        $(modal).find('.modal-body>form').hide();
                        $(modal).find('.modal-body>.preloader').show();
                    });
                }
            }
            
            function initCityFilters(){
                // Init country select2 for city filter
                var coutry_select = $('#data_block_country_filter_input').get()[0];
                $(coutry_select).select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            };
                        }
                    },
                    minimumInputLength: 2,
                    allowClear: true,
                    width: '100%',
                    dropdownParent: $(coutry_select).closest('.m-portlet__body'),
                    placeholder: GetTranslation("hbi_filter_by_country_placeholder", "Select country")
                });
                
                // Init city filter by country
                $(coutry_select).on('change', function(){
                    alert('Filter by country=' + $(this).val());            // TODO: delete this string after filter realize
                    DatatablesMaster.datatables.datatable_city.reload();    // TODO: realize reload datatable with filter data
                });

                // Init city filter by cityname
                $('#data_block_city_filter_input').on('keyup', function(){
                    var input = this;
                    var timeout_id = $(input).data('filter-city-by-name-timeout-id');
                    if(timeout_id !== undefined && timeout_id !== null){
                        clearTimeout(timeout_id);
                    }
                    timeout_id = setTimeout(function(){
                        alert('Filter by country=' + $(input).val());           // TODO: delete this string after filter realize
                        DatatablesMaster.datatables.datatable_city.reload();    // TODO: realize reload datatable with filter data
                    }, 1000);
                    $(input).data('filter-city-by-name-timeout-id', timeout_id);
                });
            }
            
            function initHotelFilters(){
                // Init hotel filter by category
                $('#data_block_hotel_name_filter_input').on('keyup', function(){
                    var input = this;
                    var timeout_id = $(input).data('filter-hotel-by-category-timeout-id');
                    if(timeout_id !== undefined && timeout_id !== null){
                        clearTimeout(timeout_id);
                    }
                    timeout_id = setTimeout(function(){
                        alert('Filter by category=' + $(input).val());           // TODO: delete this string after filter realize
                        DatatablesMaster.datatables.datatable_hotel.reload();    // TODO: realize reload datatable with filter data
                    }, 1000);
                    $(input).data('filter-hotel-by-category-timeout-id', timeout_id);
                });
                
                // Init hotel category select2 for hotel filter
                var hotel_category_select = $('#data_block_hotel_category_filter_select').get()[0];
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-hotel-category',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        search: '',
                        type: 'public',
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        for (var i in data) {
                            if ($(hotel_category_select).find('option[value="' + data[i]['id'] + '"]').length == 0){
                                $(hotel_category_select).append('<option value="' + data[i]['id'] + '">' + data[i]['title'] + '</option>');
                            }
                        }
                        $('#data_block_hotel_category_filter_select_preloader').hide();
                        $(hotel_category_select).selectpicker('refresh');
                    }
                });
                
                // Init hotel filter by hotel catgory
                $(hotel_category_select).on('change', function(){
                    alert('Filter by hotel category=' + $(this).val());     // TODO: delete this string after filter realize
                    DatatablesMaster.datatables.datatable_hotel.reload();   // TODO: realize reload datatable with filter data
                });
                
                // Init city select2 for hotel filter
                var city_select = $('#data_block_hotel_filter_by_city_select').get()[0];
                $(city_select).select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/requests/get-cityes-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                page: params.page || 1,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data,params) {
                            var array = [];
                            params.page = params.page || 1;
                            for (var i in data.results) {
                                array.push({id: data.results[i]['id'], text: data.results[i]['title']});
                            }
                            return {
                                results: array,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        }
                    },
                    templateSelection: function (data) {
                        if (data.id === '') {
                            return GetTranslation("hbi_filter_hotel_by_city_placeholder", "Select city");
                        }
                        return data.text;
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    templateResult: function (perm) {
                        return perm.text;;
                    },
                    language: tenant_lang,
                    allowClear: true,
                    minimumInputLength: 2,
                    width: '100%',
                    dropdownParent: $(city_select).closest('.m-portlet__body'),
                    placeholder: GetTranslation("hbi_filter_hotel_by_city_placeholder", "Select city")
                });
                
                // Init hotel filter by city
                $(city_select).on('change', function(){
                    alert('Filter by city=' + $(this).val());               // TODO: delete this string after filter realize
                    DatatablesMaster.datatables.datatable_hotel.reload();   // TODO: realize reload datatable with filter data
                });
                
                // Init country select2 for hotel filter
                var coutry_select = $('#data_block_hotel_filter_by_country_select').get()[0];
                $(coutry_select).select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            };
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data)
                        {
                            var array = [];
                            for (var i in data){
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            };
                        }
                    },
                    minimumInputLength: 2,
                    allowClear: true,
                    width: '100%',
                    dropdownParent: $(coutry_select).closest('.m-portlet__body'),
                    placeholder: GetTranslation("hbi_filter_by_country_placeholder", "Select country")
                });
                
                // Init city filter by country
                $(coutry_select).on('change', function(){
                    alert('Filter by country=' + $(this).val());            // TODO: delete this string after filter realize
                    DatatablesMaster.datatables.datatable_hotel.reload();   // TODO: realize reload datatable with filter data
                });
            }
        }
    });
})(jQuery);