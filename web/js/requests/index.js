var requests_datatable = null;

(function($){
    $(document).ready(function(){
        initRequestsDataTable();
        
        function initRequestsDataTable(){
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    // Имя
                    field: 'full_name',
                    title: GetTranslation('rc_datatable_col_title_3', ''),
                    sortable: false,
                    template: function(data) {
                        return '<a href="'+baseUrl + '/ita/' + tenantId + '/requests/view?id='+data.id+'">'+data.full_name+'</a>';
                    }
                },
                {
                    // Статус
                    field: 'status',
                    title: GetTranslation('rc_datatable_col_title_11', ''),
                    sortable: false,
                    template: function(data) {
                        return data.request_status_id ? '<span class="m-badge m-badge--'+data.status_color+' m-badge--dot"></span><span class="m--font-bold m--font-'+data.status_color+'"> '+(data.status_name ? data.status_name : '-')+'</span>' : '';
                    }
                },
                {
                    // Направления
                    field: 'countries',
                    title: GetTranslation('rc_datatable_col_title_4', ''),
                    sortable: false,
                    template: function (data) {
                        return data.countries ? data.countries : '-';
                    }
                },
                {
                    // Даты
                    field: 'dates',
                    title: GetTranslation('rc_datatable_col_title_5', ''),
                    sortable: false,
                    template: function(data) {
                        var output;
                        if((data.dates !== undefined) && (data.dates !== null)){
                            output = '<div>'+data.dates+'</div>';
                            if((data.date_interval !== undefined) && (data.date_interval !== null) && (data.date_interval < 10)){
                                output = '<div class="m--font-' + (data.date_interval <= 3 ? 'danger' : 'warning') + '">' + data.dates + '</div>';
                            }
                        } else {
                            output = '-';
                        }
                        return output;
                    }
                },
//                {
//                    // Длительность
//                    field: 'period',
//                    title: GetTranslation('rc_datatable_col_title_6', ''),
//                    sortable: false
//                },
//                {
//                    // Звездность
//                    field: 'hotels',
//                    title: GetTranslation('rc_datatable_col_title_7', ''),
//                    sortable: false,
//                    template: function (data) {
//                        return data.hotels ? data.hotels : '-';
//                    }
//                },
//                {
//                    // Туристы
//                    field: 'tourists',
//                    title: GetTranslation('rc_datatable_col_title_8', ''),
//                    sortable: false,
//                    template: function(data) {
//                        var output;
//
//                        if (data.tourists_adult_count > 0)
//                            output = data.tourists_adult_count + '&nbsp;AD';
//
//                        if (data.tourists_children_count > 0)
//                            output += ' + ' + data.tourists_children_count+'&nbsp;CH';
//
//                        if (data.tourists_children_age.length > 0)
//                            output += ' ('+data.tourists_children_age+')';
//
//                        return output;
//                    }
//                },
                {
                    // Бюджет
                    field: 'budget',
                    title: GetTranslation('rc_datatable_col_title_9', ''),
                    sortable: false,
                    template: function(data) {
                        return data.budget_from && data.budget_to ? GetTranslation('request_budget_from', '') + '&nbsp;'+data.budget_from+'&nbsp;'+data.budget_currency+' '+GetTranslation('request_budget_to', '')+'&nbsp;'+data.budget_to+'&nbsp;' + data.budget_currency : '-';
                    }
                },
                {
                    field: 'responsible',
                    title: GetTranslation('rc_datatable_col_title_10', ''),
                    sortable: false,
                    template: function(data) {
                        return '<div class="m-card-user m-card-user--sm"><div class="m-card-user__pic"><img src="'+baseUrl+'/'+data.responsible_photo+'" class="m--img-rounded m--marginless m--img-centered" alt="'+data.responsible+'" title="'+data.responsible+'"></div></div>';
                    }
                }
            ];
            
            requests_datatable = $('#requests_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/requests/get-data',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                    // filter_responsible: userId
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(requests_datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(requests_datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = requests_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_requests_buttons').css({display : "flex"});
                }else{
                    $('#bulk_requests_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-requests').on('click', function(){
                var selected_records = requests_datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('rc_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('rc_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/requests/bulk-delete',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                requests_datatable.reload();
                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
    });
})(jQuery);