(function ($) {
    $(document).ready(function(){
        
        var hotels_data = [];
        var main_country = undefined;
        var cur_service = undefined;
        
        initServicesPopupWidgets();
        initContactsChangePopupWidgets();
        initContactsEditPopupWidgets();
        initInfoTableWidgets();
        initDeleteForm();
        initSeviceSelectCheckboxes();
        initCPModalsWidgets();
        initPageWidgets();

        $('body').find('form').attr('autocomplete', 'off');
        
        function initDeleteForm() {
            var form = $('#request-view-delete-form').get()[0];
            $(form).on('submit', function (event) {
                if (!$(form).data('confirmed')) {
                    event.preventDefault();
                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        text: GetTranslation('request_delete_question', 'Delete this request?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value === true) {
                            $(form).data('confirmed', true);
                            $(form).submit();
                        } else {
                            return false;
                        }
                    });
                }
            });
        }
        
        function initSeviceSelectCheckboxes(){
            var checkboxes = $('#request-services-list-partlet .serv_cbx_tag').get();
            var cp_change_buttons = $('#change_cp_method_modal .cm-item').get();
            var add_cp_btn = $('#add_commercial_proposal_btn').get();
            // Checkbox change event
            $(checkboxes).each(function(){
                $(this).on('change', function(){
                    var checked_ids = getCheckedIds();
                    if(checked_ids.length>0){
                        $(add_cp_btn).stop().fadeIn(300);
                    }else{
                        $(add_cp_btn).stop().fadeOut(200);
                    }
                });
            });
            // Check all checkbox
            $('#check_all_serv_cbx_tag').on('change', function(){
                var check_all_box = this;
                $(checkboxes).each(function(){
                    this.checked = check_all_box.checked;
                    $(this).trigger('change');
                });
            });
            // Add CP button click event
            $(add_cp_btn).on('click', function(event){
                event.preventDefault();
                var checked_ids = getCheckedIds();
                if(checked_ids.length>0){
                    $('#change_cp_method_modal').modal('show');
                }else{
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error(GetTranslation('request_cpp_no_changed_services_error', 'You dont change any services!'));
                }
            });

            // Click on CP send variant event
            $(cp_change_buttons).each(function(){
                var variant_button = this;
                $(variant_button).on('click', function(){
                    var button = this;
                    var checked_ids = getCheckedIds();
                    var method = $(button).data('cp-method');
                    $('#change_cp_method_modal').modal('hide');
                    setTimeout(function(){
                        $('#cp_method_' + method + '_modal').modal('show');
                    }, 370);
                });
            });

            // Collect ids from changed checkboxes
            function getCheckedIds(){
                var ids = [];
                $(checkboxes).each(function(){
                    if(this.checked){
                        ids.push(parseInt($(this).val()));
                    }
                });
                return ids;
            }
        }
        
        function initCPModalsWidgets(){
            // Select of sms contact phone
            var phone_select = $('#cp_for_who_select').get()[0];
            var new_phone_input = $('#cp_for_who_input').get()[0];
            $(phone_select).on('change', function(){
                var select_val = $(phone_select).val();
                if(select_val === 'CREATE_NEW_CONTACT'){
                    phone_select.disabled = true;
                    $(phone_select).val('').selectpicker('refresh').selectpicker('hide');
                    $(new_phone_input).show();
                }
            });

            // Select of email contact phone
            var phone_select = $('#cp_email_for_who_select').get()[0];
            var new_phone_input = $('#cp_email_for_who_input').get()[0];
            $(phone_select).on('change', function(){
                var select_val = $(phone_select).val();
                if(select_val === 'CREATE_NEW_CONTACT'){
                    phone_select.disabled = true;
                    $(phone_select).val('').selectpicker('refresh').selectpicker('hide');
                    $(new_phone_input).show();
                }
            });

            // Sms boby textarea with sms-counter
            autosize($('#cp_sms_body_textarea'));
            $('#cp_sms_body_textarea').countSms('#sms-counter');

            // Copy link button
            var copy_btn = $('#copy_link_button').get()[0];
            var link_input = $('#copy_link_input').get()[0];
            var copy_bufer = $('#copy_bufer').get()[0];
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $(copy_btn).on('click', function(){
                var value = $(link_input).val();
                if(value.length>0){
                    $(copy_bufer).text(value);
                    copy_bufer.select();
                    try {
                        var successful = document.execCommand('copy');
                        if(successful === true){
                            toastr.success(GetTranslation('request_cp_link_modal_bufer_copy_success', 'Link was copy in buffer'));
                            $('#cp_method_link_modal').modal('hide');
                        }else{
                            toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                        }
                    } catch (err) {
                        console.log(err);
                        toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_system_error', 'Copy system error!'));
                    }
                }else{
                    toastr.error(GetTranslation('request_cp_link_modal_bufer_copy_no_data_error', 'No data for copy'));
                }
            });
            // Email body Summernote init
            $('#cp_method_email_modal').on('shown.bs.modal', function(){
                if($('#cp_email_body_editor').data('was-inited') !== true){
                    // init only when modal was shown (this is for summernote editable area bug slove)
                    $('#cp_email_body_editor').summernote({
                        height: 300,
                        toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']]
                        ]
                    });
                    $('#cp_email_body_editor').data('was-inited', true);
                }
            });
        }
        
        function initPageWidgets(){
            $('#request_change_contact').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/ajax/search-contacts',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            _csrf: $('meta[name="csrf-token"]').attr('content'),
                            search: params.term
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data)
                    {
                        var array = [];
                        for (var i in data){
                            array.push({id: data[i]['id'], text: data[i]['first_name']});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                placeholder: GetTranslation('request_select_new_contact'),
                width: '100%',
                dropdownParent: $('#request_change_contact').parents('.modal-body')
            });
        }
        
        function initNextBtn(modal) {
            $('#btn-next').off('click').on('click', function () {
                $(modal).find('.select-additional').each(function () {
                    if ($(this).prop('checked')) {
                        var service_id = $(this).val();
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: service_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                    var portlet = new mPortlet(this);
                                });
                                add_count++;
                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal);
                            }
                        });
                    }
                });
                $(this).hide();
                $('#service-additional-services-portlet-block').hide();
                $('#btn-save').show();
                $('#add-srv-btn').empty();
                // Get service data
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                    data: {
                        service_id: $(this).attr('data-service-id'),
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    success: function (response) {
                        $('#add-srv-btn').append(renderAddServicesList(response['links']));
                        $(modal).find('.add-service').on('click', function () {
                            var service_id = $(this).attr('data-service-id');
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                data: {
                                    service_id: service_id,
                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                },
                                method: 'POST',
                                success: function (response) {
                                    $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                    $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                        var portlet = new mPortlet(this);
                                        portlet.expand();
                                    });
                                    add_count++;
                                    initDynamicInputs(modal);
                                    initCopyBtn(modal);
                                    initNextBtn(modal);
                                }
                            });
                        });
                    }
                });
            });
        }

        function initCopyBtn(modal){
            $('#m_modal_header_requests .copy-service').off('click').on('click', function () {
                var element = $(this).closest('.additional-filed');

                var service_id = element.attr('data-service-id');
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                    data: {
                        service_id: service_id,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    success: function (response) {
                        $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                        $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                            var portlet = new mPortlet(this);
                            portlet.expand();
                        });
                        add_count++;
                        initDynamicInputs(modal);
                        initCopyBtn(modal);
                        initNextBtn(modal);
                    }
                });
                //
                // element.attr('data-id',add_count + 1);
                // element.find('input[name$="[link_id]"]').remove();
                // element.find('[name*="ServiceAdditionals["]').each(function () {
                //     var r = /\[|\]/;
                //     var parts = $(this).attr('name').split(r);
                //     parts[1] = add_count + 1;
                //     var name = parts[0];
                //     for(var i = 1;i<parts.length;i++)
                //         if(parts[i]!='')
                //             name += '['+parts[i]+']';
                //     console.log(parts);
                //    $(this).attr('name',name);
                // });
                // var id = add_count+ 1;
                //     $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                //     $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                //          var portlet = new mPortlet(this);
                //          portlet.expand();
                //      });
                // add_count++;
                // initDynamicInputs();
                // $('#service-additional-services-details').append(element);
            });
        }

        function initServicesPopupWidgets() {
            var modal = $('#m_modal_service_details').get()[0];
            var custom_portlet_title = $('#service-custom-details-portlet-title').get()[0];
            var additional_services_portlet = $('#service-additional-services-portlet').get()[0];
            var default_inputs_block = $('#service-default-details-block').get()[0];
            var custom_inputs_block = $('#service-custom-details-block').get()[0];
            var additional_services_block = $('#service-additional-services-portlet-block').get()[0];
            $(modal).on('show.bs.modal', function (event) {
                $(modal).find('#btn-duplicate').hide();
                if (event.relatedTarget !== undefined) {
                    clearDefaultPortlet();
                    clearCustomPortlet();
                    clearAdditionalServicesPortlet();
                    clearBaseAdditionalServicesPortlet();
                    var button = $(event.relatedTarget);

                    if (button.data('serv-id') !== undefined) {
                        var service_id = parseInt(button.data('serv-id'));
                        $('#create-label').show();
                        $('#update-label').hide();
                        $('#btn-back').show();

                        // Get service data
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                            data: {
                                service_id: service_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                var service = response;
                                cur_service = service;
                                var default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def);
                                var custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields);

                                var additional_services = response.links;
                                var additional_services_html = getAdditionalServicesHtmlTemplate(service.id, additional_services);
                                $(custom_portlet_title).text(service.name);
                                $(default_inputs_block).html(default_inputs_html);
                                $(custom_inputs_block).html(custom_inputs_html);

                                // var inputs = $('#service-default-details-block').find('select.select2-country');
                                // if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //     main_country = inputs.find('option:selected').clone();
                                // else {
                                //     inputs = $('#service-custom-details-block').find('select.select2-country');
                                //     if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //         main_country = inputs.find('option:selected').clone();
                                // }

                                // Additional services partlet status
                                if (additional_services_html.length > 0) {
                                    $(additional_services_block).html(additional_services_html).show();
                                    $(additional_services_portlet).show();
                                    $('#btn-next').attr('data-service-id', service_id);
                                } else {
                                    $(additional_services_portlet).hide();
                                    $('#btn-next').hide();
                                    $('#btn-save').show().attr('data-type', 'create');
                                }

                                if(service.multiservices) {
                                    var btn =  $(modal).find('#btn-duplicate');
                                    btn.show();
                                    btn.text(service.multiservices_name);
                                    btn.off('click').on('click',function () {
                                        $('#service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                        add_count ++;
                                        initDynamicInputs(modal);
                                        initCopyBtn(modal);
                                        initNextBtn(modal)
                                    });
                                }
                                else {
                                    $(modal).find('#btn-duplicate').hide();
                                }

                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal)
                            },
                            error:function (response) {
                                cur_service = undefined;
                            }
                        });
                    }
                    else if (button.data('id') !== undefined) {
                        var link_id = parseInt(button.data('id'));
                        $('#create-label').hide();
                        $('#update-label').show();
                        $('#btn-back').hide();
                        $('#btn-next').hide();
                        $('#btn-save').show().attr('data-type', 'update');
                        $(additional_services_block).hide();
                        $('#service-additional-services-details').html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>')
                        // Get service data
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-link-id',
                            data: {
                                link_id: link_id,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            success: function (response) {
                                var service = response;
                                cur_service = service;

                                var default_inputs_html = getInputsHtmlTemplate(service.id, service.fields_def, undefined, link_id);
                                var custom_inputs_html = getInputsHtmlTemplate(service.id, service.fields, undefined, link_id);
                                var additional_services = response.links;

                                $(custom_portlet_title).text(service.name);
                                $(default_inputs_block).html(default_inputs_html);
                                $(custom_inputs_block).html(custom_inputs_html);

                                if(service.multiservices) {
                                    var btn =  $(modal).find('#btn-duplicate');
                                    btn.show();
                                    btn.text(service.multiservices_name);
                                    btn.off('click').on('click',function () {
                                        $('#service-duplicates').append(renderBaseAdditionalService(cur_service,add_count));
                                        add_count ++;
                                        initDynamicInputs(modal);
                                        initCopyBtn(modal);
                                        initNextBtn(modal)
                                    });
                                }
                                else {
                                    $(modal).find('#btn-duplicate').hide();
                                }

                                $('#service-additional-services-details').empty();
                                // var inputs = $('#service-default-details-block').find('select.select2-country');
                                // if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //     main_country = inputs.find('option:selected').clone();
                                // else {
                                //     inputs = $('#service-custom-details-block').find('select.select2-country');
                                //     if (inputs.length > 0 && inputs.find('option:selected').length > 0)
                                //         main_country = inputs.find('option:selected').clone();
                                // }
                                for (key in additional_services) {
                                    if(!additional_services[key].type) {
                                        $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                        $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                            var portlet = new mPortlet(this);
                                            portlet.collapse();
                                        });
                                    } else {
                                        $('#service-duplicates').append(renderBaseAdditionalService(additional_services[key],add_count,key));
                                        add_count ++;
                                    }

                                    add_count++;
                                }
                                $('#add-srv-btn').empty();
                                $.ajax({
                                    url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                    data: {
                                        service_id: service.id,
                                        _csrf: $('meta[name="csrf-token"]').attr('content')
                                    },
                                    method: 'POST',
                                    success: function (response) {
                                        $('#add-srv-btn').append(renderAddServicesList(response['links']));
                                        if (response['links'] == undefined || response['links'].length == 0)
                                            $('#service-additional-services-portlet').hide();
                                        else
                                            $('#service-additional-services-portlet').show();
                                            $(modal).find('.add-service').on('click', function () {
                                            var service_id = $(this).attr('data-service-id');
                                            $.ajax({
                                                url: baseUrl + '/ita/' + tenantId + '/services/get-service-by-id',
                                                data: {
                                                    service_id: service_id,
                                                    _csrf: $('meta[name="csrf-token"]').attr('content')
                                                },
                                                method: 'POST',
                                                success: function (response) {
                                                    $('#service-additional-services-details').append(renderAdditional(response,undefined,add_count));
                                                    $('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]').each(function(){
                                                        var portlet = new mPortlet(this);
                                                    });
                                                    setDefault($('#service-additional-services-details').find('div[data-id="' + add_count + 1 + '"]'));
                                                    add_count++;
                                                    initDynamicInputs(modal);
                                                    initCopyBtn(modal);
                                                    initNextBtn(modal);
                                                }
                                            });
                                        });
                                    }
                                });
                                initDynamicInputs(modal);
                                initCopyBtn(modal);
                                initNextBtn(modal)
                            },
                            error:function (response) {
                                cur_service = undefined;
                            }
                        });
                    }
                }
            });



            function clearDefaultPortlet() {
                $(default_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
            }

            function clearCustomPortlet() {
                $(custom_inputs_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
                $(custom_portlet_title).text('');
            }

            function clearBaseAdditionalServicesPortlet() {
                $('#service-duplicates').empty();
            }

            function clearAdditionalServicesPortlet() {
                add_count = 0;
                $('#add-srv-btn').empty();
                $('#service-additional-services-details').empty();
                $('#btn-next').show();
                $('#btn-save').hide().attr('data-type', 'create');
                $(additional_services_block).show();
                $(additional_services_block).html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');
            }
        }

        function initContactsChangePopupWidgets() {
            $('#contact-change-popup').get()[0];
            $('[data-title="m-tooltip"]').each(function() {
                mApp.initTooltip($(this));
            });
        }

        function initContactsEditPopupWidgets() {
            var popup = $('#contact-edit-popup').get()[0];
            if (popup) {
                // Tags select
                $('#contacts-modal-tags-search-select-req').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                            return query;
                        },
                        delay: 1000,
                        processResults: function (data) {
                            var array = [];
                            for (var i in data) {
                                array.push({id: data[i]['id'], text: data[i]['name']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    language: tenant_lang,
                    width: '100%',
                    placeholder: GetTranslation("conctact-edit-popup-add-tag-plaseholder-req", "Add a tag"),
                    tags: true
                });
                //Repeaters
                initExtraDropdownOptionInputs();
                $('#contact-phones-repeater,' +
                    '#contact-emails-repeater,' +
                    '#contact-socials-repeater,' +
                    '#contact-messengers-repeater,' +
                    '#contact-sites-repeater').repeater({
                    initEmpty: false,
                    defaultValues: {
                        'text-input': 'foo'
                    },
                    show: function () {
                        $(this).slideDown();

                        $('#conctact-edit-popup-form')
                            .find('[valid="phoneContactNumber_add"]')
                            .intlTelInput({
                                utilsScript: "/plugins/phone-validator/build/js/utils.js",
                                autoPlaceholder: true,
                                preferredCountries: ['ua', 'ru'],
                                allowExtensions: false,
                                autoFormat: true,
                                autoHideDialCode: true,
                                customPlaceholder: null,
                                defaultCountry: "ua",
                                geoIpLookup: null,
                                nationalMode: false,
                                numberType: "MOBILE"
                            }).then(function (data) {

                            $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                                el = $(this);
                                // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                var formated = '+999 99 999 9999';
                                $(el).inputmask(formated);
                                $(el).on("countrychange", function (e, countryData) {
                                    var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                                    $(el).inputmask(formated)
                                });
                            });
                        });
                    $(this).find('input').each(function () {
                        var mask_data = $(this).data('inputmask');
                        if (mask_data) {
                            $(this).inputmask();
                        }
                    });
                        //Init dropdown option
                        initExtraDropdownOptionInputs(this);
                    },
                    hide: function (deleteElement) {
                        if (confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    }

                });
                function initExtraDropdownOptionInputs(added_row) {
                    if (added_row) {
                        //Init default dropdown value
                        var group = $(added_row).find('.extra-dropdown-option')[0];
                        var hidden_input_selector = $(group).data('for-input-selector');
                        var hidden_input = $(group).find(hidden_input_selector)[0];
                        var default_val = $(hidden_input).data('default-value');
                        $(hidden_input).val(default_val).removeAttr('data-default-value');
                    }
                    $('.extra-dropdown-option').each(function () {
                        var group = this;
                        var hidden_input_selector = $(group).data('for-input-selector');
                        var hidden_input = $(group).find(hidden_input_selector)[0];
                        var drop_down_btn = $(group).find('button.btn')[0];
                        $(group).find('.dropdown-menu a').each(function () {
                            var variant = this;
                            var variant_name = $(this).text();
                            var variant_val = $(this).data('value');
                            $(variant).unbind('click');
                            $(variant).on('click', function () {
                                $(hidden_input).val(variant_val);
                                $(drop_down_btn).text(variant_name);
                            });
                        });
                    });
                }

                // Phones
                var phones = $('a.phones');
                phones.each(function (i) {
                    var phone = $(this).text();
                    if (phone != null && phone.search(/(\d{2})(\d{3})(\d{3})(\d{4})/) >= 0) {
                        var result = phone.match(/(\d{2})(\d{3})(\d{3})(\d{4})/);
                        $(this).text('+' + result[1] + '(' + result[2] + ')' + result[3] + '-' + result[4]);
                    } else {
                        $(this).text((phone == null || (phone.indexOf('+') + 1) ? phone : '+' + phone));
                    }
                });
            }
        }

        function initValidate(){
            $('#conctact-edit-popup-form')
                .find('[valid="phoneContactNumber"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                $('#contact-phones-repeater').find('.validate-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated)
                    });
                });
            });


            $('#conctact-edit-popup-form')
                .find('[valid="phoneContactNumber_add"]')
                .intlTelInput({
                    utilsScript: "/plugins/phone-validator/build/js/utils.js",
                    autoPlaceholder: true,
                    preferredCountries: ['ua', 'ru'],
                    allowExtensions: false,
                    autoFormat: true,
                    autoHideDialCode: true,
                    customPlaceholder: null,
                    defaultCountry: "ua",
                    geoIpLookup: null,
                    nationalMode: false,
                    numberType: "MOBILE"
                }).then(function (data) {

                $('#contact-phones-repeater').find('.validate-add-phone').each(function(index, el) {
                    el = $(this);
                    // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    var formated = '+999 99 999 9999';
                    $(el).inputmask(formated);
                    $(el).on("countrychange", function (e, countryData) {
                        var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                        $(el).inputmask(formated);
                    });
                });
            });
        }

        $('#btn-save').off('click').on('click', function () {
            var form = $(this).closest('.modal-content').find('form');
            var formData = new FormData(form[0]);
            $(this).attr('disabled', true);
            if ($(this).attr('data-type') == 'create') {
                $('#m_modal_service_details').find('.modal-body')
                    .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/requests/add-service',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $('#btn-save').attr('disabled', false);
                        window.location.reload();
                    },
                    error: function (data) {
                        $('#btn-save').attr('disabled', false);
                    }
                });
            } else {
                $('#m_modal_service_details').find('.modal-body')
                    .html('<div class="m-loader m-loader--primary" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>');

                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/requests/update-service',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $('#btn-save').attr('disabled', false);
                        // $('#m_modal_service_details').modal('hide');
                        window.location.reload();
                    },
                    error: function (data) {
                        $('#btn-save').attr('disabled', false);
                        // window.location.reload();
                    }
                });
            }

            // console.log(form[0]);
            // console.log(form.serializeArray());
        });

        function initInfoTableWidgets() {
            $('.etitable-form .children-age.no_children_ages').slideUp(0);  // Hide on start
            $('#children_count_select').on('change', function () {
                var chidlen_count = parseInt($(this).val());
                var inputs_row = $('.etitable-form .children-age').get()[0];
                var inputs_block = $(inputs_row).find('.generated_inputs_block').get()[0];
                if (chidlen_count > 0) {
                    // Delete old inputs
                    $(inputs_block).find('.child-age-col').remove();
                    // Generate inputs
                    var inputs_html = '';
                    for (var i = 0; i < chidlen_count; i++) {
                        inputs_html += '<div class="col-4 child-age-col mb-2" >\
                                            <input type="text" name="Requests[children_age][]" class="form-control m-input age_mask" maxlength="2" value="">\
                                                                </div>';
                    }
                    // Show
                    $(inputs_block).append(inputs_html);
                    $(inputs_block).find(".age_mask").inputmask({
                        "mask": "9{1,2}"
                    });
                    $(inputs_row).stop().slideDown(400);
                } else {
                    // Close
                    $(inputs_row).stop().slideUp(400);
                    // Delete inputs
                    $(inputs_block).find('.child-age-col').slideUp(200, function () {
                        $(this).remove();
                    });
                }
            });

            $('#rc_filter_select_departure_cities').each(function () {
                var parentElement = $(this).parent();
                $('#rc_filter_select_departure_cities').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-departure-cityes-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            };
                            return query;
                        },
                        delay: 1000,
                        minimumInputLength: 0,
                        processResults: function (data) {
                            var array = [];
                            for (var i in data) {
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    width: '100%',
                    dropdownParent: parentElement,
                    placeholder: 'Select City'
                });
            });

            $('#tags_search_country').each(function () {
                var parentElement = $(this).parent();
                $('#tags_search_country').select2({
                    ajax: {
                        url: baseUrl + '/ita/' + tenantId + '/contacts/get-countries-options',
                        method: 'POST',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public',
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            };
                            return query;
                        },
                        delay: 1000,
                        minimumInputLength: 0,
                        processResults: function (data) {
                            var array = [];
                            for (var i in data) {
                                array.push({id: data[i]['id'], text: data[i]['title']});
                            }
                            return {
                                results: array
                            }
                        }
                    },
                    width: '100%',
                    dropdownParent: parentElement,
                    placeholder: 'Select Country'
                });
            });

            $('#contact-info-table-departure_date_first').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(new Date()).format(date_format),
                maxDate: moment(new Date()).add(1, 'years').format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function (e) {
                $('.daterangepicker').on('click', function (e) {
                    // It is fix for daterangepicker. It disble closing
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });

            $('#contact-info-table-departure_date_second').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(new Date()).format(date_format),
                maxDate: moment(new Date()).add(1, 'years').format(date_format),
                locale: {
                    format: date_format,
                    daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                    monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort
                },
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).click(function (e) {
                $('.daterangepicker').on('click', function (e) {
                    // It is fix for daterangepicker. It disble closing
                    // dropdown, when next and prev arrows was clicked
                    e.stopPropagation();
                });
            });

            // Responsible select
            $('#select2-responsible-ajax-search').select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-responsible-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public',
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        }
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            var full_name = '';
                            full_name += (data[i]['first_name']) ? data[i]['first_name'] + ' ' : '';
                            full_name += (data[i]['middle_name']) ? data[i]['middle_name'] + ' ' : '';
                            full_name += (data[i]['last_name']) ? data[i]['last_name'] + ' ' : '';
                            array.push({id: data[i]['id'], text: full_name.trim()});
                        }
                        return {
                            results: array
                        }
                    }
                },
                language: tenant_lang,
                width: '100%',
                placeholder: GetTranslation("rv_page_change_responsible_label", "Select responsible")
            });

            $('#select2-responsible-ajax-search').on('change', function () {
                var request_id = $('#select2-responsible-ajax-search-request-id-hint').val();
                var selected = $(this).find("option:selected").val();
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/requests/change-responsible',
                    data: {
                        request_id: request_id,
                        responsible_id: selected,
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    success: function (response) {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if (response.result === 'success') {
                            toastr.success(response.message);
                        }
                        if (response.result === 'error') {
                            toastr.error(response.message);
                        }
                    }
                });
            });

            function delete_note() {
                $('body').on('click', 'a[data-action="delete"]', function (e) {
                    var id_note = $(this).data('id_note');
                    var item = $(this).closest('.m-widget3__item');

                    swal({
                        title: GetTranslation('delete_question', 'Are you sure?'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                        cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: baseUrl + '/ita/' + tenantId + '/requests/delete-note?id_note=' + id_note,
                                method: 'GET',
                                success: function (response) {
                                    var content = {
                                        title: GetTranslation('success_message', 'Success!'),
                                        message: ''
                                    };
                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        z_index: 1031,
                                        type: "success"
                                    });
                                    item.remove();
                                },
                                error: function (response) {
                                    var content = {
                                        title: 'Error',
                                        message: response.responseText.replace(':', '')
                                    };

                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        type: "danger",
                                        z_index: 1031
                                    });
                                }
                            });
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                });
            }

            function edit_note() {
                var item, note_id;

                $('body').on('click', 'a[data-action="edit"]', function (e) {
                    item = $(this).closest('.m-widget3__item');
                    note_id = $(this).data('id_note');

                    $('.note-toolbar-wrapper').css('height', '50px');

                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/requests/get-note?id_note=' + note_id,
                        method: 'GET',
                        success: function (response) {
                            $('#edit-note-redactor').summernote('code', response.value);
                        },
                        error: function (response) {
                            var content = {
                                title: 'Error',
                                message: response.responseText.replace(':', '')
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                type: "danger",
                                z_index: 1031
                            });
                        }
                    });
                });

                $('#edit-note-save-button').click(function (e) {
                    e.preventDefault();

                    var btn = $(this);
                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    $('#request-edit-note-popup form').ajaxSubmit({
                        method: 'POST',
                        url: baseUrl + '/ita/' + tenantId + '/requests/edit-note?id_note=' + note_id,
                        success: function (response, status, xhr, $form) {
                            setTimeout(function () {
                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                                $('#request-edit-note-popup').modal('hide');

                                if (response.id) {
                                    item.find('.m-widget3__body').empty().html(response.value);

                                    var content = {
                                        title: GetTranslation('success_message', 'Success!'),
                                        message: ''
                                    };

                                    $.notify(content, {
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        z_index: 1031,
                                        type: "success"
                                    });
                                }
                            }, 1000);
                        },
                        error: function (response) {
                            var content = {
                                title: 'Error',
                                message: response.responseText.replace(':', '')
                            };

                            $.notify(content, {
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                type: "danger",
                                z_index: 1031
                            });
                        }
                    });
                });
            }

            $(document).ready(function () {
                delete_note();
                edit_note();
                initValidate();

                $('#request-add-note-popup').on('show.bs.modal', function (e) {
                    $('.note-toolbar-wrapper').css('height', '50px');
                });

                //$('#add-note-preload').hide();
                $('#add-note-redactor').summernote({
                    height: 300,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });

                //$('#add-note-preload').hide();
                $('#edit-note-redactor').summernote({
                    height: 300,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });
            });

            $('#request-services-list-partlet .remove-service').on('click', function () {
                var service_id = $(this).attr('data-id');
                var item = $(this).closest('.service-item');
                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    // text: button.data('message'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/requests/delete-service?id=' + service_id,
                            method: 'GET',
                            success: function (response) {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });
                                item.remove();
                            },
                            error: function (response) {
                                var mes = response;
                                if (response.error != undefined)
                                    mes = response.error;
                                if (response.message != undefined)
                                    mes = response.message;
                                if (response.responseText != undefined)
                                    mes = response.responseText;
                                if (response.text != undefined)
                                    mes = response.text;
                                var content = {
                                    title: 'Error',
                                    message: mes
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });

            // Description portlet init
            new mPortlet('#request-description-partlet');

            $('.description_save_block').hide();
            $('#request_create__description_textarea').on('click', function () {
                $('#request_create__description_textarea').addClass('desc_editing');
            });
            $('#request_create__description_textarea').on('focus', function () {
                $('.description_save_block').show();
            });
            $('.m-content').on('click', function (elem) {
                var e = elem.target;
                var target = document.querySelector('#decription_edit');
                var target2 = document.querySelector('#request_create__description_textarea');
                if (e !== target && e !== target2) {
                    $('.description_save_block').hide();
                }
            });

            // Description portlet init
            new mPortlet('#request-info-partlet');
        }
    });
})(jQuery);