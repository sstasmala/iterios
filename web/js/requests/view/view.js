(function($){
    // Code for all steps!!!

})(jQuery);

$('#cp_share_link').on('click', function () {
    var inputs = $('.cp_checkbox:checked');
    var services_id = [];

    inputs.each(function (i, input) {
        services_id.push($(input).val());
    });

    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/requests/commercial-proposals-on-link',
        method: 'POST',
        dataType: 'json',
        data: {
            _csrf: $('meta[name="csrf-token"]').attr('content'),
            services_id: services_id
        },
        success:function (res) {
            if (res)
                $('input#copy_link_input').val(res.link);
        },
        error: function () {
            alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
        }
    });
});

$('#save_status').on('click', function () {

    $.ajax({
        url: baseUrl + '/ita/' + tenantId + '/requests/set-status?request_id=' + $('#change-request-status input[name="request_id"]').val(),
        method: 'POST',
        dataType: 'json',
        data: {
            status: $('#change-request-status select[name="request_status"]').val(),
            _csrf: $('#change-request-status input[name="_csrf"]').val()
        },
        success:function (response) {
            // console.log(response);
            if($('#requestStatus').val() == 'canceled'){
                $('#request-change-status-popup').modal('show');
                $('#change-status-preload').hide();
                $('#request-change-status-popup .other_reason_group').hide();
            }

            $('#change-request-status').removeClass('m-dropdown--open');
            $("#change-request-status .request_status_text").text(response.status);
            $("#change-request-status .request_status_text").removeClass('btn-success btn-warning btn-danger btn-info btn-primary btn-default').addClass('btn-'+response.color);
            //text(response.color);
        }
    });

});

$('#reason_close').on('change', function(){
    if($('#reason_close').val() == 999999){
        $('#request-change-status-popup .other_reason_group').show();
    }
});


