(function ($) {

    function applyDataTable() {
        //console.log("apply");
        var filterData;
        filterData = $('#search-top-filter').serialize();

        if (!$('#search-panel #reset_search_input_button').is(':visible'))
            $('#search-panel #reset_search_input_button').show();

        var option_count = count_filters();

        if (option_count > 0) {
            $('#search-panel #filters_indicator').text(option_count + ' ' + declOfNum(option_count, ['опция', 'опции', 'опций']));
            $('#search-panel #filters_indicator').parent().addClass('show');
        } else {
            $('#search-panel #filters_indicator').parent().removeClass('show');
        }

        dess_sms_datatable.options.data.source.read.params = {
            query: {
                // generalSearch: ''
            },
            search: filterData
        };
        dess_sms_datatable.reload();

        $(document.getElementById('search-dropdown')).stop().slideUp(300, function () {
            $(this).removeClass('open');
        });
    }

    function reset() {
        //console.log("reset");
        $('#search-top-filter')[0].reset();
        $('#search-panel input[name="date_start"]').val('');
        $('#search-panel input[name="date_end"]').val('');
        $('#search-panel #select_provider').val(null).trigger('change');
        $('#search-panel #select_status').val(null).trigger('change');
        $('#search-panel #select_user').val(null).trigger('change');

        $('.filters-search .filter-item').each(function () {
            $(this).find('a').removeClass('active');
        });
        $($('.filters-search .filter-item').get([0])).find('a').addClass('active');

        applyDataTable();
    }

    function count_filters() {
        var count = 0;

        // $('.filters-search .filter-item').each(function () {
        //     if ($(this).find('a').hasClass('active'))
        //         count += 1;
        // });

        if ($('#search-panel input[name="title"]').val().length > 0)
            count += 1;

        if ($('#search-panel #select_provider').val() && $('#search-panel #select_provider').val().length > 0)
            count += 1;

        if ($('#search-panel #select_status').val() && $('#search-panel #select_status').val().length > 0)
            count += 1;

        if ($('#search-panel #m_daterangepicker_completion input[type="text"]').val().length > 0)
            count += 1;

        if ($('#search-panel #select_user').val() && $('#search-panel #select_user').val().length > 0)
            count += 1;

        return count;
    }

    function declOfNum(number, titles) {
        var cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }

    $(document).ready(function () {
        $('#select_user').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-responsible',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data) {
                    var array = [];
                    for (var i in data) {
                        array.push({id: data[i]['id'], text: data[i]['first_name']});
                    }
                    return {
                        results: array
                    };
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation('filter_top_placeholder_field_created'),
            width: '100%',
            allowClear: true,
            dropdownParent: $('#search-dropdown')
        });
        $('#select_user').val('').trigger('change');

        $('#select_provider').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-provider',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data) {
                    var array = [];
                    for (var i in data) {
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    };
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation('filter_top_placeholder_field_provider'),
            width: '100%',
            allowClear: true,
            dropdownParent: $('#search-dropdown')
        });

        $('#select_status').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-status-sms',
                method: 'POST',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data) {
                    var array = [];
                    for (var i in data) {
                        array.push({
                            id: data[i],
                            text: GetTranslation('status_' + data[i])
                        });
                    }
                    return {
                        results: array
                    };
                }
            },
            language: tenant_lang,
            placeholder: GetTranslation('filter_top_placeholder_field_status'),
            width: '100%',
            allowClear: true,
            dropdownParent: $('#search-dropdown')
        });

        $('#m_daterangepicker_completion').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            locale: {
                format: date_format,
                daysOfWeek: $.fn.datepicker.dates[tenant_lang].daysShort,
                monthNames: $.fn.datepicker.dates[tenant_lang].monthsShort,
                applyLabel: GetTranslation('bootstrap_daterangepicker_apply_btn', 'Apply'),
                cancelLabel: GetTranslation('bootstrap_daterangepicker_cancel_btn', 'Cancel'),
                fromLabel: GetTranslation('bootstrap_daterangepicker_from', 'From'),
                toLabel: GetTranslation('bootstrap_daterangepicker_to', 'To')
            }
        }, function (start, end, label) {
            $('#search-dropdown input[name="date_start"]').val(start / 1000);
            $('#search-dropdown input[name="date_end"]').val(end / 1000);
            $('#m_daterangepicker_completion .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

        $('#apply_filter').on('click', function () {
            applyDataTable();
        });

        $('.filters-search .filter-item a').on('click', function () {
            var currItem;
            $(this).addClass("active");
            currItem = $(this);
            //console.log(currItem);
            switch (currItem.attr('id')) {
                case 'my_sms':
                    $('#select_user').val(currentUserId).trigger('change');
                    break;
                default:
                    $('#select_user').val('').trigger('change');
            }

            $('.filters-search .filter-item').each(function () {
                $(this).find('a').removeClass('active');
            });
            $(this).addClass('active');

            if (!$('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #reset_search_input_button').show();

            //$('#search-panel #filters_indicator').text('1 опция');
            //$('#search-panel #filters_indicator').parent().addClass('show');

            applyDataTable();
        });

        $('input#quick-search-input').keyup(function (event) {
            if (!$('#search-panel #reset_search_input_button').is(':visible'))
                $('#search-panel #reset_search_input_button').show();

            if (event.keyCode == 13 && $('input[name="quick-search"]').val().length > 0) {
                applyDataTable();
                $(this).blur();

                if ($('input[name="quick-search"]').val().length > 0) {
                    $('#search-panel #filters_indicator').text('1 опция');
                    $('#search-panel #filters_indicator').parent().addClass('show');
                } else {
                    $('#search-panel #filters_indicator').parent().removeClass('show');
                }
            }
        });

        $('#reset_filter').on('click', function () {
            reset();

            if ($('#search-panel #reset_search_input_button').is(':visible')) {
                $('#search-panel #reset_search_input_button').hide();
                $('#search-panel #summary_indicator').parent().hide();
            }
        });

        $('#reset_search_input_button').on('click', function () {
            reset();

            if ($('#search-panel #reset_search_input_button').is(':visible')) {
                $('#search-panel #reset_search_input_button').hide();
                $('#search-panel #summary_indicator').parent().hide();
            }
        });

        $('body').on('click', 'a#show_all_link', function () {
            reset();

            if ($('#search-panel #reset_search_input_button').is(':visible')) {
                $('#search-panel #reset_search_input_button').hide();
                $('#search-panel #summary_indicator').parent().hide();
            }
        });

        dess_sms_datatable.on('m-datatable--on-reloaded', function (e) {
            dess_sms_datatable.on('m-datatable--on-layout-updated', function (e, arg) {
                $('#search-panel #summary_indicator').text(dess_sms_datatable.getTotalRows() + ' ' +
                    declOfNum(dess_sms_datatable.getTotalRows(), ['запись', 'записи', 'записей']));

                if ($('#search-panel #reset_search_input_button').is(':visible'))
                    $('#search-panel #summary_indicator').parent().show();
            });
        });
    });
})(jQuery);