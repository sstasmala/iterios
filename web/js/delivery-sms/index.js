var dess_sms_datatable = null;

(function($){
    $(document).ready(function(){
        initMessSmsDataTable();
        
        function initMessSmsDataTable(){
            // Responsive columns default sizes
            var col_sizes = {
                statistic: 280
            };
            // Small
            if(window.innerWidth >= 540){
                col_sizes.statistic = 280;
            }
            // Medium
            if(window.innerWidth >= 720){
                col_sizes.statistic = 300;
            }
            // Lage
            if(window.innerWidth >= 960){
                col_sizes.statistic = 400;
            }
            // Extra Lage
            if(window.innerWidth >= 1140){
                col_sizes.statistic = 400;
            }
            // Super Lage
            if(window.innerWidth >= 1500){
                col_sizes.statistic = 550;
            }
            var columns = [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'name',
                    title: GetTranslation('ds_sms_datatable_col_title_1', ''),
                    sortable: false,
                    template: function(data) {
                        return '<a href="'+baseUrl + '/ita/' + tenantId + '/delivery-sms/view?id='+data.id+'" class="m-link m--font-bold">'+data.name+'</a>';
                    }
                },
                {
                    field: 'created_at',
                    title: GetTranslation('ds_sms_datatable_col_title_2', ''),
                    sortable: false,
                    template: function(data) {
                        return '<span>'+ data.createdWithTimeZone +'</span>';
                    }
                },
                {
                    field: 'col_data_3',
                    title: GetTranslation('ds_sms_datatable_col_title_3', ''),
                    sortable: false,
                    template: function(data) {

                        var translate_status;

                        if (data.status) {
                            translate_status = GetTranslation('status_'+ data.status);
                        }

                        return '<span class="m-badge m-badge--success m-badge--dot"></span>\
                                <span class="m--font-bold m--font-success ml-1">'+ translate_status +'</span>';
                    }
                },
                {
                    field: 'col_data_4',
                    title: GetTranslation('ds_sms_datatable_col_title_4', ''),
                    textAlign: 'center',
                    sortable: false,
                    template: function(data) {
                        return '<span class="m--font-bold">'+ data.count_phones +'</span>';
                    }
                },
                {
                    field: 'col_data_5',
                    title: GetTranslation('ds_sms_datatable_col_title_5', ''),
                    textAlign: 'center',
                    sortable: false,
                    template: function(data) {
                        return '<span class="m--font-bold m--font-success">'+ (data.count_delivered > 0 ? data.count_delivered : 0) + '</span>';
                    }
                },
                {
                    field: 'col_data_6',
                    title: GetTranslation('ds_sms_datatable_col_title_6', ''),
                    textAlign: 'center',
                    sortable: false,
                    template: function(data) {
                        return '<span class="m--font-bold m--font-danger">'+ (data.count_not_delivered > 0 ? data.count_not_delivered : 0) + '</span>';
                    }
                },
                {
                    field: 'col_data_7',
                    title: GetTranslation('ds_sms_datatable_col_title_7', ''),
                    textAlign: 'center',
                    sortable: false,
                    template: function(data) {
                        return  '<div class="m-card-user m-card-user--sm">\
                                    <div class="m-card-user__pic">\
                                        <img src="/'+ data.photo +'" class="m--img-rounded m--marginless m--img-centered" alt="'+ data.fio + '" title="'+ data.fio + '">\
                                    </div>\
                                </div>';
                    }
                }
            ];
            
            dess_sms_datatable = $('#dess_sms_data_table').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/delivery-sms/get-index',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {

                                },
                                // search: {
                                //     filter_responsible: userId
                                // }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(dess_sms_datatable).on('m-datatable--on-init', function(e){
                initTableCheckboxes();
            });
            // Layout updated event
            $(dess_sms_datatable).on('m-datatable--on-layout-updated', function(e){
                initTableCheckboxes();
                checkBulkButtonsVisibility();
            });
            
            // Add event for checkboxes
            function initTableCheckboxes(){
                $('[data-field="id"] input[type="checkbox"]').each(function(){
                    var checkbox = this;
                    $(checkbox).off('change');
                    $(checkbox).on('change', function(){
                        checkBulkButtonsVisibility();
                    });
                });
            }
            
            // Show/hide bulk buttons
            function checkBulkButtonsVisibility(){
                var changed_records = dess_sms_datatable.getSelectedRecords();
                var changed_count = changed_records.length;
                if(changed_count>0){
                    $('#bulk_dess_sms_buttons').css({display : "flex"});
                }else{
                    $('#bulk_dess_sms_buttons').hide();
                }
            }
            
            // Bulk delete button
            $('#remove-mess-sms').on('click', function(){
                var selected_records = dess_sms_datatable.getSelectedRecords();
                var ids_to_delete = [];
                $(selected_records).each(function(){
                    var id = $(this).find('td[data-field="id"] input[type="checkbox"]').val();
                    ids_to_delete.push(id);
                });
                swal({
                    title: GetTranslation('delete_question','Are you sure?'),
                    text: GetTranslation('dess_sms_delete_message1','Are you sure to delete this') +
                            ' ' +
                            ids_to_delete.length +
                            ' ' +
                            GetTranslation('dess_sms_delete_message2','items?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation','Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel','No, cancel!'),
                    reverseButtons: true
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/delivery-sms/del-index',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                ids: ids_to_delete,
                                _csrf: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                dess_sms_datatable.reload();

                            },
                            error: function () {
                                alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                            }
                        });
                    }
                });
            });
        }
    });
})(jQuery);