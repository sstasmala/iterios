var dess_sms_recievers_datatable = null;

(function($){
    $(document).ready(function(){
        initMessSmsDataTable();
        function initMessSmsDataTable(){
            var columns = [
                {
                    field: 'col_data_1',
                    title: GetTranslation('ds_sms_report_datatable_col_title_1', ''),
                    textAlign: 'center',
                    sortable: true,
                    template: function(data) {
                        var fio = JSON.parse(data.data)[0].fio;
                         return fio;
                    }
                },
                {
                    field: 'col_data_2',
                    title: GetTranslation('ds_sms_report_datatable_col_title_2', ''),
                    textAlign: 'center',
                    sortable: true,
                    template: function(data) {
                        var phones = JSON.parse(data.data)[0].phones;
                        return phones;
                    }
                },
                {
                    field: 'col_data_3',
                    title: GetTranslation('ds_sms_report_datatable_col_title_3', ''),
                    textAlign: 'center',
                    sortable: true,
                    template: function(data) {
                        return '<span class="text-nowrap mr-1">'+JSON.parse(data.segment_copy)[0][tenant_lang_id].value+'</span>';

                    }
                },
                {
                    field: 'col_data_4',
                    title: GetTranslation('ds_sms_report_datatable_col_title_4', ''),
                    textAlign: 'center',
                    sortable: true,
                    template: function(data) {
                        var className;
                        switch (data.status){
                            case 'error':className='danger';
                            break;
                            case 'send':className='success';
                            break;
                            case 'delivered':className='primary';
                            break;
                        };

                        return '<span class="m-badge m-badge--'+className+' m-badge--dot"></span><span class="m--font-bold m--font-'+className+' ml-1">'+
                            GetTranslation(data.status +'_status', data.status) +
                            '</span>';
                    }
                }
            ];

            dess_sms_recievers_datatable = $('#sms-delivery-report-datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/delivery-sms/get-view',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                send_sms_id: send_sms_id,
                                query: {

                                },
                                // search: {
                                //     filter_responsible: userId
                                // }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {

                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
        }
    });
})(jQuery);