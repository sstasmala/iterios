(function ($) {
    $(document).ready(function () {

        daterangepickerInit();
        initSegmentsPreviewDatatable();

        function daterangepickerInit() {
            var picker = $('#delivery-sms_statistic_date_filter');
            var start = moment();
            var end = moment();

            function cb(start, end, label) {
                var title = '';
                var range = '';

                if ((end - start) < 100) {
                    title = 'Today:';
                    range = start.format('MMM D');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D');
                } else {
                    range = start.format('MMM D') + ' - ' + end.format('MMM D');
                }

                picker.find('.m-subheader__daterange-date').html(range);
                picker.find('.m-subheader__daterange-title').html(title);
            }

            picker.daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        }

        function setAlphaName() {
            $.ajax({
                url: baseUrl + '/ita/' + tenantId + '/delivery-sms/get-alpha-name',
                method: 'GET',
                data: {
                    provider_id: $('select.send-sms-provider').val(),
                },
                success: function (response) {
                    if (response.data !== null && response.data !== []) {
                        $('input[name="SendSms[alpha_name]"]').val(response.data);
                    }
                }
            });
        }

        setAlphaName();

        $('select.send-sms-provider').on('change', function () {
            setAlphaName();
        });
        
        function initSegmentsPreviewDatatable(){
            var checkboxes = $('#delivery_sms_wizard_form_step_3 .segment_checkbox').get();
            var segments_contacts_datatable = null;
            
            $(checkboxes).on('change', function(){
                actionsBeforeRefresh();
                if(segments_contacts_datatable === null){
                    initDatatable();
                } else {
                    refreshDatatable();
                }
            });
            
            function initDatatable(){
                var columns = [
                    {
                        field: 'Full_name',
                        title: GetTranslation('dess_sms_contacts_field_title_full_name', 'Full name'),
                        sortable: false,
                        template: function (row, index, datatable) {
                            if(row.last_name == null)
                                row.last_name = '';
                            if(row.first_name == null)
                                row.first_name = '';
                            if(row.middle_name == null)
                                row.middle_name = '';

                            return '<a href="'+baseUrl + '/ita/' + tenantId + '/contacts/view?id='+row.id+'">'+row.last_name + ' ' + row.first_name + ' ' + row.middle_name + '</a>';
                        }
                    },
                    {
                        field: 'Phone',
                        title: GetTranslation('dess_sms_contacts_field_title_phone', 'Phone'),
                        sortable: false,
                        template: function(row, index, datatable) {
                            var demo_phone = '+38(025)454-3534'; // TODO: CHANGE ON REAL!
                            var masked_phone = maskPhone(demo_phone);
                            return masked_phone;
                        }
                    }
                ];
                segments_contacts_datatable = $('#segments_preview_datatable').mDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: baseUrl + '/ita/' + tenantId + '/contacts/get-data',
                                method: 'GET',
                                params: {
                                    query: {
                                        // generalSearch: ''
                                    },
                                    search: {
                                        // filter_responsible: userId
                                    }
                                    // custom query params
                                },
                                map: function (raw) {

                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                }
                            }
                        },
                        pageSize: 10,
                        saveState: {
                            cookie: false,
                            webstorage: false
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    layout: {
                        theme: 'default',
                        class: '',
                        scroll: false,
                        footer: false
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 400
                    },
                    rows: {
                        afterTemplate: function (row, data, index) {
                            
                        },
                        callback: function () {
                            
                        },
                        // auto hide columns, if rows overflow. work on non locked columns
                        autoHide: false
                    },
                    columns: columns,
                    toolbar: {
                        layout: ['pagination', 'info'],
                        placement: ['bottom'],  //'top', 'bottom'
                        items: {
                            pagination: {
                                type: 'default',
                                pages: {
                                    desktop: {
                                        layout: 'default',
                                        pagesNumber: 6
                                    },
                                    tablet: {
                                        layout: 'default',
                                        pagesNumber: 3
                                    },
                                    mobile: {
                                        layout: 'compact'
                                    }
                                },
                                navigation: {
                                    prev: true,
                                    next: true,
                                    first: true,
                                    last: true
                                },

                                pageSizeSelect: [10, 20, 30, 50, 100]
                            },
                            info: true
                        }
                    },
                    translate: {
                        records: {
                            processing: GetTranslation('datatable_data_processiong','Please wait...'),
                            noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                        },
                        toolbar: {
                            pagination: {
                                items: {
                                    default: {
                                        first: GetTranslation('pagination_first','First'),
                                        prev: GetTranslation('pagination_previous','Previous'),
                                        next: GetTranslation('pagination_next','Next'),
                                        last: GetTranslation('pagination_last','Last'),
                                        more: GetTranslation('pagination_more','More pages'),
                                        input: GetTranslation('pagination_page_number','Page number'),
                                        select: GetTranslation('pagination_page_size','Select page size')
                                    },
                                    info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                                }
                            }
                        }
                    }
                });
                
                /* DATATABLE EVENTS */
                
                // Init event
                $(segments_contacts_datatable).on('m-datatable--on-init', function(e){
                    $('#segment-contacts-search-block').removeClass('d-none');
                    actionsAfterRefresh();
                });
                // Layout updated event
                $(segments_contacts_datatable).on('m-datatable--on-layout-updated', function(e){
                    actionsAfterRefresh();
                });
            }
            
            function refreshDatatable(){
                // TODO: Make datatable source filtering
                segments_contacts_datatable.reload();   // Reload datatable with new data
            }
            
            function actionsBeforeRefresh(){
                $('#segments-contacts-search-input').prop('disabled', true);
                $('#segments-contacts-search-submit').prop('disabled', true);
                $('#segments-contacts-search-submit').addClass('disabled');
                $(checkboxes).each(function(){
                    $(this).prop('disabled', true);
                    $(this).closest('.m-checkbox').addClass('m-checkbox--disabled');
                });
            }
            
            function actionsAfterRefresh(){
                $('#segments-contacts-search-input').prop('disabled', false);
                $('#segments-contacts-search-submit').prop('disabled', false);
                $('#segments-contacts-search-submit').removeClass('disabled');
                $(checkboxes).each(function(){
                    $(this).prop('disabled', false);
                    $(this).closest('.m-checkbox').removeClass('m-checkbox--disabled');
                });
            }
            
            function maskPhone(phone){
                var masked_phone = '';
                if(phone.length > 2){
                    masked_phone = phone.substr(0, (phone.length - 2)) + '**';
                    return masked_phone;
                } else {
                    console.log('Error! ' + phone + ' - incorrect phone format!');
                    return '';
                }
            }
        }
    });


})(jQuery);