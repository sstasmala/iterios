//== Class definition
var WizardDemo = function () {
    //== Base elements
    var formEl = $('#delivery_sms_form');
    var validator;
    var wizard;

    //== Private functions
    var initWizard = function () {
        //== Initialize form wizard
        wizard = new mWizard('delivery_sms_wizard', {
            startStep: 1,
            manualStepForward: true
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function (wizard) {
            if (validator.form() !== true) {
                wizard.stop();  // don't go to the next step
            } else {
                var count_phones = 0;
                var textMessage;
                textMessage = $('#dess_sms_delivery_sms_body_textarea').val();

                $('#delivery_sms_wizard_form_step_3 .segment_checkbox_item input:checked').each(function () {
                    count_phones += parseInt($(this).attr('count-phones'));
                });
                $('#delivery_sms_wizard_form_step_4 .result-deliver-phones').html(count_phones);
                $('#countPhones').val(count_phones);
                $('#delivery_sms_wizard_form_step_4 .result-deliver-text').html(textMessage);

            }
        });

        //== Change event
        wizard.on('change', function (wizard) {
            mUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                //=== Step 1
                'SendSms[name]': {
                    required: true
                },
                'SendSms[provider_id]': {
                    required: true
                },

                //=== Step 2
                'SendSms[text]': {
                    required: true
                },

                //=== Step 3
                'SendSms[segments][]': {
                    required: true
                },

                //=== Step 4
                accept: {
                    required: true
                }
            },

            //== Validation messages
            messages: {
                'account_communication[]': {
                    required: 'You must select at least one communication option'
                },
                accept: {
                    required: "You must accept the Terms and Conditions agreement!"
                }
            },

            //== Display error  
            invalidHandler: function (event, validator) {
                mUtil.scrollTop();

                swal({
                    "title": "",
                    "text": GetTranslation("dess_wizard_form_has_error_message", "There are some errors in your submission. Please correct them."),
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {
                console.log('FORM SENDING CODE HEED!!!');
            }
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-wizard-action="submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                //== See: src\js\framework\base\app.js
                mApp.progress(btn);
                //mApp.block(formEl); 

                //== See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POST',
                    success: function (responce) {
                        mApp.unprogress(btn);
                        //mApp.unblock(formEl);
                        swal({
                            "title": "",
                            "text": GetTranslation('dess_sms_wizard_form_success', "The form has been successfully submitted!"),
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                        }).then(function (result) {
                            if (result.value) {
                                // console.log(responce);
                                window.location.href = baseUrl + '/ita/' + tenantId + '/delivery-sms/view?id='+responce.model_id;
                            }
                        });
                    }
                });
            }
        });
    };

    var initWizardWidgets = function () {
        // Sms body textarea
        autosize($('#dess_sms_delivery_sms_body_textarea'));
        $('#dess_sms_delivery_sms_body_textarea').countSms('#sms-counter');

    };

    return {
        // public functions
        init: function () {
            formEl = $('#delivery_sms_form');

            initWizard();
            initValidation();
            initSubmit();
            initWizardWidgets();
        }
    };
}();

jQuery(document).ready(function () {
    WizardDemo.init();
});