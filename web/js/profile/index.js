/**
 * profile/index.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

//== Class Definition
var SnippetProfile = function() {

    var personal_details = $('#personal_details');
    var change_email = $('#change_email');
    var change_password = $('#change_password');

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div id="alert" class="form-group m-form__group m--margin-top-10">\
            <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div></div>');

        form.find('#alert').remove();
        form.removeClass('.m--margin-top-10');
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    };

    //== Private Functions
    var handlePersonalDetailsFormSubmit = function() {
        $('#personal_details_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/ita/' + tenantId + '/profile/personal-details',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.name) {
                            showErrorMsg(form, 'success', GetTranslation('personal_details_success'));

                            $('.profile_name').text(response.name);
                        }

                        if (response.error)
                            showErrorMsg(form, 'danger', GetTranslation('personal_details_error'));
                    }, 1000);
                }
            });
        });
    };

    var handleChangeEmailFormSubmit = function() {
        $('#change_email_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/ita/' + tenantId + '/profile/change-email',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.email) {
                            showErrorMsg(form, 'success', GetTranslation('profile_change_email_success'));

                            form.validate().destroy();
                            form.find('#my_email').val(response.email);

                            $('.profile_email').text(response.email);
                        }

                        if (response.error)
                            showErrorMsg(form, 'danger', response.error);
                    }, 1000);
                }
            });
        });
    };

    var handleChangePasswordFormSubmit = function() {
        $('#change_password_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 6
                    },
                    new_password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        minlength: 6,
                        equalTo: '#new_password'
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                method: 'POST',
                url: baseUrl + '/ita/' + tenantId + '/profile/change-password',
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if (response.success) {
                            showErrorMsg(form, 'success', GetTranslation('change_pass_success'));

                            form.validate().destroy();
                        }

                        if (response.error)
                            showErrorMsg(form, 'danger', response.error);
                    }, 1000);
                }
            });
        });
    };

    var handleNotificationsTags = function() {

        // Tags select
        $('.tags-select').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-tags',
                method: 'POST',
                dataType: 'json',
                    data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            width: '100%',
            placeholder: GetTranslation("profile-notifications-tags-edit_placeholder", "Add a tag"),
            tags: true
        });
    }

    var handleNotificationsStatuses = function() {

        // Tags select
        $('.status-select').select2({
            ajax: {
                url: baseUrl + '/ita/' + tenantId + '/ajax/search-statuses',
                method: 'POST',
                dataType: 'json',
                    data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                delay: 1000,
                processResults: function (data)
                {
                    var array = [];
                    for (var i in data){
                        array.push({id: data[i]['id'], text: data[i]['name']});
                    }
                    return {
                        results: array
                    }
                }
            },
            language: tenant_lang,
            width: '100%',
            placeholder: GetTranslation("profile-notifications-statuses-edit_placeholder", "Add a status"),
            tags: true
        });       
    }

    //== Public Functions
    return {
        // public functions
        init: function() {
            handlePersonalDetailsFormSubmit();
            handleChangeEmailFormSubmit();
            handleChangePasswordFormSubmit();
            handleNotificationsTags();
            handleNotificationsStatuses();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function() {
    SnippetProfile.init();

    $('body').find('form').attr('autocomplete', 'off');

    $('#personal_details')
        .find('[valid="phoneNumber"]')
        .intlTelInput({
            utilsScript: "/plugins/phone-validator/build/js/utils.js",
            autoPlaceholder: true,
            preferredCountries: ['ua', 'ru'],
            allowExtensions: false,
            autoFormat: true,
            autoHideDialCode: true,
            customPlaceholder: null,
            defaultCountry: "ua",
            geoIpLookup: null,
            nationalMode: false,
            numberType: "MOBILE"
        }).then(function (data) {
            $('.form-group').find('.validate-phone').each(function (index, el) {
                el = $(this);

                // var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                var formated = '+999 99 999 9999';
                $(el).inputmask(formated);
                $(el).on("countrychange", function (e, countryData) {
                    var formated = $(el)[0].placeholder.replace(/[0-9]/g, 9);
                    $(el).inputmask(formated);
                });
            });
    });

    $('.m-form').on('click', '#alert button.close', function (e) {
        $(this).closest('#alert').remove();
    });
});