/**
 * profile/dropzone.js
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

//== Class definition

var DropzoneDemo = function () {
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div id="alert" class="form-group m-form__group">\
            <div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" style="margin:0px">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div></div>');

        form.find('#alert').remove();

        form = form.find('.m-portlet__body');

        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    };

    //== Private functions
    var profile_picture = function () {
        // file type validation
        Dropzone.options.mDropzone = {
            url: baseUrl + '/ita/' + tenantId + '/profile/change-profile-picture',
            params: {},
            paramName: 'profile_picture', // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            // Prevents Dropzone from uploading dropped files immediately
            autoProcessQueue: false,
            init: function () {
                var submitButton = document.querySelector("#change_picture_submit");
                var pictureDropzone = this; // closure
                var upload_btn = $('#change_picture_submit');

                submitButton.addEventListener('click', function () {
                    upload_btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    $(pictureDropzone.element).find('.dz-progress').show();
                    pictureDropzone.processQueue(); // Tell Dropzone to process all queued files.
                });

                this.on('addedfile', function (file) {
                    $(this.element).find('.dz-progress').hide();
                    upload_btn.removeAttr('disabled').css('cursor', 'pointer');
                });

                this.on('maxfilesexceeded', function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('sending', function (file, xhr, formData) {
                    var csrf = upload_btn.closest('form').find('input[name="_csrf"]').val();

                    formData.append('_csrf', csrf);
                });

                this.on('success', function (file, response) {
                    if (response) {
                        // similate 2s delay
                        setTimeout(function() {
                            pictureDropzone.removeAllFiles();
                            upload_btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response.photo) {
                                showErrorMsg(upload_btn.closest('form'), 'success', GetTranslation('profile_picture_upload_success'));

                                $('img.profile_picture').each(function(){ this.src = baseUrl + '/' + response.photo; });
                            }

                            if (response.error)
                                showErrorMsg(upload_btn.closest('form'), 'danger', GetTranslation('profile_picture_upload_error'));
                        }, 500);
                    }
                });
            }
        };
    };

    // var handleProfilePictureFormSubmit = function() {
    //     $('#change_picture_submit').click(function(e) {
    //         e.preventDefault();
    //         var btn = $(this);
    //         var form = $(this).closest('form');
    //
    //         if (!form.valid()) {
    //             return;
    //         }
    //
    //         btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
    //
    //         form.ajaxSubmit({
    //             method: 'POST',
    //             url: baseUrl + '/ita/' + tenantId + '/profile/change-profile-picture',
    //             success: function(response, status, xhr, $form) {
    //                 // similate 2s delay
    //                 setTimeout(function() {
    //                     btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
    //                     showErrorMsg(form, 'success', 'Profile picture successfully changed!');
    //                 }, 2000);
    //             }
    //         });
    //     });
    // };

    return {
        // public functions
        init: function() {
            profile_picture();
            //handleProfilePictureFormSubmit();
        }
    };
}();

DropzoneDemo.init();