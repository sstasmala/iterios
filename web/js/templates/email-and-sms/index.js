var templates_datatable = null;
var template_marks_datatable = null;

(function($){
    $(document).ready(function(){
        initTemplateEditModal();
        initTemplateTableFilter();
        initTemplatesDataTable();
        initMarksDatatable();
        saveTemplate();
        deleteTemplate();

        function saveTemplate() {
            $('#m_modal_template_edit .btn-primary').click(function(e) {
                e.preventDefault();

                var btn = $(this);
                var form = $(this).closest('.modal-content').find('form');

                var template_type = $('#template_type').find('option[value="' + $('#template_type').val() + '"]').data('delivery-for');
                var template_id = $(this).data('template-id');
                var template_for = $(this).data('template-for');

                form.validate({
                    rules: {
                        'template-type': {
                            required: true
                        },
                        'template-theme': {
                            required: (template_type == 'email' ? true : false)
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'template-name': $('#m_modal_template_edit #template_name_input').val(),
                        'template-body': (template_type == 'email' ? CKEDITOR.instances['email_body_editor'].getData() : $('#sms_body_editor').val())
                    },
                    url: baseUrl + '/ita/' + tenantId + '/templates/save-email-and-sms-template' + (template_id && template_id > 0 ? '?id=' + template_id + '&template_for=' + template_for : ''),
                    success: function(response, status, xhr, $form) {
                        $('#m_modal_template_edit .btn-primary').data('template-id', null);
                        $('#m_modal_template_edit .btn-primary').data('template-for', null);

                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response) {
                                $('#m_modal_template_edit').modal('hide');

                                templates_datatable.reload();
                            }
                        }, 1000);
                    }
                });
            });
        }

        function deleteTemplate() {
            $('#templates_emails_and_sms_datatable').on('click', '.email-sms-t-delete-link', function () {
                var template_id = $(this).data('template-id');
                var template_for = $(this).data('template-for');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/templates/delete-template-by-id?id=' + template_id + '&template_for=' + template_for,
                            method: 'GET',
                            success: function () {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });

                                templates_datatable.reload();
                            },
                            error: function (response) {
                                var content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });
        }

        function initTemplateEditModal(){
            // Document body - template CK Editor
            var config = {
                customConfig: '',
                allowedContent: true,
                readOnly: true,
                disallowedContent: 'img{width,height,float}',
                extraAllowedContent: 'img[width,height,align]',
                extraPlugins: 'placeholder_mini',
                height: 450,
//                contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css', baseUrl + '/admin/plugins/ckeditor-full/document-style.css'],
                bodyClass: 'document-editor',
                format_tags: 'p;h1;h2;h3;pre',
                removeDialogTabs: 'image:advanced;link:advanced',
                stylesSet: [
                    {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                    {name: 'Cited Work', element: 'cite'},
                    {name: 'Inline Quotation', element: 'q'},
                    {
                        name: 'Special Container',
                        element: 'div',
                        styles: {
                            padding: '5px 10px',
                            background: '#eee',
                            border: '1px solid #ccc'
                        }
                    },
                    {
                        name: 'Compact table',
                        element: 'table',
                        attributes: {
                            cellpadding: '5',
                            cellspacing: '0',
                            border: '1',
                            bordercolor: '#ccc'
                        },
                        styles: {
                            'border-collapse': 'collapse'
                        }
                    },
                    {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                    {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
                ],
                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    // '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    // '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] }
                ],
                removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
            };
            CKEDITOR.replace('email_body_editor', config);
            
            // Template sms body textarea init
            autosize($('#sms_body_editor'));
            
            // Template sms body counter init
            $('#sms_body_editor').countSms('#sms-body-counter');
            
            // Template type change event
            $('#template_type').on('change', function(){
                var template_type = $(this).find('option[value="' + $(this).val() + '"]').data('delivery-for');
                if(template_type){
                    switch (template_type){
                        case 'email':
                            // Make template theme enabled
                            $('#template-theme-input').prop('disabled', false);
                            $('#template-theme-block').stop().fadeIn(400);
                            // Make sms editor disabled
                            $('#sms_body_editor').prop('disabled', true);
                            $('#template-sms-body-block').stop().fadeOut(400);
                            // Make email editor enabled
                            var config = CKEDITOR.instances['email_body_editor'].config;
                            config.readOnly = false;
                            CKEDITOR.instances['email_body_editor'].destroy();
                            CKEDITOR.replace('email_body_editor', config);
                            $('#template-email-body-block').stop().fadeIn(400);
                        break;
                        case 'sms':
                            // Make template theme disabled
                            $('#template-theme-input').prop('disabled', true);
                            $('#template-theme-block').stop().fadeOut(400);
                            // Make sms editor enabled
                            $('#sms_body_editor').prop('disabled', false);
                            $('#template-sms-body-block').stop().fadeIn(400);
                            // Make email editor disabled
                            $('#template-email-body-block').stop().fadeOut(400, function(){
                                var config = CKEDITOR.instances['email_body_editor'].config;
                                config.readOnly = true;
                                CKEDITOR.instances['email_body_editor'].destroy();
                                CKEDITOR.replace('email_body_editor', config);
                            });
                        break;
                    }
                }

                template_marks_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: $.extend(template_marks_datatable.options.data.source.read.params.search, {type: $(this).val()})
                };
                template_marks_datatable.reload();
            });
            
            // Modal shown event
            $('#m_modal_template_edit').on('shown.bs.modal', function(event){
                initModalWidgets(this, event);
            });
            $('#m_modal_template_edit').on('hidden.bs.modal', function(event){
                clearTemplateModal(this);
            });
        }
        
        function initModalWidgets(modal, event){
            var is_system = $(event.relatedTarget).data('is-system');
            var template_id = $(event.relatedTarget).data('template-id');
            var template_for = $(event.relatedTarget).data('template-for');

            if(is_system === undefined || template_id === undefined){
                //=== Start init new template modal widgets
                template_marks_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: {}
                };
                template_marks_datatable.reload();

                // Activate name input
                $("#template_name_input").addClass('focused');
                $('#template_name_input').prop('disabled', false);
                // Activate doctype input
                $('#template_type').prop('disabled', false);
                $("#template_type").selectpicker("refresh");
                // Show form
                $('#template-modal-preloader').hide();
                $('#template_form').fadeIn();
            } else {
                //=== Start init exist template modal widgets
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/templates/get-template-by-id?id=' + template_id + '&template_for=' + template_for,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        // Load name input
                        $('#template_name_input').val(data.name);
                        $('#template_name_input').prop('disabled', is_system);
                        // Load doctype input
                        $('#template_type').val(data.delivery_type_id);
                        $('#template_type').prop('disabled', is_system);
                        $("#template_type").selectpicker("refresh");
                        $('#template-theme-block').hide();
                        //Save button
                        $('#m_modal_template_edit .btn-primary').data('template-id', data.id);
                        $('#m_modal_template_edit .btn-primary').data('template-for', data.for);

                        template_marks_datatable.options.data.source.read.params = {
                            query: {
                                // generalSearch: ''
                            },
                            search: $.extend(template_marks_datatable.options.data.source.read.params.search, {type: data.delivery_type_id})
                        };
                        template_marks_datatable.reload();

                        if(data.for === 'email'){
                            // Load theme input
                            $("#template-theme-input").val(data.subject);
                            $("#template-theme-input").prop('disabled', is_system);
                            // Load email body editor
                            var config = CKEDITOR.instances['email_body_editor'].config;
                            config.readOnly = is_system;
                            CKEDITOR.instances['email_body_editor'].destroy();
                            CKEDITOR.replace('email_body_editor', config);
                            CKEDITOR.instances['email_body_editor'].setData(data.body);
                            $('#template-email-body-block').show();
                            $('#template-theme-block').show();
                        }

                        if(data.for === 'sms'){
                            // Clear sms body editor
                            $('#sms_body_editor').val(data.body);
                            $('#sms_body_editor').prop('disabled', is_system);
                            $('#template-sms-body-block').show();
                        }

                        $('#email_sms_template_default').prop('checked', data.default);

                        // Show form
                        $('#template-modal-preloader').hide();
                        $('#template_form').fadeIn();
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                    }
                });
                if(is_system){
                    // Show system temlpate notice
                    $('#system-template-notice').removeClass('d-none');
                }
            }
        }
        
        function clearTemplateModal(modal){
            // Clear id input
            $('#template_id_input').val('');
            // Clear name input
            $('#template_name_input').val('');
            $('#template_name_input').prop('disabled', true);
            // Clear doctype input
            $("#template_type").val('');
            $('#template_type').prop('disabled', true);
            $("#template_type").selectpicker("refresh");
            // Clear theme input
            $("#template-theme-input").val('');
            $("#template-theme-input").prop('disabled', true);
            $('#template-theme-block').hide();
            // Clear email body editor
            $('#template-email-body-block').hide();
            var config = CKEDITOR.instances['email_body_editor'].config;
            config.readOnly = true;
            CKEDITOR.instances['email_body_editor'].destroy();
            CKEDITOR.replace('email_body_editor', config);
            CKEDITOR.instances['email_body_editor'].setData('');
            // Clear sms body editor
            $('#template-sms-body-block').hide();
            $('#sms_body_editor').val('');
            $('#sms_body_editor').prop('disabled', false);
            // Show preloader
            $('#template_form').hide();
            $('#template-modal-preloader').show();
            // Hide system temlpate notice
            $('#system-template-notice').removeClass('d-none');

            $('#m_modal_template_edit .btn-primary').data('template-id', null);
            $('#m_modal_template_edit .btn-primary').data('template-for', null);
            $('#email_sms_template_default').prop('checked', false);
        }
        
        function initMarksDatatable(){
            var columns = [
                {
                    field: 'short_code',
                    title: GetTranslation('teasi_marks_col_title_name', 'Mark'),
                    template: function(row, index, datatable) {
                        return '<button type="button" class="btn btn-success btn-sm copy_mark_btn mr-3" title="' + GetTranslation('teasi_copy_mark_btn', 'Copy mark') + '" data-pl_id="'+row.id+'">\
                                    <i class="fa fa-copy"></i>\
                                </button>'
                                + '<span id="placeholder_text_'+row.id+'">{{'
                                + row.short_code + '}}</span>';    // Return random name from stack
                    }
                },
                {
                    field: 'description',
                    title: GetTranslation('teasi_marks_col_title_description', 'Description'),
                    template: function(row, index, datatable) {
                        return '<span data-toggle="m-popover" data-placement="top" data-content="' + (row.default == null ? '' : row.default) + '">' + row.description + '</span>';
                    }
                }
            ];

            template_marks_datatable = $('#template_marks_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/templates/get-email-and-sms-placeholders',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                        initCopyButtons(row, data, index);
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',
                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },
                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },
                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(template_marks_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(template_marks_datatable).on('m-datatable--on-layout-updated', function(e){
                
            });
            
            function initCopyButtons(row, data, index){
                $(row).find('.copy_mark_btn').each(function(){
                    var button = this;
                    $(button).on('click', function(){
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };

                        var pl_id = $(this).data('pl_id');
                        var root = document.getElementById('placeholder_text_' + pl_id);
                        var range = document.createRange();

                        range.setStart(root, 0);
                        range.setEnd(root, 1);
                        window.getSelection().empty();
                        window.getSelection().addRange(range);

                        try {
                            var successful = document.execCommand('copy');
                            window.getSelection().empty();

                            if (successful === true)
                                toastr.success(GetTranslation('teasi_mark_copy_success_message', 'Mark was copied'));
                        } catch (err) {
                            alert(err);
                        }
                    });
                });
            }

            $('#template_marks_search').on('keydown', function (e) {
                var search = $(this).val();

                if(e.which == 13) {
                    template_marks_datatable.options.data.source.read.params = {
                        query: {
                            // generalSearch: ''
                        },
                        search: $.extend(template_marks_datatable.options.data.source.read.params.search, {search_text: search})
                    };
                    template_marks_datatable.reload();
                }
            });
        }
        
        function initTemplateTableFilter(){
            function filterRequest(data) {
                templates_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: $.extend(templates_datatable.options.data.source.read.params.search, data)
                };
                templates_datatable.reload();
            }

            var filter = $('#templates-filter').get()[0];
            var portlet = $('#email-and-sms-portlet').get()[0];
            if((filter !== undefined) && (filter !== null)){
                $(filter).find('button').on('click', function(){
                    var clicked_btn = this;
                    var filter_type = $(clicked_btn).data('filter');
                    $(filter).find('button').removeClass('btn-brand').addClass('btn-secondary');
                    $(clicked_btn).removeClass('btn-secondary').addClass('btn-brand');

                    filterRequest({filter: filter_type});
                    // Show/hide content parts
//                    $(portlet).find('.reminders-index-portlet-content').removeClass('active');
//                    $(portlet).find('.reminders-index-portlet-content[data-content-part-'+filter_type+'="true"]').addClass('active');
                });
            }

            $('#m_form_status').on('change', function () {
                var type_id = $(this).val();

                filterRequest({type: type_id});
            });

            $('#input_search_template').on('keydown', function (e) {
                var search = $(this).val();

                if(e.which == 13) {
                    filterRequest({search_text: search});
                }
            });

            $('#templates_emails_and_sms_datatable').on('click', 'a#show_all_link', function () {
                templates_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: {

                    }
                };
                templates_datatable.reload();
            });
        }
        
        function initTemplatesDataTable(){
            var columns = [
                {
                    field: 'name',
                    title: GetTranslation('teasi_table_col_name', 'Name'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return '<span' + (row.type == 'system' ? ' data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('teasi_sys_template_notice', 'System template, edit disabled') + '"' : '') + '>' +
                                    '<a href="#" class="m-link name-link m--font-bold mr-1" data-is-system="' + (row.type == 'system' ? 'true' : 'false') + '" data-template-id="'+(row.id)+'" data-template-for="'+row.for+'" data-toggle="modal" data-target="#m_modal_template_edit">'
                                        + row.name +
                                    '</a>'
                                    + (row.type == 'system' ? '<i class="la la-exclamation-circle system-status-icon"></i>' : '') +
                                '</span>';
                    }
                },
                {
                    field: 'delivery_type_name',
                    title: GetTranslation('teasi_table_col_type', 'Type'),
                    sortable: true
                },
                {
                    field: 'created_at',
                    title: GetTranslation('teasi_table_col_created_at', 'Created at'),
                    sortable: true
                },
                {
                    field: 'updated_at',
                    title: GetTranslation('teasi_table_col_updated_at', 'Updated_at'),
                    sortable: true
                },
                {
                    field: 'actions',
                    title: GetTranslation('teasi_table_col_actions', 'Actions'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        if (row.type == 'system')
                            return '';

                        return '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill email-sms-t-delete-link" title="' + GetTranslation('delete', 'Delete') + '" data-template-id="'+(row.id)+'" data-template-for="'+row.for+'">\
                                    <i class="la la-trash"></i>\
                                </a>';
                    }
                }
            ];
            
            templates_datatable = $('#templates_emails_and_sms_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
//                            url: baseUrl + '/ita/' + tenantId + '/templates/get-email-and-sms-templates',
                            url: baseUrl + '/ita/' + tenantId + '/templates/get-email-and-sms-templates',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {
                                },
                                search: {
                                }
                                // custom query params
                            },
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(templates_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(templates_datatable).on('m-datatable--on-layout-updated', function(e){
                
            });
        }
    });
})(jQuery);