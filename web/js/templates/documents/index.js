

(function($){
    $(document).ready(function(){

        var template_marks_datatable = null;
        var templates_datatable = null;
        var event_set_data = false;
        var auto_save = true;
        initTemplateEditModal();
        initTemplateTableFilter();
        initTemplatesDataTable();
        initPageWidgets();
        initMarksDatatable();
        saveTemplate();
        deleteTemplate();

        function saveTemplate() {
            $('#m_modal_template_edit .btn-primary').click(function(e) {
                e.preventDefault();

                var btn = $(this);
                var form = $(this).closest('.modal-content').find('form');

                // var template_type = $('#template_type').find('option[value="' + $('#template_type').val() + '"]').data('delivery-for');
                var template_id = $(this).data('template-id');
                // var template_for = $(this).data('template-for');

                form.validate({
                    rules: {
                        'template-type': {
                            required: true
                        },
                        'supplier': {
                            required: true
                        },
                        'language': {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    $(form).find('div.form-control-feedback').css('color', 'red');
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'template-name': $('#m_modal_template_edit #template_name_input').val(),
                        'template-body': CKEDITOR.instances['document-body-template-editor'].getData()
                    },
                    url: baseUrl + '/ita/' + tenantId + '/templates/save-documents-template' + (template_id && template_id > 0 ? '?id=' + template_id : ''),
                    success: function(response, status, xhr, $form) {
                        auto_save = true;
                        $('#m_modal_template_edit .btn-primary').data('template-id', null);
                        // $('#m_modal_template_edit .btn-primary').data('template-for', null);

                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                            if (response && auto_save) {
                                $('#m_modal_template_edit').modal('hide');

                                templates_datatable.reload();
                            }
                        }, 1000);
                    }
                });
            });
        }

        function deleteTemplate() {
            $('#templates_documents_datatable').on('click', '.document-t-delete-link', function () {
                var template_id = $(this).data('template-id');
                // var template_for = $(this).data('template-for');

                swal({
                    title: GetTranslation('delete_question', 'Are you sure?'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: GetTranslation('delete_confirmation', 'Yes, delete it!'),
                    cancelButtonText: GetTranslation('delete_cancel', 'No, cancel!'),
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: baseUrl + '/ita/' + tenantId + '/templates/delete-template-by-id-documents?id=' + template_id,
                            method: 'GET',
                            success: function () {
                                var content = {
                                    title: GetTranslation('success_message', 'Success!'),
                                    message: ''
                                };
                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    z_index: 1031,
                                    type: "success"
                                });

                                templates_datatable.reload();
                            },
                            error: function (response) {
                                var content = {
                                    title: 'Error',
                                    message: response.responseText.replace(':', '')
                                };

                                $.notify(content, {
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    type: "danger",
                                    z_index: 1031
                                });
                            }
                        });
                    } else if (result.dismiss === 'cancel') {
                    }
                });
            });
        }

        function initTemplateEditModal(){
            // Document body - template CK Editor
            var config = {
                customConfig: '',
                allowedContent: true,
                disallowedContent: 'img{width,height,float}',
                extraAllowedContent: 'img[width,height,align]',
                extraPlugins: 'placeholder_edit',
                height: 700,
                contentsCss: [baseUrl + '/admin/plugins/ckeditor-full/contents.css', baseUrl + '/admin/plugins/ckeditor-full/document-style.css'],
                bodyClass: 'document-editor',
                format_tags: 'p;h1;h2;h3;pre',
                removeDialogTabs: 'image:advanced;link:advanced',
                stylesSet: [
                    {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                    {name: 'Cited Work', element: 'cite'},
                    {name: 'Inline Quotation', element: 'q'},
                    {
                        name: 'Special Container',
                        element: 'div',
                        styles: {
                            padding: '5px 10px',
                            background: '#eee',
                            border: '1px solid #ccc'
                        }
                    },
                    {
                        name: 'Compact table',
                        element: 'table',
                        attributes: {
                            cellpadding: '5',
                            cellspacing: '0',
                            border: '1',
                            bordercolor: '#ccc'
                        },
                        styles: {
                            'border-collapse': 'collapse'
                        }
                    },
                    {name: 'Borderless Table', element: 'table', styles: {'border-style': 'hidden', 'background-color': '#E6E6FA'}},
                    {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
                ],
                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    // '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    // '/',
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] }
                ],
                removeButtons: 'Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,About,ShowBlocks,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak'
            };
            CKEDITOR.replace('document-body-template-editor', config);

            $('#m_modal_template_edit').on('shown.bs.modal', function(event){
                var data = $(this);
                initModalWidgets(this, event);
                initPageWidgets();

                $('#copy_template_btn').on('click', function () {
                    switchType(data, event);
                });
            });

            $('#m_modal_template_edit').on('hide.bs.modal', function(event){
                if (!auto_save) {
                    event.preventDefault();
                    event.stopPropagation();

                    swal({
                        title: GetTranslation('document_not_save_alert_title'),
                        text: GetTranslation('document_not_save_alert_text'),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: GetTranslation('document_not_save_alert_btn'),
                        cancelButtonText: GetTranslation('document_not_save_alert_btn_cancel'),
                        reverseButtons: true
                    }).then(function (result) {
                        if (result.value) {
                            auto_save = true;
                            $('#m_modal_template_edit').modal('hide');
                            //
                            // if (open_doc_add_modal)
                            //     $('#document_add_modal').modal('show');
                        } else if (result.dismiss === 'cancel') {
                        }
                    });
                }
            });
            $('#m_modal_template_edit').on('hidden.bs.modal', function(event){
                $('#system-template-notice').addClass('d-none');    // Hide system template notice
                clearTemplateModal(this);
            });
        }

        function initModalWidgets(modal, event){
                var is_system = $(event.relatedTarget).data('is-system');
                var template_id = $(event.relatedTarget).data('template-id');

            if(is_system === undefined || template_id === undefined){
                //=== Start init new template modal widgets
                template_marks_datatable.options.data.source.read.params = {
                        query: {
                            // generalSearch: ''
                        },
                        search: {}
                    };
                    template_marks_datatable.reload();
                // Activate name input
                $("#template_name_input").addClass('focused');
                $('#template_name_input').prop('disabled', false);

                $("#template_type").val('');
                $('#template_type').prop('disabled', false);
                $("#template_type").selectpicker("refresh");

                $("#supplier").val('');
                $('#supplier').prop('disabled', false);
                // $("#supplier").selectpicker("refresh");

                $("#language").val('');
                $('#language').prop('disabled', false);
                $("#language").selectpicker("refresh");
                // Show form
                $('#template-modal-preloader').hide();
                $('#template_form').fadeIn();

                var config = CKEDITOR.instances['document-body-template-editor'].config;
                config.readOnly = is_system;
                CKEDITOR.instances['document-body-template-editor'].destroy();
                CKEDITOR.replace('document-body-template-editor', config);
                $('#system-template-notice').addClass('d-none');
                $('#m_modal_template_edit .btn-primary').prop('disabled', false);

                } else {
                    //=== Start init exist template modal widgets
                    $.ajax({
                        url: baseUrl + '/ita/' + tenantId + '/templates/get-template-doc-by-id?id=' + template_id,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            // Load name input
                            $('#template_name_input').val(data.title);
                            $('#template_name_input').prop('disabled', is_system);
                            // Load doctype input
                            $('#template_type').val(data.documents_type_id);
                            $('#template_type').prop('disabled', is_system);
                            $("#template_type").selectpicker("refresh");

                            if ($('#supplier').find("option[value='" + data.supplier_id + "']").length) {
                                $('#supplier').val(data.supplier_id).trigger('change');
                            } else {
                                // Create a DOM Option and pre-select by default
                                var newOption = new Option(data.supplier_name, data.supplier_id, true, true);
                                // Append it to the select
                                $('#supplier').append(newOption).trigger('change');
                            }


                            $('#supplier').prop('disabled', is_system);

                            $('#language').val(data.language);
                            $('#language').prop('disabled', is_system);
                            $("#language").selectpicker("refresh");
                            $('#template-theme-block').hide();
                            //Save button
                            $('#m_modal_template_edit .btn-primary').data('template-id', data.id);
                            template_marks_datatable.options.data.source.read.params = {
                                query: {
                                    // generalSearch: ''
                                },
                                search: $.extend(template_marks_datatable.options.data.source.read.params.search, {type: data.documents_type_id})
                            };
                            template_marks_datatable.reload();

                            // Load theme input
                            $("#template-theme-input").val(data.subject);
                            $("#template-theme-input").prop('disabled', is_system);

                            $('#m_modal_template_edit .btn-primary').prop('disabled', is_system);
                            // Load email body editor
                            var config = CKEDITOR.instances['document-body-template-editor'].config;
                            config.readOnly = is_system;
                            CKEDITOR.instances['document-body-template-editor'].destroy();
                            CKEDITOR.replace('document-body-template-editor', config);
                            CKEDITOR.instances['document-body-template-editor'].setData(data.body);

                            $('#language, #template_type, #template_name_input').on('change', function () {
                                auto_save = false;
                            });

                            $('#supplier').on("select2:open", function (e) { auto_save = false; });
                            $('#supplier').on("select2:close", function (e) { auto_save = false; });
                            $('#supplier').on("select2:select", function (e) { auto_save = false; });
                            $('#supplier').on("select2:unselect", function (e) { auto_save = false; });

                            CKEDITOR.instances['document-body-template-editor'].on('change', function() {
                                auto_save = false;
                            });
                            $('#document-body-template-editor').show();
                            $('#template-theme-block').show();
                            // Show form
                            $('#template-modal-preloader').hide();
                            $('#template_form').fadeIn();
                        },
                        error: function () {
                            alert(GetTranslation('cant_get_data_error','Error with data getting!'));
                        }
                    });
                    if(is_system){
                        // Show system temlpate notice
                        $('#system-template-notice').removeClass('d-none');
                    } else {
                        $('#system-template-notice').addClass('d-none');
                    }
                }
        }

        function initMarksDatatable(){
            var columns = [
                {
                    field: 'short_code',
                    title: GetTranslation('tdi_marks_col_title_name', 'Mark'),
                    template: function(row, index, datatable) {
                        return '<button type="button" class="btn btn-success btn-sm copy_mark_btn mr-3" title="' + GetTranslation('tdi_copy_mark_btn', 'Copy mark') + '" data-pl_id="'+row.id+'">'+
                                   '<i class="fa fa-copy"></i>'+
                                '</button>'
                            + '<span id="placeholder_text_'+row.id+'">{{'
                            + row.short_code + '}}</span>';    // Return random name from stack
                    }
                },
                {
                    field: 'description',
                    title: GetTranslation('tdi_marks_col_title_description', 'Description'),
                    template: function(row, index, datatable) {
                        return '<span data-toggle="m-popover" data-placement="top" data-content="' + (row.default == null ? '' : row.default) + '">' + row.description + '</span>';
                    }
                }
            ];

            template_marks_datatable = $('#template_marks_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/templates/document-placeholders',
                            method: 'GET',
                            params: {
                                query: {
                                    // generalSearch: ''
                                },
                                search: {
                                }
                                // custom query params
                            },
                            map: function (raw) {

                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                        initCopyMarkButtons(row, data, index);
                    },
                    callback: function () {

                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',
                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },
                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },
                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });

            /* DATATABLE EVENTS */

            // Init event
            $(template_marks_datatable).on('m-datatable--on-init', function(e){

            });
            // Layout updated event
            $(template_marks_datatable).on('m-datatable--on-layout-updated', function(e){

            });

            function initCopyMarkButtons(row, data, index){
                $(row).find('.copy_mark_btn').each(function(){
                    var button = this;
                    $(button).on('click', function(){
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        var pl_id = $(this).data('pl_id');
                        var root = document.getElementById('placeholder_text_' + pl_id);
                        var range = document.createRange();

                        range.setStart(root, 0);
                        range.setEnd(root, 1);
                        window.getSelection().empty();
                        window.getSelection().addRange(range);

                        try {
                            var successful = document.execCommand('copy');
                            window.getSelection().empty();

                            if (successful === true)
                                toastr.success(GetTranslation('tdi_mark_copy_success_message', 'Mark was copied'));
                        } catch (err) {
                            alert(err);
                        }

                    });
                });
            }

            $('#template_marks_search').on('keydown', function (e) {
                var search = $(this).val();
                if(e.which == 13) {
                    template_marks_datatable.options.data.source.read.params = {
                        query: {
                            // generalSearch: ''
                        },
                        search: $.extend(template_marks_datatable.options.data.source.read.params.search, {search_text: search})
                    };
                    template_marks_datatable.reload();
                }
            });
        }

        function initTemplateTableFilter(){
            function filterRequest(data) {
                templates_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: $.extend(templates_datatable.options.data.source.read.params.search, data)
                };
                templates_datatable.reload();
            }

            var filter = $('#templates-filter').get()[0];
            var portlet = $('#documents-portlet').get()[0];
            if((filter !== undefined) && (filter !== null)){
                $(filter).find('button').on('click', function(){
                    var clicked_btn = this;
                    var filter_type = $(clicked_btn).data('filter');
                    $(filter).find('button').removeClass('btn-brand').addClass('btn-secondary');
                    $(clicked_btn).removeClass('btn-secondary').addClass('btn-brand');

                    filterRequest({filter: filter_type});
                    // Show/hide content parts
//                    $(portlet).find('.reminders-index-portlet-content').removeClass('active');
//                    $(portlet).find('.reminders-index-portlet-content[data-content-part-'+filter_type+'="true"]').addClass('active');
                });
            }

            $('#m_form_status').on('change', function () {
                var type_id = $(this).val();
                filterRequest({type: type_id});
            });

            $('#filter_by_supplier_select2').on('change', function () {
                var type_id = $(this).val();
                filterRequest({supplier: type_id});
            });

            $('#input_search_template').on('keydown', function (e) {
                var search = $(this).val();

                if(e.which == 13) {
                    filterRequest({search_text: search});
                }
            });

            $('#templates_documents_datatable').on('click', 'a#show_all_link', function () {
                templates_datatable.options.data.source.read.params = {
                    query: {
                        // generalSearch: ''
                    },
                    search: {

                    }
                };
                templates_datatable.reload();
            });
        }
        
        function initTemplatesDataTable(){
            var columns = [
                {
                    field: 'title',
                    title: GetTranslation('tdi_table_col_name', 'Name'),
                    sortable: false,
                    template: function(row, index, datatable) {
                        return '<span' + (row.type == 'system' ? ' data-container="body" data-toggle="m-popover" data-placement="top" data-content="' + GetTranslation('tdi_sys_template_notice', 'System template, edit disabled') + '"' : '') + '>' +
                            '<a href="#" class="m-link name-link m--font-bold mr-1" data-is-system="' + (row.type == 'system' ? 'true' : 'false') + '" data-template-id="'+row.id+'" data-toggle="modal" data-target="#m_modal_template_edit">'
                            + row.title +
                            '</a>'
                            + (row.type == 'system' ? '<i class="la la-exclamation-circle system-status-icon"></i>' : '')
                            +'</span>';
                    }
                },
                {
                    field: 'documents_type_name',
                    title: GetTranslation('tdi_table_col_type', 'Type'),
                    sortable: true
                },
                {
                    field: 'document_supplier_name',
                    title: GetTranslation('tdi_filter_supplier_select_label', 'Supplier'),
                    sortable: true
                },
                {
                    field: 'lang_name',
                    title: GetTranslation('tdi_filter_languages', 'Languages'),
                    sortable: true,
                    template: function(row, index, datatable) {
                        return row.lang.name + ' (' + row.lang.original_name + ')';
                    }
                },
                {
                    field: 'created_at',
                    title: GetTranslation('tdi_table_col_created_at', 'Created at'),
                    sortable: true
                },
                {
                    field: 'updated_at',
                    title: GetTranslation('tdi_table_col_updated_at', 'Updated_at'),
                    sortable: true
                },
                {
                    field: 'actions',
                    title: GetTranslation('tdi_table_col_actions', 'Actions'),
                    textAlign: 'center',
                    sortable: false,
                    template: function(row, index, datatable) {
                        if (row.type == 'system')
                            return '';

                        return '<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill document-t-delete-link" title="' + GetTranslation('delete', 'Delete') + '" data-template-id="'+(row.id)+'">'+
                                   '<i class="la la-trash"></i>'+
                                '</a>';
                    }
                }
            ];
            
            templates_datatable = $('#templates_documents_datatable').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: baseUrl + '/ita/' + tenantId + '/templates/get-documents-templates',
                            method: 'GET',
                            params: {
                                _csrf: $('meta[name="csrf-token"]').attr('content'),
                                query: {
                                },
                                search: {
                                }
                                // custom query params
                            },
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch'),
                    delay: 400
                },
                rows: {
                    afterTemplate: function (row, data, index) {
                        // Init popovers
                        mApp.initPopover($(row).find('[data-toggle="m-popover"]'));
                    },
                    callback: function () {
                        
                    },
                    // auto hide columns, if rows overflow. work on non locked columns
                    autoHide: false
                },
                columns: columns,
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],  //'top', 'bottom'
                    items: {
                        pagination: {
                            type: 'default',

                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },

                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },

                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },

                        info: true
                    }
                },
                translate: {
                    records: {
                        processing: GetTranslation('datatable_data_processiong','Please wait...'),
                        noRecords: GetTranslation('datatable_records_not_found','No records found') + '.' + ' <a href="#" id="show_all_link">' + GetTranslation('datatable_show_all_message', 'Show all') + '</a>'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    first: GetTranslation('pagination_first','First'),
                                    prev: GetTranslation('pagination_previous','Previous'),
                                    next: GetTranslation('pagination_next','Next'),
                                    last: GetTranslation('pagination_last','Last'),
                                    more: GetTranslation('pagination_more','More pages'),
                                    input: GetTranslation('pagination_page_number','Page number'),
                                    select: GetTranslation('pagination_page_size','Select page size')
                                },
                                info: GetTranslation('pagination_records_info','Displaying {{start}} - {{end}} of {{total}} records')
                            }
                        }
                    }
                }
            });
            
            /* DATATABLE EVENTS */
            
            // Init event
            $(templates_datatable).on('m-datatable--on-init', function(e){
                
            });
            // Layout updated event
            $(templates_datatable).on('m-datatable--on-layout-updated', function(e){
                
            });
        }
        
        function initPageWidgets(){
            // Supplier filter
            var select = $('#filter_by_supplier_select2').get()[0];
            $(select).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-suppliers-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation('tdi_filter_supplier_select_placeholder', 'Select suppliers');
                    }
                    return data.text;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 0,
                width: '100%',
                dropdownParent: $(select).closest('.m-portlet__body'),
                placeholder: GetTranslation('tdi_filter_supplier_select_placeholder', 'Select supplier')
            });

            var select2 = $('#supplier').get()[0];
            $(select2).select2({
                ajax: {
                    url: baseUrl + '/ita/' + tenantId + '/requests/get-suppliers-options',
                    method: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _csrf: $('meta[name="csrf-token"]').attr('content')
                        };
                        return query;
                    },
                    delay: 1000,
                    processResults: function (data) {
                        var array = [];
                        for (var i in data) {
                            array.push({id: data[i]['id'], text: data[i]['name']});
                        }
                        return {
                            results: array
                        };
                    }
                },
                templateSelection: function (data) {
                    if (data.id === '') {
                        return GetTranslation('tdi_filter_supplier_select_placeholder', 'Select suppliers');
                    }
                    return data.text;
                },
                language: tenant_lang,
                allowClear: true,
                minimumInputLength: 0,
                width: '100%',
                dropdownParent: $(select2).closest('.modal-body'),
                placeholder: GetTranslation('tdi_filter_supplier_select_placeholder', 'Select supplier')
            });
        }

        function clearTemplateModal(modal){
            // Clear id input
            $('#template_id_input').val('');
            // Clear name input
            $('#template_name_input').val('');
            // Clear doctype input
            $("#template_type").val('');
            // $('#template_type').prop('disabled', true);
            $("#template_type").selectpicker("refresh");

            $("#supplier").val(null).trigger('change');

            $("#language").val('');
            $("#language").selectpicker("refresh");
            // Clear document body editor
            var config = CKEDITOR.instances['document-body-template-editor'].config;
            config.readOnly = true;
            CKEDITOR.instances['document-body-template-editor'].destroy();
            CKEDITOR.replace('document-body-template-editor', config);
            CKEDITOR.instances['document-body-template-editor'].setData('');
            // Show preloader
            $('#template_form').hide();
            // $('#template-modal-preloader').show();
            // Hide system temlpate notice
            // $('#system-template-notice').removeClass('d-none');

            $('#m_modal_template_edit .btn-primary').data('template-id', null);
            auto_save = true;
        }

        function switchType(modal,event) {
            var is_system = $(event.relatedTarget).data('is-system');
            var template_id = $(event.relatedTarget).data('template-id');
            if (is_system) {
                $('#system-template-notice').addClass('d-none');
                $.ajax({
                    url: baseUrl + '/ita/' + tenantId + '/templates/get-template-doc-by-id?id=' + template_id,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        _csrf: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        // Load name input
                        $('#template_name_input').val(data.title);
                        $('#template_name_input').prop('disabled', false);
                        // Load doctype input
                        $('#template_type').val(data.documents_type_id);
                        $('#template_type').prop('disabled', false);
                        $("#template_type").selectpicker("refresh");


                        if ($('#supplier').find("option[value='" + data.supplier_id + "']").length) {
                            $('#supplier').val(data.supplier_id).trigger('change');
                        } else {
                            // Create a DOM Option and pre-select by default
                            var newOption = new Option(data.supplier_name, data.supplier_id, true, true);
                            // Append it to the select
                            $('#supplier').append(newOption).trigger('change');
                        }
                        $('#supplier').prop('disabled', false);

                        $('#language').val(data.language);
                        $('#language').prop('disabled', false);
                        $("#language").selectpicker("refresh");
                        $('#template-theme-block').hide();
                        //Save button
                        $('#m_modal_template_edit .btn-primary').data('template-id', null);
                        template_marks_datatable.options.data.source.read.params = {
                            query: {
                                // generalSearch: ''
                            },
                            search: $.extend(template_marks_datatable.options.data.source.read.params.search, {type: data.documents_type_id})
                        };
                        template_marks_datatable.reload();

                        // Load theme input
                        $("#template-theme-input").val(data.subject);
                        // Load email body editor
                        var config = CKEDITOR.instances['document-body-template-editor'].config;
                        config.readOnly = false;
                        CKEDITOR.instances['document-body-template-editor'].destroy();
                        CKEDITOR.replace('document-body-template-editor', config);
                        CKEDITOR.instances['document-body-template-editor'].setData(data.body);
                        $('#document-body-template-editor').show();
                        $('#template-theme-block').show();
                        // Show form
                        $('#template-modal-preloader').hide();
                        $('#template_form').fadeIn();
                        $('#m_modal_template_edit .btn-primary').prop('disabled', false);
                    },
                    error: function () {
                        alert(GetTranslation('cant_get_data_error', 'Error with data getting!'));
                    }
                });
            }
        }

    });
})(jQuery);