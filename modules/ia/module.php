<?php

namespace app\modules\ia;
use Yii;

/**
 * admin module definition class
 */
class module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\ia\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->layout = '/admin/main';
//        if(isset(Yii::$app->params['stage']))
//        {
//            if (Yii::$app->params['stage'] == 'prot')
//                $this->controllerNamespace = 'app\modules\ia\controllers\prot';
//        }
    }
}
