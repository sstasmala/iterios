<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsLog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="notifications-log-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'trigger_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Trigger ID...']],

            'reminder_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Reminder ID...']],

            'contact_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Contact ID...']],
            'course_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Course Id...']],
            

            'agent_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Agent ID...']],

            'status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Status...']],

            'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

            'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],

            'errors' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Errors...','rows' => 6]],

            'channel' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Channel...', 'maxlength' => 255]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
