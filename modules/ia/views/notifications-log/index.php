<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use app\models\SystemNotifications;

use app\models\NotificationsLog;
use app\models\NotificationsCourse;
use app\models\NotificationsTrigger;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NotificationsLog $searchModel
 */

$this->registerJs("jQuery(document).ready(function(){
    jQuery('.view-text').on('click', function(){
        var text = $('#text_contain_' + $(this).data('id')).html();
        alert(text);
    });
});", \yii\web\View::POS_END);


$this->title = 'Reminder Logs';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="notifications-log-index">
    <?php
    $columns = [            
            'id',
            'trigger_id',
            [
                'attribute' => 'reminder_id',
                'header' => 'Reminder Id',
                'format' => 'raw',
                'value' => function($model){
                    return '<a href="/ia/reminder-constructor/update?id='.$model->reminder_id.'" target="_blank">'.$model->reminder_id.'</a>';
                },
            ],
            'contact_id',
            'agent_id',
            [
                'attribute' => 'read',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->channel == 'task')
                        return '';

                    if ($model->channel == 'email') {
                        $job_id = explode('||', $model->send_data);
                        $job_id[0] = explode(',', $job_id[0]);

                        foreach ($job_id[0] as $k => $v)
                            if (empty($v))
                                unset($job_id[0][$k]);

                        if (empty($job_id[0]))
                            return '';

                        $statuses = \app\models\LogsEmail::find()->select(['id', 'status'])-> where(['in', 'id', $job_id[0]])->asArray()->all();
                        if (empty($statuses))
                            return '';

                        $statuses_email = [];
                        foreach ($statuses as $status)
                            $statuses_email[] = '<a target="_blank" href="/ia/logs-public-email/view?id='.$status['id'].'"><span class="label label-info">' . $status['status'] . ' ('.$status['id'].')</span></a>';

                        return implode(', ', $statuses_email);
                    }

                    if (empty($model->system_notifications_id))
                        return '';

                    return (SystemNotifications::find()->where(['id' => $model->system_notifications_id])->one()->read) ? '<span class="label label-success">yes</span>' : '<span class="label label-danger">no</span>';
                }
            ],
            'channel',
            [
                'attribute' => 'course_id',

                'format' => 'raw',
                'value' => function($model){
                    return '<a href="/ia/reminder-queue/view?id='.$model->course_id.'" target="_blank">Queue</a>';
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model){
                    return ($model->status == 1) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">error</span>';
                },
            ],
            'errors:ntext',
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert'],
            ], 
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert'],
            ],
            [
                'attribute' => 'send_data',
                'header' => 'Details',
                'format' => 'raw',
                'value' => function ($model) {

                    if ($model->channel == 'email'){
                        $job_id = explode('||', $model->send_data);
                        $job_id[0] = explode(',', $job_id[0]);

                        $urls = [];
                        foreach($job_id[0] as $jId) {
                            if (empty($jId))
                                continue;

                            $urls[] = '<a target="_blank" href="/ia/logs-public-email/view?id=' . $jId . '">log (' . $jId . ')</a>';
                        }

                        return implode(', ', $urls);
                    }

                    if (empty($model->send_data))
                        return '';

                    $text_view = '<a class="label label-success view-text" data-id="'.$model->id.'">read</a><span id="text_contain_'.$model->id.'"style="display:none;">'.$model->send_data.'</span>';

                    if ($model->channel == 'system')
                        return $text_view;

                    if ($model->channel == 'task')
                        return $text_view ;

                    return '';
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'header' => 'Actions',
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{delete}',
            ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
        ],
        'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
    ]);

    DynaGrid::end();
    ?>

</div>
