<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Segments result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="segments-result-index">
    <?php
    $columns = [
        'segment',
        'tenant',
        'contact',
        'phone',
        'email',
    ];


    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Segments result list</h3>',
                'before' => '',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['loader'], [
                        'data-pjax'=>0,
                        'class' => ($jobUpdateSegmentsListRun)? 'btn btn-success disabled':'btn btn-success',
                        'title'=>'Refresh data (run job)',
                        'aria-disabled' => ($jobUpdateSegmentsListRun)? 'true':'false',
                    ]) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-segments-result'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
