<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\helpers\MCHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Document Placeholders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-index">
    <?php
    $columns = [
        'id',
        'short_code',
        'module',
        [
            'attribute' => 'path',
            'format'=>'raw',
            'value' => function ($model) {
                return !empty($model->fake) ? '<span class="label label-danger">Fake</span>' : $model->default;
            }
        ],
        'default',
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/document-placeholders',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme' => 'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel' => [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> document-placeholders</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content' =>
                    (true ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                ],
                ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id' => 'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
