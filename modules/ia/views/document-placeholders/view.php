<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 */

$this->title = 'DocumentPlaceholders: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Document Placeholders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-placeholders-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/document-placeholders',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            'short_code',
                            'module',
                            [
                                'attribute'=>'path',
                                'value'=> !empty($model->fake) ? '<span class="label label-danger">Fake</span>' : $model->default,
                                'format'=>'raw'
                            ],
//                            'path',
                            'default',
                            'description'
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
