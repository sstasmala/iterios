<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\DocumentPlaceholders;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/document-placeholders/items.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

?>

<div class="document-placeholders-form">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

            <?= $form->field($model, 'short_code'); ?>

            <?= $form->field($model, 'module')->widget(Select2::classname(), [
                'data' => DocumentPlaceholders::getModules(),
                'options' => ['placeholder' => 'Choose module'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <?= $form->field($model, 'path')->widget(Select2::classname(), [
                'data' => !empty($model->module) ? DocumentPlaceholders::getAllPath($model->module) : [],
                'options' => ['placeholder' => 'Choose Path'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>

            <?= $form->field($model, 'default'); ?>
            <?php echo $form->field($model, 'description')->textarea(['placeholder' => 'Enter placeholder description...']) ?>
            <?= $form->field($model, 'fake')->checkbox(); ?>
        </div>
        <div class="box-footer">
            <?php
            echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            );
            ?>
            <?php ActiveForm::end(); ?>
        </div>
</div>