<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryPlaceholders $model
 */

$this->title = $model->short_code;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Placeholders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$path = '';

// if (!empty($model->module)) {
//     switch (\app\models\DeliveryPlaceholders::USED_MODULES[$model->module]) {
//         case \app\models\DeliveryPlaceholders::MODULE_SERVICE:
//             $field = \app\models\ServicesFieldsDefault::findOne((int) $model->path);
//
//             if ($field !== null)
//                 $path = $field->code .'/'.$field->name;
//
//             break;
//     }
// }

$delivery_types = [];

foreach ($model->deliveryPlaceholderTypes as $type) {
    $delivery_types[] = Html::a($type->name, '/ia/delivery-types/view?id='.$type->id, ['target' => '_blank']);
}
?>
<div class="delivery-placeholders-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        'id',
                        [
                            'attribute' => 'delivery_type',
                            'label' => 'Delivery Type',
                            'format' => 'raw',
                            'value' => !empty($delivery_types) ? implode(', ', $delivery_types) : '(not set)'
                        ],
                        'short_code',
                        [
                            'attribute' => 'module',
                            'value' => !empty($model->module) && in_array($model->module, \app\models\DeliveryPlaceholders::USED_MODULES, true) ? \app\models\DeliveryPlaceholders::USED_MODULES[$model->module] : '(not set)'
                        ],
                        [
                            'attribute'=>'path',
                            'value'=> $model->fake ? '<span class="label label-danger">Fake</span>' : (!empty($path) ? $path : '(not set)'),
                            'format'=>'raw'
                        ],
                        'default',
                        'description',
                        [
                            'attribute'=>'created_at',
                            'containerOptions'=>['class'=>'to-datetime-convert'],
                            'valueColOptions'=>['class'=>'to-datetime-convert'],
                            'value' => null === $model->created_at ? '' : $model->created_at
                        ],
                        [
                            'attribute'=> 'updated_at',
                            'valueColOptions'=>['class'=>'to-datetime-convert'],
                            'value' => null === $model->updated_at ? '' : $model->updated_at
                        ],
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                        ]
                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
