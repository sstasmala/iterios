<?php

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryPlaceholders $model
 */

$this->title = 'Create Delivery Placeholder';
$this->params['breadcrumbs'][] = ['label' => 'Delivery Placeholders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-placeholders-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/document-placeholders',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
