<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\DeliveryPlaceholders $searchModel
 */

$this->title = 'Delivery Placeholders';
$this->params['breadcrumbs'][] = $this->title;

function getPath($model) {
    $path = '';

    // if (!empty($model->module)) {
    //     switch (\app\models\DeliveryPlaceholders::USED_MODULES[$model->module]) {
    //         case \app\models\DeliveryPlaceholders::MODULE_SERVICE:
    //             $field = \app\models\ServicesFieldsDefault::findOne((int) $model->path);
    //
    //             if ($field !== null)
    //                 $path = $field->code .'/'.$field->name;
    //
    //             break;
    //     }
    // }

    return $path;
}

function getDeliveryTypes($model)
{
    $delivery_types = [];

    foreach ($model->deliveryPlaceholderTypes as $type) {
        $delivery_types[] = Html::a($type->name, '/ia/delivery-types/view?id=' . $type->id, ['target' => '_blank']);
    }

    return $delivery_types;
}
?>
<div class="delivery-placeholders-index">
    <?php
    $columns = [
        'id',
        [
            'attribute' => 'delivery_type',
            'label' => 'Delivery Type',
            'format' => 'raw',
            'value' => function ($model) {
                $delivery_types = getDeliveryTypes($model);

                return !empty($delivery_types) ? implode(', ', $delivery_types) : '(not set)';
            }
        ],
        'short_code',
        [
            'attribute' => 'module',
            'value' => function($model) {
                return !empty($model->module) && in_array($model->module, \app\models\DeliveryPlaceholders::USED_MODULES, true) ? \app\models\DeliveryPlaceholders::USED_MODULES[$model->module] : '(not set)';
            }
        ],
        [
            'attribute' => 'path',
            'format' => 'raw',
            'value' => function($model) {
                if ($model->fake)
                    return '<span class="label label-danger">Fake</span>';

                $path = getPath($model);

                return !empty($path) ? $path : '(not set)';
            }
        ],
        'default',
        [
            'attribute' => 'description',
            'visible' => false
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->created_at ? '' : $model->created_at);
            }
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->updated_at ? '' : $model->updated_at);
            }
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'after' => false,
                'before' => $lang_select
            ],
            'toolbar' => [
                ['content'=>
                     (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
