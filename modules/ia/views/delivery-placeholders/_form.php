<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryPlaceholders $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/delivery-placeholders/items.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$path = '';

// if (!empty($model->module)) {
//     switch (\app\models\DeliveryPlaceholders::USED_MODULES[$model->module]) {
//         case \app\models\DeliveryPlaceholders::MODULE_SERVICE:
//             $field = \app\models\ServicesFieldsDefault::findOne((int) $model->path);
//
//             if ($field !== null)
//                 $path = $field->code .'/'.$field->name;
//
//             break;
//     }
// }
?>

<div class="delivery-placeholders-form">

    <?php
        $columns = [
            'id' => ['type' => Form::INPUT_HIDDEN],
            'short_code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Short Code...', 'maxlength' => 255]],
            'module' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\widgets\Select2',
                'options' => [
                    'data' => \app\models\DeliveryPlaceholders::USED_MODULES,
                    'options' => [
                        'placeholder' => 'Choose module',
                    ],
                    'pluginOptions' => [
                        'minimumResultsForSearch' => '-1'
                    ],
                ]
            ],
        ];
    ?>

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label col-md-2" for="deliveryplaceholders-delivery_types">Delivery Types</label>
                <div class="col-md-10">
                    <?php echo Select2::widget([
                        'name' => 'delivery_types',
                        'value' => \yii\helpers\ArrayHelper::getColumn($model->deliveryPlaceholderTypes, 'id'),
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\DeliveryTypes::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Choose delivery types',
                            'multiple' => 'multiple',
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <?php echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $columns
    ]);
    ?>

    <div class="form-group">
        <label class="control-label col-md-2" for="deliveryplaceholders-delivery_path">Path</label>
        <div class="col-md-10">
            <select class="form-control" id="deliveryplaceholders-path" name="DeliveryPlaceholders[path]">
                <?php if (!empty($path)): ?>
                    <option value="<?= $model->path ?>"><?= $path ?></option>
                <?php endif; ?>
            </select>
            <div class="help-block"></div>
        </div>
    </div>

    <?php echo $form->field($model, 'default')->textInput(['placeholder' => 'Enter default value']) ?>
    <?php echo $form->field($model, 'description')->textarea(['placeholder' => 'Enter placeholder description...']) ?>
    <?= $form->field($model, 'fake')->checkbox(); ?>

    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
