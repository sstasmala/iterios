<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\helpers\MCHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Sms List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-list-index">
    <?php
    $columns = [
        'id',
        [
            'attribute' => 'send_sms_id',
            'label' => 'Segment name',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->name:'(not set)';
            }
        ],
        [
            'attribute' => 'time_send',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Alpha name',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->alpha_name:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Count phones',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->count_phones:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Count delivered phones',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->count_delivered:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Count not delivered phones',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->count_not_delivered:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Provider',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->provider->name:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'Tenant',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->tenant->name:'(not set)';
            }
        ],
        [
            'attribute' => 'send_sms_id',
            'label' => 'User create',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->sendSms) ? $model->sendSms->created->first_name . ' '. $model->sendSms->created->last_name:'(not set)';
            }
        ],
    ];


    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Sms list</h3>',
//                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
//                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
