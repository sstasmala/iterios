<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TariffsOrders $model
 */

$this->title = 'Create Tariffs Orders';
$this->params['breadcrumbs'][] = ['label' => 'Tariffs Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-orders-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
