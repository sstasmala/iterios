<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TariffsOrders $model
 */

$this->title = 'Update Tariffs Orders: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tariffs Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tariffs-orders-update">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
