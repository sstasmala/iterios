<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\TariffsOrders $model
 * @var yii\widgets\ActiveForm $form
 */
$customers = \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(),'id','name')
?>

<div class="tariffs-orders-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    echo $form->errorSummary($model);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

//            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'tariff_id' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items'=> \yii\helpers\ArrayHelper::map(\app\models\Tariffs::find()->asArray()->all(),'id','name'),
                'options' => ['placeholder' => 'Enter Tariff ID...']],

            'customer_id' => ['type' => Form::INPUT_WIDGET, 'widgetClass'=>\kartik\select2\Select2::class,'options'=>
                [
                        'data'=> \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(),'id','name'),
                ]
            ],

            'duration_date' => ['type' => Form::INPUT_STATIC,
                'staticValue' => (isset($model->duration_date))?\app\helpers\DateTimeHelper::convertTimestampToDateTime($model->duration_date,'Y-m-d H:i:s',date_default_timezone_get()):'Not set',
                'options' => ['placeholder' => 'Enter Duration Date...']],

            'status' => ['type' => Form::INPUT_STATIC,
                'staticValue'=>(isset($model->status)?\app\models\TariffsOrders::STATUSES[$model->status]:\app\models\TariffsOrders::STATUSES[\app\models\TariffsOrders::STATUS_NOT_ACTIVE])
                ,'options' => ['placeholder' => 'Enter Status...']],

        ]

    ]);

    if(!$model->isNewRecord && $model->status == \app\models\TariffsOrders::STATUS_ACTIVE)
        echo Html::a('Cancel','cancel?id='.$model->id,['class'=>'btn btn-warning']);
    echo ' ';
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
