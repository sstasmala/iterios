<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TariffsOrders $searchModel
 */

$this->title = 'Tariffs Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-orders-index">
    <?php
    $columns = [
        'id',
//        'name',
        [
            'attribute' => 'tariff_id',
            'value' => function($model)
            {
                if($model->tariff_id !== null)
                    return $model->tariff->name;
                return null;
            }
        ],
        [
            'attribute' => 'status',
            'filterInputOptions'=>['placeholder'=>'Status'],
            'filter'=>\app\models\search\TariffsOrders::STATUSES,
            'filterType'=>GridView::FILTER_SELECT2,
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'value' => function($model)
            {
                if($model->status !== null)
                    return \app\models\TariffsOrders::STATUSES[$model->status];
                return null;
            }
        ],
        [
            'label' => 'Customer name',
            'attribute' => 'customer_id',
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->customer_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/tenants/view?id='.$model->customer->id.'">'.$model->customer->name.'</a>';
                return null;
            }
        ],
        [
            'label' => 'Customer legal name',
            'attribute' => 'customer_id',
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->customer_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/customers/view?id='.$model->customer->id.'">'.$model->customer->legal_name.'</a>';
                return null;
            }
        ],
        [
            'attribute' => 'duration_date',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}{update}{delete}'
        ]
    ];

    $lang_select ='';

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Tariffs orders</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    //(true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['update-translations'], ['data-pjax'=>0, 'class' => 'btn btn-danger', 'title'=>'Refresh data from config']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-tariffs-Orders'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>
</div>
