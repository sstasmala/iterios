<?php

use app\models\Segments;
use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;


$this->registerAssetBundle(\app\assets\DatepickerAsset::className());
$this->registerCssFile('@web/admin/plugins/jquery-builder/query-builder.dark.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-builder/query-builder.standalone.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$this->registerJsFile('@web/admin/js/pages/segments/builder.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

if(isset($model->rules)){
    $this->registerJs('var rules = '. $model->rules . ';', \yii\web\View::POS_HEAD);
}else {
    $this->registerJs('var rules = {condition: "AND", allow_empty: true, rules: [{empty:true}]};', \yii\web\View::POS_HEAD);
}

?>

<div class="segments-form">

    <?php $form = ActiveForm::begin(['id'=>'segments-constructor','type' => ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Name']) ?>
    <?= $form->field($model, 'modules')->widget(Select2::classname(), [
        'data' => Segments::getModules(),
        'options' => ['placeholder' => 'Choose modules'],
    ]); ?>
    <?= $form->field($model, 'type', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value'=>Segments::TYPE_SYSTEM]); ?>
    <?= $form->field($model, 'rules', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php ActiveForm::end(); ?>

    <div id="builder-widgets" class="query-builder form-inline"></div>

    <?= Html::Button($model->isNewRecord ? 'Create' : 'Update',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id'=>'segments-form-submit']
    );?>

</div>

<style type="text/css">
    #builder-widgets .rules-group-header button[data-add=group]{
        display: none;
    }
</style>