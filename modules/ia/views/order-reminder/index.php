<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Reminders $searchModel
 */

$this->title = 'Order reminder '.(Yii::$app->controller->id === 'order-reminder-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminders-index">
    <?php
    $columns = [
        'id',
        'name',
        'type',
    ];

    if (Yii::$app->controller->id === 'order-reminder-public') {
        $columns[] = [
            'format' => 'raw',
            'label' => 'Tenant',
            'value' => function ($model) {
                return null === $model->reminderTenant->tenant ? '' : Html::a($model->reminderTenant->tenant->name, '/ia/tenants/view?id='.$model->reminderTenant->tenant_id, ['target' => '_blank']);
            }
        ];
        $columns[] = [
            'format' => 'raw',
            'label' => 'Active',
            'value' => function ($model) {
                return $model->reminderTenant->active ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
            }
        ];
    }

    $columns_second = [
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return null === $model->created_at ? '' : $model->created_at;
            }
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return null === $model->updated_at ? '' : $model->updated_at;
            }
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $columns = array_merge($columns, $columns_second);

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-order_reminder'] // a unique identifier is important
    ]);
    if (0 === strpos($dynagrid->theme, 'simple')) {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>
</div>
