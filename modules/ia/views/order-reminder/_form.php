<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Reminders $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="reminders-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...', 'maxlength' => 255]]
        ]
    ]);

    echo Html::hiddenInput('Reminders[type]', (Yii::$app->controller->id === 'order-reminder-system' ? \app\models\Reminders::TYPE_SYSTEM : \app\models\Reminders::TYPE_PUBLIC));

    if (Yii::$app->controller->id !== 'order-reminder-system') {
        echo '<div class="form-group">'.
            '<label class="control-label col-md-2" for="reminders-name">Tenant</label>'.
            '<div class="col-md-10">';
        echo \kartik\widgets\Select2::widget([
            'name' => 'tenant_id',
            'value' => !$model->isNewRecord ? $model->reminderTenant->tenant->id : '',
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose Tenant'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        echo '<div class="help-block"></div></div></div>';

        echo '<div class="form-group">'.
            '<div class="col-md-offset-2 col-md-10">'.
            '<div class="checkbox"><input type="hidden" name="active" value="0"><label><input type="checkbox" id="orderreminder-is_default" name="active" value="1"'.($model->isNewRecord || $model->reminderTenant->active ? ' checked="checked"' : '').'> Active</label></div>';
        echo '<div class="help-block"></div></div></div>';
    }

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
