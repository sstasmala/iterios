<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Reminders $model
 */

$this->title = 'Order reminder ' . (Yii::$app->controller->id === 'order-reminder-system' ? 'system' : 'public') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reminders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminders-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            'id',
                            'name',
                            'type'
                        ];

                        if ($model->type === \app\models\Reminders::TYPE_PUBLIC) {
                            $columns[] = [
                                'format' => 'raw',
                                'label' => 'Tenant',
                                'value' => null === $model->reminderTenant->tenant ? '' : Html::a($model->reminderTenant->tenant->name, '/ia/tenants/view?id='.$model->reminderTenant->tenant_id, ['target' => '_blank'])
                            ];
                            $columns[] = [
                                'format' => 'raw',
                                'label' => 'Active',
                                'value' => $model->reminderTenant->active ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>'
                            ];
                        }

                        $columns_second = [
                            [
                                'attribute' => 'created_at',
                                'containerOptions' => ['class'=>'to-datetime-convert'],
                                'valueColOptions' => ['class'=>'to-datetime-convert'],
                                'value' => null === $model->created_at ? '' : $model->created_at
                            ],
                            [
                                'attribute' => 'updated_at',
                                'valueColOptions' => ['class'=>'to-datetime-convert'],
                                'value' => null === $model->updated_at ? '' : $model->updated_at
                            ],
                            [
                                'attribute' => 'created_by',
                                'format' => 'raw',
                                'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                            ],
                            [
                                'attribute' => 'updated_by',
                                'format' => 'raw',
                                'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                            ]
                        ];

                        $columns = array_merge($columns, $columns_second);
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
