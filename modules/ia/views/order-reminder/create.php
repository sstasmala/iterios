<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Reminders $model
 */

$this->title = 'Create reminder ' . (Yii::$app->controller->id === 'order-reminder-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = ['label' => 'Reminder', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminders-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
