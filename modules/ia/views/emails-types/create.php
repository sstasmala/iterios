<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EmailsTypes $model
 */

$this->title = 'Create Emails Types';
$this->params['breadcrumbs'][] = ['label' => 'Emails Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-types-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/emails-types',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
