<?php

use app\models\Settings;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile(Yii::$app->params['baseUrl'].'/admin/plugins//alertify/js/alertify.js',
    ['depends' => \app\assets\AdminAsset::class]);
$this->registerJsFile(Yii::$app->params['baseUrl'].'/admin/plugins//select2/js/select2.min.js',
    ['depends' => \app\assets\AdminAsset::class]);
$this->registerJsFile(Yii::$app->params['baseUrl'].'/admin/js/pages/settings/lang.js',
    ['depends' => \app\assets\AdminAsset::class]);
$this->registerJsFile(Yii::$app->params['baseUrl'].'/admin/js/pages/settings/settings.js',
    ['depends' => \app\assets\AdminAsset::class]);

$languages = new \yii\data\ArrayDataProvider(['allModels' => $languages,'pagination' => [
    'pageSize' => 10,
]]);
?>

<div class="settings-index row">



    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#references" data-toggle="tab" aria-expanded="true">References</a></li>
                <li class=""><a href="#settings_tab" data-toggle="tab" aria-expanded="false">Settings</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="references">
                    <div class="languages-box">
                        <?php
                        echo \kartik\grid\GridView::widget([
                            'dataProvider' => $languages,
                            'columns' => [
                                [
                                    'attribute'=>'name',
                                    'value' => function($model) {
                                        return $model['name'].' ('.$model['original_name'].')';
                                    }
                                ],
                                'iso',
                                [
                                    'class' => 'kartik\grid\ActionColumn',
                                    'template' => '{disable}',
                                    'buttons' =>
                                        [
                                            'disable' => function ($url,$model)
                                            {
                                                if($model->iso=='en')
                                                    return '';
                                                return '<a class="btn btn-info" href="'.Yii::$app->params['baseUrl'].'/ia/settings/disable-lang?id='.$model->id.'">Disable</a>';
                                            }
                                        ]
                                ]
                            ],
                            'panel' => [
                                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> Languages <span class="badge"></span> </h3>',
                                'type' => 'info',
                                'after' => \yii\helpers\Html::a(' Add more',
                                    [''], ['class' => 'btn btn-success','id'=>'language-enable']),
                                'showFooter' => false
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="settings_tab">
                    <div class="email_providers-box">
                        <div id="settings_email" class="grid-view hide-resize">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> eMail providers settings <span class="badge"></span> </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="email_providers" class="table-responsive kv-grid-container">
                                    <table id="email_providers_read" class="kv-grid-table table table-bordered table-striped kv-table-wrap">
                                        <thead>
                                        <tr>
                                            <th data-col-seq="0" style="width: 50%;">Name</th>
                                            <th data-col-seq="1" style="width: 50%;">Value</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($email_providers_settings as $i => $val): ?>
                                            <tr data-key="<?= $i ?>">
                                                <td data-col-seq="0"><?= Settings::EMAIL_TITLES[$val['key']] ?></td>
                                                <?php if ($val['key'] == Settings::PROVIDER_SYSTEM || $val['key'] == Settings::PROVIDER_PUBLIC): ?>
                                                    <td data-col-seq="1"><?= (!empty($email_providers_data) && !empty($val['value']) ? $email_providers_data[($val['key'] == Settings::PROVIDER_SYSTEM ? 'system' : 'public')][$val['value']] : '') ?></td>
                                                <?php else: ?>
                                                    <td data-col-seq="1"><?= $val['value'] ?></td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <table id="email_providers_edit" class="kv-grid-table table table-bordered table-striped kv-table-wrap" style="display: none">
                                        <thead>
                                        <tr>
                                            <th data-col-seq="0" style="width: 50%;">Name</th>
                                            <th data-col-seq="1" style="width: 50%;">Value</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($email_providers_settings as $i => $val): ?>
                                            <tr data-key="<?= $i ?>">
                                                <td data-col-seq="0"><?= Settings::EMAIL_TITLES[$val['key']] ?></td>
                                                <?php if ($val['key'] == Settings::PROVIDER_SYSTEM || $val['key'] == Settings::PROVIDER_PUBLIC): ?>
                                                    <td data-col-seq="1">
                                                        <?= \kartik\widgets\Select2::widget([
                                                            'name' => $val['key'],
                                                            'value' => $val['value'],
                                                            'data' => (!empty($email_providers_data) && !empty($email_providers_data[($val['key'] == Settings::PROVIDER_SYSTEM ? 'system' : 'public')]) ?
                                                                $email_providers_data[($val['key'] == Settings::PROVIDER_SYSTEM ? 'system' : 'public')] : []),
                                                            'options' => ['placeholder' => 'Choose provider'],
                                                            'pluginOptions' => [
                                                                'allowClear' => false
                                                            ],
                                                        ]) ?>
                                                    </td>
                                                <?php else: ?>
                                                    <td data-col-seq="1">
                                                        <div class="form-group">
                                                            <input type="text" id="<?= $val['key'] ?>" class="form-control" value="<?= $val['value'] ?>" placeholder="<?= Settings::EMAIL_TITLES[$val['key']] ?>">
                                                            <?= ($val['key'] != 'provider_system' && $val['key'] != 'provider_public' ? '<small>* Only for system provider</small>' : '') ?>
                                                        </div>
                                                    </td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kv-panel-after">
                                    <button id="edit_email_providers" class="btn btn-primary">Edit</button>
                                    <button id="save_email_providers" class="btn btn-success" style="display: none">Save</button>
                                    <button id="read_email_providers" class="btn btn-primary" style="display: none">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="setting-MCHelper-box">
                        <div id="settings-MCHelper" class="grid-view hide-resize">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> MCHelper settings <span class="badge"></span> </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="MCHelper" class="table-responsive kv-grid-container">
                                    <table id="MCHelper_read" class="kv-grid-table table table-bordered table-striped kv-table-wrap">
                                        <thead>
                                        <tr>
                                            <th data-col-seq="0" style="width: 50%;">Name</th>
                                            <th data-col-seq="1" style="width: 50%;">Value</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($MCHelper_settings as $i => $val): ?>
                                            <tr data-key="<?= $i ?>">
                                                <td data-col-seq="0"><?= Settings::MCHELPER_TITLES[$val['key']] ?></td>
                                                <td data-col-seq="1"><?= $val['value'] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <table id="MCHelper_edit" class="kv-grid-table table table-bordered table-striped kv-table-wrap" style="display: none">
                                        <thead>
                                        <tr>
                                            <th data-col-seq="0" style="width: 50%;">Name</th>
                                            <th data-col-seq="1" style="width: 50%;">Value</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($MCHelper_settings as $i => $val): ?>
                                            <tr data-key="<?= $i ?>">
                                                <td data-col-seq="0"><?= Settings::MCHELPER_TITLES[$val['key']]  ?></td>
                                                    <td data-col-seq="1">
                                                        <div class="form-group">
                                                            <input type="text" id="<?= $val['key'] ?>" class="form-control" value="<?= $val['value'] ?>" placeholder="<?= Settings::MCHELPER_TITLES[$val['key']] ?>">
                                                        </div>
                                                    </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kv-panel-after">
                                    <button id="edit_MCHelper" class="btn btn-primary">Edit</button>
                                    <button id="save_MCHelper" class="btn btn-success" style="display: none">Save</button>
                                    <button id="read_MCHelper" class="btn btn-primary" style="display: none">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
</div>

<div class="modal fade" id="select-lang" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content content-block">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select language</h4>
            </div>
            <div class="modal-body">
                <p>
                    <select multiple="multiple" id="lang-list">
                    </select>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="assign-lang">Assign</button>
            </div>
        </div>
    </div>
</div>