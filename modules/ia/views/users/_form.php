<?php

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">User <?= ($model->isNewRecord ? 'create' : 'update') ?></h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php
            $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
            $columns = [
                'first_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter First Name...', 'maxlength' => 255]],
                'last_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Last Name...', 'maxlength' => 255]],
                'middle_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Middle Name...', 'maxlength' => 255]],
                'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email...', 'maxlength' => 129]],
                'phone' => ['type' => Form::INPUT_TEXT, 'options' => ['maxlength' => 32]],
                'photo' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Photo...', 'maxlength' => 255]],
            ];
        $form->field($model, 'phone')->widget(PhoneInput::className(), [
            'jsOptions' => [
                'preferredCountries' => ['ua', 'ru'],
            ]
        ]);
        echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => $columns

            ]);
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <div class="checkbox">
                            <label>
                                <?= Html::checkbox('system_admin', $system_admin, ['value' => 1]) . 'System admin' ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
    <?php
        echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        );
    ?>
    <?php ActiveForm::end(); ?>
    </div>
</div>