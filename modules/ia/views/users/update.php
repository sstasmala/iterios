<?php

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 */

$this->title = 'Update User: ' . ' ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
        'system_admin' => $system_admin
    ]) ?>

</div>
