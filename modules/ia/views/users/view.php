<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/users/add_to_tenant.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

$this->title = 'User ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$adminId = \app\helpers\RbacHelper::getAssignedUsers('system_admin');
?>
<div class="users-view">
    <div class="row">
        <div class="col-md-6">
            <?php if ($p = Yii::$app->request->get('p', false)): ?>
                <div class="alert alert-success">
                    User Password:
                    <b>
                        <?= $p ?>
                    </b>
                </div>
            <?php endif; ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">User info</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <?php
                        $attributes = [
                            'id',
                            'first_name',
                            'last_name',
                            'middle_name',
                            'email:email',
                            [
                                'attribute' => 'phone',
                                'valueColOptions' => ['class' => 'to-phone-convert']
                            ],
                            [
                                'attribute' => 'photo',
                                'format' => 'raw',
                                'value' => (!empty($model->photo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->photo, ['class' => 'img-circle', 'style' => 'height: 128px; width: 128px;']) : '(not set)')
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => ($model->status == \app\models\User::STATUS_DELETED ? '<span class="label label-danger">Inactive</span>' : '<span class="label label-success">Active</span>')
                            ],
                            [
                                'attribute' => 'rules_admin',
                                'label' => 'Rules admin',
                                'format' => 'raw',
                                'value' => (in_array($model->id, $adminId)) ? Html::a('OFF', ['users/unassign-user-role', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger']) : Html::a('ON', ['users/assign-user-role', 'id' => $model->id], ['class' => 'btn btn-xs btn-success']),
                            ],
                            [
                                'attribute' => 'last_login',
                                'valueColOptions' => ['class' => 'to-datetime-convert'],
                                'value' => (!empty($model->last_login) ? $model->last_login : '')
                            ],
                            [
                                'attribute' => 'created_at',
                                'valueColOptions' => ['class' => 'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'valueColOptions' => ['class' => 'to-datetime-convert']
                            ],
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $attributes,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
                <div class="box-footer">
                    <?= Html::a('Update', ['update?id='.$model->id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a($model->status == \app\models\User::STATUS_ACTIVE ? 'Block' : 'Unblock',
                        ['block?id='.$model->id],
                        [
                            'class' => 'btn btn-warning' . ($model->id == \Yii::$app->user->id ? ' disabled' : '')
                        ]) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?php
                $tenant_provider = new ArrayDataProvider([
                    'allModels' => $model->tenants,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                    //'sort' => []
                ]);

                $tenant_columns = [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->name, '/ia/tenants/view?id='.$model->id);
                        }
                    ],
                    [
                        'attribute' => 'owner_id',
                        'label' => 'Owner',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->owner->first_name . ' ' . $model->owner->last_name, '/ia/users/view?id='.$model->owner_id);
                        }
                    ],
                    [
                        'attribute' => 'language_id',
                        'label' => 'Language',
                        'value' => function ($model) {
                            return $model->language->name;
                        }
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'buttons' => [
                            'delete' => function ($url, $modal) use ($model) {
                                //if ($modal->status == \common\models\User::STATUS_ACTIVE)
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Yii::$app->params['baseUrl'].'/ia/users/delete-from-tenant?id='.$modal->id.'&user_id='.$model->id,
                                    [
                                        'data-method' => 'post',
                                        'title' => 'Delete',
                                        'aria-label' => 'Delete',
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Are you sure to delete this item?',
                                    ]);
                            }
                        ],
                        'template' => '{delete}'
                    ]
                ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $tenant_provider,
                'columns' => $tenant_columns,
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
                'export' => false,
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> Tenants </h3>',
                    'type' => 'info',
                    'before' => '<button class="btn btn-success" data-toggle="modal" data-target="#addTenantModal">Add</button>',
                    'after' => false,
                    'showFooter' => false
                ],
            ]) ?>
        </div>
    </div>
</div>

<div class="modal fade" id="addTenantModal" tabindex="-1" role="dialog" aria-labelledby="addTenantLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose tenant</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" name="user_id" value="<?= $model->id ?>">
                    <div>
                        <label class="control-label" for="tenant">Tenant</label>
                        <select id="tenants-list">
                        </select>
                    </div>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="addToTenant" type="button" class="btn btn-primary">Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->