<?php

use Auth0\SDK\API\Management\Users;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\User $searchModel
 */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php
        $columns = [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email:email',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'contentOptions' => ['class' => 'to-phone-convert'],
            ],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' =>  function ($model) {
                    return (!empty($model->photo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->photo, ['class' => 'img-circle', 'style' => 'height: 64px; width: 64px;']) : '(not set)');
                },
            ],
            [
                'attribute' => 'status',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->status ?
                        '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1 => 'Active', 0 => 'Inactive'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'minimumResultsForSearch' => '-1'],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'status']
            ],
            [
                'attribute' => 'system_admin',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->system_admin ?
                        '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1 => 'System admin', 0 => 'Not system admin'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'minimumResultsForSearch' => '-1'],
                ],
                'filterInputOptions' => ['placeholder' => 'System admin', 'id' => 'system_admin']
            ],
            [
                'attribute' => 'last_login',
                'contentOptions' => ['class' => 'to-datetime-convert'],
                'value' => function ($model) {
                    return (!empty($model->last_login) ? $model->last_login : ' ');
                }
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            Yii::$app->params['baseUrl'].'/ia/users/view?id='.$model->id,
                            [
                                'title' => 'View',
                                'aria-label' => 'View',
                                'data-pjax' => 0
                            ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->params['baseUrl'].'/ia/users/update?id='.$model->id,
                            [
                                'title' => 'Update',
                                'aria-label' => 'Update',
                                'data-pjax' => 0
                            ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            Yii::$app->params['baseUrl'].'/ia/users/delete?id='.$model->id,
                            [
                                'data-method' => 'post',
                                'title' => 'Delete',
                                'aria-label' => 'Delete',
                                'data-pjax' => 0,
                                'data-confirm' => 'Are you sure to delete this item?',
                            ]);
                    }
                ]
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Users</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         (true ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
