<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\TariffsFeature $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tariffs-feature-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'class' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=> Yii::$app->params['features'],'options' => ['placeholder' => 'Enter Class...','rows' => 6]],

//            'countable' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Countable...']],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
