<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TariffsFeature $searchModel
 */

$this->title = 'Tariffs Features';
$this->params['breadcrumbs'][] = $this->title;
$path = Yii::$app->params['baseUrl'];
?>
<div class="tariffs-feature-index">
    <?php
    $columns = [
        'id',
        'name',
        'class:ntext',
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'created_by',
            'format'=>'raw',
            'value' => function($model)use($path)
            {
                if($model->created_by!=null){
                    $user = \app\models\User::findOne(['id'=>$model->created_by]);
                    if($user!=null)
                        return '<a href="'.$path.'/ia/users/view?id='.$user->id.'">'.$user->email.'</a>';
                }
                return null;
            }
        ],
        [
            'attribute' => 'updated_by',
            'format'=>'raw',
            'value' => function($model)use($path)
            {
                if($model->updated_by!=null){
                    $user = \app\models\User::findOne(['id'=>$model->updated_by]);
                    if($user!=null)
                        return '<a href="'.$path.'/ia/users/view?id='.$user->id.'">'.$user->email.'</a>';
                }
                return null;
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}{update}{delete}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/ui-translations',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> UI Translations</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    //(true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['update-translations'], ['data-pjax'=>0, 'class' => 'btn btn-danger', 'title'=>'Refresh data from config']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-ui-tariffs-feature'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>
</div>
