<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TariffsFeature $model
 */

$this->title = 'Create Tariffs Feature';
$this->params['breadcrumbs'][] = ['label' => 'Tariffs Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-feature-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/tariffs-feature',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
