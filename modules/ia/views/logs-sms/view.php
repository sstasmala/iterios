<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\LogsSms $model
 */

$this->title = 'Log SMS #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs SMS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

function getAlphaName($model) {
    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_PERSONAL_TYPE)
        return !empty($model->alpha_name_id) ? $model->alphaName->value : '(not set)';

    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_SYSTEM_TYPE)
        return !empty($model->provider_id) ? $model->provider->system_aname : '(not set)';

    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_PUBLIC_TYPE)
        return !empty($model->provider_id) ? $model->provider->public_aname : '(not set)';

    return '(not set)';
}
?>
<div class="logs-sms-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Logs SMS info
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            'text:ntext',
                            'recipient',
                            'status',
                            'error_text:ntext',
                            [
                                'attribute' => 'provider_id',
                                'label' => 'Provider',
                                'format' => 'raw',
                                'value' => empty($model->provider_id) ? '(not set)' : Html::a($model->provider->name, '/ia/sms-system-providers/view?id='.$model->provider_id)
                            ],
                            [
                                'attribute' => 'country_id',
                                'label' => 'Country',
                                'format' => 'raw',
                                'value' => empty($model->country_id) ? '(not set)' : Html::a($model->country->name, '/ia/sms-countries/view?id='.$model->country_id)
                            ],
                            'alpha_name_type',
                            [
                                'attribute' => 'alpha_name_id',
                                'label' => 'Alpha name',
                                'value' => getAlphaName($model)
                            ],
                            [
                                'attribute' => 'created_at',
                                'contentOptions' => ['class' => 'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'contentOptions' => ['class' => 'to-datetime-convert']
                            ],
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
