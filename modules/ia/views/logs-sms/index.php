<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\LogsSms $searchModel
 */

$this->title = 'Logs SMS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logs-sms-index">
    <?php
        $columns = [
            'id',
            'text:ntext',
            'recipient',
            'status',
            'error_text:ntext',
            [
                'attribute' => 'provider_id',
                'label' => 'Provider',
                'format' => 'raw',
                'value' => function ($model) {
                    if (empty($model->provider_id))
                        return '(not set)';

                    return Html::a($model->provider->name, '/ia/sms-system-providers/view?id='.$model->provider_id);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SmsSystemProviders::find()
                    ->orderBy('name')
                    ->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'SMS Provider', 'id' => 'sms_providers']
            ],
            [
                'attribute' => 'country_id',
                'label' => 'Country',
                'format' => 'raw',
                'value' => function ($model) {
                    if (empty($model->country_id))
                        return '(not set)';

                    return Html::a($model->country->name, '/ia/sms-countries/view?id='.$model->country_id);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SmsCountries::find()
                    ->where('provider_id IS NOT NULL')->orderBy('name')
                    ->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'SMS Country', 'id' => 'sms_countries']
            ],
            'alpha_name_type',
            [
                'attribute' => 'alpha_name_id',
                'label' => 'Alpha name',
                'value' => function ($model) {
                    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_PERSONAL_TYPE)
                        return !empty($model->alpha_name_id) ? $model->alphaName->value : '(not set)';

                    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_SYSTEM_TYPE)
                        return !empty($model->provider_id) ? $model->provider->system_aname : '(not set)';

                    if ($model->alpha_name_type === \app\models\SmsSystemProviders::ANAME_PUBLIC_TYPE)
                        return !empty($model->provider_id) ? $model->provider->public_aname : '(not set)';

                    return '(not set)';
                }
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view} {update}'
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
