<?php

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryEmailTemplates $model
 */

$this->title = 'Create Delivery Email Template ' . (Yii::$app->controller->id === 'delivery-email-templates-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = ['label' => 'Delivery Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-email-templates-create">
    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang
    ]) ?>
</div>
