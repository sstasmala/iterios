<?php

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryEmailTemplates $model
 */

$this->title = 'Update Delivery Email Template ' . (Yii::$app->controller->id === 'delivery-email-templates-system' ? 'system' : 'public') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="delivery-email-templates-update">
    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang
    ]) ?>
</div>
