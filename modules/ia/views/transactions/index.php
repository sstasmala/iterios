<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Transactions $searchModel
 */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
$customers = \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(),'id','name')
?>
<div class="transactions-index">
    <?php
    $columns = [

        'id',
        [
            'attribute' => 'sum',
            'label' => 'Sum USD'
        ],
//        'action',
//        'tariff_id',
        [
            'attribute' => 'financial_operation_id',
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->financial_operation_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/financial-operations/view?id='. $model->financial_operation_id.'">'. $model->financial_operation_id.'</a>';
                return null;
            }
        ],
        [
            'attribute' => 'action',
            'filterInputOptions'=>['placeholder'=>'Action'],
            'filter'=>\app\models\Transactions::ACTIONS,
            'filterType'=>GridView::FILTER_SELECT2,
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'value' => function($model)
            {
                if($model->action !== null)
                    return \app\models\Transactions::ACTIONS[$model->action];
                return null;
            }
        ],
        [
            'attribute' => 'tariff_id',
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->tariff_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/tariffs/view?id='. $model->tariff->id.'">'. $model->tariff->name.'</a>';
                return null;
            }
        ],
        [
            'attribute' => 'tariff_order_id',
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->tariff_order_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/tariffs-orders/view?id='. $model->tariff_order_id.'">'. $model->tariff_order_id.'</a>';
                return null;
            }
        ],
        [
            'label' => 'Customer',
            'attribute' => 'tenant_id',
            'filter'=>$customers,
            'filterType'=>GridView::FILTER_SELECT2,
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'Customer'],
            'format'=>'raw',
            'value' => function($model)
            {
                if($model->tenant_id !== null)
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/customers/view?id='. $model->tenant->id.'">'. $model->tenant->name.'</a>';
                return null;
            }
        ],
        [
            'attribute' => 'date',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
//        [
//            'attribute' => 'created_at',
//            'contentOptions' => ['class' => 'to-datetime-convert']
//        ],
//        [
//            'attribute' => 'updated_at',
//            'contentOptions' => ['class' => 'to-datetime-convert']
//        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}{delete}'
        ]
    ];

    $lang_select ='';

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Transactions</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
//                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    //(true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['update-translations'], ['data-pjax'=>0, 'class' => 'btn btn-danger', 'title'=>'Refresh data from config']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['process'], ['data-pjax'=>0, 'class' => 'btn btn-info', 'title'=>'Process']).
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-transactions'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
