<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Transactions $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="transactions-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'sum' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Sum...']],

            'action' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Action...']],

            'tariff_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Tariff ID...']],

            'financial_operation_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Financial Operation ID...']],

            'tariff_order_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Tariff Order ID...']],

            'owner' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Owner...']],

            'date' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Date...']],

            'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

            'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
