<?php
/**
 * compose.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$this->registerJsFile('@web/admin/plugins/ckeditor/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/mailer/init.compose.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
?>
<div class="row mailer">
    <?php if(isset($error)):?>
        <?=$this->render('_error_page',['message'=>$error]);?>
    <?php else:?>
        <div class="col-md-3">
            <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer'?>" class="btn btn-primary btn-block margin-bottom">To Inbox</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php foreach ($folders as $k=>$f):?>
                            <li><a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($k)?>">
                                    <i class="fa fa-folder-o"> <?=$f['name']?></i>
                                    <?php if($unread[$k]>0) echo '<span class="label label-primary pull-right">'.$unread[$k].'</span>';?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Compose New Message</h3>
                </div>
                <!-- /.box-header -->
                <form action="<?=Yii::$app->params['baseUrl'].'/ia/mailer/send'?>" method="post">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                <div class="box-body">

                    <div class="form-group">
                        <input class="form-control" name="to" placeholder="To:">
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="subject" placeholder="Subject:">
                    </div>
                    <div class="form-group">
                    <textarea id="compose-textarea" name="body" class="form-control" style="height: 300px">
                    </textarea>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <div class="btn btn-default btn-file">-->
<!--                            <i class="fa fa-paperclip"></i> Attachment-->
<!--                            <input type="file" name="attachment">-->
<!--                        </div>-->
<!--                        <p class="help-block">Max. 32MB</p>-->
<!--                    </div>-->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
<!--                        <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                    </div>
<!--                    <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
                </div>
                </form>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
    <?php endif;?>

</div>

