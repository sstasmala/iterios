<?php
/**
 * index.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 *
 * @var $this \yii\web\View
 */
$this->title = 'Mailer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row mailer">
    <?php if(isset($error)):?>
        <?=$this->render('_error_page',['message'=>$error]);?>
    <?php else:?>
        <div class="col-md-3">
            <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer/compose'?>" class="btn btn-primary btn-block margin-bottom">Compose</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php foreach ($folders as $k=>$f):?>
                            <li class="<?=($k == $folder)?'active':''?>"><a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($k)?>">
                                    <i class="fa <?=($k == $folder)?'fa-folder-open-o':'fa-folder-o'?>"> <?=$f['name']?></i>
                                    <?php if($unread[$k]>0) echo '<span class="label label-primary pull-right">'.$unread[$k].'</span>';?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <?php if (isset($messages)):?>
            <?=$this->render('_mailbox',compact('folders','messages','folder','total','start','unread'));?>
        <?php endif;?>
        <?php if (isset($message)):?>
            <?=$this->render('_read_email',compact('message'));?>
        <?php endif;?>
    <?php endif;?>

</div>
