<?php
/**
 * _read_email.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$data = '';
$types = $message->message->types;
$is_html = false;
if(in_array('html',$types))
    if($message->message->html->body!="") {
        $data = $message->message->html->body;
        $is_html = true;
    }
if(!$is_html)
    foreach ($types as $type)
        if($message->message->$type->body!=''){
            $data = $message->message->$type->body;
            break;
        }
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}
?>
<script>
    function resizeIframe(obj) {
        var height = obj.contentWindow.document.body.scrollHeight;
        if(height<30)
            height = 30;
        obj.style.height = height+50 + 'px';
    }
</script>
<div class="col-md-9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Read Mail</h3>

            <div class="box-tools pull-right">
<!--                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>-->
<!--                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>-->
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="mailbox-read-info">
                <h3><?=\yii\helpers\Html::encode(isset($message->header->subject)?$message->header->subject:"");?></h3>
                <h5>From: <?=\yii\helpers\Html::encode($message->header->from);?>

                    <span class="mailbox-read-time pull-right"><?=\yii\helpers\Html::encode($message->header->date)?></span></h5>
                <?=(isset($message->header->to)?'<h5>To: '.\yii\helpers\Html::encode($message->header->to).'</h5>':'')?>
            </div>
            <!-- /.mailbox-read-info -->
<!--            <div class="mailbox-controls with-border text-center">-->
<!--                <div class="btn-group">-->
<!--                    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">-->
<!--                        <i class="fa fa-trash-o"></i></button>-->
<!--                    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">-->
<!--                        <i class="fa fa-reply"></i></button>-->
<!--                    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">-->
<!--                        <i class="fa fa-share"></i></button>-->
<!--                </div>-->
<!--                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">-->
<!--                    <i class="fa fa-print"></i></button>-->
<!--            </div>-->
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
                <?php if($is_html):?>
                <iframe srcdoc="<?=\kartik\helpers\Html::encode($data)?>" style="width: 100%;" frameborder="0" scrolling="no" onload="resizeIframe(this)"></iframe>
                <?php else:?>
                <p><?=$data?></p>
                <?php endif;?>
            </div>
            <!-- /.mailbox-read-message -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
                <?php foreach ($message->attachments as $attachment):?>
                <li>
                    <?php if($attachment->info->structure->type==5):?>
                    <span class="mailbox-attachment-icon has-img"><img src="data:image/<?=$attachment->info->structure->subtype?>;base64,<?=$attachment->info->body?>" alt="<?=$attachment->name?>"></span>
                    <?php else:?>
                        <span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
                    <?php endif;?>
                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment->name?></a>
                        <span class="mailbox-attachment-size">
                          <?=formatSizeUnits($attachment->info->structure->bytes)?>
                          <a href="data:application/octet-stream;base64,<?=$attachment->info->body?>" download="<?=$attachment->name?>"
                             title="<?=$attachment->name?>" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                    </div>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
        <!-- /.box-footer -->
        <div class="box-footer">
<!--            <div class="pull-right">-->
<!--                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>-->
<!--                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>-->
<!--            </div>-->
<!--            <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>-->
<!--            <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>-->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /. box -->
</div>
