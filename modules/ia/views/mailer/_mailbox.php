<?php
/**
 * _mailbox.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
?>

<!-- /.col -->
<div class="col-md-9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?=$folders[$folder]['name']?></h3>

            <div class="box-tools pull-right">
                <div class="has-feedback">
                    <input type="text" class="form-control input-sm" placeholder="Search Mail">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                    <?=1+($start*25)?>-<?=(($start*25)+25>$total?$total:($start*25)+25)?>/<?=$total?>
                    <div class="btn-group">
                        <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($folder)?>&start=<?=$start-1?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>
                        <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($folder)?>&start=<?=$start+1?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
            <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                    <tbody>
                    <?php foreach ($messages as $message):?>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td class="mailbox-star"><a href="#">
                                <i class="fa <?=($message->header->flagged)?'fa-star':'fa-star-o'?> text-yellow"></i></a></td>
                        <td class="mailbox-name">
                            <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer/read?folder='.urlencode($folder).'&message_id='.urlencode($message->header->msgno)?>">
                                <?=\yii\helpers\Html::encode($message->header->from)?></a></td>
                        <td class="mailbox-subject">
                            <?=($message->header->seen)?\yii\helpers\Html::encode(isset($message->header->subject)?$message->header->subject:"")
                                :'<b>'.\yii\helpers\Html::encode(isset($message->header->subject)?$message->header->subject:"").'</b>'?>
                        </td>
                        <td class="mailbox-attachment"><?=(!empty($message->attachments))?'<i class="fa fa-paperclip"></i>':''?></td>
                        <td class="mailbox-date"><div class="calc-diff" data-time="<?=$message->header->udate?>"><?=$message->header->udate?></div></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <!-- /.table -->
            </div>
            <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                    <?=1+($start*25)?>-<?=(($start*25)+25>$total?$total:($start*25)+25)?>/<?=$total?>
                    <div class="btn-group">
                        <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($folder)?>&start=<?=$start-1?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>
                        <a href="<?=Yii::$app->params['baseUrl'].'/ia/mailer?folder='.urlencode($folder)?>&start=<?=$start+1?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
        </div>
    </div>
    <!-- /. box -->
</div>
