<?php
/**
 * _error_page.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
?>
<div class="col-md-12">
    <div class="callout callout-danger">
        <?=$message?>
    </div>
</div>
