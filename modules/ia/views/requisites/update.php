<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Requisites $model
 */

$this->title = 'Update Requisite: ' . ' ' . $country;
$this->params['breadcrumbs'][] = ['label' => 'Requisites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $country, 'url' => ['view', 'id' => $countryId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requisites-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
<!--                <div class="box-header with-border">
                </div>-->
                    <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
//        'models' => $models,
    ]) ?>
                    </div>
                </div>
        </div>

        <div class="col-md-6">
            <div class="box box-primary">
<!--                                <div class="box-header with-border">-->
<!--                                </div>-->
                <div class="box-body" id="add-field-block">
                    <ul id="sortable">
                        <?= $this->render('_additional', [
                            'model' => $model,
                            'groups' => $groups,
                            'fields' => $fields,
//                            'models' => $models,
                        ]) ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

<style>
    .btn.btn-primary {
        margin-top: 20px;
    }

    .none-add {
        display:none;
    }
</style>