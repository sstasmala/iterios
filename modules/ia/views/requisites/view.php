<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Requisites $model
 */

$this->title = $country;
$this->params['breadcrumbs'][] = ['label' => 'Requisites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-view">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Requisites Fields</h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive kv-detail-view">
                <table id="w0" class="table table-hover table-bordered table-striped detail-view" data-krajee-kvdetailview="kvDetailView_938deba0">
                    <tbody>
                    <tr>
                        <th class="country">Country name</th>
                        <td>
                            <div class="kv-attribute">
                                <?= $country ?>
                            </div>
                        </td>
                    </tr>
                    <?php foreach ($groups as $group): ?>
                    <tr>
                        <th class="group-name">Group Name</th>
                        <td>
                            <?= $group ?>
                        </td>
                    </tr>
                    <?php  foreach ($fields[$group] as $field): ?>
                    <tr>
                        <th class="field-name">Field Name</th>
                        <td>
                            <div class="kv-attribute">
                                <?= $field ?>
                            </div>
                        </td>
                    </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            <?= Html::a('Update', ['update?id='.$countryId], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $countryId], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

</div>

<style>
th {
    width: 20%;
    text-align: right;
    vertical-align: middle;
}
.country {
    color: blue;
}
.group-name {
    color: forestgreen;
}
.field-name {

}
</style>
