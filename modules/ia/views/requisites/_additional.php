<?php
/**
 * _additional.php
 * @copyright ©ita
 * @author Vlad Savelyev vladsova777@gmail.com
 */
use yii\helpers\Html;

$sortableId = 0;
?>
<script>
    var sortableIds = [];
</script>
<?php foreach ($groups as $groupId => $groupName): ?>
<ul id="sortable<?= $sortableId ?>" data-group-id="<?= $groupId ?>" class="ui-sortable ui-state-default ui-sortable-<?= $sortableId ?>">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <input type="hidden" name="RequisitesOld[requisites_group_id][]" value="<?= $groupId ?>">
    <span class="group-name"> Group Name: <?= $groupName ?> </span>
    <span style="float: right"><button class="btn btn-danger remove-group">X</button></span>
    <?php  foreach ($fields[$groupName] as $field): ?>
    <li class="nested-field">
        <span class="ui-icon ui-icon-arrowthick-2-n-s">
    </span><?= $field->requisitesField->name ?><input type="hidden"
      name="RequisitesOld[requisites_field_id][<?= $groupId ?>][]"
      value="<?= $field->requisitesField->id ?>">
        <input type="hidden" name="RequisitesOld[in_header][<?= $groupId ?>][<?= $field->requisitesField->id ?>]" value="0"><label>
        <input type="checkbox" name="RequisitesOld[in_header][<?= $groupId ?>][<?= $field->requisitesField->id ?>]" value="1" <?=Html::encode(($field->in_header?'checked':''))?> aria-invalid="false"> In Header</label>
    </li>
    <?php endforeach; ?>
</ul>
    <script>
        sortableIds.push(<?= $sortableId ?>);
    </script>
    <?php $sortableId++; ?>
<?php endforeach; ?>

