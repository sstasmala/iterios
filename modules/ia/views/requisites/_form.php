<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Requisites $model
 * @var yii\widgets\ActiveForm $form
 */




$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/requisites/requisites/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
//$this->registerJsFile('@web/admin/plugins/jquery-sortable/jquery-sortable-min.js',
//    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

?>

<div class="requisites-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>
        <?= $form->errorSummary($model)?>
        <label class="control-label col-md-1" for="contact_visa_country">Country</label>
        <div class="col-md-12">
        <select class="form-control m-bootstrap-select country-select-ajax"
                name="Requisites[country_id]" id="country-select" required>
            <?php if(!is_null($model['country_id'])):?>
                <option value="<?= $model['country_id'] ?>" selected="selected"><?= \app\helpers\MCHelper::getCountryById($model['country_id'],'en')[0]['title']?></option>
            <?php endif;?>
        </select>
        </div>
        <label class="control-label group-name-label col-md-1" for="contact_visa_country">Group name</label>
        <div id="field-need-add" class="col-md-12">
        <select class="form-control m-bootstrap-select country-select-ajax" id="requisites-field-id">
        </select>
        </div>
    <div id="to-form" class="col-md-12"></div>
    <div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            'id' => 'form-submit'
        ]
    ) ?>
    <?= Html::button($model->isNewRecord ? Yii::t('app', 'Add Group Field To Country') : Yii::t('app', 'Add Group Field To Country'),
        [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success',
                'id' => 'add-country'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<style>
.control-label {
    text-align: left !important;
}
.btn-success,
.btn-primary {
        margin-top: 20px;
    }

.group-name-label {
    width: 140px;
    height: 28px;
}

.ui-sortable li {
    margin: 8px 3px 5px 3px;
    padding: 3px;
    padding-left: 1.7em;
    font-size: 14px;
    height: 34px;
    margin-left: 10px;
}
.ui-sortable li span {
    position: absolute;
    margin-left: -1.3em;
}
.ui-icon-arrowthick-2-n-s {
    display: inline-table;
    background-position: -128px -46px;
}
.nested-field {
    background-color: #ccc;
}
.group-name {
        font-size: 17px;
}

.ui-sortable {
    list-style-type: none;
    margin: 0;
    margin-bottom: 10px;
    padding: 0;
    width: 100%;
}
</style>