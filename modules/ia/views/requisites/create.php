<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Requisites $model
 */

$this->title = 'Create Requisite';
$this->params['breadcrumbs'][] = ['label' => 'Requisites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script>
    var sortableIds = []
</script>
<div class="requisites-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <!--                <div class="box-header with-border">
                                </div>-->
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-primary">
                <!--                <div class="box-header with-border">
                                </div>-->
                <div class="box-body" id="add-field-block">
                    <ul id="sortable">
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
