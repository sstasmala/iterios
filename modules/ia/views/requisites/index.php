<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Requisites $searchModel
 */

$this->title = 'Requisites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-index">
    <?php
    $columns = [
//        'id',
        [
            'attribute' => 'country_id',
            'label' => 'Country Name',
            'value' => function ($model) {
                return (!empty($model->country_id) ? \app\helpers\MCHelper::getCountryById($model->country_id, 'en')[0]['title'] : 'not set');
            }
        ],
//        [
//            'attribute' => 'requisites_group_id',
//            'label' => 'Requisites Group Name',
//            'value' => function ($model) {
//                return (!empty($model->requisites_group_id) ? $model->requisitesGroup->name: 'not set');
//            }
//        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                        Yii::$app->params['baseUrl'].'/ia/requisites/view?id='.$model['country_id'],
                        [
                            'title' => 'View',
                            'aria-label' => 'View',
                            'data-pjax' => 0
                        ]);
                },
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        Yii::$app->params['baseUrl'].'/ia/requisites/update?id='.$model['country_id'],
                        [
                            'title' => 'Update',
                            'aria-label' => 'Update',
                            'data-pjax' => 0
                        ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        Yii::$app->params['baseUrl'].'/ia/requisites/delete?id='.$model['country_id'],
                        [
                            'data-method' => 'post',
                            'title' => 'Delete',
                            'aria-label' => 'Delete',
                            'data-pjax' => 0,
                            'data-confirm' => 'Are you sure to delete this item?',
                        ]);
                }
            ]
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Requisites</h3>',
                'before' => '',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>
</div>
