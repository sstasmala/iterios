<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsCourse $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="notifications-course-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'send_type' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Send Type...']],

            'user_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter User ID...']],

            'user_type' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter User Type...']],

            'send_status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Send Status...']],

            'noting_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Noting ID...']],

            'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

            'date' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Date...', 'maxlength' => 255]],

            'time' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Time...', 'maxlength' => 255]],

            'day' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Day...', 'maxlength' => 255]],

            'timezone_date' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Timezone Date...', 'maxlength' => 255]],

            'timezone_time' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Timezone Time...', 'maxlength' => 255]],

            'timezone_day' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Timezone Day...', 'maxlength' => 255]],

            'rule_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Rule ID...', 'maxlength' => 255]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
