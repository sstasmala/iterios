<?php

use app\models\Contacts;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NotificationsCourse $searchModel
 */

$this->registerJs("jQuery(document).ready(function(){
        jQuery('.forse-start').on('click', function(){
            $.ajax({
                url: baseUrl + '/ia/notifications-course/forse',
                method: 'get',
                complete: function() {
                    location.reload();
                }
            });
        });
    });", \yii\web\View::POS_END);

$this->title = 'Reminder Queue';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-course-index">
    <?php

        $columns = [
            'id',
            //'send_type',
            [
                'attribute' => 'user_id',
                'header' => 'Contact',
                'value' => function($model){
                    if ($model->user_type == 1) {
                        return $model->user_id;
                    } else {
                        return 0;
                    }
                },
                'contentOptions' => ['width' => '10px']
            ],
            [
                'attribute' => 'user_id',
                'header' => 'Agent',
                'value' => function($model){
                    if (!isset($model->user_id) || empty($model->user_id))
                        return '';

                    if ($model->user_type == 0) {
                        return $model->user_id;
                    } else {
                        $contact = Contacts::find()->where(['id' => $model->user_id])->one();
                        if (empty($contact))
                            return '';

                        return $contact->responsible;
                    }
                },
                'contentOptions' => ['width' => '10px']
            ],
            [
                'attribute' => 'tenant_id',
                'header' => 'Tenant',
                'value' => function($model){

                    return (int)$model->tenant_id;
                },
                'contentOptions' => ['width' => '10px']
            ],
            'date',
            //'time', 
            //'day', 
            'timezone_date', 
            //'timezone_time', 
            //'timezone_day', 
            [
                'attribute' => 'is_task',
                'format' => 'raw',
                'value' => function($model){
                    $errors = (empty($model->errors)) ? [] : Json::decode($model->errors);
                    $values = [];
                    foreach($errors as $erkey => $error)
                        if ($erkey == 'is_task')
                            return (empty($error)) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">error</span>';

                    return '';
                },
                'filter' => [
                    'true' => 'true',
                    'error' => 'error',
                ]
            ],
            [
                'attribute' => 'is_notification',
                'format' => 'raw',
                'value' => function($model){
                    $errors = (empty($model->errors)) ? [] : Json::decode($model->errors);
                    foreach($errors as $erkey => $error)
                        if ($erkey == 'is_system_notification')
                            return (empty($error)) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">error</span>';

                    return '';
                },
                'filter' => [
                    'true' => 'true',
                    'error' => 'error',
                ]
            ],
            [
                'attribute' => 'is_email',
                'format' => 'raw',
                'value' => function($model){
                    $errors = (empty($model->errors)) ? [] : Json::decode($model->errors);
                    foreach($errors as $erkey => $error)
                        if ($erkey == 'is_email')
                            return (empty($error)) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">error</span>';

                    return '';
                },
                'filter' => [
                    'true' => 'true',
                    'error' => 'error',
                ]
            ],
            [
                'attribute' => 'errors',
                'format' => 'raw',
                'value' => function($model){
                    $errors = (empty($model->errors)) ? [] : Json::decode($model->errors);
                    
                    $values = [];
                    foreach($errors as $erkey => $error)
                        if (!empty($error))
                            $values[] = $erkey.':'.$error;

                    return implode(',', $values);
                },
            ],
            [
                'attribute' => 'send_status',
                'format' => 'raw',
                'value' => function($model){
                    $errors = (empty($model->errors)) ? [] : Json::decode($model->errors);
                    
                    $values = [];
                    $noerror = 0;
                    foreach($errors as $erkey => $error) {
                        if (!empty($error)) {
                            $values[] = $erkey.':'.$error;
                        } else {
                            $noerror++;
                        }
                    }

                    if ($model->send_status == 0 && $noerror)
                        return '<span class="label label-warning">partial</span>';
                    

                    return ($model->send_status == 1) ? '<span class="label label-success">send</span>' : '<span class="label label-danger">no send</span>';
                },
                'filter' => [
                    1 => 'send',
                    0 => 'no send',
                ]
            ], 
            'rule_id', 
            [
                'attribute' => 'noting_id',
                'header' => 'Reminder Id',
                'format' => 'raw',
                'value' => function($model){
                    return '<a href="/ia/reminder-constructor/update?id='.$model->noting_id.'" target="_blank">'.$model->noting_id.'</a>';
                },
            ],

            [
                'attribute' => 'created_at',
                //'contentOptions' => ['class' => 'to-datetime-convert']
                'format' => 'raw',
                'value' => function($model){
                    return date('Y-m-d H:i');
                }
            ],
            [
                'attribute' => 'logs',
                'header' => 'logs by course',
                'format' => 'raw',
                'value' => function($model){
                    return '<a href="/ia/reminder-log?NotificationsLog[course_id]='.$model->id.'" target="_blank">log</a>';
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view} {update} {delete}'
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                        Html::button('Forse Start', ['class' => 'btn btn-default forse-start']),
                        Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
