<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsCourse $model
 */

$this->title = 'Update Reminder Course: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reminder Queue', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

echo $condition_orders;exit();
?>
<div class="notifications-course-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
