<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesFieldsDefault $model
 */

$this->title = 'Field id#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Services Fields Defaults', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-fields-default-view">

    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'attributes' => [
            'id',
            'code',
            'type',
            'is_additional:boolean',
            'in_header:boolean',
            'is_default:boolean',
            'variables',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
        'enableEditMode' => true,
    ]) ?>
    <?= Html::a('Update', ['update?id='.$model->id], ['class' => 'btn btn-success'])?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ])
    ?>

</div>
