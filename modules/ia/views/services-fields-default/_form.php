<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesFieldsDefault $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerJsFile('@web/admin/js/pages/services-fields-default/variables.js', ['depends' => \app\assets\AdminAsset::class]);
$fields_count = count(\app\models\search\ServicesFieldsDefault::find()->where(['is_default' => 1])->all());
$position_list  = range(1,$fields_count+1);
$position_list = array_combine($position_list,$position_list);
?>

<div class="services-fields-default-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 250]],

            'code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Code...', 'maxlength' => 250]],

            'type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \app\models\ServicesFieldsDefault::TYPES, 'options' => ['placeholder' => 'Enter Type...']],
//            'position' => [
//                    'type' => Form::INPUT_DROPDOWN_LIST,
//                'items' => $position_list,
//                'options' => ['placeholder' => 'Enter Position...']],

            'size' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12],
                'options' => ['placeholder' => 'Enter Size...']],

//            'is_additional' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Is Additional...']],

            'in_header' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter In Header...']],
            'only_for_order' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter In Header...']],

            'in_creation_view' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter In Header...']],
            'is_bold' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter In Header...']],
        ]

    ]);
    echo $form->field($model, 'variables', ['template' => '{input}','inputOptions'=>['class'=>'variables-input']])->hiddenInput();
    echo '<div class="variants-box col-md-offset-2 col-md-10"></div>';
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
