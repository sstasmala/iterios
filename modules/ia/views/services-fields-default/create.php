<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesFieldsDefault $model
 */

$this->title = 'Create Services Fields Default';
$this->params['breadcrumbs'][] = ['label' => 'Services Fields Defaults', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-fields-default-create">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/services-fields-default',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
