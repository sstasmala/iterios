<?php
/**
 * sort.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
$this->title = 'Sorty Services Fields Default';
$this->params['breadcrumbs'][] = ['label' => 'Services Fields Defaults', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$js = <<<JS
$( "#fields-set" ).sortable({
        stop:function (event, ui) {
            console.log('hey');
            $('.service-field').each(function (index) {
                console.log(index);
                $(this).find('input[name*="Fields"]').val(index);
            });
        },
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        }
    });
JS;

$this->registerJs($js,\yii\web\View::POS_END);
?>

<div class="services-fields-default-sort">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body body-info">
                    <form action="update-positions" method="post">
                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
                        <ul  style="list-style-type: none;padding: 0;text-align: center;" id="fields-set" class="sortable ui-sortable">
                            <?php
                            $models = \app\models\ServicesFieldsDefault::find()->where(['is_default'=>1])->orderBy('position')->all();
                            $i = 1;
                            foreach ($models as $model):
                                ?>
                                <li class="service-field callout ui-sortable-handle" style="border: 5px solid #eee;background-color: white;color: black">
                                    <input type="hidden" name="Fields[<?=$model->id?>]" value="<?=$i?>"><?=$model->code?>
                                </li>
                                <?php $i++; endforeach;?>
                        </ul>
                        <button type="submit" class="btn btn-info">Save</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>