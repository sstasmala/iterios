<?php

use app\helpers\MCHelper;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 */

$this->registerJsFile('@web/admin/js/pages/system-holidays/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

//$months = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
$months = array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May", "6"=>"June", "7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December");

$dayMounth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

$country = !empty($model->country_id) ? MCHelper::getCountryById($model->country_id, $def_lang) : 0;

?>
<script type="text/javascript">
    var dayMounth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var tenant_lang = '<?= $def_lang->iso; ?>';
</script>


<div class="suppliers-form">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'name'); ?>

            <div class="form-group field-systemholidays-country-id">
                <label class="control-label col-md-2" for="systemholidays-country-id">Country</label>
                <div class="col-md-10">
                    <select name="SystemHolidays[country_id]" id="search_country" name="Request[countries]">
                        <?php if ($country): print_r($country); ?>
                            <option value="<?= $model->country_id; ?>" selected="selected"><?= $country[0]['title']; ?></option>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group field-systemholidays-mounts has-success">
                <label class="control-label col-md-2" for="systemholidays-mounts">Mounth</label>
                <div class="col-md-10">
                    <select name="SystemHolidays[mounts]" id="HolidaysMounts" class="form-control">
                        <?php foreach ($months as $key=>$month) {
                            if (isset($model->mounts))
                                if ($model->mounts == $key)
                                    echo "<option value='{$key}' selected>{$month}</option>";
                                else
                                    echo "<option value='{$key}'>{$month}</option>";
                        } ?>
                    </select>
                </div>
            </div>

            <div class="form-group field-systemholidays-day has-success">
                <label class="control-label col-md-2" for="systemholidays-day">Day</label>
                <div class="col-md-10">
                    <select name="SystemHolidays[day]" id="HolidaysDay" class="form-control">
                        <?php
                        if($model->mounts){
                            for ($i=1;$i<=$dayMounth[$model->mounts];$i++) {

                                if (isset($model->mounts) && $model->day == $i)
                                    echo "<option value='{$i}' selected>{$i}</option>";
                                else
                                    echo "<option value='{$i}'>{$i}</option>";
                            }
                        }else{
                            for ($i=1;$i<=31;$i++) {

                                if (isset($model->mounts) && $model->day == $i)
                                    echo "<option value='{$i}' selected>{$i}</option>";
                                else
                                    echo "<option value='{$i}'>{$i}</option>";
                            }
                        }
                             ?>
                    </select>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <?php
            echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            );
            ?>
            <?php ActiveForm::end(); ?>
        </div>
</div>