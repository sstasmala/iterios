<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CompaniesKindActivities $model
 */

$this->title = 'Update Companies Kind Activities: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Companies Kind Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="companies-kind-activities-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/companies-kind-activities',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
