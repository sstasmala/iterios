<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\PaymentGateways $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/payment-gateways/payment-gateways.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
?>

<div class="payment-gateways-form">

    <?php if(!empty($model->logo)):?>
        <div class="row" style="margin-bottom: 10px"><div class="col col-md-12" style="text-align: center;"><img src="<?=Yii::$app->params['baseUrl']?>/<?=$model->logo?>"></div></div>
    <?php endif;?>
    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'demo' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Demo...']],

            'logo' => ['type' => Form::INPUT_FILE, 'options' => ['placeholder' => 'Enter Logo...','rows' => 6]],
            'type' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items'=> \app\models\PaymentGateways::TYPES,
                'options' => ['placeholder' => 'Enter Type']],

            'countries' => ['type' => Form::INPUT_HIDDEN, 'options' => ['placeholder' => 'Enter Countries...','rows' => 6]],

            'currencies' => ['type' => Form::INPUT_HIDDEN, 'options' => ['placeholder' => 'Enter Currencies...','rows' => 6]],

        ]

    ]);

    $providers = Yii::$app->params['payment_providers'];
    ?>

    <div class="row" <?=($model->provider == null)?'style="display:none"':''?>>
        <div class="col-sm-12">
            <div class="form-group field-paymentgateways-provider">
                <label class="control-label col-md-2" for="paymentgateways-provider">Provider</label>
                <div class="col-md-10">
                    <select id="paymentgateways-provider" class="form-control" name="PaymentGateways[provider]" placeholder="Enter Type" aria-invalid="false">
                        <?php foreach ($providers as $k => $v):?>
                        <option value="<?=$k?>" <?=($k == $model->provider)?'selected':''?>><?=$k?></option>
                        <?php endforeach;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <?=$form->field($model,'provider_config')->textInput()->widget(\kdn\yii2\JsonEditor::className(), ['clientOptions' => ['modes' => ['code', 'tree']]])?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2">Document template</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax" name="PaymentGateways[document_template_id]"
                            id="document-select" placeholder="Select template...">
                        <?php if($model->document_template_id != null):?>
                            <option value="<?=$model->document_template_id?>" selected><?= $model->documentTemplate->title?></option>
                        <?php endif;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2">Countries</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="country-select" multiple placeholder="Enter Country...">

                        <?php $countries = json_decode($model->countries); if($countries == null) $countries = []; ?>
                        <?php foreach ($countries as $country):?>
                            <option value="<?=$country?>" selected><?= \app\helpers\MCHelper::getCountryById($country,'en')[0]['title']?></option>
                        <?php endforeach;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2">Currencies</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="currency-select" multiple placeholder="Enter currency...">

                        <?php $currencies = json_decode($model->currencies); if($currencies == null) $currencies = []; ?>
                        <?php foreach ($currencies as $currency):?>
                            <option value="<?=$currency?>" selected><?= \app\helpers\MCHelper::getCurrencyById($currency,'en')[0]['iso_code']?></option>
                        <?php endforeach;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
