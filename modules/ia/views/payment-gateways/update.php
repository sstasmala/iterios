<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\PaymentGateways $model
 */

$this->title = 'Update Payment Gateways: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Payment Gateways', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-gateways-update">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
