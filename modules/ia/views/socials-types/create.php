<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SocialsTypes $model
 */

$this->title = 'Create Socials Types';
$this->params['breadcrumbs'][] = ['label' => 'Socials Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socials-types-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
