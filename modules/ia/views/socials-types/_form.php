<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SocialsTypes $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="socials-types-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'value' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...', 'maxlength' => 200]],
            'icon' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...', 'maxlength' => 200]],

//            'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],
//
//            'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],
//
//            'created_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created By...']],
//
//            'updated_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated By...']],

        ]

    ]); ?>

    <?= !empty($model->img) ? '<label class="control-label col-md-2">Image</label><img class="help-block img-rounded" src="/'.$model->img.'" height="100px">': ''?>

    <?= $form->field($model, 'img')->fileInput(); ?>


<?php

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
