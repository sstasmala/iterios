<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SocialsTypes $model
 */

$this->title = 'Update Socials Types: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Socials Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="socials-types-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
