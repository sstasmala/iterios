<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

use app\models\BaseNotifications;


/**
 * @var yii\web\View $this
 * @var app\models\BaseNotifications $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerAssetBundle(\app\assets\DatepickerAsset::className());
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerCssFile('@web/admin/plugins/jquery-builder/query-builder.dark.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-builder/query-builder.standalone.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

// CKEditor
$this->registerJsFile('@web/admin/plugins/ckeditor/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/base-notifications/ckeditor-init.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//

$this->registerJsFile('@web/admin/js/pages/base-notifications/lang_form.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/base-notifications/builder.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$this->registerJs('var template_id = '.($model->isNewRecord ? 'null':$model->id).';',\yii\web\View::POS_HEAD);
if(is_null($model->trigger_time))
    $model->trigger_time = '00:00:00';

if(is_null($model->trigger_day))
    $model->trigger_day = 0;

?>

<div class="base-notifications-form">

    <?php $form = ActiveForm::begin();?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Reminder <?= ($model->isNewRecord ? 'Create' : 'Update') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box box-info" style="background-color: grey">
                    <div class="box-header with-border">
                        <h3 class="box-title">Conditions</h3>
                    </div>
                    <div class="box-body">
                        <?= $form->field($model, 'module', ['template' => '{label}{input}'])->dropdownList(BaseNotifications::MODULES); ?>


                        <h4>Select trigger</h4>    
                        <?= $form->field($model, 'trigger_action', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                        <div id="builder_trigger"></div>

                        <div class="row">
                            <?php foreach(BaseNotifications::TRIGGERS as $ind => $title): 
                                    if (!isset(BaseNotifications::TRIGGERS_PLACEHOLDERS[$ind]))
                                        continue;
                            ?>
                            <div style="display:none;" class="<?= str_replace('.', '-', $ind); ?> placeholders col-md-5">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h3 class="box-title"><?= $title; ?></h3>
                                    </div>
                                    <div class="box-body">
                                        <p>
                                            <code>
                                                <?= implode('</code></p><p><code>', BaseNotifications::TRIGGERS_PLACEHOLDERS[$ind]); ?>
                                            </code>
                                        </p>
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <?= $form->field($model, 'condition', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                        <h4>Add condition trigger</h4>  
                        <div id="builder"></div>

                        

                        <?= $form->field($model, 'trigger', ['template' => '{label}{input}'])->dropdownList(
                            \app\models\BaseNotifications::TRIGGER_TYPES); ?>
                        <?= $form->field($model, 'trigger_day', ['template' => '{input}'])->textInput(['type' => 'number'])?>

                        <?= $form->field($model, 'trigger_time', ['template' => '{input}'])->textInput()?>
                        

                        <?= $form->field($model, 'is_email', ['template' => '{label}{input}'])->checkbox()?>
                        <?= $form->field($model, 'is_system_notification', ['template' => '{label}{input}'])->checkbox()?>
                        <?= $form->field($model, 'is_task', ['template' => '{label}{input}'])->checkbox()?>

                        <?= $form->field($model, 'subscribers', ['template' => '{label}{input}'])->dropdownList(
                            \app\models\BaseNotifications::SUBSCRIBERS); ?>
                    </div>
                </div>
                <div class="lang" data-lang="<?= $def_lang->iso ?>">
                    <div class="form-group field-base-notifications-name">
                        <label class="control-label" for="base-notifications-name">Name</label>
                        <input id="base-notifications-name" class="form-control"
                               name="BaseNotifications[name][<?= $def_lang->iso ?>]"
                               placeholder="Name..." value="<?= (isset($model->name) ? $model->name : '') ?>">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-base-notifications-subject">
                        <label class="control-label" for="base-notifications-subject">Subject</label>
                        <textarea id="base-notifications-subject" class="form-control"
                                  name="BaseNotifications[subject][<?= $def_lang->iso ?>]" rows="2"
                                  placeholder="Subject..."><?= (isset($model->subject) ? $model->subject : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-base-notifications-body">
                        <label class="control-label" for="base-notifications-body">Body</label>
                        <textarea id="base-notifications-body" class="form-control"
                                  name="BaseNotifications[body][<?= $def_lang->iso ?>]" rows="6"
                                  placeholder="Template..."><?= (isset($model->body) ? $model->body : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-base-notifications-system_notification">
                        <label class="control-label" for="base-notifications-system_notification">System notification</label>
                        <textarea id="base-notifications-system_notification" class="form-control"
                                  name="BaseNotifications[system_notification][<?= $def_lang->iso ?>]"  rows="6"
                                  placeholder="System notification..."><?= (isset($model->system_notification) ? $model->system_notification : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-base-notifications-sms">
                        <label class="control-label" for="base-notifications-sms">Sms</label>
                        <textarea id="base-notifications-sms" class="form-control"
                                  name="BaseNotifications[sms][<?= $def_lang->iso ?>]" rows="6"
                                  placeholder="Sms..."><?= (isset($model->sms) ? $model->sms : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-base-notifications-task_name">
                        <label class="control-label" for="base-notifications-task_name">Task Name</label>
                        <textarea id="base-notifications-task_name" class="form-control"
                                  name="BaseNotifications[task_name][<?= $def_lang->iso ?>]"  rows="1"
                                  placeholder="Task Name..."><?= (isset($model->task_name) ? $model->task_name : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-base-notifications-task_body">
                        <label class="control-label" for="base-notifications-task_body">Task Body</label>
                        <textarea id="base-notifications-task_body" class="form-control"
                                  name="BaseNotifications[task_body][<?= $def_lang->iso ?>]"  rows="6"
                                  placeholder="Task Body..."><?= (isset($model->task_body) ? $model->task_body : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>

                    <?= $form->field($model, 'is_slave_contact', ['template' => '{label}{input}'])->checkbox(); ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <?php ActiveForm::end(); ?>

    <div style="display:none;" class="condition-orders" data-condition-orders='<?= json_encode($condition_orders); ?>'></div>
</div>
