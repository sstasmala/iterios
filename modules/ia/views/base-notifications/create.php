<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\BaseNotifications $model
 */

$this->title = 'Create Reminder';
$this->params['breadcrumbs'][] = ['label' => 'Reminder Constructor list ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="base-notifications-create">
    <p>
        <select id="lang-list" data-action="create">
            <option selected="selected" value="<?= $def_lang->iso ?>"><?= $def_lang->name ?> (<?= $def_lang->original_name ?>)</option>
        </select>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang,
        'condition_orders' => $condition_orders
    ]) ?>
</div>
