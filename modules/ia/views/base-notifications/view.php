<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\BaseNotifications $model
 */

$this->title = 'Base notification info: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Base Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$gridColumn = [
    ['attribute' => 'id', 'visible' => false],
    'name',
    'subject',
    'body:ntext',
    'system_notification:ntext',
    'sms:ntext',
    'task_name',
    'task_body:ntext',
    'condition:ntext',
    'trigger',
    'trigger_time',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',

];
?>
<div class="base-notifications-view">
    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$this->title?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
            <div class="box-footer">
                <div class="col-md-4">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
</div>
