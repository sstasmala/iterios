<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableSmllTitle" style="">
    <tbody><tr>
        <td align="center" valign="top" style="padding-bottom: 10px;" class="smllTitle">
            <!-- Small Title Text // -->
            <p class="text" style="color:#777777; font-family:'Poppins', Helvetica, Arial, sans-serif; font-size:16px; font-weight:500; font-style:normal; letter-spacing:normal; line-height:22px; text-transform:none; text-align:center; padding:0; margin:0">
                <?= date('Y-m-d H:i', $due_date); ?>
            </p>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top" style="padding-bottom: 20px;" class="smllSubTitle">
            <!-- Info Title Text // -->
            <p class="text" style="color:#000000; font-family:'Poppins', Helvetica, Arial, sans-serif; font-size:18px; font-weight:500; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
                <?= $title; ?>
            </p>
        </td>
    </tr>
    </tbody>
</table>
