<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\BaseNotifications $model
 */

$this->title = 'Update Reminder: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reminder list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Reminder: '.$model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="base-notifications-update">
    <p>
        <select id="lang-list" data-action="update">
            <option selected="selected" value="<?= $def_lang->iso ?>"><?= $def_lang->name ?> (<?= $def_lang->original_name ?>)</option>
        </select>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang,
        'condition_orders' => $condition_orders
    ]) ?>
</div>
