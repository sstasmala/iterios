<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\BaseNotifications $searchModel
 */

$this->title = 'Reminder Constructor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="base-notifications-index">
    <?php
    $columns = [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'order' => DynaGrid::ORDER_FIX_LEFT
        // ],
        'id',
        'name',
        'module',
        [
            'attribute' => 'trigger',
            'format' => 'raw',
            'value' => function($model){
                $value = Json::decode($model->trigger_action);
                if (!isset($value['rules'][0]['id']))
                    return '';

                return $value['rules'][0]['id'];
            },
        ],

        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function($model){
                return ($model->status == 1) ? '<a class="label label-success" href="/ia/reminder-constructor/set-status?id='.$model->id.'&status=0">ON</a>' : '<a class="label label-danger" href="/ia/reminder-constructor/set-status?id='.$model->id.'&status=1">OFF</a>';
            },
        ],
//        'subject',
//        'body:ntext',
//        'system_notification:ntext',
//            'sms:ntext',
//            'task_config:ntext',
//            'condition:ntext',
//            'schedule:ntext',
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],

//            'created_by',
//            'updated_by',
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/reminder-constructor/view?id='.$model->id.'" title="View" aria-label="View" data-pjax="0">'.
                        '<span class="glyphicon glyphicon-eye-open"></span></a>';
                },
                'update' => function ($url, $model){
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/reminder-constructor/update?id='.$model->id.'" title="Update" aria-label="Update" data-pjax="0">'.
                        '<span class="glyphicon glyphicon-pencil"></span></a>';
                },
            ],
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/reminder-constructor',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-info',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Reminders list </h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']),
//                    (Yii::$app->user->can('reference.create') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create?type='.$type], ['class' => 'btn btn-success']) : '') .
//                    (Yii::$app->authManager->getAssignment('admin',\Yii::$app->user->id) ? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/templates/deleted-index?type='.$type], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Restore item page']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', '', ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-base-notifications'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end(); ?>

</div>
