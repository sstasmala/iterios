<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Suppliers Credentials: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers Credentials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-types-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">

                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'supplier_id',
                                'label' => 'Supplier',
                                'format' => 'raw',
                                'value' => !empty($model->supplier_id) ? $supplier->name : '(not set)',
                            ],
                            [
                                'attribute' => 'tenant_id',
                                'label' => 'Tenant',
                                'format' => 'raw',
                                'value' => null === $model->tenant ? '(not set)' : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank'])
                            ],
                            'params',
                            [
                                'attribute'=>'created_at',
                                'containerOptions'=>['class'=>'to-datetime-convert'],
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'created_by',
                                'label' => 'Created by',
                                'format' => 'raw',
                                'value' => (is_null($model->created_by)) ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']),
                            ],
                            [
                                'attribute' => 'updated_by',
                                'label' => 'Updated by',
                                'format' => 'raw',
                                'value' => (is_null($model->updated_by)) ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']),
                            ],
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
