<?php

use app\models\Suppliers;
use app\models\Tenants;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->registerJsFile('@web/admin/js/pages/suppliers-credentials/items.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

?>

<div class="suppliers-credentials-form">
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <?= $form->field($model, 'supplier_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Suppliers::find()->translate('en')->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose supplier'],
        ]); ?>

        <?= $form->field($model, 'tenant_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tenants::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose tenant'],
        ]); ?>
        <div id="params-body">
            <?php
            if (isset($model->params)) {
                $paramsDecode = json_decode($model->params, true);
                $inputParam = '';
                foreach ($paramsDecode as $key => $param) {
                    $inputParam .= '' .
                        '<div class="form-group">' .
                        '<label class="control-label col-md-2" for="supplierscredentials-params-' . $key . '">' . $key . '</label>' .
                        '<div class="col-md-10">' .
                        '<input type="text" id="supplierscredentials-params-' .
                        $key .
                        '" class="form-control" name="SuppliersCredentials[credentialParams][' .
                        $key .
                        ']" value="' . $param . '">' .
                        '</div>' .
                        '</div>';
                }
                echo $inputParam;

            };
            ?>
        </div>


    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        );
        ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>