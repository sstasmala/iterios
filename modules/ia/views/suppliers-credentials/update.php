<?php

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Update suppliers credentials: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers credentials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tasks-types-update">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
