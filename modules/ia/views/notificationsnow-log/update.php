<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsnowLog $model
 */

$this->title = 'Update Notificationsnow Log: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notificationsnow Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notificationsnow-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
