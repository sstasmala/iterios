<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsnowLog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="notificationsnow-log-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Status...']],

            'notification_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Notification ID...']],

            'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

            'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],

            'error' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Error...','rows' => 6]],

            'element' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Element...','rows' => 6]],

            'action' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Action...', 'maxlength' => 255]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
