<?php

use app\components\notifications_now\NotificationActions;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NotificationsnowLog $searchModel
 */

$this->title = 'Notification Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificationsnow-log-index">
    <?php
        $columns =  [
    //            'created_at',
    //            'updated_at',

                'id',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model){
                        return ($model->status == 1) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">error</span>';
                    },
                ],


                [
                    'attribute' => 'error',
                    'format' => 'raw',
                    'value' => function ($model){
                        return empty($model->error) ? '' : implode(',', Json::decode($model->error));
                    },
                ],
                [
                    'attribute' => 'Log Email',
                    'format' => 'raw',
                    'value' => function ($model){
                        if (empty($model->log_email_id))
                            return '';

                        return ($model->chanel == 'is_notice') ? $model->log_notice_id : '<a target="_blank" href="/ia/logs-public-email/view?id='.$model->log_email_id.'">log('. $model->log_email_id . ')</a>';
                    },
                ],
                [
                    'attribute' => 'notification_id',
                    'format' => 'raw',
                    'value' => function ($model){
                        return '<a href="/ia/notification-constructor/update?id='.$model->notification_id.'" target="_blank">'.$model->notification_id.'</a>';
                    },
                ],
                [
                    'attribute' => 'user_id',
                    'format' => 'raw',
                    'value' => function ($model){
                        return '<a href="/ia/users/view?id='.$model->user_id.'" target="_blank">User('.$model->user_id.')</a>';
                    },
                ],
                [
                    'attribute' => 'action',
                    'format' => 'raw',
                    'filter' => NotificationActions::ACTIONS_ADMIN,
                ],

                //'element:ntext',
                [
                    'attribute' => 'chanel',
                    'format' => 'raw',
                    'value' => function ($model){
                        return $model->chanel == 'is_notice' ? 'notice' : 'email';
                    },
                ],

                [
                    'attribute' => 'created_at',
                    'contentOptions' => ['class' => 'to-datetime-convert'],
                ],


                /*[
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                Yii::$app->urlManager->createUrl(['ia/notificationsnow-log/view', 'id' => $model->id, 'edit' => 't']),
                                ['title' => Yii::t('yii', 'Edit'),]
                            );
                        }
                    ],
                ],*/
        ];


        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>

                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
