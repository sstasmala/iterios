<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Tags $model
 */

$this->title = 'Tag ' . (Yii::$app->controller->id == 'tags-system' ? 'system' : 'custom') . ': ' . $model->value;
$this->params['breadcrumbs'][] = ['label' => 'Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tags-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            'id',
                            'value',
                            'type',
                            [
                                'attribute' => 'tenant_id',
                                'format' => 'raw',
                                'visible' => ($model->type == \app\models\Tags::TYPE_SYSTEM ? false : true),
                                'value' => is_null($model->tenant) ? '' : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank'])
                            ],
                            [
                                'attribute'=>'created_at',
                                'containerOptions'=>['class'=>'to-datetime-convert'],
                                'valueColOptions'=>['class'=>'to-datetime-convert'],
                                'value' => (is_null($model->created_at) ? '' : $model->created_at)
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert'],
                                'value' => (is_null($model->updated_at) ? '' : $model->updated_at)
                            ],
                            [
                                'attribute' => 'created_by',
                                'format' => 'raw',
                                'value' => (is_null($model->created_by)) ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                            ],
                            [
                                'attribute' => 'updated_by',
                                'format' => 'raw',
                                'value' => (is_null($model->updated_by)) ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                            ]
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
