<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\Tags $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tags-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'value' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...', 'maxlength' => 255]]
        ]
    ]);

    echo Html::hiddenInput('Tags[type]', (Yii::$app->controller->id == 'tags-system' ? \app\models\Tags::TYPE_SYSTEM : \app\models\Tags::TYPE_CUSTOM));

    if (Yii::$app->controller->id == 'tags-custom') {
        echo $form->field($model, 'tenant_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose Tenant'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    }

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
