<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use app\models\ProfileNotifications;
use app\components\notifications_now\NotificationActions;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ProfileNotifications $searchModel
 */

$this->title = 'Notification User';
$this->params['breadcrumbs'][] = $this->title;

$actions = NotificationActions::ACTIONS;
$actions_filter = [];
foreach($actions as $action => $title) {
    $actions_filter[$action] = $action;
}

?>
<div class="profile-notifications-index">
    <?php

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function($model) {
                    return '<a class="label label-warning" target="_blank" href="/ia/users/view?id='.$model->user_id.'">User (id:'.$model->user_id.')</a>';
                }
            ],
            [
                'attribute' => 'level',
                'format' => 'raw',
                'value' => function ($model){
                    return '<span class="label label-warning">'.ProfileNotifications::LEVELS_ADMIN[$model->level].'</span>';
                },
                'filter' => ProfileNotifications::LEVELS_ADMIN
            ],
            [
                'attribute' => 'is_email',
                'format' => 'raw',
                'value' => function ($model){
                    return ($model->is_email == 1) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">false</span>';
                },
                'filter' => [
                    0 => 'false',
                    1 => 'true'
                ]
            ],
            [
                'attribute' => 'is_notice',
                'format' => 'raw',
                'value' => function ($model){
                    return ($model->is_notice == 1) ? '<span class="label label-success">true</span>' : '<span class="label label-danger">false</span>';
                },
                'filter' => [
                    0 => 'false',
                    1 => 'true'
                ]
            ],
            [
                'attribute' => 'action',
                'format' => 'raw',
                'value' => function ($model){
                    return $model->action;
                },
                'filter' => $actions_filter
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['ia/profile-notifications/update', 'id' => $model->id, 'edit' => 't']),
                            ['title' => Yii::t('yii', 'Edit'),]
                        );
                    }
                ],
            ],
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
            ],
            'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
        ]);

        DynaGrid::end(); 
    ?>

</div>
