<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ProfileNotifications $model
 */

$this->title = 'Update Profile Notifications: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profile Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profile-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
