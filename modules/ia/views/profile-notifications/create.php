<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ProfileNotifications $model
 */

$this->title = 'Create Profile Notifications';
$this->params['breadcrumbs'][] = ['label' => 'Profile Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-notifications-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
