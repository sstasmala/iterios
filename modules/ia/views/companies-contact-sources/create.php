<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CompaniesContactSources $model
 */

$this->title = 'Create Companies Contact Sources';
$this->params['breadcrumbs'][] = ['label' => 'Companies Contact Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companies-contact-sources-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/companies-contact-sources',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
