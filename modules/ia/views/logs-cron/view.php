<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\LogsCron $model
 */

$this->title = 'Log Cron #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs Crons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logs-cron-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Log info
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            'id',
                            'command',
                            'status',
                            // 'output_file',
                            [
                                'attribute' => 'time_start',
                                'label' => 'Duration',
                                'value' => $model->duration
                            ],
                            [
                                'attribute' => 'memory_usage',
                                'label' => 'Memory Usage',
                                'value' => \app\helpers\PrintHelper::formatSizeUnits($model->memory_usage)
                            ],
                            [
                                'attribute' => 'created_at',
                                'valueColOptions' => ['class' => 'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'valueColOptions' => ['class' => 'to-datetime-convert']
                            ],
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    Log output
                </div>
                <div class="box-body">
                    <?php
                    $file = Yii::getAlias('@web') . $model->output_file;

                    if (file_exists($file)) {
                        echo '<pre style="background-color:#000000;">'.(new \SensioLabs\AnsiConverter\AnsiToHtmlConverter(null,true))->convert(file_get_contents($file)) . '</pre>';
                    } else {
                        echo '<pre>File does not exist or empty!</pre>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
