<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\LogsCron $searchModel
 */

$this->title = 'Logs Cron';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logs-cron-index">
    <?php
    $columns = [
        'id',
        'command',
        'status',
        // 'output_file',
        [
            'attribute' => 'time_start',
            'label' => 'Duration',
            'enableSorting' => false,
            'value' => function ($model) {
                return $model->duration;
            }
        ],
        [
            'attribute' => 'memory_usage',
            'label' => 'Memory Usage',
            'enableSorting' => false,
            'value' => function ($model) {
                return \app\helpers\PrintHelper::formatSizeUnits($model->memory_usage);
            }
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Date added',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'label' => 'Date updated',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {delete}'
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Logs Cron</h3>',
                'before' => '',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-all'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Delete logs', 'data-confirm' => 'Are you sure to delete logs?', 'data-method' => 'post'])
                ],
                ['content'=>
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],

                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
