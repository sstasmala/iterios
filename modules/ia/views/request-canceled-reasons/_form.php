<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\RequestCanceledReasons $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="request-canceled-reasons-form">

    <?php
        $attributes = [
            'type' => [
                'type' => Form::INPUT_HIDDEN,
                'options' => [
                    'value' => Yii::$app->controller->id == 'request-canceled-reasons-system' ? \app\models\RequestCanceledReasons::TYPE_SYSTEM : \app\models\RequestCanceledReasons::TYPE_OTHER
                ]
            ],
            'name' => [
                'type' => Yii::$app->controller->id == 'request-canceled-reasons-system' ? Form::INPUT_TEXT : Form::INPUT_HIDDEN,
                'options' => [
                    'placeholder' => 'Enter Name...',
                    'maxlength' => 255
                ]
            ]
        ];

        if (Yii::$app->controller->id != 'request-canceled-reasons-system') {
            $attributes['description'] = [
                'type'    => Form::INPUT_TEXTAREA,
                'options' => ['placeholder' => 'Enter Description...', 'rows' => 6]
            ];
        }
    ?>

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $attributes
    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
