<?php

/**
 * @var yii\web\View $this
 * @var app\models\RequestCanceledReasons $model
 */

$this->title = 'Create request canceled reason ' . (Yii::$app->controller->id == 'request-canceled-reasons-system' ? 'system' : 'other');
$this->params['breadcrumbs'][] = ['label' => 'Request Canceled Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-canceled-reasons-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
