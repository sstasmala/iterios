<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\RequestCanceledReasons $model
 */

$this->title = 'Request canceled reason ' . (Yii::$app->controller->id == 'request-canceled-reasons-system' ? 'system: '.$model->name : 'other #'.$model->id);
$this->params['breadcrumbs'][] = ['label' => 'Request Canceled Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-canceled-reasons-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <?php if (Yii::$app->controller->id == 'request-canceled-reasons-system'): ?>
                    <div class="box-header with-border">
                        <?php
                            $lang_select = $this->render('///layouts/admin/_language_select', [
                                'language' => $def_lang,
                                'action' => 'view',
                                'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                            ]);
                            echo $lang_select;
                        ?>
                    </div>
                <?php endif; ?>
                <div class="box-body">
                    <?php
                    $columns = [
                        'id',
                        'type',
                        [
                            'attribute' => 'name',
                            'visible' => (Yii::$app->controller->id != 'request-canceled-reasons-system' ? false : true)
                        ],
                        [
                            'attribute' => 'description',
                            'visible' => (Yii::$app->controller->id == 'request-canceled-reasons-system' ? false : true)
                        ],
                        [
                            'attribute'=>'created_at',
                            'containerOptions'=>['class'=>'to-datetime-convert'],
                            'valueColOptions'=>['class'=>'to-datetime-convert'],
                            'value' => (is_null($model->created_at) ? '' : $model->created_at)
                        ],
                        [
                            'attribute'=> 'updated_at',
                            'valueColOptions'=>['class'=>'to-datetime-convert'],
                            'value' => (is_null($model->updated_at) ? '' : $model->updated_at)
                        ],
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => (is_null($model->created_by)) ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => (is_null($model->updated_by)) ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                        ]
                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
