<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MessengersTypes $model
 */

$this->title = 'Create Messengers Types';
$this->params['breadcrumbs'][] = ['label' => 'Messengers Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messengers-types-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
