<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use app\models\ContactsEmails;
use app\models\ContactsPhones;
use app\models\ContactsSocials;

/**
 * @var yii\web\View $this
 * @var app\models\Contacts $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerJsFile('@web/admin/js/pages/contacts/items.js',['depends'=>\app\assets\AdminAsset::class]);
$emails_index = 0;
$phones_index = 0;
$messengers_index = 0;
?>
<style>
    .emails>.row, .phones>.row{
        margin-bottom: 10px;
    }
</style>
<div class="contacts-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Contact info
                </div>
                <div class="box-body">
                    <?=$form->field($model, 'first_name')->textInput(['value' => (isset($data['first_name']) ? $data['first_name'] : '')]);?>
                    <?=$form->field($model, 'middle_name')->textInput(['value' => (isset($data['middle_name']) ? $data['middle_name'] : '')]);?>
                    <?=$form->field($model, 'last_name')->textInput(['value' => (isset($data['last_name']) ? $data['last_name'] : '')]);?>
                    <?=$form->field($model, 'type')->dropDownList(['customer' => 'Customer','tourist' => 'Tourist','lid' => 'Lid','new' => 'New'],
                        ['value' => (isset($data['type']) ? $data['type'] : '')]);?>
                    <?=$form->field($model, 'gender')->dropDownList([1 => 'Male', 0 => 'Female'],
                        ['value' => (isset($data['gender']) ? $data['gender'] : '')]);?>
                    <?=$form->field($model, 'discount')->textInput(['value' => (isset($data['discount']) ? $data['discount'] : '')]);?>
                    <?=$form->field($model, 'tin')->textInput(['value' => (isset($data['tin']) ? $data['tin'] : '')]);?>
                    <?=$form->field($model, 'company_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->asArray()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Choose company',
                            'value' => (isset($data['company_id']) ? $data['company_id'] : '')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Company');?>
                    <?=$form->field($model, 'tenant_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Choose Tenant',
                            'value' => (isset($data['tenant_id']) ? $data['tenant_id'] : '')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Tenant');?>
                    <?=$form->field($model, 'responsible')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'email'),
                        'options' => [
                            'placeholder' => 'Choose Responsible',
                            'value' => (isset($data['responsible']) ? $data['responsible'] : '')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Responsible');?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?php
             $additionals = [
                 [
                     'id' => 'email',
                     'title' => 'Email',
                     'name' => 'ContactsEmails',
                     'items' => (isset($data['ContactsEmails']) ? $data['ContactsEmails'] : []),
                     'type' => 'create'
                 ],
                 [
                     'id' => 'phone',
                     'title' => 'Phone',
                     'name' => 'ContactsPhones',
                     'items' => (isset($data['ContactsPhones']) ? $data['ContactsPhones'] : []),
                     'type' => 'create'
                 ],
                 [
                     'id' => 'messenger',
                     'title' => 'Messenger',
                     'name' => 'ContactsMessengers',
                     'items' => (isset($data['ContactsMessengers']) ? $data['ContactsMessengers'] : []),
                     'type' => 'create'
                 ]
             ];
            ?>
            <?php foreach ($additionals as $additional):?>
                <?php
                    echo $this->render('_additional', $additional)
                ?>
            <?php endforeach;?>
        </div>
    <?php
    ?>
    </div>
    <?php
    echo Html::submitButton(empty($data['id']) ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => empty($data['id']) ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>

<?php
$this->registerJs('var additionals = '.json_encode($additionals).';',\yii\web\View::POS_BEGIN);
?>

<!-- Modals -->
<?php foreach ($additionals as $additional):?>
    <?php
        echo $this->render('_additional_modal', $additional)
    ?>
<?php endforeach;?>
