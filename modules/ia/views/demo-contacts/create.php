<?php

/**
 * @var yii\web\View $this
 * @var app\models\Contacts $model
 */

$this->title = 'Create Demo Contact';
$this->params['breadcrumbs'][] = ['label' => 'Demo Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
