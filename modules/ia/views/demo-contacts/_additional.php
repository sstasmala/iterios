<?php
/**
 * _additional.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 * @var $title string
 * @var $items array
 * @var $name string
 * @var $id
 */

$var = $id.'s_index';
${$var} = 0;
?>
<div class="row">
    <div class="box box-info">
        <div class="box-header with-border">
            <?=$title.'s'?>
        </div>
        <div class="box-body">
            <div class="<?=$id?>s">
                <?php foreach ($items as $item):?>
                    <div class="row" data-index="<?=${$var}?>">
                        <input class="<?=$id?>_type_id" type="hidden" value="<?=$item['type_id']?>" name="<?=$name?>[<?=${$var}?>][type_id]">
                        <input class="<?=$id?>_type_value" type="hidden" value="<?=$item['type_value']?>" name="<?=$name?>[<?=${$var}?>][type_value]">
                        <input class="<?=$id?>_id" type="hidden" value="<?=$item['id']?>" name="<?=$name?>[<?=${$var}?>][id]">
                        <input class="<?=$id?>_value" type="hidden" value="<?=$item['value']?>" name="<?=$name?>[<?=${$var}?>][value]">
                        <div class="col-md-4 text-left <?=$id?>-type">
                            <?=$item['type_value']?>
                        </div>
                        <div class="col-md-4 text-center <?=$id?>-value">
                            <?=$item['value']?>
                        </div>

                        <?php if ($type == 'create'): ?>
                            <div class="col-md-4 text-right">
                                <a data-index="<?=${$var}?>" class="btn btn-warning" data-toggle="modal" data-target="#<?=$id?>s-modal"><i class="fa fa-edit"></i></a>
                                <button data-id="<?=$item['id']?>" class="delete-item btn btn-danger"><i class="fa fa-trash"></i></button>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php ${$var}++; endforeach;?>
            </div>
            <?= ($type == 'create' ? '<a data-toggle="modal" data-target="#<?=$id?>s-modal" class="btn btn-success">Add <?=$title?></a>' : '')?>
        </div>
    </div>
</div>
<?php
$this->registerJs('var '.$var.' = '.${$var}.';',\yii\web\View::POS_BEGIN);
?>
