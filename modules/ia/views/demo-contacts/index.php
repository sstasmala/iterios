<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Contacts $searchModel
 */

$this->title = 'Demo data: Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">
    <?php
    $columns = [
        ['class' => 'yii\grid\SerialColumn'],
        // 'id',
        'last_name',
        'first_name',
        'middle_name',
        'date_of_birth',
        [
            'attribute' => 'gender',
            'value' => function ($model) {
                if ($model['gender'] === null)
                    return '(not set)';

                return ($model['gender'] == 1 ? 'Male' : 'Female');
            }
        ],
//            'discount',
//            'nationality',
//            'tin',
//            'type',
//            'company_id',
//            'residence_address_id',
//            'living_address_id',
//            'responsible',
//            'tenant_id',
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        // 'created_by',
        // 'updated_by',
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                        Yii::$app->params['baseUrl'].'/ia/demo-contacts/view?id='.$model['id'],
                        [
                            'title' => 'View',
                            'aria-label' => 'View',
                            'data-pjax' => 0
                        ]);
                },
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        Yii::$app->params['baseUrl'].'/ia/demo-contacts/update?id='.$model['id'],
                        [
                            'title' => 'Update',
                            'aria-label' => 'Update',
                            'data-pjax' => 0
                        ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        Yii::$app->params['baseUrl'].'/ia/demo-contacts/delete?id='.$model['id'],
                        [
                            'data-method' => 'post',
                            'title' => 'Delete',
                            'aria-label' => 'Delete',
                            'data-pjax' => 0,
                            'data-confirm' => 'Are you sure to delete this item?',
                        ]);
                }
            ]
        ]
    ];

//    $lang_select = $this->render('///layouts/admin/_language_select', [
//        'language' => $def_lang,
//        'action' => 'index',
//        'model_controller_path' => '/ia/ui-translations',
//    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Demo data: Contacts</h3>',
                'before' => '',//$lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-contacts'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>
</div>
