<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Contacts $model
 */

$this->title = 'Demo Contact #' . $model['id'];
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Contact info
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            // 'id',
                            'last_name',
                            'first_name',
                            'middle_name',
                            //'date_of_birth',
                            [
                                'attribute' => 'gender',
                                'value' => function () use ($model) {
                                    if ($model['gender'] === null)
                                        return '(not set)';

                                    return ($model['gender'] == 1 ? 'Male' : 'Female');
                                }
                            ],
                            'discount',
                            //'nationality',
                            'tin',
                            'type',
                            [
                                'attribute' => 'company_id',
                                'label' => 'Company',
                                'value' => function () use ($model) {
                                    $company = \app\models\Companies::findOne((!empty($model['company_id']) ? $model['company_id'] : null));

                                    return (!is_null($company) ? $company['name'] : '(not set)');
                                }
                            ],
                            //'residence_address_id',
                            //'living_address_id',
                            [
                                'attribute' => 'responsible',
                                'value' => function () use ($model) {
                                    $user = \app\models\User::findOne((!empty($model['responsible']) ? $model['responsible'] : null));

                                    return (!is_null($user) ? $user['last_name'] . ' ' . $user['first_name'] : '(not set)');
                                }
                            ],
                            [
                                'attribute' => 'tenant_id',
                                'label' => 'Tenant',
                                'value' => function () use ($model) {
                                    $tenant = \app\models\Tenants::findOne((!empty($model['tenant_id']) ? $model['tenant_id'] : null));

                                    return (!is_null($tenant) ? $tenant['name'] : '(not set)');
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'containerOptions' => ['class'=>'to-datetime-convert'],
                                'valueColOptions' => ['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            // 'created_by',
                            // 'updated_by',
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <div class="box-footer">
                        <?=Html::a('Update', ['update','id' => $model['id']], ['class' => 'btn btn-success'])?>
                        <?= Html::a('Delete', ['delete', 'id' => $model['id']], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?php
            $additionals = [
                [
                    'id' => 'email',
                    'title' => 'Email',
                    'name' => 'ContactsEmails',
                    'items' => (isset($model['ContactsEmails']) ? $model['ContactsEmails'] : []),
                    'type' => 'view'
                ],
                [
                    'id' => 'phone',
                    'title' => 'Phone',
                    'name' => 'ContactsPhones',
                    'items' => (isset($model['ContactsPhones']) ? $model['ContactsPhones'] : []),
                    'type' => 'view'
                ],
                [
                    'id' => 'messenger',
                    'title' => 'Messenger',
                    'name' => 'ContactsMessengers',
                    'items' => (isset($model['ContactsMessengers']) ? $model['ContactsMessengers'] : []),
                    'type' => 'view'
                ]
            ];
            ?>
            <?php foreach ($additionals as $additional):?>
                <?php
                echo $this->render('_additional', $additional)
                ?>
            <?php endforeach;?>
        </div>
    </div>
</div>
