<?php
/**
 * _additional_modal.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 * @var $title string
 * @var $items array
 * @var $name string
 * @var $id
 */

?>
<div class="modal fade" tabindex="-1" role="dialog" id="<?=$id?>s-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=$title?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group field-contacts-middle_name">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" for="<?=$id?>_type_id">Type</label>
                            <select class="form-control" id="<?=$id?>_type_id" style="width: 100%"></select>
                        </div>
                    </div>
                    <input type="hidden" id="<?=$id?>_id"><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" for="<?=$id?>_value"><?=$title?></label>
                            <input type="text" id="<?=$id?>_value" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="<?=$id?>_confirm" type="button" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</div>
