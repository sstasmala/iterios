<?php

/**
 * @var yii\web\View $this
 * @var app\models\Contacts $model
 */

$this->title = 'Update Demo Contact #' . $data['id'];
$this->params['breadcrumbs'][] = ['label' => 'Demo Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $data['id'], 'url' => ['view', 'id' => $data['id']]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contacts-update">

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
