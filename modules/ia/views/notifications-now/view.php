<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsNow $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Notification Constructors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-now-view">


    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        'id',
                        'name',
                        'class',
                        'is_email:email',
                        'is_notice',
                        'text_email:ntext',
                        'text_notice:ntext',
                        'created_at',
                        'updated_at',
                        'created_by',
                        'updated_by',

                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        //                    'panel' => [
                        //                        'heading' => $this->title,
                        //                        'type' => DetailView::TYPE_INFO,
                        //                    ],
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>



    <?php /*= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'id',
            'name',
            'class',
            'is_email:email',
            'is_notice',
            'text_email:ntext',
            'text_notice:ntext',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->id],
        ],
        'enableEditMode' => true,
    ]) */?>

</div>
