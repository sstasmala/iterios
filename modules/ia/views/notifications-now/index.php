<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use app\components\notifications_now\NotificationActions;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\components\notifications_now\NotificationActions
 */

$this->title = 'Notification Constructor';
$this->params['breadcrumbs'][] = $this->title;

$actions = NotificationActions::ACTIONS;
$actions_filter = [];
foreach($actions as $action => $title) {
    $actions_filter[$action] = $action;
}

?>
<div class="notifications-now-index">
    <?php 

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'class',
            [
                'attribute' => 'class',
                'format' => 'raw',
                'value' => function($model){
                    return $model->class;
                },
                'filter' => $actions_filter,
            ],
            [
                'attribute' => 'is_notice',
                'format' => 'raw',
                'value' => function($model){
                    return $model->is_notice ? '<span class="label label-success">true</span>' : '<span class="label label-danger">false</span>';
                },
                'filter' => [
                    0 => 'false',
                    1 => 'true'
                ]
            ],
            [
                'attribute' => 'is_email',
                'format' => 'raw',
                'value' => function($model){
                    return $model->is_email ? '<span class="label label-success">true</span>' : '<span class="label label-danger">false</span>';
                },
                'filter' => [
                    0 => 'false',
                    1 => 'true'
                ]
            ],

            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){
                    return $model->status ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>';
                }
            ],
    //            'text_email:ntext', 
    //            'text_notice:ntext', 
    //            'created_at', 
    //            'updated_at', 
    //            'created_by', 
    //            'updated_by', 

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['ia/notifications-now/update', 'id' => $model->id, 'edit' => 't']),
                            ['title' => Yii::t('yii', 'Edit'),]
                        );
                    }
                ],
            ],
        ];

        $lang_select = $this->render('///layouts/admin/_language_select', [
            'language' => $def_lang,
            'action' => 'index',
            'model_controller_path' => '/ia/'.Yii::$app->controller->id,
        ]);

        $dynagrid = DynaGrid::begin([
                'columns' => $columns,
                'theme'=>'panel-success',
                'showPersonalize' => true,
                'storage' => 'session',
                'gridOptions' => [
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'showPageSummary' => false,
                    'floatHeader' => false,
                    'pjax' => false,
                    'responsiveWrap' => false,
                    'responsive' => false,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'panel'=> [
                        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Reminder Event Types</h3>',
                        'before' => $lang_select,
                        'after' => false
                    ],
                    'toolbar' => [
                        ['content'=>
                             (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
        //                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                             Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                        ],
                        ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                        '{export}',
                    ]
                ],
                'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
            ]);

            if (substr($dynagrid->theme, 0, 6) == 'simple') {
                $dynagrid->gridOptions['panel'] = false;
            }

            DynaGrid::end(); ?>
</div>
