<?php

use app\components\notifications_now\NotificationActions;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsNow $model
 * @var yii\widgets\ActiveForm $form
 */

// CKEditor
$this->registerJsFile('@web/admin/plugins/ckeditor/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/base-notifications/ckeditor-init.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$this->registerJsFile('@web/admin/js/pages/notifications-now/constructor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);



$this->registerJs("jQuery(document).ready(function(){
    var shortcodes = JSON.parse('".Json::encode(NotificationActions::getShortcodes())."');
    console.log(shortcodes);
    
    jQuery('#notificationsnow-class').on('change', function(){
        var shortBlock = $('.shortcodes');
         
       // shortBlock.hide();
        
        var action = $(this).val();
        console.log(action);
        if (typeof shortcodes[action] === 'undefined')
            return false;
            
        var shorts = shortcodes[action];
        
        shortBlock.find('.action').text('Placeholders: ' + action);
        shortBlock.find('.placeholders').html('<p><code>' + shorts.join('</code></p><p><code>') + '</code></p>');
        //shortBlock.show(200);
    });
    
    jQuery('#notificationsnow-class').change();
    
});", \yii\web\View::POS_END);

?>

<div class="notifications-now-form">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="shortcodes col-md-3">
            <div class="box box-solid" style="background: #edf0f5;">
                <div class="box-header">
                    <h3 class="box-title action">Placeholders</h3>
                </div>
                <div class="box-body placeholders ">

                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'class' => ['type' => Form::INPUT_LIST_BOX, 'items' => $actions, 'options' => ['size' => 1]],

            'is_email' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Is Email...']],

            'is_notice' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Is Notice...']],

            'title_email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Title Email...', 'maxlength' => 255]],
            'text_email' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Text Email...','rows' => 6]],

            'text_notice' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Text Notice...','rows' => 6]],

            'status' => ['type' => Form::INPUT_CHECKBOX, 'label' => 'ON/OFF'],

            //'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

            //'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],

            //'created_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created By...']],

            //'updated_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated By...']],
        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

    <?/*
        initCKEditor('base-notifications-subject_'+lang_iso, field_value_promise);
    initCKEditor('base-notifications-body_'+lang_iso, field_value_promise);
    */?>

</div>
