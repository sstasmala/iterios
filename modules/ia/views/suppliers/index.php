<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\helpers\MCHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Suppliers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-index">
    <?php
    $columns = [
        'id',
        'name',
        'code',
        [
            'attribute' => 'logo',
            'format' => 'raw',
            'value' =>  function ($model) {
                return (!empty($model->logo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->logo, ['class' => 'img-round', 'style' => 'height: 64px; width: 64px;']) : '(not set)');
            },
        ],
        [
            'attribute' => 'country_id',
            'format' => 'raw',
            'value' => function ($model) {
                return MCHelper::getCountryById($model['country_id'],Yii::$app->user->identity->tenant->language->iso)[0]['title'];
            }
        ],
        'description',
        'web_site',
        'company',
        'authorization_type',
        [
            'attribute' => 'supplier_type_id',
            'format' => 'raw',
            'value' => function ($model) {
                return isset($model->type) ? $model->type->value : '(not set)';
            }
        ],
        [
            'attribute' => 'functions',
            'value' => function($model){
                return isset($model->functions) ? $model->functions : '(not set)';
            },
        ],
        [
            'attribute' => 'created_at',
            'visible' => false,
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'visible' => false,
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                $user = \app\models\User::findOne($model->created_by);
                return $user->first_name.' '.$user->last_name;
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                $user = \app\models\User::findOne($model->updated_by);
                return $user->first_name.' '.$user->last_name;
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/suppliers',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> suppliers</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
//                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
