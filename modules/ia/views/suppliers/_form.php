<?php

use app\models\ProviderFunction;
use app\models\Suppliers;
use app\models\SupplierType;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/provider/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

?>

<div class="suppliers-form">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
            <?= $form->field($model, 'name'); ?>
            <?= $form->field($model, 'code'); ?>

            <?= !empty($model->logo) ? '<label class="control-label col-md-2">Current logo</label><img class="help-block img-rounded" src="/'.$model->logo.'" height="100px">': ''?>

            <?= $form->field($model, 'logo')->fileInput(); ?>

            <?= $form->field($model, 'supplier_type_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(SupplierType::find()->all(), 'id', 'value'),
                'options' => ['placeholder' => 'Choose segment type'],
            ]); ?>

            <div class="form-group field-suppliers-country">
                <label class="control-label col-md-2" for="suppliers-country">Country</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            name="Suppliers[country_id]" id="country-select" required>
                        <?php if(null !== $model['country_id']):?>
                            <option value="<?= $model['country_id'] ?>" selected="selected"><?= \app\helpers\MCHelper::getCountryById($model['country_id'],'en')[0]['title']?></option>
                        <?php endif;?>
                    </select>

                    <div class="help-block"></div>
                </div>
            </div>

            <?= $form->field($model, 'description')->textarea(); ?>
            <?= $form->field($model, 'web_site'); ?>
            <?= $form->field($model, 'company'); ?>

            <?= $form->field($model, 'authorization_type')->widget(Select2::classname(), [
                'data' => Suppliers::getAuthorizationTypes(),
                'options' => ['placeholder' => 'Choose authorization type'],
            ]); ?>

            <div class="form-group field-providers-provider_type_id">
                <label class="control-label col-md-2" for="providers-functions">Supplier Functions</label>
                <div class="col-md-10">
                    <?= Select2::widget([
                        'name' => 'functions[]',
                        'value' => \json_decode($model->functions),
                        'data' => ArrayHelper::map(ProviderFunction::find()->translate('en')->all(), 'id','value' ),
                        'options' => ['multiple' => true, 'placeholder' => 'Choose supplier function'],
                    ]); ?>

                    <div class="help-block"></div>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <?php
            echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            );
            ?>
            <?php ActiveForm::end(); ?>
        </div>
</div>