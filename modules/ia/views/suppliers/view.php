<?php

use app\helpers\MCHelper;
use app\models\ProviderFunction;
use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Suppliers: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$funcs_ids = !empty($model->functions) ? JSON::decode($model->functions) : [];

$view_functions_list = ProviderFunction::find()->where(['in', 'id', $funcs_ids])->translate('en')->all();
$view_functions_list = array_column($view_functions_list, 'value');
?>
<div class="tasks-types-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/suppliers',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php $attr = [
                        'id',
                        'name',
                        'code',
                        [
                            'attribute' => 'logo',
                            'format' => 'raw',
                            'value' =>  !empty($model->logo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->logo, ['class' => 'img-round', 'style' => 'height: 128px; width: 128px;']) : '(not set)'
                        ],
                        [
                            'attribute' => 'country_id',
                            'format' => 'raw',
                            'value' => MCHelper::getCountryById($model['country_id'],Yii::$app->user->identity->tenant->language->iso)[0]['title']
                        ],
                        'description',
                        'web_site',
                        'company',
                        'authorization_type',
                        [
                            'attribute' => 'supplier_type_id',
                            'label'     => 'Supplier Type',
                            'format'    => 'raw',
                            'value'     => isset($model->type) ? $model->type->value : '(not set)',
                        ],
                        [
                            'attribute' => 'functions',
                            'format' => 'raw',
                            'value' => implode(', ', $view_functions_list)
                        ],
                        [
                            'attribute'        => 'created_at',
                            'valueColOptions'  => ['class' => 'to-datetime-convert']
                        ],
                        [
                            'attribute'       => 'updated_at',
                            'valueColOptions' => ['class' => 'to-datetime-convert']
                        ],
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => isset($model->created)? $model->created->first_name . ' '. $model->created->last_name: '',
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => isset($model->updated)? $model->updated->first_name . ' '. $model->updated->last_name: '',
                        ],
                    ]; ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => $attr,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
