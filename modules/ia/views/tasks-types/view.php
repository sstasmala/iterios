<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Task Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tasks Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-types-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/tasks-types',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => [
                            'id',
                            'value',
                            'default',
                            [
                                'attribute'=>'created_at',
                                'containerOptions'=>['class'=>'to-datetime-convert'],
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            'created_by',
                            'updated_by',
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
