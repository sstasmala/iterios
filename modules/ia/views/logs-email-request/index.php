<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\LogsEmailRequest $searchModel
 */

$this->title = 'Logs eMail Request';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logs-email-request-index">
    <?php
    $columns = [
        ['attribute' => 'id'],
        'event',
        [
            'attribute' => 'log_email_id',
            'label' => 'Mail Log',
            'format' => 'raw',
            'value' => function($model){
                if(!$model->log_email_id) {
                    return '(not set)';
                }
                return Html::a('ID ' . $model->log_email_id, '/ia/logs-'.$model->logEmail->type.'-email/view?id='.$model->log_email_id);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\LogsEmail::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Mail log', 'id' => 'grid-email-provider-requests-log-search-log_email_id']
        ],
        [
            'attribute' => 'email_provider_id',
            'label' => 'Mail provider',
            'format' => 'raw',
            'value' => function($model){
                if(!$model->email_provider_id) {
                    return '(not set)';
                }
                return Html::a($model->emailProvider->name, '/ia/email-providers/view?id='.$model->email_provider_id);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\EmailProviders::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Mail provider', 'id' => 'grid-email-provider-requests-log-search-email_provider_id']
        ],
        [
            'attribute' => 'method',
            'label' => 'Method',
            'value' => function($model){
                return ($model->method ? $model->method : '');
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map($data_method, 'method', 'method'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true, 'minimumResultsForSearch' => -1],
            ],
            'filterInputOptions' => ['placeholder' => 'Method', 'id' => 'grid-email-provider-requests-log-search-method']
        ],
        'url',
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Created At',
            'value' => function ($model) {
                return (!empty($model->created_at) ? $model->created_at : '');
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterInputOptions' => ['placeholder' => 'Created At', 'id' => 'grid-created_at', 'class' =>'form-control'],
            'filterWidgetOptions' => [
                'hideInput' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'YYYY-MM-DD HH:mm:00'],
                    'opens' => 'left',
                    //'drops' => 'up',
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'ranges' => [
                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                        "Last 30 Days" => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                        "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                    ]
                ],
                'pluginEvents' => [
                    "cancel.daterangepicker" => "function() { 
                        jQuery(this).find('input').val('');
                        jQuery(this).find('.range-value').text('');
                    }"
                ]
            ]
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}'
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Logs eMail request</h3>',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['/ia/logs-email-request'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-email-provider-requests-log'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end(); ?>

</div>
