<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\LogsEmailRequest $model
 */

$this->title = 'Logs eMail Request #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs eMail Request', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logs-email-request-view">
    <?php
    $gridColumn = [
        'id',
        'event',
        [
            'attribute' => 'log_email_id',
            'label' => 'Log email',
            'format' => 'raw',
            'value' => (!$model->log_email_id ? '(not set)' : Html::a('ID ' . $model->log_email_id, '/ia/logs-'.$model->logEmail->type.'-email/view?id='.$model->log_email_id))
        ],
        [
            'attribute' => 'email_provider_id',
            'label' => 'Mail provider',
            'format' => 'raw',
            'value' => (!$model->email_provider_id ? '(not set)' : Html::a($model->emailProvider->name, '/ia/email-providers/view?id='.$model->email_provider_id))
        ],
        'method',
        'url',
        [
            'attribute' => 'created_at',
            'valueColOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Created At'
        ]
    ];
    ?>

    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Logs eMail request info</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Logs eMail request headers</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= \kdn\yii2\JsonEditor::widget([
                        'clientOptions' => [
                            'modes' => ['view', 'code'], // available modes
                            'mode' => 'view', // default mode
                        ],
                        'containerOptions' => ['style' => 'height: 420px'],
                        'name' => 'mail_provider-request_log-headers',
                        'value' => $model->headers
                    ]) ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Logs eMail request body</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= \kdn\yii2\JsonEditor::widget([
                        'clientOptions' => [
                            'modes' => ['view', 'code'], // available modes
                            'mode' => 'view', // default mode
                        ],
                        'containerOptions' => ['style' => 'height: 420px'],
                        'name' => 'mail_provider-request_log-body',
                        'value' => $model->body
                    ]) ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>
