<?php

use app\models\Providers;
use app\models\Tenants;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->registerJsFile('@web/admin/js/pages/providers-credentials/items.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);


$fields = [];
/**
 * @var $provider Providers
 */
$provider = $model->provider;
/**
 * @var $model \app\models\ProvidersCredentials
 */
if(isset($model->provider)) {
    if($provider->authorization_type == 'api-token')
        $fields['apiKey'] = '';
    if($provider->authorization_type == 'login&pass') {
        $fields['username'] = '';
        $fields['password'] = '';
    }
    if(isset($provider->additional_config)) {
        $config = \json_decode($provider->additional_config,true);
        foreach ($config as $item)
            $fields[$item] = '';
    }
}
?>

<div class="providers-credentials-form">
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <?= $form->field($model, 'provider_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Providers::find()->translate('en')->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose provider'],
        ]); ?>

        <?= $form->field($model, 'tenant_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tenants::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose tenant'],
        ]); ?>

        <div id="params-body">
            <?php
            if (isset($model->params)) {
                $paramsDecode = json_decode($model->params, true);
                foreach ($paramsDecode as $k => $v)
                    if(isset($fields[$k]))
                        $fields[$k] = $v;
            }
            foreach ($fields as  $field => $value) {
                $inputParam = '' .
                    '<div class="form-group">' .
                    '<label class="control-label col-md-2" for="providerscredentials-params-' . $field . '">' . $field . '</label>' .
                    '<div class="col-md-10">' .
                    '<input type="text" id="providerscredentials-params-' .
                    $field .
                    '" class="form-control" name="ProvidersCredentials[credentialParams][' .
                    $field .
                    ']" value="' . $value . '">' .
                    '</div>' .
                    '</div>';
                echo $inputParam;
            }

//
//            if (isset($model->params)) {
//                $paramsDecode = json_decode($model->params, true);
//                $inputParam = '';
//                foreach ($paramsDecode as $key => $param) {
//                    $inputParam .= '' .
//                        '<div class="form-group">' .
//                        '<label class="control-label col-md-2" for="providerscredentials-params-' . $key . '">' . $key . '</label>' .
//                        '<div class="col-md-10">' .
//                        '<input type="text" id="providerscredentials-params-' .
//                        $key .
//                        '" class="form-control" name="ProvidersCredentials[credentialParams][' .
//                        $key .
//                        ']" value="' . $param . '">' .
//                        '</div>' .
//                        '</div>';
//                }
//                echo $inputParam;
//            } else {
//                if(isset($model->provider)) {
//                    if($model->provider->authorization_type == 'login&pass')
//                        echo '<div class="form-group">'
//                            .'<label class="control-label col-md-2" for="providerscredentials-params-username">Username</label>'
//                            .'<div class="col-md-10">'
//                            .'<input type="text" id="providerscredentials-params-username" class="form-control" name="ProvidersCredentials[credentialParams][username]">'
//                            .'</div></div>'
//                            .'<div class="form-group">'
//                            .'<label class="control-label col-md-2" for="providerscredentials-params-password">Password</label>'
//                            .'<div class="col-md-10">'
//                            .'<input type="text" id="providerscredentials-params-password" class="form-control" name="ProvidersCredentials[credentialParams][password]">'
//                            .'</div></div>';
//                    if($model->provider->authorization_type == 'api-token')
//                        echo '<div class="form-group">'
//                            .'<label class="control-label col-md-2" for="providerscredentials-params-apitoken">Api token</label>'
//                            .'<div class="col-md-10">'
//                            .'<input type="text" id="providerscredentials-params-apitoken" class="form-control" name="ProvidersCredentials[credentialParams][apiKey]">'
//                            .'</div>'
//                            .'</div>';
//                }
//            }
            ?>
        </div>
        <div id="params-body2" class="help-config">
        </div>
        <div id="params-body3" class="help-config">
        </div>
    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        );
        ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>