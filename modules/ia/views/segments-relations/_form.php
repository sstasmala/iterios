<?php

use app\models\Segments;
use app\models\SegmentsRelations;
use app\models\Tenants;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;


?>

<div class="segments-relations-form">

    <?php $form = ActiveForm::begin(['id'=>'segments-relations','type' => ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Name']) ?>

    <?= $form->field($model, 'id_segment')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Segments::find()->all(), 'id','name' ),
        'options' => ['placeholder' => 'Choose segments'],
    ]); ?>

    <?= $form->field($model, 'id_tenant')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tenants::find()->all(), 'id','name' ),
        'options' => ['placeholder' => 'Choose tenant'],
    ]); ?>

    <?= $form->field($model, 'type', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value'=>SegmentsRelations::TYPE_SYSTEM]); ?>

    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id'=>'segments-relations-form-submit']
    );?>

    <?php ActiveForm::end(); ?>

</div>
