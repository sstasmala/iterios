<?php

use app\models\Contacts;
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SitesTypes $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Segments relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="segments-relations-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/segments-relations',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'id_segment',
                                'format' => 'raw',
                                'value' => $model->segment->name,
                            ],
                            [
                                'attribute' => 'id_tenant',
                                'format' => 'raw',
                                'value' => $model->tenant->name,
                            ],
                            'type',
                            'count_phones',
                            'count_emails',
                            'count_contact',

                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Result Data Table</h3>
                </div>
                <?php

                ?>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $provider,
                    ]) ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
