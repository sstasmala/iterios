<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Segments relations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="segments-relations-index">
    <?php
    $columns = [
        'id',
        'name',
        'type',
        [
            'attribute' => 'id_segment',
            'header' => 'Segment',
            'value' => function($model){

                return $model->segment->name;
            },
        ],
        [
            'attribute' => 'id_tenant',
            'header' => 'Tenant',
            'value' => function($model){

                return $model->tenant->name;
            },
        ],
        [
            'attribute' => 'created_by',
            'value' => function($model){

                return isset($model->created)? $model->created->first_name . ' '. $model->created->last_name: '';
            },
        ],
        [
            'attribute' => 'updated_by',
            'value' => function($model){

                return isset($model->updated)? $model->updated->first_name . ' '. $model->updated->last_name: '';
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ],
        'count_phones',
        'count_emails',
        'count_contact'
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/segments-relations',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Segments relations</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['loader'], [
                        'data-pjax'=>0,
                        'class' => ($jobUpdateSegmentsListRun)? 'btn btn-success disabled':'btn btn-success',
                        'title'=>'Refresh data from config',
                        'aria-disabled' => ($jobUpdateSegmentsListRun)? 'true':'false',
                    ]) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-segments-relations'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
