<?php

function getBadge($status) {
    $status_map = [
        \app\models\LogsEmail::STATUS_WAIT => 'warning',
        \app\models\LogsEmail::STATUS_ERROR => 'danger',
        \app\models\LogsEmail::STATUS_SENT => 'success',
        \app\models\LogsEmail::STATUS_REJECTED => 'danger',
        \app\models\LogsEmail::STATUS_DELAY => 'warning',
        \app\models\LogsEmail::STATUS_DELIVERED => 'success',
        \app\models\LogsEmail::STATUS_OPEN => 'info',
        \app\models\LogsEmail::STATUS_CLICK => 'primary',
        \app\models\LogsEmail::STATUS_BOUNCE => 'danger'
    ];

    return '<span class="label label-'.$status_map[$status].'">'.$status.'</span>';
}