<?php

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\LogsEmail $model
 */

$this->title = 'Log eMail #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs eMail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->render('_status');
?>
<div class="logs-email-view">
    <?php
    $gridColumn = [
        'subject',
        [
            'attribute' => 'type',
            'format' => 'raw',
            'value' => ($model->type == \app\models\LogsEmail::TYPE_SYSTEM ?
                    '<span class="label label-default">'.$model->type.'</span>' :
                    '<span class="label label-info">'.$model->type.'</span>')
        ],
        'recipient',
        [
            'attribute' => 'template_id',
            'label' => 'Template',
            'format' => 'raw',
            'value' => (!empty($model->template_id) ? Html::a($model->template->name, '/ia/templates/view?id='.$model->template_id) : null)
        ],
        //'conversion:boolean',
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => getBadge($model->status)
        ],
        'reason',
        'ip',
        'city',
        [
            'attribute' => 'created_at',
            'valueColOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Created At'
        ],
        [
            'attribute' => 'updated_at',
            'valueColOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Updated At'
        ]
    ];
    ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Logs eMail info</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                    ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Logs eMail preview</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <label>Text</label>
                    <iframe id="preview-text" style="width: 100%" height="80" sandbox="" srcdoc="<?= $model->text ?>"></iframe>
                    <label>Html</label>
                    <iframe id="preview-html" style="width: 100%" height="305" sandbox="" srcdoc="<?= Html::encode($model->html) ?>"></iframe>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="w1">
        <?php
        $columns = [
            ['attribute' => 'id'],
            'event',
            [
                'attribute' => 'log_email_id',
                'label' => 'Mail Log',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a('ID ' . $model->log_email_id, '/ia/email-log/view?id='.$model->log_email_id);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\LogsEmail::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Email log', 'id' => 'grid-email-provider-requests-log-search-email_log_id']
            ],
            [
                'attribute' => 'method',
                'label' => 'Method',
                'value' => function($model){
                    return ($model->method ? $model->method : '');
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map($data_method, 'method', 'method'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'minimumResultsForSearch' => -1],
                ],
                'filterInputOptions' => ['placeholder' => 'Method', 'id' => 'grid-email-provider-requests-log-search-method']
            ],
            'url',
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert'],
                'label' => 'Created At',
                'value' => function ($model) {
                    return (!empty($model->created_at) ? $model->created_at : '');
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterInputOptions' => ['placeholder' => 'Created At', 'id' => 'grid-created_at', 'class' =>'form-control'],
                'filterWidgetOptions' => [
                    'hideInput' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'YYYY-MM-DD HH:mm:00'],
                        'opens' => 'left',
                        'drops' => 'up',
                        'timePicker' => true,
                        'timePicker24Hour' => true,
                        'ranges' => [
                            "Today" => ["moment().startOf('day')", "moment()"],
                            "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                            "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                            "Last 30 Days" => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                            "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                            "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                        ]
                    ],
                    'pluginEvents' => [
                        "cancel.daterangepicker" => "function() { 
                        jQuery(this).find('input').val('');
                        jQuery(this).find('.range-value').text('');
                    }"
                    ]
                ]
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<a href="/ia/logs-email-request/view?id='.$model->id.'" title="View" aria-label="View" data-pjax="0">'.
                            '<span class="glyphicon glyphicon-eye-open"></span></a>';
                    }
                ],
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-info',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Logs eMail Request</h3>',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', '', ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-email-provider-requests-log'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end(); ?>
    </div>

</div>
