<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\LogsEmail $searchModel
 */

$this->title = 'Logs eMail';
$this->params['breadcrumbs'][] = $this->title;

$this->render('_status');
?>
<div class="logs-email-index">
    <?php
    $columns = [
        ['attribute' => 'id'],
        'subject',
        [
            'attribute' => 'type',
            'label' => 'Type',
            'format' => 'raw',
            'value' => function ($model) {
                return ($model->type == \app\models\LogsEmail::TYPE_SYSTEM ?
                    '<span class="label label-default">'.$model->type.'</span>' :
                    '<span class="label label-info">'.$model->type.'</span>');
            },
            //'filterType' => GridView::FILTER_SELECT2,
            // 'filter' => [
            //     \app\models\LogsEmail::TYPE_SYSTEM => 'System',
            //     \app\models\LogsEmail::TYPE_PUBLIC => 'Public'
            // ],
            // 'filterWidgetOptions' => [
            //     'pluginOptions' => ['allowClear' => true],
            //     'hideSearch' => true
            // ],
            // 'filterInputOptions' => ['placeholder' => 'Type', 'id' => 'grid-email-log-search-type']
        ],
        'recipient',
        [
            'attribute' => 'template_id',
            'label' => 'Template',
            'format' => 'raw',
            'value' => function($model){
                return (!empty($model->template_id) ? Html::a($model->template->name, '/ia/templates/view?id='.$model->template_id) : null);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map($data_templates, 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Template', 'id' => 'grid-email-log-search-template_id']
        ],
        //'conversion:boolean',
        [
            'attribute' => 'status',
            'label' => 'Status',
            'format' => 'raw',
            'value' => function ($model) {
                return getBadge($model->status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [
                \app\models\LogsEmail::STATUS_WAIT => 'Wait',
                \app\models\LogsEmail::STATUS_ERROR => 'Error',
                \app\models\LogsEmail::STATUS_SENT => 'Sent',
                \app\models\LogsEmail::STATUS_REJECTED => 'Rejected',
                \app\models\LogsEmail::STATUS_DELAY => 'Delay',
                \app\models\LogsEmail::STATUS_DELIVERED => 'Delivered',
                \app\models\LogsEmail::STATUS_OPEN => 'Open',
                \app\models\LogsEmail::STATUS_CLICK => 'Click',
                \app\models\LogsEmail::STATUS_BOUNCE => 'Bounce'
            ],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
                'hideSearch' => true
            ],
            'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-email-log-search-status']
        ],
        //'reason',
        [
            'attribute' => 'ip',
            'label' => 'IP'
        ],
        'city',
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Created At',
            'value' => function ($model) {
                return (!empty($model->created_at) ? $model->created_at : '');
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterInputOptions' => ['placeholder' => 'Created At', 'id' => 'grid-created_at', 'class' =>'form-control'],
            'filterWidgetOptions' => [
                'hideInput' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'YYYY-MM-DD HH:mm:00'],
                    'opens' => 'left',
                    //'drops' => 'up',
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'ranges' => [
                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                        "Last 30 Days" => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                        "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                    ]
                ],
                'pluginEvents' => [
                    "cancel.daterangepicker" => "function() { 
                        jQuery(this).find('input').val('');
                        jQuery(this).find('.range-value').text('');
                    }"
                ]
            ]
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'label' => 'Updated At',
            'value' => function ($model) {
                return (!empty($model->updated_at) ? $model->updated_at : '');
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterInputOptions' => ['placeholder' => 'Updated At', 'id' => 'grid-updated_at', 'class' =>'form-control'],
            'filterWidgetOptions' => [
                'hideInput' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'YYYY-MM-DD HH:mm:00'],
                    'opens' => 'left',
                    //'drops' => 'up',
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'ranges' => [
                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                        "Last 30 Days" => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                        "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                    ]
                ],
                'pluginEvents' => [
                    "cancel.daterangepicker" => "function() { 
                        jQuery(this).find('input').val('');
                        jQuery(this).find('.range-value').text('');
                    }"
                ]
            ]
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}'
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Logs eMail</h3>',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', [''], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-ref1'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end(); ?>

</div>