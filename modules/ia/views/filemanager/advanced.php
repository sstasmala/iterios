<?php
/**
 * index.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 *
 * @var $this \yii\web\View
 */
use yii\helpers\Html;

$this->title = 'Filemanager';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-create">
<?= \app\components\filemanager\ElFinderWidget::widget([
    'id' => 'working',
    'clientOptions' => [
        'url' => Yii::$app->params['baseUrl'].'/ia/filemanager/upload-advanced',
        'height' => '80%'
    ]
]); ?>
    <div id="newww"></div>
</div>
