<?php

/**
 * @var yii\web\View $this
 * @var app\models\EmailProviders $model
 */

$this->title = 'Create Email Provider';
$this->params['breadcrumbs'][] = ['label' => 'Email Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-providers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
