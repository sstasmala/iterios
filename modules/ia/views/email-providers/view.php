<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\EmailProviders $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Email Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-providers-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    eMail provider info
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            //'id',
                            'provider',
                            'type',
                            'name',
                            'api_key',
                            [
                                'attribute' => 'created_at',
                                'containerOptions' => ['class'=>'to-datetime-convert'],
                                'valueColOptions' => ['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            // 'created_by',
                            // 'updated_by',
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
                <div class="box-footer">
                    <?=Html::a('Update', ['update','id' => $model['id']], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model['id']], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
