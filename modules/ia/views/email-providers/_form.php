<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\EmailProviders $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="email-providers-form">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    eMail provider info
                </div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' => 1,
                        'attributes' => [
                            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
                            'provider' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => '\kartik\widgets\Select2',
                                'options' => [
                                    'data' => \yii\helpers\ArrayHelper::map(\Yii::$app->params['email_providers'], 'class', 'name'),
                                    'options' => ['placeholder' => 'Choose email provider'],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ]
                                ]
                            ],
                            'api_key' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Api Key...', 'maxlength' => 255]],
                        ]

                    ]); ?>
                </div>
                <div class="box-footer">
                    <?php
                    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    );
                    ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
