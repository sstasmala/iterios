<?php

/**
 * @var yii\web\View $this
 * @var app\models\EmailProviders $model
 */

$this->title = 'Update Email Provider: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Email Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="email-providers-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
