<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesFields $model
 */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Requisites Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-fields-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/requisites-groups',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
        <div class="box-body">
            <?php
            $attributes = [
                'id',
                [
                    'attribute' => 'type',
                    'value' => $model::getTypeName($model->type)
                ],
                'name',
                'code',
                [
                    'attribute' => 'requisite_group_id',
                    'value' => $model->requisiteGroup->name
                ],
                'variants:ntext',
            ];
            ?>
            <?= DetailView::widget([
                'model' => $model,
                'condensed' => false,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'attributes' => $attributes,
                'enableEditMode' => false,
            ]) ?>
        </div>
        <div class="box-footer">
            <?= Html::a('Update', ['update?id='.$model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>
</div>
