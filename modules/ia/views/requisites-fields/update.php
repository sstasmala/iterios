<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesFields $model
 */

$this->title = 'Update Requisites Fields: ' . ' ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Requisites Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requisites-fields-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/requisites-fields',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
