<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesFields $model
 * @var yii\widgets\ActiveForm $form
 */

//$this->registerCssFile(Yii::$app->params['base_url'].'/css/pages/tariff-fields/form.css');
$this->registerJsFile('@web/admin/js/pages/requisites/requisites-fields/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
?>

<div class="requisites-fields-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]) ?>

   <?= Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 250]],
            'field_placeholder' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Field Placeholder...', 'maxlength' => 250]],
            'field_description' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Field Description...', 'maxlength' => 250]],
            'code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Code...', 'maxlength' => 250]],
        ]

    ]) ?>
    <?= $form->field($model, 'variants',['template' => '{input}'])->hiddenInput() ?>

    <?= $form->field($model, 'requisite_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\RequisitesGroups::find()->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'type')->dropDownList(app\models\RequisitesFields::getTypesLabels(),[
                    'class' => 'form-control type-selection'
                ]) ?>
    <div class="variants-box">
    </div>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    ) ?>
   <?php ActiveForm::end(); ?>

</div>

<style>

    .variants-box>.form-group {
        padding: 10px 10px 10px 50px;
    }

    .variants-list {
        margin-bottom: 10px;
    }

    .variants-element>input {
        display: inline;
        width: 80%;
    }

    .variants-element>button {
        display: inline;
    }

    .variants-box {
        margin-left: 7.5%;
    }

</style>