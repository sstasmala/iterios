<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesFields $model
 */

$this->title = 'Create Requisites Fields';
$this->params['breadcrumbs'][] = ['label' => 'Requisites Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-fields-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/passports-fields',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
