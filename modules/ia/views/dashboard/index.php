<?php
/**
 * index.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 *
 *
 * @var $this \yii\web\View
 */
$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-database"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">DB Size</span>
                <span class="info-box-number"><?=Yii::$app->getDb()->createCommand('SELECT pg_size_pretty(pg_database_size(current_database()))')->queryOne()['pg_size_pretty'];?></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Users count</span>
                <span class="info-box-number">
                            <?= \app\models\User::find()->count();?>
                        </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-object-group"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Tenants count</span>
                <span class="info-box-number">
                            <?= \app\models\Tenants::find()->count();?>
                        </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-random"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Jobs count</span>
                <span class="info-box-number">
                            <?= \app\models\Jobs::find()->where(['lock'=>0])->count()?>
                        </span>
            </div>
        </div>
    </div>
</div>
<?php
//$mailbox = "imap.ukr.net";
//$username = "valek-s@ukr.net";
//$password = "";
//$enc = \SSilence\ImapClient\ImapClient::ENCRYPT_SSL;
//
//$imap = new SSilence\ImapClient\ImapClient([
//    'flags' => [
//        'service' => \SSilence\ImapClient\ImapConnect::SERVICE_IMAP,
//        'encrypt' => \SSilence\ImapClient\ImapConnect::ENCRYPT_SSL,
//        /* This NOVALIDATE_CERT is used when the server connecting to the imap
//         * servers is not https but the imap is. This ignores the failure.
//         */
////        'validateCertificates' => \SSilence\ImapClient\ImapConnect::NOVALIDATE_CERT
//    ],
//    'mailbox' => [
//        'remote_system_name' => $mailbox,
//        'port' => 993
//    ],
//    'connect' => [
//        'username' => $username,
//        'password' => $password
//    ]
//]);
//$message = new \SSilence\ImapClient\OutgoingMessage();
//$message->setFrom("valek-s@ukr.net");
//$message->setTo("den14@i.ua");
//$message->setMessage("Hello");
//$message->setSubject("Test");
//$message->additional_headers = "Content-Type: text/plain; charset=utf-8
//Content-Transfer-Encoding: quoted-printable";
//$message->send();
//echo true;
?>
