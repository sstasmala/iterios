<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Templates $model
 */

$this->title = 'Create System Templates';
$this->params['breadcrumbs'][] = ['label' => 'System Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-create">
    <p>
        <select id="lang-list" data-action="create">
            <option selected="selected" value="<?= $def_lang->iso ?>"><?= $def_lang->name ?> (<?= $def_lang->original_name ?>)</option>
        </select>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'handlers' => $handlers,
        'def_lang' => $def_lang
    ]) ?>
</div>
