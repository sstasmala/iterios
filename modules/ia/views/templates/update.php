<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Templates $model
 */

$this->title = 'Update Templates: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="templates-update">

    <p>
        <select id="lang-list" data-action="update">
            <option selected="selected" value="<?= $def_lang->iso ?>"><?= $def_lang->name ?> (<?= $def_lang->original_name ?>)</option>
        </select>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'handlers' => $handlers,
        'def_lang' => $def_lang
    ]) ?>
</div>
