<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Templates $model
 */

$this->title = (is_null($model->name))?'Template #'.$model->id:$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/templates/lang.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/templates/template-body.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJs('var template_id = '.$model->id.';', \yii\web\View::POS_HEAD);
$this->registerJs('var template_id = '.$model->id.';', \yii\web\View::POS_HEAD);
?>
<div class="templates-view">

    <div class="w1">
        <p>
            <select id="lang-list" data-action="view">
                <option selected="selected" value="<?= $def_lang->iso ?>"><?= $def_lang->name ?> (<?= $def_lang->original_name ?>)</option>
            </select>
        </p>
    </div>

    <?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
//        ['attribute' => 'key', 'visible' => (is_null($model->key) ? false : true)],
        'name',
        'subject:ntext',
        [
            'attribute' => 'body',
            'format' => 'raw',
            'value' => Html::a('View Body', '', ['id' => 'view_body'])
        ],
        [
            'attribute' => 'id', // cheat ^_^
            'label' => 'Handler',
            'value' => (!is_null($handler) ? $handler->handler : '(not set)')
        ]
    ];
    ?>

    <?php if (!empty($notification)): ?>
        <div class="alert alert-<?= ($notification == 'success' ? 'success' : 'danger') ?>">
            <?php if ($notification == 'success'): ?>
                Test email sent successfully!
            <?php else: ?>
                <?= $notification ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Mail Template info</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
            <div class="box-footer">
                <div class="col-md-4">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
                </div>
                <div class="col-md-8 text-right">
                    <form class="form-inline" role="form" method="post" action="<?= \Yii::$app->params['baseUrl'] ?>/ia/templates/send-test-email?id=<?= $model->id ?>">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
<!--                        <div class="form-group">-->
                            <?php
                            // echo \kartik\widgets\Select2::widget([
                            //     'data' => $handlers,
                            //     'name' => 'handler',
                            //     'options' => [
                            //         'placeholder' => 'Choose Handler',
                            //         'id' => 'handlers'
                            //     ],
                            //     'pluginOptions' => [
                            //         'width' => '100%'
                            //     ]
                            // ]);
                            ?>
<!--                        </div>-->
                        <input type="hidden" name="lang" value="<?= $def_lang->iso ?>">
                        <div class="form-group">
                            <?php
                                $options = [
                                    'class' => 'btn btn-default'
                                ];

                                if (is_null($handler))
                                    $options['disabled'] = 'disabled';
                            ?>
                            <?= Html::input('string', 'recipient_email', null, [
                                'placeholder' => 'Your email...',
                                'class' => 'form-control'
                            ])?>
                        </div>
                        <?= Html::submitButton('Send test email', $options) ?>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
