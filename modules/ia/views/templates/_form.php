<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Templates $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

// CKEditor
$this->registerJsFile('@web/admin/plugins/ckeditor/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/templates/ckeditor-init.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//

$this->registerJsFile('@web/admin/js/pages/templates/lang_form.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$this->registerJs('var template_id = '.($model->isNewRecord ? 'null':$model->id).';',\yii\web\View::POS_HEAD);
?>

<p>
    <?php
    echo \kartik\widgets\Select2::widget([
        'data' => $handlers,
        'name' => 'handler',
        'options' => [
            'placeholder' => 'Choose Handler',
            'id' => 'handlers'
        ],
        'pluginOptions' => [
            'width' => '50%'
        ]
    ]);
    ?>
</p>

<div class="templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Mail Template <?= ($model->isNewRecord ? 'Create' : 'Update') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <?=''// $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

                <div class="lang" data-lang="<?= $def_lang->iso ?>">
                    <div class="form-group field-templates-name">
                        <label class="control-label" for="templates-name">Name</label>
                        <input id="templates-name" class="form-control" name="Templates[name][<?= $def_lang->iso ?>]" rows="2" placeholder="Name..." value="<?= (isset($model->name) ? $model->name : '') ?>">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-templates-subject">
                        <label class="control-label" for="templates-subject">Subject</label>
                        <textarea id="templates-subject" class="form-control" name="Templates[subject][<?= $def_lang->iso ?>]" rows="2" placeholder="Subject..."><?= (isset($model->subject) ? $model->subject : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-templates-body">
                        <label class="control-label" for="templates-body">Body</label>
                        <textarea id="templates-body" class="form-control" name="Templates[body][<?= $def_lang->iso ?>]" rows="6" placeholder="Template..."><?= (isset($model->body) ? $model->body : '') ?></textarea>
                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <?php ActiveForm::end(); ?>

</div>
