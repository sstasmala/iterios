<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Templates $searchModel
 */

$this->title = 'System Templates';
$this->params['breadcrumbs'][] = $this->title;

$handlers = \app\helpers\TemplatableHandlersHelper::getHandlers(Yii::$container->get('systemEmitter'));
$data_handlers = [];
foreach ($handlers as $handler)  $data_handlers[$handler] = $handler::getName();
?>
<div class="templates-index">
    <?php
    $columns = [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'order' => DynaGrid::ORDER_FIX_LEFT
        // ],
        'id',
        [
            'attribute' => 'handler',
            'label' => 'Handler',
            'width'=>'350px',
            'value'=>function($model)
            {
                return isset($model->handler)?$model->handler::getName():'';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $data_handlers,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Handler', 'id' => 'grid-templates-search-templates']

        ],
        'name',
        'subject:ntext',
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/templates/view?id='.$model->id.'" title="View" aria-label="View" data-pjax="0">'.
                        '<span class="glyphicon glyphicon-eye-open"></span></a>';
                },
                'update' => function ($url, $model){
                    return '<a href="'.Yii::$app->params['baseUrl'].'/ia/templates/update?id='.$model->id.'" title="Update" aria-label="Update" data-pjax="0">'.
                        '<span class="glyphicon glyphicon-pencil"></span></a>';
                },
            ],
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/templates',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-info',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> SyStem Templates </h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']),
//                    (Yii::$app->user->can('reference.create') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create?type='.$type], ['class' => 'btn btn-success']) : '') .
//                    (Yii::$app->authManager->getAssignment('admin',\Yii::$app->user->id) ? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/templates/deleted-index?type='.$type], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Restore item page']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', '', ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-templates-system'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end(); ?>

</div>
