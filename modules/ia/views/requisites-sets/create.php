<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesSets $model
 */

$this->title = 'Create Requisites Sets';
$this->params['breadcrumbs'][] = ['label' => 'Requisites Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-sets-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">

                <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>