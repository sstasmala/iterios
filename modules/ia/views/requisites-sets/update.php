<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesSets $model
 */

$this->title = 'Update Requisites Sets: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Requisites Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requisites-sets-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">

                <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
