<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\RequisitesSets $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Requisites Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-sets-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'condensed' => false,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'attributes' => [
                'id',
                'name',
                [
                    'attribute' => 'tenant_id',
                    'value' => $model->tenant->name
                ],
            ],
            'enableEditMode' => false,
        ]) ?>
        <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </div>

</div>
