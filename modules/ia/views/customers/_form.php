<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 * @var yii\widgets\ActiveForm $form
 */

$tariffs = \yii\helpers\ArrayHelper::map(\app\models\Tariffs::find()->translate('en')->all(),'id','name');
$tariffs[null]='(empty)';
?>


<div class="tenants-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_STATIC, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
            'legal_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Legal Name...', 'maxlength' => 255]],
            'balance' => ['type' => Form::INPUT_STATIC, 'options' => ['placeholder' => 'Enter Balance...']],
            'tariff_id' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items'=>$tariffs
                , 'options' => ['placeholder' => 'Enter Tariff ID...']],
            'payment_method' => ['type' => Form::INPUT_DROPDOWN_LIST,'items'=>\app\utilities\payment\Payment::PAYMENT_METHODS,
                'options' => ['placeholder' => 'Enter Payment Method...']],
        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    echo '<span> </span>';
    echo (!$model->isNewRecord)? Html::a('Withdrawal',
        Yii::$app->params['baseUrl'].'/ia/financial-operations/create?type='.\app\models\FinancialOperations::OPERATION_WITHDRAWAL,
        ['class' => 'btn btn-info']):'';
    ActiveForm::end(); ?>

</div>
