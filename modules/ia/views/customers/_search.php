<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\search\Customers $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tenants-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'owner_id') ?>

    <?= $form->field($model, 'language_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'timezone') ?>

    <?php // echo $form->field($model, 'date_format') ?>

    <?php // echo $form->field($model, 'time_format') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'reminders_email') ?>

    <?php // echo $form->field($model, 'reminders_from_who') ?>

    <?php // echo $form->field($model, 'usage_currency') ?>

    <?php // echo $form->field($model, 'main_currency') ?>

    <?php // echo $form->field($model, 'system_tags')->checkbox() ?>

    <?php // echo $form->field($model, 'mailer_config') ?>

    <?php // echo $form->field($model, 'use_webmail') ?>

    <?php // echo $form->field($model, 'week_start') ?>

    <?php // echo $form->field($model, 'legal_name') ?>

    <?php // echo $form->field($model, 'balance') ?>

    <?php // echo $form->field($model, 'tariff_id') ?>

    <?php // echo $form->field($model, 'payment_method') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
