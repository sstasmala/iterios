<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Services $model
 */

$this->title = 'Create Service';
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/services',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'def' => $def,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>