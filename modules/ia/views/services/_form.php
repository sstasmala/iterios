<?php

use app\models\Languages;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Services $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerJsFile('@web/admin/js/pages/services/form.js',['depends'=>\app\assets\AdminAsset::class]);
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//$this->registerCssFile('@web/metronic/src/vendors/flaticon/css/flaticon.css');
//$this->registerCssFile('@web/metronic/src/vendors/socicon/css/socicon.css');
//$this->registerCssFile('@web/metronic/src/vendors/line-awesome/css/line-awesome.css');


function renderField($field,$index,$enabled = true)
{
    $data = '';
    $data .= '<li class="service-field callout box"  data-field-id="'.$field['id'].'" style="border: 5px solid #eee;'.((!$enabled)?'background-color: #8f8692':'background-color: white')
        .'"><div class="row">'
        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][id]" value="'.Html::encode($field['id']).'">'
        .'<div class="col-md-1"><label class="control-label">Name</label></div><div class="col-md-3">'
        .'<input'
        .((!$enabled)?' readOnly':'')
        .' type="text" class="form-control" name="ServicesFieldsDefault['.$index.'][name]" value="'.Html::encode($field['name']).'"></div>'
        .'<div class="col-md-1"><label class="control-label">Code</label></div><div class="col-md-3">'
        .'<input'
        .((!$enabled)?' readOnly':'')
        .' type="text" class="form-control" name="ServicesFieldsDefault['.$index.'][code]" value="'.Html::encode($field['code']).'"></div>'
        .'<div class="col-md-1"><label class="control-label">Type</label></div><div class="col-md-3">'
        .'<select'.((!$enabled)?' disabled':'').' class="form-control select-type" name="ServicesFieldsDefault['.$index.'][type]">'
        .renderSelectOptions($field['type'])
        .'</select></div>'
        .'</div><br><div class="row">'

        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][in_header]" value="0"><label class="col-md-2">'
        .'<input' .((!$enabled)?' onclick="return false;"':'')
        .' type="checkbox" name="ServicesFieldsDefault['.$index.'][in_header]" value="1" '
        .Html::encode(($field['in_header']?'checked':'')).' aria-invalid="false"> In Header</label>'

        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][is_additional]" value="0"><label class="col-md-2">'
        .'<input' .((!$enabled)?' onclick="return false;"':'')
        .' type="checkbox" name="ServicesFieldsDefault['.$index.'][is_additional]" value="1" '
        .Html::encode(($field['is_additional']?'checked':'')).' aria-invalid="false"> Is Additional</label>'

        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][only_for_order]" value="0"><label class="col-md-2">'
        .'<input' .((!$enabled)?' onclick="return false;"':'')
        .' type="checkbox" name="ServicesFieldsDefault['.$index.'][only_for_order]" value="1" '
        .Html::encode(($field['only_for_order']?'checked':'')).' aria-invalid="false"> Only for order</label>'

        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][in_creation_view]" value="0"><label class="col-md-2">'
        .'<input' .((!$enabled)?' onclick="return false;"':'')
        .' type="checkbox" name="ServicesFieldsDefault['.$index.'][in_creation_view]" value="1" '
        .Html::encode(($field['in_creation_view']?'checked':'')).' aria-invalid="false"> In creation view</label>'

        .'<input type="hidden" name="ServicesFieldsDefault['.$index.'][is_bold]" value="0"><label class="col-md-2">'
        .'<input' .((!$enabled)?' onclick="return false;"':'')
        .' type="checkbox" name="ServicesFieldsDefault['.$index.'][is_bold]" value="1" '
        .Html::encode(($field['is_bold']?'checked':'')).' aria-invalid="false"> Is bold</label>'

        .'</div><div class="row">'
        .'<div class="col-md-1"><label class="control-label">Size</label></div><div class="col-md-3">'
        .'<select'.((!$enabled)?' disabled':'').' class="form-control" name="ServicesFieldsDefault['.$index.'][size]" placeholder="Enter Size..." aria-invalid="false">
            <option value="1" '.(($field['size']==1)?'selected':'').'>1</option>
            <option value="2" '.(($field['size']==2)?'selected':'').'>2</option>
            <option value="3" '.(($field['size']==3)?'selected':'').'>3</option>
            <option value="4" '.(($field['size']==4)?'selected':'').'>4</option>
            <option value="5" '.(($field['size']==5)?'selected':'').'>5</option>
            <option value="6" '.(($field['size']==6)?'selected':'').'>6</option>
            <option value="7" '.(($field['size']==7)?'selected':'').'>7</option>
            <option value="8" '.(($field['size']==8)?'selected':'').'>8</option>
            <option value="9" '.(($field['size']==9)?'selected':'').'>9</option>
            <option value="10" '.(($field['size']==10)?'selected':'').'>10</option>
            <option value="11" '.(($field['size']==11)?'selected':'').'>11</option>
            <option value="12" '.(($field['size']==12)?'selected':'').'>12</option>
            </select>'
        .'</div>'

        .('<div class="col-md-8"><button class="remove-field btn btn-danger pull-right">Remove</button></div>')
        .((!$enabled)?'':'<br><div class="variants-box col-md-offset-2 col-md-10" style="margin-top: 15px;"></div>')
        .'</div>'
        .'<input type="hidden" class="variables-input" name="ServicesFieldsDefault['.$index.'][variables]" value="'.Html::encode($field['variables']).'">'
        .'<input type="hidden" name="ServicesFields['.$index.'][position]" value="'.$index.'">'
        .'<div class="overlay" style="display:none">
              <i class="fas fa-sync fa-spin"></i>
            </div>'
        .'</li>';
    return $data;
}


function renderSelectOptions($value)
{
    $data = '';
    $options = \app\models\ServicesFieldsDefault::TYPES;
    foreach ($options as $v => $option)
    {
        if($v == $value)
            $data .= '<option value="'.$v.'" selected>'.$option.'</options>';
        else
            $data .= '<option value="'.$v.'">'.$option.'</options>';
    }
    return $data;
}
$options = \app\models\ServicesFieldsDefault::TYPES;
$this->registerJs("var types=".json_encode($options).";",\yii\web\View::POS_HEAD);
$icons = require \Yii::$app->basePath.'/data/icons.map.php';

if($model->isNewRecord){
    $linked_ids = [];
    $not = [];

}
else {
    $linked_ids = array_column(\app\models\ServicesLinks::find()->where(['parent_id' => $model->id])->asArray()->all(), 'child_id');
    $not = [$model->id];
}
$available_services = \app\models\Services::find()->where(['not in','id',$not])
    ->translate(Yii::$app->session->get('lang',\app\models\Languages::getDefault())->iso)
    ->asArray()->all();
//$data = [];
//foreach ($icons as $i)
//{
//    $data[] = ['id'=>$i,'text'=>$i];
//}
//$this->registerJs("var icons=".json_encode($data).";",\yii\web\View::POS_HEAD);
?>

<div class="services-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    echo $form->errorSummary($model,['class'=>'callout callout-danger']);
    echo '<div class="row"><div class="col-md-6">';
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 500]],

            'icon' => ['type' => Form::INPUT_DROPDOWN_LIST, 'options' => ['placeholder' => 'Enter Icon...','rows' => 6],'items'=>array_combine($icons,$icons)],

            'multiservices' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter multiservices...']],

            'multiservices_name' => ['label'=>'MS_Name','type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

        ]

    ]);
    echo '<label class="control-label col-md-2" >Fields</label>';
    echo '</div>';
    echo '<div class="col-md-6">';
    echo '<select class="select-services" name="ServicesLinks[child_id][]" multiple>';
    foreach ($available_services as $a)
        if(in_array($a['id'],$linked_ids))
            echo '<option value="'.$a['id'].'" selected>'.$a['name'].'</option>';
        else
            echo '<option value="'.$a['id'].'">'.$a['name'].'</option>';
    echo '</select></div>';
    echo '</div>';
    echo '<ul id="services-fields" class="callout bg-gray">';
    $def = \app\models\ServicesFieldsDefault::find()->where(['is_default'=>1])->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->asArray()->all();
    $i = 0;
    $default = '';
    $add = '';
    if($model->isNewRecord)
    foreach ($def as $in => $item)
    {
        echo renderField($item,$i,false);
        $i++;
    }
    else
    {
        $additionals = \app\models\ServicesFields::find()->where(['id_service'=>$model->id])->with('field')->orderBy('position')->all();

        foreach ($additionals as $additional){
            $field = $additional->field;
            $field = $field->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
            if(!$field->is_default)
            {
                $add .= renderField($field->toArray(),$i,true);
                $i++;
            }
            else{
                $default .= renderField($field->toArray(),$i,false);
                $i++;
            }
        }

        echo $default;

    }
    echo '<div class="sortable">'.$add.'</div>';
    echo '</ul>';
    echo '<div class="row"><div class="col-md-6"><button id="add-field" class="btn btn-success">Add</button>';
//    echo ' or <select id="available" placeholder="Choose">';
//    /**
//     * @var $available \app\models\ServicesFieldsDefault[]
//     */
////    echo '<option value=""></option>';
//    foreach ($available as $a)
//    {
//        if(!$a->is_default)
//            echo '<option value="'.json_encode($a->toArray()).'">'.$a->code.'</option>';
//    }
//    echo '</select>';
    echo '</div></div><br>';

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
