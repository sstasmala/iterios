<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TemplatesHandlers $searchModel
 */

$this->title = 'Templates Handlers';
$this->params['breadcrumbs'][] = $this->title;
$handlers = \app\helpers\TemplatableHandlersHelper::getHandlers(Yii::$container->get('systemEmitter'));
$data_handlers = [];
foreach ($handlers as $handler)
    $data_handlers[$handler] = $handler::getName();
?>
<div class="templates-handlers-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'template_id',
            'label' => 'Template',
            'value' => function($model){
                if ($model->template)
                {return $model->template->name;}
                else
                {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\Templates::find()
                ->orWhere(['like','type','system'])->orWhere(['like','type','systemDefault'])
                ->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Templates', 'id' => 'grid-templates-handlers-search-template_id']
        ],
        [
            'attribute' => 'handler',
            'label' => 'Handler',
            'value'=>function($model)
            {
                return ($model->handler)::getName();
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $data_handlers,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Handler', 'id' => 'grid-templates-handlers-search-handler']

        ],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>

    <?php $dynagrid = DynaGrid::begin([
        'columns' => $gridColumn,
        'theme' => 'panel-info',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel' => [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Partners</h3>',
                'before' => '',
                'after' => false
            ],
            'toolbar' => [
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']).
//                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/partners/deleted-index'], ['data-pjax' => 0, 'class' => 'btn btn-success', 'title' => 'Restore item page']) .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['/templates-handlers'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                ],
                ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id' => 'dynagrid-templates-handlers'] // a unique identifier is important
    ]);

    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end(); ?>

</div>
