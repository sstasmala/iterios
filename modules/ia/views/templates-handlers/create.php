<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TemplatesHandlers $model
 */

$this->title = 'Create Templates Handlers';
$this->params['breadcrumbs'][] = ['label' => 'Templates Handlers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-handlers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
