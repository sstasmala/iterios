<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\TemplatesHandlers $model
 */

$this->title = $model->template->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates Handlers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-handlers-view">
    <div class="row">
        <div class="col-sm-3" style="margin-bottom: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'template',
            'label' => 'Template',
            'value' => $model->template->name
        ],
        [
            'label' => 'Handler',
            'value'=>($model->handler)::getName()
        ]
    ];
    ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Template handler info</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                    ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Template preview</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php $error = null; $template = $model->template?>
                    <?php if(!is_null($template)):?>
                        <?php

                        try
                        {
                            \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->body,true);
                            \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->subject,true);
                        }
                        catch(Exception $e)
                        {
                            $error = $e->getMessage();
                        }
                        ?>
                    <?php endif;?>
                    <div id="warning">
                        <?php if(!is_null($error)):?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                                <?= $error?>
                            </div>
                        <?php endif;?>
                        <?php if(!is_null($template)):?>
                    </div>
                <?php $preview = \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->body)?>
                <?php $preview_header = \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->subject)?>
                    <iframe id="preview-header" style="width: 100%" height="50" sandbox="" srcdoc="<?=$preview_header?>"></iframe>
                    <iframe id="preview-body" style="width: 100%" height="500" sandbox="" srcdoc="<?=Html::encode($preview)?>"></iframe>
                <?php endif;?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>
