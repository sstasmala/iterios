<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\models\TemplatesHandlers;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\TemplatesHandlers $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AppAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/templates-handlers/form.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$handlersAll = \app\helpers\TemplatableHandlersHelper::getHandlers(Yii::$container->get('systemEmitter'));
$data_handlers = [];

$freeHandler = TemplatesHandlers::find()
    ->select('handler')
    ->groupBy('handler')
    ->column();

$handlers = array_diff($handlersAll,$freeHandler);
if($model->handler!=null)$handlers[] = $model->handler;

foreach ($handlers as $handler)
    $data_handlers[$handler] = $handler::getName();

$items = $data_handlers;

$temp = \yii\helpers\ArrayHelper::map(\app\models\Templates::find()->orderBy('id')->asArray()->all(), 'id', 'name');
?>

<script>
    var handlers = <?= json_encode($data_handlers);?>;
</script>

<div class="row">
    <div class="templates-handlers-form col-md-6">
        <div class="box box-default">
            <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->errorSummary($model); ?>

                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <?= $form->field($model, 'template_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => $temp,
                    'options' => ['placeholder' => 'Choose Templates'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

                <?= $form->field($model, 'handler')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => $items,
                    'options' => ['placeholder' => 'Choose Handler'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="templates-handlers-form col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Preview</h3>
            </div>
            <div class="box-body">
                <?php $error = null; $template = $model->template?>
                <?php if(!is_null($template)):?>
                    <?php

                    try
                    {
                        \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->body,true);
                        \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->subject,true);
                    }
                    catch(Exception $e)
                    {
                        $error = $e->getMessage();
                    }
                    ?>
                <?php endif;?>
                <div id="warning">
                    <?php if(!is_null($error)):?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                            <?= $error?>
                        </div>
                    <?php endif;?>
                    <?php if(!is_null($template)):?>
                </div>
            <?php $preview = \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->body)?>
            <?php $preview_header = \app\helpers\TwigHelper::render(($model->handler)::getShortcodes(),$template->subject)?>
                <iframe id="preview-header" style="width: 100%" height="50" sandbox="" srcdoc="<?=$preview_header?>"></iframe>
                <iframe id="preview-body" style="width: 100%" height="500" sandbox="" srcdoc="<?=Html::encode($preview)?>"></iframe>
            <?php endif;?>
            </div>
        </div>
    </div>

