<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TemplatesHandlers $model
 */

$this->title = 'Update Templates Handlers: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Templates Handlers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="templates-handlers-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
