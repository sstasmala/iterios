<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 */

$this->title = 'Public holidays: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Public holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$months = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
$months = array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May", "6"=>"June", "7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December");
$user = \app\models\User::findOne($model->user_id);
?>
<div class="tasks-types-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/public-holidays',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'mounts',
                                'value' => (!empty($model->mounts) ? $months[$model->mounts] : '')
                            ],
                            'day',
                            [
                                'attribute' => 'user_id',
                                'label' => 'User',
                                'value' => (!empty($model->user_id) ? $user->first_name.' '.$user->last_name : '')
                            ],
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
