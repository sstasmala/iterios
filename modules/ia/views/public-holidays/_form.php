<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 */

$this->registerJsFile('@web/admin/js/pages/system-holidays/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
$this->registerCssFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

//$months = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
$months = array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May", "6"=>"June", "7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December");

$dayMounth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
?>
<script type="text/javascript">
    var dayMounth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
</script>


<div class="suppliers-form">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->errorSummary($model); ?>


            <div class="form-group field-publicholidays-name">
                <label class="control-label col-md-2" for="publicholidays-name">Mounth</label>
                <div class="col-md-10">
                    <select name="PublicHolidays[mounts]" id="HolidaysMounts" class="form-control">
                        <?php foreach ($months as $key=>$month) {
                            echo "<option value='{$key}'>{$month}</option>";
                        } ?>
                    </select>
                </div>
            </div>

            <div class="form-group field-publicholidays-name">
                <label class="control-label col-md-2" for="publicholidays-name">Day</label>
                <div class="col-md-10">
                    <select name="PublicHolidays[day]" id="HolidaysDay" class="form-control">
                        <?php
                        if($model->mounts){
                            for ($i=1;$i<=$dayMounth[$model->mounts];$i++) {
                                echo "<option value='{$i}'>{$i}</option>";
                            }
                        }else{
                            for ($i=1;$i<=31;$i++) {
                                echo "<option value='{$i}'>{$i}</option>";
                            }
                        }
                             ?>
                    </select>
                </div>
            </div>

            <?= $form->field($model, 'name'); ?>

            <div class="form-group field-user-id">
                <label class="control-label col-md-2" for="PublicHolidays[user_id]">User</label>
                <div class="col-md-10">
                    <select name="PublicHolidays[user_id]" id="UserId" class="form-control">
                        <?php
                                $users = \app\models\User::find()->all();
                                foreach ($users as $user){
                                    if ($model->user_id != $user->id) {
                                        echo "<option value='{$user->id}'>".$user->first_name.' '.$user->last_name."</option>";
                                    }else{
                                        echo "<option value='{$user->id}' selected >".$user->first_name.' '.$user->last_name."</option>";
                                    }
                        }
                        ?>
                    </select>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <?php
            echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            );
            ?>
            <?php ActiveForm::end(); ?>
        </div>
</div>