<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\DocumentPlaceholders;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 */

// CKEditor
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/placeholders-document/form.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

if (!empty($model->placeholder_id)) {
    $ph_document = \app\models\Placeholders::find()
        ->where(['id' => $model->placeholder_id])
        ->translate('en')
        ->asArray()->one();
}
?>

<div class="document-placeholders-form">

    <div class="row">

        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>
                    <?= $form->errorSummary($model); ?>
                    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                    <div class="form-group">
                        <label class="control-label" for="placeholders_doc-id">Placeholder</label>
                        <select class="form-control" id="placeholders_doc-id" name="PlaceholdersDocument[placeholder_id]">
                            <?php if (!empty($model->placeholder_id)): ?>
                                <option value="<?= $model->placeholder_id ?>" data-type="<?= $ph_document['type_placeholder'] ?>" data-module="<?= \array_key_exists($ph_document['module'], \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$ph_document['module']] : '' ?>" data-module_class="<?= $ph_document['module'] ?>">
                                    <?= $ph_document['short_code'] ?> <?= ($ph_document['fake'] !== true ? ' - ' . \app\models\Placeholders::TYPE_PLACEHOLDERS[$ph_document['type_placeholder']] : '(Fake)') ?>
                                </option>
                            <?php endif; ?>
                        </select>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-placeholdersdocument-template" style="display:none;">
                        <label class="control-label" for="placeholdersdocument-template">Template</label>
                        <textarea id="placeholdersdocument-template" class="document-editor form-control" name="PlaceholdersDocument[template]"><?= $model->template ?></textarea>
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    ); ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <div class="col-md-6 mfield" style="display: none;">
            <div class="placeholder-index">
                <div id="dynagrid-placeholders">
                    <div id="w6" class="grid-view hide-resize">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Module fields</h3>
                                <div class="clearfix"></div>
                            </div>
                            <div id="w6-container" class="kv-grid-container" style="overflow: auto">
                                <table class="kv-grid-table table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="kv-align-center kv-align-middle skip-export" data-col-seq="0">Copy</th>
                                            <th data-col-seq="1">Code</th>
                                            <th data-col-seq="2">Field</th>
                                            <th data-col-seq="3">Module</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>