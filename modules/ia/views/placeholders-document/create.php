<?php

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Create placeholder document';
$this->params['breadcrumbs'][] = ['label' => 'Placeholders Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placeholder_document-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
