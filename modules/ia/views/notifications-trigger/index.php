<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NotificationsTrigger $searchModel
 */

$this->title = 'Reminder calendar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-trigger-index">
    <?php
        $actions = \app\models\NotificationsTrigger::find()->select(['action'])->distinct()->orderBy(['action' => SORT_ASC])->asArray()->all();
        $action_filter = [];
        foreach($actions as $act) {
            $action_filter[$act['action']] = $act['action'];
        }
        
        $type_filter = [
            0 => 'Event',
            1 => 'Condition'
        ];

        $columns = [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:20px;']
            ],

            [
                'attribute' => 'element_id',
                'contentOptions' => ['style' => 'width:20px;']
            ],
            [
                'attribute'=>'action',
                'filter'=> $action_filter,
            ],
            [
                'attribute'=>'type',
                'filter'=> $type_filter,
            ],

            'value',
            //'value_int',

            [
                'attribute' => 'trigger_time',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            'trigger_day',

            [
                'attribute' => 'agent_id',
                'contentOptions' => ['style' => 'width:20px;']
            ],
            [
                'attribute' => 'contact_id',
                'contentOptions' => ['style' => 'width:20px;']
            ],
            [
                'attribute' => 'tenant_id',
                'contentOptions' => ['style' => 'width:20px;']
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'header' => 'Actions',
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $res = \app\models\NotificationsCourse::find()->select(['trigger_id'])->distinct()->where(['trigger_id' => $model->id])->asArray()->all();

                        return !empty($res) ? '<a href="'.Yii::$app->params['baseUrl'].'/ia/reminder-calendar/view?id='.$model->id.'" title="View" aria-label="View" data-pjax="0">'.
                            '<span class="glyphicon glyphicon-eye-open"></span></a> ' : '';
                    }
                ],
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                   
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
