<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsTrigger $model
 */

$this->title = 'Create Notifications Trigger';
$this->params['breadcrumbs'][] = ['label' => 'Notifications Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-trigger-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
