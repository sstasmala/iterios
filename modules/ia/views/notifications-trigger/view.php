<?php
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsTrigger $model
 */

$this->title = $model->action;
$this->params['breadcrumbs'][] = ['label' => 'Notifications Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-trigger-view">
    <?
        $gridColumn = [
            'id',
            'action',
            'element_id',
            'value',
            'type',
            'value_int',
            'trigger_time:datetime',
            'agent_id',
            'contact_id',
            'tenant_id',
            'status',
            'data:ntext',
            'created_at',
            'updated_at',
        ];
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Trigger view</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                    ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Queue By Trigger</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?
                        $courses = \app\models\NotificationsCourse::find()->where(['trigger_id' => $model->id])->asArray()->all();
                    ?>
                    
                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                            <th>Reminder Id</th>
                            <th>Date Server</th>
                            <th>Date User</th>
                            <th>Status</th>
                        </thead>
                        <body>
                            <? foreach($courses as $course): ?>
                                <tr>
                                    <td><?= $course['noting_id']; ?></td>
                                    <td><?= $course['date']; ?></td>
                                    <td><?= $course['timezone_date']; ?></td>
                                    <td><?= ($course['send_status'] == 1) ? '<span class="label label-success">send</span>' : '<span class="label label-danger">no send</span>'; ?></td>
                                </tr>
                            <? endforeach; ?>
                        </body>
                    </table>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Logs By Trigger</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?
                        $logs = \app\models\NotificationsLog::find()->where(['trigger_id' => $model->id])->asArray()->all();
                    ?>
                    
                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                            <th>Reminder</th>
                            <th>Contact</th>
                            <th>Agent</th>
                            <th>Channel</th>
                            <th>Errors</th>
                            <th>Date</th>
                            <th>Status</th>
                        </thead>
                        <body>
                            <? foreach($logs as $log):?>
                                <tr>
                                    <td><?= $log['reminder_id']; ?></td>
                                    <td><?= $log['contact_id']; ?></td>
                                    <td><?= $log['agent_id']; ?></td>
                                    <td><?= $log['channel']; ?></td>
                                    <td><?= $log['errors']; ?></td>
                                    <td><?= date('Y-m-d H:i:s', $log['created_at']); ?></td>
                                    <td><?= ($log['status'] == 1) ? '<span class="label label-success">send</span>' : '<span class="label label-danger">no send</span>'; ?></td>
                                </tr>
                            <? endforeach; ?>
                        </body>
                    </table>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>
