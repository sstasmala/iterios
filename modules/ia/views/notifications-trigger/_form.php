<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsTrigger $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="notifications-trigger-form">
    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    $columns = [

        'action' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Action...', 'maxlength' => 255]],

        'element_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Element ID...']],

        'value_int' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value Int...']],

        'type' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Int 0-action,1-condition']],

        'trigger_time' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Trigger Time...']],

        'agent_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Agent ID...']],

        'contact_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Contact ID...']],

        'created_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Created At...']],

        'updated_at' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Updated At...']],

        'data' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Data...', 'rows' => 6]],

        'value' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...', 'maxlength' => 255]],

    ];
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $columns

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
