<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationsTrigger $model
 */

$this->title = 'Update Notifications Trigger: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notifications Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notifications-trigger-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
