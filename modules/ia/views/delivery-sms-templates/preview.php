<?php

use app\helpers\PreviewShortcodesHelper;

/**
 * @var yii\web\View $this
 * @var app\models\TemplatesHandlers $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Delivery SMS Template Preview', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/delivery-sms-templates/ckeditor-init-preview.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
?>


<div class="documents-templates-preview">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Delivery SMS Template preview</h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        if (null !== $model) {
                            try {
                                $preview = PreviewShortcodesHelper::render(\app\models\DeliveryPlaceholders::getShortcodes(), $model->body);

                                if (!empty($preview))
                                    echo '<textarea name="preview-body" id="preview-body">'. $preview .'</textarea>';
                            } catch (Exception $e) {
                                $error = 'You write bad placeholders, please edit document template';

                                echo '<div id="warning">' . $error . '</div>';
                            }
                        }
                    ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>
