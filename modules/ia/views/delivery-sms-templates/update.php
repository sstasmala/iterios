<?php

use app\models\Languages;

/**
 * @var yii\web\View $this
 * @var app\models\DeliverySmsTemplates $model
 */

$this->title = 'Update Delivery SMS Template ' . (Yii::$app->controller->id === 'delivery-sms-templates-system' ? 'system' : 'public') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="delivery-sms-templates-update">
    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang
    ]) ?>
</div>
