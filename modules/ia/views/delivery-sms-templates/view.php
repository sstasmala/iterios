<?php

use app\models\Languages;
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\DeliverySmsTemplates $model
 */

$this->title = 'Delivery SMS Templates '.(Yii::$app->controller->id === 'delivery-sms-templates-system' ? 'system' : 'public') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-sms-templates-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/delivery-sms-templates-system',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        'id',
                        'name',
                        [
                            'attribute' => 'delivery_type_id',
                            'format' => 'raw',
                            'value' => Html::a($model->deliveryType->name, '/ia/delivery-types/view?id='.$model->delivery_type_id, ['target' => '_blank'])
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => ($model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>')
                        ],
                        'body:ntext',
                        'type',
                        [
                            'attribute' => 'is_default',
                            'format' => 'raw',
                            'value' => ($model->is_default ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>')
                        ]
                    ];

                    if (Yii::$app->controller->id !== 'delivery-email-templates-system') {
                        $columns[] = [
                            'attribute' => 'tenant_id',
                            'label' => 'Tenant',
                            'format' => 'raw',
                            'value' => null === $model->tenant ? null : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank'])
                        ];
                    }

                    $columns_second = [
                        [
                            'attribute' => 'created_at',
                            'containerOptions' => ['class'=>'to-datetime-convert'],
                            'valueColOptions' => ['class'=>'to-datetime-convert'],
                            'value' => null === $model->created_at ? '' : $model->created_at
                        ],
                        [
                            'attribute' => 'updated_at',
                            'valueColOptions' => ['class'=>'to-datetime-convert'],
                            'value' => null === $model->updated_at ? '' : $model->updated_at
                        ],
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                        ]
                    ];

                    $columns = array_merge($columns, $columns_second);
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
//                    'panel' => [
//                        'heading' => $this->title,
//                        'type' => DetailView::TYPE_INFO,
//                    ],
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Preview', ['preview','id' => $model->id], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Update', ['update','id' => $model->id, 'DeliveryPlaceholders[type]' => $model->delivery_type_id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
