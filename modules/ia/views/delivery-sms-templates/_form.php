<?php

use app\models\Languages;
use app\models\search\Placeholders;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\DeliverySmsTemplates $model
 * @var yii\widgets\ActiveForm $form
 */

// CKEditor
//$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//$this->registerJsFile('@web/admin/js/pages/delivery-sms-templates/ckeditor-init.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/delivery-sms-templates/items.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//
$searchModelSms = new Placeholders;
$dataProviderSms = $searchModelSms->search(Yii::$app->request->getQueryParams(),
    Yii::$app->session->get('lang', Languages::getDefault())->iso, Placeholders::TYPE_DELIVERY);
function getPath($model) {
    $path = '';

    if (!empty($model->module)) {
        switch (\app\models\DeliveryPlaceholders::USED_MODULES[$model->module]) {
            case \app\models\DeliveryPlaceholders::MODULE_TRANSLATION:
                $translation = \app\models\UiTranslations::find()
                    ->where(['id' => (int) $model->path])
                    ->translate('en')
                    ->one();

                if ($translation !== null)
                    $path = $translation->code . ' - '.$translation->value;

                break;
            case \app\models\DeliveryPlaceholders::MODULE_SERVICE:
                $field = \app\models\ServicesFieldsDefault::findOne((int) $model->path);

                if ($field !== null)
                    $path = $field->code .'/'.$field->name;

                break;
        }
    }

    return $path;
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="w1">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' =>  $model->isNewRecord ? 'create' : 'update',
                        'model_controller_path' => '/ia/delivery-sms-templates-system',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <div class="delivery-sms-templates-form" data-template_id="<?= $model->id ?>">

                        <?php
                        $columns = [
                            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
                            'delivery_type_id' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => '\kartik\widgets\Select2',
                                'options' => [
                                    'value' => $model->delivery_type_id,
                                    'data' => \yii\helpers\ArrayHelper::map(\app\models\DeliveryTypes::find()->where(['for' => 'sms'])->orderBy(['id' => SORT_ASC])->all(), 'id', 'name'),
                                    'options' => [
                                        'placeholder' => 'Choose delivery type',
                                    ],
                                    'pluginOptions' => [
                                        'minimumResultsForSearch' => '-1'
                                    ],
                                ]
                            ],
                            'body' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Body...', 'class'=>'document-editor']],
                            'status' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => ''], 'label' => 'Status (on/off)'],
                        ];

                        $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]); echo Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 1,
                            'attributes' => $columns
                        ]);

                        echo $form->field($model, 'is_default')->checkbox();

                        echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                        );
                        ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="w1 delivery-placeholders-index">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Template placeholders</h3>
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        [
                            'class' => 'kartik\grid\ActionColumn',
                            'dropdown' => false,
                            'buttons' => [
                                'copy' => function ($url, $model){
                                    return '<a href="#" title="Copy" aria-label="Copy" data-pjax="0" data-pl_id="'.$model->id.'">'.
                                        '<span class="btn btn-success btn-sm copy_mark_btn copy_button"><span class="fa fa-copy"></span></span></a>';
                                },
                            ],
                            'template' => '{copy}',
                        ],
                        [
                            'attribute' => 'short_code',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<span id="placeholder_'.$model->id.'">{{'.$model->short_code.'}}</span>';
                            }
                        ],
                        [
                            'attribute' => 'type_placeholder',
                            'value' => function($model) {
                                return !empty($model->type_placeholder) && in_array($model->type_placeholder, \app\models\Placeholders::TYPE_PLACEHOLDERS_KEY, true) ? \app\models\Placeholders::TYPE_PLACEHOLDERS[$model->type_placeholder] : '(not set)';
                            }
                        ],
                        [
                            'attribute' => 'module',
                            'format' => 'raw',
                            'value'=> function ($model) {
                                return !empty($model->module) && \array_key_exists($model->module, \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$model->module] : '(not set)';
                            }
                        ],
                        [
                            'attribute' => 'field',
                            'format'=>'raw',
                            'value' => function ($model) {
                                return $model->fake ? '<span class="label label-danger">Fake</span>' : (!empty($model->field) ? $model->field : '(not set)');
                            }
                        ],
                        'default',
                    ];

                    $dynagrid = DynaGrid::begin([
                        'columns' => $columns,
                        'theme'=>'panel-success',
                        'showPersonalize' => true,
                        'storage' => 'session',
                        'gridOptions' => [
                            'dataProvider' => $dataProviderSms,
                            'floatHeader' => false,
                            'pjax' => true,
                            'responsiveWrap' => false,
                            'responsive' => false,
                            'containerOptions' => ['style' => 'overflow: auto'],
                        ],
                        'options' => ['id'=>'dynagrid-placeholders'] // a unique identifier is important
                    ]);
                    if (substr($dynagrid->theme, 0, 6) == 'simple') {
                        $dynagrid->gridOptions['panel'] = false;
                    }
                    DynaGrid::end();
                    ?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>