<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\DeliverySmsTemplates $model
 */

$this->title = 'Create Delivery SMS Template ' . (Yii::$app->controller->id === 'delivery-sms-templates-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = ['label' => 'Delivery Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-sms-templates-create">
    <?= $this->render('_form', [
        'model' => $model,
        'def_lang' => $def_lang
    ]) ?>
</div>
