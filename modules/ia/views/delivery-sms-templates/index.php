<?php

use app\models\Languages;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\DeliverySmsTemplates $searchModel
 */

$this->title = 'Delivery SMS Templates '.(Yii::$app->controller->id === 'delivery-sms-templates-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = $this->title;

$lang_select = $this->render('///layouts/admin/_language_select', [
    'language' => $def_lang,
    'action' => 'index',
    'model_controller_path' => '/ia/delivery-sms-templates-system',
]);
?>
<div class="delivery-sms-templates-index">
    <?php
    $columns = [
        'id',
        'name',
        [
            'attribute' => 'delivery_type_id',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->delivery_type_id !== null ? Html::a($model->deliveryType->name, '/ia/delivery-types/view?id='.$model->delivery_type_id, ['target' => '_blank']) : '';
            }
        ],
        [
            'attribute' => 'type',
            'value' => function ($model) {
                return ($model->type ? ucfirst($model->type) : '(not set)');
            }
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'status',
            'vAlign' => 'middle',
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'is_default',
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'tenant_id',
            'label' => 'Tenant',
            'format' => 'raw',
            'visible' => Yii::$app->controller->id !== 'delivery-email-templates-system',
            'value' => function ($model) {
                return null === $model->tenant ? null : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (is_null($model->created_at) ? '' : $model->created_at);
            }
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (is_null($model->updated_at) ? '' : $model->updated_at);
            }
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                return (is_null($model->created_by)) ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return (is_null($model->updated_by)) ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'update' => function ($url, $model){
                    return '<a href="'.$url.'&DeliveryPlaceholders[type]='.$model->delivery_type_id.'" title="Update" aria-label="Update" data-pjax="0">'.
                        '<span class="glyphicon glyphicon-pencil"></span></a>';
                },
            ],
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                     (Yii::$app->controller->id === 'delivery-sms-templates-system' ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
//                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
