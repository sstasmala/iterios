<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ReminderEventTypes $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="reminder-event-types-form">


    <?php

    $colors = [];

    foreach (\app\models\ReminderEventTypes::COLOR_TYPES as $color)
        $colors[$color] = '<span class="pull-right label label-'.$color.'">'.$color.'</span>';



    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
    $columns = [

        //'action' => ['type' => Form::INPUT_TEXT , 'options' => ['placeholder' => 'Enter Action...', 'maxlength' => 255]],

        'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

        'icon_class' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Icon...', 'maxlength' => 255]],

        //'color_type' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Color...', 'maxlength' => 255]],

    ];


    echo $form->field($model, 'action')->dropdownList($actions);

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $columns
    ]);

    echo $form->field($model, 'color_type')->radioList($colors);

    echo $form->field($model, 'status')->checkbox(['label' => 'ON/OFF']);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
