<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ReminderEventTypes $model
 */

$this->title = 'Create Reminder Event Types';
$this->params['breadcrumbs'][] = ['label' => 'Reminder Event Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminder-event-types-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'actions' => $actions
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
