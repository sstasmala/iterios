<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ReminderEventTypesSearch $searchModel
 */

$this->title = 'Reminder Event Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminder-event-types-index">

    <?php
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'action',
            'name',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->status == 1 ? '<span class="label label-success">ON</span>':'<span class="label label-danger">OFF</span>';
                }
            ],
            [
                'attribute' => 'color_type',
                'label' => 'Color',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<span class="label label-'.$model->color_type.'">'.$model->color_type.'</span>';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['ia/reminder-event-types/update', 'id' => $model->id, 'edit' => 't']),
                            ['title' => Yii::t('yii', 'Edit'),]
                        );
                    }
                ],
            ],
        ];
    ?>

    <?php 

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
    ]);

    $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Reminder Event Types</h3>',
                    'before' => $lang_select,
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
    //                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end(); ?>

</div>
