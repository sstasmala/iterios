<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ReminderEventTypes $model
 */

$this->title = 'Update Reminder Event Types: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reminder Event Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reminder-event-types-update">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'actions' => $actions
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
