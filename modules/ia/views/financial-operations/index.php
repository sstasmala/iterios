<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\FinancialOperations $searchModel
 */

$this->title = 'Financial Operations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financial-operations-index">
    <?php
    $columns = [
        'id',
        [
            'attribute' => 'payment_method',
            'value' => function($model)
            {
                if($model->payment_method !== null)
                    return \app\utilities\payment\Payment::PAYMENT_METHODS[$model->payment_method];
                return null;
            }
        ],
//        'account_number',
//        'transaction_number',
        [
            'attribute' => 'operation_type',
            'value' => function($model)
            {
                if($model->operation_type !== null)
                    return \app\models\FinancialOperations::OPERATIONS[$model->operation_type];
                return null;
            }
        ],
        [
            'attribute' => 'customer_id',
            'value' => function($model)
            {
                if($model->customer_id !== null)
                    return $model->customer->name;
                return null;
            }
        ],
//            'payment_gateway_id',
//            'comment:ntext',
        'sum',
        [
            'attribute' => 'currency',
            'value' => function($model)
            {
                if($model->currency !== null)
                    return \app\helpers\MCHelper::getCurrencyById($model->currency,'en')[0]['iso_code'];
                return null;
            }
        ],
        'sum_usd',
//            'date',
//            'owner',
//            'created_at',
//            'updated_at',
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view}{update}{delete}'
        ]
    ];

    $lang_select ='';

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Financial Operations</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    //(true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['update-translations'], ['data-pjax'=>0, 'class' => 'btn btn-danger', 'title'=>'Refresh data from config']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-financial-operations'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
