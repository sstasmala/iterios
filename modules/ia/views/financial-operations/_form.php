<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\FinancialOperations $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerAssetBundle(\app\assets\InputmaskAsset::className());
$this->registerJsFile('@web/admin/js/pages/financial-operations/financial-operations.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

?>

<div class="financial-operations-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'payment_method' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>\app\utilities\payment\Payment::PAYMENT_METHODS,
                'options' => ['placeholder' => 'Enter Payment Method...']],

            'transaction_number' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Transaction Number...']],

            'account_number' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Account Number...', 'maxlength' => 255]],

            'operation_type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>\app\models\FinancialOperations::OPERATIONS,
                'options' => ['placeholder' => 'Enter Operation Type...']],

//            'customer_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Customer ID...']],

//            'payment_gateway_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Payment Gateway ID...']],

//            'currency' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Currency...']],

//            'date' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Date...']],

//            'owner' => ['type' => Form::INPUT_HIDDEN_STATIC, 'options' => ['placeholder' => 'Enter Owner...']],

            'comment' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Comment...','rows' => 6]],

            'sum' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Sum...']],

            'sum_usd' => ['type' => Form::INPUT_STATIC, 'options' => ['placeholder' => 'Enter Sum Usd...']],

        ]

    ]);?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2" for="tariffs-description">Currency</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="currency-select" placeholder="Select Currency..." name="FinancialOperations[currency]">
                        <?php if(isset($model->currency)):?>
                            <option selected value="<?=$model->currency?>"><?= \app\helpers\MCHelper::getCurrencyById($model->currency,'en')[0]['iso_code']?></option>
                        <?php endif;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2">Customer</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="customer-select" placeholder="Enter Customer..." name="FinancialOperations[customer_id]">
                        <?php if(isset($model->customer_id)):?>
                            <option value="<?=$model->customer_id?>" selected><?= $model->customer->name?></option>
                        <?php endif;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2">Payment gateway</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="payment-gateways-select" placeholder="Enter Customer..." name="FinancialOperations[payment_gateway_id]">
                        <?php if(isset($model->payment_gateway_id)):?>
                            <option value="<?=$model->payment_gateway_id?>" selected><?= $model->paymentGateway->name?></option>
                        <?php endif;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>



    <?=$form->field($model,'date')->widget(\kartik\widgets\DateTimePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd hh:ii:00'
    ]])?>

    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
