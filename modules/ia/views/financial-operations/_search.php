<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\search\FinancialOperations $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="financial-operations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'payment_method') ?>

    <?= $form->field($model, 'account_number') ?>

    <?= $form->field($model, 'transaction_number') ?>

    <?= $form->field($model, 'operation_type') ?>

    <?php // echo $form->field($model, 'customer_id') ?>

    <?php // echo $form->field($model, 'payment_gateway_id') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'sum') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'sum_usd') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'owner') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
