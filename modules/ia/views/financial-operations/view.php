<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\FinancialOperations $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Financial Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financial-operations-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            'payment_method',
                            'account_number',
                            'transaction_number',
                            'operation_type',
                            'customer_id',
                            'payment_gateway_id',
                            'comment:ntext',
                            'sum',
                            'currency',
                            'sum_usd',
                            'date',
                            'owner',
                            [
                                'attribute'=>'created_at',
                                'containerOptions'=>['class'=>'to-datetime-convert'],
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute'=> 'updated_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ]
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                    <?=Html::a('Update', ['update?id='.$model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
