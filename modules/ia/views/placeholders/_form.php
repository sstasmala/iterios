<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\CustomPlaceholders $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/placeholders/form.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$fields = \app\modules\ia\controllers\PlaceholdersController::getFields($model->module);
?>

<div class="custom-placeholders-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);

    $columns = [
        'short_code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Short Code...', 'maxlength' => 255]],
        'type_placeholder' => [
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => '\kartik\widgets\Select2',
            'options' => [
                'data' => \app\models\Placeholders::TYPE_PLACEHOLDERS,
                'options' => [
                    'placeholder' => 'Choose type_placeholder',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => '-1'
                ],
            ]
        ],
        'module' => [
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => '\kartik\widgets\Select2',
            'options' => [
                'data' => \Yii::$app->params['placeholders'],
                'options' => [
                    'placeholder' => 'Choose module',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => '-1'
                ],
            ]
        ],
    ];

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $columns
    ]); ?>

    <div class="form-group">
        <label class="control-label col-md-2" for="placeholders-delivery_path">Field</label>
        <div class="col-md-10">
            <select class="form-control" id="placeholders-field" name="Placeholders[field]">
                <?php if (!empty($fields) && \array_key_exists($model->field, $fields)): ?>
                    <option value="<?= $model->field ?>"><?= $fields[$model->field] ?></option>
                <?php endif; ?>
            </select>
            <div class="help-block"></div>
        </div>
    </div>

    <?= $form->field($model, 'default')->textInput(['placeholder' => 'Enter default value...']) ?>
    <?= $form->field($model, 'description')->textarea(['placeholder' => 'Enter placeholder description...']) ?>
    <?= $form->field($model, 'fake')->checkbox(); ?>

    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
