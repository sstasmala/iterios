<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\CustomPlaceholders $model
 */

$this->title = 'Placeholder: ' . $model->short_code;
$this->params['breadcrumbs'][] = ['label' => 'Placeholders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$fields = \app\modules\ia\controllers\PlaceholdersController::getFields($model->module);
?>
<div class="custom-placeholders-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        'id',
                        'short_code',
                        [
                            'attribute' => 'type_placeholder',
                            'value' => !empty($model->type_placeholder) && \in_array($model->type_placeholder, \app\models\Placeholders::TYPE_PLACEHOLDERS_KEY, true) ? \app\models\Placeholders::TYPE_PLACEHOLDERS[$model->type_placeholder] : '(not set)'
                        ],
                        [
                            'attribute' => 'module',
                            'format' => 'raw',
                            'value'=> !empty($model->module) && \array_key_exists($model->module, \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$model->module] : '(not set)'
                        ],
                        [
                            'attribute' => 'field',
                            'format' => 'raw',
                            'value'=> $model->fake ? '<span class="label label-danger">Fake</span>' : (
                                !empty($fields) && \array_key_exists($model->field, $fields) ? $fields[$model->field] : '(not set)'
                            ),
                        ],
                        'default',
                        'description',
                        [
                            'attribute' => 'created_at',
                            'containerOptions' => ['class' => 'to-datetime-convert'],
                            'valueColOptions' => ['class' => 'to-datetime-convert'],
                            'value' => null === $model->created_at ? '' : $model->created_at
                        ],
                        [
                            'attribute' => 'updated_at',
                            'valueColOptions' => ['class' => 'to-datetime-convert'],
                            'value' => null === $model->updated_at ? '' : $model->updated_at
                        ],
                        [
                            'attribute' => 'created_by',
                            'format' => 'raw',
                            'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                        ]
                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
