<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View                   $this
 * @var yii\data\ActiveDataProvider    $dataProvider
 * @var app\models\search\Placeholders $searchModel
 */

$this->title = 'Placeholders';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="custom-placeholders-index">
    <?php
    $columns = [
        'id',
        'short_code',
        [
            'attribute' => 'type_placeholder',
            'value' => function($model) {
                return !empty($model->type_placeholder) && in_array($model->type_placeholder, \app\models\Placeholders::TYPE_PLACEHOLDERS_KEY, true) ? \app\models\Placeholders::TYPE_PLACEHOLDERS[$model->type_placeholder] : '(not set)';
            }
        ],
        [
            'attribute' => 'module',
            'format' => 'raw',
            'value'=> function ($model) {
                return !empty($model->module) && \array_key_exists($model->module, \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$model->module] : '(not set)';
            }
        ],
        [
            'attribute' => 'field',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->fake)
                    return '<span class="label label-danger">Fake</span>';

                return !empty($model->field) ? $model->field : '(not set)';
            }
        ],
        'default',
        [
            'attribute' => 'description',
            'visible' => false
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->created_at ? '' : $model->created_at);
            }
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->updated_at ? '' : $model->updated_at);
            }
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'after' => false,
                'before' => $lang_select
            ],
            'toolbar' => [
                ['content'=>
                     (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                     Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-emails-types'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
