<?php

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\SmsCountries $searchModel
 */

$this->title = 'SMS Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-countries-index">
    <?php
        $columns = [
            'id',
            'name',
            'iso_code',
            [
                'attribute' => 'dial_code',
                'value' => function ($model) {
                    return '+' . $model->dial_code;
                }
            ],
            [
                'attribute' => 'provider_id',
                'label' => 'Provider',
                'format' => 'raw',
                'value' => function ($model) {
                    if (empty($model->provider_id))
                        return '(not set)';

                    return Html::a($model->provider->name, '/ia/sms-system-providers/view?id='.$model->provider_id);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SmsSystemProviders::find()
                    ->orderBy('name')
                    ->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'SMS Provider', 'id' => 'sms_providers']
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_by',
                'format' => 'raw',
                'value' => function ($model) {
                    if (empty($model->updated_by))
                        return 'system';

                    return Html::a($model->updated->first_name . ' ' . $model->updated->last_name, '/ia/users/view?id='.$model->updated_by);
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view} {update}'
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
