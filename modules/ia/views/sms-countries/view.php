<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SmsCountries $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'SMS Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-countries-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    SMS Countries info
                </div>
                <div class="box-body">
                    <?php
                    $columns = [
                        'name',
                        'iso_code',
                        [
                            'attribute' => 'dial_code',
                            'value' => '+' . $model->dial_code
                        ],
                        [
                            'attribute' => 'provider_id',
                            'label' => 'Provider',
                            'format' => 'raw',
                            'value' => empty($model->provider_id) ? '(not set)' : Html::a($model->provider->name, '/ia/sms-system-providers/view?id='.$model->provider_id)
                        ],
                        [
                            'attribute' => 'created_at',
                            'containerOptions' => ['class'=>'to-datetime-convert'],
                            'valueColOptions' => ['class'=>'to-datetime-convert']
                        ],
                        [
                            'attribute' => 'updated_at',
                            'valueColOptions' => ['class'=>'to-datetime-convert']
                        ],
                        [
                            'attribute' => 'updated_by',
                            'format' => 'raw',
                            'value' => empty($model->updated_by) ? 'system' : Html::a($model->updated->first_name . ' ' . $model->updated->last_name, '/ia/users/view?id='.$model->updated_by)
                        ]
                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
                <div class="box-footer">
                    <?= Html::a('Update', ['update','id' => $model['id']], ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
    </div>

</div>
