<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SmsCountries $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="sms-countries-form">

    <?php
        $options = [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => 'readonly']],
            'iso_code' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => 'readonly']],
            'dial_code' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => 'readonly', 'value' => '+' . $model->dial_code]],
            'provider_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\widgets\Select2',
                'label' => 'Provider',
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map($providers, 'id', 'name'),
                    'options' => ['placeholder' => 'Choose sms provider'],
                    'pluginOptions' => [
                        'allowClear' => false
                    ]
                ]
            ],
        ];
    ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    SMS Countries info
                </div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' => 1,
                        'attributes' => $options
                    ]); ?>
                </div>
                <div class="box-footer">
                    <?php
                    echo Html::submitButton(Yii::t('app', 'Update'),
                        ['class' => 'btn btn-primary']
                    );
                    ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
