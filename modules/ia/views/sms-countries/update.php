<?php

/**
 * @var yii\web\View $this
 * @var app\models\SmsCountries $model
 */

$this->title = 'Update SMS Country: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'SMS Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-countries-update">

    <?= $this->render('_form', [
        'model' => $model,
        'providers' => $providers
    ]) ?>

</div>
