<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Log $model
 */

$this->title = 'Log #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-view">

    <div class="row">

        <div class="col-md-6">

            <div class="box-header with-border">
                <h3>Previous</h3>
            </div>

            <div class="box box-primary">
                    <div class="box-body">
                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <tbody>
                            <tr>
                                <th>Column</th>
                                <th>Content</th>
                            </tr>
                            <?php
                            if (isset($previous)) {
                                foreach ($previous as $column => $item) {
                                    echo '<tr>';
                                    echo '<td>' . $column . '</td>';
                                    echo '<td>' . (!empty($item) ? $item : 'not set') . '</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="box-header with-border">
                <h3>After</h3>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
                        <tr>
                            <th>Column</th>
                            <th>Content</th>
                        </tr>
                        <?php
                        if (isset($after)) {
                            foreach ($after as $column => $item) {
                                echo '<tr>';
                                echo '<td>' . $column . '</td>';
                                echo '<td>' . (!empty($item) ? $item : 'not set') . '</td>';
                                echo '</tr>';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="box-header with-border">
                <h3>Previous</h3>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
                        <tr>
                            <th>Column</th>
                            <th>Content</th>
                        </tr>
                        <?php
                        if (isset($afterGroupItems)) {
                            foreach ($afterGroupItems as $column => $item) {
                                echo '<tr>';
                                echo '<td>' . $column . '</td>';
                                echo '<td>' . (!empty($item) ? $item : 'not set') . '</td>';
                                echo '</tr>';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="box-header with-border">
                <h3>After</h3>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
                        <tr>
                            <th>Column</th>
                            <th>Content</th>
                        </tr>
                        <?php
                        if (isset($previousGroupItems)) {
                            foreach ($previousGroupItems as $column => $item) {
                                echo '<tr>';
                                echo '<td>' . $column . '</td>';
                                echo '<td>' . (!empty($item) ? $item : 'not set') . '</td>';
                                echo '</tr>';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box-header with-border">
                <h3>Other information about the log</h3>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => [
                            'id',
                            'table_name',
                            'item_id',
                            'group_id',
                            'created_by',
                            [
                                'attribute'=>'created_at',
                                'valueColOptions'=>['class'=>'to-datetime-convert']
                            ]
                        ],
                        'enableEditMode' => false,
                    ]) ?>
                </div>
            </div>
        </div>

    </div>

</div>
