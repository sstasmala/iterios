<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ExchangeRates $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerAssetBundle(\app\assets\InputmaskAsset::className());
$this->registerJsFile('@web/admin/js/pages/exchange-rates/exchange-rates.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
?>

<div class="exchange-rates-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'currency_id' => ['type' => Form::INPUT_HIDDEN, 'options' => ['placeholder' => 'Enter Currency ID...']],

            'currency_iso' => ['type' => Form::INPUT_HIDDEN, 'options' => ['placeholder' => 'Enter Currency Iso...', 'maxlength' => 10]],

//            'rate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Rate...']],

        ]
    ]);?>

    <?=$form->errorSummary($model);?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2" for="tariffs-description">Currency</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="currency-select" placeholder="Select Currency...">
                        <?php if(isset($model->currency_id)):?>
                            <option selected value="<?=$model->currency_id?>"><?=$model->currency_iso?></option>
                        <?php endif;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <?=$form->field($model,'rate')->textInput(['placeholder' => 'Enter Rate...'])?>

    <?php

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
