<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Tariffs $model
 */

$this->title = 'Update Tariffs: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tariffs-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/tariffs',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'features'=>$features,
                        'linked'=>$linked
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
