<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\search\Tariffs $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tariffs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'tariff_duration') ?>

    <?= $form->field($model, 'tariff_start') ?>

    <?php // echo $form->field($model, 'tariff_end') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'days_before_stop') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'need_act') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
