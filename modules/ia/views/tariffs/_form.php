<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Tariffs $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/tariff/tariff.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
?>

<div class="tariffs-form">
    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'description' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Description...','rows' => 6]],

            'country' => ['type' => Form::INPUT_HIDDEN, 'options' => ['placeholder' => 'Enter Country...','rows' => 6]],

            'tariff_duration' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=> \app\models\Tariffs::DURATIONS, 'options' => ['placeholder' => 'Enter Tariff Duration...',]],

//            'tariff_start' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Tariff Start...']],
//
//            'tariff_end' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Tariff End...']],

            'days_before_stop' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Days Before Stop...','type'=>'number']],

            'need_act' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Need Act...']],

            'price' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Price...','type'=>'number']],

        ]

    ]);?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2" for="tariffs-description">Country</label>
                <div class="col-md-10">
                    <select class="form-control m-bootstrap-select country-select-ajax"
                            id="country-select" multiple placeholder="Enter Country...">

                        <?php $countries = json_decode($model->country); if($countries == null) $countries = []; ?>
                        <?php foreach ($countries as $country):?>
                            <option value="<?=$country?>" selected><?= \app\helpers\MCHelper::getCountryById($country,'en')[0]['title']?></option>
                        <?php endforeach;?>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>

    <?=$form->field($model,'tariff_start')->widget(\kartik\widgets\DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]])?>
    <?=$form->field($model,'tariff_end')->widget(\kartik\widgets\DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]])?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group field-tariffs-description">
                <label class="control-label col-md-2" for="tariffs-description">Features:</label>
                <div class="col-md-10">
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($features as $feature):?>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group field-tariffs-description">
                    <div class="col-md-offset-2 col-md-5">
                        <div class="checkbox"><input type="hidden" name="FeaturesTariffs[<?=$feature['id']?>][feature_id]" value="0">
                            <label>
                                <input type="checkbox" name="FeaturesTariffs[<?=$feature['id']?>][feature_id]"
                                       value="<?=$feature['id']?>" placeholder="Enter Need Act..."
                                <?=(isset($linked[$feature['id']]))?"checked":""?>>
                                <?=$feature['name']?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <?php if($feature['countable']):?>
                        <input type="number" name="FeaturesTariffs[<?=$feature['id']?>][count]"
                               value="<?=(isset($linked[$feature['id']]) && isset($linked[$feature['id']]['count']))?$linked[$feature['id']]['count']:""?>">
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>

    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>
</div>
