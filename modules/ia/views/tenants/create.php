<?php

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 */

$this->title = 'Create Tenants';
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenants-create">
    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'languages' => $languages
    ]) ?>
</div>
