<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Tenants $searchModel
 */

$this->title = 'Tenants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenants-index">
    <?php
        $columns = [
            'id',
            'name',
            [
                'attribute' => 'owner_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->owner->first_name . ' ' . $model->owner->last_name, '/ia/users/view?id='.$model->owner_id);
                }
            ],
            [
                'attribute' => 'language_id',
                'value' => function ($model) {
                    return $model->language->name;
                }
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            //'created_by',
            //'updated_by',
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'order' => DynaGrid::ORDER_FIX_RIGHT,
                'template' => '{view} {update} {delete}'
            ]
        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'panel-success',
            'showPersonalize' => true,
            'storage' => 'session',
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'floatHeader' => false,
                'pjax' => false,
                'responsiveWrap' => false,
                'responsive' => false,
                'containerOptions' => ['style' => 'overflow: auto'],
                'panel'=> [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Tenants</h3>',
                    'before' => '',
                    'after' => false
                ],
                'toolbar' => [
                    ['content'=>
                         Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']).
                         Html::a('<i class="fa fa-bell-o"></i>', ['reset-base-notifications'], ['data-pjax'=>0, 'class' => 'btn btn-warning', 'title'=>'Reset base notifications']) .
                         Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                    ],
                    ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    '{export}',
                ]
            ],
            'options' => ['id'=>'dynagrid-ui-transaltions'] // a unique identifier is important
        ]);

        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }

        DynaGrid::end();
    ?>

</div>
