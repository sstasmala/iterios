<?php

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 */

$this->title = 'Update Tenants: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tenants-update">
    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'languages' => $languages
    ]) ?>
</div>
