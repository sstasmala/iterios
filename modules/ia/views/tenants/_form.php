<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 * @var yii\widgets\ActiveForm $form
 */
if(!is_null($model->mailer_config) && !empty($model->mailer_config)){
    $config = json_decode($model->mailer_config,true);
}else{
    $config = [
        "imap_host"=>null,
        "imap_port"=>null,
        "imap_encrypt"=>null,
        "imap_user"=>null,
        "imap_pass"=>null,
        "smtp_host"=>null,
        "smtp_port"=>null,
        "smtp_encrypt"=>null,
        "smtp_from"=>null,
        "smtp_user"=>null,
        "smtp_pass"=>null,
    ];
}
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Tenant <?= ($model->isNewRecord ? 'create' : 'update') ?></h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
                'owner_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' => [
                        'data' => \yii\helpers\ArrayHelper::map($users, 'id', 'email'),
                        'options' => ['placeholder' => 'Choose user'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]
                ],
                'language_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' => [
                        'data' => \yii\helpers\ArrayHelper::map($languages, 'id', 'name'),
                        'options' => ['placeholder' => 'Choose language'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]
                ]
            ]
        ]);
        ?>

    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        );
        ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
