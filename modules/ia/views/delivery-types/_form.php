<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\DeliveryTypes $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="delivery-types-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
        ]
    ]);

    echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
