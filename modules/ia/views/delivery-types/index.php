<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Delivery Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-types-index">
    <?php
    $columns = [
        'id',
        //'for',
        'type',
        'name',
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update}'
        ]
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/'.Yii::$app->controller->id,
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'after' => false,
                'before' => $lang_select
            ],
            'toolbar' => [
                ['content' => Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id' => 'dynagrid-delivery-types'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
