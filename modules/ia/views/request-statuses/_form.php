<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\RequestStatuses $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="request-statuses-form">

    <?php
        $types = [
            \app\models\RequestStatuses::TYPE_WORKING => 'Working',
            \app\models\RequestStatuses::TYPE_CANCELED => 'Canceled',
            \app\models\RequestStatuses::TYPE_SOLD => 'Sold'
        ];

        $colors = [];

        foreach (\app\models\RequestStatuses::COLOR_TYPES as $color)
            $colors[$color] = '<span class="pull-right label label-'.$color.'">'.$color.'</span>';

        $attributes = [
            'name' => [
                'type' => Form::INPUT_TEXT,
                'options' => [
                    'placeholder' => 'Enter Name...',
                    'maxlength' => 255]
            ],
        ];
    ?>

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => $attributes
    ]);

    echo $form->field($model, 'type')->radioButtonGroup($types);
    echo $form->field($model, 'color_type')->radioList($colors);
    echo $form->field($model, 'is_default')->checkbox();

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
