<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/tenants/add_user.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

$this->title = 'Tenant Settings ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenants Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenants-view">

    <?php if (!empty(Yii::$app->request->get('send_invite', false))): ?>
        <div class="alert alert-success">
            Invite successfully sent!
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tenant info</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    $attributes = [
                        'id',
                        'name',
                        [
                            'attribute' => 'owner_id',
                            'format' => 'raw',
                            'value' => Html::a($model->owner->last_name . ' ' . $model->owner->first_name, '/ia/users/view?id='.$model->owner_id)
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => ($model->status == \app\models\User::STATUS_DELETED ? '<span class="label label-danger">Inactive</span>' : '<span class="label label-success">Active</span>')

                        ],
                        [
                            'attribute' => 'language_id',
                            'value' => $model->language->name
                        ],
                        [
                            'attribute' => 'created_at',
                            'valueColOptions' => ['class' => 'to-datetime-convert']
                        ],
                        [
                            'attribute' => 'updated_at',
                            'valueColOptions' => ['class' => 'to-datetime-convert']
                        ],
                    ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $attributes,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
                <div class="box-footer">
                    <?= Html::a('Update', ['update?id='.$model->id], ['class' => 'btn btn-success']) ?>
                    <?php //Html::a('Restore owner', ['owner-restore?id='.$model->id], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    $user_provider = new ArrayDataProvider([
        'allModels' => $users,
        'pagination' => [
            'pageSize' => 10,
        ]
    ]);

    $user_columns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'format' => 'raw',
            'label' => 'Full name',
            'value' => function ($modal) use ($model) {
                $invite_link = Html::a('Send invite', Yii::$app->params['baseUrl'].'/ia/tenants/repeat-invite?id='.$model->id.'&user_id='.$modal['id']);
                $block_link = Html::a(($modal['blocked'] ? 'Unblock' : 'Block') . ' user', Yii::$app->params['baseUrl'].'/ia/tenants/block?id='.$model->id.'&user_id='.$modal['id']);

                $admin = ' <i class="fa fa-user-secret" title="Tenant owner"></i>';
                $not_confirm = ' <i class="fa fa-user-times" data-content="Not confirm user"></i> [' . $invite_link.']';
                $block = ' <i class="fa fa-ban" title="User blocked"></i>';

                return Html::a($modal['last_name'] . ' ' . $modal['first_name'] . ' ' . $modal['middle_name'], '/ia/users/view?id='.$modal['id']) .
                    ($modal['is_tenant_owner'] ? $admin : '') . ($modal['not_confirm'] ? $not_confirm : '') . ($modal['blocked'] ? $block : '') . ' ['.$block_link.']';
            }
        ],
        'email:email',
        [
            'attribute' => 'phone',
            'contentOptions' => ['class' => 'to-phone-convert']
        ],
        [
            'attribute' => 'photo',
            'format' => 'raw',
            'value' =>  function ($model) {
                return (!empty($model['photo']) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model['photo'], ['class' => 'img-circle', 'style' => 'height: 64px; width: 64px;']) : '(not set)');
            },
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'status',
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'last_login',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (!empty($model['last_login']) ? $model['last_login'] : '');
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'buttons' => [
                'delete' => function ($url, $modal) use ($model) {
                    $delete_link = Yii::$app->params['baseUrl'].'/ia/users/delete-from-tenant?id='.$model->id.'&user_id='.$modal['id'].'&r=tenant';

                    if ($modal['not_confirm'])
                        $delete_link = Yii::$app->params['baseUrl'].'/ia/tenants/delete-invites?id='.$model->id.'&user_id='.$modal['id'];

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        $delete_link,
                        [
                            'data-method' => 'post',
                            'title' => 'Delete',
                            'aria-label' => 'Delete',
                            'data-pjax' => 0,
                            'data-confirm' => 'Are you sure to delete this item?',
                        ]);
                },
                'owner' => function ($url, $modal) use ($model) {
                    if ($modal['is_tenant_owner'] || $modal['not_confirm'])
                        return '';

                    return Html::a('<span class="fa fa-star"></span>',
                        Yii::$app->params['baseUrl'].'/ia/tenants/make-owner?id='.$model->id.'&user_id='.$modal['id'],
                        [
                            'title' => 'Make owner',
                        ]);
                }
            ],
            'template' => '{delete} {owner}'
        ]
    ];
    ?>
</div>
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose user</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" name="tenant_id" value="<?= $model->id ?>">
                    <div>
                        <label class="control-label" for="tenant">User</label>
                        <select id="users-list">
                        </select>
                    </div>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="addToTenant" type="button" class="btn btn-primary">Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
