<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View $this
 * @var app\models\Tenants $model
 * @var yii\widgets\ActiveForm $form
 */
if(!is_null($model->mailer_config) && !empty($model->mailer_config)){
    $config = json_decode($model->mailer_config,true);
}else{
    $config = [
        "imap_host"=>null,
        "imap_port"=>null,
        "imap_encrypt"=>null,
        "imap_user"=>null,
        "imap_pass"=>null,
        "smtp_host"=>null,
        "smtp_port"=>null,
        "smtp_encrypt"=>null,
        "smtp_from"=>null,
        "smtp_user"=>null,
        "smtp_pass"=>null,
    ];
}
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Tenant Settings <?= ($model->isNewRecord ? 'create' : 'update') ?></h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
        ?>
        <div class="box-header with-border">
            <h3 class="box-title">Mailer config</h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <?php echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'use_webmail' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['label' => "Use in webmail"]],
            ]
        ]);
        ?>

        <?php echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'status' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['label' => "Status"]],
            ]
        ]);
        ?>
        <div class="box-header with-border">
            <h4 class="box-title"><b>Imap</b></h4>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-imap_host">Host</label>
            <div class="col-md-10">
                <input type="text" id="tenants-imap_host" class="form-control"
                       name="Tenants[mailer_config][imap_host]" value="<?=$config["imap_host"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-imap_port">Port</label>
            <div class="col-md-10">
                <input type="text" id="tenants-imap_port" class="form-control"
                       name="Tenants[mailer_config][imap_port]" value="<?=$config["imap_port"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-mailer_config">
            <label class="control-label col-md-2">Encrypt</label>
            <div class="col-md-10">
                <input type="hidden" name="Tenants[mailer_config][imap_encrypt]" value="<?=$config["imap_encrypt"]?>"><div id="tenants-encrypt" maxlength="255" placeholder="Enter Name...">
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][imap_encrypt]"
                                                     value="SSL" <?=($config['imap_encrypt']=="SSL")?'checked':''?> data-index="0"> SSL</label></div>
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][imap_encrypt]"
                                                     value="TLS" <?=($config['imap_encrypt']=="TLS")?'checked':''?> data-index="1"> TLS</label></div>
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][imap_encrypt]"
                                                     value="NO" <?=($config['imap_encrypt']=="NO")?'checked':''?> data-index="2"> NO</label></div></div>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-imap_user">User</label>
            <div class="col-md-10">
                <input type="text" id="tenants-imap_user" class="form-control"
                       name="Tenants[mailer_config][imap_user]" value="<?=$config["imap_user"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-imap_pass">Pass</label>
            <div class="col-md-10">
                <input type="password" id="tenants-imap_pass" class="form-control"
                       name="Tenants[mailer_config][imap_pass]" value="<?=$config["imap_pass"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="box-header with-border">
            <h4 class="box-title"><b>SMTP</b></h4>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-smtp_host">Host</label>
            <div class="col-md-10">
                <input type="text" id="tenants-smtp_host" class="form-control"
                       name="Tenants[mailer_config][smtp_host]" value="<?=$config["smtp_host"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-smtp_port">Port</label>
            <div class="col-md-10">
                <input type="text" id="tenants-smtp_port" class="form-control"
                       name="Tenants[mailer_config][smtp_port]" value="<?=$config["smtp_port"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-mailer_config">
            <label class="control-label col-md-2">Encrypt</label>
            <div class="col-md-10">
                <input type="hidden" name="Tenants[mailer_config][smtp_encrypt]" value="<?=$config["smtp_encrypt"]?>"><div id="tenants-encrypt" maxlength="255" placeholder="Enter Name...">
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][smtp_encrypt]"
                                                     value="SSL" <?=($config['smtp_encrypt']=="SSL")?'checked':''?> data-index="0"> SSL</label></div>
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][smtp_encrypt]"
                                                     value="TLS" <?=($config['smtp_encrypt']=="TLS")?'checked':''?> data-index="1"> TLS</label></div>
                    <div class="radio"><label><input type="radio" name="Tenants[mailer_config][smtp_encrypt]"
                                                     value="NO" <?=($config['smtp_encrypt']=="NO")?'checked':''?> data-index="2"> NO</label></div></div>

                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-smtp_from">From</label>
            <div class="col-md-10">
                <input type="text" id="tenants-smtp_from" class="form-control"
                       name="Tenants[mailer_config][smtp_from]" value="<?=$config["smtp_from"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-smtp_user">User</label>
            <div class="col-md-10">
                <input type="text" id="tenants-smtp_user" class="form-control"
                       name="Tenants[mailer_config][smtp_user]" value="<?=$config["smtp_user"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group field-tenants-name">
            <label class="control-label col-md-2" for="tenants-smtp_pass">Pass</label>
            <div class="col-md-10">
                <input type="password" id="tenants-smtp_pass" class="form-control"
                       name="Tenants[mailer_config][smtp_pass]" value="<?=$config["smtp_pass"]?>" maxlength="255" placeholder="Enter Name...">
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        );
        ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
