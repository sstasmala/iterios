<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 */

$this->title = 'Placeholders Delivery: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Placeholders Delivery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$ph_delivery = \app\models\Placeholders::find()
    ->where(['id' => $model->placeholder_id])
    ->translate('en')
    ->asArray()->one();

$delivery_types = [];

foreach ($model->placeholdersDeliveryTypes as $type) {
    $delivery_types[] = Html::a($type->name, '/ia/delivery-types/view?id='.$type->id, ['target' => '_blank']);
}
?>
<div class="delivery-placeholders-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <?php
                        $columns = [
                            'id',
                            [
                                'attribute' => 'placeholder_id',
                                'format' => 'raw',
                                'label' => 'Placeholder',
                                'value' => Html::a($ph_delivery['short_code'], '/ia/placeholders/view?id='.$model->placeholder_id, ['target' => '_blank'])
                            ],
                            [
                                'attribute' => 'delivery_type',
                                'label' => 'Delivery Type',
                                'format' => 'raw',
                                'value' => !empty($delivery_types) ? implode(', ', $delivery_types) : '(not set)'
                            ],
                            [
                                'attribute' => 'template',
                                'format' => 'raw',
                                'value' => $model->template
                            ],
                            [
                                'attribute' => 'created_at',
                                'containerOptions' => ['class'=>'to-datetime-convert'],
                                'valueColOptions' => ['class'=>'to-datetime-convert'],
                                'value' => null === $model->created_at ? '' : $model->created_at
                            ],
                            [
                                'attribute' => 'updated_at',
                                'valueColOptions' => ['class'=>'to-datetime-convert'],
                                'value' => null === $model->updated_at ? '' : $model->updated_at
                            ],
                            [
                                'attribute' => 'created_by',
                                'format' => 'raw',
                                'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
                            ],
                            [
                                'attribute' => 'updated_by',
                                'format' => 'raw',
                                'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
                            ]
                        ];

                        echo DetailView::widget([
                            'model' => $model,
                            'condensed' => false,
                            'hover' => true,
                            'mode' => DetailView::MODE_VIEW,
                            'attributes' => $columns,
                            'enableEditMode' => false,
                        ]) ?>

                    <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
