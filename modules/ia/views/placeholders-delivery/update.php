<?php

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Update placeholder delivery: ' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Placeholders Delivery', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="placeholder_delivery-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
