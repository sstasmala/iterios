<?php

/**
 * @var yii\web\View $this
 * @var app\models\TasksTypes $model
 */

$this->title = 'Create placeholder delivery';
$this->params['breadcrumbs'][] = ['label' => 'Placeholders Delivery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placeholder_delivery-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
