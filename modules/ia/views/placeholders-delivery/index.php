<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\helpers\MCHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Placeholders Delivery';
$this->params['breadcrumbs'][] = $this->title;

$phs_delivery_ids = array_column($dataProvider->getModels(), 'placeholder_id');
$phs_delivery = \app\models\Placeholders::find()
    ->where(['in', 'id', $phs_delivery_ids])
    ->translate('en')
    ->asArray()->all();
$phs_delivery = array_combine(array_column($phs_delivery, 'id'), $phs_delivery);

/**
 * @param $model
 *
 * @return array
 */
function getDeliveryTypes($model)
{
    $delivery_types = [];

    foreach ($model->placeholdersDeliveryTypes as $type) {
        $delivery_types[] = Html::a($type->name, '/ia/delivery-types/view?id=' . $type->id, ['target' => '_blank']);
    }

    return $delivery_types;
}
?>
<div class="delivery-placeholders-index">
    <?php
    $columns = [
        'id',
        [
            'attribute' => 'placeholder_id',
            'format' => 'raw',
            'label' => 'Placeholder',
            'value' => function ($model) use ($phs_delivery) {
                return Html::a($phs_delivery[$model->placeholder_id]['short_code'], '/ia/placeholders/view?id='.$model->placeholder_id, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'delivery_type',
            'label' => 'Delivery Type',
            'format' => 'raw',
            'value' => function ($model) {
                $delivery_types = getDeliveryTypes($model);

                return !empty($delivery_types) ? implode(', ', $delivery_types) : '(not set)';
            }
        ],
        [
            'attribute' => 'template',
            'visible' => false
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->created_at ? '' : $model->created_at);
            }
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert'],
            'value' => function ($model) {
                return (null === $model->updated_at ? '' : $model->updated_at);
            }
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'updated_by',
            'format' => 'raw',
            'value' => function ($model) {
                return null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme' => 'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel' => [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> '.$this->title.'</h3>',
                'before' => '',
                'after' => false
            ],
            'toolbar' => [
                ['content' =>
                    (true ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                ],
                ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id' => 'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
