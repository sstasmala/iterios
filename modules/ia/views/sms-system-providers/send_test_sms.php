<?php

use borales\extensions\phoneInput\PhoneInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SmsSystemProviders $model
 */

$this->registerJsFile('@web/admin/plugins/sms-counter/sms_counter.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
$this->registerJsFile('@web/admin/js/pages/sms-system-providers/send_test_sms.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

$this->title = 'SMS: Test send';
$this->params['breadcrumbs'][] = ['label' => 'SMS System Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-system-providers-view">
    <div class="row">
        <div class="col-md-6">
            <?php if ($send === true): ?>
                <div class="alert alert-success">
                    SMS Message sent successfully!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php elseif ($send !== false): ?>
                <div class="alert alert-danger">
                    Error! <?= $send ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <div class="box box-primary">
                <div class="box-header with-border">
                    Send test SMS
                </div>
                <div class="box-body">
                    <?php
                        $form = ActiveForm::begin([
                            'id' => 'send-test-sms-form-vertical',
                            'type' => ActiveForm::TYPE_VERTICAL
                        ]);
                    ?>

                    <?= $form->field($model, 'type_alpha_name')->radioList([
                        \app\models\SmsSystemProviders::ANAME_SYSTEM_TYPE => 'System',
                        \app\models\SmsSystemProviders::ANAME_PUBLIC_TYPE => 'Public',
                        \app\models\SmsSystemProviders::ANAME_PERSONAL_TYPE => 'Personal']); ?>

                    <div id="tenant_id" style="display: none;">
                        <?= $form->field($model, 'tenant_id')->widget(\kartik\widgets\Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Tenants::find()->asArray()->all(), 'id', 'name'),
                            'options' => ['placeholder' => 'Choose Tenant'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Tenant'); ?>
                    </div>

                    <div>
                        <label class="control-label" for="sendtestsmsform-phone_number">Phone number</label>
                        <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                'preferredCountries' => ['ua', 'ru'],
                            ]
                        ])->
//                        textInput(['placeholder' => 'Enter phone number...'])->
                        label(false); ?>
                    </div>

                    <?= $form->field($model, 'text')->textarea(['placeholder' => 'Enter sms text...']) ?>
                    <ul id="sms-counter">
                        <li>Encoding: <span class="encoding"></span></li>
                        <li>Length: <span class="length"></span></li>
                        <li>Messages: <span class="messages"></span></li>
                        <li>Per Message: <span class="per_message"></span></li>
                        <li>Remaining: <span class="remaining"></span></li>
                    </ul>

                </div>
                <div class="box-footer">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>