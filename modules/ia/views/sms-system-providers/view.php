<?php

use kartik\grid\GridView;
use kdn\yii2\JsonEditor;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\SmsSystemProviders $model
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/sms-system-providers/alpha_name.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'SMS System Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-system-providers-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    eMail provider info
                </div>
                <div class="box-body">
                    <?php
                        $columns = [
                            //'id',
                            'provider',
                            'name',
                            'system_aname',
                            'public_aname',
                            [
                                'attribute' => 'created_at',
                                'containerOptions' => ['class'=>'to-datetime-convert'],
                                'valueColOptions' => ['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'valueColOptions' => ['class'=>'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'created_by',
                                'format' => 'raw',
                                'value' => Html::a($model->created->first_name . ' ' . $model->created->last_name, '/ia/users/view?id='.$model->created_by)
                            ]
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $columns,
                        'enableEditMode' => false,
                    ]) ?>
                </div>
                <div class="box-footer">
                    <?= Html::a('Update', ['update','id' => $model['id']], ['class' => 'btn btn-success']); ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model['id']], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Credentials config
                </div>
                <div class="box-body">
                    <?php echo JsonEditor::widget(
                        [
                            // JSON editor options
                            'clientOptions' => [
                                'modes' => ['view'], // available modes
                                'mode' => 'view', // default mode
                            ],
                            'containerOptions' => ['class' => 'container', 'style' => 'width: 100%; padding: 0;'], // HTML options for JSON editor container tag
                            'expandAll' => ['view'], // expand all fields in "tree" and "form" modes
                            'name' => 'credentials_config', // hidden input name
                            'options' => ['id' => 'credentials_config'], // HTML options for hidden input
                            'value' => $model->credentials_config, // JSON which should be shown in editor
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        $user_provider = new ArrayDataProvider([
            'allModels' => $model_anames,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $user_columns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'value',
                'label' => 'Name'
            ],
            [
                'attribute' => 'tenant_id',
                'label' => 'Tenant',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id);
                }
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => ['class' => 'to-datetime-convert']
            ],
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->created->first_name . ' ' . $model->created->last_name, '/ia/users/view?id='.$model->created_by);
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $delete_link = Yii::$app->params['baseUrl'].'/ia/sms-system-providers/delete-alpha-name?id='.$model->id;

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            $delete_link,
                            [
                                'data-method' => 'post',
                                'title' => 'Delete',
                                'aria-label' => 'Delete',
                                'data-pjax' => 0,
                                'data-confirm' => 'Are you sure to delete this item?',
                            ]);
                    },
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            '#',
                            [
                                'class' => 'button_update',
                                'title' => 'Update',
                                'aria-label' => 'Update',
                                'data-pjax' => 0,
                                'data-nid' => $model->id,
                                'data-name' => $model->value,
                                'data-tid' => $model->tenant_id,
                                'data-tname' => $model->tenant->name
                            ]);
                    }
                ],
                'template' => '{update} {delete}'
            ]
        ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $user_provider,
        'columns' => $user_columns,
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => false,
        'export' => false,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> Personal alpha names</h3>',
            'type' => 'info',
            'before' => '<button class="btn btn-success" data-toggle="modal" data-target="#createAlphaNameModal">Create</button>',
            'after' => false,
            'showFooter' => false
        ],
    ]) ?>
</div>

<div class="modal fade" id="createAlphaNameModal" tabindex="-1" role="dialog" aria-labelledby="createAlphaNameLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create personal alpha name</h4>
            </div>
            <form action="/ia/sms-system-providers/create-alpha-name" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <?= Html::hiddenInput('SmsAlphaNames[provider_id]', $model->id) ?>
                        <div class="form-group">
                            <label class="control-label" for="alpha_name_value">Name</label>
                            <?= Html::textInput('SmsAlphaNames[value]', '', [
                                'class' => 'form-control',
                                'maxlength' => '255',
                                'placeholder' => 'Enter alpha name...']) ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tenant">Tenant</label>
                            <select id="tenants-list-create" name="SmsAlphaNames[tenant_id]">
                            </select>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="updateAlphaNameModal" tabindex="-1" role="dialog" aria-labelledby="updateAlphaNameLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update personal alpha name</h4>
            </div>
            <form action="/ia/sms-system-providers/update-alpha-name" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <?= Html::hiddenInput('id', '') ?>
                        <?= Html::hiddenInput('SmsAlphaNames[provider_id]', $model->id) ?>
                        <div class="form-group">
                            <label class="control-label" for="alpha_name_value">Name</label>
                            <?= Html::textInput('SmsAlphaNames[value]', '', [
                                'class' => 'form-control',
                                'maxlength' => '255',
                                'placeholder' => 'Enter alpha name...']) ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tenant">Tenant</label>
                            <select id="tenants-list-update" name="SmsAlphaNames[tenant_id]">
                            </select>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->