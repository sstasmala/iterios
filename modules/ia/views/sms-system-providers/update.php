<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SmsSystemProviders $model
 */

$this->title = 'Update SMS Provider: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'SMS Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-system-providers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
