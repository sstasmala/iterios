<?php

/**
 * @var yii\web\View $this
 * @var app\models\SmsSystemProviders $model
 */

$this->title = 'Create SMS Provider';
$this->params['breadcrumbs'][] = ['label' => 'SMS Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-system-providers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
