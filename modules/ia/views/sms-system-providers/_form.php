<?php

use kdn\yii2\JsonEditor;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SmsSystemProviders $model
 * @var yii\widgets\ActiveForm $form
 */

$this->registerJsFile('@web/admin/js/pages/sms-system-providers/form.js',
    ['position' => yii\web\View::POS_END, 'depends' => \app\assets\AdminAsset::class]);
?>

<div class="sms-system-providers-form">
    <?php
        $columns = [
            'provider' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\widgets\Select2',
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\Yii::$app->params['sms_providers'], 'class', 'name'),
                    'options' => ['placeholder' => 'Choose sms provider'],
                    'pluginOptions' => [
                        'allowClear' => false
                    ]
                ]
            ],
            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],
            'system_aname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter System AlphaName...', 'maxlength' => 255]],
            'public_aname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Public AlphaName...', 'maxlength' => 255]],
        ];
    ?>

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    SMS provider info
                </div>
                <div class="box-body">
                    <?php echo Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'columns' => 1,
                        'attributes' => $columns
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Credentials config
                </div>
                <div class="box-body">
                    <?php echo JsonEditor::widget(
                        [
                            // JSON editor options
                            'clientOptions' => [
                                'modes' => ['code', 'form', 'text', 'tree'], // available modes
                                'mode' => 'tree', // default mode
                            ],
                            'containerOptions' => ['class' => 'container', 'style' => 'width: 100%; padding: 0;'], // HTML options for JSON editor container tag
                            'expandAll' => ['tree', 'form'], // expand all fields in "tree" and "form" modes
                            'name' => 'SmsSystemProviders[credentials_config]', // hidden input name
                            'options' => ['id' => 'credentials_config'], // HTML options for hidden input
                            'value' => $model->credentials_config, // JSON which should be shown in editor
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
