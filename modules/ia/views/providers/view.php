<?php

use app\models\Contacts;
use app\models\ProviderFunction;
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var app\models\SitesTypes $model
 */

$this->title = 'Provider #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$funcs_ids = !empty($model->functions) ? JSON::decode($model->functions) : [];

$view_functions_list = ProviderFunction::find()->where(['in', 'id', $funcs_ids])->translate('en')->all();
$view_functions_list = array_column($view_functions_list, 'value');
$view_addit_conf_list = !empty($model->additional_config) ? JSON::decode($model->additional_config) : [];
?>
<div class="providers-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php
                    $lang_select = $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'view',
                        'model_controller_path' => '/ia/providers',
                    ]);
                    echo $lang_select;
                    ?>
                </div>
                <div class="box-body">
                    <?php
                        $attributes = [
                            'id',
                            'name',
                            'code',
                            [
                                'attribute' => 'logo',
                                'format' => 'raw',
                                'value' => !empty($model->logo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->logo, ['class' => 'img-round', 'style' => 'height: 128px; width: 128px;']) : '(not set)'
                            ],
                            [
                                'attribute' => 'provider_type_id',
                                'format' => 'raw',
                                'value' => isset($model->type) ? $model->type->value : '',
                            ],
                            [
                                'attribute' => 'functions',
                                'format' => 'raw',
                                'value' => implode(', ', $view_functions_list)
                            ],
                            'description',
                            'website',
                            'authorization_type',
                            [
                                'attribute' => 'additional_config',
                                'format' => 'raw',
                                'value' => implode(', ', $view_addit_conf_list)
                            ],
                            'provider_class',
                            [
                                'attribute'        => 'created_at',
                                'valueColOptions'  => ['class' => 'to-datetime-convert']
                            ],
                            [
                                'attribute'       => 'updated_at',
                                'valueColOptions' => ['class' => 'to-datetime-convert']
                            ],
                            [
                                'attribute' => 'created_by',
                                'format' => 'raw',
                                'value' => isset($model->created)? $model->created->first_name . ' '. $model->created->last_name: '',
                            ],
                            [
                                'attribute' => 'updated_by',
                                'format' => 'raw',
                                'value' => isset($model->updated)? $model->updated->first_name . ' '. $model->updated->last_name: '',
                            ],
                        ];
                    ?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'attributes' => $attributes,
                        'enableEditMode' => false,
                    ]) ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
