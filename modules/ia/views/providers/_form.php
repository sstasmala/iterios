<?php

use app\models\Providers;
use app\models\ProviderType;
use app\models\ProviderFunction;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>

<div class="providers-form">

    <?php $form = ActiveForm::begin(['id'=>'providers','type' => ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?= $form->field($model, 'name'); ?>
    <?= $form->field($model, 'code'); ?>

    <?= !empty($model->logo) ? '<label class="control-label col-md-2">Current logo</label><img class="help-block img-rounded" src="/'.$model->logo.'" height="100px">': ''?>

    <?= $form->field($model, 'logo')->fileInput(); ?>

    <?= $form->field($model, 'provider_type_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ProviderType::find()->all(), 'id','value' ),
        'options' => ['placeholder' => 'Choose provider type'],
    ]); ?>

    <div class="form-group field-providers-provider_type_id">
        <label class="control-label col-md-2" for="providers-functions">Provider Functions</label>
        <div class="col-md-10">
            <?= Select2::widget([
                'name' => 'functions[]',
                'value' => \json_decode($model->functions),
                'data' => ArrayHelper::map(ProviderFunction::find()->translate('en')->all(), 'id','value' ),
                'options' => ['multiple' => true, 'placeholder' => 'Choose provider function'],
            ]); ?>

            <div class="help-block"></div>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(); ?>

    <?= $form->field($model, 'website'); ?>

    <?= $form->field($model, 'authorization_type')->widget(Select2::classname(), [
        'data' => Providers::getAuthorizationTypes(),
        'options' => ['placeholder' => 'Choose authorization type'],
    ]); ?>

    <div class="form-group field-providers-provider_type_id">
        <label class="control-label col-md-2" for="providers-additional_config">Additional Config</label>
        <div class="col-md-10">
            <?= Select2::widget([
                'name' => 'additional_config[]',
                'value' => \json_decode($model->additional_config),
                'data' => Providers::getAdditionalConfig(),
                'options' => ['multiple' => true, 'placeholder' => 'Choose additional config'],
            ]); ?>

            <div class="help-block"></div>
        </div>
    </div>

    <?= $form->field($model, 'provider_class')->widget(Select2::classname(), [
        'data' => Providers::getClassProvider(),
        'options' => ['placeholder' => 'Choose class provider'],
    ]); ?>

    <div style="padding-top: 25px;">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id'=>'providers-form-submit']
        ); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
