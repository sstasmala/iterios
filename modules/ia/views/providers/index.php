<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TasksTypes $searchModel
 */

$this->title = 'Providers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-index">
    <?php
    $columns = [
        'id',
        'name',
        'code',
        [
            'attribute' => 'logo',
            'format' => 'raw',
            'value' =>  function ($model) {
                return (!empty($model->logo) ? Html::img(\Yii::$app->params['baseUrl'] . '/' . $model->logo, ['class' => 'img-round', 'style' => 'height: 64px; width: 64px;']) : '(not set)');
            },
        ],
        [
            'attribute' => 'provider_type_id',
            'header' => 'Provider type',
            'value' => function($model){

                return isset($model->type) ? $model->type->value : '(not set)';
            },
        ],
        [
            'attribute' => 'functions',
            'header' => 'Provider Functions',
            'value' => function($model){
                return isset($model->functions) ? $model->functions : '(not set)';
            },
        ],
        'description',
        'website',
        'authorization_type',
        'additional_config',
        [
            'attribute' => 'created_by',
            'value' => function($model){

                return isset($model->created) ? $model->created->first_name . ' '. $model->created->last_name : '';
            },
        ],
        [
            'attribute' => 'updated_by',
            'value' => function($model){

                return isset($model->updated) ? $model->updated->first_name . ' '. $model->updated->last_name : '';
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ],
    ];

    $lang_select = $this->render('///layouts/admin/_language_select', [
        'language' => $def_lang,
        'action' => 'index',
        'model_controller_path' => '/ia/providers',
    ]);

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Providers</h3>',
                'before' => $lang_select,
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (true? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>

</div>
