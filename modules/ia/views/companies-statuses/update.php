<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CompaniesStatuses $model
 */

$this->title = 'Update Companies Statuses: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Companies Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="companies-statuses-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/companies-statuses',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>

            </div>
        </div>
    </div>

</div>
