<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\UiTranslations $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="ui-translations-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

//            'group' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Group...', 'maxlength' => 250]],

            'group' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>array_combine($groups,$groups)],
            'code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Code...', 'maxlength' => 250]],

            'value' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Value...']],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
