<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UiTranslations $model
 */

$this->title = 'Create UI Translations';
$this->params['breadcrumbs'][] = ['label' => 'UI Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-translations-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'create',
                        'model_controller_path' => '/ia/ui-translations',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'groups'=>$groups
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
