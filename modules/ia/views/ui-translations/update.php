<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UiTranslations $model
 */

$this->title = 'Update Ui Translations: ' . ' ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Ui Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ui-translations-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?= $this->render('///layouts/admin/_language_select', [
                        'language' => $def_lang,
                        'action' => 'update',
                        'model_controller_path' => '/ia/ui-translations',
                        'model' => $model
                    ]) ?>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'groups'=>$groups
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>
