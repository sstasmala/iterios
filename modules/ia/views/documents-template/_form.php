<?php

use app\models\DocumentsType;
use app\models\Suppliers;
use kartik\dynagrid\DynaGrid;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use \app\models\Languages;

/**
 * @var yii\web\View $this
 * @var app\models\DocumentsTemplate $model
 * @var yii\widgets\ActiveForm $form
 */
$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

// CKEditor
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/documents-template/ckeditor-init.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
//

//$this->registerJsFile('@web/admin/js/pages/documents-template/lang_form.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);

$def_lang = Yii::$app->session->get('lang',Languages::getDefault());

$searchModelDoc = new \app\models\search\Placeholders();
$dataProviderDoc = $searchModelDoc->search(Yii::$app->request->getQueryParams(),
    Yii::$app->session->get('lang', Languages::getDefault())->iso, \app\models\search\Placeholders::TYPE_DOCUMENT);
?>
<div class="documents-template-form">

    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(['id' => 'document-template-form']); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
            <div class="w1">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Documents Template <?= ($model->isNewRecord ? 'Create' : 'Update') ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?= $form->field($model, 'title')->textInput(['placeholder' => 'Title']) ?>

                        <?= $form->field($model, 'languages')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Languages::findAll(['activated' => true]),'iso','name'),
                            'options' => ['placeholder' => 'Choose languages'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>

                        <?= $form->field($model, 'documents_type_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(DocumentsType::find()->all(),'id','name'),
                            'options' => ['placeholder' => 'Choose documents type'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>

                        <?= $form->field($model, 'suppliers_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Suppliers::find()->all(),'id','name'),
                            'options' => ['placeholder' => 'Choose Suppliers'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                        <?= Html::input('hidden','showPreview')?>
                        <?= $form->field($model, 'body')->textarea(['placeholder' => 'Body','class'=>'document-editor']) ?>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                            <a href="#" id="PreviewBtn" class="btn btn-success" onclick="return false">Preview</a>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6">
            <div class="provider-index">
                <?php
                $columns = [
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'buttons' => [
                            'copy' => function ($url, $model){
                                return '<a href="#" title="Copy" aria-label="Copy" data-pjax="0" data-pl_id="'.$model->id.'">'.
                                    '<span class="btn btn-success btn-sm copy_mark_btn copy_button"><span class="fa fa-copy"></span></span></a>';
                            },
                        ],
                        'template' => '{copy}',
                    ],
                    [
                        'attribute' => 'short_code',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return '<span id="placeholder_'.$model->id.'" style="background-color:#ffff00;">{{'.$model->short_code.'}}</span>';
                        }
                    ],
                    [
                        'attribute' => 'type_placeholder',
                        'value' => function($model) {
                            return !empty($model->type_placeholder) && in_array($model->type_placeholder, \app\models\Placeholders::TYPE_PLACEHOLDERS_KEY, true) ? \app\models\Placeholders::TYPE_PLACEHOLDERS[$model->type_placeholder] : '(not set)';
                        }
                    ],
                    [
                        'attribute' => 'module',
                        'format' => 'raw',
                        'value'=> function ($model) {
                            return !empty($model->module) && \array_key_exists($model->module, \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$model->module] : '(not set)';
                        }
                    ],
                    [
                        'attribute' => 'field',
                        'format'=>'raw',
                        'value' => function ($model) {
                            return $model->fake ? '<span class="label label-danger">Fake</span>' : (!empty($model->field) ? $model->field : '(not set)');
                        }
                    ],
                    'default',
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'order' => DynaGrid::ORDER_FIX_RIGHT,
                        'template' => '{view} {update} {delete}'
                    ]
                ];
                $lang_select = $this->render('///layouts/admin/_language_select', [
                    'language' => $def_lang,
                    'action' => 'index',
                    'model_controller_path' => '/ia/document-placeholders',
                ]);

                $dynagrid = DynaGrid::begin([
                    'columns' => $columns,
                    'theme' => 'panel-success',
                    'showPersonalize' => true,
                    'storage' => 'session',
                    'gridOptions' => [
                        'dataProvider' => $dataProviderDoc,
//                        'filterModel' => $searchModel,
                        'showPageSummary' => false,
                        'floatHeader' => false,
                        'pjax' => false,
                        'responsiveWrap' => false,
                        'responsive' => false,
                        'containerOptions' => ['style' => 'overflow: auto'],
                        'panel' => [
                            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Document placeholders</h3>',
                            'before' => $lang_select,
                            'after' => false
                        ],
                        'toolbar' => [
                            ['content' =>
                                (true ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
                                Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                            ],
                            ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                            '{export}',
                        ]
                    ],
                    'options' => ['id' => 'dynagrid-providers'] // a unique identifier is important
                ]);
                if (substr($dynagrid->theme, 0, 6) == 'simple') {
                    $dynagrid->gridOptions['panel'] = false;
                }

                DynaGrid::end();
                ?>

            </div>
        </div>
    </div>
</div>
