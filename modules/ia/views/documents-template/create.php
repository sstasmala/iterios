<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\DocumentsTemplate $model
 */

$this->title = 'Create Documents Template '.(Yii::$app->controller->id === 'documents-template-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = ['label' => 'System DocumentsTemplate', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-template-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
