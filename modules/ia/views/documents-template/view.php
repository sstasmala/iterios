<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\DocumentsTemplate $model
 */

$this->title = (is_null($model->title))?'Template #'.$model->id:$model->title;
$this->params['breadcrumbs'][] = ['label' => 'DocumentsTemplate', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/admin/plugins/select2/css/select2.min.css', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/plugins/select2/js/select2.min.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/documents-template/lang.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJsFile('@web/admin/js/pages/documents-template/template-body.js', ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\AdminAsset']);
$this->registerJs('var template_id = '.$model->id.';', \yii\web\View::POS_HEAD);
?>
<div class="documents-template-view">

    <?php
        $columns = [
            ['attribute' => 'id', 'visible' => false],
            'title',
            [
                'attribute' => 'type',
                'value' => ($model->type ? ucfirst($model->type) : '(not set)')
            ],
            [
                'attribute' => 'languages',
                'format' => 'raw',
                'value' =>  isset($model->lang)?$model->lang->name:'',
            ],
            [
                'attribute' => 'documents_type_id',
                'format' => 'raw',
                'value' =>  isset($model->types)?$model->types->name:'',
            ],
            [
                'attribute' => 'suppliers_id',
                'format' => 'raw',
                'value' =>  isset($model->supplier)?$model->supplier->name:'',
            ],
            'body',
        ];

        if (Yii::$app->controller->id !== 'documents-template-system') {
            $columns[] = [
                'attribute' => 'tenant_id',
                'label' => 'Tenant',
                'format' => 'raw',
                'value' => null === $model->tenant ? null : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank'])
            ];
        }

        $columns_second = [
            [
                'attribute' => 'created_at',
                'containerOptions' => ['class'=>'to-datetime-convert'],
                'valueColOptions' => ['class'=>'to-datetime-convert'],
                'value' => null === $model->created_at ? '' : $model->created_at
            ],
            [
                'attribute' => 'updated_at',
                'valueColOptions' => ['class'=>'to-datetime-convert'],
                'value' => null === $model->updated_at ? '' : $model->updated_at
            ],
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => null === $model->created_by ? null : Html::a($model->created->first_name . ' '. $model->created->last_name, '/ia/users/view?id='.$model->created_by, ['target' => '_blank'])
            ],
            [
                'attribute' => 'updated_by',
                'format' => 'raw',
                'value' => null === $model->updated_by ? null : Html::a($model->updated->first_name . ' '. $model->updated->last_name, '/ia/users/view?id='.$model->updated_by, ['target' => '_blank'])
            ]
        ];

        $columns = array_merge($columns, $columns_second);

    ?>

    <div class="w1">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Documents template</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => $columns
                ]);?>

                <?=Html::a('Update', ['update','id' => $model->id], ['class' => 'btn btn-success'])?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
            </div>

        </div>
        <!-- /.box -->
    </div>
</div>
