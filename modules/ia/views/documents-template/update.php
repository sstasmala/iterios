<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\DocumentsTemplate $model
 */

$this->title = 'Update Documents Template '.(Yii::$app->controller->id === 'documents-template-system' ? 'system' : 'public') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Documents Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="documents-template-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
