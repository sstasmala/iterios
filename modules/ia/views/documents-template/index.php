<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\DocumentsTemplate $searchModel
 */

$this->title = 'Documents Template '.(Yii::$app->controller->id === 'documents-template-system' ? 'system' : 'public');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="documents-template-index">

    <?php
    $columns = [
        'id',
        'title',
        [
            'attribute' => 'type',
            'value' => function ($model) {
                return ($model->type ? ucfirst($model->type) : '(not set)');
            }
        ],
        [
            'attribute' => 'languages',
            'label' => 'languages',
            'width'=>'350px',
            'value'=>function($model)
            {
                return isset($model->lang)?$model->lang->name:'';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map($langAll,'iso','name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Languages', 'id' => 'grid-documents-template-languages-search']

        ],
        [
            'attribute' => 'documents_type_id',
            'label' => 'Documents type',
            'width'=>'350px',
            'value'=>function($model)
            {
                return isset($model->types)?$model->types->name:'';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map($docTypeAll,'id','name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Documents type', 'id' => 'grid-documents-type-template-search']

        ],
        [
            'attribute' => 'tenant_id',
            'label' => 'Tenant',
            'format' => 'raw',
            'visible' => Yii::$app->controller->id !== 'documents-template-system',
            'value' => function ($model) {
                return null === $model->tenant ? null : Html::a($model->tenant->name, '/ia/tenants/view?id='.$model->tenant_id, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['class' => 'to-datetime-convert']
        ],
        [
            'attribute' => 'created_by',
            'format' => 'raw',
            'value' => function ($model) {
                $user = \app\models\User::findOne($model->created_by);
                return $user->first_name.' '.$user->last_name;
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'template' => '{view} {update} {delete}'
        ]
    ];

    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-success',
        'showPersonalize' => true,
        'storage' => 'session',
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'floatHeader' => false,
            'pjax' => false,
            'responsiveWrap' => false,
            'responsive' => false,
            'containerOptions' => ['style' => 'overflow: auto'],
            'panel'=> [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Documents Template</h3>',
                'before' => '  ',
                'after' => false
            ],
            'toolbar' => [
                ['content'=>
                    (Yii::$app->controller->id === 'documents-template-system' ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success']) : '') .
//                    (true? Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['refresh'], ['data-pjax'=>0, 'class' => 'btn btn-success', 'title'=>'Refresh data from config']) : '') .
                    Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
            ]
        ],
        'options' => ['id'=>'dynagrid-providers'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }

    DynaGrid::end();
    ?>


</div>
