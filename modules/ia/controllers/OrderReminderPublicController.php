<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\RemindersTenant;
use app\models\Tenants;
use Yii;
use app\models\Reminders;
use app\models\search\Reminders as RemindersSearch;
use yii\filters\VerbFilter;

/**
 * RemindersController implements the CRUD actions for Reminders model.
 */
class OrderReminderPublicController extends OrderReminderSystemController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reminders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemindersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Reminders::TYPE_PUBLIC,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/order-reminder/index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Reminders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new Reminders();

        $data = Yii::$app->request->post();

        if (isset($data['active'], $data['tenant_id'])) {
            $tenant = Tenants::findOne($data['tenant_id']);

            if ($tenant !== null) {
                if ($model->loadWithLang($data)) {
                    $reminder_tenant = new RemindersTenant();
                    $reminder_tenant->active = $data['active'];
                    $reminder_tenant->reminder_id = $model->id;
                    $reminder_tenant->tenant_id = $tenant->id;

                    if ($reminder_tenant->save())
                        return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('@app/modules/ia/views/order-reminder/create', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Updates an existing Reminders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $data = Yii::$app->request->post();

        if (isset($data['active'], $data['tenant_id'])) {
            $tenant = Tenants::findOne($data['tenant_id']);

            if ($tenant !== null) {
                if ($model->loadWithLang($data)) {
                    $reminder_tenant = RemindersTenant::findOne($model->reminderTenant->id);

                    if ($reminder_tenant !== null) {
                        $reminder_tenant->active = $data['active'];
                        $reminder_tenant->tenant_id = $tenant->id;

                        if ($reminder_tenant->save()) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }
            }
        }

        return $this->render('@app/modules/ia/views/order-reminder/update', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }
}
