<?php

namespace app\modules\ia\controllers;

use app\models\Jobs;
use app\models\Languages;
use app\models\SegmentsResultList;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\search\SegmentsResultList as SegmentsResultListSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class SegmentsResultListController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SegmentsResultListSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);


        $jobUpdateSegmentsListRun = false;
        if(!is_null(Jobs::findOne(['executor' => 'app\components\executors\UpdateSegmentsListExecutor']))) $jobUpdateSegmentsListRun = true;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'jobUpdateSegmentsListRun' => $jobUpdateSegmentsListRun,
            'searchModel' => $searchModel,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = SegmentsResultList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLoader()
    {
        if(is_null(Jobs::findOne(['executor' => 'app\components\executors\UpdateSegmentsListExecutor']))){
            $job = new Jobs();
            $job->executor = 'app\components\executors\UpdateSegmentsListExecutor';
            $job->save();
        }

        return $this->redirect(['index']);

    }

    public function actionTempLoader(){
        SegmentsResultList::loadFromRelation();
echo 1;
    }

}
