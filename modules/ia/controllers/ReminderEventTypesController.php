<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\NotificationsTrigger;
use Yii;
use app\models\ReminderEventTypes;
use app\models\search\RequestStatuses as RequestStatusesSearch;
use app\models\search\ReminderEventTypesSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReminderEventTypesController implements the CRUD actions for ReminderEventTypes model.
 */
class ReminderEventTypesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReminderEventTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReminderEventTypesSearch;

        $dataProvider = $searchModel->search(
            Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso
        );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single ReminderEventTypes model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {

        $model = ReminderEventTypes::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (is_null($model))
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Creates a new ReminderEventTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReminderEventTypes;

        // get actions
        $actions_data = $this->getActions();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'actions' => $actions_data,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing ReminderEventTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // get actions
        $actions_data = $this->getActions($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'actions' => $actions_data,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing ReminderEventTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReminderEventTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReminderEventTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReminderEventTypes::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getActions($id = false) {

        // get actions
        $actions = NotificationsTrigger::find()->select(['action'])->distinct()->where(['type' => 0])->asArray()->all();

        if ($id) {
            $actions_true = ReminderEventTypes::find()->where(['!=', 'id', $id])->select(['action'])->distinct()->asArray()->all();
        } else {
            $actions_true = ReminderEventTypes::find()->select(['action'])->distinct()->asArray()->all();
        }


        if ($actions_true)
            $actions_true = array_column($actions_true, 'action');

        $actions_data = [];
        foreach ($actions as $act) {
            if (!empty($actions_true) && in_array($act['action'], $actions_true)) 
                continue;
                
            $actions_data[$act['action']] = $act['action'];
        }

        return $actions_data;
    }
}
