<?php

namespace app\modules\ia\controllers;

use app\models\Suppliers;
use Yii;
use app\models\SuppliersCredentials;
use app\models\search\SuppliersCredentials as SuppliersCredentialsSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\Response;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SuppliersCredentialsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SuppliersCredentialsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = SuppliersCredentials::find()->where(['id' => $id])->one();
        $supplier = Suppliers::find()
            ->where(['id' => $model->supplier_id])
            ->translate('en')
            ->one();

        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'supplier' => $supplier
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new SuppliersCredentials;

        if ($model->load(Yii::$app->request->post())&& $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = SuppliersCredentials::findOne($id);

        if ($model->load(Yii::$app->request->post())&& $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = SuppliersCredentials::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLoadParams($supplier_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return Suppliers::findOne($supplier_id)->authorization_type;
    }
}
