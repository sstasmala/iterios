<?php

namespace app\modules\ia\controllers;

use app\models\DocumentPlaceholders;
use app\models\DocumentsTemplate;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\PaymentGateways;
use app\models\search\PaymentGateways as PaymentGatewaysSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PaymentGatewaysController implements the CRUD actions for PaymentGateways model.
 */
class PaymentGatewaysController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentGateways models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentGatewaysSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single PaymentGateways model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new PaymentGateways model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentGateways;

        $post = Yii::$app->request->post();
        if(!empty($post))
            if($post['PaymentGateways']['type'] == PaymentGateways::TYPE_CARD) {
                $post['PaymentGateways']['document_template_id'] = null;
            }
            else{
                $post['PaymentGateways']['provider'] = null;
                $post['PaymentGateways']['provider_config'] = null;
            }

        if ($model->load($post) && $model->save()) {
            $model->imageFile = UploadedFile::getInstance($model, 'logo');
            $model->upload();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PaymentGateways model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if(!empty($post))
            if($post['PaymentGateways']['type'] == PaymentGateways::TYPE_CARD) {
                $post['PaymentGateways']['document_template_id'] = null;
            }
            else{
                $post['PaymentGateways']['provider'] = null;
                $post['PaymentGateways']['provider_config'] = null;
            }

        $oldLogo = $model->logo;
        if ($model->load($post) && $model->save()) {
            $model->imageFile = UploadedFile::getInstance($model, 'logo');
            $model->upload();

            if ($model->logo == null) $model->logo = $oldLogo;

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentGateways model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentGateways model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentGateways the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentGateways::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetDocumentsTemplates() {
        $search = \Yii::$app->request->get('search', '');
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $templates = DocumentsTemplate::find()->where(['type'=>DocumentsTemplate::SYSTEM_TYPE])->andWhere(['like','title',$search])->asArray()->all();
        return $templates;
    }

    public function actionGetProviderConfig()
    {
        $providers = Yii::$app->params['payment_providers'];
        $provider = Yii::$app->request->get('provider',null);

        if(isset($providers[$provider])){
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $providers[$provider]['config'];
        }
        throw new HttpException(404);
    }
}
