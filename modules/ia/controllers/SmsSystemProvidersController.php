<?php

namespace app\modules\ia\controllers;

use app\models\SendTestSmsForm;
use app\models\SmsAlphaNames;
use app\models\Tenants;
use Yii;
use app\models\SmsSystemProviders;
use app\models\search\SmsSystemProviders as SmsSystemProvidersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SmsSystemProvidersController implements the CRUD actions for SmsSystemProviders model.
 */
class SmsSystemProvidersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SmsSystemProviders models.
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $searchModel = new SmsSystemProvidersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SmsSystemProviders model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model_alpha_names = SmsAlphaNames::find()
            ->where(['provider_id' => $model->id])
            ->orderBy('created_at DESC')
            ->all();

        return $this->render('view', ['model' => $model, 'model_anames' => $model_alpha_names]);
    }

    /**
     * Creates a new SmsSystemProviders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new SmsSystemProviders;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreateAlphaName()
    {
        $model = new SmsAlphaNames();

        if ($model->load(Yii::$app->request->post()))
            $model->save();

        return $this->redirect(['view', 'id' => $model->provider_id]);
    }

    /**
     * Updates an existing SmsSystemProviders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateAlphaName()
    {
        $id = Yii::$app->request->post('id', false);

        if (empty($id))
            throw new NotFoundHttpException('The requested page does not exist.');

        $model = SmsAlphaNames::findOne($id);

        if ($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        if ($model->load(Yii::$app->request->post()))
            $model->save();

        return $this->redirect(['view', 'id' => $model->provider_id]);
    }

    /**
     * Deletes an existing SmsSystemProviders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDeleteAlphaName($id)
    {
        $model = SmsAlphaNames::findOne($id);

        if ($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        $provider_id = $model->provider_id;

        $model->delete();

        return $this->redirect(['view', 'id' => $provider_id]);
    }

    /**
     * @param      $provider_id
     * @param null $tenant_id
     * @param null $name
     *
     * @return array
     */
    public function actionGetTenants($provider_id, $tenant_id = null, $name = null)
    {
        if (null === $name)
            $name = '';

        $existed_tenants_id = SmsAlphaNames::find()
            ->select('tenant_id')
            ->where(['provider_id' => $provider_id]);

        if (null !== $tenant_id)
            $existed_tenants_id->andWhere(['!=', 'tenant_id', $tenant_id]);

        $existed_tenants_id = $existed_tenants_id->column();

        $tenants = Tenants::find();

        if (null !== $existed_tenants_id)
            $tenants->where(['not in', 'id', $existed_tenants_id]);

        $tenants->andFilterWhere(['ilike', 'name', $name]);

        $tenants = $tenants->orderBy('name')->all();

        $data = [];

        foreach ($tenants as $tenant) {
            $data[] = [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'owner' => $tenant->owner->last_name . ' ' . $tenant->owner->first_name . ' ('.$tenant->owner->email.')'
            ];
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
    }

    /**
     * @param $provider
     *
     * @return bool|string
     */
    public function actionGetConfig($provider)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!class_exists($provider))
            return false;

        /**
         * @var $data \app\components\mailers_sms\MailerSmsInterface
         */
        return $provider::$default_config;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionSendTestSms()
    {
        $model = new SendTestSmsForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $send = $model->send();

            // return success
            if ($send === true)
                return $this->redirect(['/ia/send-test-sms', 'send' => 'success']);

            // return error mesage
            return $this->redirect(['/ia/send-test-sms', 'send' => $send]);
        }

        return $this->render('send_test_sms', [
            'model' => $model,
            'send' => \Yii::$app->request->get('send')
                ? (\Yii::$app->request->get('send') === 'success' ? true : \Yii::$app->request->get('send'))
                : false
        ]);
    }

    /**
     * Finds the SmsSystemProviders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmsSystemProviders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmsSystemProviders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
