<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\ProviderUploadForm;
use Yii;
use app\models\Providers;
use app\models\search\Providers as ProvidersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\helpers\FileHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DocumentsTypeController implements the CRUD actions for DocumentsType model.
 */
class ProvidersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new ProvidersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel
        ]);
    }


    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = Providers::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (null === $model)
            throw new HttpException('404', 'Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Providers;

        $post = Yii::$app->request->post();

        if ($model->loadWithLang($post)) {
            $model->functions = !empty($post['functions']) ? json_encode($post['functions']) : '';
            $model->additional_config = !empty($post['additional_config']) ? json_encode($post['additional_config']) : '';

            $imgLogo = new ProviderUploadForm();
            $imgLogo->src = UploadedFile::getInstance($model, 'logo');
            $upload = $imgLogo->upload();

            if ($upload !== false)
                $model->logo = $upload;

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldLogo = $model->logo;

        $post = Yii::$app->request->post();

        if ($model->loadWithLang($post)) {
            $model->functions = !empty($post['functions']) ? json_encode($post['functions']) : '';
            $model->additional_config = !empty($post['additional_config']) ? json_encode($post['additional_config']) : '';

            if (!empty($oldLogo))
                $model->logo = $oldLogo;

            $imgLogo = new ProviderUploadForm();
            $imgLogo->src = UploadedFile::getInstance($model, 'logo');
            $upload = $imgLogo->upload();

            if ($upload !== false) {
                if (!empty($model->logo))
                    FileHelper::unlink($model->logo);

                $model->logo = $upload;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!empty($model->logo)){
            FileHelper::unlink($model->logo);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return \app\models\Providers
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    protected function findModel($id)
    {
        if (($model = Providers::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
