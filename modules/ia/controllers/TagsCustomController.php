<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\Tags;
use Yii;
use app\models\search\Tags as TagsSearch;
use yii\filters\VerbFilter;

/**
 * TagsController implements the CRUD actions for Tags model.
 */
class TagsCustomController extends TagsSystemController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tags models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Tags::TYPE_CUSTOM,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/tags/index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }
}
