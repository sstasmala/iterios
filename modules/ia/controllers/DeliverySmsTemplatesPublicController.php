<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\DeliverySmsTemplates;
use app\models\search\DeliverySmsTemplates as DeliverySmsTemplatesSearch;
use yii\filters\VerbFilter;

/**
 * DeliverySmsTemplatesController implements the CRUD actions for DeliverySmsTemplates model.
 */
class DeliverySmsTemplatesPublicController extends DeliverySmsTemplatesSystemController
{
    protected $template_type = DeliverySmsTemplates::PUBLIC_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySmsTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliverySmsTemplatesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DeliverySmsTemplates::PUBLIC_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/delivery-sms-templates/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }
}
