<?php

namespace app\modules\ia\controllers;

use app\models\Templates;
use Yii;
use app\models\LogsEmail;
use app\models\search\LogsEmail as LogsEmailSearch;

/**
 * LogsEmailController implements the CRUD actions for LogsEmail model.
 */
class LogsPublicEmailController extends LogsSystemEmailController
{
    /**
     * Lists all LogsEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsEmailSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), LogsEmail::TYPE_PUBLIC);

        return $this->render('@app/modules/ia/views/logs-email/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'data_templates' => Templates::find()->orderBy('name')->all()
        ]);
    }
}
