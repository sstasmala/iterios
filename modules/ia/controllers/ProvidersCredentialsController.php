<?php

namespace app\modules\ia\controllers;

use app\components\email_provider\MailchimpProvider;
use app\components\sms_provider\SmsClubXML;
use app\models\Providers;
use app\models\Segments;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\models\Tenants;
use app\utilities\contacts\ContactsUtility;
use Yii;
use app\models\ProvidersCredentials;
use app\models\search\ProvidersCredentials as ProvidersCredentialsSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ProvidersCredentialsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProvidersCredentialsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = ProvidersCredentials::find()->where(['id' => $id])->one();
        $provider = Providers::find()
            ->where(['id' => $model->provider_id])
            ->translate('en')
            ->one();

        if (null === $model)
            throw new HttpException('404', 'Record not found');

        return $this->render('view', [
            'model' => $model,
            'provider' => $provider
        ]);
    }

    public function actionCreate()
    {
        $model = new ProvidersCredentials;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = ProvidersCredentials::findOne($id);
        if($model == null)
            throw new HttpException(404);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = ProvidersCredentials::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLoadParams($provider_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $array_const = Providers::findOne($provider_id);
        return $array_const;
    }

    public function actionGetList($id) {
        /**
         * @var $provider ProvidersCredentials
         */
        $provider = ProvidersCredentials::find()->where(['id'=>(int)$id])->one();
        if($provider != null)
        {
            /**
             * @var $tenant Tenants
             */
            $tenant = Tenants::find()->where(['id'=>$provider->tenant_id])->one();
            if($tenant == null)
                return '';
            $params = $provider->params;
            if(!empty($params))
                $config = \json_decode($params,true);
            else
                $config = [];

            $data =  [];
            if(!empty($provider->provider_data))
                $data = \json_decode($provider->provider_data,true);

            $prov = new MailchimpProvider($config,$tenant,$data);

            $segments = SegmentsRelations::find()->where(['id_tenant'=>$tenant->id])->asArray()->all();
            $ids = \array_column($segments,'id');
            $relations = SegmentsResultList::find()->where(['in','id_relation',$ids])->asArray()->all();
            $ids = \array_column($relations,'id_contact');
            $contacts = ContactsUtility::getContactsInfo($ids,$tenant->language->iso);

            $info = [];
            foreach ($segments as $segment){
                $rls = \array_filter($relations,function ($item) use ($segment){
                    return $item['id_relation'] == $segment['id'];
                });
                $cids = \array_column($rls,'id_contact');
                $cnts = \array_filter($contacts,function ($item) use ($cids){
                    return (\array_search($item['id'],$cids)!== false);
                });
                $emails = \array_column($cnts,'emails');
                if(!empty($emails)) {
                    $emails = \array_merge(...$emails);
                    $emails = \array_column($emails, 'value');
                }
                $phones = \array_column($cnts,'phones');
                if(!empty($phones)) {
                    $phones = \array_merge(...$phones);
                    $phones = \array_column($phones,'value');
                }
                $info[$segment['id']]['id'] = $segment['id'];
                $info[$segment['id']]['emails'] = $emails;
                $info[$segment['id']]['phones'] = $phones;
            }

//            $data = $prov->synchronizeSegments($info);
            $data = $prov->sendSegments($info,'test');
            $errors = $prov->getErrors();
            if(!empty($errors))
                $provider->provider_errors = \json_encode($errors);
            else
                $provider->provider_errors = null;

//            $provider->provider_data = json_encode($data);
            $provider->save();

            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $data;
        }

    }
}
