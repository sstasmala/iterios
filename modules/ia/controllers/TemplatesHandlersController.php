<?php

namespace app\modules\ia\controllers;

use app\components\base\TemplatableHandlerInterface;
use app\helpers\TwigHelper;
use app\models\Templates;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\TemplatesHandlers;
use app\models\search\TemplatesHandlers as TemplatesHandlersSearch;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TemplatesHandlersController implements the CRUD actions for TemplatesHandlers model.
 */
class TemplatesHandlersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TemplatesHandlers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TemplatesHandlersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),['system','systemDefault']);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single TemplatesHandlers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new TemplatesHandlers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TemplatesHandlers;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing TemplatesHandlers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TemplatesHandlers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TemplatesHandlers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TemplatesHandlers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TemplatesHandlers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetPreview()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $template = Yii::$app->request->post('template', null);
        $handler = Yii::$app->request->post('handler', null);
        $template = Templates::findOne(intval($template));
        if(is_null($template))
            return false;

        if (empty($handler) || !class_exists($handler) || !(new $handler() instanceof TemplatableHandlerInterface))
            return ['data' => ['subject' => $template->subject, 'body' => Html::encode($template->body)], 'error' => null];

        $error = null;

        try {
            $response['subject'] = TwigHelper::render($handler::getShortcodes(), $template->subject, true);
            $response['body'] = Html::encode(TwigHelper::render($handler::getShortcodes(), $template->body, true));
        }
        catch (\Exception $e) {
            $error = $e->getMessage();
            $response['subject'] = TwigHelper::render($handler::getShortcodes(), $template->subject);
            $response['body'] = Html::encode(TwigHelper::render($handler::getShortcodes(), $template->body));
        }

        return ['data' => $response, 'error' => $error];
    }
}
