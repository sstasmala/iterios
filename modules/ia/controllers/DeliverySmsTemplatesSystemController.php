<?php

namespace app\modules\ia\controllers;

use app\models\DeliveryPlaceholders;
use app\models\DeliveryPlaceholderTypes;
use app\models\Languages;
use app\models\Placeholders;
use app\models\PlaceholdersDelivery;
use app\models\PlaceholdersDeliveryTypes;
use Yii;
use app\models\DeliverySmsTemplates;
use app\models\search\DeliverySmsTemplates as DeliverySmsTemplatesSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DeliverySmsTemplatesController implements the CRUD actions for DeliverySmsTemplates model.
 */
class DeliverySmsTemplatesSystemController extends BaseController
{
    protected $template_type = DeliverySmsTemplates::SYSTEM_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySmsTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliverySmsTemplatesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DeliverySmsTemplates::SYSTEM_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/delivery-sms-templates/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single DeliverySmsTemplates model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = DeliverySmsTemplates::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('@app/modules/ia/views/delivery-sms-templates/view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
        ]);
    }

    /**
     * Creates a new DeliverySmsTemplates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DeliverySmsTemplates;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->type = $this->template_type;

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/delivery-sms-templates/create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing DeliverySmsTemplates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->type = $this->template_type;

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/delivery-sms-templates/update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPreview($id)
    {
        $model = $this->findModel($id);

        $model->loadWithLang(Yii::$app->request->post());

        return $this->render('@app/modules/ia/views/delivery-sms-templates/preview', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing DeliverySmsTemplates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param        $type
     * @param string $lang
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetPlaceholders($type, $lang = 'en')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!empty($type)) {
            $pl_ids = PlaceholdersDeliveryTypes::findAll(['delivery_type_id' => $type]);

            if ($pl_ids !== null) {
                $pl_ids = array_column($pl_ids, 'placeholder_delivery_id');

                if ( ! empty($pl_ids)) {
                    $ph_delivery_ids = PlaceholdersDelivery::find()
                        ->select('placeholder_id')
                        ->where(['in', 'id', $pl_ids])->column();

                    $placeholders = Placeholders::find()
                        ->where(['in', 'id', $ph_delivery_ids])
                        ->translate($lang)
                        ->asArray()->all();

                    return array_column($placeholders, 'short_code');
                }
            }
        }

        return [];
    }

    /**
     * Finds the DeliverySmsTemplates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DeliverySmsTemplates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DeliverySmsTemplates::find()->where(['id'=>$id])->translate(Languages::getDefault()->iso)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
