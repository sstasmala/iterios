<?php

namespace app\modules\ia\controllers;

use Yii;
use app\models\Languages;
use app\models\NotificationsNow;
use app\components\notifications_now\NotificationActions;
use app\models\search\NotificationsNow as NotificationsNowSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotificationsNowController implements the CRUD actions for NotificationsNow model.
 */
class NotificationsNowController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all NotificationsNow models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationsNowSearch;

        $dataProvider = $searchModel->search(
            Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso
        );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single NotificationsNow model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = NotificationsNow::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (is_null($model))
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);

    }

    /**
     * Creates a new NotificationsNow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NotificationsNow;
        $actions = $this->getActions();


        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'actions' => $actions,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }



    }

    /**
     * Updates an existing NotificationsNow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $actions = $this->getActions($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'actions' => $actions,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing NotificationsNow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NotificationsNow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NotificationsNow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotificationsNow::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getActions($id = false) {

        // get actions
        $actions = NotificationActions::ACTIONS_ADMIN;

        if ($id) {
            $actions_true = NotificationsNow::find()->where(['!=', 'id', $id])->select(['class'])->distinct()->asArray()->all();
        } else {
            $actions_true = NotificationsNow::find()->select(['class'])->distinct()->asArray()->all();
        }


        if ($actions_true)
            $actions_true = array_column($actions_true, 'class');

 

        $actions_data = [];
        foreach ($actions as $act => $title) {
            if (!empty($actions_true) && in_array($act, $actions_true)) 
                continue;
                
            $actions_data[$act] = $title;
        }

        return $actions_data;
    }
}
