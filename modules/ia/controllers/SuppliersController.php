<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\SupplierUploadForm;
use Yii;
use app\models\Suppliers;
use app\models\search\Suppliers as SuppliersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\helpers\FileHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class SuppliersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuppliersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }

    /**
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = Suppliers::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Suppliers;

        $post = Yii::$app->request->post();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->functions = !empty($post['functions']) ? json_encode($post['functions']) : '';

            $imgLogo = new SupplierUploadForm();
            $imgLogo->src = UploadedFile::getInstance($model, 'logo');
            $upload = $imgLogo->upload();

            if ($upload !== false)
                $model->logo = $upload;

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldLogo = $model->logo;

        $post = Yii::$app->request->post();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->functions = !empty($post['functions']) ? json_encode($post['functions']) : '';

            if (!empty($oldLogo))
                $model->logo = $oldLogo;

            $imgLogo = new SupplierUploadForm();
            $imgLogo->src = UploadedFile::getInstance($model, 'logo');
            $upload = $imgLogo->upload();

            if ($upload !== false) {
                if (!empty($model->logo))
                    FileHelper::unlink($model->logo);

                $model->logo = $upload;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!empty($model->logo)){
            FileHelper::unlink($model->logo);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return \app\models\Suppliers
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     */
    protected function findModel($id)
    {
        if (($model = Suppliers::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
