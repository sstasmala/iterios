<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\ServicesFields;
use app\models\ServicesFieldsDefault;
use app\models\ServicesLinks;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Services;
use app\models\search\Services as ServicesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $lang = Yii::$app->session->get('lang',Languages::getDefault());
        $searchModel = new ServicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),$lang->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => $lang,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Services;
        $post = Yii::$app->request->post();
        if ($model->loadWithLang($post)) {
            $model = $this->setFields($model,$post);
            If($model->hasErrors())
                return $this->render('create', [
                    'model' => $model,
                    'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                    'def' => \app\models\ServicesFieldsDefault::find()->where(['is_default'=>1])->asArray()->all(),
                    'available' => ServicesFieldsDefault::find()->where(['not in','id',[]])->all()
                ]);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'def' => \app\models\ServicesFieldsDefault::find()->where(['is_default'=>1])->asArray()->all(),
                'available' => ServicesFieldsDefault::find()->where(['not in','id',[]])->all()
            ]);
        }
    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadWithLang($post)) {
            $model = $this->setFields($model,$post);
            If($model->hasErrors()) {
                $ids = array_column(ServicesFields::find()->select(['id_field'])->where(['id_service'=>$model->id])->asArray()->all(),'id_field');
                return $this->render('update', [
                    'model' => $model,
                    'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
                    'def' => \app\models\ServicesFieldsDefault::find()->where(['is_default' => 1])->asArray()->all(),
                    'available' => ServicesFieldsDefault::find()->where(['not in', 'id', $ids])->all()
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $ids = array_column(ServicesFields::find()->select(['id_field'])->where(['id_service'=>$model->id])->asArray()->all(),'id_field');
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'def' => \app\models\ServicesFieldsDefault::find()->where(['is_default'=>1])->asArray()->all(),
                'available' => ServicesFieldsDefault::find()->where(['not in','id',$ids])->all()
            ]);
        }
    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $fields = $model->getServicesFields()->asArray()->all();
        $ids = array_column($fields,'id_field');
        $fields = ServicesFieldsDefault::find()->where(['in','id',$ids])->andWhere(['is_default'=>0])->all();
        foreach ($fields as $field)
            $field->delete();

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            $model = $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetIcons()
    {
//        $doc = new \DOMDocument();
//        $doc->loadHTMLFile('/var/www/ita/data/icons.html');
//        $selector = new \DOMXPath($doc);
//        $result = $selector->query('//i');
//        $data = '';
//        foreach($result as $node) {
//            $data .= "'".$node->getAttribute('class')."',";
//            $data .= '<br>';
//        }
//        return $data;
        $data = require \Yii::$app->basePath.'/data/icons.map.php';
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /**
     * @param $model Services
     * @param $post
     * @return Services
     */
    private function setFields($model, $post)
    {
        if (isset($post['ServicesFieldsDefault'])) {
            $assigned = ServicesFields::find()->where(['id_service' => $model->id])->with('field')->all();
            $old = [];
            foreach ($assigned as $a) {
                $old[$a->field->id] = $a;
            }
            $ids = array_keys($old);
            foreach ($post['ServicesFieldsDefault'] as $i => $field) {

                $field['code'] = preg_replace('/[^A-Za-z0-9\_]/', '', str_replace(' ', '_', trim($field['code'])));
                if (isset($field['id'])) {
                    if (in_array($field['id'], $ids) && !empty($field['code'])) {
                        if (!$old[$field['id']]->field->is_default) {
                            $old[$field['id']]->field->loadWithLang(['ServicesFieldsDefault'=>$field]);
//                            $old[$field['id']]->field->code = $field['code'];
//                            $old[$field['id']]->field->type = $field['type'];
//                            $old[$field['id']]->field->in_header = $field['in_header'];
//                            $old[$field['id']]->field->is_additional = $field['is_additional'];
//                            $old[$field['id']]->field->variables = $field['variables'];
//                            $old[$field['id']]->field->save();
                            if ($old[$field['id']]->field->hasErrors())
                                $model->addErrors($old[$field['id']]->field->getErrors());

                        }
                        $ln = ServicesFields::find()->where(['id_field' => $field['id']])->andWhere(['id_service' => $model->id])->one();
                        if(!is_null($ln)){
                            $ln->position = $post['ServicesFields'][$i]['position'];
                            $ln->save();
                        }
                        if(isset($old[$field['id']]))
                            unset($old[$field['id']]);
                    } else {
                        $f = ServicesFieldsDefault::findOne(['id' => intval($field['id'])]);
                        if (!is_null($f)) {
                            $ln = ServicesFields::find()->where(['id_field' => $f->id])->andWhere(['id_service' => $model->id])->one();
                            if (is_null($ln)) {
                                $link = new ServicesFields();
                                $link->id_service = $model->id;
                                $link->id_field = $f->id;
                                $link->position = $post['ServicesFields'][$i]['position'];
                                $link->save();
                            }
                            else{
                                $ln->position = $post['ServicesFields'][$i]['position'];
                                $ln->save();
                            }

                        }
                    }
                } else {
                    if (!empty($field['code'])) {
                        $f = new ServicesFieldsDefault();
                        $f->loadWithLang(['ServicesFieldsDefault'=>$field]);
//                        $f->code = $field['code'];
//                        $f->type = $field['type'];
//                        $f->is_additional = $field['is_additional'];
//                        $f->in_header = $field['in_header'];
//                        $f->variables = $field['variables'];
//                        $f->save();
                        if ($f->hasErrors())
                            $model->addErrors($f->getErrors());
                        else {
                            $link = new ServicesFields();
                            $link->id_service = $model->id;
                            $link->id_field = $f->id;
                            $link->position = $post['ServicesFields'][$i]['position'];
                            $link->save();
                        }
                    }
                }
            }
            If (!$model->hasErrors())
                foreach ($old as $f) {
                    if (!$f->field->is_default) {
                        $f->field->delete();
                    }
                    else{
                        $f->delete();
                    }
                }
        }
        if (isset($post['ServicesLinks'])) {
            $links = ServicesLinks::find()->where(['parent_id' => $model->id])->all();
            $old = [];
            foreach ($links as $l)
                $old[$l->child_id] = $l;
            $ids = array_keys($old);
            foreach ($post['ServicesLinks']['child_id'] as $s) {
                if (in_array($s, $ids))
                    unset($old[$s]);
                else {
                    $link = new ServicesLinks();
                    $link->child_id = $s;
                    $link->parent_id = $model->id;
                    $link->save();
                }
            }
            foreach ($old as $o) {
                $o->delete();
            }
        } else {
            $links = ServicesLinks::find()->where(['parent_id' => $model->id])->all();
            foreach ($links as $l) {
                $l->delete();
            }
        }
        return $model;
    }

    public function actionGetFieldTranslation($id,$lang)
    {
        $field = ServicesFieldsDefault::find()->where(['id'=>intval($id)])->translate($lang)->one();
        if(is_null($field))
            return null;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $field->toArray();
    }
}
