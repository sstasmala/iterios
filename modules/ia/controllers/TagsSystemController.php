<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Tags;
use app\models\search\Tags as TagsSearch;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TagsController implements the CRUD actions for Tags model.
 */
class TagsSystemController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tags models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Tags::TYPE_SYSTEM,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/tags/index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Tags model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = Tags::find()->where(['id'=>$id])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();

        if(null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('@app/modules/ia/views/tags/view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
        ]);
    }

    /**
     * Creates a new Tags model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tags;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/tags/create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing Tags model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/tags/update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing Tags model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tags model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tags the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tags::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
