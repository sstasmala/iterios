<?php

namespace app\modules\ia\controllers;

use app\models\Transactions;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\TariffsOrders;
use app\models\search\TariffsOrders as TariffsOrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TariffsOrdersController implements the CRUD actions for TariffsOrders model.
 */
class TariffsOrdersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TariffsOrders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TariffsOrdersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single TariffsOrders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new TariffsOrders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TariffsOrders;


        if ($model->load(Yii::$app->request->post()) ) {
            $other = TariffsOrders::find()->where(['customer_id'=>$model->customer_id])->andWhere(['or',['status'=>TariffsOrders::STATUS_NOT_ACTIVE],['status'=>TariffsOrders::STATUS_ACTIVE]])->all();
            if(!empty($other))
            {
                $model->addError('customer_id','This customer already have order');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TariffsOrders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $other = TariffsOrders::find()->where(['customer_id'=>$model->customer_id])
                ->andWhere(['or',['status'=>TariffsOrders::STATUS_NOT_ACTIVE],['status'=>TariffsOrders::STATUS_ACTIVE]])
                ->andWhere(['<>','id',$model->id])
                ->all();
            if(!empty($other))
            {
                $model->addError('customer_id','This customer already have order');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TariffsOrders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TariffsOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TariffsOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TariffsOrders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCancel($id)
    {
        $model = $this->findModel(intval($id));
        if($model->status != TariffsOrders::STATUS_ACTIVE)
            return $this->redirect(['view', 'id' => $model->id]);
        $customer = $model->customer;
        $customer->tariff_id = null;

        if($model->duration_date == null)
            return $this->redirect(['view', 'id' => $model->id]);
        $e_date = new \DateTime();
        $e_date->setTimestamp($model->duration_date);

        $a_date = $model->activation_date;
        if($a_date == null) {
            $tariff = $model->tariff;
            $a_date =  new \DateTime();
            $a_date->setTimestamp($model->duration_date);
            switch ($tariff->tariff_duration)
            {
                case 0:$a_date->sub(new \DateInterval('P1M'));break;
                case 1:$a_date->sub(new \DateInterval('P6M'));break;
                case 2:$a_date->sub(new \DateInterval('P1Y'));break;
            }
        }
        else {
            $a_date = (new \DateTime())->setTimestamp($a_date);
        }
        $now =  new \DateTime();
        $now->setTimestamp(time());
        $total_diff = date_diff($a_date,$e_date);
        $left_diff =date_diff($now,$e_date);

        $price_per_date =  (float)$model->tariff->price/(float)$total_diff->days;
        $refund = $left_diff->days*$price_per_date;

        /**
         * For more accuracy
         */
//        $test = bcdiv($model->tariff->price,$total_diff->days,2);
//        $ref_test = bcmul($left_diff->days,$test);
//        $test_b = bcadd($customer->balance,$ref_test,2);

        $customer->balance = $customer->balance+$refund;
        $model->status = TariffsOrders::STATUS_SUSPENDED;
        if($model->save() && $customer->save()) {
            $tr = new Transactions();
            $tr->action = Transactions::ACTION_REFUND;
            $tr->sum = $refund;
            $tr->owner = Yii::$app->user->id;
            $tr->tariff_order_id = $model->id;
            $tr->tariff_id = $model->tariff_id;
            $tr->date = time();
            $tr->save();
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

}
