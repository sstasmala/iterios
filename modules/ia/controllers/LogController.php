<?php

namespace app\modules\ia\controllers;

use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Log;
use app\models\search\Log as LogSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Log model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (is_null($model)) {
            throw new HttpException('404','Record not found');
        }
        $data = json_decode($model->data);
        $after = $data->after;
        $previous = $data->previous;

        $groupItems = Log::find()->where(['group_id' => $id])->all();
        $afterGroupItems = [];
        $previousGroupItems = [];
        foreach ($groupItems as $groupItem) {
            $decodeItems = json_decode($groupItem->data);
            if (!is_null($decodeItems->after)) {
                foreach ($decodeItems->after as $column => $decodeItem) {
                    $afterGroupItems[$column] = $decodeItem;
                }
            }
            if (!is_null($decodeItems->previous)) {
                foreach ($decodeItems->previous as $column => $decodeItem) {
                    $previousGroupItems[$column] = $decodeItem;
                }
            }
        }
        return $this->render('view', [
            'model' => $model,
            'previous' => $previous,
            'after' => $after,
            'afterGroupItems' => $afterGroupItems,
            'previousGroupItems' => $previousGroupItems
        ]);
    }



    /**
     * Finds the Log model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Log the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
