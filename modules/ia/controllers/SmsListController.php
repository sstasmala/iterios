<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\SendSmsStatus;
use Yii;
use app\models\search\SendSmsStatus as SendSmsStatusSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestStatusesController implements the CRUD actions for RequestStatuses model.
 */
class SmsListController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestStatuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SendSmsStatusSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }


    protected function findModel($id)
    {
        if (($model = SendSmsStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
