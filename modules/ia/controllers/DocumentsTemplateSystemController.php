<?php

namespace app\modules\ia\controllers;

use app\components\base\TemplatableHandlerInterface;
use app\helpers\TemplatableHandlersHelper;
use app\helpers\TwigHelper;
use app\models\DocumentPlaceholders;
use app\models\DocumentsType;
use app\models\EmailProviders;
use app\models\Languages;
use app\models\LogsEmail;
use app\models\Placeholders;
use app\models\PlaceholdersDocument;
use app\models\Settings;
use app\models\TemplatesHandlers;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\DocumentsTemplate;
use app\models\search\DocumentsTemplate as DocumentsTemplateSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DocumentsTemplateSystemController implements the CRUD actions for DocumentsTemplate model.
 */
class DocumentsTemplateSystemController extends BaseController
{
    protected $template_type = DocumentsTemplate::SYSTEM_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all DocumentsTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentsTemplateSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DocumentsTemplate::SYSTEM_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        $langAll = Languages::find(['activated' => true])
            ->asArray()
            ->all();
        $docTypeAll = DocumentsType::find()
            ->asArray()
            ->all();

        return $this->render('@app/modules/ia/views/documents-template/index', [
            'dataProvider' => $dataProvider,
            'langAll' => $langAll,
            'docTypeAll' => $docTypeAll,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single DocumentsTemplate model.
     *
     * @param integer $id
     * @param bool $notification
     *
     * @return mixed
     */
    public function actionView($id, $notification = false)
    {
        $model = DocumentsTemplate::find()->where(['id' => $id])->one();

        return $this->render('@app/modules/ia/views/documents-template/view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new DocumentsTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $model = new DocumentsTemplate();
        $documentPlaceholder = DocumentPlaceholders::find()->all();
        $model->type = $this->template_type;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/documents-template/create', [
                'placeholders' => $documentPlaceholder,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DocumentsTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionUpdate($id)
    {

        $model = DocumentsTemplate::find()->where(['id' => $id])->one();
        $model->type = $this->template_type;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Yii::$app->request->post('showPreview')){
                return $this->redirect(['preview', 'id' => $model->id]);
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/documents-template/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DocumentsTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DocumentsTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DocumentsTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentsTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPreview($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) $model->save();

        return $this->render('@app/modules/ia/views/documents-template/preview', [
            'model' => $model,
        ]);

    }

    /**
     * @param $lang
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetPlaceholders($lang = 'en')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (empty($lang))
            $lang = 'en';

        $ph_doc_ids = PlaceholdersDocument::find()
            ->select('placeholder_id')->column();
        $docPlaceholder = Placeholders::find()
            ->where(['in', 'id', $ph_doc_ids])
            ->translate($lang)->asArray()->all();

        return array_column($docPlaceholder, 'short_code');
    }


    public function actionGetTemplateBody($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return $model->body;
    }

}
