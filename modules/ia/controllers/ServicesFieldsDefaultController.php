<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\Services;
use app\models\ServicesFields;
use app\models\ServicesLinks;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\ServicesFieldsDefault;
use app\models\search\ServicesFieldsDefault as ServicesFieldsDefaultSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServicesFieldsDefaultController implements the CRUD actions for ServicesFieldsDefault model.
 */
class ServicesFieldsDefaultController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicesFieldsDefault models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicesFieldsDefaultSearch;
//        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),true);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),true,
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single ServicesFieldsDefault model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new ServicesFieldsDefault model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServicesFieldsDefault;
        $model->is_default = 1;
        $model->position = ServicesFieldsDefault::find()->where(['is_default'=>1])->count()+1;
        if ($model->loadWithLang(Yii::$app->request->post())) {
            $services = Services::find()->all();
            foreach ($services as $service)
            {
                $link = ServicesFields::find()->where(['id_service'=>$service->id])->andWhere(['id_field'=>$model->id])->one();
                if(is_null($link))
                {
                    $link = new ServicesFields();
                    $link->id_service = $service->id;
                    $link->id_field = $model->id;
                    $link->position = $model->position;
                    $link->save();
                }else{
                    $link->position = $model->position;
                    $link->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Updates an existing ServicesFieldsDefault model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->is_default = 1;
        if ($model->loadWithLang(Yii::$app->request->post())) {
            $services = Services::find()->all();
            foreach ($services as $service)
            {
                $link = ServicesFields::find()->where(['id_service'=>$service->id])->andWhere(['id_field'=>$model->id])->one();
                if(is_null($link))
                {
//                    $link = new ServicesFields();
//                    $link->id_service = $service->id;
//                    $link->id_field = $model->id;
//                    $link->position = $model->position;
//                    $link->save();
                }else
                {
                    $link->position = $model->position;
                    $link->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ServicesFieldsDefault model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ServicesFieldsDefault model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServicesFieldsDefault the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServicesFieldsDefault::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatePositions()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['Fields']))
            return $this->render('sort');
        $ids = array_keys($data['Fields']);
        $fields = ServicesFieldsDefault::find()->where(['in','id',$ids])->all();
        foreach ($fields as $field) {
            $field->position = $data['Fields'][$field->id];
            $field->save();
        }

        $fields = ServicesFields::find()->where(['in','id_field',$ids])->all();
        foreach ($fields as $field) {
            $field->position = $data['Fields'][$field->id_field];
            $field->save();
        }
        return $this->redirect(['index']);
    }
}
