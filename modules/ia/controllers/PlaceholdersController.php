<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\Placeholders;
use app\models\search\Placeholders as PlaceholdersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CustomPlaceholdersController implements the CRUD actions for CustomPlaceholders model.
 */
class PlaceholdersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomPlaceholders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlaceholdersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Displays a single CustomPlaceholders model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Creates a new CustomPlaceholders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Placeholders;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Updates an existing CustomPlaceholders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Deletes an existing CustomPlaceholders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param      $module
     * @param bool $multi_array
     *
     * @return array
     */
    public static function getFields($module, $multi_array = false): array {
        if (empty($module) || !class_exists($module))
            return [];

        $fields = $module::getFields();

        $data = [];

        foreach ($fields as $key => $field) {
            if ($multi_array) {
                $data[] = [
                    'name' => \is_int($key) ? $field : $key,
                    'text' => $field
                ];
            } else {
                $data[\is_int($key) ? $field : $key] = $field;
            }
        }

        return $data;
    }

    public function actionLoadFields()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $module = \Yii::$app->request->post('module', false);

        return static::getFields($module, true);
    }

    /**
     * Finds the CustomPlaceholders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return \app\models\Placeholders
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     */
    protected function findModel($id)
    {
        if (($model = Placeholders::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
