<?php

namespace app\modules\ia\controllers;

use app\models\FinancialOperations;
use app\models\TariffsOrders;
use app\models\Tenants;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Transactions;
use app\models\search\Transactions as TransactionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransactionsController implements the CRUD actions for Transactions model.
 */
class TransactionsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transactions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransactionsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Transactions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new Transactions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Transactions;
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing Transactions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing Transactions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionProcess()
    {
        $transactions = Transactions::find()->where('financial_operation_id IS NOT NULL')->asArray()->all();
        $ids = array_column($transactions,'financial_operation_id');

        $fo = FinancialOperations::find()->where(['not in','id',$ids])->andWhere(['operation_type'=>1])->all();
        $ords = TariffsOrders::find()->where(['status'=>TariffsOrders::STATUS_NOT_ACTIVE])->all();

        foreach ($fo as $o)
        {
            if($o->operation_type)
            {
                $trans = new Transactions();
                $trans->financial_operation_id = $o->id;
                $trans->action = Transactions::ACTION_CHARGE;
                $trans->sum = $o->sum_usd;
                $trans->date = time();
                $trans->owner = 0;
                $trans->tenant_id = $o->customer_id;
                if($trans->save()) {
                    $customer = Tenants::find()->where(['id'=>$o->customer_id])->one();
                    if($customer!=null) {
                        $customer->balance = (float)$customer->balance + $trans->sum;
                        $customer->save();
                    }
                }
            }
        }
        foreach ($ords as $ord)
        {
            $customer = Tenants::find()->where(['id'=>$ord->customer_id])->one();

            if($customer !== null) {
                $tor = TariffsOrders::find()->where(['customer_id'=>$customer->id])->andWhere(['status'=>TariffsOrders::STATUS_ACTIVE])->all();
                if(empty($tor))
                {
                    $price = $ord->tariff->price;

                    if($customer->balance>$price) {
                        $trans = new Transactions();
                        $trans->tariff_order_id = $ord->id;
                        $trans->tariff_id = $ord->tariff_id;
                        $trans->action = Transactions::ACTION_WRITE_OFF;
                        $trans->sum = $price;
                        $trans->date = time();
                        $trans->owner = 0;
                        $trans->tenant_id = $customer->id;
                        if($trans->save()) {
                            $customer->balance = (float)$customer->balance - $price;
                            $customer->tariff_id = $ord->tariff_id;
                            if($customer->save()) {
                                $ord->status = TariffsOrders::STATUS_ACTIVE;
                                $duration = new \DateTime();
                                $interval = $ord->tariff->tariff_duration;
                                switch ($interval)
                                {
                                    case 0:$duration->add(new \DateInterval('P1M'));break;
                                    case 1:$duration->add(new \DateInterval('P6M'));break;
                                    case 2:$duration->add(new \DateInterval('P1Y'));break;
                                }
                                $ord->duration_date = $duration->getTimestamp();
                                $ord->save();
                            }
                        }
                    }
                }
            }
        }
        return $this->redirect(['index']);
    }
}
