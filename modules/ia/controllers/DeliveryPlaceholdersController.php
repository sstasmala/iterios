<?php

namespace app\modules\ia\controllers;

use app\models\DeliveryPlaceholderTypes;
use app\models\DeliveryTypes;
use app\models\Languages;
use Yii;
use app\models\DeliveryPlaceholders;
use app\models\search\DeliveryPlaceholders as DeliveryPlaceholdersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DeliveryPlaceholdersController implements the CRUD actions for DeliveryPlaceholders model.
 */
class DeliveryPlaceholdersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliveryPlaceholders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveryPlaceholdersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single DeliveryPlaceholders model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = DeliveryPlaceholders::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Creates a new DeliveryPlaceholders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new DeliveryPlaceholders();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $delivery_types = \Yii::$app->request->post('delivery_types', false);
            $this->deliveryTypesSave(!empty($delivery_types) ? $delivery_types : [], $model->id);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing DeliveryPlaceholders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $delivery_types = \Yii::$app->request->post('delivery_types', false);
            $this->deliveryTypesSave(!empty($delivery_types) ? $delivery_types : [], $model->id);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing DeliveryPlaceholders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionLoadPath($module, $search = '')
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return [];
    }

    /**
     * @param array $delivery_types
     * @param int   $delivery_placeholder_id
     *
     * @throws \yii\web\NotFoundHttpException
     */
    private function deliveryTypesSave(array $delivery_types, int $delivery_placeholder_id)
    {
        $delivery_placeholder = $this->findModel($delivery_placeholder_id);
        DeliveryPlaceholderTypes::deleteAll(['delivery_placeholder_id' => $delivery_placeholder->id]);

        if (!empty($delivery_types)) {
            foreach ($delivery_types as $type) {
                $model = DeliveryTypes::findOne($type);

                if ($model !== null) {
                    $delivery_placeholder->link('deliveryPlaceholderTypes', $model);
                }
            }
        }
    }

    /**
     * Finds the DeliveryPlaceholders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DeliveryPlaceholders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DeliveryPlaceholders::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
