<?php

namespace app\modules\ia\controllers;

use app\models\DemoData;
use app\models\EmailsTypes;
use app\models\MessengersTypes;
use app\models\PhonesTypes;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Contacts;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class DemoContactsController extends BaseController
{
   public function behaviors()
   {
       return [
           'verbs' => [
               'class' => VerbFilter::className(),
               'actions' => [
                   'delete' => ['post'],
               ],
           ],
       ];
   }

    private function saveData($data, $id = null)
    {
        $json = $data['Contacts'];

        if (isset($data['ContactsEmails']))
            $json['ContactsEmails'] = $data['ContactsEmails'];

        if (isset($data['ContactsPhones']))
            $json['ContactsPhones'] = $data['ContactsPhones'];

        if (isset($data['ContactsMessengers']))
            $json['ContactsMessengers'] = $data['ContactsMessengers'];

        $model = (empty($id) ? new DemoData() : DemoData::findOne($id));
        $model->type = DemoData::TYPE_CONTACT;
        $model->data = json_encode($json);

        if ($model->save())
            return $model;

        return false;
    }

   /**
    * Lists all Contacts models.
    * @return mixed
    */
   public function actionIndex()
   {
       $model = DemoData::find()->where(['type' => DemoData::TYPE_CONTACT])->asArray()->all();
       $data = [];

       foreach ($model as $demo_data) {
           $data[] = array_merge($demo_data, json_decode($demo_data['data'], true));
       }

       $dataProvider = new ArrayDataProvider([
           'allModels' => $data,
           'pagination' => [
               'pageSize' => 10,
           ],
           //'sort' => []
       ]);

       return $this->render('index', [
           'dataProvider' => $dataProvider
       ]);
   }

    /**
     * Displays a single Contacts model.
     *
     * @param integer $id
     *
     * @return mixed
     */
   public function actionView($id)
   {
       $model = DemoData::find()->where(['id' => $id])->asArray()->one();

       if (!is_null($model))
           $model = array_merge($model, json_decode($model['data'], true));

       return $this->render('view', ['model' => $model]);
   }

   /**
    * Creates a new Contacts model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
   public function actionCreate()
   {
       $data = Yii::$app->request->post();

       if (!empty($data)) {
           $data = $this->saveData($data);

           if ($data->id) {
               return $this->redirect(['view', 'id' => $data->id]);
           }
       }

       return $this->render('create', [
           'model' => new Contacts()
       ]);
   }

   /**
    * Updates an existing Contacts model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id
    * @return mixed
    */
   public function actionUpdate($id)
   {
       $data = Yii::$app->request->post();

       if (!empty($data)) {
           $data = $this->saveData($data, $id);

           if ($data->id) {
               return $this->redirect(['view', 'id' => $data->id]);
           }
       };

       $demo_data = DemoData::find()->where(['id' => $id])->asArray()->one();

       if (!is_null($demo_data))
           $demo_data = array_merge($demo_data, json_decode($demo_data['data'], true));

       return $this->render('update', [
           'model' => new Contacts(),
           'data' => $demo_data
       ]);
   }

    /**
     * Deletes an existing Contacts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
   public function actionDelete($id)
   {
       $this->findModel($id)->delete();

       return $this->redirect(['index']);
   }

   /**
    * Finds the Contacts model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return DemoData the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
   protected function findModel($id)
   {
       if (($model = DemoData::findOne($id)) !== null) {
           return $model;
       } else {
           throw new NotFoundHttpException('The requested page does not exist.');
       }
   }

   public function actionSearchType($name = '', $type)
   {
       Yii::$app->response->format = Response::FORMAT_JSON;
       switch ($type)
       {
           case 'email': return EmailsTypes::find()->andWhere(['like', 'value',$name])
               ->orWhere(['like', 'value', strtolower($name)])
               ->orWhere(['like', 'value', ucfirst(strtolower($name))])->asArray()->all();
           case 'phone': return PhonesTypes::find()->andWhere(['like', 'value',$name])
               ->orWhere(['like', 'value', strtolower($name)])
               ->orWhere(['like', 'value', ucfirst(strtolower($name))])->asArray()->all();
           case 'messenger': return MessengersTypes::find()->andWhere(['like', 'value',$name])
               ->orWhere(['like', 'value', strtolower($name)])
               ->orWhere(['like', 'value', ucfirst(strtolower($name))])->asArray()->all();
       }
       return [];
   }
}
