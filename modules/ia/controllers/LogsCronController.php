<?php

namespace app\modules\ia\controllers;

use app\helpers\LogsCronHelper;
use Yii;
use app\models\LogsCron;
use app\models\search\LogsCron as LogsCronSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogsCronController implements the CRUD actions for LogsCron model.
 */
class LogsCronController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogsCron models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsCronSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single LogsCron model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Deletes an existing LogsCron model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $file = Yii::getAlias('@web') . $model->output_file;

        if (file_exists($file))
            unlink($file);

        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteAll()
    {
        LogsCron::deleteAll();
        return $this->redirect(['index']);
    }

    /**
     * Finds the LogsCron model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogsCron the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogsCron::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
