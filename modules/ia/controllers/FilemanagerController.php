<?php
/**
 * FilemanagerContorller.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ia\controllers;


use app\components\filemanager\FileManager;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use yii\web\HttpException;

class FilemanagerController extends BaseController
{
    public function beforeAction($action)
    {
        if(\Yii::$app->user->isGuest)
            throw new HttpException(404);
        if (!\Yii::$app->authManager->getAssignment('system_admin',\Yii::$app->user->id))
            throw new HttpException(403,'Access denied!');
        if ($action->id == 'upload' || $action->id == 'upload-advanced') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload(){
        $fileManager = new FileManager();
        $fileManager->_uploadPath = Yii::getAlias('@webroot').'/storage/sys_image';
        $fileManager->_uploadUrl = Yii::getAlias('@web').'/storage/sys_image';
        $fileManager->_tmbPath =  Yii::getAlias('@webroot').'/storage/sys_image/thumbnails';
        /*to mount Google Drive ends*/
        $fileManager->connector();
    }
    public function actionAdvanced()
    {
        return $this->render('advanced');
    }

    public function actionUploadAdvanced(){
        $fileManager = new FileManager();
        $fileManager->_admin = true;
        $fileManager->_uploadPath = Yii::getAlias('@webroot').'/storage/';
        $fileManager->_uploadUrl = Yii::getAlias('@web').'/storage/';
        $fileManager->_tmbPath =  Yii::getAlias('@webroot').'/storage/thumbnails';
        /*to mount Google Drive ends*/
        $fileManager->connector();
    }
}