<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\ProviderFunction;
use app\models\search\ProviderFunction as ProviderFunctionSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProviderFunctionController implements the CRUD actions for ProviderFunction model.
 */
class ProviderFunctionController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProviderFunction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProviderFunctionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single ProviderFunction model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = ProviderFunction::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (is_null($model))
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Creates a new ProviderFunction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProviderFunction;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing ProviderFunction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Deletes an existing ProviderFunction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProviderFunction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProviderFunction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProviderFunction::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
