<?php

namespace app\modules\ia\controllers;

use app\models\Placeholders;
use app\models\Translations;
use Yii;
use app\models\PlaceholdersDocument;
use yii\data\ActiveDataProvider;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PlaceholdersDocumentController implements the CRUD actions for PlaceholdersDocument model.
 */
class PlaceholdersDocumentController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlaceholdersDocument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PlaceholdersDocument::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlaceholdersDocument model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new PlaceholdersDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PlaceholdersDocument;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PlaceholdersDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PlaceholdersDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param null $search
     *
     * @return array
     */
    public function actionGetPlaceholders($search = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $phs = Placeholders::find()
            ->select(['id', 'short_code', 'module', 'field', 'fake', 'type_placeholder']);

        if (!empty($search)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $search])
                ->orFilterWhere(['ilike', 'value',  $search])
                ->andWhere('"key" ILIKE :table', [':table'=>"placeholders.short_code.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $phs->andWhere(['in', 'id', array_unique($translate)]);
        }

        $phs = $phs->translate('en')
            ->asArray()->all();

        $data = [];

        foreach ($phs as $ph) {
            $data[] = [
                'id' => $ph['id'],
                'name' => $ph['short_code'],
                'fake' => $ph['fake'],
                'module_class' => $ph['module'],
                'module' => !empty($ph['module']) && \array_key_exists($ph['module'], \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$ph['module']] : '',
                'field' => $ph['field'],
                'type' =>  !empty($ph['type_placeholder']) && \array_key_exists($ph['type_placeholder'],Placeholders::TYPE_PLACEHOLDERS) ? Placeholders::TYPE_PLACEHOLDERS[$ph['type_placeholder']] : '',
                'type_ph' => $ph['type_placeholder']
            ];
        }

        return $data;
    }

    /**
     * Finds the PlaceholdersDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlaceholdersDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlaceholdersDocument::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
