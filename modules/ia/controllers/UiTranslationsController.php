<?php

namespace app\modules\ia\controllers;

use app\helpers\TranslationHelper;
use app\models\Jobs;
use app\models\Languages;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\UiTranslations;
use app\models\search\UiTranslations as UiTranslationsSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UiTranslationsController implements the CRUD actions for UiTranslations model.
 */
class UiTranslationsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UiTranslations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UiTranslationsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        $jobTranslateRun = false;
        if(!is_null(Jobs::findOne(['executor' => 'app\components\executors\UpdateTranslateExecutor']))) $jobTranslateRun = true;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
            'jobTranslateRun' => $jobTranslateRun,
        ]);
    }

    /**
     * Displays a single UiTranslations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = UiTranslations::find()->where(['id'=>$id])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();
        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', ['model' => $model,'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())]);
    }

    /**
     * Creates a new UiTranslations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UiTranslations;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $groups = TranslationHelper::getGroups();
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'groups'=>$groups
            ]);
        }
    }

    /**
     * Updates an existing UiTranslations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = UiTranslations::find()->where(['id'=>$id])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();

        if(is_null($model))
            throw new HttpException('404','Record not found');

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $groups = TranslationHelper::getGroups();
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'groups'=>$groups
            ]);
        }
    }

    /**
     * Deletes an existing UiTranslations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UiTranslations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UiTranslations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UiTranslations::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // DEPRECATED
    // public function actionRefresh($overwrite = false)
    // {
    //     $path = Yii::$app->basePath.'/data/translations.map.php';
    //     $data = require $path;
    //     $pages = $data['pages'];
    //     foreach ($pages as $k => $page) {
    //         foreach ($page as $i => $item)
    //         {
    //             $model = UiTranslations::findOne(['code'=>$i]);
    //             if(is_null($model))
    //             {
    //                 $model = new UiTranslations();
    //                 $model->code=$i;
    //             }
    //             $model->group = 'pages:'.$k;
    //             foreach ($item as $lang => $value)
    //             {
    //                 if(!is_null($model->id))
    //                     if(!is_null($model->translate($lang)->value) && !$overwrite)
    //                         continue;
    //                 $model->value = $value;
    //                 $model->saveWithLang($lang);
    //             }
    //         }
    //     }
    //
    //     $general = $data['general'];
    //     foreach ($general as $i => $item)
    //     {
    //         $model = UiTranslations::findOne(['code'=>$i]);
    //         if(is_null($model))
    //         {
    //             $model = new UiTranslations();
    //             $model->code=$i;
    //         }
    //         $model->group = 'general';
    //         foreach ($item as $lang => $value)
    //         {
    //             if(!is_null($model->id))
    //                 if(!is_null($model->translate($lang)->value) && !$overwrite)
    //                     continue;
    //             $model->value = $value;
    //             $model->saveWithLang($lang);
    //         }
    //     }
    //
    //     return $this->redirect(['index']);
    //
    // }

    /**
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionUpdateTranslations()
    {

        if(is_null(Jobs::findOne(['executor' => 'app\components\executors\UpdateTranslateExecutor']))){
            $job = new Jobs();
            $job->executor = 'app\components\executors\UpdateTranslateExecutor';
            $job->save();
        }

        return $this->redirect(['index']);
    }
}
