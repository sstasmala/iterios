<?php
/**
 * DashboardController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ia\controllers;

use app\components\events\SystemUserRegistrationEvent;
use app\components\handlers\SystemUserRegistrationHandler;
use app\components\mailers_sms\smsclub\SmsMailer;
use app\models\UiTranslations;
use app\models\User;
use app\modules\ia\controllers\base\BaseController;
use yii\web\Controller;

class DashboardController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

//    public function actionTrigger()
//    {
//        $model = User::findOne(['id'=>\Yii::$app->user->id]);
//        $event = new SystemUserRegistrationEvent($model,'12345');
//        \Yii::$container->get('systemEmitter')->trigger($event);
//    }

    // public function actionSms()
    // {
    //     $mailer = new SmsMailer([
    //         'token' => [
    //             'username' => '380669706549',
    //             //'token' => 'behfuiwgfyuiwbc'
    //             'token' => 'Uag61VOtziI11x6'
    //         ],
    //         'login' => [
    //             'username' => null,
    //             'password' => null
    //         ]
    //     ]);
    //
    //     $res = $mailer->getSmsStatus('477707245');
    //
    //     var_dump($res);
    // }
}