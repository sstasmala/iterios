<?php

namespace app\modules\ia\controllers;

use app\models\DocumentsTemplate;
use app\models\Languages;
use Yii;
use app\models\DocumentPlaceholders;
use app\models\search\DocumentPlaceholders as DocumentPlaceholdersSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DocumentPlaceholdersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentPlaceholdersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }

    /**
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = DocumentPlaceholders::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();
//        $fields = $model->module::
        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DocumentPlaceholders;
        $placeholder = DocumentsTemplate::find()->all();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'placeholder' => $placeholder,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionLoadPath()
    {
        $request = Yii::$app->request->get();

        return json_encode(DocumentPlaceholders::getAllPath($request['module']));
    }

    /**
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return \app\models\DocumentPlaceholders|null the loaded model
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentPlaceholders::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
