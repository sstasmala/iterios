<?php

namespace app\modules\ia\controllers;

use app\models\DeliveryPlaceholders;
use app\models\DeliveryPlaceholderTypes;
use app\models\Languages;
use app\models\Placeholders;
use app\models\PlaceholdersDelivery;
use app\models\PlaceholdersDeliveryTypes;
use Yii;
use app\models\DeliveryEmailTemplates;
use app\models\search\DeliveryEmailTemplates as DeliveryEmailTemplatesSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DeliveryEmailTemplatesController implements the CRUD actions for DeliveryEmailTemplates model.
 */
class DeliveryEmailTemplatesSystemController extends BaseController
{
    protected $template_type = DeliveryEmailTemplates::SYSTEM_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliveryEmailTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveryEmailTemplatesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DeliveryEmailTemplates::SYSTEM_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/delivery-email-templates/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single DeliveryEmailTemplates model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = DeliveryEmailTemplates::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (null === $model)
            throw new HttpException('404','Record not found');

        return $this->render('@app/modules/ia/views/delivery-email-templates/view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
        ]);
    }

    /**
     * Creates a new DeliveryEmailTemplates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DeliveryEmailTemplates;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->type = $this->template_type;

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/delivery-email-templates/create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing DeliveryEmailTemplates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $model->type = $this->template_type;

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/delivery-email-templates/update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionPreview($id)
    {
        $model = $this->findModel($id);

        $model->loadWithLang(Yii::$app->request->post());
        $model->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/delivery-email-templates/preview', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing DeliveryEmailTemplates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param        $type
     * @param string $lang
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetPlaceholders($type, $lang = 'en')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!empty($type)) {
            $pl_ids = PlaceholdersDeliveryTypes::findAll(['delivery_type_id' => $type]);

            if ($pl_ids !== null) {
                $pl_ids = array_column($pl_ids, 'placeholder_delivery_id');

                if ( ! empty($pl_ids)) {
                    $ph_delivery_ids = PlaceholdersDelivery::find()
                        ->select('placeholder_id')
                        ->where(['in', 'id', $pl_ids])->column();

                    $placeholders = Placeholders::find()
                        ->where(['in', 'id', $ph_delivery_ids])
                        ->translate($lang)
                        ->asArray()->all();

                    return array_column($placeholders, 'short_code');
                }
            }
        }

        return [];
    }

    /**
     * Finds the DeliveryEmailTemplates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DeliveryEmailTemplates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DeliveryEmailTemplates::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
