<?php
/**
 * MailerController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ia\controllers;


use app\models\Tenants;
use app\modules\ia\controllers\base\BaseController;
use SSilence\ImapClient\ImapClient;
use SSilence\ImapClient\ImapClientException;
use SSilence\ImapClient\ImapConnect;

class MailerController extends BaseController
{
    private function connect()
    {
        $tenant = Tenants::find()->where(['use_webmail'=>1])->one();
        if(is_null($tenant)) {
            throw new \Exception('Tenant is not choosen!');
        }
        $config = $tenant->mailer_config;
        if(is_null($config)){
            throw new \Exception('Config not defined!');
        }
        $config = json_decode($config,true);
        $encrypt = ImapConnect::ENCRYPT_NOTLS;
        if($config['imap_encrypt'] == 'SSL')
            $encrypt = ImapConnect::ENCRYPT_SSL;
        if($config['imap_encrypt'] == 'TLS')
            $encrypt = ImapConnect::ENCRYPT_TLS;
        $imap = new \app\components\mailers\imap\ImapClient([
                'flags' => [
                    'service' => \SSilence\ImapClient\ImapConnect::SERVICE_IMAP,
                    'encrypt' => $encrypt,
                    /* This NOVALIDATE_CERT is used when the server connecting to the imap
                     * servers is not https but the imap is. This ignores the failure.
                     */
//                    'validateCertificates' => \SSilence\ImapClient\ImapConnect::NOVALIDATE_CERT
                ],
                'mailbox' => [
                    'remote_system_name' => $config['imap_host'],
                    'port' => $config['imap_port']
                ],
                'connect' => [
                    'username' => $config['imap_user'],
                    'password' => $config['imap_pass']
                ]
            ]);
        return $imap;
    }

    public function actionIndex($folder = null,$start = 0)
    {
        try{
        $imap = $this->connect();
        }
        catch (ImapClientException $e)
        {
            return $this->render('index',['error'=>$e->getInfo()]);
        }
        catch (\Exception $e)
        {
            return $this->render('index',['error'=>$e->getMessage()]);
        }
        $folders = $imap->getFoldersDecode();
        $unread = [];
        foreach ($folders as $k=>$f)
        {
            $imap->selectFolder($folders[$k]['id']);
            $unread[$k] = $imap->countUnreadMessages();
        }
        if(!is_null($folder)){
            if(!isset($folders[$folder]))
                return $this->render('index',['error'=>"Folder not exist!"]);
        }
        else
            $folder = 0;
        $imap->selectFolder($folders[$folder]['id']);

        $total = $imap->countMessages();
        if($start<0)
            $start=0;
        if(($start+1)*25>=$total)
            $start = intval($total/25);
        $messages = $imap->getMessages(25,$start);
        return $this->render('index',['folders'=>$folders,'messages'=>$messages,'folder'=>$folder,'total'=>$total,'start'=>$start,'unread'=>$unread]);
    }


    public function actionRead($folder,$message_id)
    {
        try{
            $imap = $this->connect();
            $folders = $imap->getFoldersDecode();
            if(!is_null($folder)){
                if(!isset($folders[$folder]))
                    return $this->render('index',['error'=>"Folder not exist!"]);
            }
            else
                return $this->render('index',['error'=>"Folder required!"]);

            $imap->selectFolder($folders[$folder]['id']);
            $message = $imap->getMessage(intval($message_id));
            $imap->setReaded($message_id);
            $unread = [];
            foreach ($folders as $k=>$f)
            {
                $imap->selectFolder($f['id']);
                $unread[$k] = $imap->countUnreadMessages();
            }
            $imap->selectFolder($folders[$folder]['id']);
        }catch (\Exception $e)
        {
            return $this->render('index',['error'=>$e->getMessage()]);
        }

        return $this->render('index',['folders'=>$folders,'folder'=>$folder,'unread'=>$unread,'message'=>$message]);
    }

    public function actionCompose()
    {
        $imap = $this->connect();
        $folders = $imap->getFoldersDecode();
        $unread = [];
        foreach ($folders as $k=>$f)
        {
            $imap->selectFolder($f['id']);
            $unread[$k] = $imap->countUnreadMessages();
        }
        return $this->render('compose',compact('folders','unread'));
    }

    public function actionSend()
    {
        $post = \Yii::$app->request->post();
        if(!isset($post['to']) || empty($post['to']))
            return $this->render('_error_page',['message'=>'Field "to" required!']);

        $tenant = Tenants::find()->where(['use_webmail'=>1])->one();
        if(is_null($tenant)) {
            return $this->render('_error_page',['message'=>"Tenant is not choosen!"]);
        }
        $config = $tenant->mailer_config;
        if(is_null($config)){
            return $this->render('_error_page',['message'=>"Config not defined!"]);
        }
        $config = json_decode($config,true);

        try{
            $transport = (new \Swift_SmtpTransport($config['smtp_host'],$config['smtp_port'],$config['smtp_encrypt']))
                ->setUsername($config['smtp_user'])->setPassword($config['smtp_pass']);
            $mailer = new \Swift_Mailer($transport);
            $message = new \Swift_Message();
            $message->setSubject($post['subject']);
            $message->setFrom($config['smtp_from']);
            $message->setTo($post['to']);
            $message->setBody($post['body'],'text/html');
            $message->addPart(html_entity_decode(strip_tags($post['body'])),'text/plain');
            $mailer->send($message);
        }
        catch (\Exception $e)
        {
            return $this->render('_error_page',['message'=>$e->getMessage()]);
        }
        try{
            $imap = $this->connect();
            $data = $message->toString();
            $imap->saveMessageInSentA($data);
        }
        catch (ImapClientException $e)
        {
            return $this->render('index',['error'=>$e->getInfo()]);
        }
        catch (\Exception $e)
        {
            return $this->render('index',['error'=>$e->getMessage()]);
        }
        return $this->redirect('index');

    }
}