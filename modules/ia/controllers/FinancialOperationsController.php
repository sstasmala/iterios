<?php

namespace app\modules\ia\controllers;

use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\models\PaymentGateways;
use app\models\Tenants;
use app\modules\ia\controllers\base\BaseController;
use app\utilities\exchanger\CurrencyExchanger;
use Yii;
use app\models\FinancialOperations;
use app\models\search\FinancialOperations as FinancialOperationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * FinancialOperationsController implements the CRUD actions for FinancialOperations model.
 */
class FinancialOperationsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinancialOperations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinancialOperationsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single FinancialOperations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (isset($model->date))
            $model->date = DateTimeHelper::convertTimestampToDateTime($model->date, 'Y-m-d H:i:s', date_default_timezone_get());
        return $this->render('view', ['model' => $model]);
    }


    private function processDate($data)
    {
        if(isset($data['FinancialOperations']['sum'])) {
            $cur = 'USD';
            if (isset($data['FinancialOperations']['currency']))
                $cur = MCHelper::getCurrencyById($data['FinancialOperations']['currency'],'en')[0]['iso_code'];
            $data['FinancialOperations']['sum_usd'] = CurrencyExchanger::exchangeFromTo($data['FinancialOperations']['sum'],$cur,'USD');
        }
        if(isset($data['FinancialOperations']['date']))
            $data['FinancialOperations']['date']
                = DateTimeHelper::convertDateTimeToTimestamp($data['FinancialOperations']['date'],'Y-m-d H:i:s',date_default_timezone_get());

        return $data;
    }

    /**
     * Creates a new FinancialOperations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {
        $model = new FinancialOperations;

        $post = Yii::$app->request->post();
        $post = $this->processDate($post);
        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if(isset($model->date))
                $model->date = DateTimeHelper::convertTimestampToDateTime($model->date,'Y-m-d H:i:s',date_default_timezone_get());
            if($type!=null)
                $model->operation_type = $type;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FinancialOperations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        $post = $this->processDate($post);
        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if(isset($model->date))
                $model->date = DateTimeHelper::convertTimestampToDateTime($model->date,'Y-m-d H:i:s',date_default_timezone_get());
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FinancialOperations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinancialOperations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinancialOperations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinancialOperations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionGetCustomer($search = null)
    {
        $query = Tenants::find();

        if(isset($search) && !empty($search))
            $query->where(['ilike','name',$search]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $query->asArray()->all();
    }


    public function actionGetPaymentGateways($search = null)
    {
        $query = PaymentGateways::find();

        if(isset($search) && !empty($search))
            $query->where(['ilike','name',$search]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $query->asArray()->all();
    }

    public function actionExchangeRate($value,$iso = 'USD')
    {
        return CurrencyExchanger::exchangeFromTo($value,$iso,'USD');
    }
}
