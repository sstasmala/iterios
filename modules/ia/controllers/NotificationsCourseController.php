<?php

namespace app\modules\ia\controllers;

use Yii;
use app\models\NotificationsCourse;
use app\models\search\NotificationsCourse as NotificationsCourseSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Symfony\Component\Process\Process;

/**
 * NotificationsCourseController implements the CRUD actions for NotificationsCourse model.
 */
class NotificationsCourseController extends BaseController
{
    private $log_id = null;
    private $log_file_path = null;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all NotificationsCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationsCourseSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single NotificationsCourse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new NotificationsCourse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new NotificationsCourse;
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing NotificationsCourse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NotificationsCourse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    
        return $this->redirect(['index']);
    }

    /**
     * Finds the NotificationsCourse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NotificationsCourse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotificationsCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionForse()
    {
        $date = new \DateTime();
        $process = new Process($this->getCmdLine('notifications/day',true));
        $process->run();
    }

    private function getLogFile($command)
    {
        $date = new \DateTime();
        return $this->log_file_path = \Yii::$app->basePath.'/logs/'.$date->format('Y-m-d').':'.time().':'.str_replace('/',':',$command).'.log';
    }

    private function getCmdLine($command,$with_log = true)
    {
        $date = new \DateTime();
        $line = '';
        $line .= \Yii::$app->params['path_to_php'].' ';
        $line .= \Yii::$app->params['path_to_app'].'/yii ';
        $line .= $command;
        if($with_log)
        {
            $path = $this->getLogFile($command);
            $line .= ' -l='.$path.' >> '.$path;
        }
        
        return $line;
    }
}
