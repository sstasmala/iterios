<?php

namespace app\modules\ia\controllers;

use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Companies;
use app\models\search\Companies as CompaniesSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompaniesController implements the CRUD actions for Companies model.
 */
class DemoCompaniesController extends BaseController
{
   // public function behaviors()
   // {
   //     return [
   //         'verbs' => [
   //             'class' => VerbFilter::className(),
   //             'actions' => [
   //                 'delete' => ['post'],
   //             ],
   //         ],
   //     ];
   // }
   //
   // /**
   //  * Lists all Companies models.
   //  * @return mixed
   //  */
   // public function actionIndex()
   // {
   //     $searchModel = new CompaniesSearch;
   //     $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
   //
   //     return $this->render('index', [
   //         'dataProvider' => $dataProvider,
   //         'searchModel' => $searchModel,
   //     ]);
   // }
   //
   // /**
   //  * Displays a single Companies model.
   //  * @param integer $id
   //  * @return mixed
   //  */
   // public function actionView($id)
   // {
   //     $model = $this->findModel($id);
   //
   //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
   //         return $this->redirect(['view', 'id' => $model->id]);
   //     } else {
   //         return $this->render('view', ['model' => $model]);
   //     }
   // }
   //
   // /**
   //  * Creates a new Companies model.
   //  * If creation is successful, the browser will be redirected to the 'view' page.
   //  * @return mixed
   //  */
   // public function actionCreate()
   // {
   //     $model = new Companies;
   //
   //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
   //         return $this->redirect(['view', 'id' => $model->id]);
   //     } else {
   //         return $this->render('create', [
   //             'model' => $model,
   //         ]);
   //     }
   // }
   //
   // /**
   //  * Updates an existing Companies model.
   //  * If update is successful, the browser will be redirected to the 'view' page.
   //  * @param integer $id
   //  * @return mixed
   //  */
   // public function actionUpdate($id)
   // {
   //     $model = $this->findModel($id);
   //
   //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
   //         return $this->redirect(['view', 'id' => $model->id]);
   //     } else {
   //         return $this->render('update', [
   //             'model' => $model,
   //         ]);
   //     }
   // }
   //
   // /**
   //  * Deletes an existing Companies model.
   //  * If deletion is successful, the browser will be redirected to the 'index' page.
   //  * @param integer $id
   //  * @return mixed
   //  */
   // public function actionDelete($id)
   // {
   //     $this->findModel($id)->delete();
   //
   //     return $this->redirect(['index']);
   // }
   //
   // /**
   //  * Finds the Companies model based on its primary key value.
   //  * If the model is not found, a 404 HTTP exception will be thrown.
   //  * @param integer $id
   //  * @return Companies the loaded model
   //  * @throws NotFoundHttpException if the model cannot be found
   //  */
   // protected function findModel($id)
   // {
   //     if (($model = Companies::findOne($id)) !== null) {
   //         return $model;
   //     } else {
   //         throw new NotFoundHttpException('The requested page does not exist.');
   //     }
   // }
}
