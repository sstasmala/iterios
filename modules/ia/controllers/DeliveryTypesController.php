<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\DeliveryTypes;
use yii\data\ActiveDataProvider;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeliveryTypesController implements the CRUD actions for DeliveryTypes model.
 */
class DeliveryTypesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliveryTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DeliveryTypes::find()->orderBy(['id' => SORT_ASC])->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single DeliveryTypes model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Updates an existing DeliveryTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data = [];

        if (!empty(Yii::$app->request->post())) {
            $data['DeliveryTypes']['name'] = Yii::$app->request->post()['DeliveryTypes']['name'];

            if ($model->loadWithLang($data)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Finds the DeliveryTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return DeliveryTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     */
    protected function findModel($id)
    {
        if (($model = DeliveryTypes::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
