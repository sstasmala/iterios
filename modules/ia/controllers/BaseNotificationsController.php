<?php

namespace app\modules\ia\controllers;

use app\components\constructor\ConditionConstructor;
use app\helpers\BuilderHelper;
use app\helpers\TwigHelper;
use app\models\Languages;
use app\models\SystemHolidays;
use app\models\Tags;
use app\models\RequestStatuses;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\BaseNotifications;
use app\models\search\BaseNotifications as BaseNotificationsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Notification;
use app\models\Services;

/**
 * BaseNotificationsController implements the CRUD actions for BaseNotifications model.
 */
class BaseNotificationsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BaseNotifications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BaseNotificationsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSetStatus($id, $status)
    {
        $status = (int)$status;
        $id = (int)$id;

        if (empty($id))
            return false;

        BaseNotifications::updateAll(['status' => $status], ['id' => $id]);

        return Yii::$app->response->redirect(['ia/reminder-constructor']);
    }


    /**
     * Lists all BaseNotifications models.
     * @return mixed
     */
    public function actionCalendar()
    {
        $searchModel = new BaseNotificationsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('calendar', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single BaseNotifications model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $translator = new ConditionConstructor(json_decode($model->condition,true));
        $translator->makeQueries();
        $r = $translator->executeQueries(null);
        return $this->render('view', ['model' => $model]);

    }

    public function actionGetServices($name = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $lang =  Yii::$app->session->get('lang', Languages::getDefault());
        $services = Services::find()->translate($lang->iso)->asArray()->all();
        if (empty($services))
            return [];

        return $services;
    }

    public function actionGetServiceById($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = intval($id);
        $lang =  Yii::$app->session->get('lang', Languages::getDefault());
        $service = Services::find()->where(['id' => $id])->translate($lang->iso)->asArray()->one();
        if (empty($service))
            return [];

        return $service;
    }

    public function actionGetHolidays($name = null) {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $lang =  Yii::$app->session->get('lang', Languages::getDefault());
        $holidays = SystemHolidays::find()->translate($lang->iso)->asArray()->all();

        if (empty($holidays))
            return [];

        return $holidays;
    }

    public function actionGetHolidayById($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = intval($id);
        $lang =  Yii::$app->session->get('lang', Languages::getDefault());
        $holiday = SystemHolidays::find()->where(['id' => $id])->translate($lang->iso)->asArray()->one();
        if (empty($holiday))
            return [];

        return $holiday;
    }

    private function getConditionOrders() {
        $lang =  Yii::$app->session->get('lang', Languages::getDefault());
        $services = Services::find()->translate($lang->iso)->asArray()->all();
        if (empty($services))
            return [];

        return $services;


        /*

            {
                id: 'holidays.system',
                label: 'System Holidays',
                type: 'integer',
                operators: ['equal'],
                input: function (rule, input_name) {
                    // $('#basenotifications-trigger[value=2]').prop('disabled', false).show();
                    return '<input type=hidden value="1"  name="' + input_name + '">';
                },
                default_value:1
            },


        */

/*
        $return = [];     
        foreach ($services as $serv) {
            print_r($serv);exit();
            $return[] = [
                'id' => 
            ];
        }

        return [];*/
    }

    /**
     * Creates a new BaseNotifications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BaseNotifications;

        $post = Yii::$app->request->post();

        if (!empty($post['BaseNotifications']['subject'])) {
            foreach ($post['BaseNotifications']['subject'] as $lng => $subject)
                $post['BaseNotifications']['subject'][$lng] = trim(strip_tags($subject));
        }
        if (!empty($post['BaseNotifications']['sms'])) {
            foreach ($post['BaseNotifications']['sms'] as $lng => $subject)
                $post['BaseNotifications']['sms'][$lng] = trim(strip_tags($subject));
        }
        if(!empty($post))
            if (!empty($post['BaseNotifications']['condition']))
                $post['BaseNotifications']['condition'] = BuilderHelper::unformatConfig($post['BaseNotifications']['condition'],true);
        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'condition_orders' => $this->getConditionOrders(),
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Updates an existing BaseNotifications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $lang =  Yii::$app->session->get('lang',Languages::getDefault());
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if (!empty($post['BaseNotifications']['subject'])) {
            foreach ($post['BaseNotifications']['subject'] as $lng => $subject)
                $post['BaseNotifications']['subject'][$lng] = trim(strip_tags($subject));
        }
        if (!empty($post['BaseNotifications']['sms'])) {
            foreach ($post['BaseNotifications']['sms'] as $lng => $subject)
                $post['BaseNotifications']['sms'][$lng] = trim(strip_tags($subject));
        }

        if(!empty($post))
            if (!empty($post['BaseNotifications']['condition'])) {

                $post['BaseNotifications']['condition'] = BuilderHelper::filterConditionRules($post['BaseNotifications']['condition'], $post);

                if ($post['BaseNotifications']['condition'])
                    $post['BaseNotifications']['condition'] = BuilderHelper::unformatConfig($post['BaseNotifications']['condition'],true);
            }
            

        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->condition = BuilderHelper::formatConfig($model->condition,true);
            return $this->render('update', [
                'model' => $model,
                'condition_orders' => $this->getConditionOrders(),
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing BaseNotifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BaseNotifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BaseNotifications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BaseNotifications::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetStatuses($name = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(is_null($name))
            return RequestStatuses::find()->asArray()->all();

        return RequestStatuses::find()->andFilterWhere(['ilike', 'name', $name])->asArray()->all();
    }

    public function actionGetStatusById($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = intval($id);
        $status = RequestStatuses::find()->where(['id'=>$id])->asArray()->one();
        return $status;
    }

    public function actionGetTags($name = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(is_null($name))
            return Tags::find()->asArray()->all();

        return Tags::find()->andFilterWhere(['ilike', 'value', $name])->asArray()->all();
    }

    public function actionGetTagById($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = intval($id);
        $tag = Tags::find()->where(['id'=>$id])->asArray()->one();
        return $tag;
    }
}
