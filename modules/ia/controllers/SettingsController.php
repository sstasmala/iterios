<?php

namespace app\modules\ia\controllers;

use app\models\EmailProviders;
use app\models\Languages;
use app\models\Settings;
use app\modules\ia\controllers\base\BaseController;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\Response;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $languages = Languages::findAll(['activated' => true]);
        $email_providers_settings = Settings::getByGroup(Settings::EMAIL_PROVIDERS_GROUP);
        $email_providers = EmailProviders::find()->all();
        $MCHelper_settings = Settings::getByGroup(Settings::MCHELPER_GROUP);

        $email_providers_data = [];

        foreach ($email_providers as $email_provider) {
            if (empty($email_provider['type']))
                continue;

            $email_providers_data[$email_provider['type']][$email_provider['id']] = $email_provider['name'] . ' (' . $email_provider['provider'] . ')';
        }

        return $this->render(
            'index',
            compact(
                'languages', 'email_providers_settings', 'email_providers_data', 'MCHelper_settings'
            )
        );
    }

    /**
     * @param null $name
     *
     * @return array
     */
    public function actionGetLanguagesList($name = null)
    {
        if(is_null($name))
            $name = '';

        $query = new Query();
        $query->select([])->from('languages')
            ->andWhere(['like', 'name', $name])->orWhere(['ilike', 'name', $name])
            ->orWhere(['like', 'original_name', $name])->orWhere(['ilike', 'original_name', $name])
            ->orWhere(['like', 'iso', $name])->orWhere(['ilike', 'iso', $name])
            ->andWhere(['activated'=>false]);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $result = $query->all();

        return $result;
    }

    /**
     * @return bool
     * @throws HttpException
     */
    public function actionActivateLanguages()
    {
        $data = \Yii::$app->request->bodyParams;

        if(!isset($data['ids']))
            throw new HttpException(400,'Missing parameter ids in request body!');

        foreach ($data['ids'] as $v)
        {
            $lang = Languages::findOne(['id' => $v]);

            if(is_null($lang))
                throw new HttpException(400,'Language with id: '.$v.'not found!');

            $lang->activated = true;
            $lang->save();
        }

        return true;
    }

    /**
     * @param $id
     * @throws HttpException
     */
    public function actionDisableLang($id)
    {
        $lang = Languages::findOne(['id' => $id]);

        if (is_null($lang))
            throw new HttpException(400,'Language with id: '.$id.'not found!');

        $lang->activated = false;
        $lang->save();

        $this->redirect(\Yii::$app->params['baseUrl'].'/ia/settings');
    }

    /**
     * @return bool
     */
    public function actionSettingsSave()
    {
        $data = \Yii::$app->request->bodyParams;

        foreach ($data as $key => $val) {
            if (Settings::getByKey($key) === false)
                continue;

            Settings::saveByKey($key, $val);
        }

        return true;
    }
}
