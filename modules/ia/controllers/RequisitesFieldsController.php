<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\Requisites;
use app\models\RequisitesTenants;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\RequisitesFields;
use app\models\search\RequisitesFields as RequisitesFieldsSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequisitesFieldsController implements the CRUD actions for RequisitesFields model.
 */
class RequisitesFieldsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequisitesFields models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequisitesFieldsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single RequisitesFields model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = RequisitesFields::find()->where(['id'=>$id])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();

        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
        ]);
    }

    /**
     * Creates a new RequisitesFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequisitesFields;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $requisites = Requisites::find()->where(['requisites_group_id'=>$model->requisite_group_id])->asArray()->all();
            $countries = array_combine(array_column($requisites,'country_id'),array_column($requisites,'group_position'));
            foreach ($countries as $c=>$g)
            {
                $exist = Requisites::find()->where(['requisites_group_id'=>$model->requisite_group_id])
                    ->andWhere(['requisites_field_id'=>$model->id])->andWhere(['country_id'=>$c])->one();
                if(is_null($exist))
                {
                    $req = new Requisites();
                    $req->country_id = $c;
                    $req->group_position = $g;
                    $req->field_position = 999;
                    $req->requisites_field_id = $model->id;
                    $req->requisites_group_id = $model->requisite_group_id;
                    $req->save();
                }else
                    $req = $exist;
                $ids = array_column($requisites, 'id');
                $rt = RequisitesTenants::find()->where(['in', 'requisite_id', $ids])->asArray()->all();
                $sets = array_unique(array_combine(array_column($rt, 'tenant_id'), array_column($rt, 'set_id')));
                foreach ($sets as $tenant => $set_id) {
                    $set = RequisitesTenants::find()->where(['set_id' => $set_id])->andWhere(['requisite_id' => $req->id])->one();
                    if (is_null($set)) {
                        $set = new RequisitesTenants();
                        $set->set_id = $set_id;
                        $set->tenant_id = $tenant;
                        $set->requisite_id = $req->id;
                        if ($model->type == 6)
                            $set->value = '[]';
                        else
                            $set->value = '';
                        $set->save();
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing RequisitesFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            $requisites = Requisites::find()->where(['requisites_group_id' => $model->requisite_group_id])->asArray()->all();
            $countries = array_combine(array_column($requisites, 'country_id'), array_column($requisites, 'group_position'));
            foreach ($countries as $c => $g) {
                $exist = Requisites::find()->where(['requisites_group_id' => $model->requisite_group_id])
                    ->andWhere(['requisites_field_id' => $model->id])->andWhere(['country_id' => $c])->one();
                if (is_null($exist)) {
                    $req = new Requisites();
                    $req->country_id = $c;
                    $req->group_position = $g;
                    $req->field_position = 999;
                    $req->requisites_field_id = $model->id;
                    $req->requisites_group_id = $model->requisite_group_id;
                    $req->save();
                }else
                    $req = $exist;
                $ids = array_column($requisites, 'id');
                $rt = RequisitesTenants::find()->where(['in', 'requisite_id', $ids])->asArray()->all();
                $sets = array_unique(array_combine(array_column($rt, 'tenant_id'), array_column($rt, 'set_id')));
                foreach ($sets as $tenant => $set_id) {
                    $set = RequisitesTenants::find()->where(['set_id' => $set_id])->andWhere(['requisite_id' => $req->id])->one();
                    if (is_null($set)) {
                        $set = new RequisitesTenants();
                        $set->set_id = $set_id;
                        $set->tenant_id = $tenant;
                        $set->requisite_id = $req->id;
                        if ($model->type == 6)
                            $set->value = '[]';
                        else
                            $set->value = '';
                        $set->save();
                    }
                }

            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    /**
     * Deletes an existing RequisitesFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequisitesFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RequisitesFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequisitesFields::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
