<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\models\SocialTypesUploadForm;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\SocialsTypes;
use app\models\search\SocialsTypes as SocialsTypesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SocialsTypesController implements the CRUD actions for SocialsTypes model.
 */
class SocialsTypesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SocialsTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SocialsTypesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SocialsTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = SocialsTypes::find()->where(['id'=>$id])->one();
        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new SocialsTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SocialsTypes;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $imgImage = new SocialTypesUploadForm();
            $imgImage->src = UploadedFile::getInstance($model, 'img');
            $upload = $imgImage->upload();

            if ($upload !== false)
                $model->img = $upload;

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SocialsTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->img;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (!empty($oldImage))
                $model->img = $oldImage;

            $imgImage = new SocialTypesUploadForm();
            $imgImage->src = UploadedFile::getInstance($model, 'img');
            $upload = $imgImage->upload();

            if ($upload !== false) {
                if (!empty($model->img))
                    FileHelper::unlink($model->img);

                $model->img = $upload;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SocialsTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SocialsTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialsTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SocialsTypes::findOne($id)) !== null) {
            return $model;
            //return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
