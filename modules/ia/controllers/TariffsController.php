<?php

namespace app\modules\ia\controllers;

use app\helpers\DateTimeHelper;
use app\models\FeaturesTariffs;
use app\models\Languages;
use app\models\search\TariffsFeature;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Tariffs;
use app\models\search\Tariffs as TariffsSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TariffsController implements the CRUD actions for Tariffs model.
 */
class TariffsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tariffs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TariffsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single Tariffs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Tariffs::find()->where(['id'=>intval($id)])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();
        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', ['model' => $model,'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())]);
    }

    /**
     * Creates a new Tariffs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tariffs;
        $post = Yii::$app->request->post();
        if(isset($post['Tariffs']['tariff_start']))
            $post['Tariffs']['tariff_start'] = DateTimeHelper::convertDateToTimestamp($post['Tariffs']['tariff_start'],'Y-m-d',date_default_timezone_get());
        if(isset($post['Tariffs']['tariff_end']))
            $post['Tariffs']['tariff_end'] = DateTimeHelper::convertDateToTimestamp($post['Tariffs']['tariff_end'],'Y-m-d',date_default_timezone_get());
        if ($model->loadWithLang($post)) {
            $linked = FeaturesTariffs::find()->where(['tariff_id'=>$model->id])->asArray()->all();
            $linked = array_combine(array_column($linked,'id'),$linked);
            if(isset($post['FeaturesTariffs']))
            {
                foreach ($post['FeaturesTariffs'] as $feature)
                {
                    if(isset($feature['feature_id']) && $feature['feature_id']!="0")
                    {
                        $record = FeaturesTariffs::find()->where(['feature_id'=>$feature['feature_id']])
                            ->andWhere(['tariff_id'=>$model->id])->one();
                        if(is_null($record))
                            $record = new FeaturesTariffs();
                        if(isset($feature['count']))
                            $record->count = $feature['count'];
                        $record->tariff_id = $model->id;
                        $record->feature_id = intval($feature['feature_id']);
                        if($record->save())
                        {
                            if(isset($linked[$record->id]))
                                unset($linked[$record->id]);
                        }
                    }
                }
            }
            $old = FeaturesTariffs::find()->where(['in','id',array_keys($linked)])->all();
            foreach ($old as $v)
                $v->delete();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $features = TariffsFeature::find()->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->asArray()->all();
            $linked = [];
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'features'=>$features,
                'linked'=>$linked
            ]);
        }
    }

    /**
     * Updates an existing Tariffs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if(isset($post['Tariffs']['tariff_start']))
            $post['Tariffs']['tariff_start'] = DateTimeHelper::convertDateToTimestamp($post['Tariffs']['tariff_start'],'Y-m-d',date_default_timezone_get());
        if(isset($post['Tariffs']['tariff_end']))
            $post['Tariffs']['tariff_end'] = DateTimeHelper::convertDateToTimestamp($post['Tariffs']['tariff_end'],'Y-m-d',date_default_timezone_get());

        if ($model->loadWithLang($post)) {
            $linked = FeaturesTariffs::find()->where(['tariff_id'=>$model->id])->asArray()->all();
            $linked = array_combine(array_column($linked,'id'),$linked);
            if(isset($post['FeaturesTariffs']))
            {
                foreach ($post['FeaturesTariffs'] as $feature)
                {
                    if(isset($feature['feature_id']) && $feature['feature_id']!="0")
                    {
                        $record = FeaturesTariffs::find()->where(['feature_id'=>$feature['feature_id']])
                            ->andWhere(['tariff_id'=>$model->id])->one();
                        if(is_null($record))
                            $record = new FeaturesTariffs();
                        if(isset($feature['count']))
                            $record->count = $feature['count'];
                        $record->tariff_id = $model->id;
                        $record->feature_id = intval($feature['feature_id']);
                        if($record->save())
                        {
                            if(isset($linked[$record->id]))
                                unset($linked[$record->id]);
                        }
                    }
                }
            }
            $old = FeaturesTariffs::find()->where(['in','id',array_keys($linked)])->all();
            foreach ($old as $v)
                $v->delete();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if(isset($model->tariff_start))
                $model->tariff_start = DateTimeHelper::convertTimestampToDate($model->tariff_start,'Y-m-d',date_default_timezone_get());
            if(isset($model->tariff_end))
                $model->tariff_end = DateTimeHelper::convertTimestampToDate($model->tariff_end,'Y-m-d',date_default_timezone_get());

            $features = TariffsFeature::find()->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->asArray()->all();
            $linked = FeaturesTariffs::find()->where(['tariff_id'=>$model->id])->asArray()->all();
            $linked = array_combine(array_column($linked,'feature_id'),$linked);
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
                'features'=>$features,
                'linked'=>$linked
            ]);
        }
    }

    /**
     * Deletes an existing Tariffs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tariffs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tariffs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tariffs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
