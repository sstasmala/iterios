<?php

namespace app\modules\ia\controllers;

use app\models\search\LogsEmailRequest;
use app\models\Templates;
use Yii;
use app\models\LogsEmail;
use app\models\search\LogsEmail as LogsEmailSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * LogsEmailController implements the CRUD actions for LogsEmail model.
 */
class LogsSystemEmailController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogsEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsEmailSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), LogsEmail::TYPE_SYSTEM);

        return $this->render('@app/modules/ia/views/logs-email/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'data_templates' => Templates::find()->orderBy('name')->all()
        ]);
    }

    /**
     * Displays a single LogsEmail model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $searchModel = new LogsEmailRequest();
        $params = Yii::$app->request->queryParams;
        $params['LogsEmailRequest']['log_email_id'] = $id;
        $dataProvider = $searchModel->search($params);

        $method = LogsEmailRequest::find()->select(['method'])->groupBy('method')->asArray()->all();

        return $this->render('@app/modules/ia/views/logs-email/view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data_method' => $method
        ]);
    }

    /**
     * @param      $id
     *
     * @return string
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetMail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->findModel($id)->html;
    }

    /**
     * Finds the LogsEmail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogsEmail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogsEmail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
