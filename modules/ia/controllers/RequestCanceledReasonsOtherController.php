<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\RequestCanceledReasons;
use app\models\search\RequestCanceledReasons as RequestCanceledReasonsSearch;
use yii\filters\VerbFilter;

/**
 * RequestCanceledReasonsController implements the CRUD actions for RequestCanceledReasons model.
 */
class RequestCanceledReasonsOtherController extends RequestCanceledReasonsSystemController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestCanceledReasons models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestCanceledReasonsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            RequestCanceledReasons::TYPE_OTHER,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/request-canceled-reasons/index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }
}
