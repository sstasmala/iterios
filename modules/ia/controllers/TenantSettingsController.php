<?php

namespace app\modules\ia\controllers;

use app\components\events\SystemUserInviteEvent;
use app\helpers\RbacHelper;
use app\helpers\TenantHelper;
use app\models\AgentsMapping;
use app\models\Invites;
use app\models\Languages;
use app\models\User;
use Yii;
use app\models\Tenants;
use app\models\search\Tenants as TenantsSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;


class TenantSettingsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tenants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TenantsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Tenants model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $users = User::find()->select(['id', 'first_name', 'last_name', 'middle_name', 'phone', 'email', 'photo', 'status']);

        $ids = AgentsMapping::find()->where(['tenant_id' => $model->id])->asArray()->all();
        $ids = array_column($ids, 'user_id');

        $invites = Invites::find()->where(['tenant_id' => $model->id])->asArray()->all();
        $invites_id = array_column($invites, 'user_id');

        $users = $users->where(['in', 'id', array_merge($ids, $invites_id)]);
        $users = $users->all();

        $data = [];

        foreach ($users as $i => $user) {
            $data_user = $user->getAttributes();

            $data_user['not_confirm'] = in_array($user['id'], $invites_id) ? true : false;
            $data_user['is_tenant_owner'] = $user['id'] === $model->owner_id ? true : false;
            $data_user['blocked'] = $user->checkTenantBlock($model->id) ? true : false;

            $data[] = $data_user;
        }

        return $this->render('view', ['model' => $model, 'users' => $data]);
    }

    /**
     * Updates an existing Tenants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users = User::findAll(['status' => User::STATUS_ACTIVE]);
        $languages = Languages::findAll(['activated' => true]);
        $post = Yii::$app->request->post();
        if(isset($post['Tenants']['mailer_config']))
            $post['Tenants']['mailer_config'] = json_encode($post['Tenants']['mailer_config']);
        $old_owner_id = $model->owner_id;

        if ($model->load($post) && $model->save()) {
            $agent_mapping = AgentsMapping::findOne(['tenant_id' => $id, 'user_id' => $model->owner_id]);
            if($model->use_webmail)
                Tenants::updateAll(['use_webmail'=>0],['<>','id',$model->id]);
            if ($old_owner_id != $model->owner_id && is_null($agent_mapping))
                $model->link('users', User::findOne($model->owner_id));

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
                'languages' => $languages
            ]);
        }
    }

    /**
     * @param $id
     * @param $user_id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteInvites($id, $user_id)
    {
        Invites::findOne(['user_id' => $user_id, 'tenant_id' => $id])->delete();

        return $this->redirect(['/ia/tenants/view?id='.$id]);
    }

    /**
     * @param      $tenant_id
     * @param null $name
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetUsers($tenant_id, $name = null)
    {
        if(is_null($name))
            $name = '';

        $tenant_users = $this->findModel($tenant_id)->getUsers()->select('id')->column();

        $query = User::find();
        $query->andWhere(['like', 'first_name', $name])
            ->orWhere(['like', 'first_name', mb_strtolower($name, 'UTF-8')])
            ->orWhere(['like', 'first_name', mb_convert_case(mb_strtolower($name, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')]);
        $query->orWhere(['like', 'last_name', $name])
            ->orWhere(['like', 'last_name', mb_strtolower($name, 'UTF-8')])
            ->orWhere(['like', 'last_name', mb_convert_case(mb_strtolower($name, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')]);
        $query->andWhere(['not in', 'id', $tenant_users]);
        $users = $query->all();

        $data = [];

        foreach ($users as $user)
        {
            $data[] = [
                'id' => $user->id,
                'name' => $user->last_name . ' ' . $user->first_name,
                'email' => $user->email
            ];
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
    }

    /**
     * Finds the Tenants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tenants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tenants::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionResetBaseNotifications()
    {
        $tenants = Tenants::find()->all();
        foreach ($tenants as $t)
            TenantHelper::setBasteNotifications($t->id);
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param $user_id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\HttpException
     */
    public function actionRepeatInvite($id, $user_id) {
        $user = User::find()->where(['id' => $user_id])->one();

        if (is_null($user))
            throw new HttpException(404,"User #$user_id is not found");

        $invites = Invites::find()->where(['tenant_id' => $id, 'user_id' => $user_id])->one();

        if (is_null($invites))
            throw new HttpException(404,"Invite is not found");

        $token = Yii::$app->security->generateRandomString();
        $tenant = Tenants::findOne(['id' => $id]);

        $event = new SystemUserInviteEvent($user, $tenant, $token, $invites->user_role);
        Yii::$container->get('systemEmitter')->trigger($event);

        return $this->redirect(['/ia/tenants/view?id='.$id.'&send_invite=1']);
    }

    public function actionBlock($id, $user_id)
    {
        $user = User::find()->where(['id' => $user_id])->one();

        $agents_mappings = AgentsMapping::findOne(['user_id' => $user->id, 'tenant_id' => $id]);

        if (!is_null($agents_mappings)) {
            $agents_mappings->block = $agents_mappings->block ? false : true;
            $agents_mappings->save();
        }

        return $this->redirect(['/ia/tenants/view?id='.$id]);
    }
}