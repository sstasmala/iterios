<?php

namespace app\modules\ia\controllers;

use app\models\RequisitesGroups;
use app\models\search\RequisitesFields;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Requisites;
use app\models\search\Requisites as RequisitesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * RequisitesController implements the CRUD actions for Requisites model.
 */
class RequisitesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Requisites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequisitesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Requisites model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $models = Requisites::find()->where(['country_id' => $id])->all();
        $country = \app\helpers\MCHelper::getCountryById($id, 'en')[0]['title'];

        $fields = [];
        $groups = [];

        foreach ($models as $model) {
            $fields[$model->requisitesGroup->name][] = $model->requisitesField->name;
            $groups[] = $model->requisitesGroup->name;
        }
        $groups = array_unique($groups);

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
            return $this->render('view',
                [
                    'fields' => $fields,
                    'groups' => $groups,
                    'country' => $country,
                    'countryId' => $id
                ]);
//        }
    }

    /**
     * Creates a new Requisites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Requisites;

//        $requisites = RequisitesFields::find()->all();
        $data = Yii::$app->request->post();
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($data) {
            if(!isset($data['Requisites']['country_id']))
                throw new HttpException(403,'Missing parametr country_id');
            $exist = Requisites::find()->where(['country_id'=>$data['Requisites']['country_id']])->one();
            if(!is_null($exist)) {
                $model->addError($data['Requisites']['country_id'],'Requisites with this lang already exist');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if(isset($data['Requisites']['requisites_group_id']))
            foreach ($data['Requisites']['requisites_group_id'] as $groupPosition => $groupId) {
                if(isset($data['Requisites']['requisites_field_id'][$groupId]))
                foreach ($data['Requisites']['requisites_field_id'][$groupId] as $fieldPosition => $fieldId) {
                    $model = new Requisites;
                    $model->country_id = $data['Requisites']['country_id'];
                    $model->requisites_group_id = $groupId;
                    $model->requisites_field_id = $fieldId;
                    $model->group_position = $groupPosition;
                    $model->field_position = $fieldPosition;
                    $model->in_header = $data['Requisites']['in_header'][$groupId][$fieldId];
                    $model->save();
                }
            }
            return $this->redirect(['view', 'id' => $data['Requisites']['country_id']]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Requisites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $data = Yii::$app->request->post();
        if (isset($data['Requisites']['country_id'])) {
            $countryId = intval($data['Requisites']['country_id']);
        } else {
            $countryId = intval($id);
        }
        $models = Requisites::find()->where(['country_id' => $countryId])->orderBy(['group_position'=>'ASC','field_position'=>'ASC'])->all();
        $ids = [];
        foreach ($models as $m)
        {
            $ids[$m->id] = $m;
        }
        if ($data) {
            if(isset($data['RequisitesOld']['requisites_group_id']))
            foreach ($data['RequisitesOld']['requisites_group_id'] as $groupPosition => $groupId) {
                if(isset($data['RequisitesOld']['requisites_field_id'][$groupId]))
                foreach ($data['RequisitesOld']['requisites_field_id'][$groupId] as $fieldPosition => $fieldId) {
                    $model = Requisites::find()
                        ->where(['country_id' => $id])
                        ->andWhere(['requisites_group_id' => $groupId])
                        ->andWhere(['requisites_field_id' => $fieldId])
                        ->one();
                    $model->country_id = $countryId;
                    $model->requisites_group_id = $groupId;
                    $model->requisites_field_id = $fieldId;
                    $model->group_position = $groupPosition;
                    $model->field_position = $fieldPosition;
                    $model->in_header = $data['RequisitesOld']['in_header'][$groupId][$fieldId];
                    $model->save();
                    unset($ids[$model->id]);
                }
            }
            if(isset($data['Requisites']['requisites_group_id']))
            if (isset($data['Requisites']['requisites_field_id'])) {
                foreach ($data['Requisites']['requisites_group_id'] as $groupPosition => $groupId) {
                    if(isset($data['Requisites']['requisites_field_id'][$groupId]))
                    foreach ($data['Requisites']['requisites_field_id'][$groupId] as $fieldPosition => $fieldId) {
                        $exist = Requisites::find()
                            ->where(['country_id' => $id])
                            ->andWhere(['requisites_group_id' => $groupId])
                            ->andWhere(['requisites_field_id' => $fieldId])
                            ->one();
                        if(!is_null($exist)) {
                            unset($ids[$exist->id]);
                            continue;
                        }
                        $model = new Requisites;
                        $model->country_id = $countryId;
                        $model->requisites_group_id = $groupId;
                        $model->requisites_field_id = $fieldId;
                        $model->group_position = $groupPosition;
                        $model->field_position = $fieldPosition;
                        $model->in_header = $data['Requisites']['in_header'][$groupId][$fieldId];
                        $model->save();
                    }
                }
            }
            foreach ($ids as $m)
                $m->delete();
            $models = Requisites::find()->where(['country_id' => $countryId])->all();
            if(empty($models))
                return $this->redirect(['index']);
            return $this->redirect(['view', 'id' => $countryId]);
        } else {

            $country = \app\helpers\MCHelper::getCountryById($countryId, 'en')[0]['title'];
            if(!isset($models[0]))
                throw new HttpException(404);
            $model = $this->findModel($models[0]['id']);

            $fields = [];
            $groups = [];

            foreach ($models as $model) {
                $fields[$model->requisitesGroup->name][] = $model;
                $groups[$model->requisitesGroup->id] = $model->requisitesGroup->name;
            }

            return $this->render('update',
                [
                    'model' => $model,
                    'groups' => $groups,
                    'fields' => $fields,
                    'countryId' => $countryId,
//                'models' => $models,
                    'country' => $country,
                ]);
        }
    }

    /**
     * Deletes an existing Requisites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $models = Requisites::find()->where(['country_id' => $id])->all();
        foreach ($models as $model) {
            $model->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Requisites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requisites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Requisites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetGroups()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return RequisitesGroups::find()->all();
    }

    public function actionGetFields()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return RequisitesFields::find()->where(['requisite_group_id' => $_POST['id']])->all();
    }
}
