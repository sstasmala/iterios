<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\SitesTypes;
use app\models\search\SitesTypes as SitesTypesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SitesTypesController implements the CRUD actions for SitesTypes model.
 */
class SitesTypesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SitesTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SitesTypesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams()
            ,Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SitesTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = SitesTypes::find()->where(['id'=>$id])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();
        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', ['model' => $model,'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())]);
    }

    /**
     * Creates a new SitesTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SitesTypes;

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing SitesTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing SitesTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SitesTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SitesTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SitesTypes::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
