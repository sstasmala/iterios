<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05.07.18
 * Time: 14:50
 */

namespace app\modules\ia\controllers;

use app\models\DocumentsTemplate;
use app\models\DocumentsType;
use app\models\Languages;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class DocumentsTemplatePublicController extends DocumentsTemplateSystemController
{
    protected $template_type = DocumentsTemplate::PUBLIC_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySmsTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new \app\models\search\DocumentsTemplate();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DocumentsTemplate::PUBLIC_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        $langAll = Languages::find(['activated' => true])
            ->asArray()
            ->all();
        $docTypeAll = DocumentsType::find()
            ->asArray()
            ->all();

        return $this->render('@app/modules/ia/views/documents-template/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'langAll' => $langAll,
            'docTypeAll' => $docTypeAll,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
        ]);
    }

    /**
     * @return mixed|void
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate()
    {
        throw new NotFoundHttpException();
    }
}
