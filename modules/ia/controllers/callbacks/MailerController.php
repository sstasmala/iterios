<?php
/**
 * MailerController.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\modules\ia\controllers\callbacks;

use app\models\EmailProviders;
use app\models\LogsEmail;
use app\models\LogsEmailRequest;
use yii\web\Controller;
use yii\web\Response;

class MailerController extends Controller
{
    /**
     * @var array [event => email_log status, ...]
     */
    protected $sparkpost_events = [
        'injection' => LogsEmail::STATUS_SENT,
        'delivery' => LogsEmail::STATUS_DELIVERED,
        'open' => LogsEmail::STATUS_OPEN,
        'delay' => LogsEmail::STATUS_DELAY,
        'bounce' => LogsEmail::STATUS_BOUNCE,
        'click' => LogsEmail::STATUS_CLICK,
    ];

    /**
     * @var array [event => email_log status, ...]
     */
    protected $sendgrid_events = [
        'processed' => LogsEmail::STATUS_SENT,
        'delivered' => LogsEmail::STATUS_DELIVERED,
        'open' => LogsEmail::STATUS_OPEN,
        'deferred' => LogsEmail::STATUS_DELAY,
        'bounce' => LogsEmail::STATUS_BOUNCE,
        'click' => LogsEmail::STATUS_CLICK,
    ];

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    private function sendResponse()
    {
        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }

    public function actionSparkPost()
    {
        $request = \Yii::$app->request;
        $raw_body = $request->rawBody;
        $body = json_decode($raw_body, true);

        // Creating request log
        $log = new LogsEmailRequest();
        $log->method = $request->method;
        $log->url = $request->url;
        $log->body = $raw_body;
        $log->headers = json_encode($request->headers->toArray());

        if (is_array($body)) {
            $last_event = array_pop($body);

            if (isset($last_event['msys']['track_event'])) {
                $event = $last_event['msys']['track_event'];
            } elseif (isset($last_event['msys']['message_event'])) {
                $event = $last_event['msys']['message_event'];
            }

            if (isset($event)) {
                $log->event = isset($event['type']) ? $event['type'] : null;

                if ($event['msg_from'] == 'sender@example.com')
                    return $this->sendResponse();

                if (isset($event['rcpt_meta'])) {
                    if (isset($event['rcpt_meta']['email_log_id']))
                        $log->log_email_id = (int) $event['rcpt_meta']['email_log_id'];

                    if (isset($event['rcpt_meta']['email_provider_id']))
                        $log->email_provider_id = (int) $event['rcpt_meta']['email_provider_id'];

                    if (isset($event['rcpt_meta']['customKey']))
                        return $this->sendResponse();
                }
            }
        }

        $log->created_at = time();
        $log->save();

        $new_status = isset($this->sparkpost_events[$log->event]) ? $this->sparkpost_events[$log->event] : null;

        if (is_null($new_status)) {
            return $this->sendResponse();
        }

        // Changing email status
        $email = LogsEmail::findOne($log->log_email_id);

        if (is_null($email))
            return $this->sendResponse();

        if (!is_null($email->status) && ($email->status == LogsEmail::STATUS_BOUNCE || $email->status == LogsEmail::STATUS_CLICK))
            return $this->sendResponse();

        $email->status = $new_status;

        if (!empty($event)) {
            if ($new_status == LogsEmail::STATUS_BOUNCE || $new_status == LogsEmail::STATUS_DELAY) {
                $email->reason = $event['reason'];
            } else {
                $email->reason = null;
            }

            if ($new_status == LogsEmail::STATUS_CLICK || $new_status == LogsEmail::STATUS_OPEN) {
                $email->city = $event['geo_ip']['city'];
                $email->ip   = $event['ip_address'];
            }
        }

        $email->save();

        return $this->sendResponse();
    }

    public function actionSendGrid()
    {
        $request = \Yii::$app->request;
        $raw_body = $request->rawBody;
        $body = json_decode($raw_body, true);

        // Creating request log
        $log = new LogsEmailRequest();
        $log->method = $request->method;
        $log->url = $request->url;
        $log->body = $raw_body;
        $log->headers = json_encode($request->headers->toArray());

        if (is_array($body)) {
            $event = array_pop($body);

            if (isset($event)) {
                $log->event = isset($event['type']) ? $event['type'] : $event['event'];

                if (isset($event['email_log_id']))
                    $log->log_email_id = (int) $event['email_log_id'];

                if (isset($event['email_provider_id']))
                    $log->email_provider_id = (int) $event['email_provider_id'];
            }
        }

        $log->created_at = time();
        $log->save();

        $new_status = isset($this->sendgrid_events[$log->event]) ? $this->sendgrid_events[$log->event] : null;

        if (is_null($new_status)) {
            return $this->sendResponse();
        }

        // Changing email status
        $email = LogsEmail::findOne($log->log_email_id);

        if (is_null($email))
            return $this->sendResponse();

        if (!is_null($email->status) && ($email->status == LogsEmail::STATUS_BOUNCE || $email->status == LogsEmail::STATUS_CLICK))
            return $this->sendResponse();

        $email->status = $new_status;

        if (!empty($event)) {
            if ($new_status == LogsEmail::STATUS_BOUNCE || $new_status == LogsEmail::STATUS_DELAY) {
                $email->reason = $event['reason'];
            } else {
                $email->reason = null;
            }

            if ($new_status == LogsEmail::STATUS_CLICK || $new_status == LogsEmail::STATUS_OPEN) {
                $email->ip = $event['ip'];
            }
        }

        $email->save();

        return $this->sendResponse();
    }
}