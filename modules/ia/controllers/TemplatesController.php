<?php

namespace app\modules\ia\controllers;

use app\components\base\TemplatableHandlerInterface;
use app\helpers\TemplatableHandlersHelper;
use app\helpers\TwigHelper;
use app\models\EmailProviders;
use app\models\Languages;
use app\models\LogsEmail;
use app\models\Settings;
use app\models\TemplatesHandlers;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\Templates;
use app\models\search\Templates as TemplatesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TemplatesController implements the CRUD actions for Templates model.
 */
class TemplatesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    /**
     * Lists all Templates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TemplatesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Templates model.
     *
     * @param integer $id
     * @param bool    $notification
     *
     * @return mixed
     */
    public function actionView($id, $notification = false)
    {
        $lang =  Yii::$app->session->get('lang',Languages::getDefault());
        $model = Templates::find()->where(['id'=>$id])->translate($lang->iso)->one();

        // $handlers = TemplatableHandlersHelper::getHandlers(\Yii::$container->get('systemEmitter'));
        // $data_handlers = [];
        //
        // foreach ($handlers as $handler)
        //     $data_handlers[$handler] = $handler::getName();

        return $this->render('view', [
            'model' => $model,
            'def_lang' => $lang,
            'notification' => $notification,
            'handler' => TemplatesHandlers::findOne(['template_id' => $id]),
        ]);
    }

    /**
     * Creates a new Templates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $model = new Templates();

        $handlers = TemplatableHandlersHelper::getHandlers(\Yii::$container->get('systemEmitter'));
        $data_handlers = [];

        foreach ($handlers as $handler)
            $data_handlers[$handler] = $handler::getName();

        $post = Yii::$app->request->post();

        if (!empty($post['Templates']['subject'])) {
            foreach ($post['Templates']['subject'] as $lng => $subject)
                $post['Templates']['subject'][$lng] = trim(strip_tags($subject));
        }
        if(!empty($post))
            $post['Templates']['type'] = 'system';
        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'handlers' => $data_handlers,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Updates an existing Templates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionUpdate($id)
    {

        $lang =  Yii::$app->session->get('lang',Languages::getDefault());
        $model = Templates::find()->where(['id'=>$id])->translate($lang->iso)->one();
        $handlers = TemplatableHandlersHelper::getHandlers(\Yii::$container->get('systemEmitter'));
        $data_handlers = [];

        foreach ($handlers as $handler)
            $data_handlers[$handler] = $handler::getName();

        $post = Yii::$app->request->post();

        if (!empty($post['Templates']['subject'])) {
            foreach ($post['Templates']['subject'] as $lng => $subject)
                $post['Templates']['subject'][$lng] = trim(strip_tags($subject));
        }
        if(!empty($post))
            $post['Templates']['type'] = 'system';
        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'handlers' => $data_handlers,
                'def_lang' => $lang,
            ]);
        }
    }

    /**
     * Deletes an existing Templates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Templates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Templates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Templates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetShortcodes($handler, $info = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (empty($handler) || !class_exists($handler) || !(new $handler() instanceof TemplatableHandlerInterface))
            return [];

        return (!empty($info) ? $handler::getShortcodes() : array_keys($handler::getShortcodes()));
    }

    public function actionGetPreview()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $text = Yii::$app->request->post('text', '');
        $handler = Yii::$app->request->post('handler', null);

        if (empty($handler) || !class_exists($handler) || !(new $handler() instanceof TemplatableHandlerInterface))
            return ['data' => $text, 'error' => null];

        $error = null;

        try {
            $response = TwigHelper::render($handler::getShortcodes(), $text, true);
        }
        catch (\Exception $e) {
            $error = $e->getMessage();
            $response = TwigHelper::render($handler::getShortcodes(), $text);
        }

        return ['data' => $response, 'error' => $error];
    }

    public function actionGetTemplateBody($id, $lang = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (is_null($lang))
            $lang = Languages::getDefault();

        $model = $this->findModel($id)->translate($lang);

        return $model->body;
    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSendTestEmail($id)
    {
        $r_lang = $r_email = \Yii::$app->request->post('lang', 'en');
        $model = Templates::findOne($id)->translate($r_lang, true);//\Yii::$app->user->identity->tenant->language->iso,true);
        //$handler = \Yii::$app->request->post('handler', false);
        $r_email = \Yii::$app->request->post('recipient_email', false);

        if (empty($r_email))
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Error! Email is empty!']);

        $provider_id = Settings::getByKey(Settings::PROVIDER_SYSTEM);

        if (empty($provider_id))
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'System mailer not specified!']);

        $provider = EmailProviders::findOne($provider_id);

        if (is_null($provider))
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Mail provider not found!']);

        $mailer = new \app\components\mailers\sparkpost\Mailer($provider->api_key);

        $handler = TemplatesHandlers::findOne(['template_id' => $id]);

        if (is_null($handler))
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Error! Handler is empty!']);

        $handler = $handler->handler;

        if (!class_exists($handler))
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Error! Handler is not exists!']);

        $schema = $handler::getShortcodes();

        $subject = TwigHelper::render($schema, $model->subject);
        $html = TwigHelper::render($schema, $model->body);
        $text = html_entity_decode(strip_tags($html));

        $log = new LogsEmail();
        $log->subject = $model->subject;
        $log->text = $text;
        $log->html = $html;
        $log->type = LogsEmail::TYPE_SYSTEM;
        $log->recipient = $r_email;
        $log->status = LogsEmail::STATUS_SENT;
        $log->template_id = $id;
        $log->save();

        $message = $mailer->compose();
        $message->setTextBody($text)
            ->setHtmlBody($html)
            ->setSubject($subject)
            ->setFrom([Settings::getByKey(Settings::FROM_EMAIL) => Settings::getByKey(Settings::FROM_NAME)])
            ->setTo($r_email)
            ->setMetadata([
                'email_log_id' => $log->id,
                'email_provider_id' => $provider->id
            ])
            ->setReplyTo(Settings::getByKey(Settings::REPLY_TO));

        try {
            $message->send();

            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'success']);
        } catch (\app\components\mailers\MailerProviderException $e) {
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Mailer error! '.$e->getMessage()]);
        } catch (\Exception $e) {
            return $this->redirect(['view', 'id' => $model->id, 'notification' => 'Server error!' . $e->getMessage()]);
        }
    }

//    public function actionGetTranslate($id, $lang)
//    {
//        return parent::actionGetTranslate($id, $lang);
//    }
}
