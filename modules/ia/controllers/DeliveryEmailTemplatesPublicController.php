<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\DeliveryEmailTemplates;
use app\models\search\DeliveryEmailTemplates as DeliveryEmailTemplatesSearch;
use yii\filters\VerbFilter;

/**
 * DeliveryEmailTemplatesController implements the CRUD actions for DeliveryEmailTemplates model.
 */
class DeliveryEmailTemplatesPublicController extends DeliveryEmailTemplatesSystemController
{
    protected $template_type = DeliveryEmailTemplates::PUBLIC_TYPE;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliveryEmailTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveryEmailTemplatesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), DeliveryEmailTemplates::PUBLIC_TYPE,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/delivery-email-templates/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
        ]);
    }
}
