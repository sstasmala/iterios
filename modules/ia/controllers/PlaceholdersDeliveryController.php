<?php

namespace app\modules\ia\controllers;

use app\models\DeliveryTypes;
use app\models\Placeholders;
use app\models\PlaceholdersDeliveryTypes;
use app\models\Translations;
use Yii;
use app\models\PlaceholdersDelivery;
use yii\data\ActiveDataProvider;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PlaceholdersDeliveryController implements the CRUD actions for PlaceholdersDelivery model.
 */
class PlaceholdersDeliveryController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlaceholdersDelivery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PlaceholdersDelivery::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlaceholdersDelivery model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new PlaceholdersDelivery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new PlaceholdersDelivery;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $delivery_types = \Yii::$app->request->post('delivery_types', false);
            $this->deliveryTypesSave(!empty($delivery_types) ? $delivery_types : [], $model->id);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PlaceholdersDelivery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $delivery_types = \Yii::$app->request->post('delivery_types', false);
            $this->deliveryTypesSave(!empty($delivery_types) ? $delivery_types : [], $model->id);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PlaceholdersDelivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param null $search
     *
     * @return array
     */
    public function actionGetPlaceholders($search = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $phs = Placeholders::find()
            ->select(['id', 'short_code', 'module', 'field', 'fake', 'type_placeholder']);

        if (!empty($search)) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $search])
                ->orFilterWhere(['ilike', 'value',  $search])
                ->andWhere('"key" ILIKE :table', [':table'=>"placeholders.short_code.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $phs->andWhere(['in', 'id', array_unique($translate)]);
        }

        $phs = $phs->translate('en')
            ->asArray()->all();

        $data = [];

        foreach ($phs as $ph) {
            $data[] = [
                'id' => $ph['id'],
                'name' => $ph['short_code'],
                'fake' => $ph['fake'],
                'module_class' => $ph['module'],
                'module' => !empty($ph['module']) && \array_key_exists($ph['module'], \Yii::$app->params['placeholders']) ? \Yii::$app->params['placeholders'][$ph['module']] : '',
                'field' => $ph['field'],
                'type' =>  !empty($ph['type_placeholder']) && \array_key_exists($ph['type_placeholder'],Placeholders::TYPE_PLACEHOLDERS) ? Placeholders::TYPE_PLACEHOLDERS[$ph['type_placeholder']] : '',
                'type_ph' => $ph['type_placeholder']
            ];
        }

        return $data;
    }

    /**
     * @param array $delivery_types
     * @param int   $placeholder_delivery_id
     *
     * @throws \yii\web\NotFoundHttpException
     */
    private function deliveryTypesSave(array $delivery_types, int $placeholder_delivery_id)
    {
        $placeholder_delivery = $this->findModel($placeholder_delivery_id);
        PlaceholdersDeliveryTypes::deleteAll(['placeholder_delivery_id' => $placeholder_delivery->id]);

        if (!empty($delivery_types)) {
            foreach ($delivery_types as $type) {
                $model = DeliveryTypes::findOne($type);

                if ($model !== null) {
                    $placeholder_delivery->link('placeholdersDeliveryTypes', $model);
                }
            }
        }
    }

    /**
     * Finds the PlaceholdersDelivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlaceholdersDelivery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlaceholdersDelivery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
