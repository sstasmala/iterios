<?php

namespace app\modules\ia\controllers;

use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\ContactsPhones;
use app\models\Languages;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\modules\ia\controllers\base\BaseController;
use leandrogehlen\querybuilder\Translator;
use Yii;
use app\models\Segments;
use app\models\search\Segments as SegmentsSearch;
use yii\data\SqlDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class SegmentsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new SegmentsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        $langAll = Languages::find(['activated' => true])
            ->asArray()
            ->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'langAll' => $langAll,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }


    public function actionView($id)
    {
        $model = Segments::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (is_null($model))
            throw new HttpException('404', 'Record not found');

        return $this->render('view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    public function actionCreate()
    {
        $model = new Segments();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
            ]);
        }
    }

    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
//            $rules = json_decode($model->rules,true);
//            $builderTranslator = new Translator($rules);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Segments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionLoadFilter()
    {
        $request = Yii::$app->request->post();
        if (isset($request['module'])) {
            return json_encode(Segments::getFilterWithModules($request['module']));
        };

        return 0;

    }

    public function actionResultList()
    {
        $resultRelation = SegmentsResultList::find()
            ->select('segments_result_list.id_contact as contact, contacts_phones.value as phones, contacts_emails.value as email')
            ->leftJoin(SegmentsRelations::tableName(),'segments_result_list.id_relation = segments_relations.id')
            ->leftJoin(ContactsEmails::tableName(), 'segments_result_list.id_contact = contacts_emails.contact_id')
            ->leftJoin(ContactsPhones::tableName(), 'segments_result_list.id_contact = contacts_phones.contact_id')
            ->leftJoin(Contacts::tableName(), 'segments_result_list.id_contact = contacts.id');
//            ->where('segments_result_list.id_relation = '.$model->id);


        $provider = new SqlDataProvider([
            'sql' => $resultRelation->createCommand()->sql,
            'pagination' => [
                'pageSize' => 10,
            ],
//            'sort' => [
//                'defaultOrder' => [
//                    'created_at' => SORT_DESC,
//                ]
//            ],
        ]);
    }
}
