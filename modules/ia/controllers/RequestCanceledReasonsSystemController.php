<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use Yii;
use app\models\RequestCanceledReasons;
use app\models\search\RequestCanceledReasons as RequestCanceledReasonsSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestCanceledReasonsController implements the CRUD actions for RequestCanceledReasons model.
 */
class RequestCanceledReasonsSystemController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestCanceledReasons models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestCanceledReasonsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            RequestCanceledReasons::TYPE_SYSTEM,
            Yii::$app->session->get('lang', Languages::getDefault())->iso);

        return $this->render('@app/modules/ia/views/request-canceled-reasons/index', [
            'dataProvider' => $dataProvider,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault()),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single RequestCanceledReasons model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = RequestCanceledReasons::find()->where(['id' => $id])
            ->translate(Yii::$app->session->get('lang', Languages::getDefault())->iso)->one();

        if (is_null($model))
            throw new HttpException('404','Record not found');

        return $this->render('@app/modules/ia/views/request-canceled-reasons/view', [
            'model' => $model,
            'def_lang' => Yii::$app->session->get('lang', Languages::getDefault())
        ]);
    }

    /**
     * Creates a new RequestCanceledReasons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestCanceledReasons();

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/request-canceled-reasons/create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())
            ]);
        }
    }

    /**
     * Updates an existing RequestCanceledReasons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadWithLang(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/modules/ia/views/request-canceled-reasons/update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestCanceledReasons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RequestCanceledReasons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequestCanceledReasons::findOne($id)) !== null) {
            return $model->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
