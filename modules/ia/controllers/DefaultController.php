<?php

namespace app\modules\ia\controllers;

use app\modules\ia\controllers\base\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `ia` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
//    public function actionIndex()
//    {
//        return $this->render('index');
//    }
}
