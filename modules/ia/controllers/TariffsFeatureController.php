<?php

namespace app\modules\ia\controllers;

use app\models\Languages;
use app\modules\ia\controllers\base\BaseController;
use Yii;
use app\models\TariffsFeature;
use app\models\search\TariffsFeature as TariffsFeatureSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TariffsFeatureController implements the CRUD actions for TariffsFeature model.
 */
class TariffsFeatureController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TariffsFeature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TariffsFeatureSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),
            Yii::$app->session->get('lang',Languages::getDefault())->iso);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
        ]);
    }

    /**
     * Displays a single TariffsFeature model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = TariffsFeature::find()->where(['id'=>intval($id)])
            ->translate(Yii::$app->session->get('lang',Languages::getDefault())->iso)->one();
        if(is_null($model))
            throw new HttpException('404','Record not found');
        return $this->render('view', ['model' => $model,'def_lang' => Yii::$app->session->get('lang',Languages::getDefault())]);
    }

    /**
     * Creates a new TariffsFeature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TariffsFeature;

        $post = Yii::$app->request->post();
        if(isset($post['TariffsFeature']['class']))
        {
            if(null !== $post['TariffsFeature']['class']::COUNTABLE && $post['TariffsFeature']['class']::COUNTABLE)
                $post['TariffsFeature']['countable'] = 1;
            else
                $post['TariffsFeature']['countable'] = 0;
        }
        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Updates an existing TariffsFeature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(is_null($model))
            throw new HttpException('404','Record not found');

        $post = Yii::$app->request->post();
        if(isset($post['TariffsFeature']['class']))
        {
            if(null !== $post['TariffsFeature']['class']::COUNTABLE && $post['TariffsFeature']['class']::COUNTABLE)
                $post['TariffsFeature']['countable'] = '1';
            else
                $post['TariffsFeature']['countable'] = '0';
        }

        if ($model->loadWithLang($post)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'def_lang' => Yii::$app->session->get('lang',Languages::getDefault()),
            ]);
        }
    }

    /**
     * Deletes an existing TariffsFeature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TariffsFeature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TariffsFeature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TariffsFeature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
