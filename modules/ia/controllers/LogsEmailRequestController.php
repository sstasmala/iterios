<?php

namespace app\modules\ia\controllers;

use Yii;
use app\models\LogsEmailRequest;
use app\models\search\LogsEmailRequest as LogsEmailRequestSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogsEmailRequestController implements the CRUD actions for LogsEmailRequest model.
 */
class LogsEmailRequestController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogsEmailRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsEmailRequestSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $method = LogsEmailRequest::find()->select(['method'])->groupBy('method')->asArray()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data_method' => $method
        ]);
    }

    /**
     * Displays a single LogsEmailRequest model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Finds the LogsEmailRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogsEmailRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogsEmailRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
