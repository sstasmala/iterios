<?php

namespace app\modules\ia\controllers;

use app\components\events\SystemUserAddedToTenantEvent;
use app\helpers\RbacHelper;
use app\models\AgentsMapping;
use app\models\Auth;
use app\models\Tenants;
use Yii;
use app\models\User;
use app\models\search\UserAdminView as UserSearch;
use app\modules\ia\controllers\base\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single User model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \HttpException
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new User;

        if ($model->load(Yii::$app->request->post())) {
            $pass = Yii::$app->security->generateRandomString(10);

            if (User::checkAuth0User($model->email))
                throw new \yii\web\HttpException(400, 'User with email - '.$model->email.', already exists in Auth0!');

            $client = Yii::$app->auth0->management_api_client;
            $user_data = $client->users->create([
                'user_id' => '',
                'connection' => 'Username-Password-Authentication',
                'email' => $model->email,
                'username' => strstr($model->email, '@', true) . '_' . time(),
                'password' => $pass,
                'user_metadata' => [
                    'first_name' => $model->first_name,
                    'last_name' => $model->last_name
                ],
                'email_verified' => false,
                'verify_email' => true
            ]);

            $model->setPassword($pass);

            $phone_trim = trim($model->phone);
            $model->phone_clone = preg_replace('/[^0-9]/', '', $phone_trim);

            if ($user_data['user_id']) {
                $transaction = $model->getDb()->beginTransaction();

                if ($model->save()) {

                    $auth = new Auth([
                        'user_id'   => $model->id,
                        'source'    => 'auth0',
                        'source_id' => (string) $user_data['user_id'],
                    ]);

                    if ($auth->save()) {
                        $transaction->commit();

                        if (Yii::$app->request->post('system_admin', false)) {
                                RbacHelper::assignRole('system_admin', $model->id);
                        }

                        return $this->redirect(['view', 'id' => $model->id, 'p' => $pass]);
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id, 'p' => $pass]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $admin_ids = \app\helpers\RbacHelper::getAssignedUsers('system_admin');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $phone_trim = trim($model->phone);
            $model->phone_clone = preg_replace('/[^0-9]/', '', $phone_trim);
            $model->save();
            $client = Yii::$app->auth0->management_api_client;
            $client->users->update($model->auth->source_id, [
                'connection' => 'Username-Password-Authentication',
                'email' => $model->email,
                'email_verified' => false,
                'verify_email' => true
            ]);

            if (Yii::$app->request->post('system_admin', false)) {
                if (!in_array($model->id, $admin_ids))
                    RbacHelper::assignRole('system_admin', $model->id);
            } else {
                RbacHelper::unassignRole('system_admin', $model->id);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'system_admin' => in_array($model->id, $admin_ids)
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $auth0_id = $model->auth->source_id;

        if ($model->delete()) {
            $client = Yii::$app->auth0->management_api_client;
            $client->users->delete($auth0_id);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param $user_id
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \Exception
     */
    public function actionAddToTenant($id, $user_id)
    {
        $mapping = AgentsMapping::findOne(['tenant_id' => $id, 'user_id' => $user_id]);

        if (!is_null($mapping))
            return false;

        $tenant = Tenants::findOne($id);
        $tenant->link('users', User::findOne($user_id));

        $event = new SystemUserAddedToTenantEvent(User::findOne($user_id),$tenant);
        Yii::$container->get('systemEmitter')->trigger($event);

        return true;
    }

    /**
     * @param      $id
     * @param      $user_id
     *
     * @param bool $r
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFromTenant($id, $user_id, $r = false)
    {
        //$tenant = Tenants::findOne($id);

        //if ($tenant->owner_id != $user_id)
            AgentsMapping::findOne(['tenant_id' => $id, 'user_id' => $user_id])->delete();

        return $this->redirect([$r === 'tenant' ? '/ia/tenants/view?id='.$id : 'view?id='.$user_id]);
    }

    /**
     * @param      $user_id
     * @param null $name
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetTenants($user_id, $name = null)
    {
        if (is_null($name))
            $name = '';

        $user_tenants = $this->findModel($user_id)->getTenants()->select('id')->column();

        $query = Tenants::find();
        $query->andWhere(['like', 'name', $name])
            ->orWhere(['like', 'name', mb_strtolower($name, 'UTF-8')])
            ->orWhere(['like', 'name', mb_convert_case(mb_strtolower($name, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')]);
        $query->andWhere(['not in', 'id', $user_tenants]);
        $tenants = $query->all();

        $data = [];

        foreach ($tenants as $tenant)
        {
            $data[] = [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'language' => $tenant->language->name,
                'owner' => $tenant->owner->first_name . ' ' . $tenant->owner->last_name
            ];
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionBlock($id)
    {
        $user = $this->findModel($id);

        if ($user->id !== \Yii::$app->user->id)
            $user->blockUser($user->status === User::STATUS_ACTIVE);

        return $this->redirect(['view', 'id' => $id]);
    }


    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionAssignUserRole($id)
    {
        $user = $this->findModel($id);

        RbacHelper::assignRole('system_admin', $user->id);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUnassignUserRole($id)
    {
        $user = $this->findModel($id);

        RbacHelper::unassignRole('system_admin', $user->id);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
