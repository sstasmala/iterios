<?php
/**
 * SettingsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\helpers\RbacHelper;
use app\models\Agencies;
use app\models\AgenciesEmails;
use app\models\AgenciesMessengers;
use app\models\AgenciesPhones;
use app\models\AgenciesSocials;
use app\models\AgencyPictureUploadForm;
use app\models\Requisites;
use app\models\RequisitesSets;
use app\models\RequisitesTenants;
use app\models\RequisiteUploadForm;
use app\models\Tenants;
use app\modules\ita\controllers\base\BaseController;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use app\utilities\contacts\ContactsUtility;

class SettingsController extends BaseController
{
    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        if(!$parent) return $parent;
        if(!RbacHelper::can('settings.read.access') && !RbacHelper::owner())
            throw new HttpException(403);

        return $parent;
    }

    public function actionAccount()
    {
        /* Set params */
        $tenant = Tenants::findOne(['id'=>\Yii::$app->user->identity->tenant->id]);
        $date_formats = [
            'd.m.Y'=>'DD.MM.YYYY',
            'Y.m.d'=>'YYYY.MM.DD',
            'm.d.Y'=>'MM.DD.YYYY',
            'Y.d.m'=>'YYYY.DD.MM',
        ];
        $contacts_types = ContactsUtility::prepareTypes(\Yii::$app->user->identity->tenant->language->iso);
        $allCurrency = array_column(MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso), 'iso_code', 'id');

        $agency = Agencies::findOne(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        $agency_phones = AgenciesPhones::findAll(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        $agency_messengers = AgenciesMessengers::findAll(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        $agency_emails = AgenciesEmails::findAll(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        $agency_socials = AgenciesSocials::findAll(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        /* Render view */
        return $this->render('account',compact('tenant','date_formats', 'contacts_types', 'allCurrency', 'agency', 'agency_phones', 'agency_messengers', 'agency_emails', 'agency_socials'));
    }
    
    public function actionGetLanguage($name=null)
    {
        if(is_null($name))
            $name = '';

        $query = new Query();
        $query->select([])->from('languages')
            ->andWhere(['like', 'name',$name])->orWhere(['like', 'original_name', $name])
            ->orWhere(['like', 'name', strtolower($name)])->orWhere(['like', 'original_name', strtolower($name)])
            ->orWhere(['like', 'name', ucfirst(strtolower($name))])->orWhere(['like', 'original_name', ucfirst(strtolower($name))])
            ->orWhere(['like', 'iso', $name])
            ->orWhere(['like', 'iso', strtoupper($name)])
            ->orWhere(['like', 'iso', ucfirst(strtoupper($name))])
            ->andWhere(['activated' => true]);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $query->all();
        return $result;
    }

    public function actionUpdateBaseSettings()
    {
        $data = \Yii::$app->request->post();
        if(is_null($data))
            return;
        $tenant = Tenants::findOne(['id'=>\Yii::$app->user->identity->tenant->id]);
        if(isset($data['name']))
            $tenant->name = $data['name'];
        if(isset($data['country']))
            $tenant->country = $data['country'];
        if(isset($data['lang']))
        {
            $tenant->language_id = $data['lang'];
        }
        if(isset($data['timezone']))
            $tenant->timezone = $data['timezone'];
        if(isset($data['date_format']))
            $tenant->date_format = $data['date_format'];
        if(isset($data['time_format']))
            $tenant->time_format = $data['time_format'];
        if(isset($data['week_start']))
            $tenant->week_start = $data['week_start'];
        if(isset($data['reminders_email']))
            $tenant->reminders_email = $data['reminders_email'];
        if(isset($data['reminders_from_who']))
            $tenant->reminders_from_who = $data['reminders_from_who'];
        if(isset($data['curr']))
            $tenant->usage_currency = json_encode($data['curr']);
        if(isset($data['main_currency']))
            $tenant->main_currency = $data['main_currency'];
        if(isset($data['currency_display']))
            $tenant->currency_display = $data['currency_display'];

        $tenant->system_tags = isset($data['system_tags']) ? $data['system_tags'] : 0;

        $agency = Agencies::findOne(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        if(!isset($agency)){
            $agency = new Agencies(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);
            $agency->created_at = time();
            $agency->created_by = \Yii::$app->user->id;
            $agency->updated_at = time();
            $agency->updated_by = \Yii::$app->user->id;
        }

        if(isset($data['agency']['site']))
            $agency->site = $data['agency']['site'];
        if(isset($data['agency']['city']))
            $agency->city = $data['agency']['city'];
        if(isset($data['agency']['street']))
            $agency->street = $data['agency']['street'];
        if(isset($data['agency']['house']))
            $agency->house = $data['agency']['house'];
        if(isset($data['agency']['description']))
            $agency->address_description = $data['agency']['description'];
        $agency->updated_at = time();
        $agency->updated_by = \Yii::$app->user->id;

        if(isset($data['agency'])) {

            /*PHONES*/
            if(isset($data['agency']['phones'])) {
                AgenciesPhones::deleteAll(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
//                foreach($data['agency']['phones'] as $phone)
//                    if(!empty($phone['id']))
//                        $phones_ids[] = $phone['id'];
//                $agenciesPhones = AgenciesPhones::find()->where(['in', 'id', $phones_ids])->all();
                $agenciesPhonesData = [];
//                if (!empty($agenciesPhones))
//                    foreach($agenciesPhones as $phone)
//                        $agenciesPhonesData[$phone->id] = $phone;
                foreach ($data['agency']['phones'] as $phone_mass) {
                    if(!isset($agenciesPhonesData[$phone_mass['id']])) {
                        $agency_phone = new AgenciesPhones();
                        $agency_phone->created_at = time();
                        $agency_phone->updated_at = time();
                        $agency_phone->created_by = \Yii::$app->user->id;
                        $agency_phone->updated_by = \Yii::$app->user->id;
                        $agency_phone->tenant_id = \Yii::$app->user->identity->tenant->id;
                    } else {
                        $agency_phone = $agenciesPhonesData[$phone_mass['id']];
                    }
                    $agency_phone->value = $phone_mass['phone'];
                    $agency_phone->updated_by = \Yii::$app->user->id;
                    $agency_phone->updated_at = time();
//                    $agency_phone->type_id = $phone_mass['type_id'];
                    $agency_phone->save();
                }
            }

            /*EMAILS*/
            if(isset($data['agency']['emails'])) {
                AgenciesEmails::deleteAll(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
                foreach($data['agency']['emails'] as $mail) {
                    $emails_ids[] = (int)$mail['id'];
                }
                $agenciesEmails = AgenciesEmails::find()->where(['in', 'id', $emails_ids])->all();
                $agenciesEmailsData = [];
                if (!empty($agenciesEmails))
                    foreach($agenciesEmails as $mail)
                        $agenciesEmailsData[$mail->id] = $mail;
                foreach ($data['agency']['emails'] as $email) {
                    if(!isset($agenciesEmailsData[$email['id']])) {
                        $agency_email = new AgenciesEmails();
                        $agency_email->created_at = time();
                        $agency_email->updated_at = time();
                        $agency_email->created_by = \Yii::$app->user->id;
                        $agency_email->updated_by = \Yii::$app->user->id;
                        $agency_email->tenant_id = \Yii::$app->user->identity->tenant->id;
                    } else {
                        $agency_email = $agenciesEmailsData[$email['id']];
                    }
                    $agency_email->value = $email['value'];
                    $agency_email->updated_by = \Yii::$app->user->id;
                    $agency_email->updated_at = time();
//                    $agency_email->type_id = $email['type_id'];
                    $agency_email->save();
                }
            }

            /*MESSENGERS*/
            if(isset($data['agency']['messengers'])) {
                AgenciesMessengers::deleteAll(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
                foreach($data['agency']['messengers'] as $mess) {
                    $messengers_ids[] = (int)$mess['id'];
                }
                $agenciesMessengers = AgenciesMessengers::find()->where(['in', 'id', $messengers_ids])->all();
                $agenciesMessengersData = [];
                if (!empty($agenciesMessengers))
                    foreach($agenciesMessengers as $mess)
                        $agenciesMessengersData[$mess->id] = $mess;
                foreach ($data['agency']['messengers'] as $messenger) {
                    if(!isset($agenciesMessengersData[$messenger['id']])) {
                        $agency_messenger = new AgenciesMessengers();
                        $agency_messenger->created_at = time();
                        $agency_messenger->updated_at = time();
                        $agency_messenger->created_by = \Yii::$app->user->id;
                        $agency_messenger->updated_by = \Yii::$app->user->id;
                        $agency_messenger->tenant_id = \Yii::$app->user->identity->tenant->id;
                    } else {
                        $agency_messenger = $agenciesMessengersData[$messenger['id']];
                    }
                    $agency_messenger->value = $messenger['value'];
                    $agency_messenger->updated_by = \Yii::$app->user->id;
                    $agency_messenger->updated_at = time();
                    $agency_messenger->type_id = $messenger['type_id'];
                    $agency_messenger->save();
                }
            }

            /*SOCIALS*/
            if(isset($data['agency']['socials'])) {
                AgenciesSocials::deleteAll(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
                foreach($data['agency']['socials'] as $soc) {
                    $socials_ids[] = (int)$soc['id'];
                }
                $AgenciesSocials = AgenciesSocials::find()->where(['in', 'id', $socials_ids])->all();
                $AgenciesSocialsData = [];
                if (!empty($AgenciesSocials))
                    foreach($AgenciesSocials as $soc)
                        $AgenciesSocialsData[$soc->id] = $soc;
                foreach ($data['agency']['socials'] as $social) {
                    if(!isset($AgenciesSocialsData[$social['id']])) {
                        $agency_social = new AgenciesSocials();
                        $agency_social->created_at = time();
                        $agency_social->updated_at = time();
                        $agency_social->created_by = \Yii::$app->user->id;
                        $agency_social->updated_by = \Yii::$app->user->id;
                        $agency_social->tenant_id = \Yii::$app->user->identity->tenant->id;
                    } else {
                        $agency_social = $AgenciesSocialsData[$social['id']];
                    }
                    $agency_social->value = $social['value'];
                    $agency_social->updated_by = \Yii::$app->user->id;
                    $agency_social->updated_at = time();
                    $agency_social->type_id = $social['type_id'];
                    $agency_social->save();
                }
            }
        }
        $agency->save();
        $tenant->save();
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionChangeAgencyPicture()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();
        $model = new AgencyPictureUploadForm();

        if (\Yii::$app->request->isPost) {
            if(empty($post['action'])){

            $model->agency_picture = UploadedFile::getInstanceByName('agency_picture');
                if ($logo = $model->upload()) {
                    // file is uploaded successfully
                    return ['photo' => $logo];
                }
            } else {
                $model = Agencies::findOne(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
                FileHelper::unlink($model->logo);
                $model->logo = null;
                $model->save();
                return ['photo' => $model->logo];
            }
        }

        return ['error' => true];
    }

    public function actionGetFields()
    {
        $requisites = RequisitesTenants::find()
            ->select(["requisites_tenants.*","requisites.group_position", "requisites.requisites_field_id", "requisites.requisites_group_id"])
            ->leftJoin(Requisites::tableName(), "requisites_tenants.requisite_id = requisites.id")
            ->orderBy(["requisites.group_position" => SORT_ASC, "requisites.requisites_field_id" => SORT_ASC])
            ->where(["requisites_tenants.tenant_id" => \Yii::$app->user->identity->tenant->id])
            ->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return self::formatRequisite($requisites);
    }

    public function actionGetGeneratedFields()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return self::getGeneratedFields();
    }

    public function actionGetFieldsUpdateRequisites()
    {
        $requisites = RequisitesTenants::find()
            ->where(['set_id' => $_GET['set_id']])
            ->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return self::formatRequisite($requisites);
    }

    public function actionUpdateRequisite()
    {
        $requisites = RequisitesTenants::find()
            ->where(['set_id' => $_GET['set_id']])
            ->all();
        foreach ($requisites as $requisite)
        {
            if (isset($_GET['Requisites'][$requisite->requisite_id]) && is_array($_GET['Requisites'][$requisite->requisite_id])) {
                $requisite->value = json_encode($_GET['Requisites'][$requisite->requisite_id], JSON_UNESCAPED_UNICODE);
            } else {
                if (isset($_GET['Requisites'][$requisite->requisite_id]))
                    if($requisite->requisite->requisitesField->type==7)
                        $requisite->value = DateTimeHelper::convertDateToTimestamp($_GET['Requisites'][$requisite->requisite_id]).'';
                    else
                        $requisite->value = $_GET['Requisites'][$requisite->requisite_id];
                else
                    $requisite->value = "";
            }
            $requisite->save();
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return self::formatRequisite($requisites);
    }

    public function actionCreateRequisites()
    {
        $tenantId = \Yii::$app->user->identity->tenant->id;
        $set = new RequisitesSets();
//        $set->name = $_GET['RequisitesSet'];
        $set->tenant_id = $tenantId;
        $set->save();
        $setId = $set->id;
        $arrRequisites = [];
        foreach ($_GET['Requisites'] as $requisiteId => $value) {
            $field = Requisites::findOne(['id'=>$requisiteId])->requisitesField;
            $requisiteTenant = new RequisitesTenants();
            $requisiteTenant->requisite_id = $requisiteId;
            $requisiteTenant->set_id = $setId;
            if($field->type == 7 && !empty($value)){
                $requisiteTenant->value = DateTimeHelper::convertDateToTimestamp($value);
            }
            else
            if (is_array($value)) {
                $requisiteTenant->value = json_encode($value);
            } else {
                $requisiteTenant->value = $value;
            }
            $requisiteTenant->tenant_id = $tenantId;
            $requisiteTenant->save();
            $arrRequisites[$requisiteId] = $requisiteTenant;
        }
        $arrRequisites = self::formatRequisite($arrRequisites);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $arrRequisites;
    }

    private function formatRequisite($requisites)
    {
        $formatRequisites = [];
        foreach ($requisites as $tenRequisite) {
                $group = $tenRequisite->requisite->requisitesGroup;
                $group = $group->translate(\Yii::$app->user->identity->tenant->language->iso);
                $groupName = $group->name;
                $fieldPosition = $tenRequisite->requisite->field_position;
                $field = $tenRequisite->requisite->requisitesField;
                $field = $field->translate(\Yii::$app->user->identity->tenant->language->iso);
//                $setName = $tenRequisite->set->name;
                $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['id']
                    = $tenRequisite->requisite->id;
                $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['set_id']
                    = $tenRequisite->set->id;
                $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['type']
                    = $field->type;
                $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['name']
                    = $field->name;
                if($field->type == 7 && !is_null($tenRequisite->value) && !empty($tenRequisite->value)) {
                    $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['value']
                        = DateTimeHelper::convertTimestampToDate(intval($tenRequisite->value));
                }
                else{
                    $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['value']
                        = $tenRequisite->value;
                }
                $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['variants']
                    = $tenRequisite->requisite->requisitesField->variants;
            $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['in_header']
                = $tenRequisite->requisite->in_header;
            $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['placeholder']
                = $field->field_placeholder;
            $formatRequisites[$tenRequisite->set->id][$groupName][$fieldPosition]['description']
                = $field->field_description;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $formatRequisites;
    }

    private function getGeneratedFields()
    {
        $requisites = Requisites::find()->where(['country_id' => \Yii::$app->user->identity->tenant->country])
            ->orderBy(['group_position'=>'ASC','field_position'=>'ASC'])->all();
        $newRequisites = [];
        $i = 0;
        foreach ($requisites as $requisite) {
            $group = $requisite->requisitesGroup;
            $group = $group->translate(\Yii::$app->user->identity->tenant->language->iso);
            $groupName = $group->name;
            $field = $requisite->requisitesField;
            $field = $field->translate(\Yii::$app->user->identity->tenant->language->iso);
            $newRequisites[$group->id]['name'] = $groupName;
            $newRequisites[$group->id]['count'] = $i++;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $requisite->id;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $field->type;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $field->name;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $field->variants;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $field->field_placeholder;
            $newRequisites[$group->id]['fields'][$requisite->field_position][] = $field->field_description;
        }
        return $newRequisites;
    }

    public function actionRemoveSet($id)
    {
        $model = RequisitesSets::findOne(['id'=>intval($id)]);
        if(is_null($model))
            throw new HttpException(404);

        $model->delete();

        return $id;
    }

    public function actionUploadImage()
    {
        if(\Yii::$app->request->isPost)
        {
            $model = new RequisiteUploadForm();
            $model->requisite_picture = UploadedFile::getInstanceByName('requisite_picture');
            return $model->upload();
        }
        return null;
    }

    public function actionRemoveFile()
    {
        if(\Yii::$app->request->isPost)
        {
            $path = \Yii::$app->request->post('file',null);
            if(!is_null($path)) {
                $path  = \Yii::$app->basePath.'/'.$path;
                if(file_exists($path))
                    unlink($path);
            }
        }
    }
}