<?php
/**
 * RemindersController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\RbacHelper;
use app\models\BaseNotifications;
use app\models\TenantNotifications;
use app\modules\ita\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\Response;

class RemindersController extends BaseController
{
    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        if(!$parent) return $parent;
        if(!RbacHelper::can('reminders.read.access') && !RbacHelper::owner())
            throw new HttpException(403);

        return $parent;
    }

    /**
     * @return string
     */
    public function actionAgent()
    {
        return $this->render('agent');
    }

    /**
     * @return string
     */
    public function actionTourist()
    {
        return $this->render('tourist');
    }

    /**
     * @return array
     */
    public function actionGetReminder()
    {
        if (\Yii::$app->request->isAjax) {
            $reminder_id = \Yii::$app->request->post('reminder_id');
            // Get reminder by id (text_data)
            $reminder = [
                'id' => $reminder_id,
                'name' => 'Имя напоминалки',
                'notification_activate_mode' => 2,
                'channels' => [
                    [
                        'name' => 'Email',
                        'checked' => false
                    ],
                    [
                        'name' => 'Notice',
                        'checked' => true
                    ],
                    [
                        'name' => 'Task',
                        'checked' => false
                    ]
                ]
            ];
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $reminder;
        }
    }
}