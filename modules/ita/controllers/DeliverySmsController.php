<?php

namespace app\modules\ita\controllers;

use app\components\sms_provider\SmsClubXML;
use app\models\AgentsMapping;
use app\models\Contacts;
use app\models\ContactsPhones;
use app\models\Providers;
use app\models\ProvidersCredentials;
use app\models\Segments;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\models\SendSms;
use app\models\search\SendSms as SendSmsSearch;
use app\models\SendSmsStatus;
use app\models\Tenants;
use app\models\Translations;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class DeliverySmsController extends BaseController
{
    public function actionIndex()
    {
        /* Set params */
        $search_data = \Yii::$app->request->get('search');

        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];

        $filterData = array(
            'providers' => Providers::find()->all(),
            'statuses' => SendSms::STATUS,
            'users' => AgentsMapping::find()->joinWith('user')->where(['tenant_id' => Yii::$app->user->identity->tenant->id])->all(),
        );


        return $this->render('index', [
            'filterData' => $filterData,
        ]);
    }

    public function actionCreate()
    {
        $model = new SendSms();
        $lang = Yii::$app->user->identity->tenant->language->iso;
            $providers = Providers::find()->joinWith('credential')
                ->where(['providers_credentials.tenant_id' => Yii::$app->user->identity->tenant->id])
                ->all();

            $segmentsRelations = SegmentsRelations::find()
                ->where(['id_tenant' => Yii::$app->user->identity->tenant->id])
                    ->indexBy('id_segment')
                ->asArray()
                ->all();

            $segments = Segments::find()
                ->where(['in', 'id' , array_column($segmentsRelations, 'id_segment')])
                ->translate($lang)
                ->all();

            if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                $model->status = SendSms::STATUS_READY;
                $model->tenant_id = Yii::$app->user->identity->tenant->id;
                $model->save();

                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ['model_id' => $model->id];
            }

        return $this->render('create', [
                'segRelations' => $segmentsRelations,
                'providers' => $providers,
                'segments' => $segments,
            ]
        );
    }

    public function actionGetIndex()
    {
        $search = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');
        $search_data = [];

        if (isset($search)) {
            parse_str($search, $search_data);
        }

        $searchModel = new SendSmsSearch();
        $query = $searchModel->search($search_data);

        $meta['total'] = $query->count();
        if (isset($meta['perpage']) && isset($meta['page'])) {
            $query->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1))->orderBy('created_at');
        }

        $resultData = [];
        foreach ($query->each(30) as $key => $item) {
            $resultData[$key] = $item->toArray();
            $resultData[$key]['createdWithTimeZone'] = $item->created_at_formatted;
            $resultData[$key]['photo'] = $item->created->photo;
            $resultData[$key]['fio'] = $item->created->first_name . ' ' . $item->created->last_name;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $resultData, 'meta' => $meta];
    }

    public function actionDelIndex()
    {
        $ids = \Yii::$app->request->post('ids');

        $smsAll = SendSms::find()->where(['in', 'id', $ids])->all();
        foreach ($smsAll as $sms) {
            $sms->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return 1;
    }

    public function actionView($id)
    {
        $model = SendSms::find()
        ->where(['send_sms.id' => $id])
        ->joinWith('provider')
        ->one();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionGetView()
    {
        $send_sms_id = \Yii::$app->request->get('send_sms_id');
        $meta = \Yii::$app->request->get('pagination');
        $lang = Yii::$app->user->identity->tenant->language->id;

        $deliveredSms = SendSmsStatus::find()
            ->select([
                'CONCAT(contacts.last_name, \' \', contacts.first_name) as fio',
                'contacts_phones.value as phones',
                'send_sms_status.segments',
                'send_sms_status.status',
                'send_sms_status.data',
                'send_sms.segment_copy',
            ])
            ->leftJoin(ContactsPhones::tableName(), "send_sms_status.contact_phone_id = contacts_phones.id")
            ->leftJoin(Contacts::tableName(), "contacts_phones.contact_id = contacts.id")
            ->leftJoin(SendSms::tableName(), "send_sms_status.send_sms_id = send_sms.id")
            ->where(['send_sms_id' => $send_sms_id]);

        $meta['total'] = $deliveredSms->count();
        $deliveredSms->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1))->orderBy('contact_phone_id, send_sms_id DESC');
        $deliveredSms = $deliveredSms->asArray()->all();

        $allSegment = Segments::find()->indexBy('id')->all();
        foreach ($deliveredSms as $key => $sms) {
            $status = array_flip(SendSmsStatus::DELIVERY_STATUS);
            $deliveredSms[$key]['status'] = $status[$sms['status']];
            $segments = [];
            foreach (explode(";", $sms['segments']) as $item) {
                $segments[] = $allSegment[$item]->name;
            }

            $deliveredSms[$key]['segments'] = implode(',', $segments);
            $array_segment_list = json_decode($deliveredSms[$key]['segment_copy'],true);
            $array_segment_sort = [];
            $final_array = [];
            foreach($array_segment_list as $tes) {
                $array_segment_sort[$tes['id_segment']] = $tes['name'];
            }
            foreach(explode(';', $sms['segments']) as $segment_id){
                $final_array[] = $array_segment_sort[$segment_id];
            }
            $deliveredSms[$key]['segment_list_copy'] = $final_array;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $deliveredSms, 'meta' => $meta];
    }

    /**
     * @param $provider_id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetAlphaName($provider_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (!$provider_id)
            return [];
        
        $provider = ProvidersCredentials::find()
            ->where(['provider_id' => $provider_id])
            ->andWhere(['tenant_id' => Yii::$app->user->identity->tenant->id])
            ->one();

        if ($provider === null)
            throw new NotFoundHttpException('Provider not found!');

        return ['data' => $provider->getAlphaName()];
    }

    /**
     * @param $id
     *
     * @return \app\models\SendSms
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = SendSms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
