<?php

namespace app\modules\ita\controllers;

use app\helpers\CompaniesHelper;
use app\helpers\LogHelper;
use app\helpers\MCHelper;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\Companies;
use app\models\CompaniesAddresses;
use app\models\CompaniesNotes;
use app\models\Contacts;
use app\models\ContactsTags;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use app\helpers\RbacHelper;
use app\utilities\companies\CompaniesUtility;
use app\models\TasksTypes;
use app\utilities\contacts\ContactsUtility;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CompaniesController extends BaseController
{
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        switch ($action->id)
        {
            case 'index':if(!RbacHelper::can('companies.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'view':if(!RbacHelper::can('companies.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'create':if(!RbacHelper::can('companies.create.access') && !RbacHelper::owner())throw new HttpException(403);break;
            case 'delete':if(!RbacHelper::can('companies.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
            case 'bulk-delete':if(!RbacHelper::can('companies.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
        }
        return $parent;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {

        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        \Yii::$app->view->params['subheader'] = [
            'show' => false
        ];

        return $this->render('index');
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $companies = Companies::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        $companies->joinWith('companiesEmails')->joinWith('companiesPhones')
            ->with('companyAddress', 'employees', 'companyKindActivity',
                'companiesMessengers', 'companiesSites', 'companiesSocials', 'companiesTags', 'companyStatus',
                'companyType', 'responsible');

        if (!empty($search_data['name']) || !empty($search_data['search_text']))
            $companies->andWhere(['ilike', 'companies.name',
                !empty($search_data['name']) ? $search_data['name'] : $search_data['search_text']
            ]);

        if (!empty($search_data['contact'])) {
            $contacts = Contacts::find()
                ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->andWhere('contacts.company_id IS NOT NULL')
                ->joinWith(['contactsEmails', 'contactsPhones', 'contactsPassports']);

            $search_contact = false;

            if (!empty($search_data['contact']['passport'])) {
                $contacts->andWhere(['ilike', 'contacts_passports.serial', $search_data['contact']['passport']]);

                $search_contact = true;
            }

            if (!empty($search_data['contact']['first_name'])) {
                $contacts->andWhere(['ilike', 'contacts.first_name', $search_data['contact']['first_name']]);

                $search_contact = true;
            }

            if (!empty($search_data['contact']['last_name'])) {
                $contacts->andWhere(['ilike', 'contacts.last_name', $search_data['contact']['last_name']]);

                $search_contact = true;
            }

            if (!empty($search_data['contact']['email'])) {
                $contacts->andWhere(['ilike', 'contacts_emails.value', $search_data['contact']['email']]);

                $search_contact = true;
            }

            if (!empty($search_data['contact']['phone'])) {
                $contacts->andWhere(['ilike', 'contacts_phones.value', $search_data['contact']['phone']])
                    ->orWhere(['ilike', 'contacts_phones.value', str_replace(['(', ')', '+', '-'], '', $search_data['contact']['phone'])]);

                $search_contact = true;
            }



            if (!empty($search_data['contact']['tags'])) {
                $tags = ContactsTags::find()->select(['contact_id', 'COUNT(tag_id) as tag_count'])
                    ->where(['in', 'tag_id', $search_data['contact']['tags']])
                    ->having(['COUNT(tag_id)' => count($search_data['contact']['tags'])])
                    ->groupBy('contact_id')
                    ->asArray()->all();

                $contacts_ids = array_column($tags, 'contact_id');

                if (!empty($contacts_ids)) {
                    $contacts->andWhere(['in', 'contacts.id', $contacts_ids]);

                    $search_contact = true;
                }
            }

            $contacts = $contacts->asArray()->all();
            $company_id = array_unique(array_column($contacts, 'company_id'));

            if (!empty($company_id) && $search_contact)
                $companies->andWhere(['in', 'companies.id', $company_id]);
        }

        if (!empty($search_data['filter']) && $search_data['filter'] === 'only_my' && empty($search_data['responsible'])) {
            $companies->andWhere(['companies.responsible_id' => \Yii::$app->user->id]);
        }

        if (!empty($search_data['email']))
            $companies->andWhere(['ilike', 'companies_emails.value', $search_data['email']]);

        if (!empty($search_data['phone']))
            $companies->andWhere(['ilike', 'companies_phones.value', $search_data['phone']])
                ->orWhere(['ilike', 'companies_phones.value', str_replace(['(', ')', '+', '-'], '', $search_data['phone'])]);

        if (!empty($search_data['status']))
            $companies->andWhere(['companies.company_status_id' => $search_data['status']]);

        if (!empty($search_data['type']))
            $companies->andWhere(['companies.company_type_id' => $search_data['type']]);

        if (!empty($search_data['activities_type']))
            $companies->andWhere(['companies.company_kind_activity_id' => $search_data['activities_type']]);

        if (!empty($search_data['responsible']))
            $companies->andWhere(['companies.responsible_id' => $search_data['responsible']]);

        $meta['total'] = $companies->count('DISTINCT companies.id');

        $companies->orderBy('created_at DESC')
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $companies = $companies->asArray()->all();

        $new_companies = [];

        foreach ($companies as $company) {
            $data = CompaniesUtility::prepareViewArray($company);

            $new_companies[] = $data;
        }

        return ['meta' => $meta, 'data' => $new_companies];
    }

    /**
     * @return array|mixed
     * @throws \yii\web\HttpException
     */
    public function actionSetParamAjax()
    {
        $lang = \Yii::$app->user->identity->tenant->language->iso;
        if (\Yii::$app->request->isAjax) {
            //PROCCESS
            $response = \Yii::$app->request->post();
            $company = Companies::find()->where(['id'=>$response['id']])
                ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->one();

            if(null === $company)
                throw new HttpException(404);

            if (RbacHelper::can('companies.update.only_own')) {
                if($company->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
            }

            if (RbacHelper::can('companies.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
                if(!empty($roles)) {
                    $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                    if(array_search($company->responsible_id, $ids)===false)
                        throw new HttpException(403);
                }
                else
                    if($company->responsible_id != \Yii::$app->user->id)
                        throw new HttpException(403);
            }


            switch ($response['property']){
                case 'type':
                    //logic
                    $company->company_type_id = (int) $response['value'];
                    $company->save();
                    if($company->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $company->companyType->value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'kind_activity':
                    //logic
                    $company->company_kind_activity_id = (int) $response['value'];
                    $company->save();
                    if($company->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $company->companyKindActivity->value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'status':
                    //logic
                    $company->company_status_id = (int) $response['value'];
                    $company->save();
                    if($company->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $company->companyStatus->value;
                    $response['color'] = !empty($company->companyStatus->color_type) ? $company->companyStatus->color_type : 'default';
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'company_address':
                    $address = $company->companyAddress;
                    if(is_null($address))
                        $address = new CompaniesAddresses();
                    //logic
                    /* Generate new adress row */
                    $arderss_row = '';
                    if (!empty($response['company_address'])) {
                        if (isset($response['company_address']['country']) && !empty($response['company_address']['country'])) {
                            $address->country = $response['company_address']['country'];
                            $arderss_row .= MCHelper::getCountryById($response['company_address']['country'],$lang)[0]['title'] . ', ';
                        }
                        if (isset($response['company_address']['region']) && !empty($response['company_address']['region'])) {
                            $address->region = $response['company_address']['region'];
                            $arderss_row .= $response['company_address']['region'] . ', ';
                        }
                        if (isset($response['company_address']['city']) && !empty($response['company_address']['city'])) {
                            $address->city = $response['company_address']['city'];
                            $arderss_row .= $response['company_address']['city'] . ', ';
                        }
                        if (isset($response['company_address']['street']) && !empty($response['company_address']['street'])) {
                            $address->street = $response['company_address']['street'];
                            $arderss_row .= $response['company_address']['street'];
                            if (isset($response['company_address']['house']) && !empty($response['company_address']['house'])) {
                                $address->house = $response['company_address']['house'];
                                $arderss_row .= ' ' . $response['company_address']['house'];
                                if (isset($response['company_address']['flat']) && !empty($response['company_address']['flat'])) {
                                    $address->flat = $response['company_address']['flat'];
                                    $arderss_row .= ', ' . $response['company_address']['flat'];
                                }
                            }
                            $arderss_row .= ', ';
                        }
                        if (isset($response['company_address']['postcode']) && !empty($response['company_address']['postcode'])) {
                            $address->postcode = $response['company_address']['postcode'];
                            $arderss_row .= $response['company_address']['postcode'] . ', ';
                        }
                        $arderss_row = substr(trim($arderss_row), 0, -1);
                        $logItemId = $address->save();
                        if ($logItemId !== true) {
                            $company->company_address_id = $address->id;
                            $company->touch('updated_at');
                            $company->force_update = true;
                            $logMainItemId = $company->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }

                        if($company->hasErrors())
                        {
                            $response['result'] = 'error';
                            $response['message'] = TranslationHelper::getTranslation('error',$lang);
                            break;
                        }
                    } else {
                        $arderss_row = 'Not set';
                    }
                    /* Set response */
                    $response['new_value'] = $arderss_row;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                default :
                    $response['result'] = 'error';
                    $response['new_value'] = $response['value'];
                    $response['message'] = TranslationHelper::getTranslation('ajax_error_message_undefined_object_property', $lang, 'Undefined object property') . ' ' . $response['property'];
                    break;
            }
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    /**
     * @return array|bool
     */
    public function actionCreate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $company_data = \Yii::$app->request->post();
        $company_id = CompaniesHelper::create($company_data);

        return !empty($company_id) ? ['id' => $company_id] : false;
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\BadRequestHttpException
     * @throws \Throwable
     */
    public function actionView($id)
    {
        if (\Yii::$app->request->post()) {
            CompaniesUtility::saveCompanyData($id);

            return $this->redirect('view?id='.$id);
        }

        $company = Companies::find()
            ->where(['companies.id' => $id])->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
        $company->joinWith('companiesEmails')->joinWith('companiesPhones')
            ->with('companyAddress', 'companyContactSource', 'employees', 'companyKindActivity',
                'companiesMessengers', 'companiesSites', 'companiesSocials', 'companiesTags', 'companyStatus',
                'companyType', 'responsible');
        $company = $company->asArray()->one();
        $company = CompaniesUtility::prepareViewArray($company);

        if (null === $company)
            throw new BadRequestHttpException();

        $company_contacts = Contacts::find()
            ->where(['company_id' => $id])->asArray()->all();
        
        $lang = \Yii::$app->user->identity->tenant->language->iso;
        $contacts_types = ContactsUtility::prepareTypes($lang);
        $all_task_types = TasksTypes::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->orderBy('default DESC')
            ->asArray()
            ->all();

        $timeline = [
            'all' => [
                [
                    'time' => '14:52',
                    'title' => 'Alex Johnson <span class="m--font-normal">создал</span> контактах:',
                    'content' =>    'имя - Name<br>
                                    фамилия - Last<br>
                                    тип - new<br>
                                    ответственный - Alex Johnson',
                    'icon' => [
                        'class' => 'la la-eye',
                        'color' => 'warning'
                    ],
                    'buttons' => [
                        'remove' => false,
                        'edit' => false,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_13.jpg',
                        'link' => '#',
                        'title' => 'Alex Johnson'
                    ]
                ],
                [
                    'time' => 'Вчера',
                    'title' => 'Max Webster <span class="m--font-normal">добавил</span> заметку в <a href="#" class="m-link">Запросе #214</a>',
                    'content' => 'Произвольная заметка, написанная для тестирования работы timeline элемента',
                    'icon' => [
                        'class' => 'la la-sticky-note',
                        'color' => 'info'
                    ],
                    'buttons' => [
                        'remove' => true,
                        'edit' => true,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_3.jpg',
                        'link' => '#',
                        'title' => 'Max Webster'
                    ]
                ],
                [
                    'time' => '1 день назад',
                    'title' => 'Alex Johnson <span class="m--font-normal">изменил</span> дату:',
                    'content' =>    'День рождения - 22.05.1985',
                    'icon' => [
                        'class' => 'la la-eye',
                        'color' => 'warning'
                    ],
                    'buttons' => [
                        'remove' => false,
                        'edit' => false,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_13.jpg',
                        'link' => '#',
                        'title' => 'Alex Johnson'
                    ]
                ],
                [
                    'time' => '2 дня назад',
                    'title' => 'Alex Johnson <span class="m--font-normal">закрыл</span> задачу:',
                    'content' =>    'Задача - Название задачи<br>
                                    Выполнено - 20.04.2018 11:25',
                    'icon' => [
                        'class' => 'la la-check',
                        'color' => 'success'
                    ],
                    'buttons' => [
                        'remove' => false,
                        'edit' => false,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_13.jpg',
                        'link' => '#',
                        'title' => 'Alex Johnson'
                    ],
                    'aditional_info' => [
                        [
                            'title' => 'Task type',
                            'text' => 'Main task',
                        ]
                    ]
                ],
                [
                    'time' => '5 дней назад',
                    'title' => 'Bill Cosby <span class="m--font-normal">добавил</span> заметку в <a href="#" class="m-link">Запросе #345</a>',
                    'content' => 'Произвольная заметка, написанная для тестирования работы timeline элемента',
                    'icon' => [
                        'class' => 'la la-sticky-note',
                        'color' => 'info'
                    ],
                    'buttons' => [
                        'remove' => true,
                        'edit' => true,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_14.jpg',
                        'link' => '#',
                        'title' => 'Bill Cosby'
                    ]
                ]
            ],
            'notes' => [
                [
                    'time' => 'Вчера',
                    'title' => 'Max Webster <span class="m--font-normal">добавил</span> заметку в <a href="#" class="m-link">Запросе #214</a>',
                    'content' => 'Произвольная заметка, написанная для тестирования работы timeline элемента',
                    'icon' => [
                        'class' => 'la la-sticky-note',
                        'color' => 'info'
                    ],
                    'buttons' => [
                        'remove' => true,
                        'edit' => true,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_3.jpg',
                        'link' => '#',
                        'title' => 'Max Webster'
                    ]
                ],
                [
                    'time' => '5 дней назад',
                    'title' => 'Bill Cosby <span class="m--font-normal">добавил</span> заметку в <a href="#" class="m-link">Запросе #345</a>',
                    'content' => 'Произвольная заметка, написанная для тестирования работы timeline элемента',
                    'icon' => [
                        'class' => 'la la-sticky-note',
                        'color' => 'info'
                    ],
                    'buttons' => [
                        'remove' => true,
                        'edit' => true,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_14.jpg',
                        'link' => '#',
                        'title' => 'Bill Cosby'
                    ]
                ]
            ],
            'tasks' => [
                [
                    'time' => '2 дня назад',
                    'title' => 'Alex Johnson <span class="m--font-normal">закрыл</span> задачу:',
                    'content' =>    'Задача - Название задачи<br>
                                    Выполнено - 20.04.2018 11:25',
                    'icon' => [
                        'class' => 'la la-check',
                        'color' => 'success'
                    ],
                    'buttons' => [
                        'remove' => false,
                        'edit' => false,
                    ],
                    'avatar' => [
                        'src' => 'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_13.jpg',
                        'link' => '#',
                        'title' => 'Alex Johnson'
                    ],
                    'aditional_info' => [
                        [
                            'title' => 'Task type',
                            'text' => 'Main task',
                        ]
                    ]
                ]
            ]
        ];

        $view_params = [
            'company' => $company,
            'company_contacts' => $company_contacts,
            'contacts_types' => $contacts_types,
            'all_task_types' => $all_task_types,
            'timeline' => $timeline
        ];
        
        return $this->render('view', $view_params);
    }

    /**
     * @return array|boolean
     * @throws \yii\web\HttpException
     */
    public function actionChangeResponsible()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }
        $post = \Yii::$app->request->post();
        $company_id = $post['company_id'];
        $responsible_id = $post['responsible_id'];

        // Work with database...
        $company = Companies::find()->where(['id'=>$company_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);
        if (RbacHelper::can('companies.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $company->andWhere(['in','responsible_id',$ids]);
            }
        }

        if (RbacHelper::can('companies.update.only_own')) {
            $company->andWhere(['responsible_id' => \Yii::$app->user->id]);
        }
        $company = $company->one();
        if(null !== $company)
        {
            $user = User::find()->where(['id'=>$responsible_id])->one();
            if(null !== $user)
            {
                $agent = AgentsMapping::find()->where(['user_id'=>$responsible_id])
                    ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one();
                if(null !== $agent)
                {
                    $company->responsible_id = $responsible_id;
                    $company->save();
                    $response = [
                        'result' => 'success', // success|error
                        'message' => TranslationHelper::getTranslation('saved', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
                    ];
                    \Yii::$app->response->format = Response::FORMAT_JSON;
                    return $response;
                }
            }
        }
        /* Create response */
        $response = [
            'result' => 'error', // success|error
            'message' => TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso, 'Saved')
        ];
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;

    }

    /**
     * @param $id
     *
     * @return
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(null === $model)
            throw new HttpException(404);

        if (RbacHelper::can('companies.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        if (RbacHelper::can('companies.delete.only_own')) {
            if($model->responsible_id !== \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('companies.delete.only_own_role')) {
            if(isset($ids_r)) {
                if(array_search($model->responsible_id, $ids_r) === false)
                    throw new HttpException(403);
            }
            else
                if($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        $model->delete();

        return $this->redirect('index');
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionBulkDelete()
    {
        $ids = \Yii::$app->request->post('ids', false);

        if (empty($ids))
            throw new BadRequestHttpException();

        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (RbacHelper::can('companies.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        foreach ($ids as $id)
        {
            $model = $this->findModel($id);

            if (RbacHelper::can('companies.delete.only_own')) {
                if($model->responsible_id !== \Yii::$app->user->id)
                    throw new HttpException(403);
            }

            if (RbacHelper::can('companies.delete.only_own_role')) {
                if (isset($ids_r)) {
                    if (array_search($model->responsible_id, $ids_r) === false)
                        throw new HttpException(403);
                } else {
                    if ($model->responsible_id !== \Yii::$app->user->id) {
                        throw new HttpException(403);
                    }
                }
            }
            $model->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function actionRemoveContact($id, $company_id)
    {
        $contact = Contacts::findOne($id);

        if ($contact !== null) {
            $contact->company_id = null;
            $contact->save();
        }

        return $this->redirect('view?id='.$company_id);
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSetContact()
    {
        $post = \Yii::$app->request->post();

        if (empty($post['id']) || empty($post['contact_id']))
            throw new BadRequestHttpException('Empty company id or contact id!');

        $company = $this->findModel($post['id']);

        $contact = Contacts::findOne($post['contact_id']);

        if ($contact !== null) {
            $contact->company_id = $company->id;
            $contact->save();
        }

        return $this->redirect('view?id='.$company->id);
    }

    /**
     * @param bool $email
     * @param bool $phone
     *
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionGetCompanies($email = false, $phone = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = [];

        $query = Companies::find();

        if (RbacHelper::can('companies.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);

            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $query->andWhere(['in', 'responsible_id', $ids]);
            }
        }

        if (RbacHelper::can('companies.read.only_own')) {
            $query->andWhere(['responsible_id' => \Yii::$app->user->id]);
        }

        $query->joinWith('companiesEmails')->joinWith('companiesPhones');

        if (!empty($email)) {
            $query->andWhere(['like', 'companies_emails.value', $email])
                ->orWhere(['like', 'companies_emails.value', strtolower($email)]);
        }

        if (!empty($phone)) {
            $query->andWhere(['like', 'companies_phones.value', $phone])
                ->orWhere(['like', 'companies_phones.value', str_replace(['(', ')', '+', '-'], '', $phone)]);
        }

        $query->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        $companies = $query->all();

        if (null !== $companies) {
            foreach ($companies as $company) {
                $data[] = [
                    'id' => $company->id,
                    'email' => implode(', ', array_column($company->companiesEmails, 'value')),
                    'phone' => implode(', ', array_column($company->companiesPhones, 'value')),
                    'name' => $company->name,
                ];
            }
        }

        return $data;
    }

    /**
     * @param $company_id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAddNotes($company_id)
    {
        if (!RbacHelper::can('companies.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $company_id = (int) $company_id;
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        $company = $this->findModel($company_id);

        if (null === $company)
            throw new HttpException(404);

        $note = new CompaniesNotes();
        $note->company_id = $company->id;
        $note->value = $post['value'];
        $note->save();

        return $this->redirect('view?id=' . $company_id);
    }

    /**
     * @param int $id
     *
     * @return \app\models\CompaniesNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionEditNote($id)
    {
        $id = (int) $id;
        $response = \Yii::$app->request->post();

        /** @var CompaniesNotes $note */
        $note = CompaniesNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('companies.update.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('companies.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->company_id) && $note->company->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->value = $response['value'];
        $note->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id
     *
     * @return \app\models\CompaniesNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionGetNote($id)
    {
        $id = (int) $id;

        /** @var CompaniesNotes $note */
        $note = CompaniesNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('companies.read.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('companies.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->company_id) && $note->company->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id
     *
     * @throws \yii\web\HttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteNote($id)
    {
        $id = (int) $id;

        /** @var CompaniesNotes $note */
        $note = CompaniesNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('companies.delete.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('companies.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->company_id) && $note->company->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->delete();
    }

    /**
     * @param integer $id
     *
     * @return array|Companies
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Companies::find()->where(['id' => $id])->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
