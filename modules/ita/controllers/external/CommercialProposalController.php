<?php

namespace app\modules\ita\controllers\external;

use app\models\CommercialProposals;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CommercialProposalController extends Controller
{
    /**
     * @param $code
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($code)
    {
        $cp = CommercialProposals::find()
            ->where(['link_code' => $code])
            ->one();

        if ($cp === null)
            throw new NotFoundHttpException();

        return $this->renderPartial('index', ['data' => $cp]);
    }
}
