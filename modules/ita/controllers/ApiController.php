<?php

namespace app\modules\ita\controllers;

use app\modules\ita\controllers\base\BaseController;

class ApiController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
