<?php
/**
 * ImportController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\modules\ita\controllers\base\BaseController;
use app\helpers\TranslationHelper;
use yii\web\Response;

class ImportController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionCreate()
    {
        $view_data = [
            'import_demo' => [
                'with_matches' => [
                    [
                        'label' => 'First Name',
                        'preview' => 'Leslie',
                    ],
                    [
                        'label' => 'Last Name',
                        'preview' => 'Knope',
                    ],
                    [
                        'label' => 'Email',
                        'preview' => 'leslie.knope@cocaola.com',
                    ],
                    [
                        'label' => 'Phone Number',
                        'preview' => '555-843-8116',
                    ],
                    [
                        'label' => 'Street Address',
                        'preview' => '68 Street Rd',
                    ],
                    [
                        'label' => 'City',
                        'preview' => 'Pawnee',
                    ],
                    [
                        'label' => 'State',
                        'preview' => 'IN',
                    ],
                    [
                        'label' => 'Contact Owner',
                        'preview' => 'joe@hubspot.com',
                    ],
                    [
                        'label' => 'Lifecycle Stage',
                        'preview' => 'Subscriber',
                    ],
                    [
                        'label' => 'Lead Status',
                        'preview' => 'Unqualified',
                    ]
                ],
                'without_matches' => [
                    [
                        'label' => 'Favorite Color',
                        'preview' => 'Blue',
                    ]
                ]
            ]
        ];
        return $this->render('create', $view_data);
    }
    public function actionView($id)
    {
        $import = [
            'id' => $id
        ];
        return $this->render('view', ['import' => $import]);
    }
    public function actionSetData(){
        if (\Yii::$app->request->isAjax) {
            $lang = \Yii::$app->user->identity->tenant->language->iso;
            $response = \Yii::$app->request->post();
            
            // Add save property code...
            
            // Fake response data
            $response['new_value'] = $response['value'];
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
            
            
            // Send response
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
}