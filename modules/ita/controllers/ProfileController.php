<?php
/**
 * ProfileController.php
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\modules\ita\controllers;

use app\models\ProfilePictureUploadForm;
use app\models\traits\ProfileTrait;
use app\models\User;
use app\models\Tags;
use app\models\ProfileNotificationsTags;
use app\models\ProfileNotifications;
use app\components\notifications_now\NotificationActions;
use app\models\ProfileNotificationsStatuses;
use app\models\RequestStatuses;
use app\modules\ita\controllers\base\BaseController;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

class ProfileController extends BaseController
{
    use ProfileTrait;

    private $notificationsActions = false;

    protected $data;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->setParams(['header' => 'show_search_panel', 'subheader' => 'show']);
        $this->setData([
            'number_of_tourists' => 200,
            'number_of_requests' => 355,
            'number_of_bookings' => 180,
            'user' => Yii::$app->user->identity->toArray(),
            'notifications' => $this->getNotificationsIndex(Yii::$app->user->id)
        ]);

        return $this->render('index', ['data' => $this->data]);
    }


    public function actionSaveNotifications($id)
    {

        $user_id = intval($id);
        $tenant = \Yii::$app->user->identity->tenant;

        if (empty($user_id))
            $this->redirect('/ita/' . $tenant->id . '/profile?tab=3');


        $post = \Yii::$app->request->post();
        $notificationsData = $this->getNotifications($user_id);

        foreach ($post as $action => $data) {
            if (!in_array($action, $this->getNotActions()))
                continue;

            if (isset($notificationsData[$action])) {
                $notification = ProfileNotifications::find()->where(['id' => $notificationsData[$action]['id']])->one();
            } else {
                $notification = new ProfileNotifications();
                $notification->action = $action;
                $notification->user_id = $user_id;
            }

            // save other data
            $notification->level = isset($data['level']) ? (int)$data['level'] : 0;
            $notification->is_email = isset($data['is_email']) ? (int)$data['is_email'] : 0;
            $notification->is_notice = isset($data['is_notice']) ? (int)$data['is_notice'] : 0;
            $notification->save();


            // save tags
            if (isset($data['tags'])) {

                $notification_tags = isset($notificationsData[$action]['tags']) ? $notificationsData[$action]['tags'] : [];
                $old_ids = array_column($notification_tags, 'tag_id');
                foreach ($data['tags'] as $tid) {
                    if (in_array($tid, $old_ids)) {
                        $exist = array_search($tid, $old_ids);
                        unset($old_ids[$exist]);
                        continue;
                    }

                    if (is_numeric($tid)) {
                        $tag = Tags::find()
                            ->where(['id' => $tid])->one();
                    }

                    if (empty($tag)) continue;

                    $link = ProfileNotificationsTags::find()->where([
                        'notification_id' => $notification->id,
                        'tag_id' => $tag->id
                    ])->one();

                    if (is_null($link)) {
                        $link = new ProfileNotificationsTags();
                        $link->notification_id = $notification->id;
                        $link->tag_id = $tag->id;
                        $link->save();
                    }
                }

                if (!empty($old_ids)) {
                    $oldTags = ProfileNotificationsTags::find()->where([
                        'and',
                        ['notification_id' => $notification->id],
                        ['in', 'tag_id', $old_ids]
                    ])->all();

                    if ($oldTags)
                        foreach ($oldTags as $oTag)
                            $oTag->delete();

                }
            } else {
                ProfileNotificationsTags::deleteAll(['notification_id' => $notification->id]);
            }

            // save statuses
            if (isset($data['statuses'])) {

                $notification_tags = isset($notificationsData[$action]['statuses']) ? $notificationsData[$action]['statuses'] : [];
                $old_ids = array_column($notification_tags, 'status_id');
                foreach ($data['statuses'] as $tid) {
                    if (in_array($tid, $old_ids)) {
                        $exist = array_search($tid, $old_ids);
                        unset($old_ids[$exist]);
                        continue;
                    }

                    if (is_numeric($tid)) {
                        $status = RequestStatuses::find()
                            ->where(['id' => $tid])->one();
                    }

                    if (empty($status)) continue;

                    $link = ProfileNotificationsStatuses::find()->where([
                        'notification_id' => $notification->id,
                        'status_id' => $status->id
                    ])->one();

                    if (is_null($link)) {
                        $link = new ProfileNotificationsStatuses();
                        $link->notification_id = $notification->id;
                        $link->status_id = $status->id;
                        $link->save();
                    }
                }

                if (!empty($old_ids)) {
                    $oldTags = ProfileNotificationsStatuses::find()->where([
                        'and',
                        ['notification_id' => $notification->id],
                        ['in', 'status_id', $old_ids]
                    ])->all();

                    if ($oldTags)
                        foreach ($oldTags as $oTag)
                            $oTag->delete();

                }
            } else {
                ProfileNotificationsStatuses::deleteAll(['notification_id' => $notification->id]);
            }
        }

        $this->redirect('/ita/' . $tenant->id . '/profile?tab=3');
    }

    public function actionPersonalDetails()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (\Yii::$app->request->isPost) {
            $model = User::findOne(\Yii::$app->user->id);

            $data = [
                'User' => \Yii::$app->request->post()
            ];

            if ($model->load($data) && $model->save()) {
                $client = \Yii::$app->auth0->management_api_client;
                $client->users->update($model->auth->source_id, [
                    'connection' => 'Username-Password-Authentication',
                    'user_metadata' => [
                        'first_name' => $model->first_name,
                        'last_name' => $model->last_name,
                        'middle_name' => $model->middle_name,
                        'phone' => $model->phone
                    ],
                ]);

                return ['name' => $model->first_name . ' ' . $model->last_name];
            }
        }

        return ['error' => true];
    }

    public function actionChangeEmail()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        $email = \Yii::$app->request->post('email', false);
        $password = \Yii::$app->request->post('password', false);

        $model = User::findOne(\Yii::$app->user->id);

        if (empty($email) || empty($password))
            return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_change_email_error', $lang)];

        if (User::checkAuth0User($email))
            return ['error' => \app\helpers\TranslationHelper::getTranslation('signup_user_email_error', $lang)];

        try {
            $auth0Data = \Yii::$app->auth0->auth_api_client->getUserInfo($model->email, $password);

            if (empty($auth0Data['email']))
                return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_pass_error', $lang)];

            $client = \Yii::$app->auth0->management_api_client;
            $user_data = $client->users->update($auth0Data['sub'], [
                'connection' => 'Username-Password-Authentication',
                'email' => $email,
                'email_verified' => false,
                'verify_email' => true
            ]);

            if ($user_data['user_id']) {
                $model->email = $email;

                if ($model->save())
                    return ['email' => $email];
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_pass_error', $lang)];
        }

        return ['error' => 'Internal server error!'];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionChangePassword()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        $password = \Yii::$app->request->post('password', false);
        $new_password = \Yii::$app->request->post('new_password', false);
        $confirm_password = \Yii::$app->request->post('confirm_password', false);

        $model = User::findOne(\Yii::$app->user->id);

        if (empty($password) || empty($new_password) || empty($confirm_password))
            return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_fields_empty_error', $lang)];

        if ($new_password !== $confirm_password)
            return ['error' => \app\helpers\TranslationHelper::getTranslation('signup_user_pass_error', $lang)];

        try {
            $auth0Data = \Yii::$app->auth0->auth_api_client->getUserInfo($model->email, $password);

            if (empty($auth0Data['email']))
                return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_pass_error', $lang)];

            $client = \Yii::$app->auth0->management_api_client;
            $user_data = $client->users->update($auth0Data['sub'], [
                'connection' => 'Username-Password-Authentication',
                'password' => $new_password
            ]);

            if ($user_data['user_id']) {
                $model->setPassword($new_password);

                if ($model->save())
                    return ['success' => true];
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return ['error' => \app\helpers\TranslationHelper::getTranslation('profile_pass_error', $lang)];
        }

        return ['error' => 'Internal server error!'];
    }

    /**
     * @return array|bool
     * @throws \yii\base\Exception
     */
    public function actionChangeProfilePicture()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new ProfilePictureUploadForm();

        if (\Yii::$app->request->isPost) {
            $model->profile_picture = UploadedFile::getInstanceByName('profile_picture');

            if ($photo = $model->upload()) {
                // file is uploaded successfully
                return ['photo' => $photo];
            }
        }

        return ['error' => true];
    }


    private function getNotActions()
    {
        return ($this->notificationsActions === false) ? $this->notificationsActions = array_keys(NotificationActions::ACTIONS) : $this->notificationsActions;
    }

    private function array_column_recursive(array $haystack, $needle)
    {
        $found = [];
        array_walk_recursive($haystack, function ($value, $key) use (&$found, $needle) {
            if ($key == $needle)
                $found[] = $value;
        });
        return $found;
    }

    private function getNotificationsIndex($user_id)
    {
        $notificationsData = $this->getNotifications($user_id);
        if (empty($notificationsData))
            return [];

        // add tags names
        $tags_ids = $this->array_column_recursive($notificationsData, 'tag_id');
        $tags_names = [];
        $tags_names_data = [];

        if ($tags_ids)
            $tags_names = Tags::find()->where(['in', 'id', $tags_ids])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();

        if ($tags_names)
            foreach ($tags_names as $tname)
                $tags_names_data[$tname['id']] = $tname['value'];

        foreach ($notificationsData as &$notifdata) {
            if (!isset($notifdata['tags']) || empty($notifdata['tags']))
                continue;

            foreach ($notifdata['tags'] as &$notiftag) {
                $notiftag['name'] = isset($tags_names_data[$notiftag['tag_id']]) ? $tags_names_data[$notiftag['tag_id']] : '[Not translated]';
            }
        }

        // add statuses names
        $statuses_ids = $this->array_column_recursive($notificationsData, 'status_id');
        $statuses_names_data = [];
        $statuses_names = [];

        if ($statuses_ids)
            $statuses_names = RequestStatuses::find()->where(['in', 'id', $statuses_ids])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();

        if ($statuses_names)
            foreach ($statuses_names as $tname)
                $statuses_names_data[$tname['id']] = $tname['name'];

        foreach ($notificationsData as &$notifdata) {
            if (!isset($notifdata['statuses']) || empty($notifdata['statuses']))
                continue;

            foreach ($notifdata['statuses'] as &$notiftag) {
                $notiftag['name'] = isset($statuses_names_data[$notiftag['status_id']]) ? $statuses_names_data[$notiftag['status_id']] : '[Not translated]';
            }
        }

        return $notificationsData;
    }

    private function getNotifications($user_id)
    {
        $notifications = ProfileNotifications::find()
            ->joinWith('tags')
            ->joinWith('statuses')
            ->where([
                'and',
                ['user_id' => $user_id],
                ['in', 'action', $this->getNotActions()],
            ])
            ->asArray()
            ->all();

        if (empty($notifications))
            return [];

        $notificationsData = [];
        foreach ($notifications as $notif)
            $notificationsData[$notif['action']] = $notif;

        return $notificationsData;
    }
}
