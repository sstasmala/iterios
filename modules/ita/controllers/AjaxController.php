<?php

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\helpers\SystemNotificationsHelper;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\Contacts;
use app\models\ContactsTags;
use app\models\Requests;
use app\models\ProvidersCredentials;
use app\models\SendSms;
use app\models\RequestType;
use app\models\SystemNotifications;
use app\models\Tags;
use app\models\RequestStatuses;
use app\models\TasksTypes;
use app\models\Translations;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use app\helpers\MCHelper;

class AjaxController extends Controller
{

    public function actionNotifications()
    {
//        SystemNotificationsHelper::addNotification('My notification!', \Yii::$app->user->identity, \Yii::$app->user->identity->tenant);
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $items_per_load = intval($data['items_per_load']);
            $offset = intval($data['offset']);

            $response = [];

            $notifications = SystemNotificationsHelper::getNotificationsObject();

            $notifications_count = $notifications->count();

            $notifications = $notifications->limit($items_per_load)
                ->offset($offset)
                ->asArray()->all();

            $response['notifications'] = $notifications;
            $response['notifications_count'] = $notifications_count;
            $response['stack_finished'] = empty($notifications) || $notifications_count < $items_per_load ? true : false;
            $response['new_offset'] = empty($notifications) || $notifications_count < $items_per_load ? false : ($items_per_load + $offset);

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public function actionReadNotification($id)
    {
        if (Yii::$app->request->isAjax) {
            $notification = SystemNotifications::find()
                ->where(['user_id' => \Yii::$app->user->id])
                ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->andWhere(['id' => $id])
                ->one();

            $notification->read = $notification->read == false ? true : false;
            $notification->save();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return true;
        }
    }

    public function actionSearchTaskTypes()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $types = TasksTypes::find()
                ->translate(\Yii::$app->user->identity->tenant->language->iso);

            if (isset($data['search'])) {
                $types->andFilterWhere(['like', 'value', $data['search']])
                    ->orFilterWhere(['ilike', 'value', $data['search']]);
            }

            $types = $types->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $types;
        }
    }

    public function actionSearchResponsible()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $tenant_users = AgentsMapping::find()
                ->select('user_id')
                ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->column();

            $users = User::find()
                ->select(['id', "CONCAT(last_name, ' ', first_name) AS first_name"]);

            if (isset($data['search'])) {
                $users->andFilterWhere(['like', "CONCAT(last_name, ' ', first_name)", $data['search']])
                    ->orFilterWhere(['ilike', "CONCAT(last_name, ' ', first_name)", $data['search']]);
            }

            $users->andWhere(['in', 'id', $tenant_users]);
            $users = $users->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $users;
        }
    }

    public function actionSearchRequest() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $request_name = "CONCAT('".TranslationHelper::getTranslation('order_request', Yii::$app->user->identity->tenant->language->iso)."', ' #', requests.id, ' (', contacts.last_name, ' ', contacts.first_name, ')')";

            $requests = Requests::find()
                ->select(['requests.id', $request_name . ' AS request_description']);

            if (isset($data['search'])) {
                $requests->andFilterWhere(['like', $request_name, $data['search']])
                    ->orFilterWhere(['ilike', $request_name, $data['search']]);
            }

            $requests->andWhere(['requests.tenant_id' => Yii::$app->user->identity->tenant->id])
                ->joinWith('contact')
                ->orderBy(['requests.id' => SORT_ASC]);
            $requests = $requests->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $requests;
        }
    }

    public function actionSearchContacts()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $contacts = Contacts::find()
                ->select(['id', "CONCAT(first_name, ' ', last_name) AS first_name"]);

            if (isset($data['search'])) {
                $contacts->andFilterWhere(['like', "CONCAT(first_name, ' ', last_name)", $data['search']])
                    ->orFilterWhere(['ilike', "CONCAT(first_name, ' ', last_name)", $data['search']]);
            }

            $contacts->andWhere(['tenant_id' => Yii::$app->user->identity->tenant->id]);
            $contacts->orderBy(['first_name' => SORT_ASC]);
            $contacts = $contacts->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $contacts;
        }
    }


    public function actionSearchProvider()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $providers = ProvidersCredentials::find()
                ->select(["providers.id", "providers.name", "provider_id"])
                ->joinWith('provider');

            if (isset($data['search'])) {
                $providers->andFilterWhere(['like', "name", $data['search']])
                    ->orFilterWhere(['ilike', "name", $data['search']]);
            }

            $providers->andWhere(['tenant_id' => Yii::$app->user->identity->tenant->id]);
            $providers->orderBy(['name' => SORT_ASC]);
            $providers = $providers->asArray()->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $providers;
        }
    }

    public function actionSearchStatusSms()
    {
        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return SendSms::STATUS;
        }
    }

    public function actionSearchTags()
    {
        if (Yii::$app->request->isAjax) {
            $lang = Yii::$app->user->identity->tenant->language->iso;
            $data = Yii::$app->request->post();

            $tags = Tags::find()->where(['tenant_id' => Yii::$app->user->identity->tenant->id]);

            if (Yii::$app->user->identity->tenant->system_tags || $data['type'] == Tags::TYPE_SYSTEM)
                $tags->orWhere(['type' => 'system']);

            $tags->translate($lang);

            if (isset($data['search'])) {
                $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
                $translate->orFilterWhere(['like', 'value', $data['search']])
                    ->orFilterWhere(['ilike', 'value', $data['search']])
                    ->andWhere('"key" LIKE :table', [':table' => "tags.value.%"]);
                $translate->addParams([':div' => '.', ':index' => 3]);
                $translate = $translate->asArray()->column();

                $tags->andWhere(['in', 'id', array_unique($translate)]);
            }

            $tags = $tags->asArray()->all();

            foreach ($tags as $i => $item) {
                $keys = array_keys($item);
                $keys[array_search('value', $keys)] = 'name';
                $item = array_combine($keys, $item);
                if (is_null($item['name']))
                    $item['name'] = '[Not translated]';
                $tags[$i] = $item;
            }
            $response = $tags;


            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public function actionGetOrderRequestSources()
    {
        if (!Yii::$app->request->isAjax)
            return;

        $lang = Yii::$app->user->identity->tenant->language->iso;

        $sources = RequestType::find()
            ->where(['system' => false])
            ->orWhere(['system_type' => RequestType::SYSTEM_FROM_REQUEST_TYPE])
            ->orderBy(['system_type' => SORT_ASC])
            ->translate($lang)
            ->all();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $sources;
    }

    public function actionSearchStatuses()
    {
        if (!Yii::$app->request->isAjax)
            return;

        $lang = Yii::$app->user->identity->tenant->language->iso;
        $data = Yii::$app->request->post();

        $statuses = RequestStatuses::find();
        $statuses->translate($lang);

        if (isset($data['search'])) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value', $data['search']])
                ->orFilterWhere(['ilike', 'value', $data['search']])
                ->andWhere('"key" LIKE :table', [':table' => "request_statuses.name.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $statuses->andWhere(['in', 'id', array_unique($translate)]);
        }

        $statuses = $statuses->asArray()->all();

        foreach ($statuses as $i => $item) {
            $keys = array_keys($item);
            $keys[array_search('value', $keys)] = 'name';
            $item = array_combine($keys, $item);
            if (is_null($item['name']))
                $item['name'] = '[Not translated]';
            $tags[$i] = $item;
        }

        $response = $statuses;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    public function actionGetTagsList()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        $lang = Yii::$app->user->identity->tenant->language->iso;

        $tags = Tags::find()->where(['tenant_id' => Yii::$app->user->identity->tenant->id]);

        if (Yii::$app->user->identity->tenant->system_tags)
            $tags->orWhere(['type' => 'system']);

        $tags->translate($lang);

        if (isset($data['search'])) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value', $data['search']])
                ->orFilterWhere(['ilike', 'value', $data['search']])
                ->andWhere('"key" LIKE :table', [':table' => "tags.value.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $tags->andWhere(['in', 'id', array_unique($translate)]);
        }

        $tenant = Yii::$app->user->identity->tenant;
        $contacts_ids = array_column(Contacts::find()
            ->select(['id'])->where(['tenant_id' => $tenant->id])
            ->asArray()->all(), 'id');
        $tags = $tags->orderBy('created_at DESC')->asArray()->all();

        foreach ($tags as $i => $tag) {
            $keys = array_keys($tag);
            $keys[array_search('value', $keys)] = 'name';
            $tag = array_combine($keys, $tag);

            if (is_null($tag['name']))
                $tag['name'] = '[Not translated]';

            $tag['created_at'] = DateTimeHelper::convertTimestampToDateTime($tag['created_at']);

            $tag['contact_uses'] = count(ContactsTags::find()->where(['in', 'contact_id', $contacts_ids])
                ->andWhere(['tag_id' => $tag['id']])->all());
            $tag['company_uses'] = 0;//TODO change when companies will be done
            $tag['link'] = '#';//TODO change when tags link wil be resolved
            $tags[$i] = $tag;
        }

        // Response
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $tags;
    }

    public function actionUpdateTag()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        $post = Yii::$app->request->post();

        if (!isset($post['id']) || !isset($post['value']) || !isset($post['property']))
            return;

        if ($post['property'] != 'name')
            return;

        $tag = Tags::find()->where(['tenant_id' => Yii::$app->user->identity->tenant->id])
            ->andWhere(['id' => intval($post['id'])])->one();

        if (is_null($tag) || $tag->type == Tags::TYPE_SYSTEM)
            throw new HttpException(404, 'Tag not found!');

        $tag->value = $post['value'];
        $tag->saveWithLang(Yii::$app->user->identity->tenant->language->iso);

        if (!empty($post['value'])) {
            // Save tag code...
            $response['new_value'] = $post['value'];
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
        } else {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso, 'Error');
            $response['result'] = 'error';
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    public function actionGetUsageCurrency()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $currencies_data = null;
        $main_currency = null;
        $main_currency_id = (isset(\Yii::$app->user->identity->tenant->main_currency)) ? \Yii::$app->user->identity->tenant->main_currency : null;
        if ($main_currency_id) {
            $mc_data = MCHelper::getCurrencyById($main_currency_id, \Yii::$app->user->identity->tenant->language->iso);
            $main_currency = (!empty($mc_data)) ? $mc_data[0] : null;
            if (!is_null($main_currency)) {
                $currencies_data['main_currency'] = $main_currency;
            }
        }
        if (isset(\Yii::$app->user->identity->tenant->usage_currency)) {
            $usage_currencies = json_decode(\Yii::$app->user->identity->tenant->usage_currency);
            if (!empty($usage_currencies)) {
                $usage_currencies_ids = implode(',', $usage_currencies);
                $currencies_data['usage_currencies'] = MCHelper::getCurrencyById($usage_currencies_ids, \Yii::$app->user->identity->tenant->language->iso);
            }
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (is_null($currencies_data['main_currency'] || is_null($currencies_data['usage_currencies'])))
            return [];
        return $currencies_data;
    }

    /* Disable csef validation */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionDeleteTag()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->post();

        if (!isset($data['tag_id'])) {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso);
            $response['result'] = 'error';
            return $response;
        }

        $tag = Tags::find()->where(['type' => 'system'])->orWhere(['tenant_id' => Yii::$app->user->identity->tenant->id])
            ->andWhere(['id' => $data['tag_id']])->one();

        if (is_null($tag) || $tag->type == Tags::TYPE_SYSTEM) {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso);
            $response['result'] = 'error';
            return $response;
        }

        if ($tag->delete()) {  //CHANGE ON REAL DELETE RESULT
            // Save tag code...
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('deleted', Yii::$app->user->identity->tenant->language->iso);
        } else {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso);
            $response['result'] = 'error';
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    public function actionCreateTag()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        $data = Yii::$app->request->post();

        if (!isset($data['tag_name'])) {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso);
            $response['result'] = 'error';
            return $response;
        }

        $tag = new Tags();
        $tag->value = $data['tag_name'];
        $tag->type = Tags::TYPE_CUSTOM;
        $tag->tenant_id = Yii::$app->user->identity->tenant->id;

        //Saving code...
        if ($tag->saveWithLang(Yii::$app->user->identity->tenant->language->iso)) {  //CHANGE ON REAL CREATE RESULT
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('tag_was_created', Yii::$app->user->identity->tenant->language->iso);
        } else {
            $response['message'] = TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso);
            $response['result'] = 'error';
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

}
