<?php
/**
 * ContactsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\ContactsHelper;
use app\helpers\DateTimeHelper;
use app\helpers\LogHelper;
use app\helpers\MCHelper;
use app\helpers\RbacHelper;
use app\models\AgentsMapping;
use app\models\Contacts;
use app\models\ContactsAddresses;
use app\models\ContactsNotes;
use app\models\ContactsPassports;
use app\models\ContactsPhones;
use app\models\ContactsTags;
use app\models\ContactsTimelineView1;
use app\models\ContactsVisas;
use app\models\Log;
use app\models\MessengersTypes;
use app\models\PassportsTypes;
use app\models\search\PhonesTypes;
use app\models\RequestNotes;
use app\helpers\TranslationHelper;
use app\models\Tags;
use app\models\Tasks;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use app\utilities\contacts\ContactsUtility;
use app\utilities\logs\LogsUtility;
use app\utilities\timeline\TimeLineUtility;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use app\models\TasksTypes;

//use app\components\notifications_now\NotificationActions;

class ContactsController extends BaseController
{
    public $template;
    public $types_template;

    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

        $lang = Yii::$app->user->identity->tenant->language->iso;

        switch ($action->id)
        {
            case 'index':if(!RbacHelper::can('contacts.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'select-all':if(!RbacHelper::can('contacts.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'get-data':if(!RbacHelper::can('contacts.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'view':if(!RbacHelper::can('contacts.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'create':if(!RbacHelper::can('contacts.create.access') && !RbacHelper::owner())throw new HttpException(403);break;
            case 'set-param-ajax':if(!RbacHelper::can('contacts.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'change-responsible':if(!RbacHelper::can('contacts.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'bulk-actions':if(!RbacHelper::can('contacts.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'delete-passport':if(!RbacHelper::can('contacts.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
            case 'delete-visa':if(!RbacHelper::can('contacts.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
            case 'delete':if(!RbacHelper::can('contacts.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
            case 'bulk-delete':if(!RbacHelper::can('contacts.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
        }
        return $parent;
    }

    public function actionIndex()
    {

        // action (class), id contact, id tag
        /*$action = new NotificationActions('contact_tag_add', 38, 102);
        $action->send();*/

        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        \Yii::$app->view->params['subheader'] = [
            'show' => false
        ];

        return $this->render('index');
    }

    /**
     *
     * @throws \yii\web\HttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $contacts = Contacts::find();
        $contacts->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'my_contacts') {
            $contacts->andWhere(['responsible' => \Yii::$app->user->id]);
        }

        if (RbacHelper::can('contacts.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $contacts->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('contacts.read.only_own')) {
            $contacts->andWhere(['responsible' => \Yii::$app->user->id]);
        }

        if (!empty($search_data['search_text']) && empty($search_data['filter_email']) && empty($search_data['filter_phone'])) {
            $search_arr = explode(' ', trim($search_data['search_text']));
            $search_arr = array_diff($search_arr, []);

            $contacts->andWhere(['ilike', 'contacts_emails.value', trim($search_data['search_text'])])
                ->orWhere(['ilike', 'contacts_phones.value', trim($search_data['search_text'])]);

            foreach ($search_arr as $search) {
                $contacts->orWhere(['ilike', 'first_name', trim($search)])
                    ->orWhere(['ilike', 'last_name', trim($search)])
                    ->orWhere(['ilike', 'middle_name', trim($search)]);
            }
        }

        if (!empty($search_data['filter_email']))
            $contacts->andWhere(['ilike', 'contacts_emails.value', $search_data['filter_email']]);

        if (!empty($search_data['filter_phone']))
            $contacts->andWhere(['ilike', 'contacts_phones.value', $search_data['filter_phone']]);

        if (!empty($search_data['filter_tags'])) {
            $tags = ContactsTags::find()->select(['contact_id', 'COUNT(tag_id) as tag_count'])
                ->where(['in', 'tag_id', $search_data['filter_tags']])
                ->having(['COUNT(tag_id)' => count($search_data['filter_tags'])])
                ->groupBy('contact_id')
                ->asArray()->all();

            $contacts_ids = array_column($tags, 'contact_id');

            if (isset($contacts_ids))
                $contacts->andWhere(['in', 'contacts.id', $contacts_ids]);
        }

        $contacts->joinWith('contactsEmails')->joinWith('contactsPhones')
            ->with('company','residenceAddress','livingAddress','contactsPassports'
                ,'contactsMessengers','contactsMessengers','contactsSites','contactsSocials','contactsTags','contactsVisas',
                'contactsMessengers.type','responsible');
        $meta['total'] = $contacts->count('DISTINCT contacts.id');
        $contacts->limit($meta['perpage'])->offset($meta['perpage']*($meta['page']-1))->orderBy('created_at DESC');
        $contacts = $contacts->asArray()->all();

        $new_contacts = [];

        foreach ($contacts as $contact)
        {
            $data = ContactsUtility::prepareContactsViewArray($contact);
            $data['date_of_birth'] = DateTimeHelper::convertTimestampToDate($data['date_of_birth']);
            $data['created_at'] = DateTimeHelper::convertTimestampToDateTime($data['created_at']);
            $data['updated_at'] = DateTimeHelper::convertTimestampToDateTime($data['updated_at']);
            $data['nationality'] = MCHelper::getCountryById($data['nationality'],\Yii::$app->user->identity->tenant->language->iso)[0]['title'];
            $new_contacts[] = $data;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data'=>$new_contacts,'meta'=>$meta];
    }
    
    public function actionSetParamAjax()
    {
        $lang = \Yii::$app->user->identity->tenant->language->iso;
        if (\Yii::$app->request->isAjax) {
            //PROCCESS
            $response = \Yii::$app->request->post();
            $contact = Contacts::find()->where(['id'=>$response['id']])
                ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->one();
            if(is_null($contact))
                throw new HttpException(404);

            if (RbacHelper::can('contacts.update.only_own')) {
                if($contact->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }

            if (RbacHelper::can('contacts.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
                if(!empty($roles)) {
                    $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                    if(array_search($contact->responsible,$ids)===false)
                        throw new HttpException(403);
                }
                else
                    if($contact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }


            switch ($response['property']){
                case 'company':
                    //logic
                    $contact->company_id = (int) $response['value'];
                    $contact->save();
                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $contact->company->name;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'date_of_birth':
                    //logic
                    $contact->date_of_birth = DateTimeHelper::convertDateToTimestamp($response['value']);
                    $contact->save();
                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'sex':
                    if($response['value'] == 0 || $response['value'] == 1) {
                        $contact->gender = $response['value'];
                        $contact->save();
                        if($response['value'] == '0'){
                            $gender_value = TranslationHelper::getTranslation('female_sex',$lang);
                        }
                        if($response['value'] == '1'){
                            $gender_value = TranslationHelper::getTranslation('male_sex',$lang);
                        }
                    } else {
                        $contact->gender = null;
                        $contact->save();
                        $gender_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }

                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    //logic
                    $response['new_value'] = $gender_value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved',$lang);
                    break;
                case 'discount':
                    //logic
                    if (!empty($response['value'])) {
                        if($response['value'] >= 0 && $response['value'] <=100){
                            $contact->discount = $response['value'];
                            $contact->save();
                            $discount_value = $response['value'] . '%';
                        } else {
                            $contact->discount = null;
                            $contact->save();
                            $discount_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                        }
                    } else {
                        $contact->discount = null;
                        $contact->save();
                        $discount_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $discount_value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'type':
                    //logic

                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'nationality':
                    //logic
                    if (!empty($response['value'])) {
                        $contact->nationality = $response['value'];
                        $contact->save();
                        $nationality_value = MCHelper::getCountryById($response['value'],$lang)[0]['title'];
                    } else {
                        $contact->nationality = null;
                        $contact->save();
                        $nationality_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }

                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $nationality_value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'tin':
                    //logic
                    if (!empty($response['value'])) {
                        if($response['value'] > 0){
                            $contact->tin = $response['value'];
                            $contact->save();
                            $tin_value = $response['value'];
                        } else {
                            $contact->tin = null;
                            $contact->save();
                            $tin_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                        }
                    } else {
                        $contact->tin = null;
                        $contact->save();
                        $tin_value = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }

                    if($contact->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $tin_value;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'living_address':
                    $address = $contact->livingAddress;
                    if(is_null($address))
                        $address = new ContactsAddresses();
                    //logic
                    /* Generate new adress row */
                    $arderss_row = '';
                    if (!empty($response['living_address'])) {
                        if (isset($response['living_address']['country']) && !empty($response['living_address']['country'])) {
                            $address->country = $response['living_address']['country'];
                            $arderss_row .= MCHelper::getCountryById($response['living_address']['country'],$lang)[0]['title']. ', ';
                        }
                        if (isset($response['living_address']['region']) && !empty($response['living_address']['region'])) {
                            $address->region = $response['living_address']['region'];
                            $arderss_row .= $response['living_address']['region'] . ', ';
                        }
                        if (isset($response['living_address']['city']) && !empty($response['living_address']['city'])) {
                            $address->city = $response['living_address']['city'];
                            $arderss_row .= $response['living_address']['city'] . ', ';
                        }
                        if (isset($response['living_address']['street']) && !empty($response['living_address']['street'])) {
                            $address->street = $response['living_address']['street'];
                            $arderss_row .= $response['living_address']['street'];
                            if (isset($response['living_address']['house']) && !empty($response['living_address']['house'])) {
                                $address->house = $response['living_address']['house'];
                                $arderss_row .= ' ' . $response['living_address']['house'];
                                if (isset($response['living_address']['flat']) && !empty($response['living_address']['flat'])) {
                                    $address->flat = $response['living_address']['flat'];
                                    $arderss_row .= ', ' . $response['living_address']['flat'];
                                }
                            }
                            $arderss_row .= ', ';
                        }
                        if (isset($response['living_address']['postcode']) && !empty($response['living_address']['postcode'])) {
                            $address->postcode = $response['living_address']['postcode'];
                            $arderss_row .= $response['living_address']['postcode'] . ', ';
                        }
                        $arderss_row = substr(trim($arderss_row), 0, -1);
                        $logItemId = $address->save();
                        if ($logItemId !== true) {
                            $contact->living_address_id = $address->id;
                            $contact->touch('updated_at');
                            $contact->force_update = true;
                            $logMainItemId = $contact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }
                        if($contact->hasErrors())
                        {
                            $response['result'] = 'error';
                            $response['message'] = TranslationHelper::getTranslation('error',$lang);
                            break;
                        }
                    } else {
                        $arderss_row = 'Not set';
                    }
                    /* Set response */
                    $response['new_value'] = $arderss_row;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'residence_address':
                    $address = $contact->residenceAddress;
                    if(is_null($address))
                        $address = new ContactsAddresses();
                    //logic
                    /* Generate new adress row */
                    $arderss_row = '';
                    if (!empty($response['residence_address'])) {
                        if (isset($response['residence_address']['country']) && !empty($response['residence_address']['country'])) {
                            $address->country = $response['residence_address']['country'];
                            $arderss_row .= MCHelper::getCountryById($response['residence_address']['country'],$lang)[0]['title'] . ', ';
                        }
                        if (isset($response['residence_address']['region']) && !empty($response['residence_address']['region'])) {
                            $address->region = $response['residence_address']['region'];
                            $arderss_row .= $response['residence_address']['region'] . ', ';
                        }
                        if (isset($response['residence_address']['city']) && !empty($response['residence_address']['city'])) {
                            $address->city = $response['residence_address']['city'];
                            $arderss_row .= $response['residence_address']['city'] . ', ';
                        }
                        if (isset($response['residence_address']['street']) && !empty($response['residence_address']['street'])) {
                            $address->street = $response['residence_address']['street'];
                            $arderss_row .= $response['residence_address']['street'];
                            if (isset($response['residence_address']['house']) && !empty($response['residence_address']['house'])) {
                                $address->house = $response['residence_address']['house'];
                                $arderss_row .= ' ' . $response['residence_address']['house'];
                                if (isset($response['residence_address']['flat']) && !empty($response['residence_address']['flat'])) {
                                    $address->flat = $response['residence_address']['flat'];
                                    $arderss_row .= ', ' . $response['residence_address']['flat'];
                                }
                            }
                            $arderss_row .= ', ';
                        }
                        if (isset($response['residence_address']['postcode']) && !empty($response['residence_address']['postcode'])) {
                            $address->postcode = $response['residence_address']['postcode'];
                            $arderss_row .= $response['residence_address']['postcode'] . ', ';
                        }
                        $arderss_row = substr(trim($arderss_row), 0, -1);
                        $logItemId = $address->save();
                        if ($logItemId !== true) {
                            $contact->residence_address_id = $address->id;
                            $contact->touch('updated_at');
                            $contact->force_update = true;
                            $logMainItemId = $contact->save();
                            LogHelper::setGroupId($logItemId, $logMainItemId);
                        }

                        if($contact->hasErrors())
                        {
                            $response['result'] = 'error';
                            $response['message'] = TranslationHelper::getTranslation('error',$lang);
                            break;
                        }
                    } else {
                        $arderss_row = 'Not set';
                    }
                    /* Set response */
                    $response['new_value'] = $arderss_row;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                default :
                    $response['result'] = 'error';
                    $response['new_value'] = $response['value'];
                    $response['message'] = TranslationHelper::getTranslation('ajax_error_message_undefined_object_property', $lang, 'Undefined object property') . ' ' . $response['property'];
                    break;
            }
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public function actionCreate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $contact_data = \Yii::$app->request->post();
        $contact_id = ContactsHelper::create($contact_data);

        return !empty($contact_id) ? ['id' => $contact_id] : false;
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionBulkActions()
    {
        $data = \Yii::$app->request->post();

        if (empty($data['ids']))
            throw new HttpException(404);

        $ids = explode(',', $data['ids']);

        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        if (isset($data['responsible'])) {
            $user = AgentsMapping::findOne([
                'user_id' => $data['responsible'],
                'tenant_id' => Yii::$app->user->identity->tenant->id
            ]);

            if (is_null($user))
                throw new HttpException(404);

            foreach ($ids as $id) {
                $contact = Contacts::findOne($id);
                if (RbacHelper::can('contacts.update.only_own')) {
                    if($contact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
                }
                if (RbacHelper::can('contacts.update.only_own_role')) {
                    if(isset($ids_r)) {
                        if(array_search($contact->responsible,$ids_r)===false)
                            throw new HttpException(403);
                    }
                    else
                        if($contact->responsible != \Yii::$app->user->id)
                            throw new HttpException(403);
                }
                $contact->responsible = $data['responsible'];
                $contact->save();
            }
        }

        if (isset($data['tags'])) {
            if (!is_array($data['tags']))
                throw new HttpException(404);

            foreach ($ids as $id) {
                $tags = ContactsTags::findAll(['contact_id' => $id]);

                foreach ($tags as $tag) {
                    $forceContact = Contacts::findOne($id);
                    if (RbacHelper::can('contacts.update.only_own')) {
                        if($forceContact->responsible != \Yii::$app->user->id)
                            throw new HttpException(403);
                    }
                    if (RbacHelper::can('contacts.update.only_own_role')) {
                        if(isset($ids_r)) {
                            if(array_search($forceContact->responsible,$ids_r)===false)
                                throw new HttpException(403);
                        }
                        else
                            if($forceContact->responsible != \Yii::$app->user->id)
                                throw new HttpException(403);
                    }
                    $forceContact->touch('updated_at');
                    $forceContact->force_update = true;
                    $logMainItemId = $forceContact->save();
                    $logItemId = $tag->delete();
                    LogHelper::setGroupId($logItemId, $logMainItemId);
                }

                foreach ($data['tags'] as $tag_id) {
                    $contact_tag = new ContactsTags();
                    $contact_tag->tag_id = $tag_id;
                    $contact_tag->contact_id = $id;

                    $forceContact = Contacts::findOne($id);
                    if (RbacHelper::can('contacts.update.only_own')) {
                        if($forceContact->responsible != \Yii::$app->user->id)
                            throw new HttpException(403);
                    }
                    if (RbacHelper::can('contacts.update.only_own_role')) {
                        if(isset($ids_r)) {
                            if(array_search($forceContact->responsible,$ids_r)===false)
                                throw new HttpException(403);
                        }
                        else
                            if($forceContact->responsible != \Yii::$app->user->id)
                                throw new HttpException(403);
                    }
                    $forceContact->touch('updated_at');
                    $forceContact->force_update = true;
                    $logMainItemId = $forceContact->save();
                    $logItemId = $contact_tag->save();
                    LogHelper::setGroupId($logItemId, $logMainItemId);
                }

                $contact = Contacts::findOne($id);
                $contact->updated_at = time();
                $contact->updated_by = \Yii::$app->user->id;
                $contact->save();
            }
        }

        return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts');
    }

    /**
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionSelectAll()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $search_data = \Yii::$app->request->get('search');
        $contact = Contacts::find()->Where(['contacts.tenant_id'=>\Yii::$app->user->identity->tenant->id]);
        if (!empty($search_data['filter']) && $search_data['filter'] == 'my_contacts') {
            $contact->andWhere(['responsible' => \Yii::$app->user->id]);
        }

        if (RbacHelper::can('contacts.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $contact->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('contacts.read.only_own')) {
            $contact->andWhere(['responsible' => \Yii::$app->user->id]);
        }

        if (!empty($search_data['search_text'])) {
            $search_arr = explode(' ', $search_data['search_text']);

            $contact->andWhere(['ilike', 'contacts_emails.value', $search_data['search_text']])
                ->orWhere(['ilike', 'contacts_phones.value', $search_data['search_text']]);

            foreach ($search_arr as $search) {
                $contact->orWhere(['ilike', 'first_name', $search])
                    ->orWhere(['ilike', 'last_name', $search])
                    ->orWhere(['ilike', 'middle_name', $search]);
            }
        }

        if (!empty($search_data['filter_email']))
            $contact->andWhere(['ilike', 'contacts_emails.value', $search_data['filter_email']]);

        if (!empty($search_data['filter_phone']))
            $contact->andWhere(['ilike', 'contacts_phones.value', $search_data['filter_phone']]);

        if (!empty($search_data['filter_tags'])) {
            $tags = Tags::find()->select(['tags.id', 'contacts_tags.contact_id'])
                ->where(['in', 'tags.id', $search_data['filter_tags']])
                ->andWhere('contacts_tags.contact_id IS NOT NULL')
                ->joinWith('contactsTags')->asArray()->all();

            $contacts_ids = array_column($tags, 'contact_id');

            if (isset($contacts_ids))
                $contact->andWhere(['in', 'contacts.id', $contacts_ids]);
        }

        $ids = array_column($contact->asArray()->all(),'id');
        return $ids;
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $id = (int) $id;
        $post = \Yii::$app->request->post();
        if(!empty($post))
        {
            $contact = Contacts::find()->where(['contacts.id'=>$id])
                ->andWhere(['contacts.tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->joinWith('contactsEmails')
                ->joinWith('contactsPhones')
                ->joinWith('company')
                ->joinWith('contactsPassports')
                ->joinWith('contactsMessengers')
                ->joinWith('contactsSites')
                ->joinWith('contactsSocials')
                ->joinWith('contactsTags')
                ->joinWith('contactsVisas')
                ->joinWith('responsible')
                ->with('residenceAddress')
                ->with('livingAddress')
                ->asArray()->one();

            if(null === $contact)
                throw new HttpException(404);

            if (RbacHelper::can('contacts.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
                if (!empty($roles)) {
                    $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
                }
            }
            if (RbacHelper::can('contacts.read.only_own')) {
                if($contact->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('contacts.read.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($contact->responsible,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($contact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }

            $new_contact = ContactsUtility::prepareContactsViewArray($contact);
            /* Post processing */
            ContactsPhones::deleteAll(['contact_id' => $id]);
            foreach ($post['contacts']['phones'] as $key => $phone){
                $default_phone_type = PhonesTypes::findOne(['default' => true]);

                if (null === $default_phone_type)
                    $default_phone_type = PhonesTypes::find()->one();
                $phones = new ContactsPhones();
                if( $phone['phone'] != '' ) {
                    $phones->contact_id = $id;
                    $phones->type_id = (null !== $default_phone_type ? $default_phone_type->id : null);
                    $phones->value = $phone['phone'];
                    $phones->save();
                }
            }
            if(ContactsUtility::postContactsContacts($post, $new_contact, $id))
            {

                $contact = Contacts::findOne(['id'=>$contact['id']]);

                if(null !== $contact) {
                    $contact->touch('updated_at');
                    $contact->save();
                }

                return $this->redirect('/ita/' . (!empty($post['m_type']) ? $post['m_type'] . '/view?id='.$post['m_id'] : 'contacts/view?id='.$id));
            }
        }

        $contact = Contacts::find()->where(['contacts.id'=>$id])
            ->andWhere(['contacts.tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->joinWith('contactsEmails')
            ->joinWith('contactsPhones')
            ->joinWith('company')
            ->joinWith('contactsPassports')
            ->joinWith('contactsMessengers')
            ->joinWith('contactsSites')
            ->joinWith('contactsSocials')
            ->joinWith('contactsTags')
            ->joinWith('contactsVisas')
            ->joinWith('responsible')
            ->with('residenceAddress')
            ->with('livingAddress')
            ->asArray()->one();
        
        if (null === $contact)
            throw new HttpException(404);

        $new_contact = ContactsUtility::prepareContactsViewArray($contact);

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        /* Default data */
        $types = ContactsUtility::prepareTypes($lang);
        
        $all_task_types = TasksTypes::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->orderBy('default DESC')
            ->asArray()
            ->all();

        return $this->render('view', [
            'contact' => $new_contact,
            'contacts_types' => $types,
            'all_task_types' => $all_task_types,
            'timeline' => [
                'all' => TimeLineUtility::getContactTimeLine($id),
                'notes' => TimeLineUtility::getContactNotes($id),
                'tasks' => TimeLineUtility::getContactTasks($id)
            ]
        ]);
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionAddPassport($id)
    {
        $id = intval($id);
        $post = \Yii::$app->request->post();
        $parsed = parse_str($post['data'], $query);
        $post['ContactPassport'] = $query['ContactPassport'];
        $json = array('ok' => false);

        if(!empty($post))
        {
            $contact = Contacts::find()->where(['contacts.id'=>$id])
                ->andWhere(['contacts.tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->asArray()->one();

            if(is_null($contact))
                throw new HttpException(404);

            if (RbacHelper::can('contacts.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
                if (!empty($roles)) {
                    $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
                }
            }
            if (RbacHelper::can('contacts.read.only_own')) {
                if($contact->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('contacts.read.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($contact->responsible,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($contact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if($error_list = ContactsUtility::addPassport($post, $id))
            {
                $contact = Contacts::findOne(['id'=>$id]);

                if (null !== $contact) {
                    $contact->touch('updated_at');
                    $contact->save();
                }

                $json = ['ok' => true];

                if (!empty($error_list['serial'])) {
                    $json = ['error' => $error_list['serial'], 'ok' => false];
                } elseif(!empty($error_list['nationality'])) {
                    $json = ['error' => $error_list['nationality'], 'ok' => false];
                }
            }

        }

        return $json;
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionAddVisa($id)
    {
        $id = intval($id);
        $post = \Yii::$app->request->post();
        if(!empty($post))
        {
            $contact = Contacts::find()->where(['contacts.id'=>$id])
                ->andWhere(['contacts.tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->asArray()->one();

            if(is_null($contact))
                throw new HttpException(404);

            if (RbacHelper::can('contacts.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
                if (!empty($roles)) {
                    $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
                }
            }
            if (RbacHelper::can('contacts.read.only_own')) {
                if($contact->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('contacts.read.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($contact->responsible,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($contact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }

            if(ContactsUtility::addVisa($post, $id))
            {
                $contact = Contacts::findOne(['id'=>$id]);
                if(!is_null($contact)) {
                    $contact->touch('updated_at');
                    $contact->save();
                }
                return $this->redirect('view?id='.$id);
            }
        }

    }


    /**
     * @param $id
     *
     * @return
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id,$redirect=true)
    {
        $model = $this->findModel($id);

        if(is_null($model))
            throw new HttpException(404);

        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        if (RbacHelper::can('contacts.delete.only_own')) {
            if($model->responsible != \Yii::$app->user->id)
                throw new HttpException(403);
        }
        if (RbacHelper::can('contacts.delete.only_own_role')) {
            if(isset($ids_r)) {
                if(array_search($model->responsible,$ids_r)===false)
                    throw new HttpException(403);
            }
            else
                if($model->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if($redirect)
            return $this->redirect('index');
        else
            return [];
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionBulkDelete()
    {
        $ids = \Yii::$app->request->post('ids', false);

        if (empty($ids))
            throw new BadRequestHttpException();

        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }
        foreach ($ids as $id)
        {
            $model = $this->findModel($id);
            if (RbacHelper::can('contacts.delete.only_own')) {
                if($model->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('contacts.delete.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($model->responsible,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($model->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }
            $model->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @return array|boolean
     * @throws \yii\web\HttpException
     */
    public function actionChangeResponsible()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }
        $post = \Yii::$app->request->post();
        $contact_id = $post['contact_id'];
        $responsible_id = $post['responsible_id'];

        // Work with database...
        $contact = Contacts::find()->where(['id'=>$contact_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);
        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $contact->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('contacts.update.only_own')) {
            $contact->andWhere(['responsible' => \Yii::$app->user->id]);
        }
        $contact = $contact->one();
        if(!is_null($contact))
        {
            $user = User::find()->where(['id'=>$responsible_id])->one();
            if(!is_null($user))
            {
                $agent = AgentsMapping::find()->where(['user_id'=>$responsible_id])
                    ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one();
                    if(!is_null($agent))
                    {
                        $contact->responsible = $responsible_id;
                        $contact->save();
                        $response = [
                            'result' => 'success', // success|error
                            'message' => TranslationHelper::getTranslation('saved', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
                        ];
                        \Yii::$app->response->format = Response::FORMAT_JSON;
                        return $response;
                    }
            }
        }
        /* Create response */
        $response = [
            'result' => 'error', // success|error
            'message' => TranslationHelper::getTranslation('error', Yii::$app->user->identity->tenant->language->iso, 'Saved')
        ];
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;

    }

    /**
     * @param bool $email
     * @param bool $phone
     *
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionGetContacts($email = false, $phone = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = [];

        $query = Contacts::find();
        if (RbacHelper::can('contacts.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $query->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('contacts.read.only_own')) {
            $query->andWhere(['responsible' => \Yii::$app->user->id]);
        }
        $query->joinWith('contactsEmails')->joinWith('contactsPhones');

        if (!empty($email)) {
            $query->andWhere(['like', 'contacts_emails.value', $email])
                ->orWhere(['like', 'contacts_emails.value', strtolower($email)]);
        }

        if (!empty($phone)) {
            $query->andWhere(['like', 'contacts_phones.value', $phone])
                ->orWhere(['like', 'contacts_phones.value', str_replace(['(', ')', '+', '-'], '', $phone)]);
        }

        $query->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        $contacts = $query->all();

        if (!is_null($contacts)) {
            foreach ($contacts as $contact) {
                $data[] = [
                    'id' => $contact->id,
                    'email' => implode(', ', array_column($contact->contactsEmails, 'value')),
                    'phone' => implode(', ', array_column($contact->contactsPhones, 'value')),
                    'name' => trim($contact->last_name . ' ' . $contact->first_name . ' ' . $contact->middle_name),
                    'first_name' => $contact->first_name,
                    'last_name' => $contact->last_name
                ];
            }
        }

        return $data;
    }

    /**
     * @param integer $id
     * @return Contacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contacts::find()->where(['id'=>$id])->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $passport_id
     *
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDeletePassport($passport_id)
    {
        $id = intval($passport_id);
        $passport = ContactsPassports::find()->where(['id'=>$id])->with('contact')->one();
        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }
        if(!is_null($passport))
        {
            if($passport->contact->tenant_id == \Yii::$app->user->identity->tenant->id)
            {
                $forceContact = Contacts::findOne($passport->contact->id);
                if (RbacHelper::can('contacts.update.only_own')) {
                    if($forceContact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
                }
                if (RbacHelper::can('contacts.update.only_own_role')) {
                    if(isset($ids_r)) {
                        if(array_search($forceContact->responsible,$ids_r)===false)
                            throw new HttpException(403);
                    }
                    else
                        if($forceContact->responsible != \Yii::$app->user->id)
                            throw new HttpException(403);
                }
                $forceContact->touch('updated_at');
                $forceContact->force_update = true;
                $logMainItemId = $forceContact->save();
                $logItemId = $passport->delete();
                LogHelper::setGroupId($logItemId, $logMainItemId);
                if($logItemId)
                    return $this->redirect(Url::to(['view','id'=>$passport->contact_id]));
                throw new HttpException(500,json_encode($passport->getErrors()));
            }
            throw new HttpException(403);
        }
        throw new HttpException(404,'Passport not found!');
    }

    /**
     * @param $visa_id
     *
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDeleteVisa($visa_id)
    {
        $id = intval($visa_id);
        $visa = ContactsVisas::find()->where(['id'=>$id])->with('contact')->one();
        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }
        if(!is_null($visa))
        {
            if($visa->contact->tenant_id == \Yii::$app->user->identity->tenant->id)
            {
                $forceContact = Contacts::findOne($visa->contact->id);
                if (RbacHelper::can('contacts.update.only_own')) {
                    if($forceContact->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
                }
                if (RbacHelper::can('contacts.update.only_own_role')) {
                    if(isset($ids_r)) {
                        if(array_search($forceContact->responsible,$ids_r)===false)
                            throw new HttpException(403);
                    }
                    else
                        if($forceContact->responsible != \Yii::$app->user->id)
                            throw new HttpException(403);
                }
                $forceContact->touch('updated_at');
                $forceContact->force_update = true;
                $logMainItemId = $forceContact->save();
                $logItemId = $visa->delete();
                LogHelper::setGroupId($logItemId, $logMainItemId);
                if($logItemId)
                    return $this->redirect(Url::to(['view','id'=>$visa->contact_id]));
                throw new HttpException(500,json_encode($visa->getErrors()));
            }
            throw new HttpException(403);
        }
        throw new HttpException(404,'Passport not found!');
    }

    /**
     * @param $contact_id
     *
     * @return string
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAddNote($contact_id)
    {
        if (!RbacHelper::can('companies.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $contact_id = (int) $contact_id;
        $post = \Yii::$app->request->post();

        if (empty($post) || empty($post['value']))
            throw new HttpException(404);

        $contact = $this->findModel($contact_id);

        if (null === $contact)
            throw new HttpException(404);

        $note = new ContactsNotes();
        $note->value = $post['value'];
        $note->contact_id = $contact->id;

        if (!$note->save())
            throw new BadRequestHttpException('Note not saved!');

        $item = $this->renderPartial('partlets/timeline_item', [
            'item' => TimeLineUtility::prepareNote($note),
            'is_first' => true
        ]);

        \Yii::$app->response->format = Response::FORMAT_HTML;

        return $item;
    }

    /**
     * @param int $id
     *
     * @return \app\models\ContactsNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionEditNote($id)
    {
        $id = (int) $id;
        $response = \Yii::$app->request->post();

        if (empty($response) || empty($response['type']) || empty($response['value']))
            throw new HttpException(404);

        /** @var ContactsNotes $note */
        $note = $response['type'] === 'request' ? RequestNotes::findOne($id) : ContactsNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('contacts.update.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('contacts.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        if (!empty($note->contact_id) && $note->contact->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->value = $response['value'];
        $note->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id
     *
     * @return \app\models\ContactsNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionGetNote($id)
    {
        $id = (int) $id;

        $post = \Yii::$app->request->post();

        if (empty($post) || empty($post['type']))
            throw new HttpException(404);

        /** @var ContactsNotes $note */
        $note = $post['type'] === 'request' ? RequestNotes::findOne($id) : ContactsNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('contacts.read.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('contacts.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        if (!empty($note->contact_id) && $note->contact->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id
     *
     * @throws \yii\web\HttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteNote($id)
    {
        $id = (int) $id;

        $post = \Yii::$app->request->post();

        if (empty($post) || empty($post['type']))
            throw new HttpException(404);

        /** @var ContactsNotes $note */
        $note = $post['type'] === 'request' ? RequestNotes::findOne($id) : ContactsNotes::findOne($id);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('contacts.delete.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('contacts.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        if (!empty($note->contact_id) && $note->contact->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->delete();
    }

    /**
     * @param $id
     * @param $type
     * @param $offset
     *
     * @param $search
     *
     * @return string
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionTimeLineLoadMore($id, $type, $offset, $search)
    {
        $id = (int) $id;

        $contact = $this->findModel($id);

        if (null === $contact)
            throw new HttpException(404);

        $timelines = [];

        if ($type === 'all') {
            $timelines = TimeLineUtility::getContactTimeLine($id, $offset, $search);
        } elseif ($type === 'notes') {
            $timelines = TimeLineUtility::getContactNotes($id, $offset);
        } elseif ($type === 'tasks') {
            $timelines = TimeLineUtility::getContactTasks($id, $offset);
        }

        $templates = '';

        foreach ($timelines as $timeline) {
            $templates .= $this->renderPartial('partlets/timeline_item', [
                'item' => $timeline,
                'is_first' => false
            ]);
        }

        \Yii::$app->response->format = Response::FORMAT_HTML;

        return $templates;
    }

    /**
     * @param $id
     *
     * @param $search
     *
     * @return string
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionTimeLineSearch($id, $search)
    {
        $id = (int) $id;

        $contact = $this->findModel($id);

        if (null === $contact)
            throw new HttpException(404);

        $timelines = TimeLineUtility::getContactTimeLine($id, 0, $search);

        $templates = '';

        foreach ($timelines as $key => $timeline) {
            $templates .= $this->renderPartial('partlets/timeline_item', [
                'item' => $timeline,
                'is_first' => $key === 0
            ]);
        }

        \Yii::$app->response->format = Response::FORMAT_HTML;

        return $templates;
    }

    /**
     * @param $contact_id
     * @return string
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionAddTask($contact_id)
    {
        $contact = $this->findModel($contact_id);

        if (null === $contact)
            throw new HttpException(404);

        $model = new Tasks();

        if ($model->load(\Yii::$app->request->post())) {

            if (empty($model->due_date))
                $model->due_date = time();

            if (empty($model->email_reminder))
                $model->email_reminder = $model->due_date - (60 * 60 * 3); // 3 hour

            if (empty($model->tenant_id))
                $model->tenant_id = \Yii::$app->user->identity->tenant->id;

            if (empty($model->assigned_to_id))
                $model->assigned_to_id = \Yii::$app->user->id;

            if (empty($model->type_id)) {
                $type = TasksTypes::findOne(['default' => true]);

                if (null !== $type) {
                    $model->type_id = $type->id;
                }
            }

            $model->contact_id = $contact->id;

            if ($model->save()) {
                $task_type = TasksTypes::find()
                    ->where(['id' => $model->type_id])
                    ->translate(\Yii::$app->user->identity->tenant->language->iso)
                    ->one();
                $task_type = $task_type !== null ? [$task_type->id => $task_type] : [];

                $task = Tasks::find()
                    ->where(['id' => $model->id])
                    ->with('user')->asArray()->one();

                $item = $this->renderPartial('partlets/timeline_item', [
                    'item' => TimeLineUtility::prepareTask($task, $task_type),
                    'is_first' => true
                ]);

                \Yii::$app->response->format = Response::FORMAT_HTML;

                return $item;
            }
        }

        throw new BadRequestHttpException('Task not save!');
    }
}