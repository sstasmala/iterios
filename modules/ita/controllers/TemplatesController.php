<?php

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\models\DeliveryEmailTemplates;
use app\models\DeliveryPlaceholders;
use app\models\DeliveryPlaceholderTypes;
use app\models\DeliverySmsTemplates;
use app\models\DeliveryTypes;
use app\models\DocumentPlaceholders;
use app\models\DocumentsTemplate;
use app\models\DocumentsType;
use app\models\Languages;
use app\models\Placeholders;
use app\models\PlaceholdersDelivery;
use app\models\PlaceholdersDeliveryTypes;
use app\models\PlaceholdersDocument;
use app\models\Suppliers;
use app\models\Translations;
use app\modules\ita\controllers\base\BaseController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TemplatesController extends BaseController
{
    // Documents actions
    public function actionDocumentsIndex()
    {
        $documents_type = DocumentsType::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $suppliers = Suppliers::find()
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $languages = Languages::find()
            ->where(['activated' => 1])
            ->orderBy(['id' => SORT_ASC])->asArray()->all();

        return $this->render('documents/index', [
            'documents_types' => $documents_type,
            'suppliers' => $suppliers,
            'languages' => $languages
        ]);
    }

    /**
     * Email and sms actions
     *
     * @return string
     */
    public function actionEmailAndSmsIndex()
    {
        $delivery_types = DeliveryTypes::find()
            ->orderBy(['id' => SORT_ASC])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();

        return $this->render('email-and-sms/index', [
            'delivery_types' => $delivery_types,
        ]);
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetEmailAndSmsTemplates()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $templates_email = DeliveryEmailTemplates::find()
            ->select([
                'delivery_email_templates.id',
                'delivery_email_templates.name',
                'delivery_email_templates.type',
                'delivery_email_templates.delivery_type_id',
                'delivery_types.name as delivery_type_name',
                'delivery_types.for as for',
                'delivery_email_templates.created_by',
                'delivery_email_templates.created_at',
                'delivery_email_templates.updated_at'])
            ->where(['and', ['tenant_id' => null], ['status' => 1]])
            ->orWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->joinWith('deliveryType')
            ->translate(\Yii::$app->user->identity->tenant->language->iso,
                ['name' => 'delivery_email_templates.name'],
                ['name']);

        $templates_sms = DeliverySmsTemplates::find()
            ->select([
                'delivery_sms_templates.id',
                'delivery_sms_templates.name',
                'delivery_sms_templates.type',
                'delivery_sms_templates.delivery_type_id',
                'delivery_types.name as delivery_type_name',
                'delivery_types.for as for',
                'delivery_sms_templates.created_by',
                'delivery_sms_templates.created_at',
                'delivery_sms_templates.updated_at'])
            ->where(['and', ['tenant_id' => null], ['status' => 1]])
            ->orWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->joinWith('deliveryType')
            ->translate(\Yii::$app->user->identity->tenant->language->iso,
                ['name' => 'delivery_sms_templates.name'],
                ['name']);

        $templates_email->union($templates_sms, true);

        $templates = (new \yii\db\Query())->select('*')->from([$templates_email]);

        if (!empty($search_data['filter']) && $search_data['filter'] === 'system') {
            $templates->andWhere(['type' => DeliveryEmailTemplates::SYSTEM_TYPE]);
        }

        if (!empty($search_data['filter']) && $search_data['filter'] === 'my') {
            $templates->andWhere(['and',
                ['type' => DeliveryEmailTemplates::PUBLIC_TYPE],
                ['created_by' => \Yii::$app->user->id]
            ]);
        }

        if (!empty($search_data['type'])) {
            $templates->andWhere(['delivery_type_id' => (int) $search_data['type']]);
        }

        if (!empty($search_data['search_text'])) {
            $templates->andWhere(['ilike', 'name', $search_data['search_text']]);
        }

        $meta['total'] = $templates->count();

        $templates->orderBy(['created_at' => SORT_DESC])
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));

        $templates = $templates->all();

        $delivery_types = DeliveryTypes::find()
            ->where(['in', 'id', array_column($templates, 'delivery_type_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->orderBy(['id' => SORT_ASC])->asArray()->all();
        $delivery_types = array_combine(array_column($delivery_types, 'id'), $delivery_types);

        foreach ($templates as $i => $template) {
            if ($templates[$i]['name'] === null)
                $templates[$i]['name'] = \app\helpers\TranslationHelper::getTranslation('not_set', \Yii::$app->user->identity->tenant->language->iso);

            $templates[$i]['delivery_type_name'] = $delivery_types[$template['delivery_type_id']]['name'];

            $templates[$i]['created_at'] = DateTimeHelper::convertTimestampToDateTime($template['created_at']);
            $templates[$i]['updated_at'] = DateTimeHelper::convertTimestampToDateTime($template['updated_at']);
        }

        return ['meta' => $meta, 'data' => $templates];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetDocumentsTemplates()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $templates = DocumentsTemplate::find()
            ->with('lang');

        if (!empty($search_data['filter']) && $search_data['filter'] === 'system') {
            $templates->andWhere(['type' => DocumentsTemplate::SYSTEM_TYPE]);
        }

        if (!empty($search_data['filter']) && $search_data['filter'] === 'my') {
            $templates->andWhere(['and',
                ['type' => DocumentsTemplate::PUBLIC_TYPE],
            ]);
        }

        if (!empty($search_data['type'])) {
            $templates->andWhere(['documents_type_id' => (int) $search_data['type']]);
        }
        if (!empty($search_data['supplier'])) {
            $templates->andWhere(['suppliers_id' => (int) $search_data['supplier']]);
        }

        if (!empty($search_data['search_text'])) {
            $templates->andWhere(['ilike', 'title', $search_data['search_text']]);
        }

        $meta['total'] = $templates->count();

        $templates->orderBy(['created_at' => SORT_DESC])
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));

        $templates = $templates->asArray()->all();

        if ($templates === null)
            return ['meta' => $meta, 'data' => []];

        $documents_type = DocumentsType::find()
            ->where(['in', 'id', array_column($templates, 'documents_type_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $documents_type = array_combine(array_column($documents_type, 'id'), $documents_type);

        $suppliers = Suppliers::find()
            ->where(['in', 'id', array_column($templates, 'suppliers_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $suppliers = array_combine(array_column($suppliers, 'id'), $suppliers);

        foreach ($templates as $i => $template) {
            if ($templates[$i]['title'] === null)
                $templates[$i]['title'] = \app\helpers\TranslationHelper::getTranslation('not_set', \Yii::$app->user->identity->tenant->language->iso);

            $templates[$i]['documents_type_name'] = $documents_type[$template['documents_type_id']]['name'];
            $templates[$i]['document_supplier_name'] = !empty($suppliers) ? $suppliers[$template['suppliers_id']]['name'] : '';
            $templates[$i]['created_at'] = DateTimeHelper::convertTimestampToDateTime($template['created_at']);
            $templates[$i]['updated_at'] = DateTimeHelper::convertTimestampToDateTime($template['updated_at']);
        }

        return ['meta' => $meta, 'data' => $templates];
    }

    /**
     * @param $id
     *
     * @param $template_for
     *
     * @return array
     * @throws \HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetTemplateById($id, $template_for)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = ($template_for === DeliveryTypes::FOR_EMAIL
            ? DeliveryEmailTemplates::find()
            : DeliverySmsTemplates::find());

        $model->where(['id' => (int)$id])
            ->translate(\Yii::$app->user->identity->tenant->language->iso);
        $model = $model->one();

        if ($model === null || ($model::PUBLIC_TYPE === $model->type && $model->tenant_id !== \Yii::$app->user->identity->tenant->id))
            throw new \HttpException(400, 'Template not found!');
        
        $template = [
            'id' => $model->id,
            'name' => $model->name,
            'delivery_type_id' => $model->delivery_type_id,
            'for' => $model->deliveryType->for,
            'subject' => $template_for === DeliveryTypes::FOR_EMAIL ? $model->subject : '',
            'body' => $model->body,
            'default' => $model->is_default
        ];

        return $template;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \HttpException
     */
    public function actionGetTemplateDocById($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = DocumentsTemplate::find();

        $model->where(['id' => (int)$id]);
        $model = $model->one();

        if ($model === null || ($model::PUBLIC_TYPE === $model->type && $model->tenant_id !== \Yii::$app->user->identity->tenant->id))
            throw new \HttpException(400, 'Template not found!');

        $type = DocumentsType::findOne((int) $model['documents_type_id']);

        $supplier_current = Suppliers::find()->translate(\Yii::$app->user->identity->tenant->language->iso)->where(['id' => $model->suppliers_id])->one();

        $template = [
            'id' => $model->id,
            'title' => $model->title,
            'documents_type_id' => $type->id,
            'language' => $model->languages,
            'body' => $model->body,
            'supplier_id' => $model->suppliers_id,
            'supplier_name' => $supplier_current->name
        ];

        return $template;
    }

    /**
     * @param $id
     * @param $template_for
     *
     * @return bool
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteTemplateById($id, $template_for)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = ($template_for === DeliveryTypes::FOR_EMAIL
            ? DeliveryEmailTemplates::findOne($id)
            : DeliverySmsTemplates::findOne($id));

        if ($model === null || $model->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Template not found!');

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteTemplateByIdDocuments($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = DocumentsTemplate::findOne($id);

        if ($model === null)
            throw new \HttpException(400, 'Template not found!');

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetEmailAndSmsPlaceholders()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $placeholders = Placeholders::find()
            ->where(['fake' => false]);

        $ph_delivery_ids = PlaceholdersDelivery::find()
            ->select('placeholder_id');

        if (!empty($search_data['type'])) {
            $placeholders_type_ids = PlaceholdersDeliveryTypes::find()
                ->select(['placeholder_delivery_id'])
                ->where(['delivery_type_id' => (int) $search_data['type']]);

            $ph_delivery_ids->andWhere(['in', 'id', $placeholders_type_ids]);
        }

        $ph_delivery_ids = $ph_delivery_ids->column();

        if (!empty($search_data['search_text'])) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $search_data['search_text']])
                ->orFilterWhere(['ilike', 'value',  $search_data['search_text']])
                ->andWhere('"key" ILIKE :table', [':table'=>"placeholders.short_code.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $intersect_ids = array_intersect($translate, $ph_delivery_ids);

            $placeholders->andWhere(['or',
                ['in', 'id', $intersect_ids],
                ['ilike', 'description', $search_data['search_text']]
            ]);
        } else {
            $placeholders->andWhere(['in', 'id', $ph_delivery_ids]);
        }

        $meta['total'] = $placeholders->count();

        $placeholders->orderBy(['short_code' => SORT_ASC])
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));

        $placeholders = $placeholders->all();

        return ['meta' => $meta, 'data' => $placeholders];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDocumentPlaceholders()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $placeholders = Placeholders::find()
            ->where(['fake' => false]);

        $ph_doc_ids = PlaceholdersDocument::find()
            ->select('placeholder_id')->column();

        if (!empty($search_data['search_text'])) {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $search_data['search_text']])
                ->orFilterWhere(['ilike', 'value',  $search_data['search_text']])
                ->andWhere('"key" ILIKE :table', [':table'=>"placeholders.short_code.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();

            $intersect_ids = array_intersect($translate, $ph_doc_ids);

            $placeholders->andWhere(['or',
                ['in', 'id', $intersect_ids],
                ['ilike', 'default', $search_data['search_text']]
            ]);
        } else {
            $placeholders->andWhere(['in', 'id', $ph_doc_ids]);
        }

        $meta['total'] = $placeholders->count();

        $placeholders->orderBy(['short_code' => SORT_ASC])
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));

        $placeholders = $placeholders->all();

        return ['meta' => $meta, 'data' => $placeholders];

    }

    /**
     * @param string $id
     *
     * @param string $template_for
     *
     * @return bool
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Throwable
     */
    public function actionSaveEmailAndSmsTemplate($id = '', $template_for = '')
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = \Yii::$app->request->post();

        if (empty($data['template-type']))
            throw new BadRequestHttpException('Template type is empty!');

        $type = DeliveryTypes::findOne((int) $data['template-type']);

        if ($type === null)
            throw new BadRequestHttpException('Template type not found!');

        $model = ($type->for === DeliveryTypes::FOR_EMAIL ? new DeliveryEmailTemplates() : new DeliverySmsTemplates());

        if (!empty($id) && !empty($template_for)) {
            if ($template_for !== $type->for) {
                $old_model = ($template_for === DeliveryTypes::FOR_EMAIL
                    ? DeliveryEmailTemplates::findOne((int)$id)
                    : DeliverySmsTemplates::findOne((int)$id));

                if ($old_model !== null)
                    $old_model->delete();
            } else {
                $model = ($type->for === DeliveryTypes::FOR_EMAIL
                    ? DeliveryEmailTemplates::findOne((int)$id)
                    : DeliverySmsTemplates::findOne((int)$id));
            }
        }

        $model->delivery_type_id = $type->id;
        $model->type = $model::PUBLIC_TYPE;
        $model->name = !empty($data['template-name'])
            ? $data['template-name']
            : \app\helpers\TranslationHelper::getTranslation('default_template_name', \Yii::$app->user->identity->tenant->language->iso);

        if ($type->for === DeliveryTypes::FOR_EMAIL)
            $model->subject = $data['template-theme'];

        $model->body = $data['template-body'];
        $model->status = true;
        $model->tenant_id = \Yii::$app->user->identity->tenant->id;
        $model->is_default = !empty($data['is_default_template']) ? true : false;

        $model->saveWithLang(\Yii::$app->user->identity->tenant->language->iso);

        return true;
    }

    public function actionSaveDocumentsTemplate($id = '')
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = \Yii::$app->request->post();

        if (empty($data['template-type']))
            throw new BadRequestHttpException('Template type is empty!');

        if (empty($data['language']))
            throw new BadRequestHttpException('Language is empty!');

        if (empty($data['supplier']))
            throw new BadRequestHttpException('Supplier is empty!');

        $type = DocumentsType::findOne((int) $data['template-type']);

        if ($type === null)
            throw new BadRequestHttpException('Template type not found!');

        $model =  new DocumentsTemplate();

        if (!empty($id)) {
            $old_model = DocumentsTemplate::findOne((int)$id);
            if ($old_model !== null)
                $old_model->delete();
        }

        $model->documents_type_id = $type->id;
        $model->type = $model::PUBLIC_TYPE;
        $model->languages = $data['language'];
        $model->suppliers_id = $data['supplier'];
        $model->title = !empty($data['template-name'])
            ? $data['template-name']
            : \app\helpers\TranslationHelper::getTranslation('default_template_name', \Yii::$app->user->identity->tenant->language->iso);

        $model->body = $data['template-body'];
        $model->tenant_id = \Yii::$app->user->identity->tenant->id;
        $model->save();

        return true;
    }
}
