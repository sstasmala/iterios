<?php
/**
 * RolesController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\RbacHelper;
use app\modules\ita\controllers\base\BaseController;
use yii\web\HttpException;
use yii\web\Response;

class RolesController extends BaseController
{
    /**
     * @param $action
     * @return bool
     * @throws HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        if(!$parent) return $parent;
        if(!RbacHelper::can('settings.read.access') && !RbacHelper::owner())
            throw new HttpException(403);

        return $parent;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetData()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $roles = RbacHelper::getRolesByTenant(\Yii::$app->user->identity->tenant->id);
        $roles_array = [];
        foreach ($roles as $role)
        {
            $roles_array[] = [
                'id' => $role->name,
                'name' => explode('.',$role->name)[1],
                'users' => count(RbacHelper::getAssignedUsers($role->name)),
            ];
        }
        return $roles_array;
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionCreate()
    {
        $role = \Yii::$app->request->post('Role',null);

        if(!is_null($role) && !empty(trim($role['name']))) {
            $tenant = \Yii::$app->user->identity->tenant;
            $role['name'] = trim($role['name']);

            if ($role['name'] == 'system_admin')
                throw new HttpException(404);

            $r = RbacHelper::getRole($tenant->id.'.'.$role['name']);

            if (is_null($r))
                RbacHelper::createRole($tenant->id.'.'.$role['name']);
            else
                return $this->render('create');

            $r = RbacHelper::getRole($tenant->id.'.'.$role['name']);

            foreach ($role['permissions'] as $nm => $p)
            {
                foreach ($p as $k => $v)
                {
                    $perm_name = $tenant->id.'.'.$nm.'.'.$k.'.'.$v;
                    $pr = RbacHelper::getPermission($perm_name);
                    if(is_null($pr))
                        RbacHelper::createPermission($perm_name);
                    RbacHelper::assignPermission($r->name,$perm_name);
                }
            }
            return $this->redirect('index');
        }

        /* Render view */
        return $this->render('create');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionUpdate()
    {
        $tenant = \Yii::$app->user->identity->tenant;
        $name = \Yii::$app->request->get('name',null);

        if (is_null($name))
            throw new HttpException(403,'Role name is required!');

        $parts = explode('.',$name);
        if($tenant->id != $parts[0])
            throw new HttpException(404);
        if($parts[0] == 'system_admin')
            throw new HttpException(404);
        $role = RbacHelper::getRole($name);
        if(is_null($role))
            throw new HttpException(404);
        $data = \Yii::$app->request->post('Role',null);
        if(!is_null($data) && !empty(trim($data['name'])))
        {
            //$data['name'] = preg_replace('/[^A-Za-z0-9\ ]/', '', $data['name']);
            $data['name'] = trim($data['name']);
            $ext = RbacHelper::getRole($tenant->id.'.'.$data['name']);
            if(is_null($ext) && $data['name'] != 'system_admin')
            {
                $role->name = $tenant->id.'.'.$data['name'];
                if(RbacHelper::updateItem($name,$role))
                    $name = $role->name;
            }
            $existed_permissions = RbacHelper::getAssignedPermissions($name);
            foreach ($existed_permissions as $ep)
            {
                RbacHelper::unassignPermission($name,$ep->name);
            }
            foreach ($data['permissions'] as $nm => $p)
            {
                foreach ($p as $k => $v)
                {
                    $perm_name = $tenant->id.'.'.$nm.'.'.$k.'.'.$v;
                    $pr = RbacHelper::getPermission($perm_name);
                    if(is_null($pr))
                        RbacHelper::createPermission($perm_name);
                    RbacHelper::assignPermission($name,$perm_name);
                }
            }
            return $this->redirect('index');
        }
        $role_permissions = \app\helpers\TenantHelper::getPermissionsScheme(\Yii::$app->user->identity->tenant->language->iso);

        foreach ($role_permissions as $i => $group)
        {
            foreach($group as $m => $module)
            {
                $mod_name = $module['permission_name'];
                foreach ($module as $act => $pr)
                    if(is_array($pr) && array_search($act, ['create', 'update', 'read', 'delete']) !== false)
                        foreach ($pr as $p => $permission) {
                            if (RbacHelper::isAssigned($name, $tenant->id . '.' . $mod_name . '.' . $act . '.' . $permission['value']))
                                $role_permissions[$i][$m][$act][$p]['checked'] = true;
                            else
                                $role_permissions[$i][$m][$act][$p]['checked'] = false;
                        }
            }
        }
        return $this->render('update', compact('role_permissions', 'name'));
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete()
    {
        $tenant = \Yii::$app->user->identity->tenant;
        $name = \Yii::$app->request->get('name',null);

        if (is_null($name))
            throw new HttpException(403,'Role name is required!');

        $parts = explode('.', $name);

        if ($tenant->id != $parts[0])
            throw new HttpException(404);

        $role = RbacHelper::getRole($name);

        if (is_null($role))
            throw new HttpException(404);

        $role_users = RbacHelper::getAssignedUsers($name);

        if (empty($role_users))
            RbacHelper::deleteRole($name);

        return $this->redirect('index');
    }
}