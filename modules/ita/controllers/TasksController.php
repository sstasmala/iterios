<?php
/**
 * TasksController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\components\notifications\Notification;
use app\helpers\DateTimeHelper;
use app\helpers\RbacHelper;
use app\helpers\TranslationHelper;
use app\models\Contacts;
use app\models\NotificationsTrigger;
use app\models\OrderReminders;
use app\models\Requests;
use app\models\Tasks;
use app\models\TasksTypes;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use app\utilities\calendar\CalendarUtility;
use PHPUnit\Util\Type;
use yii\db\ActiveQuery;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TasksController extends BaseController
{
    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        switch ($action->id)
        {
            case 'index':if(!RbacHelper::can('tasks.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'get-task':if(!RbacHelper::can('tasks.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'get-data':if(!RbacHelper::can('tasks.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'create':if(!RbacHelper::can('tasks.create.access') && !RbacHelper::owner())throw new HttpException(403);break;
            case 'update':if(!RbacHelper::can('tasks.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'change-status':if(!RbacHelper::can('tasks.update',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'delete':if(!RbacHelper::can('tasks.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
            case 'bulk-delete':if(!RbacHelper::can('tasks.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
        }
        return $parent;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];

        /* Set data */
        $data = [];

        $default_task_type = TasksTypes::find()
            ->where(['default' => true])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->one();

        return $this->render('index', compact('data', 'default_task_type'));
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $tasks = Tasks::find()
            ->andWhere(['tasks.tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->joinWith(['contact', 'company']);

        if (empty($search_data['filter']))
            $tasks->andWhere(['status' => false]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'task-open')
            $tasks->andWhere(['>', 'due_date', time()])->andWhere(['status' => false]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'task-overdue')
            $tasks->andWhere(['<', 'due_date', time()])->andWhere(['status' => false]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'task-completed')
            $tasks->andWhere(['status' => true]);

        if (!empty($search_data['filter_title']) || !empty($search_data['search_text']))
            $tasks->andWhere(['ilike', 'title', !empty($search_data['filter_title']) ? $search_data['filter_title'] : trim($search_data['search_text'])]);

        if (!empty($search_data['filter_responsible']))
            $tasks->andWhere(['assigned_to_id' => $search_data['filter_responsible']]);

        if (RbacHelper::can('tasks.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $tasks->andWhere(['in','assigned_to_id',$ids]);
            }
        }

        if (RbacHelper::can('tasks.read.only_own')) {
            $tasks->andWhere(['assigned_to_id' => \Yii::$app->user->id]);
        }

        if (!empty($search_data['filter_author']))
            $tasks->andWhere(['created_by' => $search_data['filter_author']]);

        if (!empty($search_data['filter_type']))
            $tasks->andWhere(['type_id' => $search_data['filter_type']]);

        if (!empty($search_data['completion']['start']) && !empty($search_data['completion']['end']))
            $tasks->andWhere(['>=', 'due_date', $search_data['completion']['start']])->andWhere(['<=', 'due_date', $search_data['completion']['end']]);

        if (!empty($search_data['create']['start']) && !empty($search_data['create']['end']))
            $tasks->andWhere(['>=', 'created_at', $search_data['create']['start']])->andWhere(['<=', 'created_at', $search_data['create']['end']]);

        if (!empty($search_data['update']['start']) && !empty($search_data['update']['end']))
            $tasks->andWhere(['>=', 'updated_at', $search_data['update']['start']])->andWhere(['<=', 'updated_at', $search_data['update']['end']]);

        $meta['total'] = $tasks->count();

        $tasks->orderBy('created_at DESC')
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $tasks = $tasks->all();
        $new_tasks = [];

        foreach ($tasks as $task) {
            $data = $task->getAttributes();

            $full_name = !is_null($task->assigned_to_id) ? $task->assignedTo->last_name . ' ' . $task->assignedTo->first_name . ' ' . $task->assignedTo->middle_name : '';

            $data['assigned_to'] = trim($full_name);
            $data['type'] = !is_null($task->type_id) ?
                $task->type->translate(\Yii::$app->user->identity->tenant->language->iso)->value : '';
            $data['overdue'] = !$task->status && !empty($task->due_date) && $task->due_date < time() ? true : false;
            $data['due_date'] = empty($task->due_date) ? '' : DateTimeHelper::convertTimestampToDateTime($task->due_date, \Yii::$app->user->identity->tenant->date_format . ' ' . str_replace(':s', '', \Yii::$app->user->identity->tenant->time_format));
            $data['contact_fn'] = $task->contact['first_name'] . ' ' . $task->contact['last_name'] . ' ' . $task->contact['middle_name'];
            $data['company_t'] = $task->company['name'];
            $data['created_at'] = DateTimeHelper::convertTimestampToDateTime($task->created_at);
            $data['updated_at'] = DateTimeHelper::convertTimestampToDateTime($task->updated_at);

            $new_tasks[] = $data;
        }

        return ['meta' => $meta, 'data' => $new_tasks];
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionGetTask($id) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $task = Tasks::findOne($id);
        if (RbacHelper::can('tasks.read.only_own')) {
            if($task->assigned_to_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }
        if (RbacHelper::can('tasks.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                if(array_search($task->assigned_to_id,$ids)===false)
                    throw new HttpException(403);
            }
            else
                if($task->assigned_to_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        $task_types = TasksTypes::find()
            ->select(['id', 'value'])
            ->where(['id' => $task->type_id])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->one();
        $assigned_to = User::find()
            ->select(['id', 'first_name', 'last_name'])
            ->where(['id' => $task->assigned_to_id])
            ->asArray()->one();

        $order = null;

        if (!empty($task->order)) {
            $order = [
                'id' => $task->order->id,
                'name' => $task->order->contact->last_name . ' ' . $task->order->contact->first_name
            ];
        }

        return [
            'task' => $task,
            'type' => $task_types,
            'user' => $assigned_to,
            'contact' => $task->contact,
            'company' => $task->company,
            'order' => $order
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new Tasks();

        if ($model->load(\Yii::$app->request->post())) {

            if (empty($model->due_date))
                $model->due_date = time();

            if (empty($model->email_reminder))
                $model->email_reminder = $model->due_date - (60 * 60 * 3); // 3 hour

            if (empty($model->tenant_id))
                $model->tenant_id = \Yii::$app->user->identity->tenant->id;

            if (empty($model->assigned_to_id))
                $model->assigned_to_id = \Yii::$app->user->id;

            if (empty($model->type_id)) {
                $type = TasksTypes::findOne(['default' => true]);

                if (null !== $type) {
                    $model->type_id = $type->id;
                }
            }

            if ($model->save())
                return $this->redirect(['index']);
        }

        $error = $model->getErrors();

        throw new BadRequestHttpException(array_shift($error)[0]);
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function actionUpdate()
    {
        $task_id = \Yii::$app->request->post('task_id', null);

        if (empty($task_id))
            throw new BadRequestHttpException();

        $model = Tasks::findOne($task_id);

        if (is_null($model))
            throw new BadRequestHttpException();

        if (RbacHelper::can('tasks.update.only_own')) {
            if($model->assigned_to_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('tasks.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                if(array_search($model->assigned_to_id,$ids)===false)
                    throw new HttpException(403);
            }
            else
                if($model->assigned_to_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        if ($model->load(\Yii::$app->request->post())) {
            if (empty(\Yii::$app->request->post('Tasks')['contact_id']))
                $model->contact_id = null;

            if (empty(\Yii::$app->request->post('Tasks')['company_id']))
                $model->company_id = null;

            $model->save();

            return $this->redirect(['index']);
        }

        $error = $model->getErrors();

        throw new BadRequestHttpException(array_shift($error)[0]);
    }

    /**
     * @return array|bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function actionChangeStatus()
    {
        $task_id = \Yii::$app->request->post('task_id', null);

        if (empty($task_id))
            throw new BadRequestHttpException();

        $model = Tasks::findOne($task_id);

        if (null === $model)
            throw new BadRequestHttpException();

        if (RbacHelper::can('tasks.update.only_own')) {
            if($model->assigned_to_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('tasks.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                if(array_search($model->assigned_to_id,$ids)===false)
                    throw new HttpException(403);
            }
            else
                if($model->assigned_to_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        if ( ($model->created_by !== null && $model->created_by != \Yii::$app->user->id) || $model->tenant_id != \Yii::$app->user->identity->tenant->id)
            throw new BadRequestHttpException();

        $model->status = $model->status ? false : true;

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->save()) {
            $reminder = OrderReminders::findOne(['task_id' => $model->id]);

            if ($reminder !== null) {
                $reminder->status = !$model->status ? false : true;
                $reminder->save();
            }

            return ['id' => $model->id];
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDelete($id) {
        if (empty($id))
            throw new BadRequestHttpException();

        $model = Tasks::findOne($id);

        if (is_null($model))
            throw new BadRequestHttpException();

        if (RbacHelper::can('tasks.delete.only_own')) {
            if($model->assigned_to_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('tasks.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                if(array_search($model->assigned_to_id,$ids)===false)
                    throw new HttpException(403);
            }
            else
                if($model->assigned_to_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->delete())
            return true;

        return false;
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $ids = \Yii::$app->request->post('ids', false);

        if (empty($ids))
            throw new BadRequestHttpException();

        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (RbacHelper::can('tasks.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        foreach ($ids as $id)
        {
            $model = Tasks::findOne($id);
            if (RbacHelper::can('tasks.delete.only_own')) {
                if($model->assigned_to_id != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('tasks.delete.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($model->assigned_to_id,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($model->assigned_to_id != \Yii::$app->user->id)
                        throw new HttpException(403);
            }
            $model->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }



    public function actionEventsCalendar()
    {
        $start = \Yii::$app->request->get('start',null);
        $end = \Yii::$app->request->get('end',null);
        $options = \Yii::$app->request->get('options',[]);

        $user = \Yii::$app->user->getIdentity();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return CalendarUtility::getEventsList($start,$end,$user->tenant->id,$user->id,$options);
    }
}