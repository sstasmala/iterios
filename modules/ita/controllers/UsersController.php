<?php
/**
 * UsersController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\components\events\SystemPasswordChangedEvent;
use app\components\events\SystemUserAddedToTenantEvent;
use app\components\events\SystemUserInviteEvent;
use app\components\events\SystemUserRegistrationEvent;
use app\helpers\RbacHelper;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\Auth;
use app\models\Contacts;
use app\models\Invites;
use app\models\Tenants;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use Auth0\SDK\API\Management\Users;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UsersController extends BaseController
{
    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        if(!$parent) return $parent;
        if(!RbacHelper::can('users.read.access') && !RbacHelper::owner())
            throw new HttpException(403);

        return $parent;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $users = User::find()->select(['id', 'first_name', 'last_name', 'middle_name', 'phone', 'email', 'photo', 'status']);

        $ids = AgentsMapping::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])->asArray()->all();
        $ids = array_column($ids, 'user_id');

        $invites = Invites::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])->asArray()->all();
        $invites_id = array_column($invites, 'user_id');

        $users = $users->where(['in', 'id', array_merge($ids, $invites_id)]);

        $meta['total'] = $users->count();

        $users->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $users = $users->all();

        $data = [];

        foreach ($users as $i => $user) {
            $data_user = $user->getAttributes();

            $roles = [];
            $data_user['not_confirm'] = in_array($user['id'], $invites_id) ? true : false;
            $data_user['is_tenant_owner'] = $user['id'] === \Yii::$app->user->identity->tenant->owner_id ? true : false;
            $data_user['status'] = $user->checkTenantBlock() ? 0 : 10;

            if ($data_user['is_tenant_owner']) {
                $data_user['role'] =  TranslationHelper::getTranslation('owner', \Yii::$app->user->identity->tenant->language->iso);
            } elseif($data_user['not_confirm']) {
                $invites = Invites::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id, 'user_id' => $user['id']])->asArray()->one();

                if (!empty($invites['user_role'])) {
                    $role = explode('.', $invites['user_role']);

                    $data_user['role'] = !empty($role[1]) ? $role[1] : $role;
                }
            } else {
                foreach (RbacHelper::getAssignedRoles($user['id'], \Yii::$app->user->identity->tenant->id) as $role) {
                    if ($role->roleName != 'system_admin')
                        $roles[] = explode('.', $role->roleName)[1];
                }

                $data_user['role'] = implode(', ', $roles);
            }

            $data[] = $data_user;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return ['data' => $data, 'meta' => $meta];
    }

    /**
     * @param $id
     *
     * @return array|null|User
     * @throws \yii\web\HttpException
     */
    protected function findOne($id)
    {
        $ids = AgentsMapping::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])->asArray()->all();
        $ids = array_column($ids, 'user_id');
        $user = User::find()->where(['in', 'id', $ids])->andWhere(['id'=>$id])->one();

        if(is_null($user))
            throw new HttpException(404,"User #$id is not found");

        return $user;
    }

    /**
     * @param $user_id
     * @param $password
     *
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function setPassword($user_id, $password) {
        $user = User::findOne($user_id);

        if (!is_null($user)) {
            $user->setPassword($password);

            if ($user->save()) {
                $client = Yii::$app->auth0->management_api_client;

                $user_data = $client->users->update($user->auth->source_id, [
                    'connection' => 'Username-Password-Authentication',
                    'password' => $password
                ]);

                if ($user_data['user_id']) {
                    $event = new SystemPasswordChangedEvent($user, $password);
                    Yii::$container->get('systemEmitter')->trigger($event);
                }
            }
        }
    }

    /**
     * @param      $id
     * @param null $assign_to
     *
     * @return Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id, $assign_to = null)
    {
        $tags = Yii::$app->request->get('tags', []);
        $ntd = Yii::$app->request->get('not_transfer_data',false);

        $user = $this->findOne($id);

        if (is_null($user))
            throw new HttpException(400, 'User not found!');

        if ($user->id == \Yii::$app->user->id || $user->id == \Yii::$app->user->identity->tenant->owner_id)
            return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/users');

        $user->reassign(($ntd ? null : $assign_to), $tags);

        $tenant = Tenants::findOne(\Yii::$app->user->identity->tenant->id);
        $tenant->unlink('users', $user);

        $roles = RbacHelper::getAssignedRoles($id, \Yii::$app->user->identity->tenant->id);

        foreach ($roles as $role) {
            if ($role->roleName != 'system_admin')
                RbacHelper::unassignRole($role->roleName, $id);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDeleteNotConfirm($id)
    {
        $invite = Invites::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id, 'user_id' => $id])->one();

        if (is_null($invite))
            throw new HttpException(400, 'Invite not found!');

        $invite->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\HttpException
     */
    public function actionUpdate($id)
    {
        $user = $this->findOne($id);

        if ($user->id == \Yii::$app->user->id || $user->id == \Yii::$app->user->identity->tenant->owner_id)
            return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/users');

        if ($user->load(\Yii::$app->request->post()) && $user->save())
        {
            $user_role = \Yii::$app->request->post('role', false);
            $user_pass = \Yii::$app->request->post('password', false);

            if ($user_role && $user_role != 'system_admin') {
                $roles = RbacHelper::getAssignedRoles($user->id, \Yii::$app->user->identity->tenant->id);

                foreach ($roles as $role) {
                    if ($role->roleName != 'system_admin')
                        RbacHelper::unassignRole($role->roleName, $user->id);
                }

                RbacHelper::assignRole($user_role, $user->id);
            }

            if ($user_pass)
                $this->setPassword($user->id, $user_pass);

            return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/users');
        }

        throw new HttpException(403, implode('; ', $user->getErrorSummary(true)));
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function actionBlock($id)
    {
        $user = $this->findOne($id);

        if ($user->id != \Yii::$app->user->id && $user->id != \Yii::$app->user->identity->tenant->owner_id) {
            $agents_mappings = AgentsMapping::findOne(['user_id' => $user->id, 'tenant_id' => \Yii::$app->user->identity->tenant->id]);

            if (!is_null($agents_mappings)) {
                $agents_mappings->block = true;
                $agents_mappings->save();
            }
            //$user->blockUser();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function actionUnblock($id)
    {
        $user = $this->findOne($id);

        if ($user->id != \Yii::$app->user->id && $user->id != \Yii::$app->user->identity->tenant->owner_id) {
            $agents_mappings = AgentsMapping::findOne(['user_id' => $user->id, 'tenant_id' => \Yii::$app->user->identity->tenant->id]);

            if (!is_null($agents_mappings)) {
                $agents_mappings->block = false;
                $agents_mappings->save();
            }
            //$user->blockUser(false);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $pass = Yii::$app->request->post('password');
            $user_role = \Yii::$app->request->post('role', false);

            if (User::checkAuth0User($model->email))
                throw new HttpException(400, 'User with email - '.$model->email.', already exists in Auth0!');

            $client = Yii::$app->auth0->management_api_client;
            $user_data = $client->users->create([
                'user_id' => '',
                'connection' => 'Username-Password-Authentication',
                'email' => $model->email,
                'username' => strstr($model->email, '@', true) . '_' . time(),
                'password' => $pass,
                'user_metadata' => [
                    'first_name' => $model->first_name,
                    'last_name' => $model->last_name
                ],
                'email_verified' => false,
                'verify_email' => true
            ]);

            $model->setPassword($pass);

            if (strlen($model->phone) <= 1)
                $model->phone = null;

            if ($user_data['user_id']) {
                $transaction = $model->getDb()->beginTransaction();

                if ($model->save()) {
                    $auth = new Auth([
                        'user_id' => $model->id,
                        'source' => 'auth0',
                        'source_id' => (string)$user_data['user_id'],
                    ]);

                    if ($auth->save()) {
                        $event = new SystemUserRegistrationEvent($model, $pass);
                        Yii::$container->get('systemEmitter')->trigger($event);

                        $transaction->commit();

                        if ($user_role && $user_role != 'system_admin') {
                            // $roles = RbacHelper::getAssignedRoles($model->id, \Yii::$app->user->identity->tenant->id);
                            //
                            // foreach ($roles as $role) {
                            //     if ($role->roleName != 'system_admin')
                            //         RbacHelper::unassignRole($role->roleName, $model->id);
                            // }

                            RbacHelper::assignRole($user_role, $model->id);
                        }

                        $tenant = Tenants::findOne(['id' => Yii::$app->user->identity->tenant->id]);
                        $tenant->link('users', $model);

                        $event = new SystemUserAddedToTenantEvent($model,$tenant);
                        Yii::$container->get('systemEmitter')->trigger($event);
                    }
                }
            }
        }

        return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/users');
    }

    /**
     * @param null $search
     * @param bool $tenant_user
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSearchUsers($search = null, $tenant_user = false)
    {
        if (!filter_var($search, FILTER_VALIDATE_EMAIL) && empty($tenant_user)) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [];
        }

        $users = User::find()->select(['id', 'email', 'phone', 'first_name', 'last_name', 'photo']);

        $ids = AgentsMapping::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])->asArray()->all();
        $ids = array_column($ids, 'user_id');

        if (empty($tenant_user)) {
            $users = $users->where(['not in', 'id', $ids]);
        } else {
            $users = $users->where(['in', 'id', $ids])
                ->andWhere(['not in', 'id', $tenant_user]);
        }

        if (!is_null($search))
            $users->andFilterWhere(['ilike', 'email', $search]);

        $users = $users->asArray()->all();

        $user_tenant = false;

        if (filter_var($search, FILTER_VALIDATE_EMAIL)) {
            $user = User::find()->select(['id', 'email'])
                ->where(['in', 'id', $ids])
                ->andWhere(['ilike', 'email', $search])
                ->one();

            if (!is_null($user))
                $user_tenant = $user->email;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return empty($users) ? (!$tenant_user && $user_tenant ? ['user_tenant' => $user_tenant] : []) : $users;
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\HttpException
     */
    public function actionAddUser($id)
    {
        $id = intval($id);
        $user_role = \Yii::$app->request->post('role', false);

        $user = User::find()->where(['id' => $id])->one();

        if (is_null($user))
            throw new HttpException(404,"User #$id is not found");

        $lang = \Yii::$app->user->identity->tenant->language->iso;
        $ids = AgentsMapping::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])->asArray()->all();
        $ids = array_column($ids, 'user_id');

        if (in_array($id, $ids)) {
            throw new HttpException(403, TranslationHelper::getTranslation('error', $lang));
        }

        $token = Yii::$app->security->generateRandomString();
        $tenant = Tenants::findOne(['id' => Yii::$app->user->identity->tenant->id]);

        $event = new SystemUserInviteEvent($user, $tenant, $token, $user_role);
        Yii::$container->get('systemEmitter')->trigger($event);

        // $response['result'] = 'success';
        // $response['message'] = TranslationHelper::getTranslation('saved', $lang);
        // return $response;

        return $this->redirect( Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/users');
    }

    /**
     * @param $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionGetUser($id) {
        $user = User::find()->select(['id', 'first_name', 'last_name', 'photo', 'email', 'phone'])
            ->where(['id' => $id]);
        $user = $user->one();

        $data = $user->getAttributes();
        $data['count_tourist'] = $user->getAssignedItems()['contacts'];

        $data['roles'] = RbacHelper::getRolesByTenant(\Yii::$app->user->identity->tenant->id);

        $data['role'] = array_filter(array_keys(RbacHelper::getAssignedRoles($user->id, \Yii::$app->user->identity->tenant->id)), function ($role) {
            return $role != 'system_admin' ? true : false;
        });

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return empty($data) ? [] : $data;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\HttpException
     */
    public function actionRepeatInvite($id) {
        $id = intval($id);

        $user = User::find()->where(['id' => $id])->one();

        if (is_null($user))
            throw new HttpException(404,"User #$id is not found");

        $invites = Invites::find()->where(['tenant_id' => \Yii::$app->user->identity->tenant->id, 'user_id' => $id])->one();

        if (is_null($invites))
            throw new HttpException(404,"Invite is not found");

        $token = Yii::$app->security->generateRandomString();
        $tenant = Tenants::findOne(['id' => Yii::$app->user->identity->tenant->id]);

        $event = new SystemUserInviteEvent($user, $tenant, $token, $invites->user_role);
        Yii::$container->get('systemEmitter')->trigger($event);

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }
}