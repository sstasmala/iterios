<?php

/**
 * RequestsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\models\OrdersServicesLinks;
use app\models\RequestsServicesLinks;
use app\models\RequestsServicesValues;
use app\models\Suppliers;
use app\modules\ita\controllers\base\BaseController;
use app\utilities\services\ServicesUtility;
use yii\web\HttpException;
use app\helpers\RbacHelper;
use app\models\Languages;
use app\models\ServicesFields;
use app\models\ServicesFieldsDefault;
use app\models\ServicesLinks;
use Yii;
use app\models\Services;
use yii\helpers\Json;
use yii\web\Response;

class ServicesController extends BaseController
{
    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

//        if(!RbacHelper::can('requests.read.access') && !RbacHelper::owner())
//            throw new HttpException(403);

        return $parent;
    }
    
    public function actionIndex()
    {
        $services = Services::getServicesWithFields();
        
        return $this->render('index', ['services' => $services]);
    }
    
    public function actionGetServiceById()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }
        
        $service_id = \Yii::$app->request->post('service_id',null);
        $service = ServicesUtility::getServices(intval($service_id),Yii::$app->user->identity->tenant->language->iso);
        if(count($service)<0)
            throw new HttpException(404,'Service not found!');

        $service = reset($service);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $service;
    }

    public function actionGetServiceByLinkId()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }

        $link_id = \Yii::$app->request->post('link_id',null);
        $link = RequestsServicesLinks::find()->where(['id'=>$link_id])->one();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if(is_null($link))
            return [];
        $service = ServicesUtility::getServices($link->service_id,Yii::$app->user->identity->tenant->language->iso);
        if(count($service)<0)
            throw new HttpException(404,'Service not found!');

        $service = reset($service);
//        $vss = RequestsServicesValues::find()->where(['link_id'=>intval($link_id)])->asArray()->all();
        $pr = ServicesUtility::getRequestsValues($link_id);
        $service = ServicesUtility::fillService($service,$pr);
        if(!isset($service['fields']))
            $service['fields'] = [];
        $fields = ServicesUtility::getFieldsFromService($service);
        $reference = ServicesUtility::getMCValuesFields($fields, \Yii::$app->user->identity->tenant);
        $service = ServicesUtility::setTitles($service,$reference);
        return $service;
    }

    public function actionGetOrdersServiceByLinkId()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }

        $link_id = \Yii::$app->request->post('link_id',null);
        /**
         * @var $link OrdersServicesLinks
         */
        $link = OrdersServicesLinks::find()->where(['id'=>$link_id])->one();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if($link === null)
            return [];
        $service = ServicesUtility::getServices($link->service_id,Yii::$app->user->identity->tenant->language->iso);
        if(count($service)<0)
            throw new HttpException(404,'Service not found!');

        $service = reset($service);
//        $vss = RequestsServicesValues::find()->where(['link_id'=>intval($link_id)])->asArray()->all();
        $pr = ServicesUtility::getOrdersValues($link_id);
        $service = ServicesUtility::fillService($service,$pr);
        if(!isset($service['fields']))
            $service['fields'] = [];
        $fields = ServicesUtility::getFieldsFromService($service);
        $reference = ServicesUtility::getMCValuesFields($fields, \Yii::$app->user->identity->tenant);
        $service = ServicesUtility::setTitles($service,$reference);
        $service['tourists'] = ServicesUtility::getTouristsFromOrdersService($link_id,Yii::$app->user->identity->tenant->language->iso);
        $service['adults_count'] = $link->adults_count;
        $service['children_count'] = $link->children_count;
        return $service;
    }
}
