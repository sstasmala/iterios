<?php

namespace app\modules\ita\controllers;

use app\helpers\ContactsHelper;
use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\Contacts;
use app\models\ContactsPassports;
use app\models\ContactsPassportsView;
use app\models\ContactsTags;
use app\models\DocumentsTemplate;
use app\models\OrderDocuments;
use app\models\OrderDocumentsUploadForm;
use app\models\OrderNotes;
use app\models\OrderReminders;
use app\models\Orders;
use app\models\OrdersExRates;
use app\models\OrdersServiceFieldsView;
use app\models\OrdersServicesAdditionalLinks;
use app\models\OrdersServicesLinks;
use app\models\OrdersServicesValues;
use app\models\OrdersTouristsPayments;
use app\models\OrderUploadDocuments;
use app\models\OrdersTourists;
use app\models\PassportsTypes;
use app\models\Reminders;
use app\models\RemindersTenant;
use app\models\RemindersTenantSystem;
use app\models\RequestCountries;
use app\models\Requests;
use app\models\RequestType;
use app\models\ServicesFields;
use app\models\ServicesFieldsDefault;
use app\models\Suppliers;
use app\models\Tasks;
use app\models\TasksTypes;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use app\helpers\RbacHelper;
use app\modules\ita\controllers\base\PaymentsController;
use app\utilities\contacts\ContactsUtility;
use app\utilities\document\DocumentUtility;
use app\utilities\orders\OrdersUtility;
use app\utilities\services\ServicesUtility;
use Yii;
use yii\helpers\FileHelper;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Services;

/**
 * Class OrdersController
 * @package app\modules\ita\controllers
 */

class OrdersController extends PaymentsController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        \Yii::$app->view->params['subheader'] = [
            'show' => false
        ];

        $contacts_types = ContactsUtility::prepareTypes(\Yii::$app->user->identity->tenant->language->iso);

        return $this->render('index', [
            'contacts_types' => $contacts_types
        ]);
    }

    /**
     * @param       $orders
     * @param array $search_data
     *
     * @return \yii\db\ActiveQuery
     */
    private function orderFilters(\yii\db\ActiveQuery $orders, array $search_data): \yii\db\ActiveQuery
    {
        if (!empty($search_data['filter']) && $search_data['filter'] === 'only_my_orders')
            $orders->andWhere(['created_by' => \Yii::$app->user->id])
                ->orWhere(['responsible_id' => \Yii::$app->user->id]);

        if (!empty($search_data['contact'])) {
            $contacts = Contacts::find()
                ->select('contacts.id')
                ->joinWith(['contactsEmails', 'contactsPhones', 'contactsPassports']);

            if ( ! empty($search_data['contact']['passport'])) {
                $contacts->andWhere(['ilike', 'contacts_passports.serial', $search_data['contact']['passport']]);
            }

            if ( ! empty($search_data['contact']['first_name'])) {
                $contacts->andWhere(['ilike', 'contacts.first_name', $search_data['contact']['first_name']]);
            }

            if ( ! empty($search_data['contact']['last_name'])) {
                $contacts->andWhere(['ilike', 'contacts.last_name', $search_data['contact']['last_name']]);
            }

            if ( ! empty($search_data['contact']['email'])) {
                $contacts->andWhere(['ilike', 'contacts_emails.value', $search_data['contact']['email']]);
            }

            if ( ! empty($search_data['contact']['phone'])) {
                $contacts->andWhere(['ilike', 'contacts_phones.value', $search_data['contact']['phone']])
                    ->orWhere([
                        'ilike',
                        'contacts_phones.value',
                        str_replace(['(', ')', '+', '-'], '', $search_data['contact']['phone'])
                    ]);
            }

            if ( ! empty($search_data['contact']['tags'])) {
                $tags = ContactsTags::find()->select(['contact_id', 'COUNT(tag_id) as tag_count'])
                    ->where(['in', 'tag_id', $search_data['contact']['tags']])
                    ->having(['COUNT(tag_id)' => \count($search_data['contact']['tags'])])
                    ->groupBy('contact_id')
                    ->asArray()->all();

                $contacts_ids = array_column($tags, 'contact_id');

                if ( ! empty($contacts_ids)) {
                    $contacts->andWhere(['in', 'contacts.id', $contacts_ids]);
                }
            }

            $contacts->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
            $contacts_id = $contacts->column();

            if ( ! empty($contacts_id)) {
                $orders->andWhere(['in', 'contact_id', $contacts_id]);
            }
        }

        if (!empty($search_data['create_date_start']) && !empty($search_data['create_date_end']))
            $orders->andWhere(['>=', 'created_at', DateTimeHelper::convertDateToTimestamp($search_data['create_date_start'])])
                ->andWhere(['<=', 'created_at', DateTimeHelper::convertDateToTimestamp($search_data['create_date_end'])]);

        if (!empty($search_data['update_date_start']) && !empty($search_data['update_date_end']))
            $orders->andWhere(['>=', 'updated_at', DateTimeHelper::convertDateToTimestamp($search_data['update_date_start'])])
                ->andWhere(['<=', 'updated_at', DateTimeHelper::convertDateToTimestamp($search_data['update_date_end'])]);

        if (!empty($search_data['responsible']))
            $orders->andWhere(['responsible_id' => $search_data['responsible']]);

        if (!empty($search_data['request_number'])) {
            $orders->andWhere(['request_id' => (int) $search_data['request_number']]);
        }

        if ( ! empty($search_data['countries']) && \is_array($search_data['countries'])) {
            $view_country = OrdersServiceFieldsView::find()->select('id')
                ->where([
                    'and',
                    ['in', 'value', $search_data['countries']],
                    ['type' => ServicesFieldsDefault::TYPE_COUNTRY]
                ])->distinct();
            $orders->andWhere(['in', 'id', $view_country]);
        }

        if ( ! empty($search_data['departure_date_start'])) {
            $view_date_start = OrdersServiceFieldsView::find()->select('id')
                ->where([
                    'and',
                    ['type' => ServicesFieldsDefault::TYPE_DATE_STARTING],
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_DATE_STARTING."' THEN CAST(nullif(value, '') AS int) ELSE null END >= " . DateTimeHelper::convertDateToTimestamp($search_data['departure_date_start']),
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_DATE_STARTING."' THEN CAST(nullif(value, '') AS int) ELSE null END <= " . DateTimeHelper::convertDateToTimestamp($search_data['departure_date_end'])
                ])->distinct();
            $orders->andWhere(['in', 'id', $view_date_start]);
        }

        if ( ! empty($search_data['departure_date_end'])) {
            $view_date_end = OrdersServiceFieldsView::find()->select('id')
                ->where([
                    'and',
                    ['type' => ServicesFieldsDefault::TYPE_DATE_ENDING],
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_DATE_ENDING."' THEN CAST(nullif(value, '') AS int) ELSE null END >= " . DateTimeHelper::convertDateToTimestamp($search_data['departure_date_start']),
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_DATE_ENDING."' THEN CAST(nullif(value, '') AS int) ELSE null END <= " . DateTimeHelper::convertDateToTimestamp($search_data['departure_date_end'])
                ])->distinct();
            $orders->andWhere(['in', 'id', $view_date_end]);
        }

        if ( ! empty(\Yii::$app->user->identity->tenant->main_currency) && ( ! empty($search_data['budget_from']) || ! empty($search_data['budget_to']))) {
            $view_currency = OrdersServiceFieldsView::find()->select('id')
                ->where([
                    'and',
                    ['value' => \Yii::$app->user->identity->tenant->main_currency],
                    ['type' => ServicesFieldsDefault::TYPE_CURRENCY]
                ])->distinct();
            $orders->andWhere(['in', 'id', $view_currency]);
        }

        if ( ! empty($search_data['budget_from']) || ! empty($search_data['budget_to'])) {
            $view_budget = OrdersServiceFieldsView::find()->select('id');

            if ( ! empty($search_data['budget_from']))
                $view_budget->andWhere([
                    'and',
                    ['type' => ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER],
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER."' THEN CAST(nullif(value, '') AS float) ELSE null END >= " . $search_data['budget_from']
                ]);

            if ( ! empty($search_data['budget_to']))
                $view_budget->andWhere([
                    'and',
                    ['type' => ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER],
                    "CASE WHEN type = '".ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER."' THEN CAST(nullif(value, '') AS float) ELSE null END <= " . $search_data['budget_to']
                ]);

            $view_budget->distinct();
            $orders->andWhere(['in', 'id', $view_budget]);
        }

        if (!empty($search_data['reservation_number'])) {
            $view_rn = OrdersServiceFieldsView::find()->select('id')
                ->where([
                    'and',
                    ['value' => (int) $search_data['reservation_number']],
                    ['type' => ServicesFieldsDefault::TYPE_RESERVATION]
                ])->distinct();
            $orders->andWhere(['in', 'id', $view_rn]);
        }

        if (!empty($search_data['search_text'])) {
            $where = ['or'];

            if (empty($search_data['contact']['first_name']) && empty($search_data['contact']['last_name'])) {
                $contacts = Contacts::find()->select('id');

                $search_arr = explode(' ', trim($search_data['search_text']));
                $search_arr = array_diff($search_arr, []);

                foreach ($search_arr as $search) {
                    $contacts->andWhere(['ilike', 'first_name', trim($search)])
                        ->orWhere(['ilike', 'last_name', trim($search)])
                        ->orWhere(['ilike', 'middle_name', trim($search)]);
                }

                $contacts->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

                $where[] = ['in', 'contact_id', $contacts->column()];
            }

            if (empty($search_data['request_number'])) {
                $where[] = ['request_id' => (int) $search_data['search_text']];
            }

            if (empty($search_data['reservation_number'])) {
                $view_rn = OrdersServiceFieldsView::find()->select('id')
                    ->where([
                        'and',
                        ['value' => (int)$search_data['search_text']],
                        ['type' => ServicesFieldsDefault::TYPE_RESERVATION]
                    ])->distinct();
                $where[] = ['in', 'id', $view_rn];
            }

            if (\count($where) > 1)
                $orders->andWhere($where);
        }

        $orders->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        return $orders;
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $orders = Orders::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        if (!empty($search_data) && \is_array($search_data)) {
            //$orders->andWhere(['in', 'id', $this->orderFilters($search_data)]);
            $orders = $this->orderFilters($orders, $search_data);
        }

        $orders->with(['contact', 'responsible']);

        $meta['total'] = $orders->count('DISTINCT orders.id');

        $orders->orderBy('created_at DESC')
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $orders = $orders->all();


        $ods = \array_map(function($item){return $item->id;},$orders);

        $new_orders = [];

        $ids = OrdersServicesLinks::find()->where(['in','order_id',$ods])->asArray()->all();
        $l_ids = \array_column($ids,'id');
        $s_ids = \array_combine($l_ids,\array_column($ids,'service_id'));
        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
        $services = \array_combine(\array_column($services,'id'),$services);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getOrdersValues($l_ids);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);

        $services_info = [];

        $srl = \array_combine(\array_column($ids,'id'),$ids);

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        foreach ($services as $k => $service)
        {
            $order_id = $srl[$k]['order_id'];
            $data['service_icon'] = $service['icon'];
            $data['service_name'] = $service['name'];
            $data['service_status'] = $srl[$k]['status'];
            $data['reservation'] = ServicesUtility::getServiceReservation($service);
            $data['fields_list'] = ServicesUtility::getServiceHeader($service,ServicesUtility::getServiceCurrency($service));
            $services_info[$order_id][] = $data;

            $total_sum = null;
            $suppliers_sum = null;
        }

        $payments = OrdersTouristsPayments::find()->where(['in','order_id',$ods])->asArray()->all();
        $payments_info = OrdersUtility::getPaymentInfo($ods);
//        $all_ex_rates = OrdersExRates::find()->where(['in','order_id',$ods])->asArray()->all();

        foreach ($orders as $order) {
            $data = $order->getAttributes();

            $data['full_name'] = isset($order->contact_id) ? trim($order->contact->first_name . ' ' . $order->contact->last_name . ' ' . $order->contact->middle_name) : '';
            $data['responsible'] = isset($order->responsible_id) ? trim($order->responsible->first_name . ' ' . $order->responsible->last_name) : '';
            $data['responsible_photo'] = (!empty($order->responsible->photo) ? $order->responsible->photo : 'img/profile_default.png');
            $data['order_payment'] = $payments_info[$order->id];
            $data['order_info'] = $services_info[$order->id] ?? [];
            $new_orders[] = $data;
        }

        return ['meta' => $meta, 'data' => $new_orders];
    }

    /**
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\HttpException
     */
    public function actionCreate()
    {
        if (!RbacHelper::can('deals.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $data = \Yii::$app->request->post();

        if (!empty($data['contact_id'])) {
            $contact = Contacts::find()
                ->where(['id' => $data['contact_id']])
                ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->one();

            if (null === $contact)
                throw new BadRequestHttpException('Contact not found');

            $contact_id = $contact->id;
        } else {
            $contact_id = ContactsHelper::create($data['Contacts']);
        }

        if (empty($contact_id))
            throw new BadRequestHttpException('contact_id is empty');

        $model = new Orders();
        $model->contact_id = $contact_id;
        $model->responsible_id = \Yii::$app->user->id;
        $model->tenant_id = \Yii::$app->user->identity->tenant->id;
        if(\Yii::$app->user->identity->tenant->main_currency === null)
        {
            $curs = MCHelper::getCurrency();
            $curs = array_filter($curs,function($item){return $item['iso_code']=='USD';});
            $model->currency = reset($curs)['id'];
        }
        else
            $model->currency = \Yii::$app->user->identity->tenant->main_currency;

        if (!$model->save())
            throw new BadRequestHttpException('Order not save');

        $order_reminders = Reminders::find()
            ->select('id')
            ->where(['type' => Reminders::TYPE_SYSTEM])
            ->andWhere(['NOT IN', 'id', RemindersTenantSystem::find()->select('reminder_id')->column()])
            ->column();
        $order_reminders_public = Reminders::find()
            ->select('reminders.id')
            ->distinct()
            ->where(['reminders.type' => Reminders::TYPE_PUBLIC])
            ->andWhere(['reminders_tenant.tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->andWhere(['reminders_tenant.active' => true])
            ->joinWith(['reminderTenant', 'orderReminders'])
            ->column();
        $reminders = array_merge($order_reminders, $order_reminders_public);

        foreach ($reminders as $reminder) {
            $reminder_model = new OrderReminders();
            $reminder_model->reminder_id = $reminder;
            $reminder_model->order_id = $model->id;
            $reminder_model->save();
        }

        return $this->redirect(['view?id='.$model->id]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        /**
         * @var $order Orders
         */
        $order = Orders::find()
            ->where(['id' => $id])->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->with([
                'contact',
                'contact.contactsEmails',
                'contact.contactsPhones',
                'contact.contactsMessengers',
                'contact.contactsSites',
                'contact.contactsSocials',
                'contact.contactsTags',
                'responsible',
                'request',
                'request.contact'
            ])
            ->asArray()->one();

        if (null === $order)
            throw new HttpException(404);

        if (!empty($order['request_source_id'])) {
            $order['request_source'] = RequestType::find()
                ->where(['id' => $order['request_source_id']])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->asArray()->one();
        }

        $or = Orders::find()->where(['id' => $id])->one();
        if($or->currency === null) {
            if(\Yii::$app->user->identity->tenant->main_currency === null)
            {
                $curs = MCHelper::getCurrency();
                $curs = \array_filter($curs,function($item){return $item['iso_code']=='USD';});
                $or->currency = \reset($curs)['id'];
            }
            else
                $or->currency = \Yii::$app->user->identity->tenant->main_currency;
            $or->save();
        }

//        $a = OrdersUtility::getPaymentInfo($id);


        $contacts_types = ContactsUtility::prepareTypes(\Yii::$app->user->identity->tenant->language->iso);
        $order_notes = OrderNotes::find()->where(['order_id' => $id])->orderBy(['created_at' => SORT_DESC])->all();

        $order_reminders = OrderReminders::find()
            ->where(['order_id' => $id])
            ->orderBy(['id' => SORT_ASC])
            ->asArray()->all();
        $reminders = Reminders::find()
            ->where(['in', 'id', \array_column($order_reminders, 'reminder_id')])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $reminders = \array_combine(\array_column($reminders, 'id'), $reminders);

        foreach ($order_reminders as $key => $order_reminder)
            $order_reminders[$key]['reminder'] = $reminders[$order_reminder['reminder_id']];

        $document_templates = DocumentsTemplate::find()
            //->where(['languages' => \Yii::$app->user->identity->tenant->language->iso])
            ->asArray()->all();

        $documents = OrderDocuments::find()
            ->where(['order_id' => $id])
            ->orderBy(['updated_at' => SORT_DESC])
            ->asArray()->all();
        $upload_documents = OrderUploadDocuments::find()
            ->where(['order_id' => $id])
            ->orderBy(['updated_at' => SORT_DESC])
            ->asArray()->all();

        $ids = OrdersServicesLinks::find()->where(['order_id'=>$order['id']])->asArray()->all();
        $l_ids = \array_column($ids,'id');
        $s_ids = \array_combine($l_ids,\array_column($ids,'service_id'));
        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
        $services = \array_combine(\array_column($services,'id'),$services);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getOrdersValues($l_ids);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);

        if(empty($services))
            $order_status = 0;
        else
            $order_status = 1;

        $total = 0;
        $filled = 0;
        foreach ($services as $service)
        {
            if (!isset($service['fields']))
                continue; 
            
            $f = $service['fields'];
            $total+=\count($f);
            $c = \array_column($f,'value');
            $filled += \count($c);
            $f = $service['fields_def'];
            $total+=\count($f);
            $c = \array_column($f,'value');
            $filled += \count($c);
        }
        if(!($total>$filled) && $order_status!=0)
            $order_status = 2;

        foreach($ids as $link)
            $services[$link['id']]['status'] = $link['status'];

        $values = [];
        $values['suppliers'] = Suppliers::find()->asArray()->all();
        $values['suppliers'] = \array_combine(\array_column($values['suppliers'],'id'),$values['suppliers']);
        $values['currencies'] = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $values['currencies'] = \array_combine(\array_column($values['currencies'],'id'),$values['currencies']);

        $tourists = OrdersTourists::find()
            ->where(['order_id' => $id])
            ->with(['contact', 'passport', 'link'])
            ->orderBy(['id' => SORT_ASC])
            ->asArray()->all();

        $view_params = [
            'order' => $order,
            'contact' => ContactsUtility::prepareContactsViewArray($order['contact']),
            'contacts_types' => $contacts_types,
            'orderNotes' => $order_notes,
            'reminders' => $order_reminders,
            'values' => $values,
            'services_linked' => $services,
            'doc_templates' => $document_templates,
            'documents' => $documents,
            'upload_documents' => $upload_documents,
            'services' => Services::getServicesWithFields(),
            'tourists' => $tourists,
            'order_status'=>$order_status
        ];

        return $this->render('view', $view_params);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id) {
        if (empty($id))
            throw new BadRequestHttpException();

        $model = Orders::findOne($id);

        if (null === $model)
            throw new BadRequestHttpException();

        if (RbacHelper::can('deals.delete.only_own')) {
            if($model->responsible_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('deals.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (\array_search($model->responsible_id, $ids) === false)
                    throw new HttpException(403);
            }
            else
                if ($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function actionBulkDelete()
    {
        $ids = \Yii::$app->request->post('ids', false);

        if (empty($ids))
            throw new BadRequestHttpException();

        if (!\is_array($ids))
            $ids = \explode(',', $ids);

        if (RbacHelper::can('deals.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        foreach ($ids as $id)
        {
            $model = Orders::findOne($id);

            if (RbacHelper::can('deals.delete.only_own')) {
                if($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('deals.delete.only_own_role')) {
                if(isset($ids_r)) {
                    if(\array_search($model->responsible_id,$ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($model->responsible_id != \Yii::$app->user->id)
                        throw new HttpException(403);
            }
            $model->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @return array|boolean
     * @throws HttpException
     */
    public function actionChangeResponsible()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }

        $post = \Yii::$app->request->post();
        $order_id = $post['order_id'];
        $responsible_id = $post['responsible_id'];

        // Work with database...
        $order = Orders::find()->where(['id' => $order_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        if (RbacHelper::can('deals.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $order->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('deals.update.only_own')) {
            $order->andWhere(['responsible' => \Yii::$app->user->id]);
        }

        $order = $order->one();

        if(null !== $order)
        {
            $user = User::find()->where(['id'=>$responsible_id])->one();
            if(null !== $user)
            {
                $agent = AgentsMapping::find()->where(['user_id'=>$responsible_id])
                    ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one();

                if(null !== $agent) {
                    $order->responsible_id = $responsible_id;
                    $order->save();
                    $response = [
                        'result' => 'success', // success|error
                        'message' => TranslationHelper::getTranslation('saved', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
                    ];
                    \Yii::$app->response->format = Response::FORMAT_JSON;
                    return $response;
                }
            }
        }

        /* Create response */
        $response = [
            'result' => 'error', // success|error
            'message' => TranslationHelper::getTranslation('error', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
        ];

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;

    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionChangeContact($id)
    {
        $id = (int) $id;
        $response = \Yii::$app->request->post();

        if (empty($response['contact_id']))
            throw new BadRequestHttpException('contact_id is empty');

        $order = Orders::find()
            ->where(['id' => $id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if ($order === null)
            throw new NotFoundHttpException('Request not fount!');

        $contact = Contacts::find()
            ->where(['id' => $response['contact_id']])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if ($contact === null)
            throw new NotFoundHttpException('Contact not fount!');

        $order->contact_id = $contact->id;
        $order->save();

        return $this->redirect('/ita/orders/view?id='.$id);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionAddNotes($id)
    {
        if (!RbacHelper::can('deals.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $id = (int) $id;
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        $orders = Orders::find()->where(['id' => $id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $orders)
            throw new HttpException(404);

        $orderNote = new OrderNotes();
        $orderNote->order_id = $orders->id;
        $orderNote->contact_id = $orders->contact_id;
        $orderNote->value = $post['value'];
        $orderNote->save();

        return $this->redirect('view?id=' . $id);
    }

    /**
     * @param int $id_note
     *
     * @return \app\models\OrderNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionEditNote($id_note)
    {
        $id_note = (int) $id_note;
        $response = \Yii::$app->request->post();

        /** @var OrderNotes $note */
        $note = OrderNotes::findOne($id_note);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('deals.update.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('deals.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if ($note->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->value = $response['value'] ?? '';
        $note->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id_note
     *
     * @return \app\models\OrderNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionGetNote($id_note)
    {
        $id_note = (int) $id_note;
        /** @var OrderNotes $note */
        $note = OrderNotes::findOne($id_note);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('deals.read.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('deals.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if ($note->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id_note
     *
     * @throws \yii\web\HttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteNote($id_note)
    {
        $id_note = (int) $id_note;
        /** @var OrderNotes $note */
        $note = OrderNotes::findOne($id_note);

        if ($note === null)
            throw new \HttpException(400, 'Note not found!');

        if (RbacHelper::can('orders.delete.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new \HttpException(403);
        }

        if (RbacHelper::can('orders.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if ($note->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Note not found!');

        $note->delete();
    }

    /**
     * @param $id
     *
     * @throws \HttpException
     */
    public function actionReminderChecked($id)
    {
        $id = (int) $id;

        $reminder = OrderReminders::findOne($id);

        if ($reminder === null)
            throw new \HttpException(400, 'Reminder not found!');

        $reminder->status = $reminder->status === 1 ? 0 : 1;

        if ($reminder->save() && $reminder->task_id !== null) {
            $task = Tasks::findOne($reminder->task_id);

            if ($task !== null) {
                $task->status = $reminder->status !== 1 ? 0 : 1;
                $task->save();
            }
        }
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \HttpException
     */
    public function actionReminderSetDate($id)
    {
        $id = (int) $id;
        $response = \Yii::$app->request->post();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $reminder = OrderReminders::findOne($id);


        if ($reminder === null)
            throw new \HttpException(400, 'Reminder not found!');


        if (!empty($response['due_date'])) {
            $rm = Reminders::find()
                ->where(['id' => $reminder->reminder_id])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->one();

            $task = new Tasks();
            $task->title = $rm->name;
            $task->due_date = !empty($response['due_date']) ? $response['due_date'] : time();
            $task->email_reminder = $task->due_date - (60 * 60 * 3); // 3 hour
            $task->tenant_id = \Yii::$app->user->identity->tenant->id;
            $task->assigned_to_id = \Yii::$app->user->id;
            $task->order_id = $reminder->order_id;

            $type = TasksTypes::findOne(['default' => true]);

            if (null !== $type) {
                $task->type_id = $type->id;
            }

            if ($task->save()) {
                $reminder->due_date = $task->due_date;
                $reminder->task_id = $task->id;

                if ($reminder->save()) {
                    return [
                        'due_date' => DateTimeHelper::convertTimestampToDateTime($response['due_date']),
                        'due_date_str' => DateTimeHelper::convertTimestampToDateTime($task->due_date, 'd M')
                        ];
                }
            }
        }
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\HttpException
     * @throws \Exception
     */
    public function actionAddReminder($id)
    {
        if (!RbacHelper::can('deals.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $format = Yii::$app->user->identity->tenant->date_format;
        $id = (int) $id;
        $post = \Yii::$app->request->post()['Reminder'];

        if (empty($post))
            throw new HttpException(404);

        $order = Orders::find()->where(['id' => $id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(404);

        if (!empty($post['name'])) {
            $rm = new Reminders();
            $rm->type = Reminders::TYPE_PUBLIC;
            $rm->name = $post['name'];

            if ($rm->saveWithLang(\Yii::$app->user->identity->tenant->language->iso)) {
                $reminder_tenant = new RemindersTenant();
                $reminder_tenant->reminder_id = $rm->id;
                $reminder_tenant->tenant_id = \Yii::$app->user->identity->tenant->id;
                $reminder_tenant->active = true;

                if ($reminder_tenant->save()) {
                    $task = new Tasks();
                    $task->title = $rm->name;
                    $task->due_date = !empty($post['due_date']) ? $post['due_date'] : time();
                    $task->email_reminder = $task->due_date - (60 * 60 * 3); // 3 hour
                    $task->tenant_id = \Yii::$app->user->identity->tenant->id;
                    $task->assigned_to_id = \Yii::$app->user->id;
                    $task->order_id = $order->id;

                    $type = TasksTypes::findOne(['default' => true]);

                    if (null !== $type) {
                        $task->type_id = $type->id;
                    }

                    if ($task->save()) {
                        $reminder = new OrderReminders();
                        $reminder->reminder_id = $rm->id;
                        $reminder->order_id = $order->id;
                        $reminder->due_date = $task->due_date;
                        $reminder->task_id = $task->id;

                        if ($reminder->save()) {
                            \Yii::$app->response->format = Response::FORMAT_JSON;

                            return [
                                'id' => $reminder->id,
                                'due_date_str' => DateTimeHelper::convertTimestampToDateTime($task->due_date, $format),
                                'due_date' => DateTimeHelper::convertTimestampToDateTime($task->due_date),
                                'name' => $task->title
                            ];
                        }
                    }
                }
            }
        }

        throw new BadRequestHttpException('Reminder not save!');
    }

    /**
     * @param $order_id
     * @param $tid
     *
     * @return array
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \yii\web\HttpException
     * @throws \Twig_Error_Syntax
     */
    public function actionGetDocumentPreview($order_id, $tid)
    {
        $order = Orders::find()->where(['id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(404);

        $template = DocumentsTemplate::findOne($tid);

        if (null === $template)
            throw new HttpException(404);

        $preview = DocumentUtility::getDocumentPreview($template, $order);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return ['title' => $template->title, 'body' => $preview];
    }

    /**
     * @param $document_id
     *
     * @return \app\models\OrderDocuments
     * @throws \yii\web\HttpException
     * @throws \HttpException
     */
    public function actionGetDocument($document_id)
    {
        $doc = OrderDocuments::findOne($document_id);

        if ($doc === null)
            throw new HttpException(404);

        if ($doc->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $doc;
    }

    /**
     * @param      $order_id
     * @param      $tid
     * @param null $document_id
     *
     * @return \app\models\OrderDocuments|array|null|\yii\db\ActiveRecord
     * @throws \yii\web\HttpException
     * @throws \HttpException
     */
    public function actionSaveDocument($order_id, $tid, $document_id = null)
    {
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        if (\md5($post['body']) !== $post['body_hash'])
            throw new BadRequestHttpException('Document do not save!');

        $order = Orders::find()->where(['id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(404);

        $template = DocumentsTemplate::findOne($tid);

        if (null === $template)
            throw new HttpException(404);

        if (null !== $document_id) {
            $model = OrderDocuments::find()
                ->where(['id' => $document_id])
                ->andWhere(['order_id' => $order_id])
                ->one();

            if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
                throw new \HttpException(400, 'Document not found!');
        } else {
            $model = new OrderDocuments();
            $model->order_id = $order->id;
            $model->template_id = $template->id;
        }

        $model->title = !empty($post['title']) ? $post['title'] : (empty($model->title) ? $template->title : $model->title);
        $model->body = $post['body'];
        $model->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $model;
    }

    /**
     * @param $document_id
     *
     * @return \app\models\OrderDocuments
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionSaveTitleDocument($document_id)
    {
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        $model = OrderDocuments::findOne($document_id);

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        $model->title = !empty($post['title']) ? $post['title'] : $model->title;
        $model->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $model;
    }

    /**
     * @param $document_id
     *
     * @return bool
     * @throws \Exception
     * @throws \HttpException
     * @throws \Throwable
     */
    public function actionDeleteDocument($document_id): bool
    {
        $model = OrderDocuments::find()
            ->where(['id' => $document_id])
            ->one();

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @param $document_id
     *
     * @return bool
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteUploadDocument($document_id): bool
    {
        $model = OrderUploadDocuments::find()
            ->where(['id' => $document_id])
            ->one();

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        if (file_exists($model->file_path))
            FileHelper::unlink($model->file_path);

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @param $order_id
     *
     * @return \app\models\OrderUploadDocuments|array
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionUploadDocuments($order_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $order = Orders::find()->where(['id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(404);

        $model = new OrderDocumentsUploadForm();

        if (\Yii::$app->request->isPost)
            return $model->upload($order);

        return ['error' => true];
    }

    /**
     * @param $document_id
     *
     * @return \app\models\OrderUploadDocuments
     * @throws \yii\web\HttpException
     * @throws \HttpException
     */
    public function actionGetUploadDocument($document_id)
    {
        $doc = OrderUploadDocuments::findOne($document_id);

        if ($doc === null)
            throw new HttpException(404);

        if ($doc->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $doc;
    }

    /**
     * @param $document_id
     *
     * @return \app\models\OrderDocuments|array|null|\yii\db\ActiveRecord
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionSaveUploadDocument($document_id)
    {
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        $model = OrderUploadDocuments::findOne($document_id);

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        $model->title = !empty($post['title']) ? $post['title'] : $model->title;
        $model->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $model;
    }

    /**
     * @param $document_id
     *
     * @return \yii\console\Response|\yii\web\Response
     * @throws \HttpException
     */
    public function actionDownloadDocument($document_id)
    {
        $model = OrderUploadDocuments::findOne($document_id);

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Document not found!');

        return \Yii::$app->response->sendFile($model->file_path, $model->title . '.' . pathinfo($model->file_path, PATHINFO_EXTENSION));
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionAddService()
    {
        $post = \Yii::$app->request->post();
        $tenant_id = \Yii::$app->user->identity->tenant->id;
        $order_id = (int) $post['order_id'];
        $order = Orders::find()->where(['id'=>$order_id])->one();

        if (null === $order)
            throw new HttpException(400,'Order is not found!');

        /**
         * @var $link OrdersServicesLinks
         */
        $link = ServicesUtility::createService($post,$order_id, $tenant_id,OrdersServicesLinks::class,
            OrdersServicesValues::class,OrdersServicesAdditionalLinks::class);

        if (isset($post['Adults_count']))
            $link->adults_count = (int) $post['Adults_count'];

        if (isset($post['Children_count']))
            $link->children_count = (int) $post['Children_count'];

        $link->status = OrdersServicesLinks::STATUS_WAIT;
        $link->save();

        $tourist_count = !empty($post['Tourists']) ? array_count_values(array_column($post['Tourists'], 'type')) : [];

        if ($link->adults_count > 0) {
            $adult_count = \array_key_exists(OrdersTourists::TYPE_ADULT, $tourist_count) ? $link->adults_count - $tourist_count[OrdersTourists::TYPE_ADULT] : $link->adults_count;

            for ($x = 0; $x < $adult_count; $x++) {
                $post['Tourists'][] = [
                    'type' => OrdersTourists::TYPE_ADULT
                ];
            }
        }

        if ($link->children_count > 0) {
            $child_count = \array_key_exists(OrdersTourists::TYPE_CHILD, $tourist_count) ? $link->children_count - $tourist_count[OrdersTourists::TYPE_CHILD] : $link->children_count;

            for ($x = 0; $x < $child_count; $x++) {
                $post['Tourists'][] = [
                    'type' => OrdersTourists::TYPE_CHILD
                ];
            }
        }

        if (isset($post['Tourists']) && $link) {
            foreach ($post['Tourists'] as $tourist) {
                $tr = new OrdersTourists();

                if (isset($tourist['id']) && !empty($tourist['id']))
                    $tr->contact_id = $tourist['id'];

                if (isset($tourist['passport_id']) && !empty($tourist['passport_id']))
                    $tr->passport_id = $tourist['passport_id'];

                $tr->link_id = $link->id;
                $tr->order_id = $order->id;
                $tr->type = $tourist['type'];
                $tr->save();
            }
        }
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionUpdateService()
    {
        $post = \Yii::$app->request->post();

        $tenant_id = \Yii::$app->user->identity->tenant->id;
        $order_id = (int) $post['order_id'];
        $order = Orders::find()
            ->where(['id' => (int)$order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(400,'Order is not found!('.$order_id.') Tenant - '.\Yii::$app->user->identity->tenant->id);

        /**
         * @var $link OrdersServicesLinks
         */
        $link = ServicesUtility::updateService($post,$order->id,$tenant_id,OrdersServicesLinks::class,
            OrdersServicesValues::class,OrdersServicesAdditionalLinks::class);

        $old = OrdersTourists::find()->where(['link_id'=>$link->id])->all();

        $old = array_combine(array_map(function($item){return $item->id;},$old),$old);

        if(isset($post['Adults_count']))
            $link->adults_count = (int)$post['Adults_count'];
        if(isset($post['Children_count']))
            $link->children_count = (int)$post['Children_count'];
        $link->save();

        $tourist_count = !empty($post['Tourists']) ? array_count_values(array_column($post['Tourists'], 'type')) : [];

        if ($link->adults_count > 0) {
            $adult_count = \array_key_exists(OrdersTourists::TYPE_ADULT, $tourist_count) ? $link->adults_count - $tourist_count[OrdersTourists::TYPE_ADULT] : $link->adults_count;

            for ($x = 0; $x < $adult_count; $x++) {
                $post['Tourists'][] = [
                    'type' => OrdersTourists::TYPE_ADULT
                ];
            }
        }

        if ($link->children_count > 0) {
            $child_count = \array_key_exists(OrdersTourists::TYPE_CHILD, $tourist_count) ? $link->children_count - $tourist_count[OrdersTourists::TYPE_CHILD] : $link->children_count;

            for ($x = 0; $x < $child_count; $x++) {
                $post['Tourists'][] = [
                    'type' => OrdersTourists::TYPE_CHILD
                ];
            }
        }

        if (isset($post['Tourists']) && $link) {
            foreach ($post['Tourists'] as $tourist) {
                if(isset($tourist['order_tourist_id']) && isset($old[$tourist['order_tourist_id']])) {
                    $tr = $old[$tourist['order_tourist_id']];
                    unset($old[$tourist['order_tourist_id']]);
                } else {
                    $tr = new OrdersTourists();

                    if (isset($tourist['id']) && !empty($tourist['id'])) {
                        $tr->contact_id = $tourist['id'];
                    }

                    if (isset($tourist['passport_id']) && ! empty($tourist['passport_id'])) {
                        $tr->passport_id = $tourist['passport_id'];
                    }

                    $tr->link_id  = $link->id;
                    $tr->order_id = $order->id;
                    $tr->type     = $tourist['type'];
                    $tr->save();
                }
            }
        }

        foreach ($old as $o)
            $o->delete();
    }

    /**
     * @param $id
     *
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteService($id)
    {
        $service = OrdersServicesLinks::find()->where(['id'=>intval($id)])->one();

        if (null === $service || $service->tenant_id!=\Yii::$app->user->identity->tenant->id)
            throw new \HttpException('400',"Service not found!");

        $service->delete();
    }

    /**
     * @return array
     * @throws \yii\web\HttpException
     * @throws \HttpException
     */
    public function actionCreateTourist()
    {
        $data = Yii::$app->request->post();

        if(!isset($data['Contact']) && !isset($data['Tourist']))
            throw new HttpException(400);

        $contact = $data['Contact'];

        if (!isset($contact['first_name']) || empty($contact['first_name'])
            || !isset($contact['last_name']) || empty($contact['last_name']))
            throw new HttpException(400);

        $order_tourist = null;

        if (!empty($data['order_tourist_id'])) {
            $order_tourist = OrdersTourists::findOne($data['order_tourist_id']);

            if ($order_tourist === null || $order_tourist->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
                throw new \HttpException(400, 'Tourist not found!');
        }

        if ($order_tourist === null) {
            $contact_model             = new Contacts();
            $contact_model->first_name = $contact['first_name'];
            $contact_model->last_name  = $contact['last_name'];
            $contact_model->tenant_id  = Yii::$app->user->identity->tenant->id;
            $contact_model->type       = Contacts::NEW_ITEM;
        } else {
            $contact_model = $order_tourist->contact;
        }

        if ($contact_model->save())
        {
            $tourist = $data['Tourist'];

            $response_data = $contact_model->toArray();

            if (isset($tourist['first_name']) || !empty($tourist['first_name'])
                || isset($tourist['last_name']) || !empty($tourist['last_name'])
                || isset($tourist['serial'])
                || isset($tourist['date_limit'])
                || isset($tourist['passport_type'])
                || isset($tourist['birth_date'])
                || isset($tourist['issued_date']))
            {
                $passport = new ContactsPassports();
                $passport->contact_id = $contact_model->id;
                $passport->first_name = $tourist['first_name'];
                $passport->last_name  = $tourist['last_name'];
                $passport->serial     = $tourist['serial'];
                $passport->date_limit = DateTimeHelper::convertDateToTimestamp($tourist['date_limit']);
                $passport->type_id    = $tourist['passport_type'];
                $passport->birth_date = DateTimeHelper::convertDateToTimestamp($tourist['birth_date']);
                $passport->issued_date = DateTimeHelper::convertDateToTimestamp($tourist['issued_date']);

                if ($passport->save()) {
                    if ($order_tourist !== null) {
                        $order_tourist->passport_id = $passport->id;
                        $order_tourist->save();
                    }

                    $passport_type = PassportsTypes::find()
                        ->where(['id' => $passport->id])
                        ->translate(\Yii::$app->user->identity->tenant->language->iso)
                        ->asArray()->one();

                    $passport = $passport->toArray();
                    $passport['type'] = [
                        'id'    => $passport['type_id'],
                        'value' => $passport_type['value']
                    ];
                    $passport['date_limit'] = DateTimeHelper::convertTimestampToDate($passport['date_limit']);
                    $passport['birth_date'] = DateTimeHelper::convertTimestampToDate($passport['birth_date']);
                    $passport['issued_date'] = DateTimeHelper::convertTimestampToDate($passport['issued_date']);

                    $response_data['passport'] = $passport;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response_data;
        }

        throw new HttpException(400);
    }

    /**
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionUpdateTourist()
    {
        $data = Yii::$app->request->post();

        if (empty($data['contact_id']))
            throw new HttpException(400);

        if (!isset($data['Contact']) && !isset($data['Tourist']))
            throw new HttpException(400);

        $contact = $data['Contact'];

        if (!isset($contact['first_name']) || empty($contact['first_name'])
            || !isset($contact['last_name']) || empty($contact['last_name']))
            throw new HttpException(400);

        $order_tourist = null;

        $contact_model = Contacts::findOne($data['contact_id']);

        if ($contact_model === null)
            throw new HttpException(400, 'Contact is not found!');

        $contact_model->first_name = $contact['first_name'];
        $contact_model->last_name  = $contact['last_name'];
        $contact_model->tenant_id  = Yii::$app->user->identity->tenant->id;
        $contact_model->type       = Contacts::NEW_ITEM;

        if ($contact_model->save())
        {
            $tourist = $data['Tourist'];

            $response_data = $contact_model->toArray();

            if (isset($tourist['first_name']) || !empty($tourist['first_name'])
                || isset($tourist['last_name']) || !empty($tourist['last_name'])
                || isset($tourist['serial'])
                || isset($tourist['date_limit'])
                || isset($tourist['passport_type'])
                || isset($tourist['birth_date'])
                || isset($tourist['issued_date'])
                || isset($tourist['issued_owner'])
                || isset($tourist['nationality']))
            {
                $passport = empty($data['passport_id']) ? new ContactsPassports() : ContactsPassports::findOne($data['passport_id']);

                if ($passport !== null) {
                    if(isset($contact_model->id))
                        $passport->contact_id   = $contact_model->id;
                    if(isset($tourist['first_name']))
                        $passport->first_name   = $tourist['first_name'];
                    if(isset($tourist['last_name']))
                        $passport->last_name    = $tourist['last_name'];
                    if(isset($tourist['serial']))
                        $passport->serial       = $tourist['serial'];
                    if(isset($tourist['date_limit']))
                        $passport->date_limit   = DateTimeHelper::convertDateToTimestamp($tourist['date_limit']);
                    if(isset($tourist['passport_type']))
                        $passport->type_id      = $tourist['passport_type'];
                    if(isset($tourist['nationality']))
                        $passport->nationality  = $tourist['nationality'];
                    if(isset($tourist['issued_owner']))
                        $passport->issued_owner = $tourist['issued_owner'];
                    if(isset($tourist['birth_date']))
                        $passport->birth_date   = DateTimeHelper::convertDateToTimestamp($tourist['birth_date']);
                    if(isset($tourist['issued_date']))
                        $passport->issued_date  = DateTimeHelper::convertDateToTimestamp($tourist['issued_date']);

                    if ($passport->save()) {
                        $passport_type = PassportsTypes::find()
                            ->where(['id' => $passport->id])
                            ->translate(\Yii::$app->user->identity->tenant->language->iso)
                            ->asArray()->one();

                        $passport = $passport->toArray();
                        $passport['type'] = [
                            'id'    => $passport['type_id'],
                            'value' => $passport_type['value']
                        ];
                        $passport['date_limit'] = DateTimeHelper::convertTimestampToDate($passport['date_limit']);
                        $passport['birth_date'] = DateTimeHelper::convertTimestampToDate($passport['birth_date']);
                        $passport['issued_date'] = DateTimeHelper::convertTimestampToDate($passport['issued_date']);

                        $response_data['passport'] = $passport;
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response_data;
        }

        throw new HttpException(400);
    }

    /**
     * @return array
     */
    public function actionSearchContacts()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $passports = ContactsPassportsView::find();
            $passports->andFilterWhere(['or',
                ['like', "CONCAT(last_name, ' ', first_name)", $data['search']],
                ['ilike', "CONCAT(last_name, ' ', first_name)", $data['search']],
                ['like', 'serial', $data['search']],
                ['ilike', 'serial', $data['search']]
            ]);
            $passports->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);
            $passports = $passports->asArray()->all();

            $passport_type = PassportsTypes::find()
                ->where(['in', 'id', array_column($passports, 'type_id')])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->asArray()->all();
            $passport_type = array_combine(array_column($passport_type, 'id'), $passport_type);

            $tourists = [];

            foreach ($passports as $i => $passport) {
                $tourist_passport = [];

                if (!empty($passport['type_id'])) {
                    $tourist_passport = [
                        'id' => $passport['passport_id'],
                        'date_limit' => DateTimeHelper::convertTimestampToDate($passport['date_limit']),
                        'birth_date' => DateTimeHelper::convertTimestampToDate($passport['birth_date']),
                        'serial' => $passport['serial'],
                        'first_name' => $passport['first_name'],
                        'last_name' => $passport['last_name'],
                        'type' => [
                            'id'    => $passport['type_id'],
                            'value' => $passport_type[$passport['type_id']]['value']
                        ],
                    ];
                }

                $tourists[] = [
                    'id' => $passport['contact_id'],
                    'first_name' => $passport['first_name'],
                    'last_name' => $passport['last_name'],
                    'passport' => $tourist_passport
                ];
            }

            $tourists = array_combine(array_column($tourists,'id'), $tourists);

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $tourists;
        }

        return [];
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionSetRequestSource()
    {
         $data = Yii::$app->request->post();

         if (empty($data['id']))
             throw new HttpException(404);

         $order = Orders::find()
             ->where(['id' => $data['id']])
             ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
             ->one();

         if ($order === null)
             throw new HttpException(404);

         $order->request_id = null;

         if ($order->load($data) && $order->save()) {
             $request_source = RequestType::find()
                 ->where(['id' => $order->request_source_id])
                 ->translate(\Yii::$app->user->identity->tenant->language->iso)
                 ->asArray()->one();
             $request_source['request_id'] = $order->request_id;
             $request_source['request_contact_name'] = !empty($order->request_id) ? $order->request->contact->last_name . ' ' . $order->request->contact->first_name : '';

             Yii::$app->response->format = Response::FORMAT_JSON;
             return $request_source;
         }
    }

    /**
     * @param $order_id
     *
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionGetOrderServices($order_id)
    {
        $order_id = (int) $order_id;
        $order = Orders::find()
            ->where(['id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(400,'Order is not found!');

        $services = $this->getOrderServices($order->id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $services;
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionAddTourist()
    {
        $data = Yii::$app->request->post();

        if (empty($data['order_id']))
            throw new HttpException(400,'Order is not found!');

        $order_id = (int) $data['order_id'];
        $order = Orders::find()
            ->where(['id' => $order_id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $order)
            throw new HttpException(400,'Order is not found!');

        if (empty($data['tourist_id']) || empty($data['order_tourist_id']))
            throw new HttpException(400);

        $tourist = OrdersTourists::find()
            ->where(['id' => (int)$data['order_tourist_id']])
            ->one();

        if (null === $tourist) {
            throw new HttpException(400, 'Tourist is not found!');
        }

        if ( ! empty($data['tourist_id'])) {
            $contact = Contacts::findOne((int) $data['tourist_id']);

            if ($contact !== null)
                $tourist->contact_id = (int) $data['tourist_id'];
        }

        if ( ! empty($data['passport_id'])) {
            $passport = ContactsPassports::findOne((int)$data['passport_id']);

            if ($passport !== null && !empty($tourist->contact_id) && $tourist->contact_id === $passport->contact_id)
                $tourist->passport_id = (int)$data['passport_id'];
        }

        $tourist->type = empty($data['tourist_type']) ? $tourist->type : $data['tourist_type'];
        $tourist->save();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $tourist;
    }

    /**
     * @param $tourist_id
     *
     * @return bool
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionUpdateTouristPassports($tourist_id)
    {
        $model = OrdersTourists::find()
            ->where(['id' => $tourist_id])
            ->one();

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Tourist not found!');

        $data = Yii::$app->request->post();

        if(!isset($data['Tourist']) || !isset($data['Contact']))
            throw new HttpException(400);

        $contact = Contacts::findOne($model->contact_id);

        if ($contact !== null) {
            $contact->first_name = $data['Contact']['first_name'];
            $contact->last_name  = $data['Contact']['last_name'];
            $contact->save();
        }

        $data = $data['Tourist'];

        for ($i = 0, $iMax = \count($data['passport_id']) - 1; $i <= $iMax; $i++) {

            if ( empty($data['passport_id'][$i])
                || ! isset($data['first_name'][$i]) || empty($data['first_name'][$i])
                || ! isset($data['last_name'][$i]) || empty($data['last_name'][$i])
                || ! isset($data['serial'][$i])
                || ! isset($data['date_limit'][$i])
                || ! isset($data['passport_type'][$i])
                || ! isset($data['birth_date'][$i])
                || ! isset($data['issued_date'][$i])
                || ! isset($data['nationality'][$i]))
                continue;

            $passport = ContactsPassports::findOne($data['passport_id'][$i]);

            if ($passport !== null && $passport->contact_id === $contact->id) {

                if(isset($data['first_name'][$i]))
                    $passport->first_name   = $data['first_name'][$i];
                if(isset($data['last_name'][$i]))
                    $passport->last_name    = $data['last_name'][$i];
                if(isset($data['serial'][$i]))
                    $passport->serial       = $data['serial'][$i];
                if(isset($data['date_limit'][$i]))
                    $passport->date_limit   = DateTimeHelper::convertDateToTimestamp($data['date_limit'][$i]);
                if(isset($data['passport_type'][$i]))
                    $passport->type_id      = $data['passport_type'][$i];
                if(isset($data['nationality'][$i]))
                    $passport->nationality  = $data['nationality'][$i];
                if(isset($data['issued_owner'][$i]))
                    $passport->issued_owner = $data['issued_owner'][$i];
                if(isset($data['birth_date'][$i]))
                    $passport->birth_date   = DateTimeHelper::convertDateToTimestamp($data['birth_date'][$i]);
                if(isset($data['issued_date'][$i]))
                    $passport->issued_date  = DateTimeHelper::convertDateToTimestamp($data['issued_date'][$i]);
                $passport->save();
            }
        }

        if (isset($data['selected_passport'])) {
            $selected_passport = ContactsPassports::findOne($data['selected_passport']);

            if ($selected_passport !== null && $contact !== null && $selected_passport->contact_id === $contact->id)
                $model->passport_id = (int) $data['selected_passport'];
        }

        $model->save();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @param $tourist_id
     *
     * @return bool
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteTourist($tourist_id)
    {
        $model = OrdersTourists::find()
            ->where(['id' => $tourist_id])
            ->one();

        if ($model === null || $model->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Tourist not found!');

        $link = OrdersServicesLinks::findOne($model->link_id);

        if ($link !== null) {
            if ($model->type === OrdersTourists::TYPE_CHILD) {
                --$link->children_count;
            } else {
                --$link->adults_count;
            }

            $link->save();
        }

        $model->delete();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @param $tourist_id
     * @return array|\yii\db\ActiveRecord[]
     * @throws \HttpException
     */
    public function actionGetTouristPassports($tourist_id)
    {
        $tourist = OrdersTourists::find()
            ->where(['id' => $tourist_id])
            ->one();

        if ($tourist === null || $tourist->order->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new \HttpException(400, 'Tourist not found!');

        $passports = [];

        if (!empty($tourist->passport_id)) {
            $passports     = ContactsPassports::find()
                ->where(['contact_id' => $tourist->passport->contact_id])
                ->asArray()->all();
            $passport_type = PassportsTypes::find()
                ->where(['in', 'id', array_column($passports, 'type_id')])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->asArray()->all();
            $passport_type = array_combine(array_column($passport_type, 'id'), $passport_type);

            foreach ($passports as $i => $passport) {
                $passports[$i]['date_limit'] = DateTimeHelper::convertTimestampToDate($passport['date_limit']);
                $passports[$i]['birth_date'] = DateTimeHelper::convertTimestampToDate($passport['birth_date']);
                $passports[$i]['issued_date'] = DateTimeHelper::convertTimestampToDate($passport['issued_date']);
                $passports[$i]['type']       = [
                    'id'    => $passport['type_id'],
                    'value' => $passport_type[$passport['type_id']]['value']
                ];
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'tourist_passport_id' => $tourist->passport_id,
            'contact' => [
                'first_name' => $tourist->contact->first_name,
                'last_name' => $tourist->contact->last_name,
            ],
            'passports' => $passports
        ];
    }

    public function actionSetServiceStatus($order_id,$link_id) {
        $order = Orders::find()->where(['id'=>$order_id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);
        $link = OrdersServicesLinks::find()->where(['id'=>$link_id])->one();
        if($link === null)
            throw new HttpException(404);
        $status = Yii::$app->request->post('status',null);
        if($status!==null) {
            $link->status = $status;
            $link->save();
        }
        return true;
    }

    public function actionCheckPassportSerial($serial, $passport_id = '')
    {
        if (!empty($serial)) {
            $query = ContactsPassports::find()
                ->joinWith('contact')
                ->where(['contacts.tenant_id' => \Yii::$app->user->identity->tenant->id])
                ->andWhere(['contacts_passports.serial' => $serial]);

            if (!empty($passport_id))
                $query->andWhere(['!=', 'contacts_passports.id', $passport_id]);

            $query = $query->count();

            \Yii::$app->response->format = Response::FORMAT_JSON;

            if ($query) {
                return [
                    'error'     => 1,
                    'error_msg' => TranslationHelper::getTranslation('modal_passport_serial_check',
                        \Yii::$app->user->identity->tenant->language->iso) . $serial,
                ];
            }
        }

        return ['error' => 0];
    }

    public function actionGetCountryName($country_name = '')
    {
        if(!empty($country_name) && isset($country_name)) {
            $country_name = MCHelper::getCountryById($country_name,\Yii::$app->user->identity->tenant->language->iso)[0]['title'];
            return $country_name;
        } else {
            return '';
        }

    }

    /**
     * @param $order_id
     * @param bool $link_id
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getOrderServices($order_id, $link_id = false)
    {
        $ids = OrdersServicesLinks::find()
            ->where(['order_id' => $order_id]);

        if ($link_id !== false)
            $ids->andWhere(['id' => $link_id]);

        $ids = $ids->asArray()->all();

        $s_ids = array_column($ids, 'service_id');
        $services = Services::find()
            ->where(['in', 'id', $s_ids])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()->all();
        $services = array_combine(array_column($services, 'id'), $services);

        $ids = array_map(function ($item) use ($services) {
            $item['name'] = $services[$item['service_id']]['name'] . '(ID #' . $item['id'] . ')';
            return $item;
        }, $ids);

        return $ids;
    }
}
