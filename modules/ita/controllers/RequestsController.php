<?php

/**
 * RequestsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\helpers\RequestsHelper;
use app\models\CommercialProposals;
use app\models\Contacts;
use app\models\ContactsTags;
use app\models\RequestCities;
use app\models\RequestCountries;
use app\models\RequestHotelCategories;
use app\models\RequestNotes;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\RequestsServicesAdditionalLinks;
use app\models\RequestsServicesLinks;
use app\models\RequestsServicesValues;
use app\models\RequestStatuses;
use app\models\RequestType;
use app\models\search\RequestCanceledReasons;
use app\models\ServicesFieldsDefault;
use app\models\Suppliers;
use app\models\User;
use app\modules\ita\controllers\base\BaseController;
use app\utilities\commercial_proposals\CommercialProposalsUtility;
use app\utilities\services\ServicesUtility;
use DateTime;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use app\helpers\RbacHelper;
use app\models\Requests;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use app\utilities\contacts\ContactsUtility;
use app\models\Services;

class RequestsController extends BaseController
{
    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

        switch ($action->id)
        {
            case 'view': if (!RbacHelper::can('requests.read',['full','only_own','only_own_role']) && !RbacHelper::owner()) throw new HttpException(403); break;
            case 'change-responsible': if (!RbacHelper::can('requests.update',['full','only_own','only_own_role']) && !RbacHelper::owner()) throw new HttpException(403); break;
        }
//        if(!RbacHelper::can('requests.read.access') && !RbacHelper::owner())
//            throw new HttpException(403);

        return $parent;
    }
    
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];

        return $this->render('index');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $requests = Requests::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'opened_requests')
            $requests->andWhere(['>', 'departure_date_end', time()]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'only_my_requests')
            $requests->andWhere(['requests.created_by' => \Yii::$app->user->id])->orWhere(['responsible_id' => \Yii::$app->user->id]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'successfully_requests')
            $requests->andWhere(['request_statuses.type' => RequestStatuses::TYPE_SOLD]);

        if (!empty($search_data['filter']) && $search_data['filter'] == 'unrealized_requests')
            $requests->andWhere(['request_statuses.type' => RequestStatuses::TYPE_CANCELED]);

        if (!empty($search_data['contact']) || !empty($search_data['search_text'])) {
            $contacts = Contacts::find()
                ->joinWith(['contactsEmails', 'contactsPhones', 'contactsPassports']);

            if (!empty($search_data['search_text']) && empty($search_data['contact']['first_name']) && empty($search_data['contact']['last_name'])) {
                $search_arr = explode(' ', trim($search_data['search_text']));
                $search_arr = array_diff($search_arr, []);

                foreach ($search_arr as $search) {
                    $contacts->andWhere(['ilike', 'contacts.first_name', trim($search)])
                        ->orWhere(['ilike', 'contacts.last_name', trim($search)])
                        ->orWhere(['ilike', 'contacts.middle_name', trim($search)]);
                }
            }

            if (!empty($search_data['contact']['passport']))
                $contacts->andWhere(['ilike', 'contacts_passports.serial', $search_data['contact']['passport']]);

            if (!empty($search_data['contact']['first_name']))
                $contacts->andWhere(['ilike', 'contacts.first_name', $search_data['contact']['first_name']]);

            if (!empty($search_data['contact']['last_name']))
                $contacts->andWhere(['ilike', 'contacts.last_name', $search_data['contact']['last_name']]);

            if (!empty($search_data['contact']['email']))
                $contacts->andWhere(['ilike', 'contacts_emails.value', $search_data['contact']['email']]);

            if (!empty($search_data['contact']['phone']))
                $contacts->andWhere(['ilike', 'contacts_phones.value', $search_data['contact']['phone']])
                    ->orWhere(['ilike', 'contacts_phones.value', str_replace(['(', ')', '+', '-'], '', $search_data['contact']['phone'])]);

            if (!empty($search_data['contact']['tags'])) {
                $tags = ContactsTags::find()->select(['contact_id', 'COUNT(tag_id) as tag_count'])
                    ->where(['in', 'tag_id', $search_data['contact']['tags']])
                    ->having(['COUNT(tag_id)' => count($search_data['contact']['tags'])])
                    ->groupBy('contact_id')
                    ->asArray()->all();

                $contacts_ids = array_column($tags, 'contact_id');

                if (!empty($contacts_ids))
                    $contacts->andWhere(['in', 'contacts.id', $contacts_ids]);
            }

            $contacts->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id]);
            $contacts = $contacts->asArray()->all();
            $contacts_id = array_column($contacts, 'id');

            if (!empty($contacts_id))
                $requests->andWhere(['in', 'contact_id', $contacts_id]);
        }

        if (!empty($search_data['create_date_start']) && !empty($search_data['create_date_end']))
            $requests->andWhere(['>=', 'requests.created_at', DateTimeHelper::convertDateToTimestamp($search_data['create_date_start'])])
                ->andWhere(['<=', 'requests.created_at', DateTimeHelper::convertDateToTimestamp($search_data['create_date_end'])]);

        if (!empty($search_data['update_date_start']) && !empty($search_data['update_date_end']))
            $requests->andWhere(['>=', 'requests.updated_at', DateTimeHelper::convertDateToTimestamp($search_data['update_date_start'])])
                ->andWhere(['<=', 'requests.updated_at', DateTimeHelper::convertDateToTimestamp($search_data['update_date_end'])]);

        if (!empty($search_data['responsible']))
            $requests->andWhere(['responsible_id' => $search_data['responsible']]);

        if (!empty($search_data['departure_date_start']))
            $requests->andWhere(['>=', 'requests.departure_date_start', DateTimeHelper::convertDateToTimestamp($search_data['departure_date_start'])]);

        if (!empty($search_data['departure_date_end']))
            $requests->andWhere(['<=', 'requests.departure_date_end', DateTimeHelper::convertDateToTimestamp($search_data['departure_date_end'])]);

        if (!empty($search_data['currency']) && (!empty($search_data['budget_from']) || !empty($search_data['budget_to'])))
            $requests->andWhere(['budget_currency' => $search_data['currency']]);

        if (!empty($search_data['budget_from']))
            $requests->andWhere(['>=', 'budget_from', $search_data['budget_from']]);

        if (!empty($search_data['budget_to']))
            $requests->andWhere(['<=', 'budget_to', $search_data['budget_to']]);

        if (!empty($search_data['number']))
            $requests->andWhere(['requests.id' => (int) $search_data['number']]);

        if (!empty($search_data['countries']) || !empty($search_data['search_text'])) {
            if (!empty($search_data['countries'])) {
                $countries_id = $search_data['countries'];
            } else {
                $search_countries = MCHelper::searchCountry(trim($search_data['search_text']), \Yii::$app->user->identity->tenant->language->iso);

                $countries_id = !empty($search_countries) ? array_column($search_countries, 'id') : [];
            }

            $countries = RequestCountries::find()
                ->where(['in', 'country', $countries_id])
                ->all();

            if (!empty($countries))
                $requests->andWhere(['in', 'requests.id', array_column($countries, 'request_id')]);
        }

        if (!empty($search_data['cities'])) {
            $cities = RequestCities::find()
                ->where(['in', 'city', $search_data['cities']])
                ->all();

            $requests->andWhere(['in', 'requests.id', array_column($cities, 'request_id')]);
        }

        $requests->joinWith(['requestStatus'])
            ->with(['contact', 'responsible', 'requestCountries', 'requestHotelCategories']);

        $meta['total'] = $requests->count('DISTINCT requests.id');

        $requests->orderBy('created_at DESC')
            ->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $requests = $requests->all();

        $countries_id = [];
        //$hotels_id = [];
        $currencies_id = [];

        foreach ($requests as $request) {
            if (!empty($request->requestCountries))
                $countries_id = array_merge(array_column($request->requestCountries, 'country'), $countries_id);

            // if (!empty($request->requestHotelCategories))
            //     $hotels_id = array_merge(array_column($request->requestHotelCategories, 'hotel_category'), $hotels_id);

            if (!empty($request->budget_currency))
                $currencies_id[] = $request->budget_currency;
        }

        $countries_id = array_unique($countries_id);
        $countries = MCHelper::getCountryById($countries_id, \Yii::$app->user->identity->tenant->language->iso);

        // $hotels_id = array_unique($hotels_id);
        // $hotels = MCHelper::getHotelCategoryById($hotels_id, \Yii::$app->user->identity->tenant->language->iso);

        $currencies = MCHelper::getCurrencyById($currencies_id, \Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies, 'id'), $currencies);

        $new_requests = [];

        foreach ($requests as $request) {
            $data = $request->getAttributes();

            $data['created_at'] = DateTimeHelper::convertTimestampToDate($request->created_at);
            $data['full_name'] = isset($request->contact_id) ? trim($request->contact->first_name . ' ' . $request->contact->last_name . ' ' . $request->contact->middle_name) : '';

            if (!empty($request->requestCountries)) {
                $req_countries = array_column($request->requestCountries, 'country');

                $data['countries'] = implode(', ', array_column(array_filter($countries, function ($v) use ($req_countries) {
                    return in_array($v['id'], $req_countries);
                }), 'title'));
            }

            if (!empty($request->departure_date_start)) {
                $now = new DateTime('@' . $request->departure_date_start);
                $date = new DateTime();
                $data['date_interval'] = $now->diff($date)->d;
                $data['dates'] = DateTimeHelper::convertTimestampToDate($request->departure_date_start) . ' - ' . DateTimeHelper::convertTimestampToDate($request->departure_date_end);
            }

            //$data['period'] = $request->trip_duration_start . ' - ' . $request->trip_duration_end;

            // if (!empty($request->requestHotelCategories)) {
            //     $req_hotels = array_column($request->requestHotelCategories, 'hotel_category');
            //
            //     $data['hotels'] = implode(', ', array_column(array_filter($hotels, function ($v) use ($req_hotels) {
            //         return in_array($v['id'], $req_hotels);
            //     }), 'title'));
            // }

            //$data['tourists_children_age'] = !empty($request->tourists_children_age) ? implode(', ', json_decode($request->tourists_children_age, true)) : '';

            if (!empty($request->budget_currency))
                $data['budget_currency'] = $currencies[$request->budget_currency]['iso_code'];

            $data['responsible'] = isset($request->responsible_id) ? trim($request->responsible->first_name . ' ' . $request->responsible->last_name) : '';
            $data['responsible_photo'] = (!empty($request->responsible->photo) ? $request->responsible->photo : 'img/profile_default.png');

            if (!empty($request->request_status_id)) {
                $data['status_name']  = $request->requestStatus->translate(\Yii::$app->user->identity->tenant->language->iso)->name;
                $data['status_color'] = $request->requestStatus->color_type;
            }

            $new_requests[] = $data;
        }

        return ['meta' => $meta, 'data' => $new_requests];
    }

    /**
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\web\HttpException
     */
    public function actionCreate()
    {
        if (!RbacHelper::can('requests.create.access') && !RbacHelper::owner())
            throw new HttpException(403);
        
        $req_id = RequestsHelper::create(\Yii::$app->request->post());

        return $this->redirect(['view?id='.$req_id]);
    }

    private function getValues($services)
    {
        $fields_arr = array_column($services,'fields');
        $fields = [];
        foreach ($fields_arr as $f)
        {
            $fields = array_merge($fields,$f);
        }
        $suppliers = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_SUPPLIERS && !is_null($var['value']) && $var['value']!="");
        });
        $airports = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_AIRPORT && !is_null($var['value']) && $var['value']!="");
        });
        $cities = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_CITY && !is_null($var['value']) && $var['value']!="");
        });
        $countries = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_COUNTRY && !is_null($var['value']) && $var['value']!="");
        });
        $departure_cities = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_DEPARTURE_CITY && !is_null($var['value']) && $var['value']!="");
        });
        $foods = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_FOOD && !is_null($var['value']) && $var['value']!="");
        });
        $hotels = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_HOTEL && !is_null($var['value']) && $var['value']!="");
        });
        $star_ratings = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_STAR_RATING && !is_null($var['value']) && $var['value']!="");
        });
        $currencies = array_filter($fields,function($var){
            return ($var['field']['type']==ServicesFieldsDefault::TYPE_CURRENCY && !is_null($var['value']) && $var['value']!="");
        });

        $tenant = \Yii::$app->user->identity->tenant;
        $suppliers = Suppliers::find()->where(['in','id',array_column($suppliers,'value')])->asArray()->all();
        $airports = MCHelper::getAirportsById(array_column($airports,'value'),$tenant->language->iso);
        $cities = MCHelper::getCityById(array_column($cities,'value'),$tenant->language->iso);
        $countries = MCHelper::getCountryById(array_column($countries,'value'),$tenant->language->iso);
        $departure_cities = MCHelper::getDepartureCityById(array_column($departure_cities,'value'),$tenant->language->iso);
        $foods = MCHelper::getFoodOptionsById(array_column($foods,'value'),$tenant->language->iso);
        $hotels = MCHelper::getHotelsOptionsById(array_column($hotels,'value'),$tenant->language->iso);
        $star_ratings = MCHelper::getHotelCategoryById(array_column($star_ratings,'value'),$tenant->language->iso);
        $currencies = MCHelper::getCurrencyById(array_column($currencies,'value'),$tenant->language->iso);

        $data = compact('star_ratings','suppliers','airports','cities','countries','departure_cities','foods','hotels','currencies');
        foreach ($data as $i => $type)
        {
            $data[$i] = array_combine(array_column($type,'id'),$type);
        }

        return $data;
    }

    /**
     * @param $id
     * @param int $step
     * @return string
     * @throws BadRequestHttpException
     * @throws HttpException
     */
    public function actionView($id)
    {
        $request = Requests::find()
            ->where(['id' => $id])
            ->with([
                'contact',
                'contact.contactsEmails',
                'contact.contactsPhones',
                'contact.contactsMessengers',
                'contact.contactsSites',
                'contact.contactsSocials',
                'contact.contactsTags',
                'requestHotelCategories',
                'requestCountries',
                'responsible',
                'requestCities',
                'requestType',
                ])
            ->asArray()->one();

        if (null === $request)
            throw new BadRequestHttpException();

        $request['requestCanceledReason'] = RequestCanceledReasons::find()
            ->where(['id'=>$request['request_canceled_reason_id']])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->one();

        $request['requestStatus'] = RequestStatuses::find()
            ->where(['id'=>$request['request_status_id']])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->one();

        $types = RequestType::find()
            ->select(['id', 'value'])
            ->from('request_type')
            ->where(['system' => false])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();
        $currency = MCHelper::getCurrencyById($request['budget_currency'], \Yii::$app->user->identity->tenant->language->iso)[0];
        $ids = RequestsServicesLinks::find()->where(['request_id'=>$request['id']])->asArray()->all();
        $l_ids = array_column($ids,'id');
        $s_ids = array_combine($l_ids,array_column($ids,'service_id'));
        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
        $services = array_combine(array_column($services,'id'),$services);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getRequestsValues($l_ids);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);

//        $services = RequestsServicesLinks::getFields($request['id']);
//

        $values['suppliers'] = Suppliers::find()->asArray()->all();
        $values['suppliers'] = array_combine(array_column($values['suppliers'],'id'),$values['suppliers']);
        $view_params = [
            'request' => $request,
            'request_currency' => $currency,
            'types' => $types
        ];
        
        $view_params['contacts_types'] = ContactsUtility::prepareTypes(\Yii::$app->user->identity->tenant->language->iso);
        $view_params['services'] = Services::getServicesWithFields();
        // Modify request var
        $request['contact'] = ContactsUtility::prepareContactsViewArray($request['contact']);
        $view_params['request'] = $request;
        $view_params['linked_services'] = $services;
        $view_params['values'] = $values;
        $view_params['requestNotes'] = RequestNotes::find()->where(['request_id'=>$id])->orderBy(['created_at'=> SORT_DESC])->all();
        
        return $this->render('view', $view_params);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        if (empty($id))
            throw new BadRequestHttpException();

        $model = Requests::findOne($id);

        if (is_null($model))
            throw new BadRequestHttpException();

        if (RbacHelper::can('requests.delete.only_own')) {
            if($model->responsible_id != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('requests.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($model->responsible_id, $ids)===false)
                    throw new HttpException(403);
            }
            else
                if ($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     */
    public function actionBulkDelete()
    {
        $ids = \Yii::$app->request->post('ids', false);

        if (empty($ids))
            throw new BadRequestHttpException();

        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (RbacHelper::can('requests.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        foreach ($ids as $id)
        {
            $model = Requests::findOne($id);
            if (RbacHelper::can('requests.delete.only_own')) {
                if($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
            }
            if (RbacHelper::can('requests.delete.only_own_role')) {
                if(isset($ids_r)) {
                    if(array_search($model->responsible_id, $ids_r)===false)
                        throw new HttpException(403);
                }
                else
                    if($model->responsible_id != \Yii::$app->user->id)
                        throw new HttpException(403);
            }
            $model->delete();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionAddNotes($id)
    {
        if (!RbacHelper::can('requests.create.access') && !RbacHelper::owner())
            throw new HttpException(403);

        $id = intval($id);
        $post = \Yii::$app->request->post();

        if (empty($post))
            throw new HttpException(404);

        $request = Requests::find()->where(['requests.id' => $id])
            ->andWhere(['requests.tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if (null === $request)
            throw new HttpException(404);

        $requestNote = new RequestNotes();
        $requestNote->request_id = $request->id;
        $requestNote->contact_id = $request->contact_id;
        $requestNote->value = $post['value'];
        $requestNote->save();

        return $this->redirect('view?id=' . $id);
    }

    /**
     * @param int $id_note
     *
     * @return \app\models\RequestNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionEditNote($id_note)
    {
        $id_note = intval($id_note);
        $response = \Yii::$app->request->post();

        /** @var RequestNotes $note */
        $note = RequestNotes::findOne($id_note);

        if ($note === null)
            throw new HttpException(400, 'Note not found!');

        if (RbacHelper::can('requests.update.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('requests.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new HttpException(400, 'Note not found!');

        $note->value = $response['value'];
        $note->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id_note
     *
     * @return \app\models\RequestNotes
     * @throws \HttpException
     * @throws \yii\web\HttpException
     */
    public function actionGetNote($id_note)
    {
        $id_note = intval($id_note);
        /** @var RequestNotes $note */
        $note = RequestNotes::findOne($id_note);

        if ($note === null)
            throw new HttpException(400, 'Note not found!');

        if (RbacHelper::can('requests.read.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('requests.read.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new HttpException(400, 'Note not found!');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $note;
    }

    /**
     * @param int $id_note
     *
     * @throws \yii\web\HttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteNote($id_note)
    {
        $id_note = intval($id_note);
        /** @var RequestNotes $note */
        $note = RequestNotes::findOne($id_note);

        if ($note === null)
            throw new HttpException(400, 'Note not found!');

        if (RbacHelper::can('requests.delete.only_own')) {
            if($note->created_by !== \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('requests.delete.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);

                if (array_search($note->created_by, $ids) === false)
                    throw new HttpException(403);
            } else {
                if ($note->created_by !== \Yii::$app->user->id) {
                    throw new HttpException(403);
                }
            }
        }

        if (!empty($note->request_id) && $note->request->tenant_id !== \Yii::$app->user->identity->tenant->id)
            throw new HttpException(400, 'Note not found!');

        $note->delete();
    }

    /**
     * @return array|boolean
     * @throws HttpException
     */
    public function actionChangeResponsible()
    {
        if (!\Yii::$app->request->isAjax) {
            return false;
        }
        $post = \Yii::$app->request->post();
        $request_id = $post['request_id'];
        $responsible_id = $post['responsible_id'];

        // Work with database...
        $request = Requests::find()->where(['id'=>$request_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);
        if (RbacHelper::can('requests.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles))
            {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                $request->andWhere(['in','responsible',$ids]);
            }
        }

        if (RbacHelper::can('requests.update.only_own')) {
            $request->andWhere(['responsible' => \Yii::$app->user->id]);
        }
        $request = $request->one();
        if(null !== $request)
        {
            $user = User::find()->where(['id'=>$responsible_id])->one();
            if(null !== $user)
            {
                $agent = AgentsMapping::find()->where(['user_id'=>$responsible_id])
                    ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one();
                if(null !== $agent)
                {
                    $request->responsible_id = $responsible_id;
                    $request->save();
                    $response = [
                        'result' => 'success', // success|error
                        'message' => TranslationHelper::getTranslation('saved', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
                    ];
                    \Yii::$app->response->format = Response::FORMAT_JSON;
                    return $response;
                }
            }
        }
        /* Create response */
        $response = [
            'result' => 'error', // success|error
            'message' => TranslationHelper::getTranslation('error', \Yii::$app->user->identity->tenant->language->iso, 'Saved')
        ];
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;

    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionChangeContact($id)
    {
        $id = (int) $id;
        $response = \Yii::$app->request->post();

        if (empty($response['contact_id']))
            throw new BadRequestHttpException('contact_id is empty');

        $request = Requests::find()
            ->where(['id' => $id])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if ($request === null)
            throw new NotFoundHttpException('Request not fount!');

        $contact = Contacts::find()
            ->where(['id' => $response['contact_id']])
            ->andWhere(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->one();

        if ($contact === null)
            throw new NotFoundHttpException('Contact not fount!');

        $request->contact_id = $contact->id;
        $request->save();

        return $this->redirect('/ita/requests/view?id='.$id);
    }

    /**
     * @return array|mixed
     * @throws \yii\web\HttpException
     */
    public function actionSetParamAjax()
    {
        $lang = \Yii::$app->user->identity->tenant->language->iso;
        $allCurrency = array_column(MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso), 'iso_code', 'id');

        if (\Yii::$app->request->isAjax) {
            //PROCCESS
            $response = \Yii::$app->request->post();
            $request = Requests::find()->where(['id'=>$response['id']])
                ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
                ->one();

            if(null === $request)
                throw new HttpException(404);

            if (RbacHelper::can('requests.update.only_own')) {
                if($request->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
            }

            if (RbacHelper::can('requests.update.only_own_role')) {
                $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
                if(!empty($roles)) {
                    $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                    if(array_search($request->responsible,$ids)===false)
                        throw new HttpException(403);
                }
                else
                    if($request->responsible != \Yii::$app->user->id)
                        throw new HttpException(403);
            }
            switch ($response['property']) {
                case 'adultsCount':
                    if (!empty($response['value'])) {
                        if($response['value'] > 0){
                            $request->tourists_adult_count = $response['value'];
                            $request->save();
                            $adultsCountValue = $response['value'];
                        } elseif ($response['value'] <= 0){
                            $request->tourists_adult_count = 1;
                            $request->save();
                            $adultsCountValue = $request->tourists_adult_count;}
                    } else {
                        $request->tourists_adult_count = 1;
                        $request->save();
                        $adultsCountValue = $request->tourists_adult_count;
                    }
                    if ($request->hasErrors()) {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error', $lang);
                        break;
                    }
                    $response['new_value'] = $adultsCountValue;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'childrenCount':
                    if (!empty($response['Requests']['children_count']) && !empty($response['Requests']['children_age'])) {
                        if($response['Requests']['children_count'] > 0){
                            $ages_json = json_encode($response['Requests']['children_age'], true);
                            $request->tourists_children_age = $ages_json;
                            $request->tourists_children_count = $response['Requests']['children_count'];
                            $request->save();

                            $view_list_ages = ' '.TranslationHelper::getTranslation('request_children_reduction', $lang, 'ch.').' ( ' . implode(', ', json_decode($ages_json, true)) . ' ) ';
                            $childrenCountValue = $response['Requests']['children_count'];
                        } else{
                            $request->tourists_children_age = null;
                            $request->tourists_children_count = null;
                            $request->save();
                            $childrenCountValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                            $view_list_ages = '';
                        }
                    } else {
                        $request->tourists_children_age = null;
                        $request->tourists_children_count = null;
                        $request->save();
                        $childrenCountValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                        $view_list_ages = '';
                    }
                    if ($request->hasErrors()) {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error', $lang);
                        break;
                    }
                    $response['new_value'] = $childrenCountValue . $view_list_ages;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'tripDuration':
                    if (!empty($response['trip_duration_start']) && !empty($response['trip_duration_end'])) {
                        if ($response['trip_duration_start'] > 0 && $response['trip_duration_end'] > 0) {
                            $request->trip_duration_start = $response['trip_duration_start'];
                            $request->trip_duration_end = $response['trip_duration_end'];
                            $request->save();
                            $tripDurationValue = $response['trip_duration_start'] . ' - ' . $response['trip_duration_end'];
                        } else {
                            $request->trip_duration_start = null;
                            $request->trip_duration_end = null;
                            $request->save();
                            $tripDurationValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                        }

                    } else {
                        $request->trip_duration_start = null;
                        $request->trip_duration_end = null;
                        $request->save();
                        $tripDurationValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if ($request->hasErrors()) {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error', $lang);
                        break;
                    }
                    $response['new_value'] = $tripDurationValue . ' ' . TranslationHelper::getTranslation('rv_page_nights_count', $lang, 'nights');
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'budget':
                    if (!empty($response['budget_from']) && !empty($response['budget_to']) && !empty($response['currency'])) {
                        if ($response['budget_from'] > 0 && $response['budget_to'] > 0) {
                            $request->budget_currency = $response['currency'];
                            $request->budget_from = $response['budget_from'];
                            $request->budget_to = $response['budget_to'];
                            $request->save();
                            $budgetValue = $response['budget_from'] . ' - ' . $response['budget_to'] . ' ' . $allCurrency[$response['currency']];
                        } else {
                            $request->budget_currency = null;
                            $request->budget_from = null;
                            $request->budget_to = null;
                            $request->save();
                            $budgetValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                        }
                    } else {
                        $request->budget_currency = null;
                        $request->budget_from = null;
                        $request->budget_to = null;
                        $request->save();
                        $budgetValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $budgetValue;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'departureDate':
                    if (!empty($response['value_start']) && !empty($response['value_end'])) {
                        $request->departure_date_start = \app\helpers\DateTimeHelper::convertDateToTimestamp($response['value_start']);
                        $request->departure_date_end = \app\helpers\DateTimeHelper::convertDateToTimestamp($response['value_end']);
                        $request->save();
                        $departureDateValue = $response['value_start'] . ' - ' . $response['value_end'];
                    } else {
                        $departureDateValue = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $departureDateValue;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'cities':
                    RequestCities::deleteAll(['request_id' => $request->id]);
                    if (!empty($response['Requests']['cities'])) {
                        if (count($response['Requests']['cities']) == 1){
                            $cities = new RequestCities();
                            $cities->request_id = $response['id'];
                            $cities->city = $response['Requests']['cities'];
                            $cities->save();
                            $value_cities = \app\helpers\MCHelper::getDepartureCityById($response['Requests']['cities'], $lang)[0]['title'];
                        } else {
                            foreach ($response['Requests']['cities'] as $city){
                                $cities = new RequestCities();
                                $cities->request_id = $response['id'];
                                $cities->city = $city;
                                $cities->save();
                                $cities_title = \app\helpers\MCHelper::getDepartureCityById($response['Requests']['cities'], $lang);
                                $value_cities = array_column($cities_title, 'title');
                            }
                        }
                    } else {
                        $value_cities = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $value_cities;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'countries':
                    RequestCountries::deleteAll(['request_id' => $request->id]);
                    if (!empty($response['Request']['countries'])) {
                        if (count($response['Request']['countries']) == 1){
                            $countries = new RequestCountries();
                            $countries->request_id = $response['id'];
                            $countries->country = $response['Request']['countries'];
                            $countries->save();
                            $value_countries = \app\helpers\MCHelper::getCountryById($response['Request']['countries'], $lang)[0]['title'];
                        } else {
                            foreach ($response['Request']['countries'] as $country) {
                                $countries = new RequestCountries();
                                $countries->request_id = $response['id'];
                                $countries->country = $country;
                                $countries->save();
                                $countries_title = \app\helpers\MCHelper::getCountryById($response['Request']['countries'], $lang);
                                $value_countries = array_column($countries_title, 'title');
                            }
                        }
                    } else {
                        $value_countries = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $value_countries;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'hotels':
                    RequestHotelCategories::deleteAll(['request_id' => $request->id]);
                    if (!empty($response['Requests']['hotel_category'])) {
                        if (count($response['Requests']['hotel_category']) == 1) {
                            !($response['id'] == 7 || $response['id'] == 8 || $response['id'] == 9) ? $star = '*' : $star = '';
                            $hotels = new RequestHotelCategories();
                            $hotels->request_id = $response['id'];
                            $hotels->hotel_category = $response['Requests']['hotel_category'];
                            $hotels->save();
                            $value_hotels = \app\helpers\MCHelper::getHotelCategoryById($response['Requests']['hotel_category'], $lang)[0]['title'] . $star;
                        } else {
                            foreach ($response['Requests']['hotel_category'] as $hotel){
                                $value_hotels = [];
                                $hotels = new RequestHotelCategories();
                                $hotels->request_id = $response['id'];
                                $hotels->hotel_category = $hotel;
                                $hotels->save();
                                $hotels_title = \app\helpers\MCHelper::getHotelCategoryById($response['Requests']['hotel_category'], $lang);
                                for($i=0; $i< count($hotels_title); $i++){
                                    ($hotels_title[$i]['title'] == '0' || $hotels_title[$i]['title'] == '1' || $hotels_title[$i]['title'] == '2' || $hotels_title[$i]['title'] == '3' ||$hotels_title[$i]['title'] == '4' || $hotels_title[$i]['title'] == '5')? $star='*': $star = '';
                                   $value_hotels[] = $hotels_title[$i]['title'] . $star;
                                }
                            }
                        }
                    }  else {
                        $value_hotels = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $value_hotels;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
                case 'type':
                    if (!empty($response['Requests']['type'])) {
                        $request->request_type_id = $response['Requests']['type'];
                        $request->save();
                        $requestTypes = RequestType::find()->where(['id' => $response['Requests']['type'], 'system' => false])->one()->value;
                    } else {
                        $requestTypes = TranslationHelper::getTranslation('not_set', $lang, 'Not set');
                    }
                    if($request->hasErrors())
                    {
                        $response['result'] = 'error';
                        $response['message'] = TranslationHelper::getTranslation('error',$lang);
                        break;
                    }
                    $response['new_value'] = $requestTypes;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
                    break;
            }
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionSetDescription()
    {
        $id = \Yii::$app->request->post('id');
        $description = \Yii::$app->request->post('description');
        $request = Requests::find()->where(['id'=>$id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->one();

        if(is_null($request))
            throw new HttpException(404);

        if (RbacHelper::can('requests.update.only_own')) {
            if($request->responsible != \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('requests.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id,\Yii::$app->user->identity->tenant->id);
            if(!empty($roles)) {
                $ids = RbacHelper::getAssignedUsers($roles[0]->roleName);
                if(array_search($request->responsible,$ids)===false)
                    throw new HttpException(403);
            }
            else
                if($request->responsible != \Yii::$app->user->id)
                    throw new HttpException(403);
        }
        if(isset($description) && !empty(trim($description))){
            $request->request_description = $description;
            $request->save();
        }else{
            if(empty(trim($description))) {
                $request->request_description = null;
                $request->save();
            }
        }
        return $this->redirect(['view?id='.$id]);
    }

    /**
     * @param $request_id
     *
     * @return array|mixed
     */
    public function actionSetStatus($request_id){
        $response = \Yii::$app->request->post();
        $request = Requests::find()->where(['id'=>$request_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->one();
        $status_id = RequestStatuses::find()->where(['type'=>$response['status']])->translate(\Yii::$app->user->identity->tenant->language->iso)->one();
        $request->request_status_id = $status_id->id;

        if($response['status']!=RequestStatuses::TYPE_CANCELED)
        $request->request_canceled_reason_id = null;

        $request->save();
        $response['result'] = 'success';
        $response['status'] = $status_id->name;
        $response['color'] = $status_id->color_type;

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /**
     * @param $request_id
     *
     * @return \yii\web\Response
     */
    public function actionSetCanceledReasons($request_id){
        $post = \Yii::$app->request->post();
        $request = Requests::find()->where(['id'=>$request_id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->one();
        if(isset($post['reason_close'])){
            $request->request_canceled_reason_id = $post['reason_close'];
            $request->save();
        };
        if(isset($post['other_reason'])&&$post['other_reason']!=''){
            $resoneOtherNew = new RequestCanceledReasons();
            $resoneOtherNew->type = RequestCanceledReasons::TYPE_OTHER;
            $resoneOtherNew->description = $post['other_reason'];
            $resoneOtherNew->save();
            $resoneOtherNewArr = $resoneOtherNew->toArray();
        }
        if(isset($resoneOtherNewArr['id'])) {
            $request->request_canceled_reason_id = $resoneOtherNewArr['id'];
            $request->save();
        }


        return $this->redirect('view?id=' . $request_id);
    }

    /**
     * @throws HttpException
     */
    public function actionAddService()
    {
        $post = \Yii::$app->request->post();
        $tenant_id = \Yii::$app->user->identity->tenant->id;
        $request_id = intval($post['request_id']);
        $request = Requests::find()->where(['id'=>$request_id])->one();
        if(is_null($request))
            throw new HttpException(400,'Request is not found!');
        ServicesUtility::createService($post,$request_id, $tenant_id, RequestsServicesLinks::class,
            RequestsServicesValues::class,RequestsServicesAdditionalLinks::class);
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionUpdateService()
    {
        $post = \Yii::$app->request->post();
        $tenant_id = \Yii::$app->user->identity->tenant->id;
        $request_id = intval($post['request_id']);
        $request = Requests::find()->where(['id'=>$request_id])->one();
        if(is_null($request))
            throw new HttpException(400,'Request is not found!');

        ServicesUtility::updateService($post,$request->id,$tenant_id,RequestsServicesLinks::class,
            RequestsServicesValues::class,RequestsServicesAdditionalLinks::class);
    }

    /**
     * @param $id
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionDeleteService($id)
    {
        $service = RequestsServicesLinks::find()->where(['id'=>intval($id)])->one();
        if(is_null($service) || $service->tenant_id!=\Yii::$app->user->identity->tenant->id)
            throw new HttpException('400',"Service not found!");
        $service->delete();
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionCommercialProposalsOnLink()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = \Yii::$app->request->post('services_id');
        $proposal = CommercialProposalsUtility::createProposal(CommercialProposals::TYPE_LINK, $data);

        if ($proposal === null)
            return false;

        return ['link' => \Yii::$app->params['baseUrl'] . '/ita/external/commercial-proposal?code=' . $proposal->link_code];
    }
}