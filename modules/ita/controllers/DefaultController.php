<?php

namespace app\modules\ita\controllers;

use app\helpers\RbacHelper;
use app\models\AgentsMapping;
use app\models\Invites;
use app\models\Tenants;
use app\modules\ita\controllers\base\BaseController;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Default controller for the `ita` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        $request = \Yii::$app->request;
        $params = $request->queryParams;
        $controller = isset($params['controller']) ? $params['controller'] : null;//isset($request[1]['controller']) ? $request[1]['controller'] : null;
        $action = isset($params['action']) ? $params['action'] : null;//isset($request[1]['action']) ? $request[1]['action'] : null;
        $url = '/ita/'.\Yii::$app->user->identity->tenant->id;
        if(!is_null($controller))
        {
            $url .= '/'.$controller;
        }
        if(!is_null($action))
        {
            $url .= '/'.$action;
        }
        if($request->queryString!='')
            $url.='?'.$request->queryString;
        return $this->redirect(Url::to($url));
    }

    /**
     * @param $token
     *
     * @return string * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAddUser($token)
    {
        $invite = Invites::find()->where(['token'=>$token])->andWhere(['user_id'=>\Yii::$app->user->id])->one();

        if (is_null($invite))
            throw new HttpException(404, 'Invite not found!');

        $agent = AgentsMapping::find()->where(['user_id'=>\Yii::$app->user->id])->andWhere(['tenant_id'=>$invite->tenant_id])->one();

        if (!is_null($agent))
            return $this->redirect(\Yii::$app->params['baseUrl']);

        $tenant = Tenants::find()->where(['id'=>$invite->tenant_id])->one();

        if (is_null($tenant))
            throw new HttpException(404,'Tenant not found!');

        $tenant->link('users', \Yii::$app->user->identity);

        if ($invite->user_role && $invite->user_role != 'system_admin') {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, $invite->tenant_id);

            foreach ($roles as $role) {
                if ($role->roleName != 'system_admin')
                    RbacHelper::unassignRole($role->roleName, \Yii::$app->user->id);
            }

            RbacHelper::assignRole($invite->user_role, \Yii::$app->user->id);
        }

        $invite->delete();

        return $this->render('added_user', ['tenant' => $tenant]);
    }

    public function actionBlocked()
    {
        return $this->render('blocked');
    }
}
