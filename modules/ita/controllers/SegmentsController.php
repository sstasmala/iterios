<?php

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\models\Segments;
use app\models\SegmentsRelations;
use app\models\SegmentsResultList;
use app\modules\ita\controllers\base\BaseController;
use app\helpers\RbacHelper;
use app\utilities\contacts\ContactsUtility;
use yii\web\HttpException;
use yii\web\Response;

class SegmentsController extends BaseController
{
    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);

        if(!$parent) return $parent;

        $lang = \Yii::$app->user->identity->tenant->language->iso;

        switch ($action->id)
        {
            case 'index':if(!RbacHelper::can('segments.read',['access']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'view':if(!RbacHelper::can('segments.read',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403);break;
            case 'create':if(!RbacHelper::can('segments.create.access') && !RbacHelper::owner())throw new HttpException(403);break;
            case 'delete':if(!RbacHelper::can('segments.delete',['full','only_own','only_own_role']) && !RbacHelper::owner())throw new HttpException(403, TranslationHelper::getTranslation('delete_forbidden_error', $lang));break;
        }
        return $parent;
    }

    private function normalizeData($data) {
        try {
            $items = json_decode($data,true);
        } catch (\Exception $e) {
            return '{}';
        }
        if(!isset($items['rules']))
            return $data;
        foreach ($items['rules'] as $k => $rule) {
            if(isset($rule['type']) && $rule['type'] == 'datetime') {
                $rule['value'] = DateTimeHelper::convertDateToTimestamp($rule['value']);
                $rule['value'] = DateTimeHelper::convertTimestampToDate($rule['value'],'Y-m-d');
                $items['rules'][$k] = $rule;
            }
        }
        return json_encode($items);

    }
    private function unnormalizeData($data) {
        try {
            $items = json_decode($data,true);
        } catch (\Exception $e) {
            return null;
        }
        if(!isset($items['rules']))
            return $data;
        foreach ($items['rules'] as $k => $rule) {
            if(isset($rule['type']) && $rule['type'] == 'datetime') {
                $rule['value'] = DateTimeHelper::convertDateToTimestamp($rule['value'],'Y-m-d');
                $rule['value'] = DateTimeHelper::convertTimestampToDate($rule['value']);
                $items['rules'][$k] = $rule;
            }
        }
        return json_encode($items);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $data = \Yii::$app->request->post('Segments', null);
        if($data == null)
            return $this->render('index');

        $data['rules'] = $this->normalizeData($data['rules']);
        $data['type'] = Segments::TYPE_PUBLIC;
        $data['name'] = [\Yii::$app->user->identity->tenant->language->iso => $data['name']];
        $model = new Segments();
        $model->loadWithLang(['Segments'=>$data]);
        if($model->save()) {
            $link = new SegmentsRelations();
            $link->id_segment = $model->id;
            $link->type = Segments::TYPE_PUBLIC;
            $link->id_tenant = \Yii::$app->user->identity->tenant->id;
            $link->save();
            return $this->redirect('index');
        }
        return $this->redirect('index');
    }

    public function actionUpdate()
    {
        $data = \Yii::$app->request->post('Segments', null);
        if($data == null)
            return $this->render('index');

        $data['rules'] = $this->normalizeData($data['rules']);
        $data['type'] = Segments::TYPE_PUBLIC;
        $data['name'] = [\Yii::$app->user->identity->tenant->language->iso => $data['name']];

        if(!isset($data['id']))
            throw new HttpException(400);

        $model = Segments::find()->where(['id'=>(int)$data['id']])->andWhere(['<>','type',Segments::TYPE_SYSTEM])->one();
        if($model == null)
            throw new HttpException(400);
        $model->loadWithLang(['Segments'=>$data]);
        if($model->save()) {
//            $link = new SegmentsRelations();
//            $link->id_segment = $model->id;
//            $link->type = Segments::TYPE_PUBLIC;
//            $link->id_tenant = \Yii::$app->user->identity->tenant->id;
//            $link->save();
            return $this->redirect('index');
        }
        return $this->redirect('index');
    }

    public function actionView($id)
    {
        $view_params = [
            'segment' => [
                'id' => $id
            ]
        ];
        return $this->render('view', $view_params);
    }
    
    public function actionGetSegment(){
        if (\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            $segment = Segments::find()->where(['id'=>(int)$post['id']])->asArray()->one();
            if($segment == null)
                throw new HttpException(400);
//            if($segment['type'] == Segments::TYPE_SYSTEM)
//                throw new HttpException(400);
            $segment['rules'] = $this->unnormalizeData($segment['rules']);
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $segment;
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(null === $model)
            throw new HttpException(404);

        if (RbacHelper::can('segments.update.only_own_role')) {
            $roles = RbacHelper::getAssignedRoles(\Yii::$app->user->id, \Yii::$app->user->identity->tenant->id);
            if (!empty($roles)) {
                $ids_r = RbacHelper::getAssignedUsers($roles[0]->roleName);
            }
        }

        if (RbacHelper::can('segments.delete.only_own')) {
            if($model->responsible_id !== \Yii::$app->user->id)
                throw new HttpException(403);
        }

        if (RbacHelper::can('segments.delete.only_own_role')) {
            if(isset($ids_r)) {
                if(array_search($model->responsible_id, $ids_r) === false)
                    throw new HttpException(403);
            }
            else
                if($model->responsible_id != \Yii::$app->user->id)
                    throw new HttpException(403);
        }

        $model->delete();

        return $this->redirect('index');
    }

    /**
     * @return array
     */
    public function actionGetData() {
        $request = \Yii::$app->request->get();
        $meta = $request['pagination'];
        $query = SegmentsRelations::find()->with('segment')->where(['id_tenant'=>\Yii::$app->user->identity->tenant->id]);

        if(isset($request['query']['type'])) {
            if($request['query']['type'] == 'system')
                $query->andWhere(['type'=>Segments::TYPE_SYSTEM]);
            if($request['query']['type'] == 'my')
                $query->andWhere(['type'=>Segments::TYPE_PUBLIC]);
        }

        $data = $query->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        $meta['total'] = \count($data);

        $query->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $data = $query->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();


        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['meta' => $meta, 'data' => $data];
    }

    public function actionGetSegmentData() {

        $request = \Yii::$app->request->get();
        $meta = $request['pagination'];
        $id = $request['query']['segment_id'] ?? null;
        if($id == null)
            throw new HttpException(400);
        $ids = SegmentsResultList::find()->where(['id_relation'=>(int)$id]);
        $meta['total'] = $ids->count();
        $ids->limit($meta['perpage'])->offset($meta['perpage'] * ($meta['page'] - 1));
        $ids = $ids->asArray()->all();
        $ids = array_column($ids,'id_contact');
        $data = ContactsUtility::getContactsInfo($ids,\Yii::$app->user->identity->tenant->language->iso);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['meta' => $meta, 'data' => $data];
    }

    public function actionDeleteSegment() {
        $id = \Yii::$app->request->post('id',null);
        if($id == null)
            throw new HttpException(400);

        $segment = Segments::find()->where(['id'=>(int)$id])->one();
        if($segment == null || $segment->type == Segments::TYPE_SYSTEM)
            throw new HttpException(400);
        $segment->delete();
    }
}
