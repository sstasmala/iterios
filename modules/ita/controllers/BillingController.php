<?php

namespace app\modules\ita\controllers;

use app\helpers\DateTimeHelper;
use app\models\FeaturesTariffs;
use app\models\Tariffs;
use app\models\TariffsFeature;
use app\models\TariffsOrders;
use app\models\Transactions;
use app\modules\ita\controllers\base\BaseController;
use yii\web\Response;

class BillingController extends BaseController
{
    // My orders actions
    public function actionMyOrdersIndex()
    {
        return $this->render('my-orders/index');
    }
    
    // Rates actions
    public function actionRatesIndex()
    {
        $tenant = \Yii::$app->user->identity->tenant;
        $tariffs = Tariffs::find()->translate($tenant->language->iso)->orderBy(['price'=>SORT_ASC])->asArray()->all();

        $country = $tenant->country;
        $tariffs = \array_filter($tariffs,function($item)use($country){
            if(empty($item['country']))
                return true;
            $cn = \json_decode($item['country'],true);
            return \array_search($country,$cn) !== false;
        });

        $ids = \array_column($tariffs,'id');
        $links = FeaturesTariffs::find()->where(['in','tariff_id',$ids])->asArray()->all();
        $fids = \array_column($links,'feature_id');
        $features = TariffsFeature::find()->where(['in','id',$fids])->translate($tenant->language->iso)->asArray()->all();
        $features = \array_combine(\array_column($features,'id'),$features);

        foreach ($tariffs as $k => $tariff) {
            if($tariff['id'] == $tenant->tariff_id)
                $tariff['current'] = true;
            else
                $tariff['current'] = false;
            $ls = \array_filter($links,function($item)use($tariff){
                return $item['tariff_id']==$tariff['id'];
            });
            foreach ($ls as $v) {
                $tariff['features'][$v['feature_id']] = ($v['count']!==null)?$v['count']:true;
            }

            foreach ($features as $f) {
                if(!isset($tariff['features'][$f['id']]))
                    $tariff['features'][$f['id']] = false;
                if($f['countable'] && $tariff['features'][$f['id']]===true)
                    $tariff['features'][$f['id']] = null;
            }
            $tariffs[$k] = $tariff;
        }
        return $this->render('rates/index',compact('tariffs','features'));
    }

    public function actionGetAccountInfo() {
        $balance = 0;
        $tenant = \Yii::$app->user->identity->tenant;
        if($tenant != null)
            $balance = $tenant->balance;

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'balance' => $balance
        ];
    }

    public function actionLoadOrders() {
        $orders = TariffsOrders::find()->where(['customer_id'=>\Yii::$app->user->identity->tenant->id])
            ->orderBy(['duration_date'=>SORT_DESC])->asArray()->all();
        $ids = \array_column($orders,'tariff_id');
        $tariffs = Tariffs::find()->where(['in','id',$ids])->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->all();
        $tariffs = \array_combine(\array_column($tariffs,'id'),$tariffs);

        foreach ($orders as $k => $v) {
            $orders[$k]['name'] = $tariffs[$v['tariff_id']]['name'];
            $orders[$k]['price'] = $tariffs[$v['tariff_id']]['price'];
            $end = (new \DateTime())->setTimestamp($v['duration_date']);
            $now = (new \DateTime());
            $diff = $end->diff($now);
            $orders[$k]['days_left'] = ($diff->invert)?$diff->days:0;
            $orders[$k]['actual_date'] = DateTimeHelper::convertTimestampToDate($tariffs[$v['tariff_id']]['tariff_end']);
        }


        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $orders;
    }

    public function actionGetTransactions()
    {
        $page = \Yii::$app->request->get('page',1);
        $offset = \Yii::$app->request->get('offset',5);

        $transactions = Transactions::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->all();
        $total = \count($transactions);
        $transactions = Transactions::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->limit($offset)->offset(($page-1)*$offset)->orderBy(['updated_at'=>SORT_DESC])->asArray()->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => $transactions,
            'meta' => [
                'page' => $page,
                'offset' => $offset,
                'total' => $total
            ]
        ];
    }
}
