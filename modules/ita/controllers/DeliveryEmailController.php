<?php

namespace app\modules\ita\controllers;

use app\components\email_provider\base\EmailProviderInterface;
use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\models\Contacts;
use app\models\ContactsEmails;
use app\models\EmailsDispatch;
use app\models\Providers;
use app\models\ProvidersCredentials;
use app\models\Segments;
use app\models\SegmentsResultList;
use app\modules\ita\controllers\base\BaseController;
use app\models\SegmentsRelations;
use app\utilities\contacts\ContactsUtility;
use yii\web\HttpException;
use yii\web\Response;

class DeliveryEmailController extends BaseController
{
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        return $this->render('index');
    }

    public function actionCreate()
    {
        $segments = SegmentsRelations::find()
            ->with(['segment'])
            ->where(['id_tenant' => \Yii::$app->user->identity->tenant->id])->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();
        $providers = ProvidersCredentials::find()
            ->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->asArray()->all();

        $ids = \array_column($providers,'provider_id');
        $providers_info = Providers::find()->where(['in','id',$ids])->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        $providers_info = \array_combine(\array_column($providers_info,'id'),$providers_info);

        foreach ($providers as $k => $provider)
            $providers[$k]['provider'] = $providers_info[$provider['provider_id']];
        return $this->render('create', [
            'segments' => $segments,
            'providers' => $providers
        ]);
    }

    public function actionView($id)
    {
        $data = EmailsDispatch::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->andWhere(['id'=>(int)$id])->one();
        if($data == null)
            throw new HttpException(404);

        $params = $data->provider->params;
        if (!empty($params)) {
            $config = \json_decode($params, true);
            $data_s = [];
            if (!empty($data->provider->provider_data))
                $data_s = \json_decode($data->provider->provider_data, true);
            if(!empty($data_s)) {
                $class = $data->provider->provider->provider_class;

                /**
                 * @var $dispatch EmailProviderInterface
                 */
                $dispatch = new $class($config, $data->tenant, $data_s);

                $info = $data->provider_data;
                if(empty($info)) {
                    $data->stat_update = \time();
                    $data->save();
                } else {
                    $info = \json_decode($info,true);
                    if($data->stat_update == 0) {
                        $data->stat_update = \time();
                        $data->save();
                    }
                }

                $data_s = $data->statistic_data;
                if(empty($data_s)) {
                    $data_s = [
                        'total' => 0,
                        'opened_total' => 0,
                        'clicked_total' => 0,
                        'unique_opens' => 0,
                        'unique_clicks' => 0,
                        'last_open' => 0,
                        'last_click' => 0,
                        'not_delivered' => 0,
                        'unsubscribed' => 0,
                        'open_rate' => 0,
                        'click_rate' => 0,
                        'history' => [],
                        'links' => [],
                        'emails' => [],
                        'locations' => []
                    ];
                } else {
                    $data_s =\json_decode($data_s,true);
                }

                $stat = $dispatch->getStatistic($info,$data_s);
                if($stat != false) {
                    $data->clicks = $stat['unique_clicks']??0;
                    $data->opens = $stat['unique_opens']??0;
                    $data->statistic_data = \json_encode($stat);
                    $data->save();
                }
            }
        }

        $data = $data->toArray();

        $ref = [];
        if(!empty($data['statistic_data'])) {
            $info = \json_decode($data['statistic_data'],true);
            $locations = $info['locations'];

            $countries = \array_column($locations,'country');
            $countries = \array_unique($countries);

            foreach ($countries as $country){
                $res = MCHelper::searchCountry($country,\Yii::$app->user->identity->tenant->language->iso);
                $res = \array_filter($res,function ($item)use($country){return $item['iso_code']==$country;});
                if(!empty($res))
                    $ref[$country] = \reset($res);
            }
        }

        return $this->render('view', [
            'delivery' => $data,
            'countries' => $ref
        ]);
    }

    public function actionGetIndex(){
        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $data = EmailsDispatch::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id]);

        $meta['total'] = \count($data->all());

        $data = $data->with('owner')->limit($meta['perpage'])->offset($meta['perpage']*($meta['page']-1))->asArray()->all();

        $segments = \array_column($data,'receivers');
        $segments = \array_map(function($item){ return json_decode($item);},$segments);

        $segments = \array_merge(...$segments);
        $segments = \array_unique($segments);

        $relations = SegmentsResultList::find()->where(['in','id_relation',$segments])->asArray()->all();
        $ids = \array_column($relations,'id_contact');
        $contacts = ContactsUtility::getContactsInfo($ids,\Yii::$app->user->identity->tenant->language->iso);

        $info = [];
        foreach ($segments as $segment){
            $rls = \array_filter($relations,function ($item) use ($segment){
                return $item['id_relation'] == $segment;
            });
            $cids = \array_column($rls,'id_contact');
            $cnts = \array_filter($contacts,function ($item) use ($cids){
                return (\array_search($item['id'],$cids)!== false);
            });
            $emails = \array_column($cnts,'emails');
            if(!empty($emails)) {
                $emails = \array_merge(...$emails);
                $emails = \array_column($emails, 'value');
            }
            $info[$segment] = $emails;
        }

        foreach ($data as $k => $item) {
            if(!empty($item['date']))
                $data[$k]['date'] = DateTimeHelper::convertTimestampToDate($item['date']);

            $data[$k]['errors'] = (!empty($item['errors'])?\count(\json_decode($item['errors'],true)):0);
            $data[$k]['errors_data'] = (!empty($item['errors'])?\json_decode($item['errors'],true):[]);
            $receivers = (!empty($item['receivers'])?\json_decode($item['receivers']):[]);
            $contacts = \array_filter($info,function ($item) use ($receivers) {
                return (\array_search($item,$receivers)!==false);
            },ARRAY_FILTER_USE_KEY);
            $contacts = \array_unique(\array_merge(...$contacts));
            $data[$k]['receivers'] = \count($contacts);
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data'=>$data,'meta'=>$meta];
    }


    public function actionGetProviderInfo($id) {
        /**
         * @var $data ProvidersCredentials
         */
        $data = ProvidersCredentials::find()->where(['id'=>(int)$id])
            ->andWhere(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->one();

        if($data ==  null)
            throw new HttpException(404);

        $provider_data = [];
        $provider = $data->provider->translate(\Yii::$app->user->identity->tenant->language->iso);
        $provider_data['name'] = $provider->name;
        $provider_data['description'] = $provider->description;
        $provider_data['site'] = $provider->website;
        $provider_data['total_sub'] = 0;

        $params = $data->params;
        if (!empty($params)) {
            $config = \json_decode($params, true);
            $data_s = [];
            if (!empty($data->provider_data))
                $data_s = \json_decode($data->provider_data, true);
            if (!empty($data_s)) {
                $class = $data->provider->provider_class;

                /**
                 * @var $dispatch EmailProviderInterface
                 */
                $dispatch = new $class($config, \Yii::$app->user->identity->tenant, $data_s);

                $ad_data = $dispatch->getProviderInfo();
                if($ad_data !== false)
                    $provider_data['total_sub'] = $ad_data['total_sub']??0;
            }
        }
        $provider_data['total_contacts'] = \count(Contacts::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->all());
        $provider_data['dispatches'] = \count(EmailsDispatch::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])->all());
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $provider_data;
    }


    public function actionGetUsersInfo($id) {

        $data = EmailsDispatch::find()->where(['tenant_id'=>\Yii::$app->user->identity->tenant->id])
            ->andWhere(['id'=>(int)$id])->one();
        if($data == null)
            throw new HttpException(404);

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');

        $segments_ids = $data->receivers;
        $segments_ids = \json_decode($segments_ids,true);
        $segments = SegmentsRelations::find()->where(['id_tenant'=>$data->tenant->id])->andWhere(['in','id',$segments_ids])->asArray()->all();
        $s_ids = \array_column($segments,'id_segment');
        $segments_info = Segments::find()->where(['in','id',$s_ids])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        $ids = \array_column($segments,'id');
        $relations = SegmentsResultList::find()->where(['in','id_relation',$ids])->asArray()->all();
        $ids = \array_column($relations,'id_contact');
        $contacts = ContactsUtility::getContactsInfo($ids,\Yii::$app->user->identity->tenant->language->iso);

        $emails = [];
        if(!empty($data->statistic_data)) {
            $info = \json_decode($data->statistic_data,true);
            $emails = $info['emails'];

            $emails = \array_combine(\array_column($emails,'email'),$emails);
        }
        $meta['total'] = \count($emails);
        $emails = \array_slice($emails,$meta['perpage']*($meta['page']-1),$meta['perpage']);
        foreach ($contacts as $contact)
        {
            foreach ($contact['emails'] as $email) {
                if(isset($emails[$email['value']])) {
                    $name = '';
                    if(!empty($contact['first_name']))
                        $name = $contact['first_name'];
                    if(!empty($contact['middle_name'])) {
                        if(!empty($name))
                            $name .= ' ';
                        $name .= $contact['middle_name'];
                    }
                    if(!empty($contact['last_name'])) {
                        if(!empty($name))
                            $name .= ' ';
                        $name .= $contact['last_name'];
                    }

                    $emails[$email['value']]['name'] = $name;
                    $emails[$email['value']]['id'] = $contact['id'];
                    $id_c = $contact['id'];
                    $sgs = \array_filter($relations,function($item)use($id_c){
                        return $item['id_contact'] == $id_c;
                    });
                    $sgs = \array_column($sgs,'id_relation');
                    $sgs = \array_filter($segments,function ($item)use($sgs){
                       return (\array_search($item['id'],$sgs)!==false);
                    });
                    $sgs = \array_column($sgs,'id_segment');
                    $segs = \array_filter($segments_info,function ($item)use($sgs){
                        return (\array_search($item['id'],$sgs)!==false);
                    });
                    $result = \implode(', ',\array_column($segs,'name'));
                    $emails[$email['value']]['segments'] = $result;
                }
            }
        }


        $data = $emails;
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data'=>$data,'meta'=>$meta];
    }

    public function actionGetSegmentsInfo() {
        $request = \Yii::$app->request->get();

        $search_data = \Yii::$app->request->get('search');
        $meta = \Yii::$app->request->get('pagination');
        $data = [];
        if(isset($request['query']) && isset($request['query']['segments'])) {
            $segments = SegmentsRelations::find()->where(['in','id',$request['query']['segments']])
                ->andWhere(['id_tenant'=>\Yii::$app->user->identity->tenant->id])->asArray()->all();

            $relations = \array_column($segments,'id');

            $w_emails = ContactsEmails::find()->asArray()->all();
            $w_emails = \array_column($w_emails,'contact_id');

            $results = SegmentsResultList::find()->where(['in','id_relation',$relations])->andWhere(['in','id_contact',$w_emails])
                ->limit($meta['perpage'])->offset($meta['perpage']*($meta['page']-1))->asArray()->all();

            $contacts_ids = \array_column($results,'id_contact');

            $meta['total'] = \count($contacts_ids);
            $contacts = ContactsUtility::getContactsInfo($contacts_ids,\Yii::$app->user->identity->tenant->language->iso);

            $contacts = \array_filter($contacts,function ($item){return !empty($item['emails']);});
            foreach ($contacts as $contact)
            {
                $data [] = [
                    'id' => $contact['id'],
                    'first_name' => $contact['first_name'],
                    'middle_name' => $contact['middle_name'],
                    'last_name' => $contact['last_name'],
                    'emails'=>\array_column($contact['emails'],'value')
                ];
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data'=>$data,'meta'=>$meta];
    }


    public function actionCreateDispatch()
    {
        $data = \Yii::$app->request->post();
        if(!isset($data['EmailsDispatch']))
            throw new HttpException(400);
        $dispatch = $data['EmailsDispatch'];

        if(!isset($dispatch['name'])
            || !isset($dispatch['name'])
            || !isset($dispatch['subject'])
            || !isset($dispatch['provider_id'])
            || !isset($dispatch['email'])
            || !isset($dispatch['email_type'])
            || !isset($dispatch['segments'])
            || empty($dispatch['segments']))
            throw new HttpException(400);
        $model = new EmailsDispatch();
        $model->name = $dispatch['name'];
        $model->subject = $dispatch['subject'];
        $model->provider_id = $dispatch['provider_id'];
        $model->email = $dispatch['email'];
        $model->email_type = $dispatch['email_type'];
        $model->receivers = \json_encode($dispatch['segments']);
        $model->owner_id = \Yii::$app->user->identity->getId();
        $model->tenant_id = \Yii::$app->user->identity->tenant->id;
        $model->date = time();
        $model->save();
        return $model->id;
    }
}
