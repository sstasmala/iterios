<?php

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;
use yii\web\Controller;

class DashboardController extends BaseController
{
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        \Yii::$app->view->params['subheader'] = [
            'show' => true
        ];
        /* Set data */
        $data = [];
        $data['number_of_tourists'] = 1223;
        $data['number_of_requests'] = 1349;
        $data['amount_of_deals'] = 567;
        $data['number_of_bookings'] = 276;
        return $this->render('index',compact('data'));
    }
}