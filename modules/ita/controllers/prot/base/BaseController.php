<?php
/**
 * BaseController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers\prot\base;

use Yii;
use yii\web\Response;
use app\models\fake\FakeIdentity;
use app\models\Languages;
use app\models\Tenants;
use app\models\User;
use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        $this->setFakeIdentity();

        return parent::beforeAction($action);
    }

    public function setFakeIdentity()
    {
//        $user = new User();
//        $tenant = new Tenants();
//        $language = new Languages();
//
//        $user->id = 1;
//        $user->first_name = 'Ivan';
//        $user->last_name = 'Ivanov';
//        $user->middle_name = 'Ivanovich';
//        $user->email = 'example@email.com';
//        $user->phone = '+380123456789';
//
//        $tenant->id = 1;
//        $tenant->name = 'Fake tenant';
//        $tenant->owner_id = 1;
//        $tenant->language_id = 1;
//
//        $language->id = 1;
//        $language->name = 'Russian';
//        $language->original_name = 'русский язык';
//        $language->iso = 'ru';
//
//        $tenant->language = $language;
//        $user->tenant = $tenant;
//
        Yii::$app->user->setIdentity((new FakeIdentity()));
    }
    public function actionGetCountriesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $countries = [];
        if(isset($data['search']) && !empty($data['search'])){
            $search_param = $data['search'];
            // Return filtered countries
            $countries = [
                [
                    "id" => 4,
                    "iso_code" => "AG",
                    "title" => "Антигуа и Барбуда"
                ],
                [
                    "id" => 243,
                    "iso_code" => "VU",
                    "title" => "Вануату"
                ]
            ];
        } else {
            //Search param not set
            //Return all countries
            $countries = [
                [
                    "id" => 4,
                    "iso_code" => "AG",
                    "title" => "Антигуа и Барбуда"
                ],
                [
                    "id" => 243,
                    "iso_code" => "VU",
                    "title" => "Вануату"
                ],
                [
                    "id" => 87,
                    "iso_code" => "GP",
                    "title" => "Гваделупа"
                ],
                [
                    "id" => 91,
                    "iso_code" => "GT",
                    "title" => "Гватемала"
                ],
                [
                    "id" => 92,
                    "iso_code" => "GU",
                    "title" => "Гуам"
                ],
                [
                    "id" => 134,
                    "iso_code" => "LT",
                    "title" => "Литва"
                ],
                [
                    "id" => 166,
                    "iso_code" => "NI",
                    "title" => "Никарагуа"
                ],
                [
                    "id" => 177,
                    "iso_code" => "PG",
                    "title" => "Папуа-Новая Гвинея"
                ],
                [
                    "id" => 187,
                    "iso_code" => "PY",
                    "title" => "Парагвай"
                ],
                [
                    "id" => 231,
                    "iso_code" => "UA",
                    "title" => "Украина"
                ],
                [
                    "id" => 235,
                    "iso_code" => "UY",
                    "title" => "Уругвай"
                ],
                [
                    "id" => 63,
                    "iso_code" => "EC",
                    "title" => "Эквадор"
                ],
                [
                    "id" => 88,
                    "iso_code" => "GQ",
                    "title" => "Экваториальная Гвинея"
                ]
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $countries;
    }
    
    public function actionGetCompaniesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $companies = [];
        if(isset($data['search']) && !empty($data['search'])){
            $search_param = $data['search'];
            // Return filtered companies
            $companies = [
                [
                    "id" => 1,
                    "name" => "Germans inc"
                ],
                [
                    "id" => 4,
                    "name" => "Wayer"
                ]
            ];
        } else {
            //Search param not set
            //Return all companies
            $companies = [
                [
                    "id" => 1,
                    "name" => "Germans inc"
                ],
                [
                    "id" => 2,
                    "name" => "Funny papers"
                ],
                [
                    "id" => 3,
                    "name" => "Bugs dealers LRD"
                ],
                [
                    "id" => 4,
                    "name" => "Wayer"
                ],
                [
                    "id" => 5,
                    "name" => "Bords and Birds"
                ],
                [
                    "id" => 6,
                    "name" => "Food maker"
                ]
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $companies;
    }
    
    public function actionGetResponsibleOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $responsible = [];
        if(isset($data['search']) && !empty($data['search'])){
            $search_param = $data['search'];
            // Return filtered companies
            $responsible = [
                [
                    'id' => 2,
                    'first_name' => 'Lazy',
                    'middle_name' => '',
                    'last_name' => 'Timmy',
                ],
                [
                    'id' => 3,
                    'first_name' => 'Николай',
                    'middle_name' => 'Петрович',
                    'last_name' => 'Крузенштерн',
                ],
            ];
        } else {
            //Search param not set
            //Return all companies
            $responsible = [
                [
                    'id' => 1,
                    'first_name' => 'Vasya',
                    'middle_name' => 'Ivanovich',
                    'last_name' => 'Pupkin',
                ],
                [
                    'id' => 2,
                    'first_name' => 'Lazy',
                    'middle_name' => '',
                    'last_name' => 'Timmy',
                ],
                [
                    'id' => 3,
                    'first_name' => 'Николай',
                    'middle_name' => 'Петрович',
                    'last_name' => 'Крузенштерн',
                ],
                [
                    'id' => 4,
                    'first_name' => 'Виталий',
                    'middle_name' => 'Васильевич',
                    'last_name' => 'Васнецов',
                ],
                [
                    'id' => 5,
                    'first_name' => 'Max',
                    'middle_name' => '',
                    'last_name' => 'Madness',
                ],
                [
                    'id' => 6,
                    'first_name' => 'Jimm',
                    'middle_name' => '',
                    'last_name' => 'Blackwater',
                ]
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $responsible;
    }
}