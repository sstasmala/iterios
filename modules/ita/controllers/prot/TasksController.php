<?php

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;
use yii\web\Controller;

class TasksController extends BaseController
{
    public function actionIndex()
    {
        $data = [];
        return $this->render('index',compact('data'));
    }
}