<?php
/**
 * ProfileController.php
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;

class ProfileController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'change-profile-picture') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        \Yii::$app->view->params['subheader'] = [
            'show' => true
        ];
        /* Set data */
        $data = [];
        $data['number_of_tourists'] = 200;
        $data['number_of_requests'] = 355;
        $data['number_of_bookings'] = 180;
        $data['photo'] = 'metronic/assets/app/media/img/users/user4.jpg';
        $data['email'] = 'mark.andre@gmail.com';
        $data['first_name'] = 'Mark';
        $data['last_name'] = 'Andre';
        $data['middle_name'] = '';
        $data['phone'] = '+380995554433';

        return $this->render('index', compact('data'));
    }

    public function actionPersonalDetails()
    {
        return true;
    }

    public function actionChangeEmail()
    {
        return true;
    }

    public function actionChangePassword()
    {
        return true;
    }

    public function actionChangeProfilePicture()
    {
        return (isset($_FILES['profile_picture']) ? true : false);
    }
}
