<?php
/**
 * ContactsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;
use app\helpers\TranslationHelper;
use yii\web\Controller;
use yii\web\Response;
use Yii;

class ContactsController extends BaseController
{
    
    public $template;
    public $types_template;


    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }
    
    public function init()
    {
        $this->template = [
            'id' => 1,
            'first_name' => 'Vasya',
            'middle_name' => 'Ivanovich',
            'last_name' => 'Pupkin',
            'date_of_birth' => time(),
            'sex' => 1,
            'discount' => '',
            'nationality' => 231,
            'tin' => '3223322',
            'type' => 'New',
            'passports' => [
                [
                    'id' => 10,
                    'type_id' => 1,
                    'first_name' => 'Jack',
                    'last_name' => 'Richer',
                    'serial' => 'AA654321',
                    'country' => 231,
                    'nationality' => 231,
                    'birth_date' => time(),
                    'date_limit' => time(),
                    'issued_date' => time(),
                    'issued_owner' => 'Argentina Passports Holders'
                ],
                [
                    'id' => 11,
                    'type_id' => 2,
                    'first_name' => 'Jack',
                    'last_name' => 'Richer',
                    'serial' => 'BC123456',
                    'country' => 231,
                    'nationality' => 231,
                    'birth_date' => time(),
                    'date_limit' => time(),
                    'issued_date' => time(),
                    'issued_owner' => 'American Express'
                ],
                [
                    'id' => 12,
                    'type_id' => 3,
                    'first_name' => 'Jack',
                    'last_name' => 'Richer',
                    'serial' => 'BC123456',
                    'country' => 231,
                    'nationality' => 231,
                    'birth_date' => time(),
                    'date_limit' => time(),
                    'issued_date' => time(),
                    'issued_owner' => 'American Express'
                ],
            ],
            'visas' => [
                [
                    'id' => 1,
                    'type_id' => 1,
                    'country' => 231,
                    'begin_date' => time(),
                    'end_date' => time(),
                    'description' => 'Short text about this visa'
                ],
                [
                    'id' => 2,
                    'type_id' => 2,
                    'country' => 231,
                    'begin_date' => time(),
                    'end_date' => time(),
                    'description' => 'Short text about this visa'
                ],
                [
                    'id' => 3,
                    'type_id' => 3,
                    'country' => 231,
                    'begin_date' => time(),
                    'end_date' => time(),
                    'description' => 'Short text about this visa'
                ]
            ],
            'company' => [
                'id' => 1,
                'name' => 'Some company'
            ],
            'source' => 'Some source',
            'tags' => [
                ['id' => 1, 'link' => '#', 'name' => 'Baged'],
                ['id' => 2, 'link' => '#', 'name' => 'Colored'],
                ['id' => 3, 'link' => '#', 'name' => 'Fixed'],
                ['id' => 4, 'link' => '#', 'name' => 'Lorem ipsum']
            ],
            'phones' => [
                [
                    'id' => 1,
                    'type_id'=> 1,
                    'value'=>'+380671231234'
                ],
                [
                    'id' => 2,
                    'type_id'=> 2,
                    'value'=>'+380953125476'
                ],
                [
                    'id' => 3,
                    'type_id'=> 3,
                    'value'=>'+380678816800'
                ],
                [
                    'id' => 4,
                    'type_id'=> 2,
                    'value'=>'+380243338877'
                ],
                [
                    'id' => 5,
                    'type_id'=> 1,
                    'value'=>'+380657892154'
                ],
            ],
            'emails' =>[
                [
                    'id' => 1,
                    'type_id' => 2,
                    'value' => 'example@email.com'
                ],
                [
                    'id' => 2,
                    'type_id' => 2,
                    'value' => 'example2@email.com'
                ],
                [
                    'id' => 3,
                    'type_id' => 3,
                    'value' => 'example3@email.com'
                ],
            ],
            'socials' => [
                [
                    'id' => 1,
                    'type_id' => 1,
                    'value' => '#fb_url'
                ],
                [
                    'id' => 2,
                    'type_id' => 2,
                    'value' => '#vk_url'
                ],
                [
                    'id' => 3,
                    'type_id' => 3,
                    'value' => '#ln_url'
                ],
            ],
            'messengers' => [
                [
                    'id' => 1,
                    'type_id' => 1,
                    'value' => '#'
                ],
                [
                    'id' => 2,
                    'type_id' => 2,
                    'value' => '#'
                ],
                [
                    'id' => 3,
                    'type_id' => 3,
                    'value' => '#'
                ],
            ],
            'sites' => [
                [
                    'id' => 1,
                    'type_id' => 1,
                    'value' => 'http://example1.com'
                ],
                [
                    'id' => 2,
                    'type_id' => 2,
                    'value' => 'http://example2.ru'
                ],
                [
                    'id' => 3,
                    'type_id' => 3,
                    'value' => 'http://example3.com.ua'
                ],
            ],
            'residence_address' => [
                'country' => 231,
                'region' => 'Golden river',
                'city' => 'Vankuver',
                'street' => 'Elm st.',
                'house' => '23',
                'flat' => '12',
                'postcode' => '12345'
            ],
            'living_address' => [
                'country' => 231,
                'region' => 'Гомельская обл.',
                'city' => 'с. Малиновка',
                'street' => 'ул. Титова',
                'house' => '12',
                'flat' => '4',
                'postcode' => '12345'
            ],
            'responsible' => [
                'id' => 1,
                'first_name' => 'Vasya',
                'middle_name' => 'Ivanovich',
                'last_name' => 'Pupkin',
            ],
            'created_at' => time(),
            'updated_at' => time(),
        ];
        $this->types_template = [
            'phones' => [
                '1' => [
                    'id' => 1,
                    'name' => 'Phone type 1'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'Phone type 2'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'Phone type 3'
                ],
            ],
            'emails' => [
                '1' => [
                    'id' => 1,
                    'name' => 'Email type 1'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'Email type 2'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'Email type 3'
                ],
            ],
            'socials' => [
                '1' => [
                    'id' => 1,
                    'name' => 'facebook',
                    'icon' => '<i class="fa fa-facebook"></i>'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'vk',
                    'icon' => '<i class="fa fa-vk"></i>'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'linkedin',
                    'icon' => '<i class="fa fa-linkedin"></i>'
                ],
            ],
            'messengers' => [
                '1' => [
                    'id' => 1,
                    'name' => 'viber',
                    'icon' => '<i class="socicon-viber"></i>'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'whatsapp',
                    'icon' => '<i class="fa fa-whatsapp"></i>'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'telegram',
                    'icon' => '<i class="fa fa-telegram"></i>'
                ],
            ],
            'sites' => [
                '1' => [
                    'id' => 1,
                    'name' => 'Sites type 1'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'Sites type 2'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'Sites type 3'
                ],
            ],
            'passports' => [
                '1' => [
                    'id' => 1,
                    'name' => 'Passport type 1'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'Passport type 2'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'Passport type 3'
                ],
            ],
            'visas' => [
                '1' => [
                    'id' => 1,
                    'name' => 'Visa type 1'
                ],
                '2' => [
                    'id' => 2,
                    'name' => 'Visa type 2'
                ],
                '3' => [
                    'id' => 3,
                    'name' => 'Visa type 3'
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];

        \Yii::$app->view->params['subheader'] = [
            'show' => false
        ];

        return $this->render('index', [
            'tags' => []
        ]);
    }

    /**
     *
     */
    public function actionGetData()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        
        $data = [];
        $t1 = $this->template;
        $t1['tags'] = null;
        $t1['id'] = 1;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['tags'] = [['id' => 1, 'name' => 'Taaaaaaaaaaag #1']];
        $t1['id'] = 2;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['phones'] = [];
        $t1['emails'] = [];
        $t1['id'] = 3;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['phones'] = [['type'=>'Office','value'=>'+380999999999']];
        $t1['emails'] = ['example@email.com'];
        $t1['id'] = 4;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['phones'] = [];
        $t1['emails'] = ['example@email.com'];
        $t1['id'] = 5;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['phones'] = [['type'=>'Office','value'=>'+380999999999']];
        $t1['emails'] = [];
        $t1['id'] = 6;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['phones'] = [['type'=>'Office','value'=>'+380999999999']];
        $t1['emails'] = [];
        $t1['id'] = 7;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['first_name'] = 'Vasa';
        $t1['middle_name'] = 'Ivanovich';
        $t1['last_name'] = 'PupkinLongLastNameHere';
        $t1['id'] = 8;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['first_name'] = 'Vasa';
        $t1['middle_name'] = null;
        $t1['last_name'] = 'PupkinLongLastNameHere';
        $t1['id'] = 9;
        $data[] = $t1;

        $t1 = $this->template;
        $t1['first_name'] = 'Vasa';
        $t1['middle_name'] = 'Ivanovich';
        $t1['last_name'] = null;
        $t1['id'] = 10;
        $data[] = $t1;

        for ($i = 0; $i<10; $i++)
        {
            $new = $this->template;
            $new['id'] = $i+10;
            $data[] = $new;
        }
        $meta = \Yii::$app->request->get('pagination');
        $meta['total'] = 100;
        return ['data'=>$data,'meta'=>$meta];
    }
    
    public function actionSetParamAjax()
    {
        if (\Yii::$app->request->isAjax) {
            //BUISENESS LOGIC
            $response = \Yii::$app->request->post();
            switch ($response['property']){
                case 'date_of_birth':
                    //logic
                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'sex':
                    //logic
                    if($response['value'] == '0'){
                        $response['new_value'] = TranslationHelper::getTranslation('female_sex', Yii::$app->user->identity->tenant->language->iso, 'Female');
                    }
                    if($response['value'] == '1'){
                        $response['new_value'] = TranslationHelper::getTranslation('male_sex', Yii::$app->user->identity->tenant->language->iso, 'Male');
                    }
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'discount':
                    //logic
                    $response['new_value'] = $response['value'] . '%';
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'type':
                    //logic
                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'nationality':
                    //logic
                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'tin':
                    //logic
                    $response['new_value'] = $response['value'];
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'company':
                    //logic
                    $response['new_value'] = 'New company name';
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'living_address':
                    //logic
                    /* Generate new adress row */
                    $arderss_row = '';
                    if (!empty($response['living_address'])) {
                        if (isset($response['living_address']['country']) && !empty($response['living_address']['country'])) {
                            $arderss_row .= $response['living_address']['country'] . ', ';
                        }
                        if (isset($response['living_address']['region']) && !empty($response['living_address']['region'])) {
                            $arderss_row .= $response['living_address']['region'] . ', ';
                        }
                        if (isset($response['living_address']['city']) && !empty($response['living_address']['city'])) {
                            $arderss_row .= $response['living_address']['city'] . ', ';
                        }
                        if (isset($response['living_address']['street']) && !empty($response['living_address']['street'])) {
                            $arderss_row .= $response['living_address']['street'];
                            if (isset($response['living_address']['house']) && !empty($response['living_address']['house'])) {
                                $arderss_row .= ' ' . $response['living_address']['house'];
                                if (isset($response['living_address']['flat']) && !empty($response['living_address']['flat'])) {
                                    $arderss_row .= '/' . $response['living_address']['flat'];
                                }
                            }
                            $arderss_row .= ', ';
                        }
                        if (isset($response['living_address']['postcode']) && !empty($response['living_address']['postcode'])) {
                            $arderss_row .= $response['living_address']['postcode'] . ', ';
                        }
                        $arderss_row = substr(trim($arderss_row), 0, -1);
                    } else {
                        $arderss_row = 'Not set';
                    }
                    /* Set response */
                    $response['new_value'] = $arderss_row;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                case 'residence_address':
                    //logic
                    /* Generate new adress row */
                    $arderss_row = '';
                    if (!empty($response['residence_address'])) {
                        if (isset($response['residence_address']['country']) && !empty($response['residence_address']['country'])) {
                            $arderss_row .= $response['residence_address']['country'] . ', ';
                        }
                        if (isset($response['residence_address']['region']) && !empty($response['residence_address']['region'])) {
                            $arderss_row .= $response['residence_address']['region'] . ', ';
                        }
                        if (isset($response['residence_address']['city']) && !empty($response['residence_address']['city'])) {
                            $arderss_row .= $response['residence_address']['city'] . ', ';
                        }
                        if (isset($response['residence_address']['street']) && !empty($response['residence_address']['street'])) {
                            $arderss_row .= $response['residence_address']['street'];
                            if (isset($response['residence_address']['house']) && !empty($response['residence_address']['house'])) {
                                $arderss_row .= ' ' . $response['residence_address']['house'];
                                if (isset($response['residence_address']['flat']) && !empty($response['residence_address']['flat'])) {
                                    $arderss_row .= '/' . $response['residence_address']['flat'];
                                }
                            }
                            $arderss_row .= ', ';
                        }
                        if (isset($response['residence_address']['postcode']) && !empty($response['residence_address']['postcode'])) {
                            $arderss_row .= $response['residence_address']['postcode'] . ', ';
                        }
                        $arderss_row = substr(trim($arderss_row), 0, -1);
                    } else {
                        $arderss_row = 'Not set';
                    }
                    /* Set response */
                    $response['new_value'] = $arderss_row;
                    $response['result'] = 'success';
                    $response['message'] = TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved');
                    break;
                default :
                    $response['result'] = 'error';
                    $response['new_value'] = $response['value'];
                    $response['message'] = TranslationHelper::getTranslation('ajax_error_message_undefined_object_property', Yii::$app->user->identity->tenant->language->iso, 'Undefined object property') . ' ' . $response['property'];
                    break;
            }
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
    
    public function actionChangeResponsible()
    {
        if (!\Yii::$app->request->isAjax) {
            return;
        }
        $post = \Yii::$app->request->post();
        $contact_id = $post['contact_id'];
        $responsible_id = $post['responsible_id'];
        
        // Work with database...
        
        /* Create response */
        $response = [
            'result' => 'success', // success|error
            'message' => TranslationHelper::getTranslation('saved', Yii::$app->user->identity->tenant->language->iso, 'Saved')
        ];
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    public function actionCreate()
    {
        return true;
    }

    public function actionSelectAll()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $ids = [];
        for($i = 1; $i <= 100; $i++)
            $ids[] = $i;

        return $ids;
    }
    
    public function actionView($id)
    {
        /* Post processing */
        $post = \Yii::$app->request->post();
        /*if(!empty($post)){
            echo '<h1>Post processing...</h1>';
            echo '<pre>';
            print_r($post);
            echo '</pre>';
            die;
        }*/

        /* Default data */
        return $this->render('view', [
            'contact' => $this->template,
            'contacts_types' => $this->types_template
        ]);
    }
    
    public function actionDelete($id)
    {
        return true;
    }

    public function actionMassDelete($ids)
    {
        return true;
    }

    public function actionGetContacts($email = false, $phone = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = [
            [
                'id'    => 1,
                'photo' => '/metronic/assets/app/media/img/users/100_4.jpg',
                'email' => 'example@email.com',
                'phone' => '+380999999999',
                'name'  => 'Pupkina Anna Ivanovna'
            ],
            [
                'id'    => 2,
                'photo' => '/metronic/assets/app/media/img/users/100_14.jpg',
                'email' => 'example2@email.com',
                'phone' => '+380888888888',
                'name'  => 'Pupkin Vasya Ivanovich'
            ],
            [
                'id'    => 3,
                'photo' => '/metronic/assets/app/media/img/users/100_6.jpg',
                'email' => 'example3@email.com',
                'phone' => '+380777777777',
                'name'  => 'Pupkin Ivan Vasilevich'
            ]
        ];

        if ( ! $email && ! $phone) {
            return null;
        }

        $search_result = array_filter($data, function ($arr) use ($email, $phone) {
            if ($email && $phone) {
                return (stristr($arr['email'], $email) !== false && stristr($arr['phone'], $phone) !== false);
            }

            return (($email != false && stristr($arr['email'], $email) !== false) ||
                ($phone != false && stristr($arr['phone'], $phone) !== false));
        });

        return array_values($search_result);
    }
}