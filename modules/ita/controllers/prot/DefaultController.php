<?php

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `ita` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        /* Set params */
        \Yii::$app->view->params['header'] = [
            'show_search_panel' => true
        ];
        $request = \Yii::$app->urlManager->parseRequest(\Yii::$app->request);
        $controller = isset($request[1]['controller']) ? $request[1]['controller'] : null;
        $action = isset($request[1]['action']) ? $request[1]['action'] : null;
        $url = '/ita/'.\Yii::$app->user->identity->tenant->id;
        if(!is_null($controller))
            $url .= '/'.$controller;
        if(!is_null($action))
            $url .= '/'.$action;
        $this->redirect(Url::to($url));
    }
}
