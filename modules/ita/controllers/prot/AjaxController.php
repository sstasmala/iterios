<?php

namespace app\modules\ita\controllers\prot;

use app\modules\ita\controllers\prot\base\BaseController;
use Yii;
use yii\web\Response;
use app\helpers\TranslationHelper;

class AjaxController extends BaseController
{

    public function actionNotifications() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $items_per_load = intval($data['items_per_load']);
            $offset = intval($data['offset']);
            $response = [];
            $notifications = self::getFakeNotifications($items_per_load, $offset);
            $response['notifications'] = $notifications;
            $response['stack_finished'] = ((empty($notifications)) || (count($notifications) < $items_per_load)) ? true : false;
            $response['new_offset'] = ((empty($notifications)) || (count($notifications) < $items_per_load)) ? false : ($items_per_load + $offset);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public static function getFakeNotifications($items_per_load, $offset) {
        $notifications = [];
        
        /* Notification template */
        $item = [
            'id' => 1,
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'attach_object' => [
                'link' => '#',
                'text' => 'Attach link'
            ],
            'was_read' => true,
            'created_at' => 'Just now'
        ];
        
        /* Fake notifications array */
        for ($i = 1; $i < 201; $i++) {
            $item['was_read'] = ($i < 15) ? false : true;
            $was_read_class = ($item['was_read']) ? ' m-list-timeline__item--read' : '';
            $read_marker_text = ($item['was_read']) ? 'Mark as unread' : 'Mark as read';
            $notifications[] = <<<HDS
                    <div class="m-list-timeline__item{$was_read_class}" data-notification-id="{$i}">
                        <div class="read_marker" title="{$read_marker_text}"></div>
                        <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                        <div class="m-list-timeline__text">
                            <div class="notification-content">{$i}. {$item['text']}</div>
                            <div class="attach-link">
                                <a href="{$item['attach_object']['link']}">{$item['attach_object']['text']}</a>
                            </div>
                        </div>
                        <span class="m-list-timeline__time">{$item['created_at']}</span>
                    </div>
HDS;
        }

        /* Filter and return*/
        return array_slice($notifications, $offset, $items_per_load);
    }
    
    public function actionSearchTags() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            
            $response = [
                ['id' => 1, 'link' => '#', 'name' => 'Baged'],
                ['id' => 2, 'link' => '#', 'name' => 'Colored'],
                ['id' => 3, 'link' => '#', 'name' => 'Fixed'],
                ['id' => 4, 'link' => '#', 'name' => 'Lorem ipsum']
            ];
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
    
    public function actionGetTagsList() {
        if (!Yii::$app->request->isAjax) {
            return;
        }
        $data = Yii::$app->request->post();
        if(!$data['search']){
            return;
        }
        $response = [];
        // Searching
        switch ($data['search']) {
            case 'default':
                $response = [
                    [
                        'id' => 1,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 2,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 3,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 4,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 5,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 6,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 7,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 8,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 9,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 10,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 11,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 12,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 13,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 14,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 15,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 16,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 17,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 18,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 19,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 20,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 21,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 22,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 23,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 24,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ],
                    [
                        'id' => 25,
                        'name' => 'Baged',
                        'contact_uses' => 12,
                        'company_uses' => 5,
                        'created_at' => '22.06.2015'
                    ],
                    [
                        'id' => 26,
                        'name' => 'Colored',
                        'contact_uses' => 17,
                        'company_uses' => 9,
                        'created_at' => '13.11.2016'
                    ],
                    [
                        'id' => 27,
                        'name' => 'Fixed',
                        'contact_uses' => 7,
                        'company_uses' => 2,
                        'created_at' => '03.07.2015'
                    ],
                    [
                        'id' => 28,
                        'name' => 'Upgraded',
                        'contact_uses' => 73,
                        'company_uses' => 25,
                        'created_at' => '08.02.2017'
                    ]
                ];
                break;
        }
        // Response
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
    
    public function actionUpdateTag(){
        if (!Yii::$app->request->isAjax) {
            return;
        }
        $data = Yii::$app->request->post();
        if(!empty($data['value'])){
            // Save tag code...
            $response['new_value'] = $data['value'];
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('saved', $lang, 'Saved');
        }else{
            $response['message'] = TranslationHelper::getTranslation('error', $lang, 'Error');
            $response['result'] = 'error';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
    
    public function actionDeleteTag(){
        if (!Yii::$app->request->isAjax) {
            return;
        }
        $data = Yii::$app->request->post();
        $response = [];
        //Delete code...
        if(true){  //CHANGE ON REAL DELETE RESULT
            // Save tag code...
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('deleted', $lang, 'Deleted');
        }else{
            $response['message'] = TranslationHelper::getTranslation('error', $lang, 'Error');
            $response['result'] = 'error';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
    
    public function actionCreateTag(){
        if (!Yii::$app->request->isAjax) {
            return;
        }
        $data = Yii::$app->request->post();
        $response = [];
        //Saving code...
        if(true){  //CHANGE ON REAL CREATE RESULT
            $response['result'] = 'success';
            $response['message'] = TranslationHelper::getTranslation('tag_was_created', $lang, 'Tag was created');
        }else{
            $response['message'] = TranslationHelper::getTranslation('error', $lang, 'Error');
            $response['result'] = 'error';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /* Disable csef validation */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

}
