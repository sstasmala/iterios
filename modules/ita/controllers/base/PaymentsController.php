<?php
/**
 * PaymentsController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */

namespace app\modules\ita\controllers\base;


use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;
use app\helpers\TranslationHelper;
use app\models\Orders;
use app\models\OrdersExRates;
use app\models\OrdersServicesLinks;
use app\models\OrdersTouristsPayments;
use app\models\ServicesFieldsDefault;
use app\utilities\orders\OrdersUtility;
use app\utilities\services\ServicesUtility;
use Yii;
use yii\web\HttpException;
use yii\web\Response;

class PaymentsController extends BaseController
{
    /**
     * @param $id
     * @return array
     * @throws HttpException
     */
    public function actionGetInfo($id)
    {
        /**
         * @var $order Orders
         */
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);


        Yii::$app->response->format = Response::FORMAT_JSON;
        return OrdersUtility::getPaymentInfo($id);
    }



    /**
     * @param $id
     * @return mixed
     * @throws HttpException
     */
    public function actionGetOrderDiscount($id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        $value['value'] = $order->discount;
        $value['type'] = $order->discount_type==0?'per':'cur';

        $data['value'] = json_encode($value);
        $data['cur'] = $currencies[$order->currency]['iso_code'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }


    public function actionSetDiscount($id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);

        $data = Yii::$app->request->post();

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        if(isset($data['discount']) && !empty($data['discount'])){
            $discount = json_decode($data['discount'],true);
            $order->discount = $discount['value'];
            $order->discount_type = $discount['type']=='per'?0:1;
            $order->save();
        }
        $discount = $order->discount;
        if($order->discount_type == 0) {
            $discount .= '%';
        }
        else {
            $discount .= ' '.$currencies[$order->currency]['iso_code'];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response['new_value'] = $discount;
        $response['result'] = 'success';
        $response['message'] = TranslationHelper::getTranslation('saved', \Yii::$app->user->identity->tenant->language->iso);
//        self::calculateTotal($id);
        return $response;
    }
    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     * @throws HttpException
     */
    public function actionGetTouristsPayments($id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);

        $payments = OrdersTouristsPayments::find()->where(['order_id'=>$order->id])->andWhere(['type'=>OrdersTouristsPayments::TYPE_TOURIST])->asArray()->all();

        $payments = array_map(function($item){
            $item['date'] = (!empty($item['date']))?DateTimeHelper::convertTimestampToDate($item['date']):DateTimeHelper::convertTimestampToDate(time());
            return $item;
        },$payments);

        $ids = OrdersServicesLinks::find()->where(['order_id'=>$id])
            ->andWhere(['in','status',[OrdersServicesLinks::STATUS_ACCEPTED,OrdersServicesLinks::STATUS_WAIT]])->asArray()->all();
        $l_ids = array_column($ids,'id');
        $s_ids = array_combine($l_ids,array_column($ids,'service_id'));
        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
        $services = array_combine(array_column($services,'id'),$services);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getOrdersValues($l_ids);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);

        $data = [];

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        foreach ($services as $k => $service) {
            $sum = array_filter($service['fields_def'], function ($item) {
                return $item['type'] == ServicesFieldsDefault::TYPE_SERVICES_PRICE;
            });
            $cur = array_filter($service['fields_def'], function ($item) {
                return $item['type'] == ServicesFieldsDefault::TYPE_CURRENCY;
            });

            if(!empty($sum) && !empty($cur)){
                $sum = reset($sum);
                $sum = (isset($sum['value']))?$sum['value']:0;

                $cur = reset($cur);
                $cur = (isset($cur['value']['id']))?$cur['value']:null;
            }
            else {
                $sum = 0;
                $cur = null;
            }

            $payed = array_filter($payments,function ($item)use($k){ return $item['link_id'] == $k;});

            $payed_cur = array_sum(array_column($payed,'sum'));

            $payed = array_map(function($a){
                if($a['ext_rates']!==null && $a['ext_rates']!=''){
                    $rate = $a['ext_rates'];
                    $a['sum'] = $a['sum']/$rate;
                }
                else
                    $a['sum'] = 0;
                return $a['sum'];
            },$payed);

            $payed = array_sum($payed);

            $data[] = [
                'id' => $k,
                'sum' => $sum,
                'cur' => $cur,
                'payed' => $payed,
                'payed_cur' =>$payed_cur
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['payments'=>$payments,'services'=>$data,'cur'=>$currencies[$order->currency]];
    }

    /**
     * @param $id
     * @return bool
     * @throws HttpException
     */
    public function actionSaveTouristPayments($id)
    {
        /**
         * @var $order Orders|null
         */
        $order = Orders::find()->where(['id'=>(int)$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);

        $payment = Yii::$app->request->post('Payment',null);
        if($payment == null)
            throw new HttpException(400);

        if(empty($payment['sum'])|| empty($payment['exchange_rate']) || !isset($payment['service_id']))
            throw new HttpException(400);

        if(isset($payment['id']))
            $record = OrdersTouristsPayments::find()->where(['id'=>$payment['id']])->one();
        else
            $record = new OrdersTouristsPayments();

        if(is_null($record))
            throw new HttpException(400);

        $record->ext_rates = $payment['exchange_rate'];
        $record->link_id = $payment['service_id'];
        $record->order_id = $id;
        $record->date = (!empty($payment['date']))?DateTimeHelper::convertDateToTimestamp($payment['date']):null;
        $record->sum = $payment['sum'];
        $record->note = $payment['note'];
        $record->save();

        $prev_sum = $order->total_sum;
        $data = OrdersUtility::getPaymentInfo($order->id);
        if($prev_sum != $data['sum'])
            $data['message'] = TranslationHelper::getTranslation('ov_page_payment_diff_warning');

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /**
     * @param $id
     * @param $payment_id
     * @return bool
     * @throws HttpException
     */
    function actionRemoveTouristsPayment($id,$payment_id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(400,'Order not found!');

        $record = OrdersTouristsPayments::find()->where(['id'=>$payment_id])->one();

        if(is_null($record))
            throw new HttpException(400,'Payment not found!');
        $record->delete();
        return true;
    }

    /**
     * @param $id
     * @param $payment_id
     * @return bool
     * @throws HttpException
     */
    function actionRemoveProvidersPayment($id,$payment_id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(400,'Order not found!');

        $record = OrdersTouristsPayments::find()->where(['id'=>$payment_id])->one();

        if(is_null($record))
            throw new HttpException(400,'Payment not found!');
        $record->delete();
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws HttpException
     */
    public function actionSaveProviderPayments($id)
    {
        $order = Orders::find()->where(['id'=>$id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404,'Order not found!');

        $payment = Yii::$app->request->post('Payment',null);
        $service_id = Yii::$app->request->post('service_id');
        if($payment == null)
            throw new HttpException(400,'Payment not found!');

        if(empty($payment['sum'])|| empty($payment['exchange_rate']))
            throw new HttpException(400,'Missing sum or exchange rate!');

        if(isset($payment['id']))
            $record = OrdersTouristsPayments::find()->where(['id'=>$payment['id']])->one();
        else
            $record = new OrdersTouristsPayments();

        if(is_null($record))
            throw new HttpException(400);

        $record->ext_rates = $payment['exchange_rate'];
        $record->link_id =$service_id;
        $record->order_id = $id;
        $record->date = (!empty($payment['date']))?DateTimeHelper::convertDateToTimestamp($payment['date']):null;
        $record->sum = $payment['sum'];
        $record->note = $payment['note'];
        $record->type = OrdersTouristsPayments::TYPE_PROVIDER;
        $record->save();
        $data = OrdersUtility::getPaymentInfo($order->id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /**
     * @param $order_id
     * @param $link_id
     * @return array
     * @throws HttpException
     */
    public function actionGetProviderPayments($order_id,$link_id){
        $order = Orders::find()->where(['id'=>$order_id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
        if($order === null)
            throw new HttpException(404);

        $link = OrdersServicesLinks::find()->where(['id' => $link_id])->asArray()->one();
        if($link == null)
            throw new HttpException(404);

        $payments = OrdersTouristsPayments::find()->where(['order_id'=>$order->id])->andWhere(['link_id'=>$link_id])
            ->andWhere(['type'=>OrdersTouristsPayments::TYPE_PROVIDER])->asArray()->all();

        $payments = array_map(function($item){
            $item['date'] = (!empty($item['date']))?DateTimeHelper::convertTimestampToDate($item['date']):null;
            return $item;
        },$payments);

        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
        $currencies = array_combine(array_column($currencies,'id'),$currencies);

        $l_ids = array_column([$link],'id');
        $s_ids = array_combine($l_ids,array_column([$link],'service_id'));
        $service = ServicesUtility::getServices($link['service_id'],\Yii::$app->user->identity->tenant->language->iso);
        $services = array_combine(array_column($service,'id'),$service);
        foreach ($s_ids as $k=>$v)
            $s_ids[$k] = $services[$v];
        $values = ServicesUtility::getOrdersValues([$link_id]);
        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);
        $service = reset($services);

        $cur = ServicesUtility::getServiceCurrency($service);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['payments'=>$payments,'services'=>[],'cur'=>$currencies[$order->currency],'service_cur'=>$cur];
    }


    /**
     * @param $order_id
     * @throws HttpException
     */
//    public static function calculateTotal($order_id)
//    {
//        $order = Orders::find()->where(['id'=>$order_id])->andWhere(['tenant_id'=>Yii::$app->user->identity->tenant->id])->one();
//        if($order === null)
//            throw new HttpException(404);
//
//        $ex_rates = OrdersExRates::find()->where(['order_id'=>$order_id])->asArray()->all();
//        $ex_rates = array_combine(array_column($ex_rates,'currency'),$ex_rates);
//
//        $ids = OrdersServicesLinks::find()->where(['order_id'=>$order_id])->asArray()->all();
//        $l_ids = array_column($ids,'id');
//        $s_ids = array_combine($l_ids,array_column($ids,'service_id'));
//        $services = ServicesUtility::getServices($s_ids,\Yii::$app->user->identity->tenant->language->iso);
//        $services = array_combine(array_column($services,'id'),$services);
//        foreach ($s_ids as $k=>$v)
//            $s_ids[$k] = $services[$v];
//        $values = ServicesUtility::getOrdersValues($l_ids);
//        $services = ServicesUtility::fillServices($s_ids,$values,\Yii::$app->user->identity->tenant);
//
//        $data = [];
//
//        $currencies = MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso);
//        $currencies = array_combine(array_column($currencies,'id'),$currencies);
//
//        $total_sum = null;
//        $suppliers_sum = null;
////        if(!empty($ex_rates))
//        foreach ($services as $service)
//        {
//            $sum = array_filter($service['fields_def'],function($item){return $item['type']==ServicesFieldsDefault::TYPE_SERVICES_PRICE;});
//            $cur = array_filter($service['fields_def'],function($item){return $item['type']==ServicesFieldsDefault::TYPE_CURRENCY;});
//            if(!empty($sum) && !empty($cur))
//            {
//                $cur_id = reset($cur);
//                if(!isset($cur_id['value']['id']))
//                    continue;
//                $cur_id = $cur_id['value']['id'];
//                if(!isset($ex_rates[$cur_id]) && $cur_id!=$order->currency)
//                {
//                    $total_sum = null;
//                    break;
//                }
//                $sum = reset($sum);
//                $sum = (isset($sum['value']))?$sum['value']: 0;
//                if($cur_id == $order->currency)
//                    $total_sum += $sum;
//                else
//                    $total_sum += $sum*$ex_rates[$cur_id]['value'];
//            }
//            $sr_sum = array_filter($service['fields_def'],function($item){return $item['type']==ServicesFieldsDefault::TYPE_PAYMENT_TO_SUPPLIER;});
//
//            if(!empty($sr_sum) && !empty($cur) && isset(reset($cur)['value']))
//            {
//                $cur_id = reset($cur)['value']['id'];
//                if(!isset($ex_rates[$cur_id]) && $cur_id!=$order->currency)
//                {
//                    $suppliers_sum = null;
//                    break;
//                }
//                $sr_sum = reset($sr_sum);
//                $sr_sum = (isset($sr_sum['value']))?$sr_sum['value']:$sr_sum = 0;
//                if($cur_id == $order->currency)
//                    $suppliers_sum += $sr_sum;
//                else
//                    $suppliers_sum += $sr_sum*$ex_rates[$cur_id]['value'];
//            }
//        }
//
//        $discount = null;
//
//        $total_sum_wd = $total_sum;
//
//        if($total_sum!==null && $suppliers_sum!==null) {
//
//            if($order->discount!==null) {
//                if($order->discount_type == 0) {
//                    $total_sum = $total_sum - $total_sum/100*$order->discount;
//                }
//                else {
//                    $total_sum = $total_sum - $order->discount;
//                }
//            }
//        }
////
////        $data['total_sum'] = $total_sum;
////        $data['total_sum_wd'] = $total_sum_wd;
//        $order->total_sum = $total_sum;
//        $order->total_sum_wd = $total_sum_wd;
//        $order->save();
//    }

}