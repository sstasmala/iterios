<?php
/**
 * Created by PhpStorm.
 * User: zapleo
 * Date: 30.01.18
 * Time: 15:46
 */

namespace app\modules\ita\controllers\base;

use app\helpers\MCHelper;
use app\helpers\TranslationHelper;
use app\models\AgentsMapping;
use app\models\Companies;
use app\models\CompaniesKindActivities;
use app\models\CompaniesStatuses;
use app\models\CompaniesTypes;
use app\models\Orders;
use app\models\Suppliers;
use app\models\Tasks;
use app\models\Translations;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeAction($action)
    {

        if (\Yii::$app->user->getIsGuest()) {
            $url = \Yii::$app->request->getUrl();

            $this->redirect('/site/index' . (!empty($url) ? '?redirect_url=' . base64_encode($url) : ''));
            return false;
        }

        if (!\Yii::$app->user->identity->tenant) {
            $this->redirect('/');

            return false;
        }

        if (!\Yii::$app->user->identity->tenant->checkUser()) {
            $user_tenant = AgentsMapping::findOne(['user_id' => \Yii::$app->user->id]);

            $user = User::findOne(\Yii::$app->user->id);
            $user->current_tenant_id = !is_null($user_tenant) ? $user_tenant->tenant_id : null;
            $user->save();
        }

        if ($action->id != 'blocked') {
            $agent_map = AgentsMapping::findOne(['user_id'   => \Yii::$app->user->id, 'tenant_id' => \Yii::$app->user->identity->tenant->id]);

            if ($agent_map->block === true) {
                $this->redirect('/ita/blocked');

                return false;
            }
        }
        return parent::beforeAction($action);
    }

    public function render($view, $params = [])
    {
        TranslationHelper::getPageTranslation(Yii::$app->controller->getRoute(),Yii::$app->user->identity->tenant->language->iso);
        return parent::render($view, $params);
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \yii\web\HttpException
     */
    public function actionGetCountriesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $search = Yii::$app->request->post('search', '');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = MCHelper::searchCountry($search, Yii::$app->user->identity->tenant->language->iso);
        if(is_null($result))
            return [];
        return $result;
    }
    
    public function actionGetCityesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $search = Yii::$app->request->post('search', null);
        $country = Yii::$app->request->post('country', null);
        $page = Yii::$app->request->post('page', 1);
        $result = [];
        if(is_array($country)){
            foreach ($country as $one_country){
                $query_res = MCHelper::searchCity($search, Yii::$app->user->identity->tenant->language->iso, $one_country);
                if(is_null($query_res))
                    $query_res = [];
                $result = array_merge($result, $query_res);
            }
        } else {
            $result = MCHelper::searchCity($search, Yii::$app->user->identity->tenant->language->iso, $country);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(is_null($result))
            return ['results'=>[],'total_count'=>0];
        $count = count($result);
        $result = array_slice($result,($page-1)*10,10);
        return ['results'=>$result,'total_count'=>$count];
    }
    
    public function actionGetDepartureCityesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $search = Yii::$app->request->post('search', null);
        $result = MCHelper::searchDepartureCity($search, Yii::$app->user->identity->tenant->language->iso);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if(is_null($result))
            return [];
        return $result;
    }
    
    public function actionGetHotelCategory()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search', null);
        $result = MCHelper::searchHotelCategory($search, Yii::$app->user->identity->tenant->language->iso);
        if(is_null($result))
            return [];
        return $result;
    }
    
    public function actionGetAirports()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search', null);
        $result = MCHelper::getAirports($search, Yii::$app->user->identity->tenant->language->iso);
        if(is_null($result))
            return [];
        return $result;
    }
    
    public function actionGetFoodOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search', null);
        $result = MCHelper::getFoodOptions($search, Yii::$app->user->identity->tenant->language->iso);
        if(is_null($result))
            return [];
        return $result;
    }
    
    public function actionGetHotelsOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search', null);
        $country = Yii::$app->request->post('country', null);
        $page = Yii::$app->request->post('page', 1);
        $result = MCHelper::getHotelsOptions($search, Yii::$app->user->identity->tenant->language->iso,($country!=''?$country:null));
        if(empty($result))
            return ['results'=>[],'total_count'=>0];
        $count = count($result);
        $result = array_slice($result,(($page-1)*10),10);
        return ['results'=>$result,'total_count'=>$count];
    }

    public function actionGetCompaniesTypesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $query = CompaniesTypes::find();

        if(isset($data['search']) && !empty($data['search'])){
            $query->andWhere(['ilike', 'value', $data['search']]);
        }

        $companies_types = $query->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $companies_types;
    }

    public function actionGetCompaniesKindActivitiesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $query = CompaniesKindActivities::find();

        if(isset($data['search']) && !empty($data['search'])){
            $query->andWhere(['ilike', 'value', $data['search']]);
        }

        $companies_ka = $query->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $companies_ka;
    }

    public function actionGetCompaniesStatusesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $query = CompaniesStatuses::find();

        if(isset($data['search']) && !empty($data['search'])){
            $query->andWhere(['ilike', 'value', $data['search']]);
        }

        $companies_statuses = $query->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $companies_statuses;
    }

    /**
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function actionGetCompaniesOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $query = Companies::find()->where(['tenant_id'=>Yii::$app->user->identity->tenant->id]);
        if(isset($data['search']) && !empty($data['search'])){
            $query->andWhere(['ilike','name',$data['search']]);
        }
        $companies = $query->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $companies;
    }

    /**
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function actionGetOrdersOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $data = Yii::$app->request->post();
        $query = Orders::find()->where(['orders.tenant_id'=>Yii::$app->user->identity->tenant->id]);

        if (isset($data['search']) && !empty($data['search'])){
            $query->andFilterWhere(['like', "CONCAT(contacts.last_name, ' ', contacts.first_name)", $data['search']])
                ->orFilterWhere(['ilike', "CONCAT(contacts.last_name, ' ', contacts.first_name)", $data['search']]);
        }

        $query->joinWith('contact');
        $query->orderBy(['contacts.first_name' => SORT_ASC]);
        $orders = $query->asArray()->all();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $orders;
    }

    /**
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function actionGetResponsibleOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $data = Yii::$app->request->post();
        $query = AgentsMapping::find()->where(['tenant_id'=>Yii::$app->user->identity->tenant->id])->asArray()->all();
        $ids = array_column($query,'user_id');
        $query = User::find();
        if(isset($data['search']) && !empty($data['search'])){
            $query->orFilterWhere(['ilike','first_name',$data['search']]);
            $query->orFilterWhere(['ilike','middle_name',$data['search']]);
            $query->orFilterWhere(['ilike','last_name',$data['search']]);
            $parts = explode(' ',$data['search']);
            foreach ($parts as $v)
            {
                $query->orFilterWhere(['ilike','first_name',$v]);
                $query->orFilterWhere(['ilike','middle_name',$v]);
                $query->orFilterWhere(['ilike','last_name',$v]);
            }
        }
        $query->andWhere(['in','id',$ids]);
        $responsible = $query->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $responsible;
    }

    /**
     * @return array
     */
    public function actionGetCountOverdueTask()
    {
        $tasks = Tasks::find()
            ->where(['tenant_id' => Yii::$app->user->identity->tenant->id])
            ->andWhere(['assigned_to_id' => Yii::$app->user->id])
            ->andWhere(['<', 'due_date', time()])->andWhere(['status' => false])
            ->count();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['count' => $tasks];
    }



    public function actionGetSuppliersOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $search = Yii::$app->request->post('search', null);
        $result = [];
        if(!is_null($search) && !empty($search))
        {
            $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
            $translate->orFilterWhere(['like', 'value',  $search])
                ->orFilterWhere(['ilike', 'value',  $search])
                ->andWhere('"key" ILIKE :table', [':table'=>"suppliers.name.%"]);
            $translate->addParams([':div' => '.', ':index' => 3]);
            $translate = $translate->asArray()->column();
            $result = Suppliers::find()->where(['ilike','name',$search]);

            $result->orWhere(['in', 'id', array_unique($translate)]);
            $result = $result->asArray()->all();
        }

        else
            $result = Suppliers::find()->translate(\Yii::$app->user->identity->tenant->language->iso)->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(is_null($result))
            return [];
        return $result;
    }

    public function afterAction($action, $result)
    {
        $session = Yii::$app->session;
        if($session->has('cur_tenant'))
            $session->remove('cur_tenant');
        return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
    }
}