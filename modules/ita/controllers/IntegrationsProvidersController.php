<?php

namespace app\modules\ita\controllers;

use app\models\ProviderFunction;
use app\models\Providers;
use app\models\ProvidersCredentials;
use app\models\ProviderType;
use app\models\Translations;
use app\modules\ita\controllers\base\BaseController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class IntegrationsProvidersController extends BaseController
{
    public function actionIndex()
    {
        $provider_types = ProviderType::find()
            ->orderBy(['value' => SORT_ASC])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();

        return $this->render('index', [
            'provider_types' => $provider_types
        ]);
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = \Yii::$app->request->post('providers');

        $providers = Providers::find();

        $provider_cred_ids = ProvidersCredentials::find()
            ->select('provider_id')
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->column();

        if ($data['integrations'] === 'all') {
            $providers->where(['not in', 'id', $provider_cred_ids]);

            if (!empty($data['type'])) {
                $providers->andWhere(['provider_type_id' => $data['type']]);
            }

            if (!empty($data['search_text'])) {
                $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
                $translate->orFilterWhere(['like', 'value',  $data['search_text']])
                    ->orFilterWhere(['ilike', 'value',  $data['search_text']])
                    ->andWhere('"key" ILIKE :table', [':table'=> "providers.name.%"]);
                $translate->addParams([':div' => '.', ':index' => 3]);
                $translate = $translate->column();

                $providers->andWhere(['in', 'id', $translate]);
            }
        } else {
            $providers->where(['in', 'id', $provider_cred_ids]);
        }

        $count_providers = $providers->count();

        $providers = $providers->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->limit(12)->offset(12 * ($data['page'] - 1))
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        $data = [];

        foreach ($providers as $provider) {
            $data[] = [
                'id' => $provider->id,
                'name' => $provider->name,
                'logo' => $provider->logo,
                'desc' => $provider->description
            ];
        }

        return ['providers' => $data, 'count' => $count_providers];
    }

    /**
     * @param $id
     *
     * @return \app\models\Providers|array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionGetProvider($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $provider = Providers::find()
            ->where(['id' => (int) $id])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->one();

        if ($provider === null)
            throw new BadRequestHttpException();

        if ($provider['functions'] !== null) {
            $provider_func = ProviderFunction::find()
                ->where(['in', 'id', json_decode($provider['functions'], true)])
                ->orderBy(['created_at' => SORT_DESC])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->all();
        }

        $provider_cred = ProvidersCredentials::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->andWhere(['provider_id' => $id])
            ->one();

        if ($provider_cred !== null)
            $provider['credentials'] = json_decode($provider_cred->params, true);

        $provider['functions'] = $provider_func ?? [];

        return $provider;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveProvider($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $auth = \Yii::$app->request->post('auth', []);
        $config = \Yii::$app->request->post('config', []);

        $provider = Providers::findOne((int) $id);

        if ($provider === null)
            throw new BadRequestHttpException();

        $provider_credentials = ProvidersCredentials::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->andWhere(['provider_id' => $provider->id])
            ->one();

        $credentials = $provider_credentials ?? new ProvidersCredentials();
        $credentials->provider_id = $provider->id;
        $credentials->tenant_id = \Yii::$app->user->identity->tenant->id;

        $credentials_array = [];

        foreach ($auth as $data) {
            if (\in_array($data['name'], ProvidersCredentials::PARAMS))
                $credentials_array[$data['name']] = $data['value'];
        }

        foreach ($config as $data) {
            if (\in_array($data['name'], ProvidersCredentials::PARAMS))
                $credentials_array[$data['name']] = $data['value'];
        }

        $credentials->params = json_encode($credentials_array);
        $credentials->save();

        if (empty(array_filter($credentials_array)))
            $credentials->delete();

        return true;
    }
}