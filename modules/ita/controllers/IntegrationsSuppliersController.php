<?php

namespace app\modules\ita\controllers;

use app\helpers\MCHelper;
use app\models\ProviderFunction;
use app\models\Suppliers;
use app\models\SuppliersCredentials;
use app\models\SupplierType;
use app\models\Translations;
use app\modules\ita\controllers\base\BaseController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class IntegrationsSuppliersController extends BaseController
{
    public function actionIndex()
    {
        $supplier_types = SupplierType::find()
            ->orderBy(['value' => SORT_ASC])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->all();

        return $this->render('index', [
            'supplier_types' => $supplier_types
        ]);
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetData()
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = \Yii::$app->request->post('suppliers');

        $suppliers = Suppliers::find()
            ->with('credentials');

        $supplier_cred_ids = SuppliersCredentials::find()
            ->select('supplier_id')
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->column();

        if ($data['integrations'] === 'all') {
            $suppliers->where(['not in', 'id', $supplier_cred_ids]);

            if (!empty($data['type'])) {
                $suppliers->andWhere(['supplier_type_id' => $data['type']]);
            }

            if (!empty($data['search_text'])) {
                $translate = Translations::find()->select(['split_part(key, :div, :index) qid']);
                $translate->orFilterWhere(['like', 'value',  $data['search_text']])
                    ->orFilterWhere(['ilike', 'value',  $data['search_text']])
                    ->andWhere('"key" ILIKE :table', [':table'=> "suppliers.name.%"]);
                $translate->addParams([':div' => '.', ':index' => 3]);
                $translate = $translate->column();

                $suppliers->andWhere(['in', 'id', $translate]);
            }
        } else {
            $suppliers->where(['in', 'id', $supplier_cred_ids]);
        }

        $count_suppliers = $suppliers->count();

        $suppliers = $suppliers->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->limit(12)->offset(12 * ($data['page'] - 1))
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()->all();

        $countries_ids = array_unique(array_column($suppliers, 'country_id'));
        $countries = MCHelper::getCountryById($countries_ids, \Yii::$app->user->identity->tenant->language->iso);
        $countries = array_combine(array_column($countries, 'id'), $countries);

        $data = [];

        foreach ($suppliers as $supplier) {
            $credentials = [];

            foreach ($supplier['credentials'] as $credential)
                $credentials[] = json_decode($credential['params'], true);

            $data[] = [
                'id' => $supplier['id'],
                'name' => $supplier['name'],
                'logo' => $supplier['logo'],
                'desc' => $supplier['description'],
                'country' => [
                    'id' => $supplier['country_id'],
                    'iso' => mb_strtolower($countries[$supplier['country_id']]['iso_code']),
                    'title' => $countries[$supplier['country_id']]['title']
                ],
                'company' => $supplier['company'],
                'code' => $supplier['code'],
                'web_site' => $supplier['web_site'],
                'credentials' => $credentials,
                'auth_type' => $supplier['authorization_type']
            ];
        }

        return ['suppliers' => $data, 'count' => $count_suppliers];
    }

    /**
     * @param $id
     *
     * @return \app\models\Providers|array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionGetSupplier($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $supplier = Suppliers::find()
            ->where(['id' => (int) $id])
            ->translate(\Yii::$app->user->identity->tenant->language->iso)
            ->asArray()->one();

        if ($supplier === null)
            throw new BadRequestHttpException();

        if ($supplier['functions'] !== null) {
            $supplier_func = ProviderFunction::find()
                ->where(['in', 'id', json_decode($supplier['functions'], true)])
                ->orderBy(['created_at' => SORT_DESC])
                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                ->all();
        }

        $supplier_cred = SuppliersCredentials::find()
            ->where(['tenant_id' => \Yii::$app->user->identity->tenant->id])
            ->andWhere(['supplier_id' => $id])
            ->all();

        $supplier['credentials'] = [];

        if ($supplier_cred !== null) {
            foreach ($supplier_cred as $cred)
                $supplier['credentials'][] = json_decode($cred->params, true);
        }

        $supplier['functions'] = $supplier_func ?? [];

        $country = MCHelper::getCountryById($supplier['country_id'], \Yii::$app->user->identity->tenant->language->iso);

        $supplier['country'] = [
            'id' => $supplier['country_id'],
            'iso' => mb_strtolower($country[0]['iso_code']),
            'title' => $country[0]['title']
        ];

        return $supplier;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveSupplier($id)
    {
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data_credentials = \Yii::$app->request->post('Credentials', []);

        $supplier = Suppliers::findOne((int) $id);

        if ($supplier === null)
            throw new BadRequestHttpException();

        SuppliersCredentials::deleteAll([
            'tenant_id' => \Yii::$app->user->identity->tenant->id,
            'supplier_id' => $supplier->id
        ]);

        foreach ($data_credentials as $data) {
            $credentials_array = [];

            foreach ($data as $key => $value) {
                if (\in_array($key, SuppliersCredentials::PARAMS)) {
                    $credentials_array[$key] = $value;
                }
            }

            if (!empty(array_filter($credentials_array))) {
                $credentials = new SuppliersCredentials();
                $credentials->supplier_id = $supplier->id;
                $credentials->tenant_id = \Yii::$app->user->identity->tenant->id;

                $credentials->params = json_encode($credentials_array);
                $credentials->save();
            }
        }

        return true;
    }
}
