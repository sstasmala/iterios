<?php

namespace app\modules\ita;
use Yii;

/**
 * ita module definition class
 */
class module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\ita\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if(isset(Yii::$app->params['stage']))
        {
            if (Yii::$app->params['stage'] == 'prot')
                $this->controllerNamespace = 'app\modules\ita\controllers\prot';
        }
    }
}
