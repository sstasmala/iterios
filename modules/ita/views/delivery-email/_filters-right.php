<?php
use app\helpers\TranslationHelper;
use app\helpers\MCHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row filters-right-search">
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Данные рассылки
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" id="filter_name" class="form-control m-input" placeholder="">
                        </div>
                        <div class="form-group">
                            Провайдер
                        </div>
                        <div class="form-group">
                            Статус
                        </div>
                        <div class="form-group">
                            Даты отправки
                        </div>
                        <div class="form-group">
                            Ответственный
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        
    </div>
</div>