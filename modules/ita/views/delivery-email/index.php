<?php

use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('dess_email_page_title', $lang, 'Email deliveries');
$this->registerJsFile('@web/js/delivery-email/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/delivery-email/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php $this->beginBlock('filters-left') ?>
<?= $this->render('_filters-left'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-right') ?>
<?= $this->render('_filters-right'); ?>
<?php $this->endBlock(); ?>

<?php
$this->beginBlock('extra_modals');
$this->endBlock();
?>

<div class="m-portlet m-portlet--mobile" id="delivery_email_datatable_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('dess_email_page_title', $lang, 'Email deliverings') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="head_tools_left">
                <div id="bulk_dess_email_buttons" class="hidden">
                    <button id="remove-mess-email" class="btn btn-sm btn-outline-info" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete'); ?>">
                        <i class="la la-trash"></i>
                        <?= TranslationHelper::getTranslation('delete', $lang, 'Delete'); ?>
                    </button>
                </div>
            </div>
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/delivery-email/create') ?>" class="btn btn-success m-btn m-btn--icon">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('dess_email_add_new_btn', $lang, 'Add delivering'); ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable" id="dess_email_data_table"></div>
    </div>
</div>