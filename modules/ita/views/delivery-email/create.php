<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('dess_email_create_title', $lang, 'Create email delivering');
$this->registerCSSFile('@web/css/delivery-email/create.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/delivery-email/create.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/delivery-email/wizard.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/delivery-email/_m_modal_email_body_templates');
echo $this->render('/partials/modals/delivery-email/_m_modal_email_body_email_designer');
echo $this->render('/partials/modals/delivery-email/_m_modal_email_body_raw_html');
$this->endBlock();
?>

<div class="row">
    <div class="col-xl-8 col-xxl-9">
        <div class="m-portlet" id="create_delivery_email_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <?= TranslationHelper::getTranslation('dess_email_create_title', $lang, 'Create email delivering') ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-wizard m-wizard--5 m-wizard--success" id="delivery_email_wizard">
                <div class="m-portlet__padding-x">
                    <!-- Here you can put a message or alert -->
                </div>
                <div class="m-wizard__head m-portlet__padding-x">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="delivery_email_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    1.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_email_wizard_step_1_title', $lang, 'Main settings') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_email_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    2.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_email_wizard_step_2_title', $lang, 'Email body') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_email_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    3.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_email_wizard_step_3_title', $lang, 'Segments') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_email_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    4.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_email_wizard_step_4_title', $lang, 'Finishing') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-wizard__form">
                    <form class="m-form m-form--label-align-left- m-form--state-" id="delivery_email_form">
                        <div class="m-portlet__body">
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="delivery_email_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_delivery_name_label', $lang, 'Delivery name') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="hidden" class="ignore" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                                    <input type="text" name="EmailsDispatch[name]" id="deliv_name_input" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('dess_email_delivery_name_placeholder', $lang, 'Enter delivery name') ?>">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_delivery_theme_label', $lang, 'Delivery theme') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="EmailsDispatch[subject]" id="deliv_theme_input" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('dess_email_delivery_theme_placeholder', $lang, 'Enter delivery theme') ?>">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_email_delivery_provider_label', $lang, 'Provider') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <select name="EmailsDispatch[provider_id]" required class="form-control m-bootstrap-select m_selectpicker send-sms-provider" title="<?= TranslationHelper::getTranslation('dess_email_delivery_provider_placeholder', $lang, 'Select provider') ?>">
                                                        <?php
                                                        // Demo providers array
                                                        foreach ($providers as $provider){ ?>
                                                            <option value="<?= $provider['id'] ?>"><?= $provider['provider']['name']?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_email_wizard_form_step_2">
                                <div class="container email-body-change-block">
                                    <input type="hidden" name="EmailsDispatch[email]" required>
                                    <input type="hidden" name="EmailsDispatch[email_type]" required>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 col-xl-12 col-xxl-4 mb-4 mb-md-0 mb-xl-4 mb-xxl-0">
                                            <a class="link-wrap" href="#" data-toggle="modal" data-target="#m_modal_email_body_templates">
                                                <div class="m-portlet m-portlet--mobile mb-0">
                                                    <div class="m-portlet__head">
                                                        <div class="m-portlet__head-caption">
                                                            <div class="m-portlet__head-title">
                                                                <h3 class="m-portlet__head-text text-center">
                                                                    <?= TranslationHelper::getTranslation('dess_email_body_select_templates_label', $lang, 'Templates') ?>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-portlet__body">
                                                        <div class="image">
                                                            <i class="fa fa-th-large"></i>
                                                        </div>
                                                        <div class="decor-line mt-3 mb-3"></div>
                                                        <div class="text text-center">
                                                            <?= TranslationHelper::getTranslation('dess_email_body_select_templates_description', $lang, 'Choose from templates saved on your account or predesigned templates') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-xl-12 col-xxl-4 mb-4 mb-md-0 mb-xl-4 mb-xxl-0">
                                            <a class="link-wrap" href="#" data-toggle="modal" data-target="#m_modal_email_body_email_designer">
                                                <div class="m-portlet m-portlet--mobile mb-0">
                                                    <div class="m-portlet__head">
                                                        <div class="m-portlet__head-caption">
                                                            <div class="m-portlet__head-title">
                                                                <h3 class="m-portlet__head-text text-center">
                                                                    <?= TranslationHelper::getTranslation('dess_email_body_select_email_designer_label', $lang, 'Email designer') ?>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-portlet__body">
                                                        <div class="image">
                                                            <i class="fa fa-paint-brush"></i>
                                                        </div>
                                                        <div class="decor-line mt-3 mb-3"></div>
                                                        <div class="text text-center">
                                                            <?= TranslationHelper::getTranslation('dess_email_body_select_email_designer_description', $lang, 'Use our user friendly Email Designer to create a new template') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-xl-12 col-xxl-4 mb-4 mb-md-0 mb-xl-4 mb-xxl-0">
                                            <a class="link-wrap" href="#" data-toggle="modal" data-target="#m_modal_email_body_raw_html">
                                                <div class="m-portlet m-portlet--mobile mb-0">
                                                    <div class="m-portlet__head">
                                                        <div class="m-portlet__head-caption">
                                                            <div class="m-portlet__head-title">
                                                                <h3 class="m-portlet__head-text text-center">
                                                                    <?= TranslationHelper::getTranslation('dess_email_body_select_raw_html_label', $lang, 'Raw html') ?>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-portlet__body">
                                                        <div class="image">
                                                            <i class="fa fa-code"></i>
                                                        </div>
                                                        <div class="decor-line mt-3 mb-3"></div>
                                                        <div class="text text-center">
                                                            <?= TranslationHelper::getTranslation('dess_email_body_select_raw_html_description', $lang, 'Create a template directly from HTML code') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_email_wizard_form_step_3">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    3. <?= TranslationHelper::getTranslation('dess_email_wizard_step_3_title', $lang, 'Segments') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_email_segments_select_label', $lang, 'Segments select') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <?php if(isset($segments)) { ?>
                                                        <div class="segment_checkboxes row">
                                                            <?php foreach ($segments as $segment) { ?>
                                                                <div class="segment_checkbox_item col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 col-xxl-4">
                                                                    <label class="m-checkbox">
                                                                        <input type="checkbox" name="EmailsDispatch[segments][]" class="segment_checkbox" value="<?= $segment->id ?>" count-phones="<?= $segment->count_phones; ?>">
                                                                        <div class="d-inline text-nowrap">
                                                                            <?= $segment->segment->name . ' (' . $segment->count_emails . ')' ?>
                                                                        </div>
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if(isset($segments)) { ?>
                                                <div class="form-group m-form__group row">
                                                    <div id="segment-contacts-search-block" class="col-12 d-none mb-4">
                                                        <div class="input-group">
                                                            <input type="text" id="segments-contacts-search-input" class="form-control" placeholder="<?= TranslationHelper::getTranslation('dess_email_search_contacts_placeholder', $lang, 'Search contact...') ?>">
                                                            <div class="input-group-append">
                                                                <button id="segments-contacts-search-submit" class="btn btn-primary" type="button">
                                                                    <?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="segments-preview-datatable-wrap">
                                                            <div class="m_datatable" id="segments_preview_datatable"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_email_wizard_form_step_4">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_email_preview_label', $lang, 'Email preview') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="result-deliver-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_email_recievers_count_label', $lang, 'Receivers count') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="result-deliver-phones">1408</span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_email_sending_date_label', $lang, 'Sending date') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="text-nowrap mr-2">
                                                        <i class="la la-calendar mr-1"></i>
                                                        <span class="result-deliver-date">23.07.2018</span>
                                                    </span>
                                                    <span class="text-nowrap">
                                                        <i class="la la-clock-o mr-1"></i>
                                                        <span class="result-deliver-time">12:30</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                            <span>
                                                <i class="la la-arrow-left"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 m--align-right">
                                        <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                            <span>
                                                <i class="la la-check"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    <?= TranslationHelper::getTranslation('complete', $lang, 'Complete') ?>
                                                </span>
                                            </span>
                                        </a>
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                            <span>
                                                <span>
                                                    <?= TranslationHelper::getTranslation('save_and_continue', $lang, 'Save & Continue') ?>
                                                </span>
                                                &nbsp;&nbsp;
                                                <i class="la la-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-xxl-3">
        <div class="m-portlet" id="deliveries_email_stats_portlet">

            <div class="m-portlet__body provider-placeholeder">
                <div class="m-section">
                    <?= TranslationHelper::getTranslation('dess_email_delivery_provider_placeholder', $lang) ?>
                </div>
            </div>
            <div class="m-portlet__body provider-data" style="display: none">
                <div class="m-section">
                    <h2 class="m-section__heading" id="provider_name">
                    </h2>
                    <div class="m-section__content provider-description">
                        <p>
                            <span id="provider_description"></span>
                        </p>
                        <p>
                        <table class="provider-stats-table">
                            <tbody>
                            <tr>
                                <td><?= TranslationHelper::getTranslation('dess_website_label', $lang, 'Website') ?>:</td>
                                <td>
                                    <a id="provider_website" href="" target="_blank" rel="nofollow">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?= TranslationHelper::getTranslation('dess_email_subs_count', $lang, 'Subscribers count') ?>:</td>
                                <td id="sub_count"></td>
                            </tr>
                            <tr>
                                <td><?= TranslationHelper::getTranslation('dess_email_current_balance_label', $lang, 'Current balance') ?>:</td>
                                <td>460 UAH</td>
                            </tr>
                            </tbody>
                        </table>
                        </p>
                    </div>
                </div>
                <div class="m-separator m-separator--fit"></div>
                <div class="delivery-sms_statistic_wrap mb-4">
                    <div class="m-subheader">
                        <div class="d-flex align-items-center">
                            <div>
                            <span class="m-subheader__daterange" id="delivery_email_statistic_date_filter">
                                <span class="m-subheader__daterange-label">
                                    <span class="m-subheader__daterange-title"></span>
                                    <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" onclick="event.preventDefault()" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-widget1 m-widget1--paddingless">
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">
                                    <?= TranslationHelper::getTranslation('dev_email_stats_contacts_quantity', $lang, 'Contacts quantity') ?>
                                </h3>
                            </div>
                            <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand" id="contacts_count">

                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">
                                    <?= TranslationHelper::getTranslation('dev_email_stats_deliveries_quantity', $lang, 'Deliveries quantity') ?>
                                </h3>
                                <span class="m-widget1__desc"></span>
                            </div>
                            <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand" id="dispatch_count">

                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
