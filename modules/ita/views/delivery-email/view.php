<?php

use app\helpers\TranslationHelper;
use app\assets\JQVMapAsset;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
if($delivery['statistic_data'] != null)
{
    $info = \json_decode($delivery['statistic_data'],true);
}
else
{
    $info = [
        'total' => 0,
        'opened_total' => 0,
        'clicked_total' => 0,
        'unique_opens' => 0,
        'unique_clicks' => 0,
        'last_open' => 0,
        'last_click' => 0,
        'not_delivered' => 0,
        'unsubscribed' => 0,
        'open_rate' => 0,
        'click_rate' => 0,
        'history' => [],
        'links' => [],
        'emails' => [],
        'locations' => []
    ];
};

$hs = $info['history'];

$clicks = \array_column($hs,'clicks');
$opens = \array_column($hs,'opens');
$times = \array_map(function($item){
    $time = \app\helpers\DateTimeHelper::convertTimestampToDateTime($item['time'],'H').':00';
    return $time;
},$hs);
$locations = $info['locations'];
$locations = \array_map(function($item){
    $item['country'] = strtolower($item['country']);
    $item['value'] = $item['value'].'';
    return $item;
},$locations);

$locations_data = [];
foreach ($locations as $l) {
    if(!isset($locations_data[$l['country']]))
        $locations_data[$l['country']] = $l;
    else
        $locations_data[$l['country']]['value'] = ($locations_data[$l['country']]['value']+$l['value']).'';
}


$locations = \array_combine(\array_column($locations_data,'country'),\array_column($locations_data,'value'));
\uasort($locations_data,function ($a,$b){
    if ($a['value'] == $b['value']) {
        return 0;
    }
    return ($a['value'] < $b['value']) ? 1 : -1;
});

$info['links'] = \array_filter($info['links'],function ($item){
    return $item['clicks'] > 0;
});
$info['emails'] = \array_filter($info['emails'],function ($item){
    return $item['opens'] > 0;
});

\uasort($info['emails'],function($a,$b){
    if ($a['opens'] == $b['opens']) {
        return 0;
    }
    return ($a['opens'] < $b['opens']) ? 1 : -1;
});



$delivery_name = (isset($delivery['name']) && !empty($delivery['name'])) ? (trim($delivery['name'])) : '';
$this->title = TranslationHelper::getTranslation('dess_email_view_title', $lang, 'Email delivery') . ' - ' . $delivery_name;
JQVMapAsset::register($this);
$this->registerJs('var clicks = '.\json_encode($clicks).';', \yii\web\View::POS_HEAD);
$this->registerJs('var opens = '.\json_encode($opens).';', \yii\web\View::POS_HEAD);
$this->registerJs('var times = '.\json_encode($times).';', \yii\web\View::POS_HEAD);
$this->registerJs('var locations = '.\json_encode($locations).';', \yii\web\View::POS_HEAD);
$this->registerJs('var dispatch_id = '.$delivery['id'].';', \yii\web\View::POS_HEAD);
$this->registerJsFile('@web/js/delivery-email/view.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/delivery-email/view.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJs('var send_email_id = '.$delivery['id'].';', \yii\web\View::POS_HEAD);


?>

<?php
$this->beginBlock('extra_modals');
$this->endBlock();
?>

<div class="m-portlet m-portlet--mobile" id="email_delivery_stats_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $delivery['name']?>
                </h3>
            </div>
        </div>
    </div>
    <?php
    ?>
    <div class="m-portlet__body">
        <div class="recipients_count_block mb-4 row">
            <div class="col-12">
                <span class="text-nowrap">
                    <span class="count-block font-italic m--font-bolder m--font-brand mr-1"><?=$info['total']?></span>
                    <span class="word-block font-italic m--font-bold"><?= TranslationHelper::getTranslation('dess_email_view_recipients_count_label', $lang, 'Recipients') ?></span>
                </span>
            </div>
        </div>
        <div class="email_additional_info_block mb-4 row">
            <div class="col-sm-6">
                <span class="text-nowrap">
                    <span class="m--font-bolder mr-1"><?= TranslationHelper::getTranslation('dess_email_view_subject_label', $lang, 'Subject') ?>:</span>
                    <span class=" font-italic m--font-bold"><?=$delivery['subject']?></span>
                </span>
            </div>
            <div class="col-sm-6">
                <a class="m-link m--font-bold" href="#"><?= TranslationHelper::getTranslation('dess_email_view_email_link_label', $lang, 'View email') ?></a>
            </div>
        </div>
        <div class="progress-stats-block mb-4 row">
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="d-flex justify-content-between m--font-bolder mb-1">
                    <span><?= TranslationHelper::getTranslation('dess_email_view_open_rate_label', $lang, 'Open rate') ?></span>
                    <span class="m--font-info"><?= $info['open_rate']?>%</span>
                </div>
                <div class="progress">
                    <div class="progress-bar m--bg-info" role="progressbar" style="width: <?= $info['open_rate']?>%;" aria-valuenow="<?= $info['open_rate']?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="d-flex justify-content-between m--font-bolder mb-1">
                    <span><?= TranslationHelper::getTranslation('dess_email_view_click_rate_label', $lang, 'Click rate') ?></span>
                    <span class="m--font-brand"><?= $info['click_rate']?>%</span>
                </div>
                <div class="progress">
                    <div class="progress-bar m--bg-brand" role="progressbar" style="width: <?= $info['click_rate']?>%;" aria-valuenow="<?= $info['click_rate']?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
        <div style="height: 10px;"></div>
        <div class="row mb-4">
            <div class="col-12">
                <div class="container">
                    <div class="stats-squares row">
                        <div class="stat-item text-center col-12 col-sm-3">
                            <div class="content">
                                <div class="number m--font-info m--font-bolder">
                                    <span><?= $info['unique_opens']?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_email_view_opened_label', $lang, 'Opened') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="stat-item text-center col-12 col-sm-3">
                            <div class="content">
                                <div class="number m--font-brand m--font-bolder">
                                    <span><?= $info['unique_clicks']?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_email_view_clicked_label', $lang, 'Clicked') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="stat-item text-center col-12 col-sm-3">
                            <div class="content">
                                <div class="number m--font-danger m--font-bolder">
                                    <span><?= $info['not_delivered']?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_email_view_bounced_label', $lang, 'Bounced') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="stat-item text-center col-12 col-sm-3">
                            <div class="content">
                                <div class="number m--font-bolder">
                                    <span><?=$info['unsubscribed']?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_email_view_unsubscribed_label', $lang, 'Unsubscribed') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 20px;"></div>
        <div id="email-delivery-additional-rows" class="row mb-4">
            <div class="col-sm-6">
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_successful_deliveries_label', $lang, 'Successful deliveries') ?></span>
                    <span class="underline"></span>
                    <span class="value">
                        <span class="m--font-bolder mr-1"><?=$info['total']-$info['not_delivered']?></span>
                        <span><?=($info['total']!=0)?100 - $info['not_delivered']/($info['total']/100):0?>%</span>
                    </span>
                </div>
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_total_opens_label', $lang, 'Total opens') ?></span>
                    <span class="underline"></span>
                    <span class="value m--font-bolder m--font-info"><?=$info['opened_total']?></span>
                </div>
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_last_opened_label', $lang, 'Last opened') ?></span>
                    <span class="underline"></span>
                    <span class="value">
                        <span class="m--font-bold"><?= !empty($info['last_open'])?\app\helpers\DateTimeHelper::convertTimestampToDateTime($info['last_open']):
                                TranslationHelper::getTranslation('not_set')?></span>
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_clicks_per_unique_opens_label', $lang, 'Clicks per unique opens') ?></span>
                    <span class="underline"></span>
                    <span class="value m--font-bolder m--font-info"><?=($info['unique_opens']!=0)?$info['unique_clicks']/($info['unique_opens']/100):0?>%</span>
                </div>
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_total_clicks_label', $lang, 'Total clicks') ?></span>
                    <span class="underline"></span>
                    <span class="value m--font-bolder m--font-info"><?=$info['clicked_total']?></span>
                </div>
                <div class="additional-row mb-2">
                    <span class="label"><?= TranslationHelper::getTranslation('dess_email_view_last_clicked_label', $lang, 'Last clicked') ?></span>
                    <span class="underline"></span>
                    <span class="value">
                        <span class="m--font-bold"><?= !empty($info['last_click'])?\app\helpers\DateTimeHelper::convertTimestampToDateTime($info['last_click']):
                                TranslationHelper::getTranslation('not_set')?></span>
                    </span>
                </div>
            </div>
        </div>
        <div style="height: 20px;"></div>
        <div id="chartline-block" class="row mb-4">
            <div class="col-12">
                <h3 class="chartline-title">
                    <?= TranslationHelper::getTranslation('dess_email_view_chartjs_title', $lang, '24-hour performance') ?>
                </h3>
            </div>
            <div class="col-12">
                <canvas id="clicks_chartjs" width="1200" height="300"></canvas>
            </div>
        </div>
        <div style="height: 20px;"></div>
        <div id="clicks_and_opens_block" class="row mb-4">
            <div class="col-sm-12 col-md-6">
                <div id="top-links" class="row mb-4 mb-md-0">
                    <div class="col-12 mb-1">
                        <h3 class="top-links-title">
                            <?= TranslationHelper::getTranslation('dess_email_view_top_links_title', $lang, 'Top links clicked') ?>
                        </h3>
                    </div>
                    <div class="col-12">
                        <?php foreach ($info['links'] as $link):?>
                        <div class="link-item row mb-1">
                            <div class="col-10 link-text-block">
                                <a href="<?=$link['url']?>" class="m-link m--font-bolder" rel="nofollow" target="_blank">
                                    <?=$link['url']?>
                                </a>
                            </div>
                            <div class="col-2 text-right">
                                <span class="m--font-bold">
                                    <?=$link['clicks']?>
                                </span>
                            </div>
                            <div class="underline"></div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div id="top-subscribers" class="row mb-4 mb-md-0">
                    <div class="col-12 mb-1">
                        <h3 class="top-subscribers-title">
                            <?= TranslationHelper::getTranslation('dess_email_view_top_subscribers_title', $lang, 'Subscribers with top opens') ?>
                        </h3>
                    </div>
                    <div class="col-12">
                        <?php foreach ($info['emails'] as $email):?>
                        <div class="subscribers-item row mb-1">
                            <div class="col-8">
                                <a href="#" class="m-link m--font-bolder" rel="nofollow" target="_blank">
                                    <?=$email['email']?>
                                </a>
                            </div>
                            <div class="col-4 text-right">
                                <span class="m--font-bold">
                                    <?=$email['opens']?>
                                </span>
                            </div>
                            <div class="underline"></div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 20px;"></div>
        <div id="locations-map" class="row mb-4">
            <div class="col-12 mb-3">
                <h3 class="locations-map-title">
                    <?= TranslationHelper::getTranslation('dess_email_view_locations_map_title', $lang, 'Top locations by opens') ?>
                </h3>
            </div>
            <div class="col-xl-6 mb-4 mb-xl-0">
                <?php foreach ($locations_data as $location):?>
                    <?php $country = $countries[\strtoupper($location['country'])]??null?>
                <div class="location-item row mb-3">
                    <div class="col-8">
                        <div class="iti-flag <?=$location['country']?> mr-2"></div>
                        <span class="country"><?=$country['title']??''?></span>
                    </div>
                    <div class="col-4 text-right">
                        <span class="m--font-bolder"><?=$location['value']?></span>
                    </div>
                    <div class="underline"></div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="col-xl-6">
                <div id="m_jqvmap_world" class="m-jqvmap" style="height:300px;"></div>
            </div>
        </div>
        <div id="receivers_datatable_block" class="row">
            <div class="col-12 mb-2">
                <h3 class="receivers-table-title">
                    <?= TranslationHelper::getTranslation('dess_email_view_recievers_table_title', $lang, 'Recievers') ?>
                </h3>
            </div>
            <div class="col-12">
                <div id="email-delivery-report-datatable"></div>
            </div>
        </div>
    </div>
</div>
