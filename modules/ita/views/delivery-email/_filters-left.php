<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold active">
        Все рассылки
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold">
        Только мои рассылки
    </a>
</div>
