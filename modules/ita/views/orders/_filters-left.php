<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<!--<div class="filter-item">-->
<!--    <a href="#" id="open" class="m-link m--font-bold active">-->
<!--        TranslationHelper::getTranslation('oi_filter_preset_open_orders_label', $lang) -->
<!--    </a>-->
<!--</div>-->
<div class="filter-item">
    <a href="#" id="only_my_orders" class="m-link m--font-bold">
        <?= TranslationHelper::getTranslation('oi_filter_preset_only_my_orders_label', $lang) ?>
    </a>
</div>
<!--<div class="filter-item">-->
<!--    <a href="#" id="closed" class="m-link m--font-bold">-->
<!--        TranslationHelper::getTranslation('oi_filter_preset_archive_orders_label', $lang) -->
<!--    </a>-->
<!--</div>-->