<?php
use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('oi_page_title', $lang, 'Orders');
?>

<?php
$this->registerCssFile('@web/css/orders/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/orders/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/orders/filters.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/orders/_m_modal_create_order', ['contacts_types' => $contacts_types]);
$this->endBlock();
?>

<?php $this->beginBlock('filters-left') ?>
<?= $this->render('_filters-left'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-right') ?>
<?= $this->render('_filters-right'); ?>
<?php $this->endBlock(); ?>

<div class="m-portlet m-portlet--mobile" id="orders_datatable_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('oi_page_title', $lang, 'Orders') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="head_tools_left">
                <div id="bulk_orders_buttons" class="hidden">
                    <button id="remove-orders" class="btn btn-sm btn-outline-info" title="<?= TranslationHelper::getTranslation('oi_delete_order', $lang, 'Delete order'); ?>">
                        <i class="la la-trash"></i>
                        <?= TranslationHelper::getTranslation('delete', $lang, 'Delete'); ?>
                    </button>
                </div>
            </div>
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_create_order">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('oi_add_request_button', $lang, 'Add order'); ?>
                            </span>
                        </span>
                    </button>
                </li>
                <li class="m-portlet__nav-item">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push ml-3" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle">
                            <i class="la la-plus m--hide"></i>
                            <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__item">
                                                <a href="" class="m-nav__link" data-toggle="modal" data-target="#">
                                                    <i class="m-nav__link-icon la la-gear"></i>
                                                    <span class="m-nav__link-text">
                                                        <?= TranslationHelper::getTranslation('contacts_menu_list_settings', $lang); ?>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/import') ?>" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-sign-in"></i>
                                                    <span class="m-nav__link-text">
                                                        <?= TranslationHelper::getTranslation('contacts_menu_list_import', $lang); ?>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable" id="orders_data_table"></div>
    </div>
</div>