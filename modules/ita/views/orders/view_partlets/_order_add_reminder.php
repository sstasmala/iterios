<?php
    use app\helpers\TranslationHelper;
use app\models\RequestNotes;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="order-add-reminder-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('order_modal_add_reminder_title', $lang) ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="add_reminder_form" action="" method="POST">
                <div class="modal-body reminder-date">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="add_reminder_input__title">
                                <?= TranslationHelper::getTranslation('order_modal_add_reminder_input_name', $lang);?>
                            </label>
                            <input type="text" name="Reminder[name]" id="create_reminder_input__title" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('order_modal_add_reminder_input_name', $lang); ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="Reminder[due_date]" class="reminder_due_date_timestamp">
                        <div class="col-7">
                            <label for="add_reminder_input__title">
                                <?= TranslationHelper::getTranslation('contacts_timeline_task_due_date_placeholder', $lang);?>
                            </label>
                            <div class="input-group date">
                                <input type="text" name="due_date" class="reminder_input__due_date form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_due_date_placeholder', $lang) ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o glyphicon-th"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-5" style="padding-right: 0;">
                            <label for="add_reminder_input__title">
                                <?= TranslationHelper::getTranslation('reminder_due_time', $lang);?>
                            </label>
                            <div class="input-group time">
                                <input type="text" name="due_time" class="reminder_input__due_time form-control m-input" placeholder="<?= TranslationHelper::getTranslation('reminder_due_time', $lang) ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-clock-o glyphicon-th"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                    </button>
                    <button type="submit" form="add_reminder_form" class="btn btn-primary" id="add_reminder_btn" data-oid="<?= $order['id'] ?>">
                        <?= TranslationHelper::getTranslation('save_changes_button', $lang, 'Save') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>