<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet request-services mb-0" id="order-product-partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <!-- Indicators: start -->
                <div class="indicators-block">
                    <?php switch ($order_status):case 0:?>
                        <span class="indicator danger"></span>
                        <span class="indicator "></span>
                        <span class="indicator "></span>
                        <?php break;?>
                    <?php case 1:?>
                        <span class="indicator warning"></span>
                        <span class="indicator warning"></span>
                        <span class="indicator "></span>
                        <?php break;?>
                    <?php case 2:?>
                        <span class="indicator success"></span>
                        <span class="indicator success"></span>
                        <span class="indicator success"></span>
                        <?php break;?>
                    <?php endswitch;?>
                    <!--<span class="indicator brand"></span>        Example -->
                    <!--<span class="indicator info"></span>         Example -->
                    <!--<span class="indicator success"></span>      Example -->
                </div>
                <!-- Indicators: end -->
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_page_products_partlet_title', $lang, 'Products') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="btn btn-outline-brand m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_select_service">
                        <span>
                            <i class="la la-plus"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('rv_page_add_service_btn', $lang, 'Add service') ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="order-product-list-block">
            <?php foreach ($services as $k => $service):?>
            <!-- ITEMS START -->
            <!-- Begin: Demo item 1 -->
            <div class="product-item">
                <div class="row-1 mb-3">
                    <div class="title">
                        <a href="#" class="m-link m--font-bolder mr-3">
                            <?=$service['name']?></a>
                        <div class="order-product-status-dropdown m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                            <?php switch ($service['status']):case 0:?>
                            <span href="#" class="m-dropdown__toggle m--font-danger product-status">
                                <span class="text"><?= TranslationHelper::getTranslation('ov_page_products_danger_status_label', $lang, 'Not confirmed')?></span>
                                <i class="la la-angle-down open_indicator"></i>
                            </span>
                            <?php break;?>
                            <?php case 1:?>
                            <span href="#" class="m-dropdown__toggle m--font-warning product-status">
                                <span class="text"><?= TranslationHelper::getTranslation('ov_page_products_warning_status_label', $lang, 'Waiting')?></span>
                                <i class="la la-angle-down open_indicator"></i>
                            </span>
                            <?php break;?>
                            <?php case 2:?>
                            <span href="#" class="m-dropdown__toggle m--font-success product-status">
                                <span class="text"><?= TranslationHelper::getTranslation('ov_page_products_success_status_label', $lang, 'Confirmed')?></span>
                                <i class="la la-angle-down open_indicator"></i>
                            </span>
                            <?php break;?>
                            <?php case 3:?>
                            <span href="#" class="m-dropdown__toggle m--font-danger product-status">
                                <span class="text"><?= TranslationHelper::getTranslation('ov_page_products_canceled_status_label', $lang, 'Canceled')?></span>
                                <i class="la la-angle-down open_indicator"></i>
                            </span>
                            <?php break;?>
                            <?php endswitch;?>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <ul class="m-nav">
                                                <li class="m-nav__item">
                                                    <a href="#" class="m-nav__link set-service-status" data-status="2" data-link-id="<?=$k?>">
                                                        <span class="m-nav__link-text m--font-success">
                                                            <?= TranslationHelper::getTranslation('ov_page_products_success_status_label', $lang, 'Confirmed') ?>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="#" class="m-nav__link set-service-status" data-status="1" data-link-id="<?=$k?>">
                                                        <span class="m-nav__link-text m--font-warning">
                                                            <?= TranslationHelper::getTranslation('ov_page_products_warning_status_label', $lang, 'Waiting') ?>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="#" class="m-nav__link set-service-status" data-status="0" data-link-id="<?=$k?>">
                                                        <span class="m-nav__link-text m--font-danger">
                                                            <?= TranslationHelper::getTranslation('ov_page_products_danger_status_label', $lang, 'Not confirmed') ?>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="#" class="m-nav__link set-service-status" data-status="3" data-link-id="<?=$k?>">
                                                        <span class="m-nav__link-text m--font-danger">
                                                            <?= TranslationHelper::getTranslation('ov_page_products_canceled_status_label', $lang, 'Not confirmed') ?>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty(\app\utilities\services\ServicesUtility::getServiceReservation($service))):?>
                        <mark class="reservation-number ml-2"><?=\app\utilities\services\ServicesUtility::getServiceReservation($service)?></mark>
                        <?php endif;?>
                    </div>
                    <div class="actions m--font-bold">
                        <div class="buttons">
                            <?php
                            $reservation_url = \app\utilities\services\ServicesUtility::getFieldFromService($service, app\models\ServicesFieldsDefault::TYPE_RESERVATION_URL);
                            if(isset($reservation_url) && !empty($reservation_url)){ ?>
                                <i class="la la-external-link mr-1" title="<?= TranslationHelper::getTranslation('to_reserve', $lang, 'Reserve') ?>" data-toggle="modal" data-target="#m_modal_create_order_reservation" data-reservation-url="<?= $reservation_url ?>"></i>
                            <?php } ?>
                            <i class="la la-pencil mr-1 edit-service" data-id="<?=$k?>" data-toggle="modal" data-target="#m_modal_service_details"></i>
                            <i class="la la-close mr-1 remove-service" data-id="<?=$k?>"></i>
                        </div>
                        <?php
                        $price = \app\utilities\services\ServicesUtility::getServicePrice($service);

                        $currency = \app\utilities\services\ServicesUtility::getServiceCurrency($service);
                        ?>
                        <div class="price ml-2">
                            <span class="value"><?=$price?></span>&nbsp;<span class="currency">
                                <?=($currency!="")?$currency['text']:""?></span>
                        </div>
                    </div>
                </div>
                <div class="row-2 mb-3">
                    <?php
                    $header = \app\utilities\services\ServicesUtility::getServiceHeader($service,$currency);
                    ?>
                    <span class="m--font-bold">
                        <?=$header?>
                    </span>
                </div>
                <div class="row-3 mb-3">
                    <div class="supplier mr-3">
                        <?php
                        $supplier = '';
                        $suppliers =  array_filter($service['fields'],function($var){
                            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
                        });
                        $suppliers = array_merge($suppliers, array_filter($service['fields_def'],function($var){
                            return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
                        }));
                        if(!empty($suppliers) && isset(reset($suppliers)['value'])) {
                            if(isset($values['suppliers'][reset($suppliers)['value']['id']]))
                                $supplier = $values['suppliers'][reset($suppliers)['value']['id']];
                        }
                        ?>
                        <?php if($supplier != ""):?>
                        <div class="logo mr-2">
                            <img src="<?=Yii::$app->params['baseUrl'].'/'.$supplier['logo']?>"
                                 alt="<?=$supplier['name']?>" title="<?=$supplier['name']?>">
                        </div>
                        <div class="title m--font-bold">
                            <?=$supplier['name']?>
                        </div>
                        <?php endif;?>
                    </div>
                    <?php
                    $additional =  $service['links'];
                    ?>
                    <?php if(!empty($additional)):?>
                        <div class="icons">
                            <?php foreach ($additional as $a):?>
                                <?php if($a['id']==$service['id']) continue;?>
                                <i class="<?=$a['icon']?> mr-1" data-toggle="m-popover" data-placement="top" data-content="<?=$a['name']?>"></i>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <!-- End: Demo item 1 -->
            <?php endforeach;?>
            <!-- ITEMS END -->
        <?php if(empty($services)):?>
            <div class="row">
                <div class="col-12 text-center">
                    <span><?= TranslationHelper::getTranslation('no_data', $lang, 'No data') ?></span>
                </div>
            </div>
        <?php endif;?>
        </div>
    </div>
</div>
