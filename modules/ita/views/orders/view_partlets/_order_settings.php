<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile order-settings mb-0">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-cogwheel-2"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_page_settings_partlet_title', $lang, 'Settings') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="select-responsible-block">
            <p>
                <?= TranslationHelper::getTranslation('ov_page_settings_partlet_responsible_label', $lang, 'Responsible') ?>
            </p>
            <input type="hidden" id="select2-responsible-ajax-search-order-id-hint" value="<?= $order['id'] ?>">
            <select class="form-control m-bootstrap-select" id="select2-responsible-ajax-search" name="order_responsible">
                <?php if(!empty($order['responsible'])){
                    $full_name = '';
                    $full_name .= ($order['responsible']['first_name']) ? $order['responsible']['first_name'] . ' ' : '';
                    $full_name .= ($order['responsible']['middle_name']) ? $order['responsible']['middle_name'] . ' ' : '';
                    $full_name .= ($order['responsible']['last_name']) ? $order['responsible']['last_name'] . ' ' : '';
                    $full_name = trim($full_name);
                    echo '<option value="' . $order['responsible']['id'] . '">' . $full_name . '</option>';
                } else {
                    echo '<option value="" selected>' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</option>';
                } ?>
            </select>
        </div>
        <hr>
        <div class="order-refresh-dates-block">
            <table>
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('ov_page__field_created', $lang, 'Created') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $order['created_at']?></span>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('ov_page__field_updated', $lang, 'Updated') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $order['updated_at']?></span>
                        </td
                    </tr>
                    <tr>
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('order_settings_request_source_name', $lang) ?>
                            </span>
                        </td>
                        <td>
                            <div id="order_request_source" class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">
                                    <?php if (!empty($order['request_source_id'])):?>
                                        <span>
                                            <?php if ($order['request_source']['system_type'] === app\models\RequestType::SYSTEM_FROM_REQUEST_TYPE): ?>
                                                <?= TranslationHelper::getTranslation('order_request', $lang) ?> #<?= (!empty($order['request_id']) ? $order['request_id'] : '') ?>
                                            <?php else: ?>
                                                <?= $order['request_source']['value'] ?>
                                            <?php endif; ?>
                                        </span>
                                    <?php else: ?>
                                        <span class="no-data"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                                    <?php endif; ?>
                                </a>
                                <span class="order-link-cell">
                                    <i class="fa fa-unlock-alt" id="unlink_request_button" <?= (!empty($order['request_source']) && $order['request_source']['system_type'] === app\models\RequestType::SYSTEM_FROM_REQUEST_TYPE ? '' : ' style="display: none;"') ?>></i>
                                </span>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="request_source">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('order_settings_request_source_name', $lang) ?>:</h6>
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="id" type="hidden" value="<?= $order['id'] ?>">
                                                            <input name="Orders[request_id]" type="hidden" value="">
                                                            <select class="form-control m-select2" id="orders_request_sources" name="Orders[request_source_id]">
                                                                <?php if (!empty($order['request_source_id'])): ?>
                                                                    <option value="<?= $order['request_source_id'] ?>" data-system_type="<?= $order['request_source']['system_type'] ?>"><?= $order['request_source']['value'] ?></option>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="order-delete-block">
            <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/orders/delete?id=' . $order['id'] ?>" id="order-view-delete-form" method="POST">
                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                <button type="submit" class="btn btn-danger m-btn m-btn--custom m-btn--icon">
                    <span>
                        <i class="fa fa-trash"></i>
                        <span><?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?></span>
                    </span>
                </button>
            </form>
        </div>
    </div>
</div>