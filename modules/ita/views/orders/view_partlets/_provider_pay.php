<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<?php
// Demo providers array
$providers = [
];

foreach ($services as $k => $service)
{
    if($service['status']!=\app\models\OrdersServicesLinks::STATUS_ACCEPTED)
        continue;
    $supplier = '';
    $suppliers =  array_filter($service['fields'],function($var){
        return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
    });
    $suppliers = array_merge($suppliers, array_filter($service['fields_def'],function($var){
        return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
    }));
    if(!empty($suppliers) && isset(reset($suppliers)['value'])) {
        if(isset($values['suppliers'][reset($suppliers)['value']['id']]))
            $supplier = $values['suppliers'][reset($suppliers)['value']['id']];
    }
    if(!empty($supplier))
        $providers[] = ['name'=>$supplier['name'],'link_id'=>$k,'cur'=>\app\utilities\services\ServicesUtility::getFieldFromService($service,\app\models\ServicesFieldsDefault::TYPE_CURRENCY)];
}
?>

<?php foreach ($providers as $key => $provider) { ?>
    <div class="m-portlet provider-payment payment-statistic-partlet<?= (($key+1) === count($providers)) ? ' mb-0' : ' mb-4' ?>" data-order-id="<?=$id?>" data-link-id="<?=$provider['link_id']?>">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <!-- Indicators: start -->
                    <div class="indicators-block provider-payment-indicator">
                        <span class="indicator danger"></span>
                        <span class="indicator"></span>
                        <span class="indicator"></span>
                        <!--<span class="indicator brand"></span>        Example -->
                        <!--<span class="indicator info"></span>         Example -->
                        <!--<span class="indicator success"></span>      Example -->
                    </div>
                    <!-- Indicators: end -->
                    <h3 class="m-portlet__head-text">
                        <?= TranslationHelper::getTranslation('ov_provider_pay_partlet_title', $lang, 'Payment') ?>
                        <span>(<?= $provider['name'] ?>)</span>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <button data-type="provider" data-toggle="modal" data-target="#m_modal_add_payment" class="btn btn-outline-brand m-btn m-btn--icon">
                        <span>
                            <i class="fa fa-briefcase"></i>
                            <span><?= TranslationHelper::getTranslation('ov_make_a_payment_btn_title', $lang, 'Make a payment') ?></span>
                        </span>
                </button>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-loader m-loader--brand" style="width: 30px;margin: 0 auto;"></div>
            <table class="table m-table m-table--head-no-border mb-0 payment-statistic-table" style="display: none">
                <tbody>
                    <tr>
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('ov_provider_pay_total_sum_label', $lang, 'Total sum') ?>:
                            </span>
                        </td>
                        <td class="text-right m--font-bold">
                            <span class="provider-total-sum"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('ov_provider_pay_payed_label', $lang, 'Payed') ?>:
                            </span>
                        </td>
                        <td class="text-right m--font-bold">
                            <span class="provider-payed"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('ov_provider_pay_provider_duty_label', $lang, 'Our duty') ?>:
                            </span>
                        </td>
                        <td class="text-right m--font-bold">
                            <span class="provider-duty"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('ov_provider_pay_status_label', $lang, 'Status') ?>:
                            </span>
                        </td>
                        <td class="text-right m--font-bold provider-status">
                            <span class="status-bage m-badge m-badge--danger m-badge--wide ">
                                Не оплачено
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="m--font-bold">
                            <a href="#" class="m-link" data-opener="true" data-target=".provider-extra-field-<?= $key ?>" data-status="open">
                                <i class="la la-caret-down" data-show_decorator="for-open"></i>
                                <i class="la la-caret-right" data-show_decorator="for-close"></i>
                                <span><?= TranslationHelper::getTranslation('ov_tourist_pay_payment_details_label', $lang, 'Payment details') ?></span>
                            </a>
                        </td>
                    </tr>
                    <tr class="provider-extra-field-<?= $key ?>">
                        <td>
                            <span>
                                <?= TranslationHelper::getTranslation('ov_provider_pay_сommission_label', $lang, 'Commission') ?>:
                            </span>
                        </td>
                        <td class="text-right m--font-bold">
                            <span class="provider-commission"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>






