<?php
    use app\helpers\TranslationHelper;
use app\models\RequestNotes;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="order-add-note-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('modal_add_note_title', $lang, 'Add note') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form data-id="0" class="m-form m-form--fit m-form--label-align-right"
                  id="add_note_form"
                  method="post"
                  action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/orders/add-notes?id=' . $order['id'] ?>">
                <div class="modal-body">
<!--                    <div id="add-note-preload">-->
<!--                        <div class="m-loader m-loader--lg"></div>-->
<!--                    </div>-->
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <textarea name="value" id="add-note-redactor"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                    </button>
                    <button type="submit" form="add_note_form" class="btn btn-primary">
                        <?= TranslationHelper::getTranslation('save_changes_button', $lang, 'Save') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="order-edit-note-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('req_edit_note_title', $lang, 'Edit note') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form data-id="0" data-text="0" class="m-form m-form--fit m-form--label-align-right"
                  id="edit_note_form"
                  method="post">
                <div class="modal-body">
<!--                    <div id="edit-note-preload">-->
<!--                        <div class="m-loader m-loader--lg"></div>-->
<!--                    </div>-->
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <textarea name="value" id="edit-note-redactor"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                    </button>
                    <button id="edit-note-save-button" type="submit" form="edit_note_form" class="btn btn-primary">
                        <?= TranslationHelper::getTranslation('save_changes_button', $lang, 'Save') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>