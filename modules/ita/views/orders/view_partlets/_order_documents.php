<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet order-documents-portlet mb-0">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-tool-1"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_page_docs_partlet_title', $lang, 'Documents') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#document_add_modal" title="<?= TranslationHelper::getTranslation('ov_page_add_doc_btn', $lang, 'Add document') ?>">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="order-docs-block">
            <span id="our_doc_block"<?= (empty($documents) ? ' style="display: none;"' : '') ?>>
                <h6 class="sub-title mb-3">
                    <?= TranslationHelper::getTranslation('ov_page_docs_subtitle_1', $lang, 'Generated documents') ?>:
                </h6>
                <hr>
                <ul class="our-docs mb-4">
                    <?php foreach ($documents as $document): ?>
                        <!-- Begin: doc item -->
                        <li>
                            <div class="name">
                                <a href="#" class="m-link m--font-bold our-doc-link" data-toggle="modal" data-target="#document_view_modal" data-doc_id="<?= $document['template_id'] ?>" data-order_id="<?= $document['order_id'] ?>" data-order_doc_id="<?= $document['id'] ?>"><?= $document['title'] ?></a>
                                <div class="date">
                                    <span class="moment-js invisible" data-format="unix"><?= $document['updated_at'] ?></span>
                                </div>
                            </div>
                            <div class="actions">
                                <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill our-doc-delete-link" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>" data-order_doc_id="<?= $document['id'] ?>">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </li>
                        <!-- End: doc item -->
                    <?php endforeach; ?>
                </ul>
            </span>
            <span id="other_doc_block"<?= (empty($upload_documents) ? ' style="display: none;"' : '') ?>>
                <h6 class="sub-title mb-3">
                    <?= TranslationHelper::getTranslation('ov_page_docs_subtitle_2', $lang, 'Simple documents') ?>:
                </h6>
                <hr>
                <ul class="other-docs mb-0">
                    <?php foreach ($upload_documents as $document): ?>
                        <!-- Begin: doc item -->
                        <li>
                            <div class="name">
                                <a href="#" class="m-link m--font-bold other-doc-link" data-toggle="modal" data-target="#document_upload_view_modal" data-order_id="<?= $document['order_id'] ?>" data-order_doc_id="<?= $document['id'] ?>"><?= $document['title'] ?></a>
                                <div class="date">
                                    <span class="moment-js invisible" data-format="unix"><?= $document['updated_at'] ?></span>
                                </div>
                            </div>
                            <div class="actions">
                                <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill other-doc-delete-link" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>" data-order_doc_id="<?= $document['id'] ?>">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </li>
                        <!-- End: doc item -->
                    <?php endforeach; ?>
                </ul>
            </span>
            <?php if (empty($documents) && empty($upload_documents)): ?>
                <div class="text-center"><?= \app\helpers\TranslationHelper::getTranslation('order_document_not_found', $lang); ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>