<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet payment-statistic-partlet mb-0" id="tourist-pay-partlet" data-order-id="<?=$id?>">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <!-- Indicators: start -->
                <div class="indicators-block" id="tourist-payment-indicator">
                    <span class="indicator danger"></span>
                    <span class="indicator"></span>
                    <span class="indicator"></span>
                    <!--<span class="indicator brand"></span>        Example -->
                    <!--<span class="indicator info"></span>         Example -->
                    <!--<span class="indicator success"></span>      Example -->
                </div>
                <!-- Indicators: end -->
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_tourist_pay_partlet_title', $lang, 'Pay from tourist') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <button data-toggle="modal" data-target="#m_modal_add_payment" class="btn btn-outline-brand m-btn m-btn--icon">
                        <span>
                            <i class="fa fa-briefcase"></i>
                            <span><?= TranslationHelper::getTranslation('ov_make_a_payment_btn_title', $lang, 'Make a payment') ?></span>
                        </span>
            </button>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-loader m-loader--brand" style="width: 30px;margin: 0 auto;"></div>
        <table class="table m-table m-table--head-no-border mb-0 payment-statistic-table" style="display: none">
            <tbody>
                <tr>
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_total_sum_label', $lang, 'Total sum') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" colspan="2">
                        <span id="order_total_sum"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_total_sum_no_discount_label', $lang, 'Total summ without discount') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" colspan="2">
                        <span id="order_total_sum_wd"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                    </td>
                </tr>
                <tr class="b-duties">
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_payed_label', $lang, 'Payed') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" colspan="2">
                        <span id="order_payed"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                    </td>
                </tr>
                <tr class="tourist-duty">
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_tourist_duty_label', $lang, 'Tourist duty') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" colspan="2">
                        <span id="order_duty"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_status_label', $lang, 'Status') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" id="order_status" colspan="2">
                        <span class="m-badge m-badge--success m-badge--wide">
                            Оплачено
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="m--font-bold">
                        <a href="#" class="m-link" data-opener="true" data-target=".tourist_pay_extra_field" data-status="open">
                            <i class="la la-caret-down" data-show_decorator="for-open"></i>
                            <i class="la la-caret-right" data-show_decorator="for-close"></i>
                            <span><?= TranslationHelper::getTranslation('ov_tourist_pay_payment_details_label', $lang, 'Payment details') ?></span>
                        </a>
                    </td>
                </tr>
                <tr class="tourist_pay_extra_field">
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_profit_label', $lang, 'Profit') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold m--font-success" colspan="2">
                        <span id="order_profit"><?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                    </td>
                </tr>
                <tr class="tourist_pay_extra_field">
                    <td>
                        <span>
                            <?= TranslationHelper::getTranslation('ov_tourist_pay_tourist_discount_label', $lang, 'Tourist discount') ?>:
                        </span>
                    </td>
                    <td class="text-right m--font-bold" colspan="2">
                        <div class="edit-tourist-payments etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" m-dropdown-persistent="1">
                            <a href="#" class="m-dropdown__toggle" style="text-decoration:none;border-bottom: 1px dashed #958fe3;">
                                <span id="order_discount"></span>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <form class="etitable-form">
                                                <div class="row align-items-center">
                                                    <div class="col-12 labels">
                                                        <h6><?= TranslationHelper::getTranslation('ov_tourist_pay_tourist_discount_label', $lang, 'Tourist discount') ?></h6>
                                                    </div>
                                                    <div class="col-12 mb-1"></div>
                                                    <div class="col-8 inputs">
                                                        <!-- TOKEN START -->
                                                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                        <!-- INPUTS START -->
                                                        <input name="ajax_url" type="hidden" value="<?='/orders/set-discount?id='.$id?>">
                                                        <div id="dicount_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-4 buttons text-right">
                                                        <button type="submit" class="btn btn-sm btn-primary editable-submit-exch"><i class="fa fa-check"></i></button>
                                                        <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="preloader" style="display:none;text-align:center;">
                                                <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="payments-history tourist_pay_extra_field">
                    <td colspan="3" class="m--font-bold">
                        <?= TranslationHelper::getTranslation('ov_tourist_pay_payments_history_label', $lang, 'Payments history') ?>:
                    </td>
                </tr>
                <tr class="payments-history-item tourist_pay_extra_field">
                    <td width="100">
                        2000,00
                    </td>
                    <td width="100" class="text-center">
                        26.6
                    </td>
                    <td width="100" class="text-right">
                        26.11.2018
                    </td>
                </tr>
                <tr class="payments-history-item tourist_pay_extra_field">
                    <td width="100">
                        370,45
                    </td>
                    <td width="100" class="text-center">
                        17,7
                    </td>
                    <td width="100" class="text-right">
                        20.08.2018
                    </td>
                </tr>
                <tr class="payments-history-item tourist_pay_extra_field">
                    <td width="100">
                        1400,00
                    </td>
                    <td width="100" class="text-center">
                        31.37
                    </td>
                    <td width="100" class="text-right">
                        04.02.2018
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

