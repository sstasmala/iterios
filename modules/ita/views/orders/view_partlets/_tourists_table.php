<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
$not_set = '<span class="no-data">(' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . ')</span>';

function age_text($date_birth, $lang) {
    $age = DateTime::createFromFormat('U', $date_birth)
        ->diff(new DateTime('now'))
        ->y;

    $year1 = TranslationHelper::getTranslation('order_tourists_passport_age1', $lang);
    $year2 = TranslationHelper::getTranslation('order_tourists_passport_age2', $lang);
    $year3 = TranslationHelper::getTranslation('order_tourists_passport_age3', $lang);

    $m = substr($age,-1,1);
    $l = substr($age,-2,2);

    return $age . ' ' . ((($m === 1) && ($l !== 11)) ? $year1 : (((($m === 2) && ($l !== 12)) || (($m === 3) && ($l !== 13)) || (($m === 4) && ($l !== 14))) ? $year2 : $year3));
}

$indicator_warning = false;

foreach ($tourists as $tourist) {
    if ($indicator_warning)
        break;

    if (time() > $tourist['passport']['date_limit'])
        $indicator_warning = true;

    if ($tourist['contact_id'] === null)
        $indicator_warning = true;

    if ($tourist['passport_id'] === null)
        $indicator_warning = true;
}

?>
<div class="m-portlet tourist-table-partlet mb-0">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <!-- Indicators: start -->
                <div class="indicators-block">
                    <?php if (empty($tourists) || (array_sum(array_column($tourists, 'contact_id')) === 0 && array_sum(array_column($tourists, 'passport_id')) === 0)): ?>
                        <span class="indicator danger"></span>
                        <span class="indicator"></span>
                        <span class="indicator"></span>
                    <?php elseif ($indicator_warning): ?>
                        <span class="indicator warning"></span>
                        <span class="indicator warning"></span>
                        <span class="indicator"></span>
                    <?php else: ?>
                        <span class="indicator success"></span>
                        <span class="indicator success"></span>
                        <span class="indicator success"></span>
                    <?php endif; ?>
                    <!--<span class="indicator brand"></span>        Example -->
                    <!--<span class="indicator info"></span>         Example -->
                    <!--<span class="indicator success"></span>      Example -->
                </div>
                <!-- Indicators: end -->
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_page_tourists_portlet_title', $lang, 'Tourists') ?>
                </h3>
            </div>
        </div>
<!--        <div class="m-portlet__head-tools">-->
<!--            <ul class="m-portlet__nav">-->
<!--                <li class="m-portlet__nav-item">-->
<!--                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_add_tourist" title=" TranslationHelper::getTranslation('ov_page_tourists_add_button', $lang, 'Add tourists') ">-->
<!--                        <i class="la la-plus"></i>-->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
    </div>
    <div class="m-portlet__body">
        <div class="m-scrollable" data-scrollbar-shown="false"  data-axis="x" data-scrollable="true">
            <table class="table m-table m-table--head-bg-success tourists-table-v2">
                <thead>
                    <tr>
                        <th>
                            <?= TranslationHelper::getTranslation('ov_page_tourists_table_service_type_title', $lang, 'Service type') ?>
                        </th>
                        <th>
                            <?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>
                        </th>
                        <th>
                            <?= TranslationHelper::getTranslation('ov_page_tourists_table_name_title', $lang, 'Name') ?>
                        </th>
                        <th>
                            <?= TranslationHelper::getTranslation('ov_page_tourists_table_passport_birthday_icon', $lang, 'Birth date') ?>
                        </th>
                        <th>
                            <?= TranslationHelper::getTranslation('ov_page_tourists_table_passport_title', $lang, 'Passport') ?>
                        </th>
                        <th>
                            <!-- Delete icon cell -->
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tourists as $tourist): ?>
                        <?php
                            if (empty($tourist['contact_id'])) {
                                echo '<tr><td><div><span>'.$services[$tourist['link']['service_id']]['name'].'</span></div></td>'.
                                    '<td><span id="add_tourist_link" href="#" class="m-link" data-toggle="modal" data-target="#m_modal_add_tourist" data-ot_id="'.$tourist['id'].'" title="'.TranslationHelper::getTranslation('ov_page_tourists_add_button', $lang).'" style="cursor: pointer;">'.
                                    '+&nbsp;'.TranslationHelper::getTranslation('order_tourists_table_add_contact', $lang).
                                    '</span></td></tr>';

                                continue;
                            }

                            $reservation = \app\utilities\services\ServicesUtility::getFieldFromService($services[$tourist['link']['service_id']], \app\models\ServicesFieldsDefault::TYPE_RESERVATION);
                        ?>

                        <tr>
                            <td scope="row">
                                <div>
                                    <span><?= $services[$tourist['link']['service_id']]['name'] ?></span>
                                </div>
                                <?php if (!empty($reservation)): ?>
                                    <div>
                                        <mark>Reservation: <?= $reservation  ?></mark>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td scope="row">
                                <div class="uppercase-name">
                                    <span>
                                        <?= !empty($tourist['passport']) ? $tourist['passport']['last_name'] : $not_set ?>
                                    </span>
                                </div>
                                <a href="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/view?id='.$tourist['contact_id'] ?>" class="m-link" target="_blank">
                                    <?= $tourist['contact']['last_name'] ?>
                                </a>
                            </td>
                            <td>
                                <div class="uppercase-name">
                                    <span>
                                        <?= !empty($tourist['passport']) ? $tourist['passport']['first_name'] : $not_set ?>
                                    </span>
                                </div>
                                <a href="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/view?id='.$tourist['contact_id'] ?>" class="m-link" target="_blank">
                                    <?= $tourist['contact']['first_name'] ?>
                                </a>
                            </td>
                            <td>
                                <div class="birth-date">
                                    <span><?= !empty($tourist['passport']) ? \app\helpers\DateTimeHelper::convertTimestampToDate($tourist['passport']['birth_date']) : (!empty($tourist['contact']['date_of_birth']) ? \app\helpers\DateTimeHelper::convertTimestampToDate($tourist['contact']['date_of_birth']) : $not_set) ?></span>
                                </div>
                                <div class="calc-age">
                                    <span><?= $tourist['contact']['gender'] !== null ? '<i class="fa fa-' . ($tourist['contact']['gender'] === \app\models\Contacts::GENDER_FEMALE ? 'female' : 'male') . '"></i>' : '' ?> <?= !empty($tourist['passport']) ? '(' .age_text($tourist['passport']['birth_date'], $lang). ')' : (!empty($tourist['contact']['date_of_birth']) ? '(' .age_text($tourist['contact']['date_of_birth'], $lang). ')' : $not_set) ?></span>
                                </div>
                            </td>
                            <td>
                                <?php if (!empty($tourist['passport'])): ?>
                                    <div class="passport-serial">
                                        <span>
                                            <?= !empty($tourist['passport']) ? $tourist['passport']['serial'] : $not_set ?>
                                        </span>
                                    </div>
                                    <div class="passport-limit-date">
                                        <span>
                                            <?= TranslationHelper::getTranslation('order_tourists_passport_limit', $lang) ?> <?= !empty($tourist['passport']) ? \app\helpers\DateTimeHelper::convertTimestampToDate($tourist['passport']['date_limit']) : $not_set ?>
                                        </span>
                                    </div>
                                <?php else: ?>
                                    <span href="#" id="add_tourist_passport_link" class="m-link" data-toggle="modal" data-source="tourist-passport" data-type="<?= $tourist['type'] ?>" data-ot_id="<?= $tourist['id'] ?>" data-fname="<?= (!empty($tourist['contact']['first_name']) ?
                                        $tourist['contact']['first_name'] : '') ?>" data-lname="<?= (!empty($tourist['contact']['last_name']) ? $tourist['contact']['last_name'] : '') ?>" data-target="#m_modal_edit_tourists" title="<?= TranslationHelper::getTranslation('ov_page_tourists_add_button', $lang, 'Add tourists') ?>" style="cursor: pointer;">
                                        +&nbsp;<?= TranslationHelper::getTranslation('order_tourists_table_add_passport', $lang) ?>
                                    </span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <button class="edit-tourist m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_edit_tourist_passports" title="<?= TranslationHelper::getTranslation('edit', $lang, 'Edit') ?>" data-tourist_id="<?= $tourist['id'] ?>">
                                    <i class="la la-edit"></i>
                                </button>
                                <button class="remove-tourist m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>" data-tourist_id="<?= $tourist['id'] ?>">
                                    <i class="la la-trash"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php if (empty($tourists)): ?>
                <table class="contacts-table no-contacts text-center" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>
                                <span class="m-widget1__desc">
                                    <?= TranslationHelper::getTranslation('order_not_tourists', $lang) ?>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
