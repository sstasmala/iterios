<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet contact-preview mb-0">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <a href="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/view?id=' . $contact['id'] ?>" class="m-link" target="_blank">
                        <?= $contact['first_name'] ?> <?= ($contact['last_name']) ? ' ' . $contact['last_name'] : '' ?>
                    </a>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#contact-edit-popup">
                        <i class="la la-edit"></i>
                    </a>
                </li>
                <li class="m-portlet__nav-item">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push ml-3" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle">
                            <i class="la la-plus m--hide"></i>
                            <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__item">
                                                <a href="" class="m-nav__link" data-toggle="modal" data-target="#contact-change-popup">
                                                    <i class="m-nav__link-icon la la-gear"></i>
                                                    <span class="m-nav__link-text">
                                                        <?= TranslationHelper::getTranslation('order_change_contact', $lang); ?>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php if(
                (!isset($contact['phones']) || empty($contact['phones'])) && 
                (!isset($contact['emails']) || empty($contact['emails'])) && 
                (!isset($contact['socials']) || empty($contact['socials'])) && 
                (!isset($contact['messengers']) || empty($contact['messengers'])) && 
                (!isset($contact['sites']) || empty($contact['sites']))) { ?>
            
            <table class="contacts-table no-contacts">
                <tbody>
                    <tr>
                        <td>
                            <span class="m-widget1__desc">
                                <?= TranslationHelper::getTranslation('contact_no_contacts_message', $lang, 'No contacts') ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span href="#" class="m-link" data-toggle="modal" data-target="#contact-edit-popup" style="cursor: pointer;">
                                +&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        
        <?php } ?>
        <?php if (!empty($contact['phones'])) { ?>
            <h5><?= TranslationHelper::getTranslation('contact-preview-phones', $lang, 'Phones') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($contact['phones'] as $phone) { ?>
                        <tr>
                            <td class="icon">
                                <i class="fa fa-phone"></i>
                            </td>
                            <td class="link">
                                <a href="tel:<?= $phone['value'] ?>" class="m-link m--font-bold phones" title="<?= $contacts_types['phones'][$phone['type_id']]['name'] ?>"><?=$phone['value']?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($contact['emails'])) { ?>
            <h5><?= TranslationHelper::getTranslation('contact-preview-emails', $lang, 'Emails') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($contact['emails'] as $email) { ?>
                        <tr>
                            <td class="icon">
                                <i class="far fa-envelope"></i>
                            </td>
                            <td class="link">
                                <a href="mailto:<?= $email['value'] ?>" class="m-link m--font-bold"><?= $email['value'] ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($contact['socials'])) { ?>
            <h5><?= TranslationHelper::getTranslation('contact-preview-socials', $lang, 'Socials') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($contact['socials'] as $social) {
                        $url = $social['value'];
                        $type = $contacts_types['socials'][$social['type_id']]['name'];
                        $icon = $contacts_types['socials'][$social['type_id']]['icon'];
                        ?>
                        <tr>
                            <td class="icon">
                                <?php if(isset($icon) && !empty($icon)){ ?>
                                    <?= $icon ?>
                                <?php } ?>
                            </td>
                            <td class="link">
                                <a href="<?= $url ?>" class="m-link m--font-bold"><?= $type ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($contact['messengers'])) { ?>
            <h5><?= TranslationHelper::getTranslation('contact-preview-messengers', $lang, 'Messengers') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($contact['messengers'] as $messenger) {
                        $url = $messenger['value'];
                        $type = $contacts_types['messengers'][$messenger['type_id']]['name'];
                        $icon = $contacts_types['messengers'][$messenger['type_id']]['icon'];
                        ?>
                        <tr>
                            <td class="icon">
                                <?php if(isset($icon) && !empty($icon)){ ?>
                                    <?= $icon ?>
                                <?php } ?>
                            </td>
                            <td class="link">
                                <a href="<?= $url ?>" class="m-link m--font-bold"><?= $url ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($contact['sites'])) { ?>
            <h5><?= TranslationHelper::getTranslation('contact-preview-sites', $lang, 'Sites') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($contact['sites'] as $site) { ?>
                        <tr>
                            <td class="icon">
                                <i class="la la-external-link"></i>
                            </td>
                            <td class="link">
                                <a href="<?= $site['value'] ?>" class="m-link m--font-bold"><?= $site['value'] ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <hr>
        <?php if (!empty($contact['tags'])) { ?>
            <?php foreach ($contact['tags'] as $tag) { ?>
                <a href="<?= $tag['link'] ?>" class="m-badge m-badge--brand m-badge--wide">
                    <?= $tag['name'] ?>
                </a>
            <?php } ?>
        <?php } ?>
    </div>
</div>