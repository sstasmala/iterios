<?php
use app\helpers\TranslationHelper;
use Jenssegers\Date\Date;

$lang = Yii::$app->user->identity->tenant->language->iso;
$format = Yii::$app->user->identity->tenant->date_format;

Date::setLocale($lang);
?>
<div class="m-portlet mb-0" id="order-reminders-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-information"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ov_reminders_portlet_title', $lang, 'Order reminders') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <table id="order-reminders-table">
            <tbody>
                <?php foreach($reminders as $reminder): ?>
                    <tr class="<?= $reminder['status'] === 1 ? ' checked-row' : '' ?>">
                        <td class="text-center checkbox-cell">
                            <label class="m-checkbox">
                                <input type="checkbox" data-rid="<?= $reminder['id'] ?>" <?= $reminder['status'] === 1 ? ' checked' : '' ?>>
                                <span></span>
                            </label>
                        </td>
                        <td class="reminder-text"><?= $reminder['reminder']['name'] ?></td>
                        <td class="reminder-date">
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <div class="new-data-set<?= ($reminder['status'] === 1 ? '' : ' m-dropdown__toggle') ?>"<?= ($reminder['status'] === 1 ? ' style="cursor: not-allowed;"' : '') ?>>
                                    <i class="la la-calendar" title="<?= (!empty($reminder['due_date'])) ? (Date::createFromTimestamp($reminder['due_date'])->format($format)) : '' ?>"></i>
                                </div>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <div class="row align-items-center">
                                                    <div class="col-sm-12 mb-1 labels">
                                                        <h6><?= TranslationHelper::getTranslation('ov_reminders_set_date_time_link', $lang) ?></h6>
                                                    </div>
                                                    <div class="col-12 col-lg-10 inputs">
                                                        <div class="row">
                                                            <input type="hidden" name="reminder_due_date" class="reminder_due_date_timestamp">
                                                            <div class="col-12 mb-3">
                                                                <div class="input-group date">
                                                                    <input type="text" name="due_date" class="reminder_input__due_date form-control m-input" value="<?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDateTime($reminder['due_date']))) ? Yii::$app->formatter->asDate(\app\helpers\DateTimeHelper::convertTimestampToDateTime($reminder['due_date']), 'dd.MM.yyyy') : '' ?>" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_due_date_placeholder', $lang, 'Due date') ?>">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">
                                                                            <i class="la la-calendar-check-o glyphicon-th"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mb-3">
                                                                <div class="input-group time">
                                                                    <input type="text" name="due_time" class="reminder_input__due_time form-control m-input" value="<?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDateTime($reminder['due_date']))) ? Yii::$app->formatter->asDate(\app\helpers\DateTimeHelper::convertTimestampToDateTime($reminder['due_date']), 'HH:ii') : '' ?>" placeholder="<?= TranslationHelper::getTranslation('reminder_due_time', $lang) ?>">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">
                                                                            <i class="la la-clock-o glyphicon-th"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-lg-2 text-center buttons">
                                                        <button type="submit" class="btn btn-sm btn-primary btn-setdate-submit" data-rid="<?= $reminder['id'] ?>">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <i class="la la-close delete-reminder-icon" data-rid="<?= $reminder['id'] ?>" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>"></i>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td class="text-center">
                        <i class="fas fa-plus add-reminder-link"></i>
                    </td>
                    <td colspan="2">
                        <span class="m--font-bolder add-reminder-link" data-toggle="modal" data-target="#order-add-reminder-popup"><?= TranslationHelper::getTranslation('order_add_reminder', $lang) ?></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>