<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
echo $this->render('_order_add_note', ['order' => $order]);
?>
<div class="m-portlet order-notes">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-list-1"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?=  TranslationHelper::getTranslation('edit_task_note_redactor_title', $lang, 'Notes') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#order-add-note-popup">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-widget3 m-scrollable" data-scrollable="true" data-max-height="380">
            <?php
            foreach ($orderNotes as $orderNote) {
                ?>
                <div class="m-widget3__item">
                    <div class="m-widget3__header">
                        <div class="m-widget3__user-img">
                            <img class="m-widget3__img"
                                 src="/<?= ($orderNote->user->photo) ? $orderNote->user->photo : 'img/profile_default.png' ?>"
                                 alt="">
                        </div>
                        <div class="m-widget3__info">
                        <span class="m-widget3__username">
                            <?= $orderNote->user->first_name . ' ' . $orderNote->user->last_name ?>
                        </span>
                            <div class="row float-right">
                                <div class="col-sm-12">
                                    <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill mr-2" data-action="delete" data-id_note="<?=$orderNote['id']?>" data-target="#order-delete-note-popup">
                                        <i class="la la-trash"></i>
                                    </a>
                                    <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"  data-toggle="modal" data-id_note="<?=$orderNote['id']?>" data-action="edit" data-target="#order-edit-note-popup">
                                        <i class="la la-edit"></i>
                                    </a>
                                </div>
                            </div>
                            <br>
                            <span class="m-widget3__time moment-js invisible" data-format="unix">
                            <?= $orderNote['created_at'] ?>
                        </span>
                        </div>
                    </div>
                    <div class="m-widget3__body">
                        <?= \yii\helpers\HtmlPurifier::process($orderNote->value) ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>