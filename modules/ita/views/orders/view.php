<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('ov_order_word', $lang, 'Order');
$this->title .= isset($order['contact']) ? ' - ' . trim($order['contact']['first_name']) . ' ' . trim($order['contact']['last_name']) : '';

/* MD5 hash Plugin */
$this->registerJsFile('@web/plugins/jquery-md5/jquery.md5.js', ['depends' => \app\assets\MainAsset::className()]);

/* Files */
$this->registerCssFile('@web/css/orders/view.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/orders/view.min.js', ['depends' => \app\assets\MainAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/orders/_m_modal_edit_contacts', [
    'order' => $order,
    'contact' => $contact,
    'contacts_types' => $contacts_types
]);
echo $this->render('/partials/modals/orders/_m_modal_change_contacts', [
    'order' => $order,
    'contact' => $contact
]);
echo $this->render('/partials/modals/orders/_m_modal_add_document', [
    'order' => $order,
    'doc_templates' => $doc_templates
]);
echo $this->render('/partials/modals/orders/_m_modal_select_service', [
    'services' => $services
]);
echo $this->render('/partials/modals/orders/_m_modal_service_details', [
    'order' => $order,
    'services' => $services
]);
echo $this->render('/partials/modals/orders/_m_modal_document_view');
echo $this->render('/partials/modals/orders/_m_modal_document_upload_view');
echo $this->render('/partials/modals/orders/_m_modal_edit_tourists', [
    'contact' => $contact,
    'contacts_types' => $contacts_types
]);
echo $this->render('/partials/modals/orders/_m_modal_edit_tourist_passports', [
    'contact' => $contact,
    'contacts_types' => $contacts_types
]);
echo $this->render('/partials/modals/orders/_m_modal_add_tourist', ['order' => $order]);
echo $this->render('/partials/modals/orders/_m_modal_add_payment');
echo $this->render('view_partlets/_order_add_reminder', ['order' => $order]);
echo $this->render('/partials/modals/orders/_m_modal_request_source', ['order' => $order]);
echo $this->render('/partials/modals/orders/_m_modal_create_order_reservation');
$this->endBlock();
?>

<div class="row" id="orders-detail-view">
    <div class="col-12 col-sm-5 col-md-4 col-lg-4 col-xxl-3">
        <div class="row">
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_contact_preview', ['contact' => $contact, 'contacts_types' => $contacts_types]) ?>
            </div>
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_order_reminders', ['order' => $order, 'reminders' => $reminders]) ?>
            </div>
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_order_documents', ['documents' => $documents, 'upload_documents' => $upload_documents]) ?>
            </div>
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_order_settings', ['order' => $order]) ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-7 col-md-8 col-lg-8 col-xxl-9">
        <div class="row">
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_product', ['contact' => $contact,'services'=>$services_linked,'values'=>$values,'order_status'=>$order_status]) ?>
            </div>
            <div class="col-12 col-md-6 mb-4">
                <?= $this->render('view_partlets/_tourist_pay', ['id'=>$order['id'],'order' => $order,'values'=>$values]) ?>
            </div>
            <div class="col-12 col-md-6 mb-4">
                <?= $this->render('view_partlets/_provider_pay', ['services'=>$services_linked,'values'=>$values,'id'=>$order['id'],'currency' => $values['currencies'][$order['currency']]['iso_code']]) ?>
            </div>
            <div class="col-12 mb-4">
                <?= $this->render('view_partlets/_tourists_table', ['order' => $order, 'tourists' => $tourists, 'services' => array_combine(array_column($services_linked, 'id'), $services_linked)]) ?>
            </div>
            <div class="col-xxl-6 col-xl-12 col-lg-6 col-md-6">
                <?= $this->render('view_partlets/_order-notes', ['order' => $order, 'orderNotes' => $orderNotes]) ?>
            </div>
        </div>
    </div>
</div>