<?php
use app\helpers\TranslationHelper;
use app\helpers\MCHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row filters-right-search">
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('oi_filter_contact_data_block_title', $lang) ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="order_contact_filter_input_passport_serial" name="OrderFilter[passport]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_passport_serial_input_placeholder', $lang) ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="order_contact_filter_input_last_name" name="OrderFilter[last_name]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_last_name_input_placeholder', $lang, 'Last name') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="order_contact_filter_input_first_name" name="OrderFilter[first_name]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_first_name_input_placeholder', $lang, 'First name') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="order_contact_filter_input_phone" name="OrderFilter[phone]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_contact_phone_input_placeholder', $lang, 'Contact phone') ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="order_contact_filter_input_email" name="OrderFilter[email]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_contact_email_input_placeholder', $lang, 'Contact email') ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="contacts-filter-tags-search-select" multiple name="tags[]"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-accordion__item" style="overflow:visible;">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_2_head" data-toggle="collapse" href="#m_accordion_1_item_2_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('oi_filter_order_data_block_title', $lang) ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_2_body" role="tabpanel" aria-labelledby="m_accordion_1_item_2_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <select name="OrderFilter[country]" id="oi_filter_select_country" class="form-control m-bootstrap-select country-select-ajax" multiple></select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="oi_filter_request_number" name="OrderFilter[request_number]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_order_data_request_number_title', $lang) ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="oi_filter_reservation_number" name="OrderFilter[reservation_number]" placeholder="<?= TranslationHelper::getTranslation('oi_filter_order_data_reservation_number_title', $lang) ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" name="OrderFilter[departure_date_range]" id="oi_filter_select_departure_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('oi_filter_order_data_departure_dates_title', $lang, 'Enter customer departure date') ?>">
                            <input type="hidden" name="OrderFilter[departure_date_start]" id="oi_filter_select_departure_date_picker_start">
                            <input type="hidden" name="OrderFilter[departure_date_end]" id="oi_filter_select_departure_date_picker_end">
                        </div>
                        <div class="form-group">
                            <?php
                                $main_currency_id = !empty(\Yii::$app->user->identity->tenant->main_currency) ? \Yii::$app->user->identity->tenant->main_currency : null;
                                $main_currency_text = null;

                                if ($main_currency_id){
                                    $mc_data = MCHelper::getCurrencyById($main_currency_id,$lang);
                                    $main_currency_text = !empty($mc_data) ? $mc_data[0]['iso_code'] : '';
                                }
                            ?>
                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-currency_id">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary">
                                        <?= $main_currency_text ?>
                                    </button>
                                </div>
                                <input type="hidden" name="OrderFilter[currency_id]" id="oi_filter_select_currency_id" value="<?= $main_currency_id ?>" class="hidden-currency_id">
                                <input type="text" name="OrderFilter[currency_min]" id="oi_filter_input_currency_min" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_min_budget_input_paceholder', $lang, 'From') ?>">
                                <input type="text" name="OrderFilter[currency_max]" id="oi_filter_input_currency_max" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_max_budget_input_paceholder', $lang, 'To') ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_2" role="tablist">
            <div class="m-accordion__item" style="overflow:visible;">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_2_item_1_head" data-toggle="collapse" href="#m_accordion_2_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('oi_filter_order_settings_block_title', $lang) ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body show" id="m_accordion_2_item_1_body" role="tabpanel" aria-labelledby="m_accordion_2_item_1_head" data-parent="#m_accordion_2" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" name="OrderFilter[create_date_picker]" id="oi_filter_create_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('oi_filter_order_create_dates_picker_placeholder', $lang, 'Create dates') ?>">
                            <input type="hidden" name="OrderFilter[create_date_start]" id="oi_filter_create_date_picker_start">
                            <input type="hidden" name="OrderFilter[create_date_end]" id="oi_filter_create_date_picker_end">
                        </div>
                        <div class="form-group">
                            <input type="text" name="OrderFilter[update_date_picker]" id="oi_filter_update_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('oi_filter_order_update_dates_picker_placeholder', $lang, 'Update dates') ?>">
                            <input type="hidden" name="OrderFilter[update_date_start]" id="oi_filter_update_date_picker_start">
                            <input type="hidden" name="OrderFilter[update_date_end]" id="oi_filter_update_date_picker_end">
                        </div>
                        <div class="form-group">
                            <select name="OrderFilter[responsible]" id="oi_filter_select_responsible" class="form-control m-bootstrap-select"></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>