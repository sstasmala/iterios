<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<?php
$this->title = TranslationHelper::getTranslation('templates', $lang, 'Templates') . ' - ' . TranslationHelper::getTranslation('email_and_sms', $lang, 'Email and SMS');
$this->registerCSSFile('@web/css/templates/email-and-sms/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/templates/email-and-sms/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/templates/email-and-sms/_m_modal_template_edit', ['delivery_types' => $delivery_types]);
$this->endBlock();
?>

<div id="email-and-sms-portlet" class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('teasi_portlet_title', $lang, 'Email and SMS templates') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <div id="templates-filter" class="btn-group m-btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-brand" data-filter="all"><?= TranslationHelper::getTranslation('teasi_filter_title_all', $lang, 'All') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="system"><?= TranslationHelper::getTranslation('teasi_filter_title_system', $lang, 'System') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="my"><?= TranslationHelper::getTranslation('teasi_filter_title_my', $lang, 'My') ?></button>
                    </div>
                </li>
                <li class="m-portlet__nav-item">
                    <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_template_edit">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('teasi_add_new_template_button', $lang, 'Add template') ?>
                            </span>
                        </span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-form m-form--label-align-right m--margin-bottom-30">
            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 col-xxl-3 mb-4 mb-md-0">
                    <div class="m-form__group m-form__group--inline">
                        <div class="m-form__label">
                            <label>
                                <?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>:
                            </label>
                        </div>
                        <div class="m-form__control">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" id="input_search_template" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>...">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 col-xxl-3">
                    <div class="m-form__group m-form__group--inline">
                        <div class="m-form__label">
                            <label class="text-nowrap">
                                <?= TranslationHelper::getTranslation('teasi_filter_type_select_label', $lang, 'Type') ?>:
                            </label>
                        </div>
                        <div class="m-form__control">
                            <select class="form-control m-bootstrap-select m_selectpicker" id="m_form_status" title="<?= TranslationHelper::getTranslation('teasi_filter_type_select_placeholder', $lang, 'Select template type') ?>">
                                <?php foreach ($delivery_types as $delivery_type): ?>
                                    <option value="<?= $delivery_type['id'] ?>"><?= $delivery_type['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="m_datatable" id="templates_emails_and_sms_datatable"></div>
            </div>
        </div>
    </div>
</div>