<?php

$this->title = ($company['name']) ? trim($company['name']) : '';

/* Files */
$this->registerCssFile('@web/css/companies/view.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/companies/view.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/company/_m_modal_edit_contact', ['company' => $company, 'contacts_types' => $contacts_types]);
$this->endBlock();

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row" id="company-detail-view">
    <div class="col-xxl-3 col-xl-4 col-lg-12">
        <div class="row">
            <div class="col-xl-12 col-lg-5 col-md-5 col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_company-preview', [
                            'company' => $company,
                            'contacts_types' => $contacts_types,
                            'lang' => $lang
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_company-info', [
                            'company' => $company,
                            'lang' => $lang
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_company-attached-contacts', [
                            'company' => $company,
                            'company_contacts' => $company_contacts,
                            'lang' => $lang
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-7 col-md-7 col-sm-6">
                <div class="row">
                    <div class="col-12">
                        <?= $this->render('partlets/_company-settings', [
                            'company' => $company,
                            'lang' => $lang
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-9 col-xl-8 col-lg-12">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12">
                <?= $this->render('partlets/_company-history_and_notes', [
                    'company' => $company,
                    'lang' => $lang,
                    'timeline' => $timeline,
                    'all_task_types' => $all_task_types
                ]) ?>
            </div>
        </div>
    </div>
</div>