<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row">
    <div class="col-lg-6 filters-right-search">
        <div class="m-accordion m-accordion--default" id="m_filter_accordion_1" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_filter_accordion_1_item_1_head" data-toggle="collapse" href="#m_filter_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('ci_filter_company_data_block_title', $lang, 'Company data') ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse show" id="m_filter_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_filter_accordion_1_item_1_head" data-parent="#m_filter_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="company_filter_input_name" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_name_input_placeholder', $lang, 'Company name') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="company_filter_input_phone" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_phone_input_placeholder', $lang, 'Company phone') ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="company_filter_input_email" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_email_input_placeholder', $lang, 'Company email') ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="form-group">
                            <select class="form-control m-bootstrap-select" id="company_filter_input_status" name=""></select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-bootstrap-select" id="company_filter_input_type" name=""></select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-bootstrap-select" id="company_filter_input_activities_type" name=""></select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-bootstrap-select" id="filter-select2-responsible-ajax-search" name=""></select>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
        </div>
    </div>
    <div class="col-lg-6 filters-right-search">
        <div class="m-accordion m-accordion--default" id="m_filter_accordion_2" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_filter_accordion_2_item_1_head" data-toggle="collapse" href="#m_filter_accordion_2_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('ci_filter_linked_contacts_block_title', $lang, 'Linked contacts') ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse show" id="m_filter_accordion_2_item_1_body" role="tabpanel" aria-labelledby="m_filter_accordion_2_item_1_head" data-parent="#m_filter_accordion_2" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="company_contact_filter_input_passport" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_passport_serial_input_placeholder', $lang, 'Passport serial number') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="company_contact_filter_input_last_name" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_last_name_input_placeholder', $lang, 'Last name') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" id="company_contact_filter_input_first_name" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_first_name_input_placeholder', $lang, 'First name') ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="company_contact_filter_input_phone" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_contact_phone_input_placeholder', $lang, 'Contact phone') ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input masked-input" id="company_contact_filter_input_email" name="text" placeholder="<?= TranslationHelper::getTranslation('ci_filter_contact_email_input_placeholder', $lang, 'Contact email') ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="contacts-filter-tags-search-select" multiple name="tags[]"></select>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
        </div>
    </div>
</div>