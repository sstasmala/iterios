<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="filter-item">
    <a href="#" id="only_my" class="m-link m--font-bold"><?= TranslationHelper::getTranslation('ci_filter_preset_only_my_companies_label', $lang, 'Only my companies') ?></a>
</div>