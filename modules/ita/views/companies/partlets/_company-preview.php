<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet company-preview">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= ($company['name']) ? ' ' . $company['name'] : '' ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#company-edit-popup">
                        <i class="la la-edit"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php if(
                (!isset($company['phones']) || empty($company['phones'])) && 
                (!isset($company['emails']) || empty($company['emails'])) && 
                (!isset($company['socials']) || empty($company['socials'])) && 
                (!isset($company['messengers']) || empty($company['messengers'])) && 
                (!isset($company['sites']) || empty($company['sites']))) { ?>
            
        <table class="contacts-table no-contacts">
            <tbody>
                <tr>
                    <td>
                        <span class="m-widget1__desc">
                            <?= TranslationHelper::getTranslation('contact_no_contacts_message', $lang, 'No contacts') ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span href="#" class="m-link" data-toggle="modal" data-target="#contact-edit-popup" style="cursor: pointer;">
                            +&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <?php } ?>
        <?php if (!empty($company['phones'])) { ?>
            <h5><?= TranslationHelper::getTranslation('company-preview-phones', $lang, 'Phones') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($company['phones'] as $phone) { ?>
                        <tr>
                            <td class="icon">
                                <i class="fa fa-phone"></i>
                            </td>
                            <td class="link">
                                <a href="tel:<?= $phone['value'] ?>" class="m-link m--font-bold phones" title="<?= $contacts_types['phones'][$phone['type_id']]['name'] ?>"><?=$phone['value']?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($company['emails'])) { ?>
            <h5><?= TranslationHelper::getTranslation('company-preview-emails', $lang, 'Emails') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($company['emails'] as $email) { ?>
                        <tr>
                            <td class="icon">
                                <i class="far fa-envelope"></i>
                            </td>
                            <td class="link">
                                <a href="mailto:<?= $email['value'] ?>" class="m-link m--font-bold"><?= $email['value'] ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($company['socials'])) { ?>
            <h5><?= TranslationHelper::getTranslation('company-preview-socials', $lang, 'Socials') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($company['socials'] as $social) {
                        $url = $social['value'];
                        $type = $contacts_types['socials'][$social['type_id']]['name'];
                        $icon = $contacts_types['socials'][$social['type_id']]['icon'];
                        ?>
                        <tr>
                            <td class="icon">
                                <?php if(isset($icon) && !empty($icon)){ ?>
                                    <?= $icon ?>
                                <?php } ?>
                            </td>
                            <td class="link">
                                <a href="<?= $url ?>" class="m-link m--font-bold"><?= $type ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($company['messengers'])) { ?>
            <h5><?= TranslationHelper::getTranslation('company-preview-messengers', $lang, 'Messengers') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($company['messengers'] as $messenger) {
                        $url = $messenger['value'];
                        $type = $contacts_types['messengers'][$messenger['type_id']]['name'];
                        $icon = $contacts_types['messengers'][$messenger['type_id']]['icon'];
                        ?>
                        <tr>
                            <td class="icon">
                                <?php if(isset($icon) && !empty($icon)){ ?>
                                    <?= $icon ?>
                                <?php } ?>
                            </td>
                            <td class="link">
                                <a href="<?= $url ?>" class="m-link m--font-bold"><?= $url ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <?php if (!empty($company['sites'])) { ?>
            <h5><?= TranslationHelper::getTranslation('company-preview-sites', $lang, 'Sites') ?></h5>
            <table class="contacts-table">
                <tbody>
                    <?php foreach ($company['sites'] as $site) { ?>
                        <tr>
                            <td class="icon">
                                <i class="la la-external-link"></i>
                            </td>
                            <td class="link">
                                <a href="<?= $site['value'] ?>" class="m-link m--font-bold"><?= $site['value'] ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        <hr>
        <?php if (!empty($company['tags'])) { ?>
            <?php foreach ($company['tags'] as $tag) { ?>
                <a href="<?= $tag['link'] ?>" class="m-badge m-badge--brand m-badge--wide">
                    <?= $tag['name'] ?>
                </a>
            <?php } ?>
        <?php } ?>
    </div>
</div>