<?php

use app\helpers\MCHelper;
use yii\helpers\Html;
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile company-info">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-user"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('company_contact_info', $lang, 'Company info') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="company-info-table-block">
            <table class="table table-striped m-table">
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('field_company_type', $lang, 'Company type') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($company['type']['value'])) ? $company['type']['value'] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('field_company_type', $lang, 'Company type') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/companies/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $company['id'] ?>">
                                                            <input name="property" type="hidden" value="type">
                                                            <select id="company-info-table-company_type_select" class="form-control m-bootstrap-select" name="value" data-live-search="true">
                                                                <?php if (!empty($company['type'])): ?>
                                                                    <option value="<?= $company['type']['id'] ?>"><?= $company['type']['value'] ?></option>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('field_company_kind_activity', $lang, 'Activities type') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($company['kind_activity']['value'])) ? $company['kind_activity']['value'] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('field_company_kind_activity', $lang, 'Company activities') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/companies/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $company['id'] ?>">
                                                            <input name="property" type="hidden" value="kind_activity">
                                                            <select id="company-info-table-company_kind_activity_select" class="form-control m-bootstrap-select" name="value" data-live-search="true">
                                                                <?php if (!empty($company['kind_activity'])): ?>
                                                                    <option value="<?= $company['kind_activity']['id'] ?>"><?= $company['kind_activity']['value'] ?></option>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('field_company_status', $lang, 'Company status') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle request_status_text btn btn-sm btn-<?= !empty($company['status']['color']) ? $company['status']['color'] : 'default' ?> m-badge--wide">
                                    <?= !empty($company['company_status_id']) ? $company['status']['value'] :'<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                                    ?>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('field_company_status', $lang, 'Company status') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/companies/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $company['id'] ?>">
                                                            <input name="property" type="hidden" value="status">
                                                            <select id="company-info-table-company_status_select" class="form-control m-bootstrap-select" name="value" data-live-search="true">
                                                                <?php if (!empty($company['status'])): ?>
                                                                    <option value="<?= $company['status']['id'] ?>"><?= $company['status']['value'] ?></option>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('field_company_adress', $lang, 'Company address') ?></td>
                        <td>
                            <?php 
                            $arderss_row = '';
                            if (!empty($company['company_address'])) {
                                if (isset($company['company_address']['country']) && !empty($company['company_address']['country'])) {
                                    $arderss_row .= MCHelper::getCountryById($company['company_address']['country'],$lang)[0]['title']. ', ';
                                }
                                if (isset($company['company_address']['region']) && !empty($company['company_address']['region'])) {
                                    $arderss_row .= $company['company_address']['region'] . ', ';
                                }
                                if (isset($company['company_address']['city']) && !empty($company['company_address']['city'])) {
                                    $arderss_row .= $company['company_address']['city'] . ', ';
                                }
                                if (isset($company['company_address']['street']) && !empty($company['company_address']['street'])) {
                                    $arderss_row .= $company['company_address']['street'];
                                    if (isset($company['company_address']['house']) && !empty($company['company_address']['house'])) {
                                        $arderss_row .= ' ' . $company['company_address']['house'];
                                        if (isset($company['company_address']['flat']) && !empty($company['company_address']['flat'])) {
                                            $arderss_row .= ', ' . $company['company_address']['flat'];
                                        }
                                    }
                                    $arderss_row .= ', ';
                                }
                                if (isset($company['company_address']['postcode']) && !empty($company['company_address']['postcode'])) {
                                    $arderss_row .= $company['company_address']['postcode'] . ', ';
                                }
                                $arderss_row = substr(trim($arderss_row), 0, -1);
                            } else {
                                $arderss_row = '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                            }
                            ?>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= $arderss_row ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labecompany_select_inputls">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-company_address-label', $lang, 'Living address') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/companies/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $company['id'] ?>">
                                                            <input name="property" type="hidden" value="company_address">
                                                            <select class="form-control m-bootstrap-select country-select-ajax" name="company_address[country]">
                                                                <?php if(isset($company['company_address']) && !is_null($company['company_address'])):?>
                                                                <option value="<?= $company['company_address']['country'] ?>" selected="selected"><?= MCHelper::getCountryById($company['company_address']['country'],$lang)[0]['title']?></option>
                                                                <?php endif;?>
                                                            </select>
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[region]" type="text" class="form-control" value="<?= (!empty($company['company_address']['region'])) ? $company['company_address']['region'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-region', $lang, 'Region') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[city]" type="text" class="form-control" value="<?= (!empty($company['company_address']['city'])) ? $company['company_address']['city'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-city', $lang, 'City') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[street]" type="text" class="form-control" value="<?= (!empty($company['company_address']['street'])) ? $company['company_address']['street'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-street', $lang, 'Street') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[house]" type="number" class="form-control m-input" value="<?= (!empty($company['company_address']['house'])) ? $company['company_address']['house'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-house', $lang, 'House') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[flat]" type="number" class="form-control m-input" value="<?= (!empty($company['company_address']['flat'])) ? $company['company_address']['flat'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-flat', $lang, 'Flat') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="company_address[postcode]" type="number" class="form-control m-input" value="<?= (!empty($company['company_address']['postcode'])) ? $company['company_address']['postcode'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('company-editable-table-addressplaceholder-postcode', $lang, 'Postcode') ?>">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
