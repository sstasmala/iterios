<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;
?>
<div class="m-portlet m-portlet--responsive-mobile company-settings">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-cogwheel-2"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('company-settings-partlet-title', $lang, 'Settings') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="select-responsible-block">
            <p>
                <?= TranslationHelper::getTranslation('company-settings-partlet-responsible_label', $lang, 'Responsible') ?>
            </p>
            <input type="hidden" id="select2-responsible-ajax-search-company-id-hint" value="<?= $company['id'] ?>">
            <select class="form-control m-bootstrap-select" id="select2-responsible-ajax-search" name="company_responsible">
                <?php if(!empty($company['responsible'])){
                    $full_name = '';
                    $full_name .= ($company['responsible']['first_name']) ? $company['responsible']['first_name'] . ' ' : '';
                    $full_name .= ($company['responsible']['middle_name']) ? $company['responsible']['middle_name'] . ' ' : '';
                    $full_name .= ($company['responsible']['last_name']) ? $company['responsible']['last_name'] . ' ' : '';
                    $full_name = trim($full_name);
                    echo '<option value="' . $company['responsible']['id'] . '">' . $full_name . '</option>';
                } else {
                    echo '<option value="" selected>' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</option>';
                } ?>
            </select>
        </div>
        <hr>
        <div class="company-refresh-dates-block">
            <table>
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('company_field_created', $lang, 'Created') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $company['created_at']?></span>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('company_field_updated', $lang, 'Updated') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $company['updated_at']?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="company-delete-block">
            <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/companies/delete?id=' . $company['id'] ?>" id="company-view-delete-form" method="POST">
                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                <button type="submit" class="btn btn-danger m-btn m-btn--custom m-btn--icon">
                    <span>
                        <i class="fa fa-trash"></i>
                        <span><?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?></span>
                    </span>
                </button>
            </form>
        </div>
    </div>
</div>