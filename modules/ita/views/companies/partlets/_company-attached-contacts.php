<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile company-attached-contacts-info">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-users"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('company_attached_contacts_title', $lang, 'Company contacts') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="company-info-table-block">
            <?php if (!empty($company_contacts)): ?>
                <table class="table table-striped m-table">
                    <tbody>
                        <?php foreach ($company_contacts as $contact): ?>
                            <tr>
                                <td>
                                    <a href="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id ?>/contacts/view?id=<?= $contact['id'] ?>" class="m-link">
                                        <?= $contact['last_name'] .' '. $contact['first_name'] ?>
                                    </a>
                                </td>
                                <td>
                                    <a class="remove_contact" href="<?= \Yii::$app->params['baseUrl'].'/ita/'.Yii::$app->user->identity->tenant->id.'/companies/remove-contact?id='.$contact['id'].'&company_id='.$company['id'] ?>" title="<?= TranslationHelper::getTranslation('delete', $lang) ?>">
                                        <i class="fa fa-times m--font-danger remove-icon"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <table class="contacts-table no-contacts">
                    <tbody>
                    <tr>
                        <td>
                            <span class="m-widget1__desc">
                                <?= TranslationHelper::getTranslation('company_no_contacts', $lang) ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <span href="#" class="m-dropdown__toggle m-link" data-toggle="modal" data-target="#contact-edit-popup" style="cursor: pointer;">
                                    +&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                                </span>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form action="<?= Yii::$app->params['baseUrl'].'/ita/'.Yii::$app->user->identity->tenant->id.'/companies/set-contact' ?>" method="post">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1">
                                                            <h6><?= TranslationHelper::getTranslation('company_added_contact', $lang) ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-10 mb-3">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="id" type="hidden" value="<?= $company['id'] ?>">
                                                            <select id="company_contact_select" class="form-control m-bootstrap-select" name="contact_id" data-live-search="true"></select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
