<?php
use app\helpers\TranslationHelper;
?>

<?php
function render_timeline_item($item, $is_first=false){
    ob_start();
?>

    <div class="m-timeline-1__item m-timeline-1__item--right<?= ($is_first) ? ' m-timeline-1__item--first' : '' ?>">
        <div class="m-timeline-1__item-circle<?= (isset($item['icon']['color']) && !empty($item['icon']['color'])) ? (' border border-'.$item['icon']['color']) : '' ?>">
            <?php if(isset($item['icon']['class']) && !empty($item['icon']['class'])) { ?>
                <i class="<?= $item['icon']['class'] ?><?= ($item['icon']['color']) ? (' m--font-'.$item['icon']['color']) : ' m--font-danger' ?>"></i>
            <?php } ?>
        </div>
        <div class="m-timeline-1__item-arrow"></div>
        <span class="m-timeline-1__item-time m--font-brand">
            <?= $item['time'] ?>
        </span>
        <div class="m-timeline-1__item-content">
            <div class="m-timeline-1__item-title">
                <?php if(isset($item['avatar']['src']) && !empty($item['avatar']['src'])) { ?>
                    <div class="m-list-pics d-inline-block">
                        <a href="<?= ($item['avatar']['link']) ? $item['avatar']['link'] : '#' ?>">
                            <img src="<?= $item['avatar']['src'] ?>" title="<?= ($item['avatar']['title']) ? $item['avatar']['title'] : '' ?>">
                        </a>
                    </div>
                <?php } ?>
                <?= $item['title'] ?>
                <?php if($item['buttons']['remove'] || $item['buttons']['edit']) { ?>
                    <div class="row float-right">
                        <div class="col-sm-12">
                        <?php if($item['buttons']['remove']) { ?>
                            <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill mr-2">
                                <i class="la la-close"></i>
                            </a>
                        <?php } ?>
                        <?php if($item['buttons']['edit']) { ?>
                             <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-edit"></i>
                            </a>
                        <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="m-timeline-1__item-body">
                <?= $item['content'] ?>
                <?php if(isset($item['aditional_info']) && !empty($item['aditional_info'])) { ?>
                    <div class="row mt-3">
                        <?php foreach ($item['aditional_info'] as $info_item) { ?>
                            <div class="col-sm-3">
                                <h6>
                                    <?= $info_item['title'] ?>:
                                </h6>
                                <span>
                                    <?= $info_item['text'] ?>
                                </span>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

<?php
$out = ob_get_clean();
return $out;
}
?>

<div class="m-portlet m-portlet--tabs history_and_notes_partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                <li class="nav-item m-tabs__item" data-content="<?= TranslationHelper::getTranslation('modal_history_and_notes__activity_tab_name', $lang, 'Activity') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#history_tab_activity" role="tab">
                        <i class="la la-bar-chart"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('modal_history_and_notes__activity_tab_name', $lang, 'Activity') ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item m-tabs__item" data-content="<?= TranslationHelper::getTranslation('modal_history_and_notes__notes_tab_name', $lang, 'Notes') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_notes" role="tab">
                        <i class="la la-sticky-note"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('modal_history_and_notes__notes_tab_name', $lang, 'Notes') ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item m-tabs__item" data-content="<?= TranslationHelper::getTranslation('modal_history_and_notes__tasks_tab_name', $lang, 'Tasks') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_tasks" role="tab">
                        <i class="la la-check-circle"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('modal_history_and_notes__tasks_tab_name', $lang, 'Tasks') ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item m-tabs__item" data-content="<?= TranslationHelper::getTranslation('modal_history_and_notes__email_tab_name', $lang, 'Email') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_email" role="tab">
                        <i class="la la-envelope"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('modal_history_and_notes__email_tab_name', $lang, 'Email') ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item m-tabs__item" data-content="<?= TranslationHelper::getTranslation('modal_history_and_notes__sms_tab_name', $lang, 'SMS') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_sms" role="tab">
                        <i class="la la-weixin"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('modal_history_and_notes__sms_tab_name', $lang, 'SMS') ?>
                        </span>
                    </a>
                </li>
<!--                <li class="nav-item m-tabs__item" data-content="<?//= TranslationHelper::getTranslation('modal_history_and_notes__calls_tab_name', $lang, 'Calls') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_calls" role="tab">
                        <i class="la la-phone"></i>
                        <span>
                            <?//= TranslationHelper::getTranslation('modal_history_and_notes__calls_tab_name', $lang, 'Calls') ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item m-tabs__item" data-content="<?//= TranslationHelper::getTranslation('modal_history_and_notes__chat_tab_name', $lang, 'Chat') ?>" data-toggle="m-popover" data-placement="top">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#history_tab_chat" role="tab">
                        <i class="la la-comments"></i>
                        <span>
                            <?//= TranslationHelper::getTranslation('modal_history_and_notes__chat_tab_name', $lang, 'Chat') ?>
                        </span>
                    </a>
                </li>-->
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="tab-content">
            <div class="tab-pane active" id="history_tab_activity" role="tabpanel">
                <?php if(isset($timeline['all']) && !empty($timeline['all'])) { ?>
                    <div class="row mb-4">
                        <div class="col-sm-8 col-md-9 mb-4 mb-sm-0">
                            <div class="input-group">
                                <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_search_input_placeholder', $lang, 'Search parameter') ?>" aria-describedby="timeline-search-addon2">
                                <div class="input-group-append">
                                    <a href="#" class="btn btn-metal m-btn m-btn--icon">
                                        <span>
                                            <i class="la la-search"></i>
                                            <span>
                                                <?= TranslationHelper::getTranslation('contacts_timeline_search_input_button', $lang, 'Search') ?>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3">
                            <div class="filters-dropdown m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--huge m-dropdown--align-right float-right" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle btn btn-brand btn-outline-brand dropdown-toggle">
                                    <?= TranslationHelper::getTranslation('contacts_timeline_filter_title', $lang, 'Filter') ?>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <div class="filters-dropdown-section">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_selection_activity', $lang, 'Activity') ?>
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_tasks', $lang, 'Tasks') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_notes', $lang, 'Notes') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="filters-dropdown-section">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_selection_changes', $lang, 'Changes') ?>
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_creating', $lang, 'Created') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_deleting', $lang, 'Deleted') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_updating', $lang, 'Edited') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="filters-dropdown-section">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_selection_sales', $lang, 'Sales') ?>
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_requests', $lang, 'Requests') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_bids', $lang, 'Bids') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="filters-dropdown-section">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_selection_email', $lang, 'Email') ?>
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_income', $lang, 'Income') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_outgo', $lang, 'Outgo') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="filters-dropdown-section">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first">
                                                            <span class="m-nav__section-text">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_selection_calls', $lang, 'Calls') ?>
                                                            </span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_income', $lang, 'Income') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox">
                                                                <?= TranslationHelper::getTranslation('contacts_timeline_filter_label_outgo', $lang, 'Outgo') ?>
                                                                <span></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-timeline-1 m-timeline-1--fixed ita-custom">
                        <div class="m-timeline-1__items">
                            <div class="m-timeline-1__marker"></div>
                            <?php foreach ($timeline['all'] as $key => $timeline_item) { ?>
                                <?= render_timeline_item($timeline_item, ($key===0)); ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m--align-center">
                            <button type="button" class="btn btn-sm m-btn--custom m-btn--pill  btn-danger">
                                Load More
                            </button>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="tab-pane" id="history_tab_notes" role="tabpanel">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="row" style="margin-top: -26px;margin-right: -45px;margin-left: -45px;">
                            <div class="col-sm-12">
                                <form class="m-form m-form--fit m-form--label-align-right" id="add_note_from_timeline_form" method="post" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/companies/add-notes?id=' . $company['id'] ?>">
                                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                    <textarea name="noteValue" id="add-note-from_timeline-redactor"></textarea>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right">
                        <button type="submit" form="add_note_from_timeline_form" class="btn btn-primary">
                            <?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                        </button>
                    </div>
                </div>
                <?php if(isset($timeline['notes']) && !empty($timeline['notes'])) { ?>
                    <div class="m-timeline-1 m-timeline-1--fixed ita-custom">
                        <div class="m-timeline-1__items">
                            <div class="m-timeline-1__marker"></div>
                            <?php foreach ($timeline['notes'] as $key => $timeline_item) { ?>
                                <?= render_timeline_item($timeline_item, ($key===0)); ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m--align-center">
                            <button type="button" class="btn btn-sm m-btn--custom m-btn--pill  btn-danger">
                                Load More
                            </button>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="tab-pane" id="history_tab_tasks" role="tabpanel">
                <div class="mb-4" id="timeline-task-create-block">
                    <div class="row mb-3">
                        <div class="col-12 col-sm-12 col-md-6 col-xl-12 col-xxl-6 mb-3 mb-md-0 mb-xl-3 mb-xxl-0">
                            <label for="create_task_input__title">
                                <?= TranslationHelper::getTranslation('contacts_timeline_create_task_name_label', $lang, 'Task name') ?>
                            </label>
                            <input type="text" name="Tasks[title]" id="create_task_input__title" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('create_task_input_title_placeholder', $lang, 'Task title');?>" required>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-xl-12 col-xxl-6">
                            <div class="row">
                                <div class="col-12">
                                    <label>
                                        <?= TranslationHelper::getTranslation('contacts_timeline_task_due_date_label', $lang, 'Task due date'); ?>
                                    </label>
                                </div>
                                <div class="col-12 col-sm-6 col-md-7 col-xl-6 col-xxl-7 mb-3 mb-sm-0">
                                    <div class="input-group">
                                        <input type="text" name="Tasks[due_date]" id="timeline_new_task_due_date" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_due_date_placeholder', $lang, 'Due date') ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-5 col-xl-6 col-xxl-5">
                                    <div class="input-group">
                                        <input type="text" name="Tasks[due_time]" id="timeline_new_task_due_time" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_due_time_placeholder', $lang, 'Due time') ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="edit_task_note_redactor">
                                <?= TranslationHelper::getTranslation('contacts_timeline_task_note_textarea_labels', $lang, 'Notes') ?>
                            </label>
                            <textarea id="timeline-create_task_note_textarea" class="form-control m-input" name="Tasks[description]" rows="3" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_note_textarea_placeholder', $lang, 'Your note...') ?>"></textarea>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 col-md-6 col-xl-12 col-xxl-6 mb-3 mb-sm-0 mb-xl-3 mb-xxl-0">
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-3 mb-md-0">
                                    <label for="timeline-create_task_type">
                                        <?= TranslationHelper::getTranslation('contacts_timeline_task_type_label', $lang, 'Task type'); ?>
                                    </label>
                                    <select name="Tasks[type_id]" id="timeline-create_task_type" class="form-control m-select2">
                                        <?php if(isset($all_task_types) && !empty($all_task_types)) { ?>
                                            <?php foreach($all_task_types as $task_type){ ?>
                                                <option value="<?= $task_type['id']?>"<?= ($task_type['default']) ? ' selected' : '' ?>><?= $task_type['value'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="timeline-create_task_assigned_to_id_select">
                                        <?= TranslationHelper::getTranslation('contacts_timeline_task_assigned_to_label', $lang, 'Assigned to'); ?>
                                    </label>
                                    <select name="Tasks[assigned_to_id]" id="timeline-create_task_assigned_to_id_select" class="form-control m-select2">
                                        <option value="<?= \Yii::$app->user->id ?>"><?= \Yii::$app->user->identity->last_name ?> <?= \Yii::$app->user->identity->first_name ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-12 col-xxl-6">
                            <div class="row">
                                <div class="col-12">
                                    <label>
                                        <?= TranslationHelper::getTranslation('contacts_timeline_task_email_reminder_date_label', $lang, 'Email reminder date'); ?>
                                    </label>
                                </div>
                                <div class="col-12 col-sm-6 col-md-7 col-xl-6 col-xxl-7 mb-3 mb-sm-0">
                                    <div class="input-group">
                                        <input type="text" name="Tasks[email_reminder_date]" id="timeline_new_task_email_reminder_date" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_email_reminder_date_placeholder', $lang, 'Email date') ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-5 col-xl-6 col-xxl-5">
                                    <div class="input-group">
                                        <input type="text" name="Tasks[email_reminder_time]" id="timeline_new_task_email_reminder_time" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('contacts_timeline_task_email_reminder_time_placeholder', $lang, 'Email time') ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center text-sm-right">
                            <a href="#" class="btn btn-primary m-btn m-btn--icon">
                                <span>
                                    <i class="fa fa-save"></i>
                                    <span>
                                        <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <?php if(isset($timeline['tasks']) && !empty($timeline['tasks'])) { ?>
                    <div class="m-timeline-1 m-timeline-1--fixed ita-custom">
                        <div class="m-timeline-1__items">
                            <div class="m-timeline-1__marker"></div>
                            <?php foreach ($timeline['tasks'] as $key => $timeline_item) { ?>
                                <?= render_timeline_item($timeline_item, ($key===0)); ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m--align-center">
                            <button type="button" class="btn btn-sm m-btn--custom m-btn--pill  btn-danger">
                                Load More
                            </button>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="tab-pane" id="history_tab_email" role="tabpanel">
                Email coming soon...
            </div>
            <div class="tab-pane" id="history_tab_sms" role="tabpanel">
                SMS coming soon...
            </div>
<!--            <div class="tab-pane" id="history_tab_calls" role="tabpanel">
                Calls coming soon...
            </div>
            <div class="tab-pane" id="history_tab_chat" role="tabpanel">
                Chat coming soon...
            </div>-->
        </div>
    </div>
</div>