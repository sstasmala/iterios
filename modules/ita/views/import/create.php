<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('imp_crt_title', $lang, 'Create import');
$this->registerCssFile('@web/css/import/create.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/import/create.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    // Render modals here...
$this->endBlock();
?>

<div class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('imp_crt_title', $lang, 'Create import') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-wizard m-wizard--2 m-wizard--success" id="import_wizard">
        <div class="m-portlet__padding-x">
            <!-- Here you can put a message or alert -->
        </div>
        <div class="m-wizard__head m-portlet__padding-x">
            <div class="m-wizard__progress">
                <div class="progress">
                    <div class="progress-bar" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="m-wizard__nav">
                <div class="m-wizard__steps">
                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="import_wizard_form_step_1">
                        <a href="#"  class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-th-large"></i>
                            </span>
                        </a>
                        <div class="m-wizard__step-info">
                            <div class="m-wizard__step-title">
                                1. <?= TranslationHelper::getTranslation('imp_crt_step_1_title', $lang, 'Select module') ?>
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__step" m-wizard-target="import_wizard_form_step_2">
                        <a href="#" class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-download"></i>
                            </span>
                        </a>
                        <div class="m-wizard__step-info">
                            <div class="m-wizard__step-title">
                                2. <?= TranslationHelper::getTranslation('imp_crt_step_2_title', $lang, 'Upload file') ?>
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__step" m-wizard-target="import_wizard_form_step_3">
                        <a href="#" class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-list-ul"></i>
                            </span>
                        </a>
                        <div class="m-wizard__step-info">
                            <div class="m-wizard__step-title">
                                3. <?= TranslationHelper::getTranslation('imp_crt_step_3_title', $lang, 'Properties mapping') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-wizard__form">
            <!--
            1) Use m-form--label-align-left class to alight the form input lables to the right
            2) Use m-form--state class to highlight input control borders on form validation
            -->
            <form class="m-form m-form--label-align-left- m-form--state-" id="import_form" method="POST" action="<?= Yii::$app->params['baseUrl'].'/ita/'.Yii::$app->user->identity->tenant->id.'/import/detail' ?>">
                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                <div class="m-portlet__body">
                    <div class="m-wizard__form-step m-wizard__form-step--current" id="import_wizard_form_step_1">
                        <div class="row mb-4">
                            <div class="col-12 import-type-change-input-wrap m--font-bolder m--font-danger">
                                <input type="hidden" name="Import[module]" id="import-type-change-input">
                            </div>
                            <div class="col-12">
                                <div id="import-type-change">
                                    <div class="inner">
                                        <div class="change-item mb-3" data-module-name="contacts">
                                            <div class="inner">
                                                <div class="icon">
                                                    <i class="flaticon-users"></i>
                                                </div>
                                                <div class="title">
                                                    <h6><?= TranslationHelper::getTranslation('imp_crt_select_module_contacts_label', $lang, 'Contacts') ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="change-item mb-3" data-module-name="companies">
                                            <div class="inner">
                                                <div class="icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="title">
                                                    <h6><?= TranslationHelper::getTranslation('imp_crt_select_module_companies_label', $lang, 'Compaies') ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="change-item mb-3" data-module-name="requests">
                                            <div class="inner">
                                                <div class="icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                </div>
                                                <div class="title">
                                                    <h6><?= TranslationHelper::getTranslation('imp_crt_select_module_requests_label', $lang, 'Requests') ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="change-item mb-3" data-module-name="orders">
                                            <div class="inner">
                                                <div class="icon">
                                                    <i class="flaticon-map"></i>
                                                </div>
                                                <div class="title">
                                                    <h6><?= TranslationHelper::getTranslation('imp_crt_select_module_orders_label', $lang, 'Orders') ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__form-step" id="import_wizard_form_step_2">
                        <div class="row">
                            <div class="col-xl-8 offset-xl-2">
                                <div class="m-form__section m-form__section--first">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">
                                            <?= TranslationHelper::getTranslation('imp_crt_step_2_title', $lang, 'Upload file') ?>
                                        </h3>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-12">
                                            <div class="m-dropzone dropzone" action="inc/api/dropzone/upload.php" id="import-dropzone-one">
                                                <div class="m-dropzone__msg dz-message needsclick">
                                                    <h3 class="m-dropzone__msg-title">
                                                        Drop files here or click to upload.
                                                    </h3>
                                                    <span class="m-dropzone__msg-desc">
                                                        This is just a demo dropzone. Selected files are
                                                        <strong>
                                                            not
                                                        </strong>
                                                        actually uploaded.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group description-block row">
                                        <div class="icon col-sm-2 mb-3 mb-sm-0">
                                            <i class="la la-thumb-tack"></i>
                                        </div>
                                        <div class="text col-sm-10">
                                            <h5 class="mb-3">
                                                <?= TranslationHelper::getTranslation('imp_crt_import_faq_description_title', $lang, 'Some content about import') ?>
                                            </h5>
                                            <p>
                                                1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                            <p>
                                                2. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                            </p>
                                            <p>
                                                3. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__form-step" id="import_wizard_form_step_3">
                        <div class="row mb-3">
                            <div class="col-12">
                                <span>
                                    Good news! These columns match some standard ITA properties. If one of these properties was incorrectly mapped, just map it to a different ITA property or create a new one of your own.
                                </span>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-12">
                                <div class="m-form__heading mb-0">
                                    <h3 class="m-form__heading-title mb-0">
                                        <?= TranslationHelper::getTranslation('imp_crt_matches_columns_table_label', $lang, 'Columns with matches') ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <table id="prop_with_matches_table" class="table m-table m-table--head-bg-success">
                                    <thead>
                                        <tr>
                                            <th class="heading_checkbox_cell">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" class="check_all_props" name="Import[check_all]" checked>
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_label_title', $lang, 'Column label from csv') ?>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_preview_title', $lang, 'Preview information') ?>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_property_title', $lang, 'Ita property') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($import_demo['with_matches'] as $key => $property) { ?>
                                            <tr>
                                                <td>
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" class="row_checkbox" name="Import[properties][<?= $key ?>][id]" value="<?= $key ?>" checked>
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <?= $property['label'] ?>
                                                </td>
                                                <td>
                                                    <?= $property['preview'] ?>
                                                </td>
                                                <td>
                                                    <select class="form-control m-select2 import_properties_select" name="Import[properties][<?= $key ?>][type]">
                                                        <optgroup label="Property set 1">
                                                            <option value="AK">
                                                                Property type 1
                                                            </option>
                                                            <option value="HI">
                                                                Property type 2
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 2">
                                                            <option value="CA">
                                                                Property type 3
                                                            </option>
                                                            <option value="NV" selected>
                                                                Property type 4
                                                            </option>
                                                            <option value="OR">
                                                                Property type 5
                                                            </option>
                                                            <option value="WA">
                                                                Property type 6
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 3">
                                                            <option value="AZ">
                                                                Property type 7
                                                            </option>
                                                            <option value="CO">
                                                                Property type 8
                                                            </option>
                                                            <option value="ID">
                                                                Property type 9
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 4">
                                                            <option value="AL">
                                                                Property type 10
                                                            </option>
                                                            <option value="AR">
                                                                Property type 11
                                                            </option>
                                                            <option value="IL">
                                                                Property type 12
                                                            </option>
                                                        </optgroup>
                                                    </select>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-12">
                                <span>
                                    No matches were found for the columns below. You can either map each one to an existing ITA property, or create a new ITA property from scratch.
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="m-form__heading mb-0">
                                    <h3 class="m-form__heading-title mb-0">
                                        <?= TranslationHelper::getTranslation('imp_crt_not_matches_columns_table_label', $lang, 'Columns without matches') ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-12">
                                <span class="hint">
                                    <?= TranslationHelper::getTranslation('imp_crt_ignor_warning_message', $lang, 'Unmatched and unchecked columns will be ignored.') ?>
                                </span>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <table id="prop_without_matches_table" class="table m-table m-table--head-bg-success">
                                    <thead>
                                        <tr>
                                            <th class="heading_checkbox_cell">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" class="check_all_props" name="Import[check_all]">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_label_title', $lang, 'Column label from csv') ?>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_preview_title', $lang, 'Preview information') ?>
                                            </th>
                                            <th>
                                                <?= TranslationHelper::getTranslation('imp_crt_matches_columns_property_title', $lang, 'Ita property') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($import_demo['without_matches'] as $key => $property) { ?>
                                            <tr>
                                                <td>
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" class="row_checkbox" name="Import[properties][<?= $key ?>][id]" value="<?= $key ?>">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <?= $property['label'] ?>
                                                </td>
                                                <td>
                                                    <?= $property['preview'] ?>
                                                </td>
                                                <td>
                                                    <select class="form-control m-select2 import_properties_select" name="Import[properties][<?= $key ?>][type]">
                                                        <optgroup label="Property set 1">
                                                            <option value="AK">
                                                                Property type 1
                                                            </option>
                                                            <option value="HI">
                                                                Property type 2
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 2">
                                                            <option value="CA">
                                                                Property type 3
                                                            </option>
                                                            <option value="NV">
                                                                Property type 4
                                                            </option>
                                                            <option value="OR">
                                                                Property type 5
                                                            </option>
                                                            <option value="WA" selected>
                                                                Property type 6
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 3">
                                                            <option value="AZ">
                                                                Property type 7
                                                            </option>
                                                            <option value="CO">
                                                                Property type 8
                                                            </option>
                                                            <option value="ID">
                                                                Property type 9
                                                            </option>
                                                        </optgroup>
                                                        <optgroup label="Property set 4">
                                                            <option value="AL">
                                                                Property type 10
                                                            </option>
                                                            <option value="AR">
                                                                Property type 11
                                                            </option>
                                                            <option value="IL">
                                                                Property type 12
                                                            </option>
                                                        </optgroup>
                                                    </select>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4 m--align-left">
                                <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                    <span>
                                        <i class="la la-arrow-left"></i>
                                        &nbsp;&nbsp;
                                        <span>
                                            <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 m--align-right">
                                <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                    <span>
                                        <i class="la la-flag-checkered"></i>
                                        &nbsp;&nbsp;
                                        <span>
                                            <?= TranslationHelper::getTranslation('imp_crt_proccess_import_btn', $lang, 'Start import') ?>
                                        </span>
                                    </span>
                                </a>
                                <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                    <span>
                                        <span>
                                            <?= TranslationHelper::getTranslation('next', $lang, 'Next') ?>
                                        </span>
                                        &nbsp;&nbsp;
                                        <i class="la la-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>