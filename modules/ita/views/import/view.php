<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('imp_view_title', $lang, 'Import detail');
$this->registerCssFile('@web/css/import/view.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/import/view.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    // Render modals here...
$this->endBlock();
?>
<a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/import/index') ?>" class="btn btn-info m-btn m-btn--icon mb-3">
    <span>
        <i class="fa fa-arrow-left"></i>
        <span>
            <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
        </span>
    </span>
</a>
<div class="m-portlet m-portlet--mobile" id="import_view_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('imp_view_title', $lang, 'Import detail') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-12">
                <div id="import-details-datatable" class="mt-3"></div>
            </div>
        </div>
    </div>
</div>