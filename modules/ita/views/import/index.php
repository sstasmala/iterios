<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('imp_inx_title', $lang, 'Import');
$this->registerCssFile('@web/css/import/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/import/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    // Render modals here...
$this->endBlock();
?>

<div class="m-portlet m-portlet--mobile" id="import_index_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="fa fa-download"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('imp_inx_import_portlet_title', $lang, 'Imports') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/import/create') ?>" class="btn btn-success">
                        <?= TranslationHelper::getTranslation('imp_inx_go_to_import_btn', $lang, 'Start an import') ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row mb-4">
            <div class="col-12">
                <div id="import-faq">
                    <div class="inner">
                        <div class="faq-block">
                            <div class="inner">
                                <div class="icon">
                                    <i class="fas fa-video"></i>
                                </div>
                                <div class="title">
                                    <h6>How to prepare your spreadsheet</h6>
                                </div>
                                <div class="description">
                                    <span>Coming to HubSpot from Excel or Google Sheets? The guide below will walk you through how to prepare your files for a successful import.</span>
                                </div>
                                <div class="link">
                                    <a href="#" class="m-link m--font-bolder">View import guide</a>
                                </div>
                            </div>
                        </div>
                        <div class="faq-block">
                            <div class="inner">
                                <div class="icon">
                                    <i class="fas fa-cloud"></i>
                                </div>
                                <div class="title">
                                    <h6>Download a sample spreadsheet</h6>
                                </div>
                                <div class="description">
                                    <span>Need to see this in action first? Download this small sample file and test the import process so there are no surprises.</span>
                                </div>
                                <div class="link">
                                    <a href="#" class="m-link m--font-bolder">Download sample spreadsheet</a>
                                </div>
                            </div>
                        </div>
                        <div class="faq-block">
                            <div class="inner">
                                <div class="icon">
                                    <i class="fa fa-info-circle"></i>
                                </div>
                                <div class="title">
                                    <h6>Have questions?</h6>
                                </div>
                                <div class="description">
                                    <span>Migrating data should be easy. Read the answers to all your questions about data security, file types, and troubleshooting.</span>
                                </div>
                                <div class="link">
                                    <a href="#" class="m-link m--font-bolder">View the FAQ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12">
                <div id="import-filter">
                    <div class="inner">
                        <ul class="filters-list">
                            <li class="filter-item active">
                                <span class="m--font-bold">
                                    <?= TranslationHelper::getTranslation('imp_inx_import_filter_contact_title', $lang, 'Contacts') ?>
                                </span>
                            </li>
                            <li class="filter-item">
                                <span class="m--font-bold">
                                    <?= TranslationHelper::getTranslation('imp_inx_import_filter_companies_title', $lang, 'Companies') ?>
                                </span>
                            </li>
                            <li class="filter-item">
                                <span class="m--font-bold">
                                    <?= TranslationHelper::getTranslation('imp_inx_import_filter_requests_title', $lang, 'Requests') ?>
                                </span>
                            </li>
                            <li class="filter-item">
                                <span class="m--font-bold">
                                    <?= TranslationHelper::getTranslation('imp_inx_import_filter_orders_title', $lang, 'Orders') ?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="import-history-datatable" class="mt-3"></div>
            </div>
        </div>
    </div>
</div>