<?php
use app\helpers\TranslationHelper;
use app\helpers\MCHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$search = \Yii::$app->request->get('search');
?>

<div class="row filters-right-search">
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Данные рассылки
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                    <div class="m-accordion__item-body show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                        <div class="m-accordion__item-content">
                            <div class="form-group">
                                <input type="text" class="form-control m-input" name="title" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_top_placeholder_field_name', $lang, 'Name delivery') ?>">
                            </div>
                            <div class="form-group">
                                <select class="form-control m-select2" id="select_provider" name="select_provider"></select>
                            </div>
                            <div class="form-group">
                                <select class="form-control m-select2" id="select_status" name="select_status"></select>
                            </div>
                            <div class="form-group">
                                <div class="input-group" id="m_daterangepicker_completion">
                                    <?= \yii\helpers\Html::input('hidden', 'date_start') ?>
                                    <?= \yii\helpers\Html::input('hidden', 'date_end') ?>
                                    <input type="text" class="form-control m-input" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_top_placeholder_field_date_send', $lang, 'Date send') ?>"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control m-select2" id="select_user" name="select_user">
                                    <option value="<?= \Yii::$app->user->id ?>"><?= \Yii::$app->user->identity->last_name ?> <?= \Yii::$app->user->identity->first_name ?></option>
                                </select>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    <div class="col-lg-6">
        
    </div>
</div>