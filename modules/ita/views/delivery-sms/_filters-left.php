<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold active" id="all_sms">
        <?= TranslationHelper::getTranslation('all_sended_sms', $lang) ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="my_sms">
        <?= TranslationHelper::getTranslation('my_sended_sms', $lang) ?>
    </a>
</div>
