<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$delivery_name = (isset($delivery['name']) && !empty($delivery['name'])) ? (trim($delivery['name'])) : '';
$this->title = TranslationHelper::getTranslation('dess_sms_view_title', $lang, 'Sms delivery') . ' - ' . $delivery_name;
$this->registerJsFile('@web/js/delivery-sms/view.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/delivery-sms/view.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJs('var send_sms_id = '.$model->id.';', \yii\web\View::POS_HEAD);
?>

<?php
$this->beginBlock('extra_modals');
$this->endBlock();
?>

<div class="m-portlet m-portlet--mobile" id="sms_delivery_stats_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $model->name; ?>
                </h3>
            </div>
        </div>
    </div>
    <?php
        $percentTrue = 0;
        $percentFalse = 0;

        if(isset($model->count_delivered) && isset($model->count_phones) && isset($model->count_not_delivered)) {
            $percentTrue  = ($model->count_delivered * 100) / $model->count_phones;
            $percentFalse = ($model->count_not_delivered * 100) / $model->count_phones;
        }
    ?>
    <div class="m-portlet__body">
        <div class="progress-stats-block mb-4 row">
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="recipients_count_block mb-4 row">
                    <div class="col-6">
                        <span class="text-nowrap">
                            <span class="count-block font-italic m--font-bolder m--font-brand mr-1"><?= isset($model->count_phones)? $model->count_phones:'';?></span>
                            <span class="word-block font-italic m--font-bold"><?= TranslationHelper::getTranslation('dess_sms_view_recipients_count_label', $lang, 'Recipients') ?></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="recipients_count_block mb-4 row">
                    <div class="col-6">
                        <span class="text-nowrap">
                            <span class="word-block font-italic m--font-bold"><?= TranslationHelper::getTranslation('dess_email_delivery_provider_label', $lang, 'Recipients') ?>:</span>
                            <span class="count-block font-italic m--font-bolder m--font-brand mr-1"><?= isset($model->count_phones)? $model->provider['name']:'';?></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="progress-stats-block mb-4 row">
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="d-flex justify-content-between m--font-bolder mb-1">
                    <span><?= TranslationHelper::getTranslation('dess_sms_view_delivery_rate_label', $lang, 'Delivery rate') ?></span>
                    <span class="m--font-brand"><?= isset($percentTrue)? $percentTrue:''?>%</span>
                </div>
                <div class="progress">
                    <div class="progress-bar m--bg-success" role="progressbar" style="width: <?= isset($percentTrue)? $percentTrue:''?>%;" aria-valuenow="<?= isset($percentTrue)? $percentTrue:''?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="d-flex justify-content-between m--font-bolder mb-1">
                    <span><?= TranslationHelper::getTranslation('dess_sms_view_error_rate_label', $lang, 'Error rate') ?></span>
                    <span class="m--font-brand"><?= isset($percentFalse)? $percentFalse:''?>%</span>
                </div>
                <div class="progress">
                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: <?= isset($percentFalse)? $percentFalse:''?>%;" aria-valuenow="<?= isset($percentFalse)? $percentFalse:''?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
        <div style="height: 25px;"></div>
        <div class="row mb-4">
            <div class="col-12">
                <div class="container">
                    <div class="stats-squares row">
                        <div class="stat-item text-center col-12 col-sm-4">
                            <div class="content">
                                <div class="number m--font-success m--font-bolder">
                                    <span><?= isset($model->count_phones)? $model->count_phones:'';?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_sms_view_send_label', $lang, 'Send') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="stat-item text-center col-12 col-sm-4">
                            <div class="content">
                                <div class="number m--font-info m--font-bolder">
                                    <span><?= isset($model->count_delivered)? $model->count_delivered:'';?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_sms_view_delivery_label', $lang, 'Delivery') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="stat-item text-center col-12 col-sm-4">
                            <div class="content">
                                <div class="number m--font-danger m--font-bolder">
                                    <span><?= isset($model->count_not_delivered)? $model->count_not_delivered:'';?></span>
                                </div>
                                <div class="text m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('dess_sms_view_error_label', $lang, 'Error') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="recipients_count_block mb-4 row">
            <div class="col-12">
                <span class="text-nowrap">
                    <span class="word-block font-italic m--font-bold"><?= TranslationHelper::getTranslation('dess_sms_text_title', $lang, 'Sms text') ?>:</span>
                    <span class="count-block font-italic m--font-bolder m--font-brand mr-1"><?= isset($model->text)? $model->text:'';?></span>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="sms-delivery-report-datatable"></div>
            </div>
        </div>
    </div>
</div>
