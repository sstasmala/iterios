<?php

use app\helpers\TranslationHelper;
use app\models\Providers;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('dess_sms_create_title', $lang, 'Create delivering sms');
$this->registerJsFile('@web/js/delivery-sms/create.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/delivery-sms/wizard.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/delivery-sms/create.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
$this->beginBlock('extra_modals');
$this->endBlock();

?>

<div class="row">
    <div class="col-xl-8 col-xxl-9">
        <div class="m-portlet" id="create_delivery_sms_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <?= TranslationHelper::getTranslation('dess_sms_create_title', $lang, 'Create delivering sms') ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-wizard m-wizard--5 m-wizard--success" id="delivery_sms_wizard">
                <div class="m-portlet__padding-x">
                    <!-- Here you can put a message or alert -->
                </div>
                <div class="m-wizard__head m-portlet__padding-x">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="delivery_sms_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    1.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_sms_wizard_step_1_title', $lang, 'Main settings') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_sms_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    2.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_sms_wizard_step_2_title', $lang, 'SMS') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_sms_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    3.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_sms_wizard_step_3_title', $lang, 'Segments') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="delivery_sms_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-seq">
                                                    4.
                                                </span>
                                                <span class="m-wizard__step-label">
                                                    <?= TranslationHelper::getTranslation('dess_sms_wizard_step_4_title', $lang, 'Finishing') ?>
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-wizard__form">
                    <!--
                        1) Use m-form--label-align-left class to alight the form input lables to the right
                        2) Use m-form--state class to highlight input control borders on form validation
                    -->
                    <form class="m-form m-form--label-align-left- m-form--state-" id="delivery_sms_form">
                        <div class="m-portlet__body">
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="delivery_sms_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    1. <?= TranslationHelper::getTranslation('dess_sms_wizard_step_1_title', $lang, 'Main settings') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_delivery_name_label', $lang, 'Delivery name') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                                    <input type="hidden" name="SendSms[count_phones]" value="" id="countPhones"/>
                                                    <input type="text" name="SendSms[name]" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('dess_sms_delivery_name_placeholder', $lang, 'Enter delivery name') ?>">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_delivery_provider_label', $lang, 'Provider') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <select name="SendSms[provider_id]" class="form-control m-bootstrap-select m_selectpicker send-sms-provider" title="<?= TranslationHelper::getTranslation('dess_sms_delivery_provider_placeholder', $lang, 'Select provider') ?>">
                                                        <?php foreach ($providers as $provider){ ?>
                                                            <option value="<?= $provider->id ?>"><?= $provider->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_delivery_alpha_name_label', $lang, 'Alpha name') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <input type="text" name="SendSms[alpha_name]" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('dess_sms_delivery_alpha_name_placeholder', $lang, 'Enter alpha name') ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_sms_wizard_form_step_2">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    2. <?= TranslationHelper::getTranslation('dess_sms_wizard_step_2_title', $lang, 'SMS') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="form-control-label">
                                                        * <?= TranslationHelper::getTranslation('dess_sms_delivery_sms_body_label', $lang, 'Text') ?>:
                                                    </label>
                                                    <textarea id="dess_sms_delivery_sms_body_textarea" class="form-control m-input" name="SendSms[text]" rows="3" placeholder="<?= TranslationHelper::getTranslation('dess_sms_delivery_sms_body_placeholder', $lang, 'Enter sms body') ?>"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div id="sms-counter">
                                                        <div class="sms-info-item mr-3">
                                                            <i class="la la-keyboard-o mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('dess_sms_delivery_sms_character_length_label', $lang, 'Total') ?>:</span>&nbsp;<span class="length"></span>
                                                        </div>
                                                        <div class="sms-info-item mr-3">
                                                            <i class="la la-flag-checkered mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('dess_sms_delivery_sms_remain_label', $lang, 'Remain') ?>:</span>&nbsp;<span class="remaining"></span>
                                                        </div>
                                                        <div class="sms-info-item mr-3">
                                                            <i class="la la-envelope mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('dess_sms_delivery_sms_count_label', $lang, 'Sms') ?>:</span>&nbsp;<span class="messages"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_sms_wizard_form_step_3">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    3. <?= TranslationHelper::getTranslation('dess_sms_wizard_step_3_title', $lang, 'Segments') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_segments_select_label', $lang, 'Segments select') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 pt-2">
                                                    <?php if(isset($segments)) { ?>
                                                        <div class="segment_checkboxes row">
                                                            <?php foreach ($segments as $segment) { ?>
                                                                <div class="segment_checkbox_item col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 col-xxl-4">
                                                                    <label class="m-checkbox">
                                                                        <input type="checkbox" name="SendSms[segmentsArr][]" class="segment_checkbox" value="<?= $segment->id ?>" count-phones="<?= $segRelations[$segment->id]['count_phones']; ?>">
                                                                        <div class="d-inline text-nowrap">
                                                                            <?= $segment->name . ' (' . $segRelations[$segment->id]['count_phones'] . ')' ?>
                                                                        </div>
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if(isset($segments)) { ?>
                                                <div class="form-group m-form__group row">
                                                    <div id="segment-contacts-search-block" class="col-12 d-none mb-4">
                                                        <div class="input-group">
                                                            <input type="text" id="segments-contacts-search-input" class="form-control" placeholder="<?= TranslationHelper::getTranslation('dess_sms_search_contacts_placeholder', $lang, 'Search contact...') ?>">
                                                            <div class="input-group-append">
                                                                <button id="segments-contacts-search-submit" class="btn btn-primary" type="button">
                                                                    <?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="segments-preview-datatable-wrap">
                                                            <div class="m_datatable" id="segments_preview_datatable"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__form-step" id="delivery_sms_wizard_form_step_4">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    4. <?= TranslationHelper::getTranslation('dess_sms_wizard_step_4_title', $lang, 'Finishing') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_preview_label', $lang, 'Sms preview') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="result-deliver-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_recievers_count_label', $lang, 'Receivers count') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="result-deliver-phones">1408</span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row d-none">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('dess_sms_sending_date_label', $lang, 'Sending date') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9 font-weight-light pt-2">
                                                    <span class="text-nowrap mr-2">
                                                        <i class="la la-calendar mr-1"></i>
                                                        <span class="result-deliver-date">23.07.2018</span>
                                                    </span>
                                                    <span class="text-nowrap">
                                                        <i class="la la-clock-o mr-1"></i>
                                                        <span class="result-deliver-time">12:30</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                            <span>
                                                <i class="la la-arrow-left"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 m--align-right">
                                        <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                            <span>
                                                <i class="la la-check"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    <?= TranslationHelper::getTranslation('complete', $lang, 'Complete') ?>
                                                </span>
                                            </span>
                                        </a>
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                            <span>
                                                <span>
                                                    <?= TranslationHelper::getTranslation('save_and_continue', $lang, 'Save & Continue') ?>
                                                </span>
                                                &nbsp;&nbsp;
                                                <i class="la la-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-xxl-3">
        <div class="m-portlet" id="deliveries_sms_stats_portlet">
            <?php
            if(!isset($provider->id)){
                ?>
            <div class="m-portlet__body ">
                <div class="m-section">
                    <?= TranslationHelper::getTranslation('dont_have_provider_label', $lang, 'You does not have active provider and you must activate') ?>
                    </br>
                    <a href="/ia/providers" target="_blank" rel="nofollow">
                        <?= TranslationHelper::getTranslation('dont_have_provider_integration_link', $lang, 'Integration') ?>
                    </a>
                </div>
            </div>
                <?php
            } else {
                ?>
                <div class="m-portlet__body ">
                    <div class="m-section">
                        <h2 class="m-section__heading">
                            <?= (isset($provider->name))? $provider->name : '' ?>
                        </h2>
                        <div class="m-section__content provider-description">
                            <p>
                                <span><?= (isset($provider->description))? $provider->description : ''?></span>
                            </p>
                            <p>
                            <table class="provider-stats-table">
                                <tbody>
                                <tr>
                                    <td><?= TranslationHelper::getTranslation('dess_sms_website_label', $lang, 'Website') ?>:</td>
                                    <td>
                                        <a href="<?= (isset($provider->website))? $provider->website : ''?>" target="_blank" rel="nofollow">
                                            <?= (isset($provider->website))? $provider->website : ''?>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= TranslationHelper::getTranslation('dess_sms_current_balance_label', $lang, 'Current balance') ?>:</td>
                                    <td>460 UAH</td>
                                </tr>
                                </tbody>
                            </table>
                            </p>
                        </div>
                    </div>
                    <div class="m-separator m-separator--fit"></div>
                    <div class="delivery-sms_statistic_wrap mb-4">
                        <div class="m-subheader">
                            <div class="d-flex align-items-center">
                                <div>
                                <span class="m-subheader__daterange" id="delivery-sms_statistic_date_filter">
                                    <span class="m-subheader__daterange-label">
                                        <span class="m-subheader__daterange-title"></span>
                                        <span class="m-subheader__daterange-date m--font-brand"></span>
                                    </span>
                                    <a href="#" onclick="event.preventDefault()" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1 m-widget1--paddingless">
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">
                                        <?= TranslationHelper::getTranslation('dev_sms_stats_contacts_quantity', $lang, 'Contacts quantity') ?>
                                    </h3>
                                    <span class="m-widget1__desc">
                                    <?= TranslationHelper::getTranslation('dev_sms_stats_contacts_quantity_desc', $lang, 'Delivered sms quantity') ?>
                                </span>
                                </div>
                                <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand">
                                    548
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">
                                        <?= TranslationHelper::getTranslation('dev_sms_stats_deliveries_quantity', $lang, 'Deliveries quantity') ?>
                                    </h3>
                                    <span class="m-widget1__desc"></span>
                                </div>
                                <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand">
                                    4
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>