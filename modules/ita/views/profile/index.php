<?php
/**
 * views/profile/index.php
 * @copyright © Iterios
 * @author Valentin Smagluk 1valeks7@gmail.com
 */

use yii\helpers\Html;
use app\models\ProfileNotifications;
use app\models\ProfileNotificationsTags;
use app\components\notifications_now\NotificationActions;

/* @var $this yii\web\View */
$lang = Yii::$app->user->identity->tenant->language->iso;

$this->registerCssFile('@web/css/profile/index.min.css',
    ['position' => yii\web\View::POS_BEGIN, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/js/profile/index.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/js/profile/dropzone.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

$this->title = \app\helpers\TranslationHelper::getTranslation('my_profile',$lang);

$currTenant = Yii::$app->user->identity->tenant->id;

$tab = Yii::$app->request->get('tab');
$tab = empty($tab) ? 1 : $tab;
$tenant_count = Yii::$app->user->identity->tenants;
?>

<div class="row">
    <div class="col-xl-3 col-lg-4">
        <div class="m-portlet m-portlet--full-height" id="my_profile_preview_portlet">
            <div class="m-portlet__body">
                <div class="m-card-profile">
                    <div class="m-card-profile__title m--hide">
                        <?= \app\helpers\TranslationHelper::getTranslation('my_profile',$lang);?>
                    </div>
                    <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                            <img class="profile_picture" src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty($data['user']['photo']) ? $data['user']['photo'] : 'img/profile_default.png') ?>" alt=""/>
                        </div>
                    </div>
                    <div class="m-card-profile__details">
                        <span class="m-card-profile__name profile_name">
                            <?= $data['user']['first_name'] ?> <?= $data['user']['last_name'] ?>
                        </span>
                        <a href="" class="m-card-profile__email m-link profile_email">
                            <?= $data['user']['email'] ?>
                        </a>
                    </div>
                </div>
                <div class="m-dropdown__wrapper tenants_dropdown_table">
                    <div class="m-dropdown__inner">
                        <div class="m-dropdown__body m-dropdown__body--paddingless">
                            <div class="m-dropdown__content">
                                <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                                    <div class="m-nav-grid m-nav-grid--skin-light">
                                        <div class="m-nav-grid__row">
                                            <div class="m-nav-grid__item2">
                                                <div class="m-dropdown__header m--align-center">
                                                <span class="m-dropdown__header-title">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('set_tenant',$lang);?>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-dropdown__wrapper tenants_dropdown_table">
                    <div class="m-dropdown__inner">
                        <div class="m-dropdown__body m-dropdown__body--paddingless">
                            <div class="m-dropdown__content">
                                <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                                    <div class="m-nav-grid m-nav-grid--skin-light">
                                        <?php if (!empty($tenant_count)) { ?>
                                            <?php
                                            $i = 0;
                                            ?>
                                            <?php foreach ($tenant_count as $tenant) { ?>
                                                <?php $i++ ?>
                                                <?= ($i % 2 ? '<div class="m-nav-grid__row">' : '') ?>
                                                <a href="/change-tenant?id=<?= $tenant->id ?>" class="m-nav-grid__item">
                                                    <i class="m-nav-grid__icon flaticon-suitcase"></i>
                                                    <span class="m-nav-grid__text">
                                                        <?= $tenant->name ?>
                                                    </span>
                                                    <span class="m-nav-grid__text account-id-info">
                                                        <?= app\helpers\TranslationHelper::getTranslation('account', $lang, 'Account') . ' ID: ' . $tenant->id ?>
                                                    </span>
                                                    <?= ($tenant->id == $currTenant) ? '<span class="m-badge m-badge--warning m-badge--wide m--margin-top-10">' . app\helpers\TranslationHelper::getTranslation('current', $lang, 'Current') . '</span>' : ''; ?>
                                                </a>
                                                <?= (!($i % 2) || ($i == count(Yii::$app->user->identity->tenants))) ? '</div>' : '' ?>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-9 col-lg-8">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">

                            <a class="nav-link m-tabs__link<?= $tab == 1 ? ' active':''; ?>" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                <?= \app\helpers\TranslationHelper::getTranslation('profile_update',$lang);?>
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link<?= $tab == 2 ? ' active':''; ?>" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                <?= \app\helpers\TranslationHelper::getTranslation('profile_change_picture',$lang);?>
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link<?= $tab == 3 ? ' active':''; ?>" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                <?= \app\helpers\TranslationHelper::getTranslation('profile_notifications', $lang, 'Notifications');?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane<?= $tab == 1 ? ' active':''; ?>" id="m_user_profile_tab_1">
                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        1. <?= \app\helpers\TranslationHelper::getTranslation('profile_personal_details',$lang);?>
                                    </h3>
                                </div>
                            </div>
                            <form id="personal_details" class="m--margin-top-10" action="">
                                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_last_name',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('profile_your_last_name',$lang);?>" name="last_name" value="<?= $data['user']['last_name'] ?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_first_name',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('profile_your_first_name',$lang);?>" name="first_name" value="<?= $data['user']['first_name'] ?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_middle_name',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('profile_your_middle_name',$lang);?>" name="middle_name" value="<?= $data['user']['middle_name'] ?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row test">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_phone',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input type="text" class="form-control m-input masked-input validate-phone input_phone_visible" name="phone" id="order_edit__contact_phone_input"  valid="phoneNumber" value="<?= $data['user']['phone'] ?>">
                                    </div>
                                </div>
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button id="personal_details_submit" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                <?= \app\helpers\TranslationHelper::getTranslation('save_changes_button',$lang);?>
                                            </button>
                                            &nbsp;&nbsp;
                                            <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                <?= \app\helpers\TranslationHelper::getTranslation('cancel',$lang);?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        2. <?= \app\helpers\TranslationHelper::getTranslation('change_email',$lang);?>
                                    </h3>
                                </div>
                            </div>
                            <form id="change_email" class="m--margin-top-10" action="" method="POST">
                                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_email',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input id="my_email" class="form-control m-input" type="text" disabled="disabled" name="email" value="<?= $data['user']['email'] ?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_new_email',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_new_email',$lang);?>" name="email">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_pass',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('profile_your_pass',$lang);?>" name="password">
                                        <span class="m-form__help">
                                            <?= \app\helpers\TranslationHelper::getTranslation('profile_enter_pass_confirm',$lang);?>
                                        </span>
                                    </div>
                                </div>
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button id="change_email_submit" type="reset" class="btn btn-danger m-btn m-btn--air m-btn--custom">
                                                <?= \app\helpers\TranslationHelper::getTranslation('change_email',$lang);?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        3. <?= \app\helpers\TranslationHelper::getTranslation('change_pass',$lang);?>
                                    </h3>
                                </div>
                            </div>
                            <form id="change_password" class="m--margin-top-10" action="" method="POST">
                                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_pass',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('profile_your_pass',$lang);?>" name="password">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_new_pass',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input id="new_password" class="form-control m-input" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_new_pass',$lang);?>" name="new_password">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('field_confirm_new_pass',$lang);?>
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_confirm_new_pass',$lang);?>" name="confirm_password">
                                        <span class="m-form__help">
                                            <?= \app\helpers\TranslationHelper::getTranslation('profile_enter_pass_again',$lang);?>
                                        </span>
                                    </div>
                                </div>
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button id="change_password_submit" type="reset" class="btn btn-danger m-btn m-btn--air m-btn--custom">
                                                <?= \app\helpers\TranslationHelper::getTranslation('change_pass',$lang);?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane<?= $tab == 2 ? ' active':''; ?>" id="m_user_profile_tab_2">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label id="pp_text" for="example-text-input" class="col-2 col-form-label">
                                    <?= \app\helpers\TranslationHelper::getTranslation('profile_picture_upload',$lang);?>
                                </label>
                                <div class="col-10">
                                    <div class="m-dropzone dropzone" id="m-dropzone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                <?= \app\helpers\TranslationHelper::getTranslation('profile_picture_upload_title',$lang);?>
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                <?= \app\helpers\TranslationHelper::getTranslation('profile_picture_upload_desc',$lang);?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="reset" id="change_picture_submit" class="btn btn-primary m-btn m-btn--air m-btn--custom" disabled="disabled" style="cursor: not-allowed;">
                                            <?= \app\helpers\TranslationHelper::getTranslation('save_picture',$lang);?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="tab-pane<?= $tab == 3 ? ' active':''; ?>" id="m_user_profile_tab_3">
                    <form class="m-form m-form--label-align-right" 
                        action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/profile/save-notifications?id=' . Yii::$app->user->id ?>" method="post">

                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                        <div class="m-portlet__body">
                            <?php $ind = 0; ?>
                            <?php foreach(NotificationActions::ACTIONS as $action => $action_title): ?>
                                <div class="m-form__section<?= ($ind == 0) ? ' m-form__section--first': ($ind == (count(NotificationActions::ACTIONS) -1) )? ' m-form__section--last':'' ;?>">
                                    <?php $ind++; ?>
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">
                                            <?= NotificationActions::ACTIONS[$action]; ?>:
                                        </h3>
                                    </div>
                                    
                                    <?php if ($action == NotificationActions::CONTACT_TAG_ADD): ?>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                <?= app\helpers\TranslationHelper::getTranslation('profile_add_tag_lable', $lang, 'Select tags') ?>:
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control m-select2 tags-select" id="<?= $action;?>-modal-tags-search-select-req" multiple name="<?= $action?>[tags][]">
                                                   
                                                    <?php if (isset($data['notifications'][$action]) && isset($data['notifications'][$action]['tags'])): ?>
                                                        <?php foreach ($data['notifications'][$action]['tags'] as $tag): ?>
                                                            <option value="<?= $tag['tag_id'] ?>" selected="selected">
                                                                <?= $tag['name']; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($action == NotificationActions::REQUEST_CHANGE_STATUS): ?>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                <?= app\helpers\TranslationHelper::getTranslation('profile_add_status_lable', $lang, 'Select statuses') ?>:
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control m-select2 status-select" id="<?= $action;?>-modal-statuses-search-select-req" multiple name="<?= $action?>[statuses][]">
                                                   
                                                    <?php if (isset($data['notifications'][$action]) && isset($data['notifications'][$action]['statuses'])): ?>
                                                        <?php foreach ($data['notifications'][$action]['statuses'] as $status): ?>
                                                            <option value="<?= $status['status_id'] ?>" selected="selected">
                                                                <?= $status['name']; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label">
                                            <?= app\helpers\TranslationHelper::getTranslation('profile_tracking_level_lable', $lang, 'Tracking level') ?>:
                                        </label>
                                        <div class="col-lg-6">
                                            <?php $curent_level = isset($data['notifications'][$action]['level']) ? $data['notifications'][$action]['level'] : 0; ?>
                                            <select name="<?=$action?>[level]" class="form-control m-bootstrap-select m_selectpicker">
                                                <?php foreach(ProfileNotifications::LEVELS as $level => $title): ?>
                                                    <option value="<?= $level; ?>"<?= ($curent_level == $level) ? ' selected': ''; ?>>
                                                        <?= $title; ?>
                                                    </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="m-form__group form-group row">
                                        <label class="col-lg-3 col-form-label">
                                            <?= app\helpers\TranslationHelper::getTranslation('profile_channel_lable', $lang, 'Channel') ?>:
                                        </label>
                                        <div class="col-lg-6">
                                            <div class="m-checkbox-list">
                                                <label class="m-checkbox">
                                                    <input name="<?=$action?>[is_email]" type="checkbox" value="1"<?= (isset($data['notifications'][$action]['is_email']) && $data['notifications'][$action]['is_email']) ? ' checked' : '';?>>
                                                    Email
                                                    <span></span>
                                                </label>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="<?=$action?>[is_notice]" value="1"<?= (isset($data['notifications'][$action]['is_notice']) && $data['notifications'][$action]['is_notice']) ? ' checked' : '';?>>
                                                    <?= app\helpers\TranslationHelper::getTranslation('profile_channel_notice_label', $lang, 'Notice') ?>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <?php endforeach; ?>

                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section--last">
                                <div class="m-form__group form-group row">
                                    <div class="col-12 text-right">
                                        <button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">
                                            <?= app\helpers\TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>