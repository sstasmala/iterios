<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('users_page_title', $lang, 'Users');
$this->registerJsFile('@web/js/users/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/users/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/user/_m_modal_create_user');
echo $this->render('/partials/modals/user/_m_modal_edit_user');
echo $this->render('/partials/modals/user/_m_modal_delete_user');
$this->endBlock();
?>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('users_page_title', $lang, 'Users') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#create_user_popup">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('add_user_button_name', $lang, 'Add user') ?>
                            </span>
                        </span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable" id="users_data_table"></div>
    </div>
</div>