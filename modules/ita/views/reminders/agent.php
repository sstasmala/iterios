<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<?php
$this->title = TranslationHelper::getTranslation('reminders_for_agent_page_title', $lang, 'Agent reminders');
$this->registerJsFile('@web/js/reminders/reminders-agent.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/reminders/reminders.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/reminders/_m_modal_agent_reminder_edit');
echo $this->render('/partials/modals/reminders/_m_modal_agent_reminder_read');
$this->endBlock();
?>

<?php
    /* THIS IS THE TEST ARRAY. REPLACE IT ON REAL DATA */
    $notification_test_data = [
        [
            'id' => 1,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 2,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 3,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 4,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 5,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 6,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 7,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 8,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 9,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 10,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 11,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 12,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 13,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 14,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 15,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 16,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 17,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 18,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => false
            ]
        ],
        [
            'id' => 19,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => true,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 20,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => true,
            'channels' => [
                'email' => [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ],
                'notice' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice'),
                    'enabled' => false,
                ],
                'task' => [
                    'name' => TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task'),
                    'enabled' => true,
                ]
            ]
        ]
    ];
    /* Get columns names */
    $column_names = [];
    
    foreach ($notification_test_data as $row) {
        foreach ($row['channels'] as $channel){
            $column_names[] = $channel['name'];
        }
    }
    $column_names = array_unique($column_names);
?>

<div id="reminders-index-portlet" class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('reminders_for_agent_page_title', $lang, 'Agent reminders') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row reminders-index-portlet-content active" data-content-part-all="true" data-content-part-system="true" data-content-part-my="true">
            <div class="col-12 table-wraper">
                <table class="table m-table m-table--head-bg-success mb-0">
                    <thead>
                        <tr>
                            <th>
                                <?= TranslationHelper::getTranslation('reminders_table_notification_column_title', $lang, 'Notification name') ?>
                            </th>
                            <?php foreach ($column_names as $col_name){ ?>
                                <th class="text-center">
                                    <?= $col_name ?>
                                </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($notification_test_data as $notification){ ?>
                            <tr>
                                <th scope="row">
                                    <?php if($notification['is_system']){ ?>
                                        <a href="#" class="m-link" data-toggle="modal" data-target="#edit_agent_reminder_readonly_popup" data-reminder-id="<?= $notification['id'] ?>">
                                            <?= $notification['name'] ?>
                                        </a>
                                        <i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-tooltip" data-placement="top" title="<?= TranslationHelper::getTranslation('system_reminder_tooltip', $lang, 'System reminder, edit disabled') ?>"></i>
                                    <?php } else { ?>
                                        <a href="#" class="m-link" data-toggle="modal" data-target="#edit_agent_reminder_popup" data-reminder-id="<?= $notification['id'] ?>">
                                            <?= $notification['name'] ?>
                                        </a>
                                    <?php } ?>
                                </th>
                                <?php foreach ($notification['channels'] as $channel_name => $channel) { ?>
                                    <?php if($channel){ ?>
                                        <td>
                                            <div class="row">
                                                <div class="col-12">
                                                    <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <?= Html::input(
                                                                'checkbox',
                                                                'name',
                                                                '',
                                                                [
                                                                    'checked' => $channel['enabled'],
                                                                    'class' => 'status_switcher'
                                                                ]) ?>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                    <?php } else { ?>
                                        <td>
                                            <?= TranslationHelper::getTranslation('reminders_table_no_'.$channel_name.'_channel_message', $lang, 'No channel') ?>
                                        </td>
                                    <?php } ?>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>