<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<?php
$this->title = TranslationHelper::getTranslation('reminders_for_tourist_page_title', $lang, 'Tourist reminders');
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/reminders/reminders-tourist.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/reminders/reminders.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/reminders/_m_modal_tourist_reminder_edit');
echo $this->render('/partials/modals/reminders/_m_modal_tourist_reminder_read');
$this->endBlock();
?>

<?php
    /* THIS IS THE TEST ARRAY. REPLACE IT ON REAL DATA */
    $notification_test_data = [
        [
            'id' => 1,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 2,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 3,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 4,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 5,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 6,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 7,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 8,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 9,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 10,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 11,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 12,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 13,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 14,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 15,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 16,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 17,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 18,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 19,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 20,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 21,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 22,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 23,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 24,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 25,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 26,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 27,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 28,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
        [
            'id' => 29,
            'name' => TranslationHelper::getTranslation('reminders_tourist_birthday_row_title', $lang, 'Tourist birthday'),
            'is_system' => true,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => true,
                ]
            ]
        ],
        [
            'id' => 30,
            'name' => TranslationHelper::getTranslation('reminders_new_year_and_christmas_row_title', $lang, 'New Year and Christmas'),
            'is_system' => false,
            'channels' => [
                [
                    'name' => TranslationHelper::getTranslation('email_title', $lang, 'Email'),
                    'enabled' => false,
                ]
            ]
        ],
    ];
    /* Get columns names */
    $column_names = [];

    foreach ($notification_test_data as $row) {
        foreach ($row['channels'] as $channel) {
            $column_names[] = $channel['name'];
        }
    }

    $column_names = array_unique($column_names);
?>

<div id="reminders-index-portlet" class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('reminders_for_tourist_page_title', $lang, 'Tourist reminders') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <div id="reminders-filter" class="btn-group m-btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-brand" data-filter="all"><?= TranslationHelper::getTranslation('reminders_filter_title_all', $lang, 'All') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="system"><?= TranslationHelper::getTranslation('reminders_filter_title_system', $lang, 'System') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="my"><?= TranslationHelper::getTranslation('reminders_filter_title_my', $lang, 'My') ?></button>
                    </div>
                </li>
                <li class="m-portlet__nav-item">
                    <button class="btn btn-success m-btn m-btn--icon">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('reminders_add_new_reminders_button', $lang, 'Add reminder') ?>
                            </span>
                        </span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row reminders-index-portlet-content active" data-content-part-all="true" data-content-part-system="true" data-content-part-my="true">
            <div class="col-12 table-wraper">
                <table class="table m-table m-table--head-bg-success mb-0">
                    <thead>
                        <tr>
                            <th>
                                <?= TranslationHelper::getTranslation('reminders_table_notification_column_title', $lang, 'Notification name') ?>
                            </th>
                            <?php foreach ($column_names as $col_name){ ?>
                                <th class="text-center">
                                    <?= $col_name ?>
                                </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($notification_test_data as $notification){ ?>
                            <tr>
                                <th scope="row">
                                    <?php if($notification['is_system']){ ?>
                                        <a href="#" class="m-link" data-toggle="modal" data-target="#edit_tourist_reminder_readonly_popup" data-reminder-id="<?= $notification['id'] ?>">
                                            <?= $notification['name'] ?>
                                        </a>
                                        <i class="la la-exclamation-circle system-status-icon" data-container="body" data-toggle="m-tooltip" data-placement="top" title="<?= TranslationHelper::getTranslation('system_reminder_tooltip', $lang, 'System reminder, edit disabled') ?>"></i>
                                    <?php } else { ?>
                                        <a href="#" class="m-link" data-toggle="modal" data-target="#edit_tourist_reminder_popup" data-reminder-id="<?= $notification['id'] ?>">
                                            <?= $notification['name'] ?>
                                        </a>
                                    <?php } ?>
                                </th>
                                <?php foreach ($notification['channels'] as $channel) { ?>
                                    <td>
                                        <div class="row">
                                            <div class="col-12">
                                                <span class="m-switch m-switch--icon">
                                                    <label>
                                                        <?= Html::input(
                                                            'checkbox',
                                                            'name',
                                                            '',
                                                            [
                                                                'checked' => $channel['enabled'],
                                                                'class' => 'status_switcher'
                                                            ]) ?>
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>