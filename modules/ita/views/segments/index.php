<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('segments_index_page_title', $lang, 'Segments');
$this->registerJsFile('@web/js/segments/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/segments/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>
<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/segments/_m_modal_create_segment');
echo $this->render('/partials/modals/segments/_m_modal_update_segment');
echo $this->render('/partials/modals/segments/_m_modal_contact_details');
$this->endBlock();
?>

<div id="segments_datatable_portlet" class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('segments_index_page_title', $lang, 'Segments') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="head_tools_left">
                <div id="bulk_segments_buttons" class="hidden">
                    <button id="remove-segment-btn" class="btn btn-sm btn-outline-info" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete'); ?>">
                        <i class="la la-trash"></i>
                        <?= TranslationHelper::getTranslation('delete', $lang, 'Delete'); ?>
                    </button>
                </div>
            </div>
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <div id="segments-filter" class="btn-group m-btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-brand" data-filter="all"><?= TranslationHelper::getTranslation('sidt_filter_title_all', $lang, 'All') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="system"><?= TranslationHelper::getTranslation('sidt_filter_title_system', $lang, 'System') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="my"><?= TranslationHelper::getTranslation('sidt_filter_title_my', $lang, 'My') ?></button>
                    </div>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#create_segment_modal">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('segments_index_add_segment_label', $lang, 'Add segment'); ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable" id="segments_datatable"></div>
    </div>
</div>