<?php
/**
 * blocked.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

use app\helpers\TranslationHelper;

$this->title = TranslationHelper::getTranslation('user_blocked_tenant', \Yii::$app->user->identity->tenant->language->iso);

?>

<div class="alert alert-danger" role="alert">
    <h2><?= TranslationHelper::getTranslation('user_blocked_oops', \Yii::$app->user->identity->tenant->language->iso) ?></h2>
    <p class="lead"><?= TranslationHelper::getTranslation('user_blocked_notification', \Yii::$app->user->identity->tenant->language->iso) ?></p>
</div>