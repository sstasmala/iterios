<?php
/**
 * added_user.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

use app\helpers\TranslationHelper;

$this->title = TranslationHelper::getTranslation('added_user', \Yii::$app->user->identity->tenant->language->iso);

?>

<div class="alert alert-success" role="alert">
    <h2><?= TranslationHelper::getTranslation('added_user', \Yii::$app->user->identity->tenant->language->iso) ?></h2>
    <p class="lead"><?= TranslationHelper::getTranslation('added_user_notification', \Yii::$app->user->identity->tenant->language->iso) ?> <strong><?= $tenant->name ?></strong>!
        <h3>
            <a class="btn m-btn--square  btn-secondary m-btn m-btn--custom" href="/change-tenant?id= <?= $tenant->id ?>"><?= TranslationHelper::getTranslation('added_user_button', \Yii::$app->user->identity->tenant->language->iso) ?> <?= $tenant->name ?>!</a>
        </h3>
    </p>
</div>