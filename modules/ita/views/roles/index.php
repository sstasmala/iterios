<?php

use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('roles_page_title', $lang, 'Roles');
$this->registerJsFile('@web/js/roles/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/roles/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('roles_page_title', $lang, 'Roles') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn btn-success m-btn m-btn--icon" href="<?= Url::toRoute('roles/create') ?>">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('add_role', $lang, 'Add role'); ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable" id="roles_data_table"></div>
    </div>
</div>