<?php

use app\helpers\TranslationHelper;
use yii\helpers\Url;
use yii\helpers\Html;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('create_roles_page_title', $lang, 'Create new role');
$this->registerCSSFile('@web/css/roles/create.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('create_roles_page_title', $lang, 'Create new role') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button type="submit" class="btn btn-success m-btn m-btn--icon" form="create_role_form">
                        <span>
                            <i class="fa fa-save"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('create', $lang, 'Create'); ?>
                            </span>
                        </span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form id="create_role_form" action="<?=Yii::$app->params['baseUrl'].'/ita/roles/create'?>" method="post" autocomplete="off">
            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group m-form__group">
                        <label for="role_create_name_input"><?= TranslationHelper::getTranslation('create_roles_page_name_input_label', $lang, 'Role name') ?></label>
                        <?=
                            Html::input(
                                    'text',
                                    'Role[name]',
                                    false,
                                    [
                                        'id' => 'role_create_name_input',
                                        'class' => 'form-control m-input',
                                        'placeholder' => TranslationHelper::getTranslation('create_roles_page_name_input_placeholder', $lang, 'Enter role name'),
                                        'required' => 'required'
                                    ])
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-2">
                    <h5><?= TranslationHelper::getTranslation('create_roles_page_access_rights_title', $lang, 'Access rights') ?></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php $role_permissions = \app\helpers\TenantHelper::getPermissionsScheme($lang)?>
                    <?php /* Render tables */ ?>
                    <?php foreach ($role_permissions as $table_name => $table_conf) { ?>
                        <div class="table-responsive">
                            <table class="table m-table m-table--head-separator-success iterios-customize">
                                <thead>
                                    <tr>
                                        <th><?= $table_name ?></th>
                                        <th><?= TranslationHelper::getTranslation('create_roles_table_col_title_reading', $lang, 'Reading') ?></th>
                                        <th><?= TranslationHelper::getTranslation('create_roles_table_col_title_creating', $lang, 'Creating') ?></th>
                                        <th><?= TranslationHelper::getTranslation('create_roles_table_col_title_updating', $lang, 'Updating') ?></th>
                                        <th><?= TranslationHelper::getTranslation('create_roles_table_col_title_deleting', $lang, 'Deleting') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php foreach ($table_conf as $permission_title => $permission_conf) { ?>
                                        <tr>
                                            <td class="permission_title_cell">
                                                <?php $has_modules_description = (isset($permission_conf['modules']) && !empty($permission_conf['modules'])) ? true : false; ?>
                                                <span class="permission_name <?= ($has_modules_description) ? ' has_modules_description' : '' ?>">
                                                    <?= $permission_title ?>
                                                </span>
                                                <?php if($has_modules_description){ ?>
                                                    <span class="permission_modules_list">
                                                        <?php foreach ($permission_conf['modules'] as $key => $module){ ?>
                                                            <span class="module-text" title="<?= $module ?>">
                                                                <?= $module ?><?= (($key+1) < count($permission_conf['modules'])) ? ', ' : '' ?>
                                                            </span>
                                                        <?php } ?>
                                                    </span>
                                                <?php } ?>
                                            </td>
                                            <?= $this->render('_table-cell', [
                                                'permission_key' => 'read',
                                                'permission_conf' => $permission_conf
                                            ]); ?>
                                            <?= $this->render('_table-cell', [
                                                'permission_key' => 'create',
                                                'permission_conf' => $permission_conf
                                            ]); ?>
                                            <?= $this->render('_table-cell', [
                                                'permission_key' => 'update',
                                                'permission_conf' => $permission_conf
                                            ]); ?>
                                            <?= $this->render('_table-cell', [
                                                'permission_key' => 'delete',
                                                'permission_conf' => $permission_conf
                                            ]); ?>
                                        </tr>
                                   <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <div class="m-portlet__foot">
        <div class="row align-items-center">
            <div class="col-12 m--align-right">
                <button type="submit" class="btn btn-success m-btn m-btn--icon" form="create_role_form">
                    <span>
                        <i class="fa fa-save"></i>
                        <span>
                            <?= TranslationHelper::getTranslation('create', $lang, 'Create'); ?>
                        </span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>