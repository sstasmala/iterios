<?php
    use app\helpers\TranslationHelper;
    
    $lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<?php if (!empty($permission_conf[$permission_key])) { ?>
    <td>
        <?php foreach ($permission_conf[$permission_key] as $permission_name => $permission) { ?>
            <label class="m-radio m-radio--bold<?= (isset($permission['color_scheme']) && !empty($permission['color_scheme'])) ? (' m-radio--state-' . $permission['color_scheme']) : '' ?>">
                <input type="radio" name="Role[permissions][<?= $permission_conf['permission_name'] ?>][<?= $permission_key ?>]" value="<?= $permission['value'] ?>" <?= (isset($permission['checked']) && $permission['checked']) ? 'checked' : '' ?>>
                <?= $permission['title'] ?>
                <span></span>
            </label>
        <?php } ?>
    </td>
<?php } else { ?>
    <td class="not-available-cell">
        <div class="not-available-message">
            <span>
                (<?= TranslationHelper::getTranslation('create_roles_table_empty_cell_message', $lang, 'Not available') ?>)
            </span>
        </div>
    </td>
<?php } ?>