<?php
/**
 * @var $this \yii\web\View
 */
/* Generate title */

$this->title = ($contact['first_name']) ? $contact['first_name'] : '';
$this->title .= ($contact['middle_name']) ? ' ' . $contact['middle_name'] : '';
$this->title .= ($contact['last_name']) ? ' ' . $contact['last_name'] : '';
$this->title = trim($this->title);

/* Files */
$this->registerCssFile('@web/css/contacts/view.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/contacts/dropzone.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/contacts/view.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/contact/_m_modal_edit_contact', ['contact' => $contact, 'contacts_types' => $contacts_types]);
echo $this->render('/partials/modals/contact/_m_modal_contact_add_passport', ['contact' => $contact, 'contacts_types' => $contacts_types]);
echo $this->render('/partials/modals/contact/_m_modal_contact_add_visa', ['contact' => $contact, 'contacts_types' => $contacts_types]);
echo $this->render('/partials/modals/contact/_m_modal_contact_edit_note', ['contact' => $contact]);
echo $this->render('/partials/modals/contact/_m_modal_add_file', ['contact' => $contact]);
echo $this->render('/partials/modals/contact/_m_modal_file_upload_view', ['contact' => $contact]);
$this->endBlock();

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="row" id="contact-detail-view">
    <div class="col-xxl-3 col-xl-4 col-lg-12">
        <div class="row">
            <div class="col-xl-12 col-lg-5 col-md-5 col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_contact-preview', [
                            'contact' => $contact,
                            'contacts_types' => $contacts_types,
                            'lang' => $lang
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_contact-files', [
                            'contact' => $contact,
                            'lang' => $lang
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $this->render('partlets/_contact-info', [
                            'contact' => $contact,
                            'lang' => $lang
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-7 col-md-7 col-sm-6">
                <div class="row">
                    <div class="col-12">
                        <?= $this->render('partlets/_contact-statistic-block', [
                            'contact' => $contact,
                            'lang' => $lang
                        ]) ?>
                    </div>
                    <div class="col-12">
                        <?= $this->render('partlets/_contact-settings', [
                            'contact' => $contact,
                            'lang' => $lang
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-9 col-xl-8 col-lg-12">
        <div class="row">
            <div class="col-12">
                <?= $this->render('partlets/_contact-history_and_notes', [
                    'contact' => $contact,
                    'lang' => $lang,
                    'timeline' => $timeline,
                    'all_task_types' => $all_task_types
                ]) ?>
            </div>
            <div class="col-xxl-6">
                <?= $this->render('partlets/_contact-passports', [
                    'contact' => $contact,
                    'lang' => $lang
                ]) ?>
            </div>
            <div class="col-xxl-6">
                <?= $this->render('partlets/_contact-visas', [
                    'contact' => $contact,
                    'contacts_types' => $contacts_types,
                    'lang' => $lang
                ]) ?>
            </div>
            <div class="col-xxl-6">
                <?= $this->render('partlets/_contact-relations', [
                    'lang' => $lang
                ]) ?>
            </div>
            <div class="col-xxl-6">
                <?= $this->render('partlets/_travels-history', [
                    'lang' => $lang
                ]) ?>
            </div>
            <div class="col-xxl-6">
                <?= $this->render('partlets/_manual-contacts-merge', [
                    'contact' => $contact,
                    'lang' => $lang
                ]) ?>
            </div>
        </div>
    </div>
</div>