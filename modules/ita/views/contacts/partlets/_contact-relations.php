<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet contact-relations">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-users"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_relations_portlet_title', $lang, 'Contact relations') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <form action="#" id="add_contact_relation_form" method="POST" class="mb-4">
            <div class="row align-items-end">
                <div class="col-sm-5 col-xxl-4 mb-3 mb-sm-0">
                    <label><?= TranslationHelper::getTranslation('contact_relation_type_select_label', $lang, 'Relation type') ?>:</label>
                    <select name="Contact[retation][type]" id="relation_contact_type_select" title="<?= TranslationHelper::getTranslation('contact_relation_type_select_placeholder', $lang, 'Select relation type') ?>" required class="form-control m-bootstrap-select m_selectpicker">
                        <option>
                            Друг
                        </option>
                        <option>
                            Семья
                        </option>
                        <option>
                            Другое
                        </option>
                    </select>
                </div>
                <div class="col-sm-5 col-xxl-5 mb-3 mb-sm-0">
                    <label><?= TranslationHelper::getTranslation('contact_relation_contact_select_label', $lang, 'Contact') ?>:</label>
                    <select name="Contact[retation][contact_id]" id="relation_contact_search_select" class="form-control m-bootstrap-select"></select>
                </div>
                <div class="col-sm-2 col-xxl-3 text-center text-sm-right">
                    <button type="submit" id="add_contact_relation_submit" class="btn btn-success disabled" disabled>
                        <?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                    </button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <table class="table m-table m-table--head-bg-success">
                    <thead>
                        <tr>
                            <th><?= TranslationHelper::getTranslation('contact_relation_type_select_label', $lang, 'Relation type') ?></th>
                            <th><?= TranslationHelper::getTranslation('contact_relation_contact_select_label', $lang, 'Contact') ?></th>
                            <th><?= TranslationHelper::getTranslation('contact_actions', $lang, 'Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Друг
                            </td>
                            <td>
                                Семен Петрович
                            </td>
                            <td width="50" class="text-center">
                                <button class="remove-contact delete-contact m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                                    <i class="la la-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Семья
                            </td>
                            <td>
                                Изольда Филиповна
                            </td>
                            <td width="50" class="text-center">
                                <button class="remove-contact delete-contact m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                                    <i class="la la-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Другое
                            </td>
                            <td>
                                Константин Романович
                            </td>
                            <td width="50" class="text-center">
                                <button class="remove-contact delete-contact m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                                    <i class="la la-trash"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>