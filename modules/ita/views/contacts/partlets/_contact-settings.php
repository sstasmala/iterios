<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;
?>
<div class="m-portlet m-portlet--responsive-mobile contact-settings">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-cogwheel-2"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact-settings-partlet-title', $lang, 'Settings') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="select-responsible-block">
            <p>
                <?= TranslationHelper::getTranslation('contact-settings-partlet-responsible_label', $lang, 'Responsible') ?>
            </p>
            <input type="hidden" id="select2-responsible-ajax-search-contact-id-hint" value="<?= $contact['id'] ?>">
            <select class="form-control m-bootstrap-select" id="select2-responsible-ajax-search" name="contact_responsible">
                <?php if(!empty($contact['responsible'])){
                    $full_name = '';
                    $full_name .= ($contact['responsible']['first_name']) ? $contact['responsible']['first_name'] . ' ' : '';
                    $full_name .= ($contact['responsible']['middle_name']) ? $contact['responsible']['middle_name'] . ' ' : '';
                    $full_name .= ($contact['responsible']['last_name']) ? $contact['responsible']['last_name'] . ' ' : '';
                    $full_name = trim($full_name);
                    echo '<option value="' . $contact['responsible']['id'] . '">' . $full_name . '</option>';
                } else {
                    echo '<option value="" selected>' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</option>';
                } ?>
            </select>
        </div>
        <hr>
        <div class="contact-refresh-dates-block">
            <table>
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('contacts_field_created', $lang, 'Created') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $contact['created_at']?></span>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('contacts_field_updated', $lang, 'Updated') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $contact['updated_at']?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="contact-delete-block">
            <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/delete?id=' . $contact['id'] ?>" id="contact-view-delete-form" method="POST">
                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                <button type="submit" class="btn btn-danger m-btn m-btn--custom m-btn--icon">
                    <span>
                        <i class="fa fa-trash"></i>
                        <span><?= TranslationHelper::getTranslation('contacts_delete', $lang, 'Delete') ?></span>
                    </span>
                </button>
            </form>
        </div>
    </div>
</div>