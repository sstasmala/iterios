<?php
use app\helpers\TranslationHelper;
use yii\helpers\Url;
?>
<div class="m-portlet m-portlet--mobile contact-statistic-block">
    <div class="m-widget1 customized">
        <div class="m-widget1__item">
            <div class="col">
                <h3 class="m-widget1__title">
                    <?= TranslationHelper::getTranslation('contact_static_block_request', $lang, 'Requests') ?>
                </h3>
                <span class="m-widget1__desc">
                    <?= TranslationHelper::getTranslation('contact_static_block_your_requests', $lang, 'Requests') ?>
                </span>
            </div>
            <div class="col text-center">
                <span class="m-widget1__number m--font-danger">
                    17
                </span>
                <br>
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/requests/index') ?>" class="m-link">
                    <?= TranslationHelper::getTranslation('contacts_view_action', $lang, 'View') ?>
                </a>
            </div>
        </div>
        <div class="m-widget1__item">
            <div class="col">
                <h3 class="m-widget1__title">
                    <?= TranslationHelper::getTranslation('contact_static_block_commercial_offers', $lang, 'Compilation') ?>
                </h3>
                <span class="m-widget1__desc">
                    <?= TranslationHelper::getTranslation('contact_static_block_your_commercial_offers', $lang, 'Total prices added') ?>
                </span>
            </div>
            <div class="col text-center">
                <span class="m-widget1__number m--font-info">
                    8
                </span>
            </div>
        </div>
        <div class="m-widget1__item">
            <div class="col">
                <h3 class="m-widget1__title">
                    <?= TranslationHelper::getTranslation('contact_static_block_deals', $lang, 'Commercial') ?>
                </h3>
                <span class="m-widget1__desc">
                    <?= TranslationHelper::getTranslation('contact_static_block_your_deals', $lang, 'Total sent offers') ?>
                </span>
            </div>
            <div class="col text-center">
                <span class="m-widget1__number m--font-success">
                    3
                </span>
            </div>
        </div>
        <div class="m-widget1__item">
            <div class="col">
                <h3 class="m-widget1__title">
                    <?= TranslationHelper::getTranslation('contact_static_block_reservations', $lang, 'Orders') ?>
                </h3>
                <span class="m-widget1__desc">
                    <?= TranslationHelper::getTranslation('contact_static_block_your_reservations', $lang, 'Total orders') ?>
                </span>
            </div>
            <div class="col text-center">
                <span class="m-widget1__number m--font-brand">
                    12
                </span>
                <br>
                <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/orders/index') ?>" class="m-link">
                    <?= TranslationHelper::getTranslation('contacts_view_action', $lang, 'View') ?>
                </a>
            </div>
        </div>
        <div class="m-widget25">
            <span class="m-widget25__price m--font-brand">$237,650</span><br>
            <span class="m-widget25__desc">Total Revenue This Month</span>
        </div>
    </div>
</div>