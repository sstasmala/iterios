<?php

use app\helpers\MCHelper;
use yii\helpers\Html;
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile contact-info">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-user"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_contact_info', $lang, 'Contact info') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="contact-info-table-block">
            <table class="table table-striped m-table">
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('birthday', $lang, 'Birthday') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;" data-date="<?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']))) ? \app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']) : \app\helpers\DateTimeHelper::convertTimestampToDate('') ?>">
                                    <?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']))) ? \app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']) : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-date_of_birth-label', $lang, 'Date') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="date_of_birth">
                                                            <div class="input-group pull-right">
                                                                <input type="text" name="value" id="contact-info-table-date_of_birth" value="<?= \app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']) ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-date_of_birth_placeholder', $lang, 'Set new date...') ?>" class="form-control m-input"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1" id="submit-date-contact"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-sex-label', $lang, 'Sex') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">
                                    <?php
                                    if (isset($contact['sex'])) {
                                        switch ($contact['sex']) {
                                            case 0:
                                                echo TranslationHelper::getTranslation('female_sex',$lang);
                                                break;
                                            case 1:
                                                echo TranslationHelper::getTranslation('male_sex',$lang);
                                                break;
                                        }
                                    } else {
                                        echo '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                                    }
                                    ?>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-sex-label', $lang, 'Sex') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="sex">
                                                            <select name="value" class="form-control m-bootstrap-select m_selectpicker" required>
                                                                <option value="1"<?= $contact['sex'] === 1 ? ' selected' : '' ?>><?=TranslationHelper::getTranslation('male_sex',$lang)?></option>
                                                                <option value="0"<?= $contact['sex'] === 0 ? ' selected' : '' ?>><?=TranslationHelper::getTranslation('female_sex',$lang)?></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-discount-label', $lang, 'Discount') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($contact['discount'])) ? $contact['discount'].'%' : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-discount-label', $lang, 'Discount') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="discount">
                                                            <div class="input-group m-input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">%</span>
                                                                </div>
                                                                <input type="number" name="value" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-set_new_value_placeholder', $lang, 'Set new value...') ?>" value="<?= (!empty($contact['discount'])) ? $contact['discount'] : '' ?>" min="1" max="100" aria-describedby="basic-addon1">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-nationality-label', $lang, 'Nationality') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($contact['nationality'])) ? MCHelper::getCountryById($contact['nationality'],$lang)[0]['title'] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-nationality-label', $lang, 'Nationality') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="nationality">
                                                            <select class="form-control m-bootstrap-select country-select-ajax" name="value">
                                                                <option value="<?= $contact['nationality'] ?>" selected="selected"><?=(!empty($contact['nationality']) ? MCHelper::getCountryById($contact['nationality'],$lang)[0]['title'] : '')?></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('contacts_field_title_type', $lang, 'Type') ?></td>
                        <td>
                            <?php
                                if((!empty($contact['type']))){
                                    $color_class = 'brand';
                                    $tooltip_text = '';
                                    $type = '';
                                    switch (strtolower ($contact['type'])){
                                        case 'customer':
                                            $color_class = 'primary';
                                            $tooltip_text = TranslationHelper::getTranslation('contacts_customer_type_tooltip', $lang);
                                            $type = TranslationHelper::getTranslation('contacts_types_customer', $lang);
                                            break;
                                        case 'tourist':
                                            $color_class = 'success';
                                            $tooltip_text = TranslationHelper::getTranslation('contacts_tourist_type_tooltip', $lang);
                                            $type = TranslationHelper::getTranslation('contacts_types_tourist', $lang);
                                            break;
                                        case 'lid':
                                            $color_class = 'warning';
                                            $tooltip_text = TranslationHelper::getTranslation('contacts_lid_type_tooltip', $lang);
                                            $type = TranslationHelper::getTranslation('contacts_types_lid', $lang);
                                            break;
                                        case 'new':
                                            $color_class = 'danger';
                                            $tooltip_text = TranslationHelper::getTranslation('contacts_new_type_tooltip', $lang);
                                            $type = TranslationHelper::getTranslation('contacts_types_new', $lang);
                                            break;
                                    }
                                    echo '<span class="m-badge m-badge--' . $color_class . ' m-badge--wide m-badge--rounded" data-toggle="m-tooltip" data-original-title="' . $tooltip_text . '">' . $type . '</span>';
                                }else{
                                    echo '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('contacts_field_company', $lang, 'Company') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($contact['company']['name'])) ? $contact['company']['name'] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <?= (!empty($contact['company']['name']) ? '<a id="company_link" href="'.Yii::$app->params['baseUrl'].'/ita/'.Yii::$app->user->identity->tenant->id.'/companies/view?id='.$contact['company']['id'].'"> <i class="la la-external-link"></i></a>' : '') ?>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-company-label', $lang, 'Company') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="company">
                                                            <select id="contact-info-table-company_select"  class="form-control m-bootstrap-select" name="value" data-live-search="true">
                                                                <option value="<?= $contact['company']['id'] ?>"><?= $contact['company']['name'] ?></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-living_address-label', $lang, 'Living address') ?></td>
                        <td>
                            <?php 
                            $arderss_row = '';
                            if (!empty($contact['living_address'])) {
                                if (isset($contact['living_address']['country']) && !empty($contact['living_address']['country'])) {
                                    $arderss_row .= MCHelper::getCountryById($contact['living_address']['country'],$lang)[0]['title']. ', ';
                                }
                                if (isset($contact['living_address']['region']) && !empty($contact['living_address']['region'])) {
                                    $arderss_row .= $contact['living_address']['region'] . ', ';
                                }
                                if (isset($contact['living_address']['city']) && !empty($contact['living_address']['city'])) {
                                    $arderss_row .= $contact['living_address']['city'] . ', ';
                                }
                                if (isset($contact['living_address']['street']) && !empty($contact['living_address']['street'])) {
                                    $arderss_row .= $contact['living_address']['street'];
                                    if (isset($contact['living_address']['house']) && !empty($contact['living_address']['house'])) {
                                        $arderss_row .= ' ' . $contact['living_address']['house'];
                                        if (isset($contact['living_address']['flat']) && !empty($contact['living_address']['flat'])) {
                                            $arderss_row .= ', ' . $contact['living_address']['flat'];
                                        }
                                    }
                                    $arderss_row .= ', ';
                                }
                                if (isset($contact['living_address']['postcode']) && !empty($contact['living_address']['postcode'])) {
                                    $arderss_row .= $contact['living_address']['postcode'] . ', ';
                                }
                                $arderss_row = substr(trim($arderss_row), 0, -1);
                            } else {
                                $arderss_row = '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                            }
                            ?>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= $arderss_row ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labecompany_select_inputls">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-living_address-label', $lang, 'Living address') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="living_address">
                                                            <select class="form-control m-bootstrap-select country-select-ajax" name="living_address[country]">
                                                                <?php if(!is_null($contact['living_address'])):?>
                                                                <option value="<?= $contact['living_address']['country'] ?>" selected="selected"><?= MCHelper::getCountryById($contact['living_address']['country'],$lang)[0]['title']?></option>
                                                                <?php endif;?>
                                                            </select>
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[region]" type="text" class="form-control" value="<?= (!empty($contact['living_address']['region'])) ? $contact['living_address']['region'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-region', $lang, 'Region') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[city]" type="text" class="form-control" value="<?= (!empty($contact['living_address']['city'])) ? $contact['living_address']['city'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-city', $lang, 'City') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[street]" type="text" class="form-control" value="<?= (!empty($contact['living_address']['street'])) ? $contact['living_address']['street'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-street', $lang, 'Street') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[house]" type="number" class="form-control m-input" value="<?= (!empty($contact['living_address']['house'])) ? $contact['living_address']['house'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-house', $lang, 'House') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[flat]" type="number" class="form-control m-input" value="<?= (!empty($contact['living_address']['flat'])) ? $contact['living_address']['flat'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-flat', $lang, 'Flat') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="living_address[postcode]" type="number" class="form-control m-input" value="<?= (!empty($contact['living_address']['postcode'])) ? $contact['living_address']['postcode'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-postcode', $lang, 'Postcode') ?>">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-residence_address-label', $lang, 'Residence address') ?></td>
                        <td>
                            <?php 
                            $arderss_row = '';
                            if (!empty($contact['residence_address'])) {
                                if (isset($contact['residence_address']['country']) && !empty($contact['residence_address']['country'])) {
                                    $arderss_row .= MCHelper::getCountryById($contact['residence_address']['country'],$lang)[0]['title'] . ', ';
                                }
                                if (isset($contact['residence_address']['region']) && !empty($contact['residence_address']['region'])) {
                                    $arderss_row .= $contact['residence_address']['region'] . ', ';
                                }
                                if (isset($contact['residence_address']['city']) && !empty($contact['residence_address']['city'])) {
                                    $arderss_row .= $contact['residence_address']['city'] . ', ';
                                }
                                if (isset($contact['residence_address']['street']) && !empty($contact['residence_address']['street'])) {
                                    $arderss_row .= $contact['residence_address']['street'];
                                    if (isset($contact['residence_address']['house']) && !empty($contact['residence_address']['house'])) {
                                        $arderss_row .= ' ' . $contact['residence_address']['house'];
                                        if (isset($contact['residence_address']['flat']) && !empty($contact['residence_address']['flat'])) {
                                            $arderss_row .= '/' . $contact['residence_address']['flat'];
                                        }
                                    }
                                    $arderss_row .= ', ';
                                }
                                if (isset($contact['residence_address']['postcode']) && !empty($contact['residence_address']['postcode'])) {
                                    $arderss_row .= $contact['residence_address']['postcode'] . ', ';
                                }
                                $arderss_row = substr(trim($arderss_row), 0, -1);
                            } else {
                                $arderss_row = '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                            }
                            ?>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($arderss_row)) ? $arderss_row : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-residence_address-label', $lang, 'Residence address') ?></h6>
                                                            <a href="#" id="copy-living-address"><?=TranslationHelper::getTranslation('contact_btn_address_copy_to_residence', $lang, 'Copy address to residence')?></a>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="residence_address">
                                                            <select class="form-control m-bootstrap-select country-select-ajax" name="residence_address[country]">
                                                                <option value="<?= $contact['residence_address']['country'] ?>" selected="selected"><?= MCHelper::getCountryById($contact['residence_address']['country'],$lang)[0]['title'] ?></option>
                                                            </select>
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[region]" type="text" class="form-control" value="<?= (!empty($contact['residence_address']['region'])) ? $contact['residence_address']['region'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-region', $lang, 'Region') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[city]" type="text" class="form-control" value="<?= (!empty($contact['residence_address']['city'])) ? $contact['residence_address']['city'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-city', $lang, 'City') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[street]" type="text" class="form-control" value="<?= (!empty($contact['residence_address']['street'])) ? $contact['residence_address']['street'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-street', $lang, 'Street') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[house]" type="number" class="form-control m-input" value="<?= (!empty($contact['residence_address']['house'])) ? $contact['residence_address']['house'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-house', $lang, 'House') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[flat]" type="number" class="form-control m-input" value="<?= (!empty($contact['residence_address']['flat'])) ? $contact['residence_address']['flat'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-flat', $lang, 'Flat') ?>">
                                                            <div class="mb-2"></div>
                                                            <input name="residence_address[postcode]" type="number" class="form-control m-input" value="<?= (!empty($contact['residence_address']['postcode'])) ? $contact['residence_address']['postcode'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-addressplaceholder-postcode', $lang, 'Postcode') ?>">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('info-editable-table-tin-label', $lang, 'TIN') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($contact['tin'])) ? $contact['tin'] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>' ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-sm-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('info-editable-table-tin-label', $lang, 'TIN') ?></h6>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-8 mb-3 inputs">
                                                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/contacts/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $contact['id'] ?>">
                                                            <input name="property" type="hidden" value="tin">
                                                            <input name="value" type="number" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-set_new_value_placeholder', $lang, 'Set new value...') ?>" value="<?= (!empty($contact['tin'])) ? $contact['tin'] : '' ?>" class="form-control m-input">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 mb-3 buttons text-xs-left text-sm-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit mr-1"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
