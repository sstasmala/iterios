<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet contact-visas">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-paper-plane"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_visas', $lang, 'Visas') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#conctact-add-visa-popup">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php if(isset($contact['visas']) && (!empty($contact['visas']))){ ?>
            <div class="contact-visas-block">
                <table class="table m-table m-table--head-bg-success">
                    <thead>
                        <tr>
                            <th><?= TranslationHelper::getTranslation('contact_type', $lang, 'Type') ?></th>
                            <th><?= TranslationHelper::getTranslation('contact_country', $lang, 'Country') ?></th>
                            <th><?= TranslationHelper::getTranslation('modal_add_passport_limit_date', $lang) ?></th>
                            <th class="actions"><?= TranslationHelper::getTranslation('contact_actions', $lang, 'Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $now = \app\helpers\DateTimeHelper::createFromTimestamp(time());
                        ?>
                        <?php foreach ($contact['visas'] as $visa) { ?>
                            <?php
                                $visa_limit_date = \app\helpers\DateTimeHelper::createFromTimestamp($visa['end_date']);
                                $date_diff = date_diff($now, $visa_limit_date);
                                $extra_row_class = '';
                                if ($date_diff->invert === 1) {
                                    $extra_row_class = 'danger';
                                } elseif ($date_diff->days <= 30) {
                                    $extra_row_class = 'warning';
                                }
                            ?>
                        <tr class="<?= $extra_row_class ? $extra_row_class : '' ?> <?= (isset($visa['description']) && !empty($visa['description'])) ? ' has-desc' : '' ?>">
                                <th class="type">
                                    <?= (isset($visa['description']) && !empty($visa['description'])) ? ('<button data-toggle="m-popover" data-trigger="focus" data-placement="top" data-content="' . $visa['description'] . '">' . $contacts_types['visas'][$visa['type_id']]['name'] . '</button>') : '' ?>
                                    <?= (!isset($visa['description']) || empty($visa['description'])) ? '<span>' . $contacts_types['visas'][$visa['type_id']]['name'] . '</span>' : '' ?>
                                </th>
                                <td class="country">
                                    <?= \app\helpers\MCHelper::getCountryById($visa['country'],$lang)[0]['title'] ?>
                                </td>
                                <td class="date">
                                    <table>
                                        <tr class="begin">
                                            <td class="column-1"><?= \app\helpers\DateTimeHelper::convertTimestampToDate($visa['begin_date']) ?></td>
                                        </tr>
                                        <tr class="end">
                                            <td class="column-1">
                                                <?= \app\helpers\DateTimeHelper::convertTimestampToDate($visa['end_date']) ?>
                                                <?php
                                                    switch ($extra_row_class){
                                                        case 'danger':
                                                            echo '<i class="la la-exclamation-circle notification-icon" data-toggle="m-tooltip" title="' . TranslationHelper::getTranslation('notification_icon_visa_date_limit_danger', $lang, "Visa's validity expires!") . '"></i>';
                                                            break;
                                                        case 'warning':
                                                            echo '<i class="la la-exclamation-triangle notification-icon" data-toggle="m-tooltip" title="' . TranslationHelper::getTranslation('notification_icon_visa_date_limit_warning', $lang, "The validity of the visa expired!") . '"></i>';
                                                            break;
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="actions">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link btn btn-sm btn-light m-btn m-btn--outline-2x m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle">
                                            <i class="la la-plus m--hide"></i>
                                            <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__item">
                                                                <?php
                                                                    $visa_data = json_encode([
                                                                        'id' => $visa['id'],
                                                                        'type' => $visa['type_id'],
                                                                        'country_id' => $visa['country'],
                                                                        'country_name' => \app\helpers\MCHelper::getCountryById($visa['country'],$lang)[0]['title'],
                                                                        'begin_date' => \app\helpers\DateTimeHelper::convertTimestampToDate($visa['begin_date']),
                                                                        'end_date' => \app\helpers\DateTimeHelper::convertTimestampToDate($visa['end_date']),
                                                                        'description' => $visa['description']
                                                                    ]);
                                                                ?>
                                                                <a href="" class="m-nav__link" data-toggle="modal" data-target="#conctact-add-visa-popup" data-visa-data='<?= $visa_data ?>'>
                                                                    <form class="edit-visa_form">
                                                                        <button type="submit">
                                                                            <i class="m-nav__link-icon flaticon-edit-1"></i>
                                                                            <span class="m-nav__link-text">
                                                                                <?= TranslationHelper::getTranslation('edit', $lang, 'Edit') ?>
                                                                            </span>
                                                                        </button>
                                                                    </form>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <form class="delete-visa_form" action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/delete-visa?id=' . $contact['id'] ?>" method="GET">
                                                                        <input type="hidden" name="visa_id" value="<?= $visa['id'] ?>">
                                                                        <button type="submit">
                                                                            <i class="m-nav__link-icon flaticon-circle"></i>
                                                                            <span class="m-nav__link-text">
                                                                                <?= TranslationHelper::getTranslation('contacts_delete', $lang, 'Delete') ?>
                                                                            </span>
                                                                        </button>
                                                                    </form>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="row">
                <div class="col-12 no-visas">
                    <span><?= TranslationHelper::getTranslation('contact_no_visas', $lang, 'No visas') ?></span>
                </div>
            </div>
        <?php } ?>
        
    </div>
</div>