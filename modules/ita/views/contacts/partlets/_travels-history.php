<?php
use app\helpers\TranslationHelper;
?>

<div class="m-portlet contact-travels-history">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-analytics"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_travels_history_portlet_title', $lang, 'Travels history') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table m-table m-table--head-bg-success">
            <thead>
                <tr>
                    <th><?= TranslationHelper::getTranslation('contact_travels_history_col_date_title', $lang, 'Date') ?></th>
                    <th><?= TranslationHelper::getTranslation('contact_travels_history_col_country_title', $lang, 'Country') ?></th>
                    <th><?= TranslationHelper::getTranslation('contact_travels_history_col_value_title', $lang, 'Value') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        Май 2015
                    </td>
                    <td>
                        Турция
                    </td>
                    <td>
                        28000 грн
                    </td>
                </tr>
                <tr>
                    <td>
                        Июнь 2016
                    </td>
                    <td>
                        Египет
                    </td>
                    <td>
                        32000 грн
                    </td>
                </tr>
                <tr>
                    <td>
                        Август 2017
                    </td>
                    <td>
                        Тайланд
                    </td>
                    <td>
                        25000 грн
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>