<?php
/**
 * timeline_item.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 *
 */

?>

<div class="m-timeline-1__item m-timeline-1__item--right<?= ($is_first) ? ' m-timeline-1__item--first' : '' ?>" data-id="<?= isset($item['id']) ? $item['id'] : '' ?>">
    <div class="m-timeline-1__item-circle<?= (isset($item['icon']['color']) && !empty($item['icon']['color'])) ? (' border border-'.$item['icon']['color']) : '' ?>">
        <?php if(isset($item['icon']['class']) && !empty($item['icon']['class'])) { ?>
            <i class="<?= $item['icon']['class'] ?><?= ($item['icon']['color']) ? (' m--font-'.$item['icon']['color']) : ' m--font-danger' ?>"></i>
        <?php } ?>
    </div>
    <div class="m-timeline-1__item-arrow"></div>
    <span class="m-timeline-1__item-time m--font-brand">
        <?= $item['time'] ?>
    </span>
    <div class="m-timeline-1__item-content">
        <div class="m-timeline-1__item-title">
            <?php if(isset($item['avatar']['src']) && !empty($item['avatar']['src'])) { ?>
                <div class="m-list-pics d-inline-block">
                    <a href="<?= ($item['avatar']['link']) ? $item['avatar']['link'] : '#' ?>">
                        <img src="<?= $item['avatar']['src'] ?>" title="<?= ($item['avatar']['title']) ? $item['avatar']['title'] : '' ?>">
                    </a>
                </div>
            <?php } ?>
            <?= $item['title'] ?>
            <?php if($item['buttons']['remove'] || $item['buttons']['edit']) { ?>
                <div class="row float-right">
                    <div class="col-sm-12">
                        <?php if($item['buttons']['remove']) { ?>
                            <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill mr-2 remove_btn" data-id="<?= isset($item['id']) ? $item['id'] : '' ?>" data-type="<?= isset($item['type_note']) ? $item['type_note'] : '' ?>">
                                <i class="la la-close"></i>
                            </a>
                        <?php } ?>
                        <?php if($item['buttons']['edit']) { ?>
                            <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill edit_btn" data-id="<?= isset($item['id']) ? $item['id'] : '' ?>" data-type="<?= isset($item['type_note']) ? $item['type_note'] : '' ?>" data-toggle="modal" data-action="edit" data-target="#contact-edit-note-popup">
                                <i class="la la-edit"></i>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="m-timeline-1__item-body">
            <?= $item['content'] ?>
            <?php if(isset($item['aditional_info']) && !empty($item['aditional_info'])) { ?>
                <div class="row mt-3">
                    <?php foreach ($item['aditional_info'] as $info_item) { ?>
                        <div class="col-sm-3">
                            <h6>
                                <?= $info_item['title'] ?>:
                            </h6>
                            <span>
                                <?= $info_item['text'] ?>
                            </span>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>