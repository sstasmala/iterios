<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

?>
<div class="m-portlet contact-notes">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-list-1"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_notes', $lang, 'Notes') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#conctact-add-note-popup">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-widget3 m-scrollable" data-scrollable="true" data-max-height="380">
            <?php
            foreach ($contactNotes as $contactNote) {
            ?>
            <div class="m-widget3__item">
                <div class="m-widget3__header">
                    <div class="m-widget3__user-img">
                        <img class="m-widget3__img" src="/<?=($contactNote->user->photo) ? $contactNote->user->photo : 'img/profile_default.png'?>" alt="">
                    </div>
                    <div class="m-widget3__info">
                        <span class="m-widget3__username">
                            <?= $contactNote->user->first_name . ' ' . $contactNote->user->last_name?>
                        </span>
                        <?= (isset($contactNote->request_id))? ', '.Html::a(TranslationHelper::getTranslation('contact_notes_request', $lang, 'Request #').$contactNote->request_id,
                            [Yii::$app->user->identity->tenant->id.'/requests/view', 'id' => $contactNote->request_id],
                            ['class' => 'profile-link']) : '' ?>
                        <br>
                        <span class="m-widget3__time moment-js invisible" data-format="unix">
                            <?= $contactNote['created_at'] ?>
                        </span>
                    </div>
                </div>
                <div class="m-widget3__body">
                    <p class="m-widget3__text">
                        <?=$contactNote->value;?>
                    </p>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>