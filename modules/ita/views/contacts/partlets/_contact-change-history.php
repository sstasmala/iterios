<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet contact-change-history">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-tool"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_change_history', $lang, 'Change history') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-scrollable mCustomScrollbar _mCS_5 mCS-autoHide" data-scrollbar-shown="true" data-scrollable="true" data-max-height="380" style="overflow: visible; height: 380px; max-height: 380px; position: relative;">
            <div class="m-timeline-2 customized">
                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                    <?php foreach ($logs as $log): ?>
                        <div class="m-timeline-2__item <?= ($log !== reset($logs)) ? 'm--margin-top-30' : '' ?>">
                            <span class="m-timeline-2__item-time moment-js invisible" data-format="unix" style="font-size: 1.1rem;">
                                <?= $log['created_at'] ?>
                            </span>
                            <div class="m-timeline-2__item-cricle">
                                <i class="fa fa-genderless <?= $log['circle_color'] ?>"></i>
                            </div>
                            <div class="m-timeline-2__item-text  m--padding-top-5">
                                <?= $log['main_message'] . $log['secondary_message'] ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>