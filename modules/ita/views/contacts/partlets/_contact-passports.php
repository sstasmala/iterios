<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet contact-passports">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-book"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_passports', $lang, 'Passports') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#conctact-add-passport-popup">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php if(isset($contact['passports']) && (!empty($contact['passports']))){ ?>
            <div class="contact-passports-block">
                <table class="table m-table m-table--head-bg-success">
                    <thead>
                        <tr>
                            <th><?= TranslationHelper::getTranslation('contact_name', $lang, 'Name') ?></th>
                            <th><?= TranslationHelper::getTranslation('contact_serial_number', $lang, 'Serial number') ?></th>
                            <th><?= TranslationHelper::getTranslation('modal_add_passport_limit_date', $lang) ?></th>
                            <th class="actions"><?= TranslationHelper::getTranslation('contact_actions', $lang, 'Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $now = \app\helpers\DateTimeHelper::createFromTimestamp(time());
                        ?>
                        <?php foreach ($contact['passports'] as $passport) { ?>
                            <?php
                                $passport_limit_date = \app\helpers\DateTimeHelper::createFromTimestamp($passport['date_limit']);
                                $date_diff = date_diff($now, $passport_limit_date);
                                $extra_row_class = '';
                                if ($date_diff->invert === 1) {
                                    $extra_row_class = 'danger';
                                } elseif ($date_diff->days <= 90) {
                                    $extra_row_class = 'warning';
                                }
                            ?>
                            <tr class="<?= $extra_row_class ? $extra_row_class : '' ?>">
                                <th class="name">
                                    <?= $passport['first_name'] . ' ' . $passport['last_name'] ?>
                                </th>
                                <td class="serial">
                                    <?= $passport['serial'] ?>
                                </td>
                                <td class="date">
                                    <?= \app\helpers\DateTimeHelper::convertTimestampToDate($passport['date_limit']) ?>
                                    <?php
                                        switch ($extra_row_class){
                                            case 'danger':
                                                echo '<i class="la la-exclamation-circle notification-icon" data-toggle="m-tooltip" title="' . TranslationHelper::getTranslation('notification_icon_passport_date_limit_danger', $lang, "Passport's validity expires!") . '"></i>';
                                                break;
                                            case 'warning':
                                                echo '<i class="la la-exclamation-triangle notification-icon" data-toggle="m-tooltip" title="' . TranslationHelper::getTranslation('notification_icon_passport_date_limit_warning', $lang, "The validity of the passport expired!") . '"></i>';
                                                break;
                                        }
                                    ?>
                                </td>
                                <td class="actions">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link btn btn-sm btn-light m-btn m-btn--outline-2x m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle">
                                            <i class="la la-plus m--hide"></i>
                                            <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__item">
                                                                <?php
                                                                    $passport_data = json_encode([
                                                                        'id' => $passport['id'],
                                                                        'type' => $passport['type_id'],
                                                                        'first_name' => $passport['first_name'],
                                                                        'last_name' => $passport['last_name'],
                                                                        'serial' => $passport['serial'],
                                                                        'country_id' => $passport['country'],
                                                                        'country_name' => \app\helpers\MCHelper::getCountryById($passport['country'],$lang)[0]['title'],
                                                                        'nationality_id' => $passport['nationality'],
                                                                        'nationality_name' => \app\helpers\MCHelper::getCountryById($passport['nationality'],$lang)[0]['title'],
                                                                        'birth_date' => \app\helpers\DateTimeHelper::convertTimestampToDate($passport['birth_date']),
                                                                        'date_limit' => \app\helpers\DateTimeHelper::convertTimestampToDate($passport['date_limit']),
                                                                        'issued_date' => \app\helpers\DateTimeHelper::convertTimestampToDate($passport['issued_date']),
                                                                        'issued_owner' => $passport['issued_owner']
                                                                    ]);
                                                                ?>
                                                                <a href="" class="m-nav__link" data-toggle="modal" data-target="#conctact-add-passport-popup" data-passport-data='<?= $passport_data ?>'>
                                                                    <form class="edit-passport_form">
                                                                        <button type="submit">
                                                                            <i class="m-nav__link-icon flaticon-edit-1"></i>
                                                                            <span class="m-nav__link-text">
                                                                                <?= TranslationHelper::getTranslation('edit', $lang, 'Edit') ?>
                                                                            </span>
                                                                        </button>
                                                                    </form>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <form class="delete-passport_form" action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/delete-passport?id=' . $contact['id'] ?>" method="GET">
                                                                        <input type="hidden" name="passport_id" value="<?= $passport['id'] ?>">
                                                                        <button type="submit">
                                                                            <i class="m-nav__link-icon flaticon-circle"></i>
                                                                            <span class="m-nav__link-text">
                                                                                <?= TranslationHelper::getTranslation('contacts_delete', $lang, 'Delete') ?>
                                                                            </span>
                                                                        </button>
                                                                    </form>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="row">
                <div class="col-12 no-passports">
                    <span><?= TranslationHelper::getTranslation('contact_no_passports', $lang, 'No passports') ?></span>
                </div>
            </div>
        <?php } ?>
    </div>
</div>