<?php
use app\helpers\TranslationHelper;
?>

<div class="m-portlet contact-files-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-tool-1"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contact_files_portlet_title', $lang, 'Contact files') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#file_add_modal" title="<?= TranslationHelper::getTranslation('contact_page_add_doc_btn', $lang, 'Add document') ?>">
                        <i class="la la-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php
            $files = [
                [
                    'id' => 1,
                    'name' => 'Metronic Documentation',
                    'icon_src' => '/metronic/assets/app/media/img/files/doc.svg'
                ],
                [
                    'id' => 2,
                    'name' => 'Make JPEG Great Again',
                    'icon_src' => '/metronic/assets/app/media/img/files/jpg.svg'
                ],
                [
                    'id' => 3,
                    'name' => 'Full Deeveloper Manual For 4.7',
                    'icon_src' => '/metronic/assets/app/media/img/files/pdf.svg'
                ],
                [
                    'id' => 4,
                    'name' => 'Make JS Great Again',
                    'icon_src' => '/metronic/assets/app/media/img/files/javascript.svg'
                ],
                [
                    'id' => 5,
                    'name' => 'Download Ziped version OF 5.0',
                    'icon_src' => '/metronic/assets/app/media/img/files/zip.svg'
                ],
                [
                    'id' => 6,
                    'name' => 'Finance Report 2016/2017',
                    'icon_src' => '/metronic/assets/app/media/img/files/pdf.svg'
                ]
            ];
        ?>
        <div class="m-widget4<?= (empty($files) ? ' d-none' : '') ?>">
            <?php foreach ($files as $file): ?>
                <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--icon">
                        <img src="<?= $file['icon_src'] ?>">
                    </div>
                    <div class="m-widget4__info">
                        <span class="m-widget4__text">
                            <a href="#" class="m-link m--font-bold file-link" data-toggle="modal" data-target="#file_upload_view_modal" data-contact_id="<?= $contact['id'] ?>" data-contact_file_id="<?= $file['id'] ?>">
                                <?= $file['name'] ?>
                            </a>
                        </span>
                    </div>
                    <div class="m-widget4__ext">
                        <a href="#" class="m-widget4__icon contact-file-delete-link" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>" data-contact_file_id="<?= $file['id'] ?>">
                            <i class="la la-trash"></i>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if (empty($files)): ?>
            <div class="text-center"><?= \app\helpers\TranslationHelper::getTranslation('contact_files_not_found', $lang, 'Documents not found!'); ?></div>
        <?php endif; ?>
    </div>
</div>