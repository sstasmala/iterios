<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet manual-contacts-merge">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-web"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('manual_contacts_merge_portlet_title', $lang, 'Contacts merge') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div id="merge-block">
            <div class="merge-block-inner">
                <div class="contact-block additional-contact">
                    <div class="avatar-block">
                        <img src="/img/profile_default.png" alt="<?= trim(($contact['first_name'] ? (' ' . $contact['first_name']) : '') . ($contact['middle_name'] ? (' ' . $contact['middle_name']) : '') . ($contact['last_name'] ? (' ' . $contact['last_name']) : '')) ?>">
                    </div>
                    <div class="select-block">
                        <select name="contact_merge_object" id="contact_merge_object_select" class="form-control m-bootstrap-select"></select>
                    </div>
                </div>
                <div class="arrow">
                    <i class="fas fa-angle-double-right"></i>
                </div>
                <div class="contact-block main-contact">
                    <div class="avatar-block">
                        <img src="/img/profile_default.png" alt="<?= trim(($contact['first_name'] ? (' ' . $contact['first_name']) : '') . ($contact['middle_name'] ? (' ' . $contact['middle_name']) : '') . ($contact['last_name'] ? (' ' . $contact['last_name']) : '')) ?>">
                    </div>
                    <div class="name">
                        <?= trim(($contact['first_name'] ? (' ' . $contact['first_name']) : '') . ($contact['middle_name'] ? (' ' . $contact['middle_name']) : '') . ($contact['last_name'] ? (' ' . $contact['last_name']) : '')) ?>
                    </div>
                    <?php if(isset($contact['emails']) && !empty($contact['emails'])){ ?>
                        <div class="email">
                            <?= $contact['emails'][0]['value'] ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="merge-contacts-names">
                <div class="names-content">
                    <span class="additional-contact-name"></span>
                    <span class="m--font-bolder">
                        <?= ' ' . TranslationHelper::getTranslation('manual_contacts_merge_will_merge_message_part', $lang, 'will be merged into') . ' ' ?>
                    </span>
                    <span class="main-contact-name">
                       <?= trim(($contact['first_name'] ? (' ' . $contact['first_name']) : '') . ($contact['middle_name'] ? (' ' . $contact['middle_name']) : '') . ($contact['last_name'] ? (' ' . $contact['last_name']) : '')) ?>
                    </span>
                </div>
            </div>
            <div class="merge-rules jumbotron">
                <?php if(isset($contact['emails']) && !empty($contact['emails'])){ ?>
                    <p>
                        1. The resulting contact record will use the primary email <span class="m--font-bolder">"<?= $contact['emails'][0]['value'] ?>"</span>
                    </p>
                <?php } else { ?>
                    <p>
                        1. Primary contact have not email adress!
                    </p>
                <?php } ?>
                <p>
                    2. Timeline activity for both contact records will be preserved in the new record
                </p>
                <p>
                    3. The most recent value for each property (e.g. "First name") will be used for the new record
                </p>
                <p>
                    <span class="m--font-bolder">Read more: </span>
                    <a href="#" class="m-link m--font-bolder">
                        What happens when Imerge two contacts?
                    </a>
                </p>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-success mr-2">
                        <?= TranslationHelper::getTranslation('contacts_merge_btn', $lang, 'Merge') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>