<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('contacts_contacts', $lang, 'Contacts');
$this->registerCssFile('@web/css/contacts/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/contacts/datatable.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/contacts/filters.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/contacts/tags-datatable.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/contact/_m_modal_tags_table');
echo $this->render('/partials/modals/contact/_m_modal_contacts_settings_modal');
echo $this->render('/partials/modals/contact/_m_modal_contacts_edit_settings_modal');
echo $this->render('/partials/modals/contact/_m_modal_quick_edit_contact');
echo $this->render('/partials/modals/contact/_m_modal_contacts_bulk_actions');
echo $this->render('/partials/modals/contact/_m_modal_contacts_merge');
$this->endBlock();
?>

<?php $this->beginBlock('subheader_controls'); ?>
    <?= $this->render('filters/_subheader_controls'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-left'); ?>
    <?= $this->render('filters/_filters-left'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-right'); ?>
    <?= $this->render('filters/_filters-right'); ?>
<?php $this->endBlock();?>

<div class="m-portlet m-portlet--mobile" id="contacts-datatable">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('contacts_contacts', $lang);?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="row align-items-center">
                <div class="col-12 text-right">
                    <div class="tools">
                        <div class="tools__left_section">
                            <div id="selection-row">
                                <button id="refresh-contacts" class="btn btn-outline-info mb-2 mb-md-0 mr-1" title="<?= TranslationHelper::getTranslation('contacts_bulk', $lang); ?>" data-toggle="modal" data-target="#m_modal_contacts_bulk_actions">
                                    <i class="la la-pencil-square-o"></i>
                                    <?= TranslationHelper::getTranslation('contacts_bulk', $lang); ?>
                                </button>
                                <button id="remove-contacts" class="btn btn-outline-info mb-2 mb-md-0 mr-1" title="<?= TranslationHelper::getTranslation('contacts_delete', $lang); ?>">
                                    <i class="la la-trash"></i>
                                    <?= TranslationHelper::getTranslation('contacts_delete', $lang); ?>
                                </button>
                            </div>
                            <button class="btn btn-success m-btn m-btn--icon mb-2 mb-md-0 ml-2" data-toggle="modal" data-target="#m_modal_create_contact">
                                <span>
                                    <i class="la la-plus-circle"></i>
                                    <span>
                                        <?= TranslationHelper::getTranslation('contacts_new_contact', $lang); ?>
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="tools__right_section">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push ml-3" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle">
                                    <i class="la la-plus m--hide"></i>
                                    <i class="la la-ellipsis-h"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link" id="remove-contacts" data-toggle="modal" data-target="#tags-table-popup">
                                                            <i class="m-nav__link-icon la la-tags"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_menu_tags', $lang); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-sitemap"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_menu_references', $lang); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link" data-toggle="modal" data-target="#contacts_settings_modal">
                                                            <i class="m-nav__link-icon la la-gears"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_menu_list_settings', $lang); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link" data-toggle="modal" data-target="#contacts_edit_settings_modal">
                                                            <i class="m-nav__link-icon la la-gear"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_edit_window_settings', $lang, 'Edit window settings'); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/import') ?>" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-sign-in"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_menu_list_import', $lang); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-group"></i>
                                                            <span class="m-nav__link-text">
                                                                <?= TranslationHelper::getTranslation('contacts_menu_contacts_combine', $lang); ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row mb-3">
            <div class="col-12">
                <div class="m-alert m-alert--icon m-alert--outline alert alert-primary" id="contacts_merge_alert" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong><?= TranslationHelper::getTranslation('attention', $lang, 'Attention'); ?>!</strong>
                        <?= ' ' . TranslationHelper::getTranslation('contacts_merge_found_message_1', $lang, 'Was found') . ' ' . 16 . ' ' .  TranslationHelper::getTranslation('contacts_merge_found_message_2', $lang, 'similar contacts') ?>
                    </div>
                    <div class="m-alert__actions">
                        <button type="button" class="btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide" data-toggle="modal" data-target="#contacts_merge_modal">
                            <?= TranslationHelper::getTranslation('contacts_merge_view_btn', $lang, 'View'); ?>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm m-btn m-btn--pill m-btn--wide" data-dismiss="alert" aria-label="Close">
                            <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse m--margin-bottom-10" id="m_datatable_selection">
            <div class="row align-items-center">
                <div class="col-xl-6">
                    <div class="m-form__group m-form__group--inline">
                        <div class="m-form__label m-form__label-no-wrap">
                            <label class="m--font-bold m--font-danger-">
                                <?= TranslationHelper::getTranslation('contacts_records_selected1', $lang);?>
                                <span id="m_datatable_selected_number"></span>
                                <?= TranslationHelper::getTranslation('contacts_records_selected2', $lang);?>
                                <a id="select-all-contacts" href="">
                                    <?= TranslationHelper::getTranslation('contacts_records_selected3', $lang);?>
                                    <span id="m_datatable_total_number"></span>
                                    <?= TranslationHelper::getTranslation('contacts_records_selected4', $lang);?>
                                </a>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m_datatable" id="contacts_t"></div>
    </div>
</div>