<div class="row">
    <div class="col-lg-6 filters-right-search">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Contacts
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control m-input" name="phone" placeholder="Phone">
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
        </div>
    </div>
    <div class="col-lg-6 tag-right-search">
        <div class="title">
            <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                Tags
            </span>
        </div>
        <div class="form-group">
            <select class="form-control m-select2" id="tags_search" name="tags-search" multiple="multiple">
            </select>
        </div>
    </div>
</div>