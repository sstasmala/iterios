<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('is_inx_page_title', $lang, 'Integrations with supliers');
$this->registerCssFile('@web/css/integrations-suppliers/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/integrations-suppliers/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/integrations-suppliers/_m_modal_integration_details');
$this->endBlock();
?>

<div class="m-portlet m-portlet--tabs" id="integtations_list_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('is_inx_ilp_title', $lang, 'Integrations') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#ils_tab_1_1" role="tab">
                        <?= TranslationHelper::getTranslation('is_inx_my_integrations_tab_label', $lang, 'My integrations') ?>
                    </a>
                </li>
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#ils_tab_1_2" role="tab">
                        <?= TranslationHelper::getTranslation('is_inx_all_integrations_tab_label', $lang, 'All integrations') ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="tab-content">
            <div class="tab-pane active" id="ils_tab_1_1">
                <div class="integration-desc-text row mb-4">
                    <div class="col-12">
                        <span>
                            <?= TranslationHelper::getTranslation('is_header_desc', $lang) ?>
                        </span>
                    </div>
                </div>
                <div class="integrations-content">
                    <div class="items-wrap">
                        <div class="text-center" style="width: 100%;padding: 20px;">
                            <?= TranslationHelper::getTranslation('datatable_records_not_found', $lang) ?>
                            <a id="ils_show_all" href="#"><?= TranslationHelper::getTranslation('datatable_show_all_message', $lang) ?></a>
                        </div>
                    </div>
                    <div class="demo-items-wrap">
                        <div class="integration-item" style="display: none;">
                            <div class="logo-block">
                                <div class="logo-inner">
                                    <img alt="[SUPPLIER_NAME]" class="logo-image" src="">
                                </div>
                            </div>
                            <div class="description-block">
                                <table class="cred">
                                    <tbody>
                                        <tr>
                                            <td><?= TranslationHelper::getTranslation('is_inx_api_domain_label', $lang, 'API server') ?>:</td>
                                            <td>
                                                server.com
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= TranslationHelper::getTranslation('is_inx_api_key_label', $lang, 'API key') ?>:</td>
                                            <td>
                                                abc213def456
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="mt-2" style="display: none;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right click-prevented">
                                                        <div class="dropdown-item">
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><?= TranslationHelper::getTranslation('is_inx_api_domain_label', $lang, 'API server') ?>:</td>
                                                                        <td>
                                                                            server.com
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><?= TranslationHelper::getTranslation('is_inx_api_key_label', $lang, 'API key') ?>:</td>
                                                                        <td>
                                                                            abc213def456
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="links-block">
                                <a href="#" class="m-link m--font-bold text-center connect-link" data-toggle="modal" data-target="#integration_details_modal" data-intergation-id="1">
                                    <?= TranslationHelper::getTranslation('is_inx_view_integration_link', $lang, 'View integration') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="paginator-wrap" style="display: none;">
                        <div class="m-datatable m-datatable--default">
                            <div class="m-datatable__pager text-center">
                                <ul class="m-datatable__pager-nav">
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_first', $lang, 'First') ?>" class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-double-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_previous', $lang, 'Previous') ?>" class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="1" title="1">1</a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_next', $lang, 'Next') ?>" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="2">
                                            <i class="la la-angle-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_last', $lang, 'Last') ?>" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="35">
                                            <i class="la la-angle-double-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="ils_tab_1_2">
                <div class="integration-desc-text row mb-4">
                    <div class="col-12">
                        <span>
                            <?= TranslationHelper::getTranslation('is_header_desc_all_integrations', $lang) ?>
                        </span>
                    </div>
                </div>
                <div id="ils_my_integrations_filter" class="form-group m-form__group row align-items-end">
                    <div class="col-sm-6 col-md-4">
                        <div class="m-form__group m-form__group--inline">
                            <div class="m-form__label">
                                <label class="m-label m-label--single">
                                    <?= TranslationHelper::getTranslation('type', $lang, 'Type') ?>:
                                </label>
                            </div>
                            <div class="m-form__control">
                                <select class="form-control m_selectpicker m-bootstrap-select" id="ils_filter_type" title="<?= TranslationHelper::getTranslation('is_select_supplier_type', $lang) ?>">
                                    <?php foreach ($supplier_types as $supplier_type): ?>
                                        <option value="<?= $supplier_type->id ?>">
                                            <?= $supplier_type->value ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="d-sm-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>..." id="ils_filter_search">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span>
                                    <i class="la la-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="integrations-content">
                    <div class="items-wrap">
                        <div class="text-center" style="width: 100%;padding: 20px;">
                            <?= TranslationHelper::getTranslation('datatable_records_not_found', $lang) ?>
                            <a id="ils_show_all" href="#"><?= TranslationHelper::getTranslation('datatable_show_all_message', $lang) ?></a>
                        </div>
                    </div>
                    <div class="demo-items-wrap">
                        <div class="integration-item" style="display: none;">
                            <div class="logo-block">
                                <div class="logo-inner">
                                    <img alt="[SUPPLIER_NAME]" class="logo-image" src="">
                                </div>
                            </div>
                            <div class="description-block">
                                <table>
                                    <tbody>
                                        <tr class="country">
                                            <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_country_label', $lang, 'Country') ?>:</td>
                                            <td>
                                                <div class="iti-flag ua mr-1"></div>
                                                <span class="country m--font-bold">Ukraine</span>
                                            </td>
                                        </tr>
                                        <tr class="code">
                                            <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_code_label', $lang, 'Code') ?>:</td>
                                            <td>
                                                <span class="m--font-bold">
                                                    Teztour
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="company">
                                            <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_company_label', $lang, 'Company') ?>:</td>
                                            <td>
                                                <span class="m--font-bold">
                                                    Туроператор
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="website">
                                            <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_website_label', $lang, 'Website') ?>:</td>
                                            <td>
                                                <a href="megatourist.com" class="m-link m--font-bold" rel="nofollow" target="_blank">
                                                    megatourist.com.ua
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="links-block">
                                <a href="#" class="m-link m--font-bold text-center connect-link" data-toggle="modal" data-target="#integration_details_modal" data-intergation-id="1">
                                    <?= TranslationHelper::getTranslation('is_inx_connect_integration_link', $lang, 'Connect') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="paginator-wrap" style="display: none;">
                        <div class="m-datatable m-datatable--default">
                            <div class="m-datatable__pager text-center">
                                <ul class="m-datatable__pager-nav">
                                    <li>
                                        <a title="First" class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-double-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Previous" class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="1" title="1">1</a>
                                    </li>
                                    <li>
                                        <a title="Next" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="2">
                                            <i class="la la-angle-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Last" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="35">
                                            <i class="la la-angle-double-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>