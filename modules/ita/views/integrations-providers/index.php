<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('ip_inx_page_title', $lang, 'Integrations with providers');
$this->registerCssFile('@web/css/integrations-providers/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/integrations-providers/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/integrations-providers/_m_modal_integration_details');
$this->endBlock();
?>

<div class="m-portlet m-portlet--tabs" id="integtations_list_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('ip_inx_ilp_title', $lang, 'Integrations') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#ilp_tab_1_1" role="tab">
                        <?= TranslationHelper::getTranslation('ip_inx_my_integrations_tab_label', $lang, 'My integrations') ?>
                    </a>
                </li>
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#ilp_tab_1_2" role="tab">
                        <?= TranslationHelper::getTranslation('ip_inx_all_integrations_tab_label', $lang, 'All integrations') ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="tab-content">
            <div class="tab-pane active" id="ilp_tab_1_1">
                <div class="integration-desc-text row mb-4">
                    <div class="col-12">
                        <span>
                            <?= TranslationHelper::getTranslation('ip_header_desc', $lang) ?>
                        </span>
                    </div>
                </div>
                <div class="integrations-content">
                    <div class="items-wrap">
                        <div class="text-center" style="width: 100%;padding: 20px;">
                            <?= TranslationHelper::getTranslation('datatable_records_not_found', $lang) ?>
                            <a id="ilp_show_all" href="#"><?= TranslationHelper::getTranslation('datatable_show_all_message', $lang) ?></a>
                        </div>
                    </div>
                    <div class="demo-items-wrap">
                        <div class="integration-item" style="display: none;">
                            <div class="logo-block">
                                <div class="logo-inner">
                                    <img alt="" class="logo-image" src="">
                                </div>
                            </div>
                            <div class="description-block">
                                <div class="text-center">
                                    <span></span>
                                </div>
                            </div>
                            <div class="links-block">
                                <a href="#" class="m-link m--font-bold text-center" data-toggle="modal" data-target="#integration_details_modal" data-intergation-id="">
                                    <?= TranslationHelper::getTranslation('ip_inx_view_integration_link', $lang, 'View integration') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="paginator-wrap" style="display: none;">
                        <div class="m-datatable m-datatable--default">
                            <div class="m-datatable__pager text-center">
                                <ul class="m-datatable__pager-nav">
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_first', $lang, 'First') ?>" class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-double-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_previous', $lang, 'Previous') ?>" class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="1" title="1">1</a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="2" title="2">2</a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="3" title="3">3</a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="4" title="4">4</a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_next', $lang, 'Next') ?>" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="2">
                                            <i class="la la-angle-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="<?= TranslationHelper::getTranslation('pagination_last', $lang, 'Last') ?>" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="35">
                                            <i class="la la-angle-double-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="ilp_tab_1_2">
                <div class="integration-desc-text row mb-4">
                    <div class="col-12">
                        <span>
                            <?= TranslationHelper::getTranslation('ip_header_desc_all_integrations', $lang) ?>
                        </span>
                    </div>
                </div>
                <div id="ilp_my_integrations_filter" class="form-group m-form__group row align-items-end">
                    <div class="col-sm-6 col-md-4">
                        <div class="m-form__group m-form__group--inline">
                            <div class="m-form__label">
                                <label class="m-label m-label--single">
                                    <?= TranslationHelper::getTranslation('type', $lang, 'Type') ?>:
                                </label>
                            </div>
                            <div class="m-form__control">
                                <select class="form-control m_selectpicker m-bootstrap-select" id="ilp_filter_type" title="<?= TranslationHelper::getTranslation('ip_select_provider_type', $lang) ?>">
                                    <?php foreach ($provider_types as $provider_type): ?>
                                        <option value="<?= $provider_type->id ?>">
                                            <?= $provider_type->value ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="d-sm-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>..." id="ilp_filter_search">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span>
                                    <i class="la la-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="integrations-content">
                    <div class="items-wrap">
                        <div class="text-center" style="width: 100%;padding: 20px;">
                            <?= TranslationHelper::getTranslation('datatable_records_not_found', $lang) ?>
                            <a id="ilp_show_all" href="#"><?= TranslationHelper::getTranslation('datatable_show_all_message', $lang) ?></a>
                        </div>
                    </div>
                    <div class="demo-items-wrap">
                        <div class="integration-item" style="display: none;">
                            <div class="logo-block">
                                <div class="logo-inner">
                                    <img alt="" class="logo-image" src="">
                                </div>
                            </div>
                            <div class="description-block">
                                <div class="text-center">
                                    <span></span>
                                </div>
                            </div>
                            <div class="links-block">
                                <a href="#" class="m-link m--font-bold text-center" data-toggle="modal" data-target="#integration_details_modal" data-intergation-id="">
                                    <?= TranslationHelper::getTranslation('ip_inx_connect_integration_link', $lang, 'Connect') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="paginator-wrap" style="display: none;">
                        <div class="m-datatable m-datatable--default">
                            <div class="m-datatable__pager text-center">
                                <ul class="m-datatable__pager-nav">
                                    <li>
                                        <a title="First" class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-double-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Previous" class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" data-page="1">
                                            <i class="la la-angle-left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="1" title="1">1</a>
                                    </li>
                                    <li>
                                        <a title="Next" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="2">
                                            <i class="la la-angle-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Last" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="">
                                            <i class="la la-angle-double-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>