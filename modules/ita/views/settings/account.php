<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('account', $lang, 'Account');
$this->registerJsFile('@web/js/settings/account.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
//$this->registerJsFile('@web/js/settings/account.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/settings/account.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php
/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/settings/_m_modal_requisites_help');
$this->endBlock();
?>

<div class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('account', $lang, 'Account') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body m-portlet__body--no-padding">
        <!--begin: Form Wizard-->
        <div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard" data-current-step="1">
            <!--begin: Message container -->
            <div class="m-portlet__padding-x">
                <!-- Here you can put a message or alert -->
            </div>
            <!--end: Message container -->
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12">
                    <!--begin: Form Wizard Head -->
                    <div class="m-wizard__head">
                        <!--begin: Form Wizard Progress -->
                        <div class="m-wizard__progress">
                            <div class="progress">
                                <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <!--end: Form Wizard Progress --> 
                        <!--begin: Form Wizard Nav -->
                        <div class="m-wizard__nav">
                            <div class="m-wizard__steps">
                                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    1
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-line">
                                            <span></span>
                                        </div>
                                        <div class="m-wizard__step-label">
                                            <a href="#" class="m-wizard__step-number" style="color: inherit;">
                                                <?= TranslationHelper::getTranslation('acc_set_main_settings_title', $lang, 'Main settings') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    2
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-line">
                                            <span></span>
                                        </div>
                                        <div class="m-wizard__step-label">
                                            <a href="#" class="m-wizard__step-number" style="color: inherit;">
                                                <?= TranslationHelper::getTranslation('acc_set_agency_contacts_title', $lang, 'Agency contacts') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    3
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-line">
                                            <span></span>
                                        </div>
                                        <div class="m-wizard__step-label">
                                            <a href="#" class="m-wizard__step-number" style="color: inherit;">
                                                <?= TranslationHelper::getTranslation('acc_set_requisites_title', $lang, 'Requisites') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    4
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-line">
                                            <span></span>
                                        </div>
                                        <div class="m-wizard__step-label">
                                            <a href="#" class="m-wizard__step-number" style="color: inherit;">
                                                <?= TranslationHelper::getTranslation('acc_set_reminders_title', $lang, 'Reminders') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end: Form Wizard Nav -->
                    </div>
                    <!--end: Form Wizard Head -->
                </div>
                <div class="col-xl-9 col-lg-12">
                    <!--begin: Form Wizard Form-->
                    <div class="m-wizard__form">
                        <!--
                            1) Use m-form--label-align-left class to alight the form input lables to the right
                            2) Use m-form--state class to highlight input control borders on form validation
                        -->
                        <div class="m-form m-form--label-align-left- m-form--state-">
                            <!--begin: Form Body -->
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                    <form id="wizard_form_1">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                <?= TranslationHelper::getTranslation('acc_set_main_settings_title', $lang, 'Main settings') ?>
                                            </h3>
                                        </div>
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                        <div class="m-form__section m-form__section--first">

                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_name_input_label', $lang, 'Name') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <?=
                                                        Html::input(
                                                                'text',
                                                                'name',
                                                                $tenant->name,
                                                                [
                                                                    'class' => 'form-control m-input'
                                                                ])
                                                    ?>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_name_help_text', $lang, 'Enter your tenant name') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_country_input_label', $lang, 'Country') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-bootstrap-select country-select-ajax" name="country" id="account_country_select">
                                                        <option value="<?= $tenant->country ?>" selected><?=\app\helpers\MCHelper::getCountryById($tenant->country,$lang)[0]['title']?></option>
                                                    </select>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_country_help_text', $lang, 'Select your tenant country') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_lang_input_label', $lang, 'Interface language') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-bootstrap-select m_selectpicker" name="lang" id="select_language">
                                                        <option value="<?= $tenant->language->id ?>" selected><?=$tenant->language->name?></option>
                                                    </select>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_lang_help_text', $lang, 'Select interface language') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <?php $zones = timezone_identifiers_list();?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_timezone_input_label', $lang, 'Timezone') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-bootstrap-select timezone-select-2" name="timezone">
                                                        <?php foreach ($zones as $zone):?>
                                                            <option value="<?=$zone?>" <?=($zone==$tenant->timezone)?'selected':'';?>><?=$zone?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_timezone_help_text', $lang, 'Change timezone') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_date_format_input_label', $lang, 'Date format') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-bootstrap-select m_selectpicker" name="date_format">
                                                        <?php foreach ($date_formats as $v=>$format):?>
                                                            <option value="<?=$v?>" <?=($tenant->date_format==$v)?'selected':'';?>><?=$format?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_date_format_help_text', $lang, 'Change date format') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_time_format_input_label', $lang, 'Time format') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <label class="m-radio mb-3 mr-4">
                                                        <input type="radio" name="time_format" value="h:i:s A" <?=($tenant->time_format=='h:i:s A')?'checked':'';?>>
                                                        12
                                                        <span></span>
                                                    </label>
                                                    <label class="m-radio mb-3">
                                                        <input type="radio" name="time_format" value="H:i:s" <?=($tenant->time_format=='H:i:s')?'checked':'';?>>
                                                        24
                                                        <span></span>
                                                    </label>
                                                    <br>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_time_format_help_text', $lang, 'Change time format') ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_week_start_input_label', $lang) ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <label class="m-radio mb-3 mr-4">
                                                        <input type="radio" name="week_start" value="0" <?=($tenant->week_start === '0')?'checked':'';?>>
                                                        <?= TranslationHelper::getTranslation('acc_set_week_start_sunday', $lang) ?>
                                                        <span></span>
                                                    </label>
                                                    <label class="m-radio mb-3">
                                                        <input type="radio" name="week_start" value="1" <?=($tenant->week_start === '1')?'checked':'';?>>
                                                        <?= TranslationHelper::getTranslation('acc_set_week_start_monday', $lang) ?>
                                                        <span></span>
                                                    </label>
                                                    <br>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_set_week_start_help_text', $lang) ?>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_usage_currency', $lang, 'Usage Currency') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">


                                                    <select class="form-control m-select2" id="acc_set_usage_currency" name="curr[]" multiple="">
                                                        <?php foreach ($allCurrency as $currId =>$currIso){ ?>
                                                            <option value="<?= $currId ?>" <?php if(isset($tenant->usage_currency))
                                                                if (in_array($currId, json_decode($tenant->usage_currency))) echo 'selected="selected"';
                                                            ?> >
                                                                <?= $currIso ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>

                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_select_currency_all', $lang, 'Select Currency') ?>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_main_currency', $lang, 'Main currency') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-select2" id="account_main_currency" name="main_currency">
                                                        <?php if (isset($tenant->usage_currency)){
                                                            foreach (json_decode($tenant->usage_currency) as $curr){ ?>
                                                                <option value="<?= $curr ?>" <?=($curr==$tenant->main_currency)?'selected="selected"':''?> >
                                                                    <?= $allCurrency[$curr]; ?>
                                                                </option>
                                                            <?php }
                                                        } ?>
                                                    </select>

                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_select_currency_main', $lang, 'Select Currency') ?>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_currency_display', $lang, 'Main currency') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <select class="form-control m-select2" id="account_currency_display" name="currency_display">
                                                        <option value="symbol" <?=('symbol'==$tenant->currency_display)?'selected="selected"':''?> >
                                                            symbol
                                                        </option>
                                                        <option value="code" <?=('code'==$tenant->currency_display)?'selected="selected"':''?> >
                                                            code
                                                        </option>
                                                    </select>
                                                    <span class="m-form__help">
                                                        <?= TranslationHelper::getTranslation('acc_select_currency_main', $lang, 'Select Currency') ?>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    <?= TranslationHelper::getTranslation('acc_set_system_set_title', $lang, 'System values') ?>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    <?= TranslationHelper::getTranslation('acc_set_input_system_tags_name', $lang, 'System tags') ?>:
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <label class="m-checkbox">
                                                        <?= Html::checkbox('system_tags', $tenant->system_tags) ?> <?= TranslationHelper::getTranslation('acc_set_input_system_tags_text', $lang, 'Use system tags') ?>
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--end: Form Wizard Step 1-->
                                <!--begin: Form Wizard Step 2-->
                                <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                    <form id="wizard_form_2">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                <?= TranslationHelper::getTranslation('acc_set_agency_contacts_title', $lang, 'Agency contacts') ?>
                                            </h3>
                                        </div>
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_phones_label', $lang, 'Phones') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div id="agency-phones-repeater">
                                                        <div class="repeater-list" data-repeater-list="agency[phones]">
                                                            <?php if(!empty($agency_phones)){
                                                                foreach ($agency_phones as $phone){ ?>
                                                                    <div data-repeater-item class="row mb-3">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group">
                                                                                <input class="form-control m-input masked-input validate-phone input_phone_visible" name="phone" id="settings_edit__phone_input"  valid="phoneNumber" value="<?= $phone['value'] ?>" />
<!--                                                                                <input type="text" name="value" class="form-control m-input masked-input" value="--><?//= $phone['value'] ?><!--" placeholder="--><?//= TranslationHelper::getTranslation('acc_set_agency_contacts_phone_input_placeholder', $lang, 'Enter phone number') ?><!--" data-inputmask="'mask': '99(999)999-9999'">-->
                                                                                <input type="hidden" name="id" value="<?= $phone['id'] ?>">
                                                                                <div class="input-group-append">
                                                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                        <i class="la la-close"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div data-repeater-item class="row mb-3">
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group">
                                                                            <input class="form-control m-input masked-input validate-add-phone" name="phone" id="settings_added__phone_input"  valid="phoneNumber_add" />
<!--                                                                            <input type="text" name="value" class="form-control m-input masked-input" value="" placeholder="--><?//= TranslationHelper::getTranslation('acc_set_agency_contacts_phone_input_placeholder', $lang, 'Enter phone number') ?><!--" data-inputmask="'mask': '99(999)999-9999'">-->
                                                                            <input type="hidden" name="id" value="">
                                                                            <div class="input-group-append">
                                                                                <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;<?= TranslationHelper::getTranslation('acc_set_agency_contacts_add_button', $lang, 'Add') ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_emails_label', $lang, 'Emails') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div id="agency-emails-repeater">
                                                        <div class="repeater-list" data-repeater-list="agency[emails]">
                                                            <?php if(!empty($agency_emails)){
                                                                foreach ($agency_emails as $email){ ?>
                                                                    <div data-repeater-item class="row mb-3">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group">
                                                                                <input type="text" name="value" class="form-control m-input masked-input" value="<?= $email['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_email_input_placeholder', $lang, 'Enter email') ?>" data-inputmask="'alias': 'email'">
                                                                                <input type="hidden" name="id" value="<?= $email['id'] ?>">
                                                                                <div class="input-group-append">
                                                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                        <i class="la la-close"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div data-repeater-item class="row mb-3">
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                                            <input type="text" name="value" class="form-control m-input masked-input" value="" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_email_input_placeholder', $lang, 'Enter email') ?>" data-inputmask="'alias': 'email'">
                                                                            <input type="hidden" name="id" value="">
                                                                            <div class="input-group-append">
                                                                                <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;<?= TranslationHelper::getTranslation('acc_set_agency_contacts_add_button', $lang, 'Add') ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_socials_label', $lang, 'Socials') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div id="agency-socials-repeater">
                                                        <div class="repeater-list" data-repeater-list="agency[socials]">
                                                            <?php if (!empty($agency_socials)){
                                                                foreach ($agency_socials as $social_key => $social){ ?>
                                                                    <div data-repeater-item class="row mb-3">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                                                <div class="input-group-prepend">
                                                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <?= !empty($contacts_types['socials'][$social['type_id']]) ? ucfirst($contacts_types['socials'][$social['type_id']]['name']) : '' ?>
                                                                                    </button>
                                                                                    <div class="dropdown-menu">
                                                                                        <?php foreach ($contacts_types['socials'] as $type) { ?>
                                                                                            <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" name="id" value="<?= $social['id'] ?>">
                                                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $social['type_id'] ?>">
                                                                                <input type="text" name="value" class="form-control m-input" value="<?= $social['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_social_input_placeholder', $lang, 'Enter social') ?>">
                                                                                <div class="input-group-append">
                                                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                        <i class="la la-close"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div data-repeater-item class="row mb-3">
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                                            <div class="input-group-prepend">
                                                                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <?= !empty($contacts_types['socials']) ? ucfirst(current($contacts_types['socials'])['name']) : '' ?>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                    <?php foreach ($contacts_types['socials'] as $type) { ?>
                                                                                        <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <input type="hidden" name="id" value="">
                                                                            <input type="hidden" name="type_id" class="hidden-extra-option" value="">
                                                                            <input type="text" name="value" class="form-control m-input" value="" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_social_input_placeholder', $lang, 'Enter social') ?>">
                                                                            <div class="input-group-append">
                                                                                <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;<?= TranslationHelper::getTranslation('acc_set_agency_contacts_add_button', $lang, 'Add') ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_messengers_label', $lang, 'Messengers') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div id="agency-messengers-repeater">
                                                        <div class="repeater-list" data-repeater-list="agency[messengers]">
                                                            <?php if(isset($agency_messengers) && !empty($agency_messengers)){
                                                                foreach ($agency_messengers as $messenger){ ?>
                                                                    <div data-repeater-item class="row mb-3">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                                                <div class="input-group-prepend">
                                                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <?= !empty($contacts_types['messengers']) ? ucfirst(current($contacts_types['messengers'])['name']) : '' ?>
                                                                                    </button>
                                                                                    <div class="dropdown-menu">
                                                                                        <?php foreach ($contacts_types['messengers'] as $type) { ?>
                                                                                            <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" name="id" value="<?= $messenger['id'] ?>">
                                                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $messenger['type_id'] ?>">
                                                                                <input type="text" name="value" class="form-control m-input" value="<?= $messenger['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_site_input_placeholder', $lang, 'Enter site') ?>">
                                                                                <div class="input-group-append">
                                                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                        <i class="la la-close"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div data-repeater-item class="row mb-3">
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                                            <div class="input-group-prepend">
                                                                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <?= !empty($contacts_types['messengers']) ? ucfirst(current($contacts_types['messengers'])['name']) : '' ?>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                    <?php foreach ($contacts_types['messengers'] as $type) { ?>
                                                                                        <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <input type="hidden" name="id" value="">
                                                                            <input type="hidden" name="type_id" class="hidden-extra-option" value="">
                                                                            <input type="text" name="value" class="form-control m-input" value="" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_site_input_placeholder', $lang, 'Enter site') ?>">
                                                                            <div class="input-group-append">
                                                                                <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;<?= TranslationHelper::getTranslation('acc_set_agency_contacts_add_button', $lang, 'Add') ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_site_label', $lang, 'Site') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="agency[site]" class="form-control m-input" value="<?= (isset($agency['site']) && !empty($agency['site'])) ? $agency['site'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_messenger_input_placeholder', $lang, 'Enter site') ?>">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_adress_label', $lang, 'Adress') ?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div class="form-group m-form__group">
                                                        <input type="text" name="agency[city]" class="form-control m-input" value="<?= (isset($agency['city']) && !empty($agency['city'])) ? $agency['city'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_city_placeholder', $lang, 'Enter city') ?>">
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <input type="text" name="agency[street]" class="form-control m-input" value="<?= (isset($agency['street']) && !empty($agency['street'])) ? $agency['street'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_street_input_placeholder', $lang, 'Enter street name') ?>">
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <input type="number" name="agency[house]" class="form-control m-input" value="<?= (isset($agency['house']) && !empty($agency['house'])) ? $agency['house'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_house_input_placeholder', $lang, 'Enter house number') ?>">
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <textarea name="agency[description]" id="agency_adress__description_textarea" rows="3" class="form-control" placeholder="<?= TranslationHelper::getTranslation('acc_set_agency_contacts_description_textarea_placeholder', $lang, 'Enter adress description') ?>"><?= (isset($agency['address_description']) && !empty($agency['address_description'])) ? $agency['address_description'] : '' ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if(!empty($agency['logo'])): ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_logotype_label', $lang, 'Logotype') ?>
                                                </label>
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                    <img class="profile_picture" src="<?= Yii::$app->params['baseUrl'] . '/' . (!empty($agency['logo']) ? $agency['logo'] : 'img/profile_default.png') ?>" alt=""/>
                                                </label>
                                                <label class="col-sm-6 m--align-right">
                                                    <a href="#" class="btn btn-outline-danger m-btn m-btn--icon" id="delete-logo" data-action="delete">
                                                        <span>
                                                            <i class="la la-trash"></i>
                                                            <?= TranslationHelper::getTranslation('agency_delete_logo', $lang, 'Delete logo') ?>
                                                        </span>
                                                    </a>
                                                </label>
                                            </div>
                                            <?php endif;?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    <?php if(empty($agency['logo'])): ?>
                                                    * <?= TranslationHelper::getTranslation('acc_set_agency_contacts_logotype_label', $lang, 'Logotype') ?>
                                                    <?php endif;?>
                                                </label>
                                                <div class="col-xl-9 col-lg-9" id="agency_dropzone_div">
                                                    <div class="m-dropzone dropzone" id="m-dropzone">
                                                        <div class="m-dropzone__msg dz-message needsclick">
                                                            <h3 class="m-dropzone__msg-title">
                                                                <?= \app\helpers\TranslationHelper::getTranslation('acc_set_agency_contacts_logotype_label',$lang);?>
                                                            </h3>
                                                            <span class="m-dropzone__msg-desc">
                                                                <?= \app\helpers\TranslationHelper::getTranslation('acc_set_agency_contacts_logotype_dropzone_placeholder',$lang);?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--end: Form Wizard Step 2-->
                                <!--begin: Form Wizard Step 3-->
                                <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                    <form id="wizard_form_3">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                <?= TranslationHelper::getTranslation('acc_set_requisites_title', $lang, 'Requisites') ?>
                                            </h3>
                                            <ul class="head-tools-list">
                                                <li>
                                                    <i class="flaticon-info help_icon" data-toggle="modal" data-target="#requisites_help_modal"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="requisites_sets_container">
                                            <div class="m-accordion m-accordion--default" id="m_requisites_accordion" role="tablist">
                                            </div>
                                        </div>
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <div class="row" id="create_requisites_form">
                                            <div class="col-12">
                                                <div class="generated_requisites_form">
                                                    <div id="requisite-form-area">
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-12 text-right">
                                                            <button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon generated_requisites_form__submit edit_button">
                                                                <span>
                                                                    <i class="la la-check"></i><?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button id="add-set-button" type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon generated_requisites_form__submit">
                                                <span>
                                                    <?= TranslationHelper::getTranslation('acc_set_requisites_add_button', $lang, 'Add requisities') ?>
                                                </span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <!--end: Form Wizard Step 3-->
                                <!--begin: Form Wizard Step 4-->
                                <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                    <form id="wizard_form_4">
                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    <?= TranslationHelper::getTranslation('acc_set_reminders_title', $lang, 'Reminders') ?>
                                                </h3>
                                            </div>
                                            <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="form-control-label">
                                                        * <?= TranslationHelper::getTranslation('acc_set_reminders_email_label', $lang, 'Email') ?>:
                                                    </label>
                                                    <?=
                                                        Html::input(
                                                                'email',
                                                                'reminders_email',
                                                                $tenant->reminders_email,
                                                                [
                                                                    'class' => 'form-control m-input'
                                                                ])
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row m-form--group-seperator-dashed">
                                                <div class="col-lg-12">
                                                    <label class="form-control-label">
                                                        * <?= TranslationHelper::getTranslation('acc_set_reminders_from_label', $lang, 'From who') ?>:
                                                    </label>
                                                    <?=
                                                        Html::input(
                                                                'text',
                                                                'reminders_from_who',
                                                                $tenant->reminders_from_who,
                                                                [
                                                                    'class' => 'form-control m-input'
                                                                ])
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--end: Form Wizard Step 4-->
                            </div>
                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit form-submit-block">
                                <div class="m-form__actions wizard-submit-block">
                                    <div class="row">
                                        <div class="col-lg-12 m--align-right">
                                            <a href="#" class="btn btn-brand m-btn m-btn--custom m-btn--icon" id="form-wizard-submit" data-wizard-action="submit">
                                                <span>
                                                    <i class="la la-check"></i>
                                                    &nbsp;&nbsp;
                                                    <span>
                                                        <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-25">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-sm-8 m--align-left">
                                            <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                <span>
                                                    <i class="la la-arrow-left"></i>
                                                    &nbsp;&nbsp;
                                                    <span>
                                                        <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="col-sm-4 m--align-right">
                                            <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                <span>
                                                    <span>
                                                        <?= TranslationHelper::getTranslation('pagination_next', $lang, 'Next') ?>
                                                    </span>
                                                    &nbsp;&nbsp;
                                                    <i class="la la-arrow-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>