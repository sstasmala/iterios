<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('api_index_page_title', $lang, 'API');
$this->registerCssFile('@web/css/api/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/api/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    // Render modals here...
$this->endBlock();
?>

<div id="api_index_portlet" class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('api_index_portlet_title', $lang, 'ITA API key') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-12">
                <p>
                    Start building applications and integrations that use the same APIs that power the ITA tools.
                </p>
                <p>
                    Your ITA API key provides access to your ITA account and shouldn't be publicly shared. Each key is specific to an account, not a user. Only one key is allowed at a time. If needed, you can deactivate your ITA API key and get a new one.
                </p>
                <p>
                    Go to the ITA developer site to <a href="#" class="m-link m--font-bold">learn more</a>.
                </p>
            </div>
        </div>
        <hr class="customized">
        <div class="row mb-3">
            <div class="col-12">
                <div id="keygen_preloader">
                    <div class="m-loader m-loader--brand"></div>
                </div>
                <a href="#" class="btn btn-success" id="keygen_start">
                    <?= TranslationHelper::getTranslation('api_index_cgeate_new_key_btn', $lang, 'Create new key') ?>
                </a>
                <div id="generated_key" class="align-items-end row d-none">
                    <div class="col-sm-8 mb-3 mb-sm-0">
                        <label for="generated_key_input">
                            <?= TranslationHelper::getTranslation('api_index_generated_key_input_label', $lang, 'ITA API key') ?>
                        </label>
                        <div class="input-group">
                            <input type="text" id="generated_key_input" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('api_index_generated_key_input_placeholder', $lang, 'Generated key') ?>" aria-describedby="copy_key_button">
                            <div class="input-group-append">
                                <button class="btn btn-info" id="copy_key_button">
                                    <i class="far fa-copy mr-1"></i>
                                    <?= TranslationHelper::getTranslation('copy', $lang, 'Copy') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-sm-right">
                        <a href="#" class="btn btn-danger" id="key_deactivate">
                            <?= TranslationHelper::getTranslation('deactivate', $lang, 'Deactivate') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <hr class="customized">
        <div class="row">
            <div class="col-12">
                <h4 class="mb-3">
                    API keys log
                </h4>
                <div class="m_datatable" id="api_data_table"></div>
            </div>
        </div>
    </div>
</div>