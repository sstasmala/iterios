<?php
use app\helpers\TranslationHelper;
use yii\helpers\Url;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = trim(TranslationHelper::getTranslation('rv_page_step_' . 1 . '_title', $lang));
?>

<?php
/* Files */
$this->registerCssFile('@web/css/requests/view.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/requests/view/view.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/requests/view/step_' . 1 . '.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/editable-table-plugin/script.min.js', ['depends' => \app\assets\MainAsset::className()]);

$page_tree = require_once 'view_steps_conf.php';

/* Modals */

$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/requests/_m_modal_edit_contacts', [
    'request' => $request,
    'contact' => $request['contact'],
    'contacts_types' => $contacts_types
]);
echo $this->render('/partials/modals/requests/_m_modal_change_contacts', [
    'request' => $request,
    'contact' => $request['contact']
]);
echo $this->render('/partials/modals/requests/_m_modal_service_details', [
    'request' => $request,
    'services' => $services
]);
echo $this->render('/partials/modals/requests/_m_modal_select_service', [
    'services' => $services
]);
echo $this->render('/partials/modals/requests/_m_modal_change_cp_method');
echo $this->render('/partials/modals/requests/_m_modal_cp_method_sms');
echo $this->render('/partials/modals/requests/_m_modal_cp_method_email');
echo $this->render('/partials/modals/requests/_m_modal_cp_method_link');
$this->endBlock();
?>

<div class="row" id="request-detail-view">
    <div class="col-xxl-3 col-xl-4 col-lg-12">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-5 col-lg-5 col-xl-12 col-xxl-12 order-sm-1 order-lg-1">
                <?= $this->render('request_partlets/_request-preview', ['contact' => $request['contact'], 'contacts_types' => $contacts_types]) ?>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-12 col-xxl-12 order-sm-3 order-lg-2">
                <?= $this->render('request_partlets/_request-info', ['request' => $request, 'request_currency' => $request_currency, 'types' => $types]) ?>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-12 col-xxl-12 order-sm-4 order-lg-3">
                <?= $this->render('request_partlets/_request-description', ['request' => $request]) ?>
            </div>
            <div class="col-12 col-sm-6 col-md-7 col-lg-5 col-xl-12 col-xxl-12 order-sm-2 order-lg-3">
                <div class="row">
                    <div class="col-12">
                        <?= $this->render('request_partlets/_request-settings', ['request' => $request]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-9 col-xl-8 col-lg-12">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12">
                <?= $this->render('request_partlets/_request-services', ['linked_services'=>$linked_services,'values'=>$values]) ?>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12">
                <?= $this->render('request_partlets/_request-commercial-proposals', []) ?>
            </div>
            <div class="col-xxl-6 col-xl-12 col-lg-6 col-md-6">
                <?= $this->render('request_partlets/_request-notes', ['request' => $request,'requestNotes'=>$requestNotes]) ?>
            </div>
            <div class="col-xxl-6 col-xl-12 col-lg-6 col-md-6">
                <?= $this->render('request_partlets/_request-change-history', []) ?>
            </div>
        </div>
    </div>
</div>
