<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="opened_requests">
        <?= TranslationHelper::getTranslation('rc_filter_custom_filter_1', $lang, 'Opened requests') ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="only_my_requests">
        <?= TranslationHelper::getTranslation('rc_filter_custom_filter_2', $lang, 'Only my requests') ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="successfully_requests">
        <?= TranslationHelper::getTranslation('rc_filter_custom_filter_3', $lang, 'Successfully completed') ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="unrealized_requests">
        <?= TranslationHelper::getTranslation('rc_filter_custom_filter_4', $lang, 'Unrealized requests') ?>
    </a>
</div>



