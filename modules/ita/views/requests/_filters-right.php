<?php
use app\helpers\TranslationHelper;
use app\helpers\MCHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row filters-right-search">
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('rc_filter_accordion_title_1', $lang, 'Contact'); ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input name="RequestFilter[passport]" id="rc_filter_input_passport" class="m-input form-control" placeholder="<?= TranslationHelper::getTranslation('rc_filter_input_passport_placeholder', $lang, 'Passport series and number'); ?>">
                        </div>
                        <div class="form-group">
                            <input name="RequestFilter[last_name]" id="rc_filter_input_last_name" class="m-input form-control" placeholder="<?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name'); ?>">
                        </div>
                        <div class="form-group">
                            <input name="RequestFilter[first_name]" id="rc_filter_input_first_name" class="m-input form-control" placeholder="<?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name'); ?>">
                        </div>
                        <div class="form-group">
                            <input name="RequestFilter[phone]" id="rc_filter_input_phone" class="m-input form-control masked-input" placeholder="<?= TranslationHelper::getTranslation('field_phone', $lang, 'Phone'); ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                        <div class="form-group">
                            <input name="RequestFilter[email]" id="rc_filter_input_email" class="m-input form-control masked-input" placeholder="<?= TranslationHelper::getTranslation('email_title', $lang, 'Email'); ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="form-group">
                            <select name="RequestFilter[tags]" id="rc_filter_select_tags" class="form-control m-select2" multiple></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-accordion__item" style="overflow:visible;">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_2_head" data-toggle="collapse" href="#m_accordion_1_item_2_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('rc_filter_accordion_title_3', $lang, 'Request settings'); ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_2_body" role="tabpanel" aria-labelledby="m_accordion_1_item_2_head" data-parent="#m_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" name="RequestFilter[create_date_picker]" id="rc_filter_create_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('rc_filter_request_create_dates_picker_placeholder', $lang, 'Create dates') ?>">
                            <input type="hidden" name="RequestFilter[create_date_start]" id="rc_filter_create_date_picker_start">
                            <input type="hidden" name="RequestFilter[create_date_end]" id="rc_filter_create_date_picker_end">
                        </div>
                        <div class="form-group">
                            <input type="text" name="RequestFilter[update_date_picker]" id="rc_filter_update_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('rc_filter_request_update_dates_picker_placeholder', $lang, 'Update dates') ?>">
                            <input type="hidden" name="RequestFilter[update_date_start]" id="rc_filter_update_date_picker_start">
                            <input type="hidden" name="RequestFilter[update_date_end]" id="rc_filter_update_date_picker_end">
                        </div>
                        <div class="form-group">
                            <select name="RequestFilter[responsible]" id="rc_filter_select_responsible" class="form-control m-bootstrap-select"></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="m-accordion m-accordion--default" id="m_accordion_2" role="tablist">
            <div class="m-accordion__item" style="overflow:visible;">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_2_item_1_head" data-toggle="collapse" href="#m_accordion_2_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= TranslationHelper::getTranslation('rc_filter_accordion_title_2', $lang, 'Request data'); ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body show" id="m_accordion_2_item_1_body" role="tabpanel" aria-labelledby="m_accordion_2_item_1_head" data-parent="#m_accordion_2" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <select name="RequestFilter[country]" id="rc_filter_select_country" class="form-control m-bootstrap-select country-select-ajax" multiple></select>
                        </div>
                        <div class="form-group">
                            <select name="RequestFilter[departure_cityes]" id="rc_filter_select_departure_cityes" class="form-control m-bootstrap-select" multiple></select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="RequestFilter[departure_date_range]" id="rc_filter_select_departure_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('mcr_departure_date_datepicker_placeholder', $lang, 'Enter customer departure date') ?>">
                            <input type="hidden" name="RequestFilter[departure_date_start]" id="rc_filter_select_departure_date_picker_start">
                            <input type="hidden" name="RequestFilter[departure_date_end]" id="rc_filter_select_departure_date_picker_end">
                        </div>
                        <div class="form-group">
                            <?php
                                $main_currency_id = (isset(\Yii::$app->user->identity->tenant->main_currency)) ? \Yii::$app->user->identity->tenant->main_currency : null;
                                $main_currency_text = null;
                                if($main_currency_id){
                                    $mc_data = MCHelper::getCurrencyById($main_currency_id,$lang);
                                    $main_currency_text = (!empty($mc_data)) ? $mc_data[0]['iso_code'] : null;
                                }
                                $currencies_data = null;
                                if(isset(\Yii::$app->user->identity->tenant->usage_currency)){
                                    $usage_currencies = json_decode(\Yii::$app->user->identity->tenant->usage_currency);
                                    if(!empty($usage_currencies)){
                                        $usage_currencies_ids = implode(',', $usage_currencies);
                                        $currencies_data = MCHelper::getCurrencyById($usage_currencies_ids,$lang);
                                    }
                                }
                            ?>
                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-currency_id">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?= $main_currency_text ?>
                                    </button>
                                    <?php if(!is_null($currencies_data) && !empty($currencies_data)){ ?>
                                        <div class="dropdown-menu">
                                            <?php foreach ($currencies_data as $one_curr_type) { ?>
                                                <a class="dropdown-item" href="#" data-value="<?= $one_curr_type['id'] ?>"><?= $one_curr_type['iso_code'] ?></a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <input type="hidden" name="RequestFilter[currency_id]" id="rc_filter_select_currency_id" value="<?= $main_currency_id ?>" class="hidden-currency_id">
                                <input type="text" name="RequestFilter[currency_min]" id="rc_filter_input_currency_min" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_min_budget_input_paceholder', $lang, 'From') ?>">
                                <input type="text" name="RequestFilter[currency_max]" id="rc_filter_input_currency_max" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_max_budget_input_paceholder', $lang, 'To') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="" class="form-control m-input currency_mask" id="rc_number" placeholder="<?= TranslationHelper::getTranslation('rc_filter_request_number_input_placeholder', $lang, 'Request number') ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>