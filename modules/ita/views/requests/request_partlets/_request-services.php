<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet request-services" id="request-services-list-partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <label class="m-checkbox">
                    <input type="checkbox" id="check_all_serv_cbx_tag">
                    <span></span>
                </label>
                <span class="m-portlet__head-icon">
                    <i class="flaticon-tea-cup"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('rv_page_services_list_partlet_title', $lang, 'Services') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" id="add_commercial_proposal_btn" class="btn btn-outline-brand m-btn m-btn--icon">
                        <span>
                            <i class="fa fa-briefcase"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('rv_page_create_proposal_btn', $lang, 'Create proposal') ?>
                            </span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" class="btn btn-outline-brand m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_select_service">
                        <span>
                            <i class="la la-plus"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('rv_page_add_service_btn', $lang, 'Add service') ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="request-services-list-block">
        <?php foreach ($linked_services as $k => $service):?>
            <div class="service-item">
                <div class="row-1 mb-3">
                    <div class="checkbox-wrap">
                        <label class="m-checkbox" style="margin-bottom: 19px;">
                            <input type="checkbox" class="serv_cbx_tag cp_checkbox" name="add_services_in_commercial_proposals[]" value="<?= $k ?>">
                            <span></span>
                        </label>
                    </div>
                    <div class="title">
                        <a href="#" class="m-link m--font-bolder">
                            <?=$service['name']?>
                        </a>
                    </div>
                    <div class="actions m--font-bold">
                        <div class="buttons">
                            <i class="la la-pencil mr-1 edit-service" title="<?= TranslationHelper::getTranslation('edit', $lang, 'Edit') ?>" data-id="<?=$k?>" data-toggle="modal" data-target="#m_modal_service_details"></i>
                            <i class="la la-close mr-1 remove-service" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>" data-id="<?=$k?>"></i>
                        </div>
                        <?php
                        $price = \app\utilities\services\ServicesUtility::getServicePrice($service);

                        $currency = \app\utilities\services\ServicesUtility::getServiceCurrency($service);
                        ?>
                        <div class="price ml-2">
                            <span class="value"><?=$price?></span>&nbsp;<span class="currency">
                                <?=($currency!="")?$currency['text']:""?></span>
                        </div>
                    </div>
                </div>
                <div class="row-2 mb-3">
                    <?php
                    $header = \app\utilities\services\ServicesUtility::getServiceHeader($service,$currency);
                    ?>
                        <span class="m--font-bold">
                            <?=$header?>
                        </span>
                </div>
                <div class="row-3 mb-3">
                    <?php
                    $supplier = '';
                    $suppliers =  array_filter($service['fields'],function($var){
                        return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
                    });
                    $suppliers = array_merge($suppliers, array_filter($service['fields_def'],function($var){
                        return $var['type'] == \app\models\ServicesFieldsDefault::TYPE_SUPPLIERS;
                    }));
                    if(!empty($suppliers) && isset(reset($suppliers)['value'])) {
                        if(isset($values['suppliers'][reset($suppliers)['value']['id']]))
                            $supplier = $values['suppliers'][reset($suppliers)['value']['id']];
                    }
                    ?>
                    <?php if($supplier != ""):?>
                    <div class="supplier mr-3">
                        <div class="logo mr-2">
                            <img src="<?=Yii::$app->params['baseUrl'].'/'.$supplier['logo']?>"
                                 alt="<?=$supplier['name']?>" title="<?=$supplier['name']?>">
                        </div>
                        <div class="title m--font-bold">
                            <?=$supplier['name']?>
                        </div>
                    </div>
                    <?php endif;?>
                    <?php
                    $additional =  $service['links'];
                    ?>
                    <?php if(!empty($additional)):?>
                    <div class="icons">
                        <?php foreach ($additional as $a):?>
                            <?php if($a['id']==$service['id']) continue;?>
                        <i class="<?=$a['icon']?> mr-1" data-toggle="m-popover" data-placement="top" data-content="<?=$a['name']?>"></i>
                        <?php endforeach;?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach;?>
        <?php if(empty($linked_services)):?>
            <div class="row">
                <div class="col-12 text-center">
                    <span>No data</span>
                </div>
            </div>
        <?php endif;?>
        </div>
    </div>
</div>