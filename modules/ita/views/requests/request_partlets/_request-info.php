<?php

use app\models\RequestCanceledReasons;
use app\models\Tenants;
use yii\helpers\Html;
use app\helpers\TranslationHelper;
use app\helpers\DateTimeHelper;
use app\helpers\MCHelper;

echo $this->render('_request_change_status.php', ['request' => $request]);

$lang = \Yii::$app->user->identity->tenant->language->iso;
$country_by_id = !empty($request['requestCountries']) ? \app\helpers\MCHelper::getCountryById(array_column($request['requestCountries'], 'country'), $lang) : [];
$city_by_id = !empty($request['requestCities']) ? \app\helpers\MCHelper::getDepartureCityById(array_column($request['requestCities'], 'city'),$lang) : [];
$hotel_by_id = !empty($request['requestHotelCategories']) ? \app\helpers\MCHelper::getHotelCategoryById(array_column($request['requestHotelCategories'], 'hotel_category'),$lang) : [];
$children_list_age = !empty($request['tourists_children_age'])  ? json_decode($request['tourists_children_age'], true) : [];
$children_ages_list = implode(', ', $children_list_age);
$hotels_categories = MCHelper::searchHotelCategory('', Yii::$app->user->identity->tenant->language->iso);
$allCurrency = array_column(MCHelper::getCurrency(\Yii::$app->user->identity->tenant->language->iso), 'iso_code', 'id');
?>
<div class="m-portlet m-portlet--responsive-mobile request-info m-portlet--head-sm" m-portlet="true" id="request-info-partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-user"></i>
                </span>
                <h3 class="m-portlet__head-text">
                   <?= TranslationHelper::getTranslation('rv_page_title_info', $lang, 'Request info ') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
                        <i class="la la-angle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="request-info-table-block">
            <table class="table table-striped m-table">
                <tbody>

                <tr>
                    <td><?= TranslationHelper::getTranslation('request_status', $lang, 'Status') ?></td>
                    <td>
                        <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1" id="change-request-status">
                            <a href="#" class="m-dropdown__toggle request_status_text btn btn-sm btn-<?=(isset($request['requestStatus']))? $request['requestStatus']['color_type']:'' ?> m-badge--wide"  >
                                <?=(isset($request['requestStatus']))? $request['requestStatus']['name'] :'<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';
                                ?>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <form class="etitable-form" >
                                                <div class="row align-items-center">
                                                    <div class="col-12 mb-1 labels">
                                                        <h6><?= TranslationHelper::getTranslation('request_status', $lang, 'Status') ?></h6>
                                                    </div>
                                                    <div class="col-8 inputs">
                                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                        <input name="request_id" type="hidden" value="<?= $request['id'] ?>">
                                                        <select name="request_status" id="requestStatus" class="form-control m-input">
                                                            <?php foreach (\app\models\RequestStatuses::find()->where(['type' => ['working','canceled','sold']])->translate($lang)->all() as $status){
                                                                echo "<option value='".$status->type."'>".$status->name."</option>";
                                                            }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-4 buttons text-right">
                                                        <button type="button" class="btn btn-sm btn-primary editable-submit" id="save_status"><i class="fa fa-check"></i></button>
                                                        <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="preloader" style="display:none;text-align:center;">
                                                <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
                    if (isset($request['requestCanceledReason'])){
                        echo '<tr><td>'. TranslationHelper::getTranslation('requests_canceled_reason', $lang, 'Canceled Reason').'</td><td><div class="reason_div_help">';

                        if (null === $request['requestCanceledReason']['description']) {
                            echo $request['requestCanceledReason']['name'];
                        } else {
                            echo $request['requestCanceledReason']['description'];
                        }

                        echo '</div></td></tr>';
                    }
                ?>

                    <tr>
                        <td><?= TranslationHelper::getTranslation('country', $lang, 'Country') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><span class="no-data">
                                        <?php if (!empty($country_by_id)):?>
                                        <?php foreach($country_by_id as $country){ echo $country['title'] . ' '; }?>
                                        <?php else: ?>
                                            <span class="no-data"><?=  TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                                        <?php endif; ?>
                                    </span></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('country', $lang, 'Country') ?>:</h6>
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="countries">
                                                            <select class="form-control m-select2" id="tags_search_country" name="Request[countries]" multiple="multiple">
                                                            </select>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('city_select_label', $lang, 'City of departure') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><span class="no-data">
                                        <?php if (!empty($city_by_id)):?>
                                            <?php foreach($city_by_id as $city){ echo $city['title'] . ' '; }?>
                                        <?php else: ?>
                                            <span class="no-data"><?=  TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                                        <?php endif; ?>
                                    </span></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('city_select_label', $lang, 'City of departure') ?></h6>
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="cities">
                                                            <div class="form-group">
                                                                <select name="Requests[cities]" id="rc_filter_select_departure_cities" class="form-control m-bootstrap-select" multiple>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr> <tr>
                        <td><?= TranslationHelper::getTranslation('departure_date_datepicker_label', $lang, 'Departure date') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">
                                    <?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_start']) && \app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_end']))) ? \app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_start']) . ' - ' . \app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_end']) : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';?>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="departureDate">
                                                            <h6><?= TranslationHelper::getTranslation('min_budget_input_placeholder', $lang, 'From') ?></h6>
                                                            <div class="input-group pull-right">
                                                                <input type="text" name="value_start" id="contact-info-table-departure_date_first" value="<?= \app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_start']) ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-date_of_birth_placeholder', $lang, 'Set new date...') ?>" class="form-control m-input "/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                                </div>
                                                            </div>
                                                            <h6><?= TranslationHelper::getTranslation('max_budget_input_placeholder', $lang, 'To') ?></h6>
                                                            <div class="input-group pull-right">
                                                                <input type="text" name="value_end" id="contact-info-table-departure_date_second" value="<?= \app\helpers\DateTimeHelper::convertTimestampToDate($request['departure_date_end']) ?>" placeholder="<?= TranslationHelper::getTranslation('info-editable-table-date_of_birth_placeholder', $lang, 'Set new date...') ?>" class="form-control m-input "/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>  <tr>
                        <td><?= TranslationHelper::getTranslation('trip_duration_inputs_label', $lang, 'Trip duration') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($request['trip_duration_start'] && $request['trip_duration_end'])) ? $request['trip_duration_start'] . ' - ' . $request['trip_duration_end'] . ' ' . TranslationHelper::getTranslation('rv_page_nights_count', $lang, 'nights') : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="tripDuration">
                                                            <div class="div_help">
                                                                <h6><?= TranslationHelper::getTranslation('trip_min_days_placeholder', $lang, 'Min') ?></h6>
                                                                <input min="1" name="trip_duration_start" placeholder="<?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?>" type="number" value="<?= (!empty($request['trip_duration_start'])) ? $request['trip_duration_start'] : '' ?>" class="form-control m-input">
                                                            </div>
                                                            <div class="div_help">
                                                                <h6><?= TranslationHelper::getTranslation('trip_max_days_placeholder', $lang, 'Max') ?></h6>
                                                                <input min="1" name="trip_duration_end" placeholder="<?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?>" type="number" value="<?= (!empty($request['trip_duration_end'])) ? $request['trip_duration_end'] : '' ?>" class="form-control m-input">
                                                            </div>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('hotel_category_select_label', $lang, 'Hotel category') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">
                                    <?php if (!empty($hotel_by_id)):?>
                                        <?php foreach ($hotel_by_id as $hotel) { !($hotel['id'] == 7 || $hotel['id'] == 8 || $hotel['id'] == 9 || $hotel['id'] == 10) ? $star = '*' : $star = ''; echo $hotel['title'] . $star . ' ';}?>
                                    <?php else: ?>
                                        <span class="no-data"><?=  TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                                    <?php endif; ?>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('hotel_category_select_label', $lang, 'Hotel category') ?></h6>
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="hotels">
                                                            <?php $all_hotels = array_column($hotel_by_id,'id');?>
                                                            <select class="form-control m-bootstrap-select m_selectpicker" name="Requests[hotel_category]" multiple>
                                                                <?php foreach($hotels_categories as $category_key => $category){ ?>
                                                                    <option value="<?= $category['id'] ?>" <?= (in_array($category['id'],$all_hotels))? 'selected' : '';?>>   <?= $category['title'] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr><tr>
                        <td><?= TranslationHelper::getTranslation('budget_inputs_label', $lang, 'Budget') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= (!empty($request['budget_from'] && $request['budget_to'] && $request['budget_currency'])) ? $request['budget_from'] . ' - ' . $request['budget_to'] . ' ' . $allCurrency[$request['budget_currency']] : '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="budget">
                                                            <div class="div_help">
                                                            <h6><?= TranslationHelper::getTranslation('rv_page_currency_label', $lang, 'Currency') ?></h6>
                                                            <select name="currency" id="currency_count_select" class="form-control m-bootstrap-select m_selectpicker">
                                                                <?php if(!empty($request['budget_currency'])) {
                                                                    foreach($allCurrency as $currency){
                                                                        ($allCurrency[$request['budget_currency']] == $currency)? $selected = 'selected' : $selected = '' ?>
                                                                        <option value="<?= array_search($currency,$allCurrency) ?>" <?=$selected ?>><?= $currency ?></option>
                                                                    <?php }
                                                                } else { ?>
                                                                    <option class="bs-title-option" value=""><?= TranslationHelper::getTranslation('rv_page_currency_placeholder', $lang, 'Select currency') ?></option>
                                                                    <?php foreach($allCurrency as $currency) { ?>
                                                                        <?php ($request['budget_currency'] == $currency)? $selected = 'selected' : $selected = '' ?>
                                                                        <option value="<?= array_search($currency,$allCurrency) ?>" <?=$selected ?>><?= $currency ?></option>
                                                                    <?php }
                                                                } ?>
                                                            </select>
                                                            </div>
                                                            <div class="div_help">
                                                                <h6><?= TranslationHelper::getTranslation('min_budget_input_placeholder', $lang, 'From') ?></h6>
                                                                <input min="1" name="budget_from" placeholder="<?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?>" type="number" value="<?= (!empty($request['budget_from'])) ? $request['budget_from'] : '' ?>" class="form-control m-input">
                                                            </div>
                                                            <div class="div_help">
                                                                <h6><?= TranslationHelper::getTranslation('max_budget_input_placeholder', $lang, 'To') ?></h6>
                                                                <input min="1" name="budget_to" placeholder="<?= TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?>" type="number" value="<?= (!empty($request['budget_to'])) ? $request['budget_to'] : '' ?>" class="form-control m-input">
                                                            </div>

                                                        </div>
                                                            <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                        </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr><tr>
                        <td><?= TranslationHelper::getTranslation('mcr_adults_person_count_label', $lang, 'Adults persons count') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;"><?= $request['tourists_adult_count']. ' ' . TranslationHelper::getTranslation('request_adult_reduction', $lang, 'ad.'); ?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                            <h6><?= TranslationHelper::getTranslation('mcr_adults_person_count_label', $lang, 'Adults persons count') ?></h6>
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="adultsCount">
                                                            <input type="number" min="1" name="value" value="<?= $request['tourists_adult_count']; ?>" class="form-control m-input"/>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('children_count_label', $lang, 'Сhildren count') ?></td>
                        <td>
                            <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">
                                    <?= (!empty($request['tourists_children_count'])) ? ($request['tourists_children_count'] . ' ' . TranslationHelper::getTranslation('request_children_reduction', $lang, 'ch.') .  ' ( ' . $children_ages_list . ' ) '  ) :
                                        '<span class="no-data">' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</span>';?></a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <form class="etitable-form">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 mb-1 labels">
                                                        </div>
                                                        <div class="col-8 inputs">
                                                            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                            <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                            <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                            <input name="property" type="hidden" value="childrenCount">
                                                            <div class="div_help">
                                                                <div class='reason_div_help'>
                                                                <label>
                                                                    <?= TranslationHelper::getTranslation('mcr_children_count_label', $lang, 'Сhildren count') ?>:
                                                                </label>
                                                                </div>
                                                                <select name="Requests[children_count]" id="children_count_select" class="form-control m-bootstrap-select m_selectpicker">
                                                                    <?php if($request['tourists_children_count'] == 0)  {
                                                                        for ($i = 0; $i <= 10; $i++) { ?>
                                                                            <option value="<?= $i ?>"<?= ($i === 0) ? ' selected' : '' ?>><?= $i ?></option>
                                                                        <?php }
                                                                    } else {
                                                                        for ($i = 0; $i <= 10; $i++) { ?>
                                                                            <option value="<?= $i ?>"<?= ($i === $request['tourists_children_count']) ? ' selected' : '' ?>><?= $i ?></option>
                                                                        <?php }
                                                                    } ?>
                                                                </select>
                                                            </div>
                                                            <div class="input_blocks_children_age">
                                                                <div class="row children-age<?= (!empty($children_list_age) ? ' has_children_ages' : ' no_children_ages') ?>">
                                                                    <div class="col-12">
                                                                        <div class='reason_div_help'>
                                                                        <label><?= TranslationHelper::getTranslation('mcr_children_age_label', $lang, 'Children age') ?>:</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="row generated_inputs_block">
                                                                            <!-- Generated inuts -->
                                                                            <?php if (!empty($children_list_age)): ?>
                                                                                <?php foreach ($children_list_age as $age): ?>
                                                                                    <div class="col-4 child-age-col mb-2" >
                                                                                        <input type="text" name="Requests[children_age][]" class="form-control m-input age_mask" maxlength="2" value="<?= $age ?>">
                                                                                    </div>
                                                                                <?php endforeach; ?>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 buttons text-right">
                                                            <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                            <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="preloader" style="display:none;text-align:center;">
                                                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr><tr>
                    <td><?= TranslationHelper::getTranslation('source', $lang, 'Source') ?></td>
                    <td>
                        <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" m-dropdown-persistent="1">
                            <a href="#" class="m-dropdown__toggle" style="text-decoration:underline;text-decoration-style:dotted;">

                                <?php if (!empty($request['requestType']['value'])):?>
                                    <?= $request['requestType']['value']?>
                                <?php else: ?>
                                    <span class="no-data"><?=  TranslationHelper::getTranslation('not_set', $lang, 'Not set') ?></span>
                                <?php endif; ?>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <form class="etitable-form">
                                                <div class="row align-items-center">
                                                    <div class="col-12 mb-1 labels">
                                                        <h6><?= TranslationHelper::getTranslation('source', $lang, 'Source') ?></h6>
                                                    </div>
                                                    <div class="col-8 inputs">
                                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                        <input name="ajax_url" type="hidden" value="/requests/set-param-ajax">
                                                        <input name="id" type="hidden" value="<?= $request['id'] ?>">
                                                        <input name="property" type="hidden" value="type">
                                                        <select class="form-control m-bootstrap-select m_selectpicker text_req_type" name="Requests[type]">
                                                            <?php foreach($types as $type) {?>
                                                                <option value="<?= $type['id'] ?>" <?= $request['request_type_id'] == $type['id'] ? ' selected' : '' ?>><?= $type['value'] ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-4 buttons text-right">
                                                        <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                        <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="preloader" style="display:none;text-align:center;">
                                                <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
