<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile request-settings">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-cogwheel-2"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('requests_field_settings', $lang, 'Settings') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="select-responsible-block">
            <p>
                <?= TranslationHelper::getTranslation('requests_field_filter_task_responsible', $lang, 'Responsible') ?>
            </p>
            <input type="hidden" id="select2-responsible-ajax-search-request-id-hint" value="<?= $request['id'] ?>">
            <select class="form-control m-bootstrap-select" id="select2-responsible-ajax-search" name="request_responsible">
                <?php if(!empty($request['responsible'])){
                    $full_name = '';
                    $full_name .= ($request['responsible']['first_name']) ? $request['responsible']['first_name'] . ' ' : '';
                    $full_name .= ($request['responsible']['middle_name']) ? $request['responsible']['middle_name'] . ' ' : '';
                    $full_name .= ($request['responsible']['last_name']) ? $request['responsible']['last_name'] . ' ' : '';
                    $full_name = trim($full_name);
                    echo '<option value="' . $request['responsible']['id'] . '">' . $full_name . '</option>';
                } else {
                    echo '<option value="" selected>' . TranslationHelper::getTranslation('not_set', $lang, 'Not set') . '</option>';
                } ?>
            </select>
        </div>
        <hr>
        <div class="request-refresh-dates-block">
            <table>
                <tbody>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('requests_field_created', $lang, 'Created') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $request['created_at']?></span>
                        </td>
                    </tr>
                    <tr>
                        <td><?= TranslationHelper::getTranslation('requests_field_updated', $lang, 'Updated') ?></td>
                        <td>
                            <span class="moment-js invisible" data-format="unix"><?= $request['updated_at']?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="request-delete-block">
            <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/requests/delete?id=' . $request['id'] ?>" id="request-view-delete-form" method="POST">
                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                <button type="submit" class="btn btn-danger m-btn m-btn--custom m-btn--icon">
                    <span>
                        <i class="fa fa-trash"></i>
                        <span><?= TranslationHelper::getTranslation('requests_field_delete', $lang, 'Delete') ?></span>
                    </span>
                </button>
            </form>
        </div>
    </div>
</div>