<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;
use app\models\RequestCanceledReasons;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="request-change-status-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('why_close', $lang, 'Why close') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"
                  id="change-status-form"
                  action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/requests/set-canceled-reasons?request_id='.$request['id']; ?>"
                  method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label"><?= TranslationHelper::getTranslation('reason', $lang, 'Reason') ?></label>
                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <select name="reason_close" id="reason_close" class="form-control">
                            <?php
                            $allReason = RequestCanceledReasons::find()
                                ->where(['type'=>RequestCanceledReasons::TYPE_SYSTEM])
                                ->translate(\Yii::$app->user->identity->tenant->language->iso)
                                ->all();
                            foreach ($allReason as $reason){
                                echo "<option value='".$reason->id."'>".$reason->name."</option>";
                            }?>
                        </select>
                    </div>
                    <div class="form-group other_reason_group">
                        <label for="recipient-name" class="form-control-label"><?= TranslationHelper::getTranslation('reason', $lang, 'Reason') ?></label>
                        <textarea name="other_reason" id="other_reason" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                    </button>
                    <button type="submit" class="btn btn-primary" id="save_cancel_reason">
                        <?= TranslationHelper::getTranslation('save_changes_button', $lang, 'Save') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
