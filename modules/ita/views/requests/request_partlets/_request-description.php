<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="request-description-partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-share"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('description', $lang, 'Description') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
                        <i class="la la-angle-down"></i>
                    </a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                        <i class="la la-expand"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body" id="decription_edit">
        <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/requests/set-description' ?>" method="POST">
            <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
            <?= Html::hiddenInput('id', $request['id'], []) ?>
            <textarea name="description" id="request_create__description_textarea" class="form-control mb-3" rows="5"
            ><?= (isset($request['request_description']) && !empty($request['request_description'])) ? $request['request_description'] : '' ?></textarea>
            <div class="row description_save_block">
                <div class="col-12 text-right">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>&nbsp;<i class="fa fa-check"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
