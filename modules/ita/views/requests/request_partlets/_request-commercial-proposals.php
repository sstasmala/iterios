<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="m-portlet m-portlet--responsive-mobile" m-portlet="true" id="request-commercial-proposals-partlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-notes"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('request_cpp_title', $lang, 'Сommercial proposals') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table m-table m-table--head-bg-success" id="commercial-proposals-table">
            <thead>
                <tr>
                    <th>
                        <?= TranslationHelper::getTranslation('request_cpp_table_head_name', $lang, 'Name') ?>
                    </th>
                    <th>
                        <?= TranslationHelper::getTranslation('request_cpp_table_head_type', $lang, 'Type') ?>
                    </th>
                    <th>
                        <?= TranslationHelper::getTranslation('request_cpp_table_head_status', $lang, 'Status') ?>
                    </th>
                    <th>
                        <?= TranslationHelper::getTranslation('request_cpp_table_head_click_map', $lang, 'Click map') ?>
                    </th>
                    <th class="text-center">
                        <?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="#" class="m-link m--font-bold">Рассылка №1</a><br>
                        <span>25.04.2018</span>
                    </td>
                    <td>
                        <span class="m--font-bold">Email</span>
                    </td>
                    <td>
                        <div>
                            <span class="m-badge m-badge--warning m-badge--dot"></span>
                            <span class="m--font-bold m--font-warning ml-1">Отправлено</span>
                        </div>
                        <div>
                            <span class="m--font-bold m--font-warning ml-3">27.02 в 23:12</span>
                        </div>
                    </td>
                    <td>
                        <span class="m--font-metal">
                            <?= TranslationHelper::getTranslation('request_cpp_table_no_clicks_message', $lang, 'No clicks') ?>
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" class="m-link m--font-bold">Рассылка №2</a><br>
                        <span>25.04.2018</span>
                    </td>
                    <td>
                        <span class="m--font-bold">Sms</span>
                    </td>
                    <td>
                        <div>
                            <span class="m-badge m-badge--info m-badge--dot"></span>
                            <span class="m--font-bold m--font-info ml-1">Доставлено</span>
                        </div>
                        <div>
                            <span class="m--font-bold m--font-info ml-3">27.02 в 23:12</span>
                        </div>
                    </td>
                    <td>
                        <span class="m--font-metal">
                            <?= TranslationHelper::getTranslation('request_cpp_table_no_clicks_message', $lang, 'No clicks') ?>
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" class="m-link m--font-bold">Рассылка №3</a><br>
                        <span>25.04.2018</span>
                    </td>
                    <td>
                        <span class="m--font-bold">Ссылка</span>
                    </td>
                    <td>
                        <div>
                            <span class="m-badge m-badge--success m-badge--dot"></span>
                            <span class="m--font-bold m--font-success ml-1">Открыто</span>
                        </div>
                        <div>
                            <span class="m--font-bold m--font-success ml-3">27.02 в 23:12</span>
                        </div>
                    </td>
                    <td>
                        <div href="#" class="m-link" data-container="body" data-toggle="m-popover" data-html="true" data-placement="top" data-content="Дата: 12.05.2018<br>IP: 127.0.0.1<br>Город: New York">
                            https://google.com
                        </div>
                    </td>
                    <td class="text-center">
                        <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" class="m-link m--font-bold">Рассылка №4</a><br>
                        <span>25.04.2018</span>
                    </td>
                    <td>
                        <span class="m--font-bold">Ссылка</span>
                    </td>
                    <td>
                        <div>
                            <span class="m-badge m-badge--brand m-badge--dot"></span>
                            <span class="m--font-bold m--font-brand ml-1">Клик</span>
                        </div>
                        <div>
                            <span class="m--font-bold m--font-brand ml-3">27.02 в 23:12</span>
                        </div>
                    </td>
                    <td>
                        <div href="#" class="m-link" data-container="body" data-toggle="m-popover" data-html="true" data-placement="top" data-content="Дата: 12.05.2018<br>IP: 127.0.0.1<br>Город: New York">
                            https://trello.com
                        </div>
                        <br>
                        <div href="#" class="m-link" data-container="body" data-toggle="m-popover" data-html="true" data-placement="top" data-content="Дата: 12.05.2018<br>IP: 127.0.0.1<br>Город: New York">
                            https://trello.com
                        </div>
                        <br>
                        <div href="#" class="m-link" data-container="body" data-toggle="m-popover" data-html="true" data-placement="top" data-content="Дата: 12.05.2018<br>IP: 127.0.0.1<br>Город: New York">
                            https://trello.com
                        </div>
                    </td>
                    <td class="text-center">
                        <a href="#" class="target-link-id m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
