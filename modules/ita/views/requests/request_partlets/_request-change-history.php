<?php
use app\helpers\TranslationHelper;
?>
<div class="m-portlet request-change-history">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-tool"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Change history
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-scrollable mCustomScrollbar _mCS_5 mCS-autoHide" data-scrollbar-shown="true" data-scrollable="true" data-max-height="380" style="overflow: visible; height: 380px; max-height: 380px; position: relative;">
            <div class="m-timeline-2 customized">
                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                    <div class="m-timeline-2__item m--margin-top-30">
                        <span class="m-timeline-2__item-time moment-js invisible" data-format="unix" style="font-size: 1.1rem;">
                            1520340638
                        </span>
                        <div class="m-timeline-2__item-cricle">
                            <i class="fa fa-genderless m--font-warning"></i>
                        </div>
                        <div class="m-timeline-2__item-text">
                            main_message
                        </div>
                    </div>
                </div>
                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                    <div class="m-timeline-2__item m--margin-top-30">
                        <span class="m-timeline-2__item-time moment-js invisible" data-format="unix" style="font-size: 1.1rem;">
                            1520340638
                        </span>
                        <div class="m-timeline-2__item-cricle">
                            <i class="fa fa-genderless m--font-warning"></i>
                        </div>
                        <div class="m-timeline-2__item-text">
                            main_message
                        </div>
                    </div>
                </div>
                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                    <div class="m-timeline-2__item m--margin-top-30">
                        <span class="m-timeline-2__item-time moment-js invisible" data-format="unix" style="font-size: 1.1rem;">
                            1520340638
                        </span>
                        <div class="m-timeline-2__item-cricle">
                            <i class="fa fa-genderless m--font-warning"></i>
                        </div>
                        <div class="m-timeline-2__item-text">
                            main_message
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>