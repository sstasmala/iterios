<?php
use app\helpers\TranslationHelper;

return [
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_1_title', $lang, 'Change tour'),
        'tabs' => [
            [
                'name' => TranslationHelper::getTranslation('rv_page_tab_1_1_title', $lang, 'Request'),
                'icon_class' => 'la la-cog',
                'content' => [
                    'view_name' => '_request-data',
                    'params' => [
                        'request' => $request,
                        'linked_services' => $linked_services,
                        'values' => $values,
                        'request_currency' => $request_currency,
                        'contacts_types' => $contacts_types,
                        'requestNotes' => $requestNotes,
                    ]
                ]
            ],
            [
                'name' => TranslationHelper::getTranslation('rv_page_tab_1_2_title', $lang, 'Bid'),
                'icon_class' => 'la la-briefcase',
                'content' => [
                    'view_name' => '_bids',
                    'params' => []
                ]
            ],
            [
                'name' => TranslationHelper::getTranslation('rv_page_tab_1_3_title', $lang, 'Tourist and finances'),
                'icon_class' => 'la la-bell-o',
                'content' => [
                    'view_name' => '_tourist-and-finances',
                    'params' => []
                ]
            ]
        ],
        'modals' => [
            [
                'view_name' => '_m_modal_edit_contacts',
                'params' => [
                    'request' => $request,
                    'contact' => $request['contact'],
                    'contacts_types' => $contacts_types
                ]
            ],
            [
                'view_name' => '_m_modal_select_service',
                'params' => [
                    'services' => $services
                ]
            ],
            [
                'view_name' => '_m_modal_service_details',
                'params' => [
                    'request' => $request,
                    'services' => $services
                ]
            ]
        ]
    ],
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_2_title', $lang, 'Sign the contract'),
        'tabs' => []
    ],
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_3_title', $lang, 'Get paid'),
        'tabs' => []
    ],
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_4_title', $lang, 'Reservation'),
        'tabs' => []
    ],
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_5_title', $lang, 'Pay to the supplier'),
        'tabs' => []
    ],
    [
        'step_name' => TranslationHelper::getTranslation('rv_page_step_6_title', $lang, 'Issue documents'),
        'tabs' => []
    ]
];
