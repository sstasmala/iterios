<?php
/**
 * @var $this \yii\web\View
 * @var $data array
 */
$this->title = "Dashboard";
$this->registerCSSFile('@web/css/dashboard/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/dashboard/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php $this->beginBlock('subheader_controls'); ?>
    <?= $this->render('filters/_subheader_controls'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-left'); ?>
    <?= $this->render('filters/_filters-left'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-right'); ?>
    <?= $this->render('filters/_filters-right'); ?>
<?php $this->endBlock();?>

<div class="m-portlet">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-12 col-lg-6 col-xl-3">
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            Tourists
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                            Number of tourists
                        </span>
                        <span class="m-widget24__stats m--font-brand">
                            <?= $data['number_of_tourists'] ?>
                        </span>
                        <div class="m--space-10"></div>
                        <span class="m-widget24__number">
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            Requests
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                            Number of requests
                        </span>
                        <span class="m-widget24__stats m--font-info">
                            <?= $data['number_of_requests'] ?>
                        </span>
                        <div class="m--space-10"></div>
                        <span class="m-widget24__number">
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            Deals

                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                            Amount of deals

                        </span>
                        <span class="m-widget24__stats m--font-danger">
                            <?= $data['amount_of_deals'] ?>
                        </span>
                        <div class="m--space-10"></div>
                        <span class="m-widget24__number">
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            Bookings
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                            Number of bookings
                        </span>
                        <span class="m-widget24__stats m--font-success">
                            <?= $data['number_of_bookings'] ?>
                        </span>
                        <div class="m--space-10"></div>
                        <span class="m-widget24__number">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dashboard_sortable_portlets">
    <div class="m-portlet m-portlet--mobile m-portlet--sortable full-mode">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-thumb-tack m--font-success"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-success">
                        1
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                            <i class="la la-tint"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the
            printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
        </div>
        <i class="resizeble-icon fas fa-caret-down"></i>
    </div>
    <div class="m-portlet m-portlet--mobile m-portlet--sortable full-mode">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-thumb-tack m--font-success"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-success">
                        2
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                            <i class="la la-tint"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the
            printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
        </div>
        <i class="resizeble-icon fas fa-caret-down"></i>
    </div>
    <div class="m-portlet m-portlet--mobile m-portlet--sortable full-mode">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-thumb-tack m--font-success"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-success">
                        3
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                            <i class="la la-tint"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the
            printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
        </div>
        <i class="resizeble-icon fas fa-caret-down"></i>
    </div>
    <div class="m-portlet m-portlet--mobile m-portlet--sortable full-mode">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-thumb-tack m--font-success"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-success">
                        4
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                            <i class="la la-tint"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the
            printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
        </div>
        <i class="resizeble-icon fas fa-caret-down"></i>
    </div>
</div>