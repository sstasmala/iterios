<div class="row">
    <div class="col-lg-6 filters-right-search">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Deals properties
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" placeholder="Deal name">
                        </div>
                        <div class="form-group">
                            <div class='input-group' id='m_daterangepicker_1'>
                                <input type='text' class="form-control m-input"  placeholder="Select date range"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_stage_select2" name="param" multiple="multiple">
                                <option value="opt_1" selected>
                                    All stages
                                </option>
                                <option value="opt_2">
                                    Stage 2
                                </option>
                                <option value="opt_3">
                                    Stage 3
                                </option>
                                <option value="opt_4">
                                    Stage 4
                                </option>
                                <option value="opt_5">
                                    Stage 5
                                </option>
                                <option value="opt_6">
                                    Stage 6
                                </option>
                                <option value="opt_7">
                                    Stage 7
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_crated_by_select2" name="param" multiple="multiple">
                                <option value="0" selected>
                                    All users
                                </option>
                                <optgroup label="Department 1">
                                    <option value="1">
                                        Kolya
                                    </option>
                                    <option value="2">
                                        Vasya
                                    </option>
                                </optgroup>
                                <optgroup label="Department 2">
                                    <option value="3">
                                        Misha
                                    </option>
                                    <option value="4">
                                        Nikolay
                                    </option>
                                    <option value="5">
                                        Dima
                                    </option>
                                    <option value="6">
                                        Valentin
                                    </option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_update_by_select2" name="param" multiple="multiple">
                                <option value="0" selected>
                                    All users
                                </option>
                                <optgroup label="Department 1">
                                    <option value="1">
                                        Kolya
                                    </option>
                                    <option value="2">
                                        Vasya
                                    </option>
                                </optgroup>
                                <optgroup label="Department 2">
                                    <option value="3">
                                        Misha
                                    </option>
                                    <option value="4">
                                        Nikolay
                                    </option>
                                    <option value="5">
                                        Dima
                                    </option>
                                    <option value="6">
                                        Valentin
                                    </option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_changed_by_select2" name="param" multiple="multiple">
                                <option value="0" selected>
                                    All users
                                </option>
                                <optgroup label="Department 1">
                                    <option value="1">
                                        Kolya
                                    </option>
                                    <option value="2">
                                        Vasya
                                    </option>
                                </optgroup>
                                <optgroup label="Department 2">
                                    <option value="3">
                                        Misha
                                    </option>
                                    <option value="4">
                                        Nikolay
                                    </option>
                                    <option value="5">
                                        Dima
                                    </option>
                                    <option value="6">
                                        Valentin
                                    </option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-input" name="param">
                                <option value="0" selected>Change tasks</option>
                                <option value="0">n/a</option>
                                <option value="1">Due today</option>
                                <option value="1">Due tomorrow</option>
                                <option value="1">Due this week</option>
                                <option value="1">Due this month</option>
                                <option value="1">Due this quarter</option>
                                <option value="1">No To-dos assigned</option>
                                <option value="1">Overdue</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Select budget
                            </label>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="budget_range_nouislider" class="m-nouislider"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="budget_range_min">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="budget_range_max">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item--> 
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_2_head" data-toggle="collapse" href="#m_accordion_1_item_2_body" aria-expanded="    false">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Properties of linked contacts
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_2_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" placeholder="Deal name">
                        </div>
                        <div class="form-group">
                            <div class='input-group' id='m_daterangepicker_2'>
                                <input type='text' class="form-control m-input"  placeholder="Select date range"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_stage_select2" name="param" multiple="multiple">
                                <option value="opt_1" selected>
                                    All stages
                                </option>
                                <option value="opt_2">
                                    Stage 2
                                </option>
                                <option value="opt_3">
                                    Stage 3
                                </option>
                                <option value="opt_4">
                                    Stage 4
                                </option>
                                <option value="opt_5">
                                    Stage 5
                                </option>
                                <option value="opt_6">
                                    Stage 6
                                </option>
                                <option value="opt_7">
                                    Stage 7
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item--> 
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_3_head" data-toggle="collapse" href="#m_accordion_1_item_3_body" aria-expanded="    false">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        Properties of related companies
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_3_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_3_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" placeholder="Deal name">
                        </div>
                        <div class="form-group">
                            <div class='input-group' id='m_daterangepicker_3'>
                                <input type='text' class="form-control m-input" placeholder="Select date range"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_stage_select2" name="param" multiple="multiple">
                                <option value="opt_1" selected>
                                    All stages
                                </option>
                                <option value="opt_2">
                                    Stage 2
                                </option>
                                <option value="opt_3">
                                    Stage 3
                                </option>
                                <option value="opt_4">
                                    Stage 4
                                </option>
                                <option value="opt_5">
                                    Stage 5
                                </option>
                                <option value="opt_6">
                                    Stage 6
                                </option>
                                <option value="opt_7">
                                    Stage 7
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
        </div>
    </div>
    <div class="col-lg-6 tag-right-search">
        <div class="title">
            <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                Tags
            </span>
            <a href="#" class="btn btn-outline-primary btn-sm m-btn m-btn--icon m-btn--pill">
                <span>
                    <i class="la la-gear"></i>
                    <span>
                        Settings
                    </span>
                </span>
            </a>
        </div>
        <div class="form-group">
            <input type="text" name="tag-search" class="form-control m-input" placeholder="Search by tag">
        </div>
    </div>
</div>