<div class="filter-item">
    <a href="#" class="m-link m--font-bold">Custom filter 1</a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold active">Custom filter active</a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold">Custom filter 3</a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold">Custom filter 4</a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold">Custom filter 5</a>
</div>
