<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_create_new_city" tabindex="-1" role="dialog" aria-labelledby="m_modal_create_new_city_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_create_new_city_label">
                    <?= TranslationHelper::getTranslation('add_new_city_title', $lang, 'Add new city') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" method="POST" id="add_new_city_form">
                    <div class="form-group m-form__group row">
                        <div class="col-12">
                            <label for="add_new_city_input">
                                <?= TranslationHelper::getTranslation('city_name_input_label', $lang, 'City name') ?>:
                            </label>
                            <br>
                            <input id="add_new_city_input" type="text" name="city_name" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('city_name_input_placeholder', $lang, 'Enter city name') ?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                </button>
                <button type="submit" form="add_new_city_form" class="btn btn-primary" id="add_new_city_form_submit">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save'); ?>
                </button>
            </div>
        </div>
    </div>
</div>