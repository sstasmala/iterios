<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="m_modal_add_handbook_item_hotel" tabindex="-1" role="dialog" aria-labelledby="m_modal_add_handbook_item_hotel_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_add_handbook_item_hotel_label">
                    <?= TranslationHelper::getTranslation('hbi_modal_add_hotel_title', $lang, 'Add hotel') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="preloader">
                    <div class="m-loader m-loader--brand"></div>
                </div>
                <form id="add_hotel_handbook_item_form" action="#" method="POST">
                    <div class="form-group m-form__group">
                        <label for="hotel_name_for_hotel_input">
                            <?= TranslationHelper::getTranslation('hbi_form_hotel_name_input_label', $lang, 'Hotel name') ?>
                        </label>
                        <input id="hotel_name_for_hotel_input" type="text" name="hotel_name" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hbi_form_hotel_name_input_placeholder', $lang, 'Enter hotel name') ?>">
                    </div>
                    <div class="form-group m-form__group select2-mod">
                        <label for="hotel_category_for_hotel_select">
                            <?= TranslationHelper::getTranslation('hbi_form_hotel_category_select_label', $lang, 'Hotel category') ?>
                        </label>
                        <select id="hotel_category_for_hotel_select" name="hotel_category" required class="form-control m-bootstrap-select" title="<?= TranslationHelper::getTranslation('hbi_form_hotel_category_select_placeholder', $lang, 'Select hotel category') ?>"></select>
                    </div>
                    <div class="form-group m-form__group select2-mod">
                        <label for="hotel_city_for_hotel_select">
                            <?= TranslationHelper::getTranslation('hbi_form_hotel_city_select_label', $lang, 'Hotel city') ?>
                        </label>
                        <select id="hotel_city_for_hotel_select" name="hotel_city" required class="form-control m-bootstrap-select"></select>
                    </div>
                    <div class="form-group m-form__group select2-mod">
                        <label for="country_for_hotel_select">
                            <?= TranslationHelper::getTranslation('hbi_form_country_select_label', $lang, 'Country') ?>
                        </label>
                        <select name="country" id="country_for_hotel_select" required class="form-control m-bootstrap-select"></select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="add_hotel_handbook_item_form">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>