<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="m_modal_add_handbook_item_city" tabindex="-1" role="dialog" aria-labelledby="m_modal_add_handbook_item_city_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_add_handbook_item_city_label">
                    <?= TranslationHelper::getTranslation('hbi_modal_add_city_title', $lang, 'Add city') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="preloader">
                    <div class="m-loader m-loader--brand"></div>
                </div>
                <form id="add_city_handbook_item_form" action="#" method="POST">
                    <div class="form-group m-form__group">
                        <label for="city_name_for_city_input">
                            <?= TranslationHelper::getTranslation('hbi_form_city_name_input_label', $lang, 'City name') ?>
                        </label>
                        <input id="city_name_for_city_input" type="text" name="city_name" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hbi_form_city_name_input_placeholder', $lang, 'Enter city name') ?>">
                    </div>
                    <div class="form-group m-form__group select2-mod">
                        <label for="country_for_city_select">
                            <?= TranslationHelper::getTranslation('hbi_form_country_select_label', $lang, 'Country') ?>
                        </label>
                        <select name="country" id="country_for_city_select" required class="form-control m-bootstrap-select"></select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="add_city_handbook_item_form">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>