<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="m_modal_add_handbook_item_reminders_orders" tabindex="-1" role="dialog" aria-labelledby="m_modal_add_handbook_item_reminders_orders_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_add_handbook_item_reminders_orders_label">
                    <?= TranslationHelper::getTranslation('hbi_modal_add_reminders_orders_title', $lang, 'Add reminders orders') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="preloader">
                    <div class="m-loader m-loader--brand"></div>
                </div>
                <form id="add_reminders_orders_handbook_item_form" action="#" method="POST">
                    <div class="form-group m-form__group">
                        <label for="reminders_orders_name_input">
                            <?= TranslationHelper::getTranslation('hbi_form_reminders_orders_name_input_label', $lang, 'Reminders orders name') ?>
                        </label>
                        <input id="reminders_orders_name_input" type="text" name="reminders_orders_name" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hbi_form_reminders_orders_name_input_placeholder', $lang, 'Enter reminders orders name') ?>">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="add_reminders_orders_handbook_item_form">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>