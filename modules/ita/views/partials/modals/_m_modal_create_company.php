<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal right fade" id="m_modal_create_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('create_company_modal_title', $lang, 'New company') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollable="true">
                    <form id="create_company_details" autocomplete="off" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/companies/create' ?>" method="POST">
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <div class="form-group m-form__group">
                            <label for="example-text-input" class="col-form-label">
                                Email
                            </label>
                            <input data-inputmask="'alias': 'email'" class="form-control m-input masked-input" type="text" placeholder="Email" name="email">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-text-input" class="col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('field_phone', $lang);?>
                            </label>
                            <input class="form-control m-input masked-input" name="phone" id="company_create"  valid="companyPhoneNumber" />
<!--                            <input data-inputmask="'mask': '99(999)999-9999'" class="form-control m-input masked-input" type="text" placeholder="--><?//= \app\helpers\TranslationHelper::getTranslation('field_phone',$lang);?><!--" name="phone" value="">-->
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-text-input" class="col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('field_company_name', $lang, 'Company name'); ?>
                            </label>
                            <input class="form-control m-input" type="text" placeholder="<?= \app\helpers\TranslationHelper::getTranslation('field_company_name', $lang);?>" name="name">
                        </div>
                    </form>

                    <div id="companies" class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="display: none;">
                        <div class="m-demo__preview">
                            <!--begin::Widget 14-->
                            <div class="m-widget4">
                                <br/><br/>
                            </div>
                            <!--end::Widget 14-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button', $lang);?>
                </button>
                <button id="create_company_submit" type="submit" form="create_company_details" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save_changes_button', $lang);?>
                </button>
            </div>
        </div>
    </div>
</div>