<?php
    use app\helpers\TranslationHelper;
    
    $lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="contacts_merge_modal" tabindex="-1" role="dialog" aria-labelledby="contactsMergeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contactsMergeModalLabel">
                    <?= TranslationHelper::getTranslation('contacts_merge_modal_tite', $lang, 'Was found similar contacts') . ' (' . 16 . ')' ?>
                </h5>
                <div class="tools">
                    <a href="#" id="merge-all-link" class="m-link m--font-bolder">
                        <?= TranslationHelper::getTranslation('contacts_merge_all_btn', $lang, 'Merge all') ?>
                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="merge-items">
                    <div class="merge-item">
                        <h5 class="m--font-bold mb-3">
                            Alex Merser
                        </h5>
                        <div class="contact-items">
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Alex Merser" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Alex Merser
                                    </div>
                                    <div class="merge-object">
                                        +380554783256
                                    </div>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Alex Merser" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Alex Merser
                                    </div>
                                    <div class="merge-object">
                                        +380554783256
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="actions text-right">
                            <a href="#" class="dismiss-merge-combination m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                            </a>
                            <a href="#" class="merge-one-link m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('contacts_merge_btn', $lang, 'Merge') ?>
                            </a>
                        </div>
                    </div>
                    <div class="merge-item">
                        <h5 class="m--font-bold mb-3">
                            Jessica Alba
                        </h5>
                        <div class="contact-items">
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Jessica Alba" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Jessica Alba
                                    </div>
                                    <div class="merge-object">
                                        jessica.roleplay@gmail.com
                                    </div>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Jessica Alba" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Jessica Alba
                                    </div>
                                    <div class="merge-object">
                                        jess34@mail.ru
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="actions text-right">
                            <a href="#" class="dismiss-merge-combination m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                            </a>
                            <a href="#" class="merge-one-link m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('contacts_merge_btn', $lang, 'Merge') ?>
                            </a>
                        </div>
                    </div>
                    <div class="merge-item">
                        <h5 class="m--font-bold mb-3">
                            Jack Shepard
                        </h5>
                        <div class="contact-items">
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Jack Shepard" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Jack Shepard
                                    </div>
                                    <div class="merge-object">
                                        +555024019348
                                    </div>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="avatar">
                                    <img alt="Jack Shepard" src="/img/profile_default.png">
                                </div>
                                <div class="data">
                                    <div class="name">
                                        Jack Shepard
                                    </div>
                                    <div class="merge-object">
                                        +555024019348
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="actions text-right">
                            <a href="#" class="dismiss-merge-combination m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                            </a>
                            <a href="#" class="merge-one-link m-link m--font-bolder">
                                <?= TranslationHelper::getTranslation('contacts_merge_btn', $lang, 'Merge') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>