<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->registerCssFile('@web/plugins/phone-validator/build/css/intlTelInput.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/js/contacts/view.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/plugins/phone-validator/build/js/intlTelInput.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
?>

<div class="modal fade" id="contact-edit-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= TranslationHelper::getTranslation('contact_preview_edit_contact', $lang, 'Edit contact') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="conctact-edit-popup-form" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/view?id=' . $contact['id'] ?>" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <div class="form-group m-form__group row">
                        <div class="col-12 mb-3 col-lg-4 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name') ?>:</h6>
                            <input type="text" name="contacts[first_name]" class="form-control m-input" value="<?= ($contact['first_name']) ? $contact['first_name'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name') ?>" required>
                        </div>
                        <div class="col-12 mb-0 col-lg-4 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>:</h6>
                            <input type="text" name="contacts[last_name]" class="form-control m-input" value="<?= ($contact['last_name']) ? $contact['last_name'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>">
                        </div>
                        <div class="col-12 mb-3 col-lg-4 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('field_middle_name', $lang, 'Middle name') ?>:</h6>
                            <input type="text" name="contacts[middle_name]" class="form-control m-input" value="<?= ($contact['middle_name']) ? $contact['middle_name'] : '' ?>" placeholder="<?= TranslationHelper::getTranslation('field_middle_name', $lang, 'Middle name') ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 mb-lg-0 mb-lg-0 mb-3">
                            <h6><?= TranslationHelper::getTranslation('contact-preview-phones', $lang, 'Phones') . ':' ?></h6>
                            <div id="contact-phones-repeater">
                                <div class="repeater-list<?= (!empty($contact['phones'])) ? ' hide-first-one' : '' ?>" data-repeater-list="contacts[phones]">
                                    <div data-repeater-item class="row mb-2">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= ucfirst(current($contacts_types['phones'])['name']) ?>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($contacts_types['phones'] as $type) { ?>
                                                            <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= current($contacts_types['phones'])['id'] ?>" data-default-value="<?= current($contacts_types['phones'])['id'] ?>">
                                                <input class="form-control m-input masked-input validate-add-phone" name="Contacts[phone]" id="request_added__contact_phone_input"  valid="phoneNumber_add" />
<!--                                                <input type="text" name="value" class="form-control m-input masked-input" value="" placeholder="--><?//= TranslationHelper::getTranslation('conctact-edit-popup-enter_phone_number-placeholder', $lang, 'Phone number') ?><!--" data-inputmask="'mask': '99(999)999-9999'">-->
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                        <i class="la la-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!empty($contact['phones'])){
                                        foreach ($contact['phones'] as $phone){ ?>
                                            <div data-repeater-item class="row mb-2">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="input-group extra-dropdown-option phone-group" data-for-input-selector=".hidden-extra-option">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <?= ucfirst(current($contacts_types['phones'])['name']) ?>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <?php foreach ($contacts_types['phones'] as $type) { ?>
                                                                    <a class="dropdown-item" href="#" data-value="<?= $type['id'] ?>"><?= ucfirst($type['name']) ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?= $phone['id'] ?>">
                                                        <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $phone['type_id'] ?>">
                                                        <input class="form-control m-input masked-input validate-phone input_phone_visible" name="Contacts[phone]" id="request_edit__contact_phone_input"  valid="phoneNumber" value="<?= $phone['value'] ?>" />
<!--                                                        <input type="text" name="value" class="form-control m-input masked-input" value="--><?//= $phone['value'] ?><!--" placeholder="--><?//= TranslationHelper::getTranslation('conctact-edit-popup-enter_phone_number-placeholder', $lang, 'Enter phone number') ?><!--" data-inputmask="'mask': '99(999)999-9999'">-->
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                <i class="la la-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <span href="#" class="m-link" data-repeater-create="">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('contact-preview-emails', $lang, 'Emails') . ':' ?></h6>
                            <div id="contact-emails-repeater">
                                <div class="repeater-list<?= (!empty($contact['emails'])) ? ' hide-first-one' : '' ?>" data-repeater-list="contacts[emails]">
                                    <div data-repeater-item class="row mb-2">
                                        <div class="col-sm-12 col-md-11">
                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= ucfirst(current($contacts_types['emails'])['name']) ?>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($contacts_types['emails'] as $email_type) { ?>
                                                            <a class="dropdown-item" href="#" data-value="<?= $email_type['id'] ?>"><?= ucfirst($email_type['name']) ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= current($contacts_types['emails'])['id'] ?>" data-default-value="<?= current($contacts_types['emails'])['id'] ?>">
                                                <input type="text" name="value" class="form-control m-input masked-input" value="" placeholder="<?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?>" data-inputmask="'alias': 'email'">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                        <i class="la la-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!empty($contact['emails'])){
                                        foreach ($contact['emails'] as $email){ ?>
                                            <div data-repeater-item class="row mb-2">
                                                <div class="col-sm-12 col-md-11">
                                                    <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <?= ucfirst($contacts_types['emails'][$email['type_id']]['name']) ?>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <?php foreach ($contacts_types['emails'] as $email_type) { ?>
                                                                    <a class="dropdown-item" href="#" data-value="<?= $email_type['id'] ?>"><?= ucfirst($email_type['name']) ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?= $email['id'] ?>">
                                                        <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $email['type_id'] ?>">
                                                        <input type="text" name="value" class="form-control m-input masked-input" value="<?= $email['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?>" data-inputmask="'alias': 'email'">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                <i class="la la-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <span href="#" class="m-link" data-repeater-create="">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 mb-lg-0 mb-lg-0 mb-3">
                            <h6><?= TranslationHelper::getTranslation('contact-preview-socials', $lang, 'Socials') . ':' ?></h6>
                            <div id="contact-socials-repeater">
                                <div class="repeater-list<?= (!empty($contact['socials'])) ? ' hide-first-one' : '' ?>" data-repeater-list="contacts[socials]">
                                    <div data-repeater-item class="row mb-2">
                                        <div class="col-sm-12 col-md-11">
                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= ucfirst(current($contacts_types['socials'])['name']) ?>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($contacts_types['socials'] as $social_type) { ?>
                                                            <a class="dropdown-item" href="#" data-value="<?= $social_type['id'] ?>"><?= ucfirst($social_type['name']) ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= current($contacts_types['socials'])['id'] ?>" data-default-value="<?= current($contacts_types['socials'])['id'] ?>">
                                                <input type="text" name="value" class="form-control m-input" value="" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                        <i class="la la-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!empty($contact['socials'])){
                                        foreach ($contact['socials'] as $social){ ?>
                                            <div data-repeater-item class="row mb-2">
                                                <div class="col-sm-12 col-md-11">
                                                    <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <?= ucfirst($contacts_types['socials'][$social['type_id']]['name']) ?>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <?php foreach ($contacts_types['socials'] as $social_type) { ?>
                                                                    <a class="dropdown-item" href="#" data-value="<?= $social_type['id'] ?>"><?= ucfirst($social_type['name']) ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?= $social['id'] ?>">
                                                        <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $social['type_id'] ?>">
                                                        <input type="text" name="value" class="form-control m-input" value="<?= $social['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                <i class="la la-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <span href="#" class="m-link" data-repeater-create="">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('contact-preview-messengers', $lang, 'Messengers') . ':' ?></h6>
                            <div id="contact-messengers-repeater">
                                <div class="repeater-list<?= (!empty($contact['messengers'])) ? ' hide-first-one' : '' ?>" data-repeater-list="contacts[messengers]">
                                    <div data-repeater-item class="row mb-2">
                                        <div class="col-sm-12 col-md-11">
                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= ucfirst(current($contacts_types['messengers'])['name']) ?>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($contacts_types['messengers'] as $messenger_type) { ?>
                                                            <a class="dropdown-item" href="#" data-value="<?= $messenger_type['id'] ?>"><?= ucfirst($messenger_type['name']) ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= current($contacts_types['messengers'])['id'] ?>" data-default-value="<?= current($contacts_types['messengers'])['id'] ?>">
                                                <input type="text" name="value" class="form-control m-input" value="" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                        <i class="la la-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!empty($contact['messengers'])){
                                        foreach ($contact['messengers'] as $messenger){ ?>
                                            <div data-repeater-item class="row mb-2">
                                                <div class="col-sm-12 col-md-11">
                                                    <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <?= ucfirst($contacts_types['messengers'][$messenger['type_id']]['name']) ?>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <?php foreach ($contacts_types['messengers'] as $messenger_type) { ?>
                                                                    <a class="dropdown-item" href="#" data-value="<?= $messenger_type['id'] ?>"><?= ucfirst($messenger_type['name']) ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?= $messenger['id'] ?>">
                                                        <input type="hidden" name="type_id" class="hidden-extra-option" value="<?= $messenger['type_id'] ?>">
                                                        <input type="text" name="value" class="form-control m-input" value="<?= $messenger['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                <i class="la la-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <span href="#" class="m-link" data-repeater-create="">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 mb-lg-0 mb-3">
                            <h6><?= TranslationHelper::getTranslation('contact-preview-sites', $lang, 'Sites') . ':' ?></h6>
                            <div id="contact-sites-repeater">
                                <div class="repeater-list<?= (!empty($contact['sites'])) ? ' hide-first-one' : '' ?>" data-repeater-list="contacts[sites]">
                                    <div data-repeater-item class="row mb-2">
                                        <div class="col-sm-12 col-md-11">
                                            <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= ucfirst(current($contacts_types['sites'])['name']) ?>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($contacts_types['sites'] as $sites_type) { ?>
                                                            <a class="dropdown-item" href="#" data-value="<?= $sites_type['id'] ?>"><?= ucfirst($sites_type['name']) ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name='type_id' class="hidden-extra-option" value="<?= current($contacts_types['sites'])['id'] ?>" data-default-value="<?= current($contacts_types['sites'])['id'] ?>">
                                                <input type="text" name='value' class="form-control m-input" value="" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                        <i class="la la-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(!empty($contact['sites'])){
                                        foreach ($contact['sites'] as $site){ ?>
                                            <div data-repeater-item class="row mb-2">
                                                <div class="col-sm-12 col-md-11">
                                                    <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-extra-option">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <?= ucfirst($contacts_types['sites'][$site['type_id']]['name']) ?>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <?php foreach ($contacts_types['sites'] as $site_type) { ?>
                                                                    <a class="dropdown-item" href="#" data-value="<?= $site_type['id'] ?>"><?= ucfirst($site_type['name']) ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?= $site['id'] ?>">
                                                        <input type="hidden" name='type_id' class="hidden-extra-option" value="<?= $site['type_id'] ?>">
                                                        <input type="text" name='value' class="form-control m-input" value="<?= $site['value'] ?>" placeholder="<?= TranslationHelper::getTranslation('enter_link', $lang, 'Enter URL') ?>">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete="">
                                                                <i class="la la-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <span href="#" class="m-link" data-repeater-create="">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-lg-0">
                            <h6><?= TranslationHelper::getTranslation('contacts_field_tags', $lang, 'Tags') ?></h6>
                            <select class="form-control m-select2" id="contacts-modal-tags-search-select" multiple name="tags[]">
                                <?php foreach ($contact['tags'] as $tag){ ?>
                                    <option value="<?= $tag['id'] ?>" selected="selected"><?= $tag['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="conctact-edit-popup-form">
                    <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                </button>
            </div>
        </div>
    </div>
</div>