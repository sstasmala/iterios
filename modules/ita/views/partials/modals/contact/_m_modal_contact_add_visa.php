<?php
    use yii\helpers\Html;
    use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="conctact-add-visa-popup" tabindex="-1" role="dialog" aria-labelledby="add-visa-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-visa-modal-title">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add_visa_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/add-visa?id=' . $contact['id'] ?>" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <input type="hidden" name="ContactVisa[visa_id]" id="ContactVisa_visa_id" value="">
                    <input type="hidden" name="ContactVisa[contact_id]" id="ContactVisa_contact_id" value="<?= $contact['id'] ?>">
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="contact_visa_country"><?= TranslationHelper::getTranslation('modal_add_visa_country', $lang, 'Country') ?></label>
                            <select class="form-control m-bootstrap-select country-select-ajax" name="ContactVisa[country]" id="contact_visa_country" required></select>
                        </div>
                        <div class="col-sm-6">
                            <label for="contact_visa_begin_date"><?= TranslationHelper::getTranslation('modal_add_visa_begin_date', $lang, 'Begin date') ?></label>
                            <div class="input-group date">
                                <input type="text" class="form-control m-input" name="ContactVisa[begin_date]" id="contact_visa_begin_date" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="contact_visa_type"><?= TranslationHelper::getTranslation('modal_add_visa_type', $lang, 'Type') ?></label>
                            <select class="form-control m-bootstrap-select m_selectpicker" name="ContactVisa[type]" id="contact_visa_type" required data-live-search="true">
                                <?php foreach($contacts_types['visas'] as $visa_type){ ?>
                                    <option value="<?= $visa_type['id'] ?>"><?= $visa_type['name'] ?></option>
                                <?php } ?>
                        </select>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="contact_visa_end_date"><?= TranslationHelper::getTranslation('modal_add_visa_end_date', $lang, 'End date') ?></label>
                            <div class="input-group date">
                                <input type="text" class="form-control m-input" name="ContactVisa[end_date]" id="contact_visa_end_date" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="contact_visa_description"><?= TranslationHelper::getTranslation('modal_add_visa_description', $lang, 'Begin date') ?></label>
                                <textarea class="form-control m-input" id="contact_visa_description" name="ContactVisa[description]" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button',$lang);?>
                </button>
                <button type="submit" form="add_visa_form" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save_changes_button',$lang);?>
                </button>
            </div>
        </div>
    </div>
</div>