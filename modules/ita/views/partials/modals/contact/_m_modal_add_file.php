<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="file_add_modal" tabindex="-1" role="dialog" aria-labelledby="file_add_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="m-portlet m-portlet--tabs" style="margin-bottom: 0;">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?= TranslationHelper::getTranslation('new_file', $lang, 'New file') ?>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-pane" id="upload_file_block" role="tabpanel">
                        <!--begin::Form-->
                        <form class="m-form m-form--fit m-form--label-align-right" method="POST">
                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                            <div class="form-group m-form__group row" style="padding-left: 0;">
                                <label id="pp_text" for="example-text-input" class="col-2 col-form-label">
                                    <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_upload_doc', $lang); ?>
                                </label>
                                <div class="col-10">
                                    <div class="m-dropzone dropzone" id="m-dropzone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_dropzone_title', $lang); ?>
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_dropzone_desc', $lang); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="reset" id="upload_btn" class="btn btn-primary m-btn m-btn--air m-btn--custom" disabled="disabled" style="cursor: not-allowed;" data-contact_id="<?= $contact['id'] ?>">
                                            <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_upload_doc_btn', $lang); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>