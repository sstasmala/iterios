<?php
$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="contacts_edit_settings_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <?= \app\helpers\TranslationHelper::getTranslation('contacts_list_options_title',$lang);?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="custom-m-scrollable">
                    <div class="scrollable-content">
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                               <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_full_name',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="Full_name">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_gender',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="gender">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_contacts',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="contacts">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_type',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="type">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_birth_date',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="date_of_birth">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_title_age',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="Age">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_nationality',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="nationality">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_company',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="company">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_tags',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="tags">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_created',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="created_at">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contacts_field_updated',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="updated_at">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('responsible',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="responsible">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-6 col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('contact_actions',$lang);?>
                            </label>
                            <div class="col-6 m--align-right">
                                <span class="m-switch m-switch--icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="" data-field="Actions">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button',$lang);?>
                </button>
                <button type="button" class="btn btn-primary" id="save-settings">
                    <?= \app\helpers\TranslationHelper::getTranslation('save_changes_button',$lang);?>
                </button>
            </div>
        </div>
    </div>
</div>