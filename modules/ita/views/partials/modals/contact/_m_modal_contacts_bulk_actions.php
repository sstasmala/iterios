<?php
    use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="m_modal_contacts_bulk_actions" tabindex="-1" role="dialog" aria-labelledby="BulkActions">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="BulkActions">
                    <?= TranslationHelper::getTranslation('contacts_bulk', $lang, 'Bulk actions') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-12"><?= TranslationHelper::getTranslation('modal_bulc_actions_operation_type_label', $lang, 'Operation type') ?></label>
                    <div class="col-12">
                        <?php
                            $contacts_bulks = [
                                [
                                    'id' => 1,
                                    'name' => TranslationHelper::getTranslation('modal_bulk_tags', $lang),
                                    'update_template' => [
                                        'label' => TranslationHelper::getTranslation('modal_bulk_add_tags', $lang),
                                        'html' => '<select class="form-control m-select2" id="tags_select" name="tags[]" multiple="multiple"></select>'
                                    ]
                                ],
                                [
                                    'id' => 2,
                                    'name' => TranslationHelper::getTranslation('modal_bulk_responsible', $lang),
                                    'update_template' => [
                                        'label' => TranslationHelper::getTranslation('modal_bulk_select_responsible', $lang),
                                        'html' => '<select class="form-control m-select2" id="responsible_select" name="responsible"></select>'
                                    ]
                                ]
                            ]
                        ?>
                        <select class="form-control m-bootstrap-select m_selectpicker" id="bulk_action_type_picker">
                            <?php foreach ($contacts_bulks as $bulk){ ?>
                                <option value="<?= $bulk['id'] ?>"><?= $bulk['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12">
                        <form action="<?= Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/bulk-actions' ?>" method="POST" id="bulk-action-form">
                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                            <?= Html::hiddenInput('ids', '', ['id' => 'ids_input']) ?>
                            <div id="form_select"></div>
                        </form>
                    </div>
                </div>
                <div class="bulk-actions-templates">
                    <?php foreach ($contacts_bulks as $bulk){ ?>
                        <div class="form-group m-form__group row bulk-update-block" id="bulk-update-block-<?= $bulk['id'] ?>">
                            <label class="col-form-label col-12"><?= $bulk['update_template']['label'] ?></label>
                            <div class="col-12">
                                <?= $bulk['update_template']['html'] ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang);?>
                </button>
                <button type="submit" form="bulk-action-form" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('save_changes_button' ,$lang);?>
                </button>
            </div>
        </div>
    </div>
</div>