<?php
    use yii\helpers\Html;
    use app\helpers\DateTimeHelper;
?>
<div class="modal fade" id="conctact-add-passport-popup" tabindex="-1" role="dialog" aria-labelledby="add-passport-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-passport-modal-title">
                    Add passport
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <form id="add_passport_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/add-passport?id=' . $contact['id'] ?>" method="POST">
                    <input type="hidden" name="ContactPassport[passport_id]" id="ContactPassport_passport_id" value="">
                    <input type="hidden" name="ContactPassport[contact_id]" id="ContactPassport_contact_id" value="<?= $contact['id'] ?>">
                    <div class="form-group">
                        <label class="control-label" for="ContactPassport_type_id" id="modal-add-passport-passport-type"></label>
                        <select class="form-control m-bootstrap-select m_selectpicker" name="ContactPassport[type]" id="ContactPassport_type_id">
                            <?php foreach($contacts_types['passports'] as $passport_type){ ?>
                                <option value="<?= $passport_type['id'] ?>"><?= $passport_type['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" for="ContactPassport_first_name" id="modal-add-passport-first-name"></label>
<!--                                data-inputmask="'alias': 'english_uppercae'"-->
                                <input class="form-control bank-name-input" name="ContactPassport[first_name]" id="ContactPassport_first_name" type="text">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" for="ContactPassport_last_name" id="modal-add-passport-last-name"></label>
                                <input class="form-control bank-name-input" name="ContactPassport[last_name]" id="ContactPassport_last_name" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" id="modal-add-passport-birth-date"></label>
                        <div class="input-group date">
                            <input type="text" name="ContactPassport[birth_date]" id="passport_form_birth_date_datepicker" class="form-control m-input">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="ContactPassport_serial" id="modal-add-passport-passport-serial"></label>
                        <input class="form-control" name="ContactPassport[serial]" id="ContactPassport_serial" type="text">
                    </div>

                    <div class="form-group">
                        <label class="control-label" id="modal-add-passport-limit-date"></label>
                        <div class="input-group date">
                            <input type="text" class="form-control m-input" name="ContactPassport[date_limit]" id="passport_form_date_limit_datepicker">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" id="modal-add-passport-issued-date"></label>
                        <div class="input-group date">
                            <input type="text" class="form-control m-input" name="ContactPassport[issued_date]" id="passport_form_issued_date_datepicker">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="passport_form_issued_owner" id="modal-add-passport-issued-owner"></label>
                        <input class="form-control" name="ContactPassport[issued_owner]" id="passport_form_issued_owner" type="text">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="ContactPassport_nationality" id="modal-add-passport-nationality"></label>
                        <select class="form-control m-bootstrap-select country-select-ajax" name="ContactPassport[nationality]" id="ContactPassport_nationality"></select>
                    </div>

<!--                    <div class="form-group">-->
<!--                        <label class="control-label" for="ContactPassport_country" id="modal-add-passport-country"></label>-->
<!--                        <select class="form-control m-bootstrap-select country-select-ajax" name="ContactPassport[country]" id="ContactPassport_country"></select>-->
<!--                    </div>-->

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal-add-passport-close"></button>
                <button type="submit" class="btn btn-primary" id="modal-add-passport-save" data-toggle="modal" data-target="#hello"></button>
            </div>
        </div>
    </div>
</div>