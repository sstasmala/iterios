<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal right fade" id="contact_quick_edit_modal" tabindex="-1" role="dialog" aria-labelledby="quickEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="quickEditModalLabel">
                    <?= TranslationHelper::getTranslation('contact_preview_quick_edit_modal', $lang, 'Edit contact') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="contact_quick_edit_modal_form" class="m-form m-form--fit" method="POST" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/contacts/index' ?>">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('contact_id', '', ['id' => 'contact_id_quick_edit_modal_field']) ?>
                    <div class="m-form__section m-form__section--first">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">1. Контактная информация туриста:</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example_input_full_name">Full Name:</label>
                            <input type="email" class="form-control m-input" placeholder="Enter full name">
                        </div>
                        <div class="form-group m-form__group">
                            <label>Email address:</label>
                            <input type="email" class="form-control m-input" placeholder="Enter email">
                        </div>
                        <div class="form-group m-form__group">
                            <label>Contact:</label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="Phone number">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-bell"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="m-form__group form-group">
                            <label for="">Communication:</label>
                            <div class="m-checkbox-list">
                                <label class="m-checkbox">
                                    <input type="checkbox"> Email
                                    <span></span>
                                </label>
                                <label class="m-checkbox">
                                    <input type="checkbox"> SMS
                                    <span></span>
                                </label>
                                <label class="m-checkbox">
                                    <input type="checkbox"> Phone
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section m-form__section--last">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">2. <?= TranslationHelper::getTranslation('quick_contact_modal_task_title', $lang, 'Create task') ?>:</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="create_task_input__title">
                                <?= TranslationHelper::getTranslation('quick_contact_modal_title_title', $lang, 'Title');?>
                            </label>
                            <input type="text" name="Tasks[title]" id="create_task_input__title" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('create_task_input_title_placeholder', $lang, 'Task title') ?>">
                        </div>
                        <div class="form-group m-form__group">
                            <label>
                                <?= TranslationHelper::getTranslation('quick_contact_modal_due_date_title', $lang, 'Due Date');?>
                            </label>
                            <div class="row mb-3">
                                <input type="hidden" name="Tasks[due_date]" class="task_due_date_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="due_date" class="task_input__due_date form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="due_time" class="task_input__due_time form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label>
                                <?= TranslationHelper::getTranslation('quick_contact_modal_email_reminder_title', $lang, 'Email reminder'); ?>
                            </label>
                            <div class="row">
                                <input type="hidden" name="Tasks[email_reminder]" class="email_reminder_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="email_reminder_date" class="task_input__email_reminder_date form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="email_reminder_time" class="task_input__email_reminder_time form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="create_task_note_redactor">
                                <?= TranslationHelper::getTranslation('quick_contact_modal_note_redactor_title', $lang, 'Notes'); ?>
                            </label>
                            <?= Html::textarea(
                                'Tasks[description]',
                                '',
                                [
                                    'id' => 'create_task_note_textarea',
                                    'class' => 'form-control m-input',
                                    'rows' => '3',
                                    'placeholder' => TranslationHelper::getTranslation('quick_contact_modal_note_redactor_placeholder', $lang, 'Your notes...')
                                ])
                            ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="contact_quick_edit_modal_form">
                    <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                </button>
            </div>
        </div>
    </div>
</div>