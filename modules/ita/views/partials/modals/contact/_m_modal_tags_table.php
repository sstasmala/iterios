<?php
    use app\helpers\TranslationHelper;
    use yii\helpers\Html;
    
    $lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="tags-table-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= TranslationHelper::getTranslation('contacts_field_tags', $lang, 'Tags'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 mb-4 text-right add-tag-form-column">
                        <div class="etitable-form-dropdown m-dropdown m-dropdown--inline m-dropdown--large m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" m-dropdown-persistent="1">
                            <a href="#" class="m-dropdown__toggle btn btn-primary">
                                <i class="la la-plus-circle m--icon-font-size-lg3"></i>
                                <?= TranslationHelper::getTranslation('add_tag', $lang, 'Add tag') ?>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <form class="etitable-form">
                                                <div class="row align-items-center">
                                                    <div class="col-12 labels">
                                                        <h6><?= TranslationHelper::getTranslation('add_tag', $lang, 'Add tag') ?></h6>
                                                    </div>
                                                    <div class="col-12 mb-1"></div>
                                                    <div class="col-8 inputs">
                                                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                                        <input name="ajax_url" type="hidden" value="/ajax/create-tag">
                                                        <input type="text" name="tag_name" placeholder="<?= TranslationHelper::getTranslation('tags_table_tag_name_placeholder', $lang, 'Tag name') ?>" class="form-control m-input" required>
                                                    </div>
                                                    <div class="col-4 buttons text-right">
                                                        <button type="submit" class="btn btn-sm btn-primary editable-submit"><i class="fa fa-check"></i></button>
                                                        <button type="button" class="btn btn-sm etitable-form-cancel"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="preloader" style="display:none;text-align:center;">
                                                <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="m_datatable-tags"></div>
            </div>
        </div>
    </div>
</div>