<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_create_new_hotel" tabindex="-1" role="dialog" aria-labelledby="m_modal_create_new_hotel_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_create_new_hotel_label">
                    <?= TranslationHelper::getTranslation('add_new_hotel_title', $lang, 'Add new hotel') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" method="POST" id="add_new_hotel_form">
                    <div class="row">
                        <div class="col-12 mb-3 form-group m-form__group">
                            <label for="add_new_hotel_name_input">
                                <?= TranslationHelper::getTranslation('hotel_name_input_label', $lang, 'Hotel name') ?>:
                            </label>
                            <br>
                            <input name="hotel_name" id="add_new_hotel_name_input" type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hotel_name_input_placeholder', $lang, 'Enter hotel name') ?>">
                        </div>
                        <div class="col-12 mb-3 form-group m-form__group">
                            <label for="add_new_hotel_category_input">
                                <?= TranslationHelper::getTranslation('hotel_category_input_label', $lang, 'Hotel category') ?>:
                            </label>
                            <br>
                            <div id="add_new_hotel_category_input_preloader" class="m-loader m-loader--brand" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>
                            <select name="hotel_category" id="add_new_hotel_category_input" class="form-control m-bootstrap-select" title="<?= TranslationHelper::getTranslation('hotel_category_input_placeholder', $lang, 'Select hotel category') ?>"></select>
                        </div>
                        <div class="col-12 mb-3 form-group m-form__group">
                            <label for="add_new_hotel_city_input">
                                <?= TranslationHelper::getTranslation('hotel_city_input_label', $lang, 'Hotel city') ?>:
                            </label>
                            <br>
                            <div>
                                <select name="hotel_city" id="add_new_hotel_city_input" class="form-control m-bootstrap-select" title="<?= TranslationHelper::getTranslation('hotel_city_input_placeholder', $lang, 'Select hotel city') ?>"></select>
                            </div>
                        </div>
                        <div class="col-12 form-group m-form__group">
                            <label for="add_new_hotel_country_input">
                                <?= TranslationHelper::getTranslation('hotel_country_input_label', $lang, 'Hotel country') ?>:
                            </label>
                            <br>
                            <select name="hotel_country" id="add_new_hotel_country_input" class="form-control m-bootstrap-select" title="<?= TranslationHelper::getTranslation('hotel_country_input_placeholder', $lang, 'Select hotel country') ?>"></select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                </button>
                <button type="submit" form="add_new_hotel_form" class="btn btn-primary" id="add_new_hotel_form_submit">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save'); ?>
                </button>
            </div>
        </div>
    </div>
</div>