<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="integration_details_modal" tabindex="-1" role="dialog" aria-labelledby="integrationDetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="integrationDetailsModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p class="description">
                            <img alt="" class="logo" src="">
                            <span></span>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('ip_inx_features_list_label', $lang, 'Features') ?>:
                        </label>
                        <ul class="features">
                            <li class="feature-item">
                                <i class="fa fa-check m--font-success mr-1"></i>
                                <span class="m--font-bold">
                                    Harder
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('ip_inx_integration_params_list_label', $lang, 'Integration params') ?>:
                        </label>
                        <form method="POST" action="" id="integration-detail-form" class="m-form">
                            <div id="provider-credentials-repeater">
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <div class="input-group double-inputs-customize">
                                            <div class="input-group-prepend">
                                                <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('ip_inx_api_domain_placeholder', $lang, 'Enter server domain') ?>">
                                            </div>
                                            <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('ip_inx_api_key_placeholder', $lang, 'Enter API key') ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('ip_inx_additional_config_list_label', $lang) ?>:
                        </label>
                        <form method="POST" action="" id="additional-config-form" class="m-form">
                            <div id="provider-additional-config">
                                <div class="row config">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="integration-detail-form">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>