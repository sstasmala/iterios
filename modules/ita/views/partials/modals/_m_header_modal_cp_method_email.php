<?php

use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="header_cp_method_email_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_email" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel_email">
                    <?= TranslationHelper::getTranslation('request_change_cp_method_email', $lang, 'Send Email') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="header_cp_email_form" action="/ENTER-YOUR-ACTION" method="POST">
                    <div class="row mb-3">
                        <div class="col-12 col-md-6 mb-3 mb-md-0">
                            <label>
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_from_who_label', $lang, 'From who') ?>
                                </span>
                            </label>
                            <input type="text" name="Email[from_who]" class="m-input form-control" value="TravelAgent" disabled>
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="header_cp_email_for_who_select">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_for_who_label', $lang, 'For who') ?>
                                </span>
                            </label>
                            <select name="Email[for_who]" id="header_cp_email_for_who_select" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('request_cp_email_modal_for_who_select_placeholder', $lang, 'Select phone nubmer') ?>">
                                <option>
                                    38(098)566-8722
                                </option>
                                <option>
                                    38(067)653-8734
                                </option>
                                <option data-divider="true"></option>
                                <option data-icon="fas fa-plus mr-1" value="CREATE_NEW_CONTACT">
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_create_new_contact_phone_btn', $lang, 'Create new') ?>
                                </option>
                            </select>
                            <input type="text" name="Email[new_contact_phone]" id="header_cp_email_for_who_input" class="m-input form-control masked-input" placeholder="<?= TranslationHelper::getTranslation('request_cp_email_modal_for_who_input_placeholder', $lang, 'Enter phone nubmer') ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-6 mb-3 mb-md-0">
                            <label>
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_hidden_copy_to_label', $lang, 'Copy to') ?>
                                </span>
                            </label>
                            <input type="text" name="Email[copy_to]" class="m-input form-control masked-input"  placeholder="<?= TranslationHelper::getTranslation('request_cp_email_modal_hidden_copy_to_placeholder', $lang, 'Enter email') ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="cp_email_template_select">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_template_label', $lang, 'Email template') ?>
                                </span>
                            </label>
                            <select name="Email[template]" id="cp_email_template_select" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('request_cp_email_modal_template_placeholder', $lang, 'Select email template') ?>">
                                <option value="1">
                                    Template 1
                                </option>
                                <option value="2">
                                    Template 2
                                </option>
                                <option value="3">
                                    Template 3
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="header_cp_email_body_editor">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_email_modal_ckeditor_label', $lang, 'Template body') ?>
                                </span>
                            </label>
                            <div id="header_cp_email_body_editor"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success m-btn m-btn--icon">
                                <span>
                                    <span style="padding-right: .5em; padding-left: 0;">
                                        <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                                    </span>
                                    <i class="fa fa-arrow-right"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>