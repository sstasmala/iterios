<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="document_upload_view_modal" tabindex="-1" role="dialog" aria-labelledby="document_upload_view_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title editable-title" id="document_view_modal_title">
                    <span>
                        <span class="text"></span>
                        <i class="la la-pencil ml-1"></i>
                    </span>
                    <input type="text" id="other-doc-title" class="m-input form-control" name="title" value="">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="head-panel" class="mb-4">
                    <div class="panel-main-row">
                        <div class="download-subpanel">
                            <div class="btn-group m-btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-secondary" id="download-document">
                                    <i class="fa fa-download mr-2"></i><?= TranslationHelper::getTranslation('order_document_download', $lang) ?>
                                </button>
                                <button type="button" class="btn btn-secondary">
                                    <i class="fa fa-print mr-2"></i><?= TranslationHelper::getTranslation('ovdvm_print_btn', $lang, 'Print') ?>
                                </button>
                            </div>
                        </div>
                        <div class="crud-subpanel">
                            <a href="#" id="doc_delete_btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--pill other-doc-delete-link" title="<?= TranslationHelper::getTranslation('delete', $lang) ?>">
                                <i class="la la-trash"></i> <?= TranslationHelper::getTranslation('delete', $lang) ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="document_preview" class="text-center">
                </div>
            </div>
        </div>
    </div>
</div>