<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="document_view_modal" role="dialog" aria-labelledby="document_view_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title editable-title" id="document_view_modal_title">
                    <span>
                        <span class="text"></span>
                        <i class="la la-pencil ml-1"></i>
                    </span>
                    <input type="text" id="gener_doc_input" class="m-input form-control" name="title" value="">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="head-panel" class="mb-4">
                    <div class="panel-main-row">
                        <div class="download-subpanel">
                            <div class="btn-group m-btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-secondary">
                                    <i class="far fa-file-pdf mr-2"></i><?= TranslationHelper::getTranslation('ovdvm_download_pdf_btn', $lang, 'Download PDF') ?>
                                </button>
                                <button type="button" class="btn btn-secondary">
                                    <i class="far fa-file-word mr-2"></i><?= TranslationHelper::getTranslation('ovdvm_download_word_btn', $lang, 'Download Word') ?>
                                </button>
                                <button type="button" class="btn btn-secondary">
                                    <i class="fa fa-print mr-2"></i><?= TranslationHelper::getTranslation('ovdvm_print_btn', $lang, 'Print') ?>
                                </button>
                            </div>
                        </div>
                        <div class="crud-subpanel">
                            <a href="#" id="doc_save_btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="<?= TranslationHelper::getTranslation('save', $lang) ?>" style="display:none;">
                                <i class="la la-save"></i>
                            </a>
                            <a href="#" id="doc_delete_btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-danger m-btn--icon m-btn--pill our-doc-delete-link" title="<?= TranslationHelper::getTranslation('delete', $lang) ?>">
                                <i class="la la-trash"></i> <?= TranslationHelper::getTranslation('delete', $lang) ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <textarea name="document-body" id="doc-editor-block">Some text here</textarea>
                </div>
            </div>
        </div>
    </div>
</div>