<?php

use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_select_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('ov_select_service_modal_title', $lang, 'Add service') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body modal-body--first">
                <div class="modal-body__group">
                    <?php if(isset($services) && !empty($services)) { ?>
                    <div class="row services-set">
                        <?php foreach ($services as $i => $service) { ?>
                        <div class="col-sm-3 col-lg-2 mb-4">
                            <a href="#" class="m-link m--font-bold service-link" data-toggle="modal" data-target="#m_modal_service_details" data-dismiss="modal" data-serv-id="<?= $service['id'] ?>">
                                <div class="icon-wraper">
                                    <i class="<?= (isset($service['icon']))?$service['icon']:'' ?>"></i>
                                </div>
                                <span class="btn-lg__text"> <?= (isset($service['name']))?$service['name']:'' ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modal-footer__btn" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>
