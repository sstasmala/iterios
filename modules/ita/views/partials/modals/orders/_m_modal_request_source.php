<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="contact-request-source" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= TranslationHelper::getTranslation('order_select_request', $lang) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-12 mb-3 col-lg-12 mb-lg-0">
                        <h6 style="color: #575962;"><?= TranslationHelper::getTranslation('order_request', $lang) ?>:</h6>
                        <select class="form-control m-select2" id="order_select_request">
                            <?php if (!empty($order['request_source']) && $order['request_source']['system_type'] === app\models\RequestType::SYSTEM_FROM_REQUEST_TYPE && !empty($order['request_id'])): ?>
                                <option value="<?= $order['request_id'] ?>">
                                    <?= TranslationHelper::getTranslation('order_request', $lang) ?> #<?= $order['request']['id'] ?> (<?= $order['request']['contact']['last_name'] . ' ' . $order['request']['contact']['first_name'] ?>)
                                </option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                </button>
            </div>
        </div>
    </div>
</div>