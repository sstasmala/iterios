<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_edit_tourist_passports" tabindex="-1" role="dialog" aria-labelledby="m_modal_edit_tourists_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_edit_tourists_title">
                    <?= TranslationHelper::getTranslation('ovmetp_title', $lang) ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tourist_passport_form" action="" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>

                    <h5><?= TranslationHelper::getTranslation('order_service_add_tourist_contact_block_title', $lang); ?></h5>
                    <div class="row mb-4 contact-fields">
                        <div class="col-12 col-sm-6">
                            <label class="col-form-label">
                                <?= \app\helpers\TranslationHelper::getTranslation('field_first_name',$lang); ?>
                            </label>
                            <div class="has-danger">
                                <input class="form-control m-input" type="text" placeholder="<?= TranslationHelper::getTranslation('field_first_name', $lang); ?>" name="Contact[first_name]">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label class="col-form-label">
                                <?= TranslationHelper::getTranslation('field_last_name', $lang); ?>
                            </label>
                            <div class="has-danger">
                                <input class="form-control m-input has-danger" type="text" placeholder="<?= TranslationHelper::getTranslation('field_last_name', $lang); ?>" name="Contact[last_name]">
                            </div>
                        </div>
                    </div>

                    <h5 class="mb-3" id="passport_data_title"><?= TranslationHelper::getTranslation('order_service_add_tourist_tourist_block_title', $lang); ?></h5>

                    <div class="m-accordion m-accordion--bordered" id="m_accordion_tourist_passports" role="tablist" style="display: none;">
                    </div>

                    <div id="edit_tourist_fields" style="display: none;">
                        <div>
                            <?= Html::hiddenInput('Tourist[passport_id][]', '', ['class' => 'edit_tourist_passport_pid']) ?>
                            <div class="row mb-3">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_type_select"><?= TranslationHelper::getTranslation('ovmet_passport_type_select_label', $lang, 'Passport type') ?></label>
                                    <div class="form-group m-form__group">
                                        <select class="form-control m-bootstrap-select m_selectpicker edit_tourist_passport_type_select" name="Tourist[passport_type][]" required>
                                            <?php foreach($contacts_types['passports'] as $passport_type){ ?>
                                                <option value="<?= $passport_type['id'] ?>"><?= $passport_type['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_first_name_select"><?= TranslationHelper::getTranslation('ovmet_passport_first_name_select_label', $lang, 'First name') ?></label>
                                    <div class="form-group m-form__group">
                                        <input class="form-control edit_tourist_passport_first_name" name="Tourist[first_name][]" type="text" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_last_name_select"><?= TranslationHelper::getTranslation('ovmet_passport_last_name_select_label', $lang, 'Last name') ?></label>
                                    <div class="form-group m-form__group">
                                        <input class="form-control edit_tourist_passport_last_name" name="Tourist[last_name][]" type="text" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_serial_select"><?= TranslationHelper::getTranslation('ovmet_passport_serial_input_label', $lang, 'Serial number') ?></label>
                                    <div class="form-group m-form__group">
                                        <input class="form-control edit_tourist_passport_serial serial_passport_input" name="Tourist[serial][]" type="text" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_date_limit_picker"><?= TranslationHelper::getTranslation('ovmet_passport_limit_date_label', $lang, 'Limit date') ?></label>
                                    <div class="form-group m-form__group">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input edit_tourist_passport_date_limit_picker" name="Tourist[date_limit][]" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for="tourist_passport_birth_date_picker"><?= TranslationHelper::getTranslation('ovmet_passport_birth_date_label', $lang, 'Birth date') ?></label>
                                    <div class="form-group m-form__group">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input edit_tourist_passport_birth_date_picker" name="Tourist[birth_date][]" id="tourist_passport_birth_date_picker" value="<?= (!empty(\app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']) && \app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']))) ? \app\helpers\DateTimeHelper::convertTimestampToDate($contact['date_of_birth']) : '';?>" required>
                                            <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="edit_passport_form_issued_date_datepicker" id="modal-add-passport-issued-date"><?= TranslationHelper::getTranslation('modal_add_passport_issued_date', $lang, 'Issued date') ?></label>
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input edit_tourist_passport_issued_date" name="Tourist[issued_date][]" id="edit_passport_form_issued_date_datepicker">
                                    <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="edit_passport_form_issued_owner" id="modal-add-passport-issued-owner"><?= TranslationHelper::getTranslation('modal_add_passport_issued_owner', $lang, 'Issued Owner') ?></label>
                                <input class="form-control edit_passport_form_issued_owner" name="Tourist[issued_owner][]" id="edit_passport_form_issued_owner" type="text">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="edit_passport_nationality" id="modal-add-passport-nationality"><?= TranslationHelper::getTranslation('modal_add_passport_nationality', $lang, 'Nationality') ?></label>
                                <select class="form-control m-bootstrap-select country-select edit_contact_passport_nationality" name="Tourist[nationality][]" id="edit_passport_nationality">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button', $lang);?>
                </button>
                <button id="create_tourist_submit" type="button" form="tourist_passport_form" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save', $lang);?>
                </button>
            </div>
        </div>
    </div>
</div>