<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_add_tourist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 1049;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_edit_tourists_title">
                    <?= TranslationHelper::getTranslation('ovmat_title', $lang, 'Edit tourist') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add_tourist_form" action="" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('order_id', $order['id'], []) ?>
                    <?= Html::hiddenInput('passport_id', '', []) ?>
                    <?= Html::hiddenInput('order_tourist_id', '', []) ?>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label class="control-label" for="tourist_service_type_select"><?= TranslationHelper::getTranslation('order_tourist_added_type_input', $lang) ?></label>
                            <div class="form-group m-form__group">
                                <select class="form-control m-bootstrap-select m_selectpicker" name="tourist_type" required>
                                    <option value="<?= \app\models\OrdersTourists::TYPE_ADULT ?>"><?= TranslationHelper::getTranslation('ov_service_tourists_adult_persons_label', $lang) ?></option>
                                    <option value="<?= \app\models\OrdersTourists::TYPE_CHILD ?>"><?= TranslationHelper::getTranslation('ov_service_tourists_children_persons_label', $lang) ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <label class="control-label" for="tourist_passport_type_select"><?= TranslationHelper::getTranslation('ovmad_select_tourist', $lang) ?></label>
                    <div class="row align-items-center mb-3">
                        <div class="col-7">
                            <div class="form-group m-form__group">
                                <select class="form-control" name="tourist_id" id="add_tourist_tourist_select" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-5">
                            <span href="#" class="m-link add_new_tourist" data-toggle="modal" data-source="service" data-type="1" data-target="#m_modal_edit_tourists" style="cursor: pointer;"> +&nbsp; <?= TranslationHelper::getTranslation('order_select_new_contact', $lang) ?></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button', $lang); ?>
                </button>
                <button type="button" form="add_tourist_form" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save', $lang); ?>
                </button>
            </div>
        </div>
    </div>
</div>