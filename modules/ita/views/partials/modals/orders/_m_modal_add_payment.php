<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal fade" id="m_modal_add_payment" tabindex="-1" role="dialog" aria-labelledby="addPaymentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addPaymentModalTitle">
                    <?= TranslationHelper::getTranslation('ovapm_title', $lang, 'Add payment') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-loader m-loader--brand" style="width: 30px;margin: 0 auto;"></div>
                   <div class="m-accordion m-accordion--bordered" id="add_payment_accordion" role="tablist">
                        <!--begin::Item-->

                        <form id="add_payment_form" action="#" method="POST" autocomplete="off">

                        </form>
                        <!--end::Item-->
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="button" class="btn btn-primary" id="save-tourist-payments">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>