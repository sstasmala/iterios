<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;

$this->registerJsFile('@web/js/orders/generate_document.min.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/orders/dropzone.min.js', ['depends' => \app\assets\MainAsset::className()]);
?>

<div class="modal fade" id="document_add_modal" tabindex="-1" role="dialog" aria-labelledby="document_add_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="m-portlet m-portlet--tabs" style="margin-bottom: 0;">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#generate_doc_tab" role="tab">
                                    <i class="la la-edit"></i> <?= TranslationHelper::getTranslation('order_document_modal_generate_doc', $lang) ?>
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#upload_doc_tab" role="tab">
                                    <i class="la la-cloud-upload"></i> <?= TranslationHelper::getTranslation('order_document_modal_upload_doc', $lang) ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body" style="min-height: 400px;">
                    <div class="tab-content">
                        <div class="tab-pane active" id="generate_doc_tab" role="tabpanel">
                            <div class="row documents-set">
                                <?php foreach ($doc_templates as $template): ?>
                                    <div class="col-sm-3 col-lg-2 mb-4">
                                        <a href="#" class="m-link m--font-bold document-link" data-toggle="modal" data-target="#document_view_modal" data-dismiss="modal" data-doc_id="<?= $template['id'] ?>" data-order_id="<?= $order['id'] ?>">
                                            <div class="icon-wraper">
                                                <i class="flaticon-file-1"></i>
                                            </div>
                                            <span class="btn-lg__text"> <?= $template['title'] ?></span>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                                <?php if (empty($doc_templates)): ?>
                                    <div class="text-center">
                                        <?= \app\helpers\TranslationHelper::getTranslation('order_document_template_not_found', $lang); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="upload_doc_tab" role="tabpanel">
                            <!--begin::Form-->
                            <form class="m-form m-form--fit m-form--label-align-right" method="POST">
                                <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                                <div class="form-group m-form__group row" style="padding-left: 0;">
                                    <label id="pp_text" for="example-text-input" class="col-2 col-form-label">
                                        <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_upload_doc', $lang); ?>
                                    </label>
                                    <div class="col-10">
                                        <div class="m-dropzone dropzone" id="m-dropzone">
                                            <div class="m-dropzone__msg dz-message needsclick">
                                                <h3 class="m-dropzone__msg-title">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_dropzone_title', $lang); ?>
                                                </h3>
                                                <span class="m-dropzone__msg-desc">
                                                    <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_dropzone_desc', $lang); ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-10">
                                            <button type="reset" id="upload_btn" class="btn btn-primary m-btn m-btn--air m-btn--custom" disabled="disabled" style="cursor: not-allowed;" data-order_id="<?= $order['id'] ?>">
                                                <?= \app\helpers\TranslationHelper::getTranslation('order_document_modal_upload_doc_btn', $lang); ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>