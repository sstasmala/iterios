<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade modal-open" id="m_modal_create_order_reservation" tabindex="-1" role="dialog" aria-labelledby="m_modal_create_order_reservation_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title">
                    <?= TranslationHelper::getTranslation('rv_create_reservation_modal_title', $lang, 'Create reservation') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="iframe-block row mb-4">
                    <div class="col-sm-12">
                        <div class="iframe-wrap"></div>
                    </div>
                </div>
                <div class="reservation-data-block row">
                    <div class="col-sm-12 mb-1">
                        <h5 class="modal-title modal-header__title">
                            <?= TranslationHelper::getTranslation('rv_create_reservation_modal_usefull_info_title', $lang, 'Useful information near') ?>:
                        </h5>
                    </div>
                    <div class="credential-block col-12 mb-1">
                        <span class="credential-label mr-2">Тур:</span>
                        <span>Болгария / 05.06.2017 / 22.06.2017 / 14 / Варна</span>
                    </div>
                    <div class="credential-block col-12 mb-1">
                        <span class="credential-label mr-2">Доступ:</span>
                        <!-- credential item start -->
                        <span>
                            <span>Логин: </span>
                            <span id="credential-login-source">login@mail.com</span>
                        </span>
                        <span>
                            <i class="fa fa-clipboard copy-icon ml-1" data-copy-target-id="credential-login-source"></i>
                        </span>
                        <!-- credential item end -->
                        <span class="mr-2">, </span>
                        <!-- credential item start -->
                        <span>
                            <span>Пароль: </span>
                            <span id="credential-password-source">A1B2C3D4</span>
                        </span>
                        <span>
                            <i class="fa fa-clipboard copy-icon ml-1" data-copy-target-id="credential-password-source"></i>
                        </span>
                        <!-- credential item end -->
                    </div>
                    <div class="col-12 mt-3">
                        <table class="table m-table m-table--head-bg-success tourists-table-v2">
                            <thead>
                                <tr>
                                    <th>
                                        <?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>
                                    </th>
                                    <th>
                                        <?= TranslationHelper::getTranslation('ov_page_tourists_table_name_title', $lang, 'Name') ?>
                                    </th>
                                    <th>
                                        <?= TranslationHelper::getTranslation('ov_page_tourists_table_passport_birthday_icon', $lang, 'Birth date') ?>
                                    </th>
                                    <th>
                                        <?= TranslationHelper::getTranslation('ov_page_tourists_table_passport_title', $lang, 'Passport') ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <div class="uppercase-name">
                                            <span>
                                                Passport last name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact last name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="uppercase-name">
                                            <span>
                                                Passport first name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact first name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="birth-date">
                                            <span>25.08.1988</span>
                                        </div>
                                        <div class="calc-age">
                                            <span>
                                                <i class="fa fa-male"></i>
                                                (29 лет)
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="passport-serial">
                                            <span>
                                                SERIA123
                                            </span>
                                        </div>
                                        <div class="passport-limit-date">
                                            <span>
                                                <?= TranslationHelper::getTranslation('order_tourists_passport_limit', $lang) ?> 01.01.2025
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row">
                                        <div class="uppercase-name">
                                            <span>
                                                Passport last name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact last name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="uppercase-name">
                                            <span>
                                                Passport first name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact first name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="birth-date">
                                            <span>25.08.1988</span>
                                        </div>
                                        <div class="calc-age">
                                            <span>
                                                <i class="fa fa-male"></i>
                                                (29 лет)
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="passport-serial">
                                            <span>
                                                SERIA123
                                            </span>
                                        </div>
                                        <div class="passport-limit-date">
                                            <span>
                                                <?= TranslationHelper::getTranslation('order_tourists_passport_limit', $lang) ?> 01.01.2025
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row">
                                        <div class="uppercase-name">
                                            <span>
                                                Passport last name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact last name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="uppercase-name">
                                            <span>
                                                Passport first name
                                            </span>
                                        </div>
                                        <a href="#" class="m-link" target="_blank">
                                            Passport contact first name
                                        </a>
                                    </td>
                                    <td>
                                        <div class="birth-date">
                                            <span>25.08.1988</span>
                                        </div>
                                        <div class="calc-age">
                                            <span>
                                                <i class="fa fa-male"></i>
                                                (29 лет)
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="passport-serial">
                                            <span>
                                                SERIA123
                                            </span>
                                        </div>
                                        <div class="passport-limit-date">
                                            <span>
                                                <?= TranslationHelper::getTranslation('order_tourists_passport_limit', $lang) ?> 01.01.2025
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer--align-space-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                </button>
            </div>
        </div>
    </div>
</div>