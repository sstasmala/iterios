<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade modal-open" id="m_modal_service_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="create-label">
                    <?= TranslationHelper::getTranslation('ov_select_service_modal_title', $lang, 'Add service') ?>
                </h5>
                <h5 class="modal-title modal-header__title" id="update-label">
                    <?= TranslationHelper::getTranslation('ov_select_service_modal_title_update', $lang, 'Update service') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                    <input type="hidden" name="order_id" value="<?=$order['id']?>">
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <?= TranslationHelper::getTranslation('ov_select_service_modal_partlet_1_title', $lang, 'Provider and service price') ?>
                                        <small></small>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="service-default-details-block"></div>
                        </div>
                    </div>
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm mb-4" id="service-custom-details-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" id="service-custom-details-portlet-title"></h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="service-custom-details-block"></div>
                        </div>
                    </div>
                    <div id="service-duplicates">

                    </div>
                    <div class="text-center mb-4">
                        <button type="button" class="btn btn-primary" id="btn-duplicate">
                        </button>
                    </div>
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm" id="service-additional-services-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" id="service-additional-services-portlet-title">
                                        <?= TranslationHelper::getTranslation('ov_select_service_modal_partlet_2_title', $lang, 'Additional services') ?>
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav" id="add-srv-btn">

                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="service-additional-services-portlet-block">
                            </div>
                            <div class="container" id="service-additional-services-details">
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm" id="service-tourists-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" id="service-tourists-portlet-title">
                                        <?= TranslationHelper::getTranslation('ov_service_tourists_title', $lang, 'Tourists') ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div id="service-tourists-details">

                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer modal-footer--align-space-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-back" data-toggle="modal" data-target="#m_modal_select_service">
                    <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                </button>
                <button type="button" class="btn btn-primary" id="btn-next">
                    <?= TranslationHelper::getTranslation('next', $lang, 'Next') ?>
                </button>
                <button type="button" class="btn btn-primary" id="btn-save" style="display: none">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>