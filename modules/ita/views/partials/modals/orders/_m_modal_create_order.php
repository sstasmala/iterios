<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$this->registerJsFile('@web/js/partials/create_order.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal right fade" id="m_modal_create_order" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('oicom_title', $lang, 'Create order') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollable="true">
                    <form id="find_contact_form" autocomplete="off" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/orders/create' ?>" method="POST">
                        <h6>
                            <?= TranslationHelper::getTranslation('oicom_add_customer_title', $lang, 'Add customer') ?>:
                        </h6>
                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                        <?= Html::hiddenInput('contact_id', '') ?>
                        <div class="row mb-4 contact-fields">
                            <div class="col-12 col-sm-6">
                                <label class="col-form-label">
                                    Email
                                </label>
                                <input data-inputmask="'alias': 'email'" class="form-control m-input masked-input" id="order_create__contact_email_input" type="text" placeholder="Email" name="Contacts[email]">
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="col-form-label">
                                    <?= TranslationHelper::getTranslation('field_phone', $lang); ?>
                                </label>
                                <input class="form-control m-input masked-input validate-add-phone" name="Contacts[phone]" id="order_create__contact_phone_input" valid="phoneNumber" />
<!--                                <input data-inputmask="'mask': '99(999)999-9999'" class="form-control m-input masked-input" id="order_create__contact_phone_input" type="text" placeholder="--><?//= TranslationHelper::getTranslation('field_phone', $lang);?><!--" name="Contacts[phone]">-->
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="col-form-label">
                                    <?= \app\helpers\TranslationHelper::getTranslation('field_first_name',$lang);?>
                                </label>
                                <input class="form-control m-input" type="text" placeholder="<?= TranslationHelper::getTranslation('field_first_name', $lang);?>" name="Contacts[first_name]">
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="col-form-label">
                                    <?= TranslationHelper::getTranslation('field_last_name', $lang);?>
                                </label>
                                <input class="form-control m-input" type="text" placeholder="<?= TranslationHelper::getTranslation('field_last_name', $lang);?>" name="Contacts[last_name]">
                            </div>
                        </div>
                        <div id="clients" class="m-demo mb-4" data-code-preview="true" data-code-html="true" data-code-js="false" style="display:none;">
                            <h6 class="clients-block-title"><?= TranslationHelper::getTranslation('oicom_contacts_was_found_title', $lang, 'Customers was found') ?></h6>
                            <div class="m-demo__preview">
                                <div class="m-widget4">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button', $lang);?>
                </button>
                <button id="create_company_submit" type="submit" form="find_contact_form" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save', $lang);?>
                </button>
            </div>
        </div>
    </div>
</div>