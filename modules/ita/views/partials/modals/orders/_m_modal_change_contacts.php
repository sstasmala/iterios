<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="contact-change-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= TranslationHelper::getTranslation('order_change_contact', $lang, 'Change contact') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="conctact-change-popup-form" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/orders/change-contact?id=' . $order['id'] ?>" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <div class="form-group row">
                        <div class="col-12 mb-3 col-lg-5 mb-lg-0">
                            <h6 style="color: #575962;"><?= TranslationHelper::getTranslation('order_current_contact', $lang) ?>:</h6>
                            <input type="text" name="current_contact" class="form-control m-input" value="<?= $contact['last_name'] . ' ' . $contact['first_name'] ?> (ID #<?= $contact['id'] ?>)" placeholder="<?= TranslationHelper::getTranslation('request_current_contact', $lang) ?>" disabled="disabled">
                        </div>
                        <div class="col-12 mb-0 col-lg-2 mb-lg-0">
<!--                            <i class="fa fa-arrow-right" aria-hidden="true" style="font-size: 2rem;"></i>-->
                        </div>
                        <div class="col-12 mb-3 col-lg-5 mb-lg-0">
                            <h6 style="color: #575962;"><?= TranslationHelper::getTranslation('order_new_contact', $lang) ?>:</h6>
                            <select class="form-control m-select2" id="order_change_contact" name="contact_id"></select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="conctact-change-popup-form">
                    <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                </button>
            </div>
        </div>
    </div>
</div>