<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;

?>

<div class="modal fade" id="delete_user_popup" tabindex="-1" role="dialog" aria-labelledby="delete_user_popup__label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="delete_user_popup__label">
                    <?= TranslationHelper::getTranslation('users_delete_popup_title', $lang, 'Dismiss user') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="m-widget4 customize_1">
                            <div class="m-widget4__item">
                                <div class="m-widget4__img m-widget4__img--pic">
                                    <?= Html::img(
                                            'https://keenthemes.com/metronic/preview/assets/app/media/img/users/100_14.jpg',
                                            [
                                                'alt' => "{{USERNAME}}",
                                                'title' => "{{USERNAME}}"
                                            ]);
                                    ?>
                                </div>
                                <div class="m-widget4__info">
                                    <span class="m-widget4__title">
                                        {{USERNAME}}
                                    </span><br> 
                                    <span class="m-widget4__sub">
                                        {{USERPOST}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-4"></div>
                    <div class="col-4 mb-4 mt-4" style="border-bottom: 1px solid #eeeeee;"></div>
                    <div class="offset-4"></div>
                </div>
                <div class="mb-3"></div>
                <div class="row">
                    <div class="col-3 text-center">
                        <span class="m--font-brand m--font-boldest lead">{{NUMBER}}</span>
                        <div class="mb-2"></div>
                        <h6><?= TranslationHelper::getTranslation('user_delete_tourists_counter_title', $lang, 'Tourists') ?></h6>
                    </div>
                    <div class="col-3 text-center">
                        <span class="m--font-info m--font-boldest lead">{{NUMBER}}</span>
                        <div class="mb-2"></div>
                        <h6><?= TranslationHelper::getTranslation('user_delete_reservations_counter_title', $lang, 'Reservations') ?></h6>
                    </div>
                    <div class="col-3 text-center">
                        <span class="m--font-danger m--font-boldest lead">{{NUMBER}}</span>
                        <div class="mb-2"></div>
                        <h6><?= TranslationHelper::getTranslation('user_delete_requests_counter_title', $lang, 'Requests') ?></h6>
                    </div>
                    <div class="col-3 text-center">
                        <span class="m--font-success m--font-boldest lead">{{NUMBER}}</span>
                        <div class="mb-2"></div>
                        <h6><?= TranslationHelper::getTranslation('user_delete_bids_counter_title', $lang, 'Bids') ?></h6>
                    </div>
                </div>
                <div class="mb-3"></div>
                <div class="row">
                    <div class="offset-4"></div>
                    <div class="col-4 mb-4 mt-4" style="border-bottom: 1px solid #eeeeee;"></div>
                    <div class="offset-4"></div>
                </div>
                <div class="form-group row align-items-end">
                    <div class="col-6">
                        <label>
                            <?= TranslationHelper::getTranslation('user_delete_transfer_data', $lang, 'Transfer all data to:') ?>
                        </label>
                        <select class="m-select2" id="user_delete_transfer_to_select" name="User[transfer_data_to_id]"></select>
                    </div>
                    <div class="col-6">
                        <label class="m-checkbox mb-0">
                            <input type="checkbox" id="not_transfer_data" name="User[transfer_data_disable]" value="1">
                            <?= TranslationHelper::getTranslation('user_delete_not_transfer_data', $lang, 'Not transfer data') ?>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('user_delete_tags', $lang, 'Tags:') ?>
                        </label>
                    </div>
                    <div class="col-12">
                        <select class="m-select2" id="user_delete_tags_select" name="User[tags]" multiple></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button type="submit" id="dismiss_user" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('user_delete_dismiss', $lang, 'Dismiss');?>
                </button>
            </div>
        </div>
    </div>
</div>