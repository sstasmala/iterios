<?php

use app\helpers\RbacHelper;
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = \Yii::$app->user->identity->tenant->language->iso;

$this->registerCssFile('@web/css/partials/add_user.min.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
?>

<div class="modal fade" id="create_user_popup" tabindex="-1" role="dialog" aria-labelledby="create_user_popup__label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="create_user_popup__label">
                    <?= TranslationHelper::getTranslation('users_new_popup_title', $lang, 'Create user') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add_exist_user_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/users/create' ?>" method="POST" autocomplete="off">
                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="user_email_select">
                                <?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?>
                            </label>
                        </div>
                        <div class="col-12">
                            <?= Html::input(
                                'text',
                                'User[email]',
                                false,
                                [
                                    'class' => 'form-control m-input',
                                    'id' => 'user_create_email_input',
                                    'placeholder' => 'Email'
                                ])
                            ?>
                        </div>
                    </div>
                    <div id="ita_users" class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="display: none;">
                        <div class="m-demo__preview">
                            <!--begin::Widget 14-->
                            <div class="m-widget4">
                                <br/><br/>
                            </div>
                            <!--end::Widget 14-->
                        </div>
                    </div>
                    <div id="user_info" style="display:none;">
                        <div class="extra_user_create_block">
                            <div class="form-group m-form__group mb-2">
                                <label>
                                    <?= TranslationHelper::getTranslation('field_pass', $lang, 'Password') ?>
                                </label>
                                <div class="input-group">
                                    <?= Html::input(
                                        'text',
                                        'password',
                                        false,
                                        [
                                            'class' => 'form-control m-input password_input_with_generator'
                                        ])
                                    ?>
                                    <div class="input-group-append">
                                        <button class="btn btn-success pass_gen_btn" type="button">
                                            <?= TranslationHelper::getTranslation('generate_password_button', $lang, 'Generate') ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="m-separator m-separator--dashed m-separator--sm"></div>
                            <h5 class="modal-content-block-title mb-4">
                                <?= TranslationHelper::getTranslation('user_create_personal_data_title', $lang, 'Personal data') ?>
                            </h5>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label>
                                                <?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name') ?>
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <?=
                                            Html::input(
                                                'text',
                                                'User[first_name]',
                                                false,
                                                [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'user_create_first_name_input'
                                                ])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label>
                                                <?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <?=
                                            Html::input(
                                                'text',
                                                'User[last_name]',
                                                false,
                                                [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'user_create_last_name_input'
                                                ])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>
                                    <?= TranslationHelper::getTranslation('field_phone', $lang, 'Phone') ?>
                                    </label>
                                </div>
                                <div class="col-12">
                                    <input class="form-control m-input masked-input validate-phone" name="phone" id="company_create"  valid="phoneNumber" />
                                </div>
                            </div>
                            <div class="m-separator m-separator--dashed m-separator--sm"></div>
                        </div>
                    </div>
                    <div id="access" style="display: none;">
                        <div class="row">
                            <div class="col-12">
                                <label for="user_email_select">
                                    <?= TranslationHelper::getTranslation('user_create_access_level_title', $lang, 'Access level') ?>
                                </label>
                            </div>
                            <div class="col-12">
                                <select class="form-control m-bootstrap-select m_selectpicker" name="role">
                                    <option data-hidden="true"></option>
                                    <?php foreach (RbacHelper::getRolesByTenant(\Yii::$app->user->identity->tenant->id) as $role): ?>
                                        <option value="<?= $role->name ?>"><?=  explode('.', $role->name)[1] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button type="submit" form="add_exist_user_form" id="create_user_popup_submit" class="btn btn-primary" disabled="disabled">
                    <?= TranslationHelper::getTranslation('create', $lang, 'Create');?>
                </button>
            </div>
        </div>
    </div>
</div>