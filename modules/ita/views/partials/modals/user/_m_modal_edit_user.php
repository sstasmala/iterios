<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;

?>

<div class="modal fade" id="edit_user_popup" tabindex="-1" role="dialog" aria-labelledby="edit_user_popup__label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit_user_popup__label">
                    <?= TranslationHelper::getTranslation('users_edit_popup_title', $lang, 'Edit user') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edit_user_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/users/update' ?>" method="POST" autocomplete="off">
                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="user_email_edit">
                                <?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?>
                            </label>
                        </div>
                        <div class="col-12">
                            <?=
                                Html::input(
                                    'email',
                                    'User[email]',
                                    '',
                                    [
                                        'class' => 'form-control m-input',
                                        'id' => 'user_email_edit'
                                    ])
                            ?>
                        </div>
                    </div>
                    <div class="form-group m-form__group mb-2">
                        <label>
                            <?= TranslationHelper::getTranslation('field_pass', $lang, 'Password') ?>
                        </label>
                        <div class="input-group">
                            <?= Html::input(
                                    'text',
                                    'password',
                                    '',
                                    [
                                        'class' => 'form-control m-input password_input_with_generator'
                                    ])
                            ?>
                            <div class="input-group-append">
                                <button class="btn btn-success pass_gen_btn" type="button">
                                    <?= TranslationHelper::getTranslation('generate_password_button', $lang, 'Generate') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <h5 class="modal-content-block-title mb-4">
                        <?= TranslationHelper::getTranslation('user_create_personal_data_title', $lang, 'Personal data') ?>
                    </h5>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>
                                        <?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name') ?>
                                    </label>
                                </div>
                                <div class="col-12">
                                    <?=
                                    Html::input(
                                            'text', 'User[first_name]',
                                            '',
                                            [
                                                'class' => 'form-control m-input',
                                                'id' => 'user_edit_first_name_input'
                                            ])
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>
                                        <?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>
                                    </label>
                                </div>
                                <div class="col-12">
                                    <?=
                                    Html::input(
                                                'text', 'User[last_name]',
                                                '',
                                                [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'user_edit_middle_name_input'
                                                ])
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label>
                            <?= TranslationHelper::getTranslation('field_phone', $lang, 'Phone') ?>
                            </label>
                        </div>
                        <div class="col-12">
                            <?=
                            Html::input(
                                    'text',
                                    'User[phone]',
                                    '',
                                    [
                                        'class' => 'form-control m-input validate-phone input_phone_visible',
                                        'id' => 'user_edit_phone_input',
                                        'valid' => 'phoneNumber'
                                    ])
                            ?>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row">
                        <div class="col-12">
                            <label for="user_email_select">
                                <?= TranslationHelper::getTranslation('user_create_access_level_title', $lang, 'Access level') ?>
                            </label>
                        </div>
                        <div class="col-12">
                            <select class="form-control m-bootstrap-select m_selectpicker" name="role" id="user_edit_role_select">
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button type="submit" form="edit_user_form" id="edit_user_popup_submit" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('edit', $lang, 'Edit');?>
                </button>
            </div>
        </div>
    </div>
</div>