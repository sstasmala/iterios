<?php
    use yii\helpers\Html;
    use app\helpers\TranslationHelper;
    
    $lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal right fade" id="add-task-popup" tabindex="-1" role="dialog" aria-labelledby="add-task-modal-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-task-modal-title">
                    <?= TranslationHelper::getTranslation('add_task', $lang, 'Add task') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add_task_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/tasks/create' ?>" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="create_task_input__title">
                                <?= TranslationHelper::getTranslation('tasks_datatable_column_title_title', $lang, 'Title');?>
                            </label>
                            <input type="text" name="Tasks[title]" id="create_task_input__title" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('create_task_input_title_placeholder', $lang, 'Task title');?>" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="create_task_assoc" class="d-block">
                                <?= TranslationHelper::getTranslation('tasks_create_associated', $lang);?>
                            </label>
                            <span id="create_assoc_contact_link" class="d-inline-block text-nowrap mr-3">
                                <i class="fas fa-plus" style="color: grey"></i>
                                <a href="#" id="create_assoc_contact" class="m-link ml-1" onclick="event.preventDefault()">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_contact', $lang) ?>
                                </a> <i class="fa fa-caret-down" style="color: grey"></i>
                            </span>
                            <span id="create_assoc_company_link" class="d-inline-block text-nowrap mr-3">
                                <i class="fas fa-plus" style="color: grey"></i>
                                <a href="#" id="create_assoc_company" class="m-link ml-1" onclick="event.preventDefault()">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_company', $lang) ?>
                                </a> <i class="fa fa-caret-down" style="color: grey"></i>
                            </span>
                            <span id="create_assoc_order_link" class="d-inline-block text-nowrap">
                                <i class="fas fa-plus" style="color: grey"></i>
                                <a href="#" id="create_assoc_order" class="m-link ml-1" onclick="event.preventDefault()">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_order', $lang) ?>
                                </a> <i class="fa fa-caret-down" style="color: grey"></i>
                            </span>
                        </div>
                    </div>
                    <span id="create_assoc_container">
                        <div class="row mb-3" id="create_assoc_contact_div" style="display: none;">
                            <div class="col-12">
                                <label for="create_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_contact', $lang); ?>
                                </label>
                                <select name="Tasks[contact_id]" id="create_task_contact_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                        <div class="row mb-3" id="create_assoc_company_div" style="display: none;">
                            <div class="col-12">
                                <label for="create_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_company', $lang); ?>
                                </label>
                                <select name="Tasks[company_id]" id="create_task_company_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                        <div class="row mb-3" id="create_assoc_order_div" style="display: none;">
                            <div class="col-12">
                                <label for="create_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_order', $lang); ?>
                                </label>
                                <select name="Tasks[order_id]" id="create_task_order_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                    </span>
                    <div class="row">
                        <div class="col-12">
                            <label for="create_task_input__title">
                                <?= TranslationHelper::getTranslation('tasks_datatable_due_date_title', $lang, 'Due Date');?>
                            </label>
                            <div class="row mb-3">
                                <input type="hidden" name="Tasks[due_date]" class="task_due_date_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="due_date" class="task_input__due_date form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="due_time" class="task_input__due_time form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="create_task_input__email_reminder">
                                <?= TranslationHelper::getTranslation('task_edit_modal_email_reminder_title', $lang, 'Email reminder'); ?>
                            </label>
                            <div class="row">
                                <input type="hidden" name="Tasks[email_reminder]" class="email_reminder_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="mail_reminder_date" class="task_input__email_reminder_date form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="mail_reminder_time" class="task_input__email_reminder_time form-control m-input">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="create_task_note_redactor">
                                <?= TranslationHelper::getTranslation('edit_task_note_redactor_title', $lang, 'Notes'); ?>
                            </label>
                            <?= Html::textarea(
                                'Tasks[description]',
                                '',
                                [
                                    'id' => 'create_task_note_textarea',
                                    'class' => 'form-control m-input',
                                    'rows' => '3',
                                    'placeholder' => TranslationHelper::getTranslation('edit_task_note_redactor_placeholder', $lang, 'Your notes...')
                                ])
                            ?>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                    <div class="row">
                        <div class="col-12">
                            <label for="create_task_type_select">
                                <?= TranslationHelper::getTranslation('tasks_datatable_column_type_title', $lang, 'Type'); ?>
                            </label>
                            <select name="Tasks[type_id]" id="create_task_type_select" class="form-control m-select2">
                                <?php if (!empty($default_task_type)): ?>
                                    <option value="<?= $default_task_type->id ?>"><?= $default_task_type->value ?></option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                    <div class="row">
                        <div class="col-12">
                            <label for="create_task_assigned_to_select">
                                <?= TranslationHelper::getTranslation('edit_task_assigned_to_title', $lang, 'Assigned to'); ?>
                            </label>
                            <select name="Tasks[assigned_to_id]" id="create_task_assigned_to_id_select" class="form-control m-select2">
                                <option value="<?= \Yii::$app->user->id ?>"><?= \Yii::$app->user->identity->last_name ?> <?= \Yii::$app->user->identity->first_name ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button type="submit" form="add_task_form" id="add_task_submit" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('create_task_button', $lang, 'Create task');?>
                </button>
            </div>
        </div>
    </div>
</div>