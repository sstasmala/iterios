<?php
    use yii\helpers\Html;
    use app\helpers\TranslationHelper;
    
    $lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal right fade" id="edit-task-popup" tabindex="-1" role="dialog" aria-labelledby="edit-task-modal-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-task-modal-title">
                    <?= TranslationHelper::getTranslation('edit_task', $lang, 'Edit task') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body m-scrollable" data-scrollable="true">
                <form id="edit_task_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/tasks/update' ?>" method="POST">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('task_id', '') ?>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="edit_task_input__title">
                                <?= TranslationHelper::getTranslation('tasks_datatable_column_title_title', $lang, 'Title'); ?>
                            </label>
                            <?= Html::input(
                                    'text',
                                    'Tasks[title]',
                                    '',
                                    [
                                        'placeholder' => TranslationHelper::getTranslation('create_task_input_title_placeholder', $lang, 'Task title'),
                                        'required' => 'required',
                                        'class' => 'form-control m-input',
                                        'id' => 'edit_task_input__title'
                                    ])
                            ?>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="edit_task_assoc">
                                <?= TranslationHelper::getTranslation('tasks_create_associated', $lang);?>
                            </label>
                            <div class="col-12">
                                <span id="edit_assoc_contact_link" class="d-inline-block text-nowrap mr-3">
                                    <i class="fas fa-plus" style="color: grey"></i>
                                    <a href="#" id="edit_assoc_contact">
                                        <?= TranslationHelper::getTranslation('tasks_create_associated_with_contact', $lang) ?>
                                    </a> <i class="fa fa-caret-down" style="color: grey"></i>
                                </span>
                                <span id="edit_assoc_company_link" class="d-inline-block text-nowrap mr-3">
                                    <i class="fas fa-plus" style="color: grey"></i>
                                    <a href="#" id="edit_assoc_company">
                                        <?= TranslationHelper::getTranslation('tasks_create_associated_with_company', $lang) ?>
                                    </a> <i class="fa fa-caret-down" style="color: grey"></i>
                                </span>
                                <span id="edit_assoc_order_link" class="d-inline-block text-nowrap">
                                    <i class="fas fa-plus" style="color: grey"></i>
                                    <a href="#" id="edit_assoc_order">
                                        <?= TranslationHelper::getTranslation('tasks_create_associated_with_order', $lang) ?>
                                    </a> <i class="fa fa-caret-down" style="color: grey"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <span id="edit_assoc_container">
                        <div class="row mb-3" id="edit_assoc_contact_div" style="display: none;">
                            <div class="col-12">
                                <label for="edit_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_contact', $lang); ?>
                                </label>
                                <select name="Tasks[contact_id]" id="edit_task_contact_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                        <div class="row mb-3" id="edit_assoc_company_div" style="display: none;">
                            <div class="col-12">
                                <label for="edit_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_company', $lang); ?>
                                </label>
                                <select name="Tasks[company_id]" id="edit_task_company_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                        <div class="row mb-3" id="edit_assoc_order_div" style="display: none;">
                            <div class="col-12">
                                <label for="edit_task_type_select">
                                    <?= TranslationHelper::getTranslation('tasks_create_associated_with_order', $lang); ?>
                                </label>
                                <select name="Tasks[order_id]" id="edit_task_order_select" class="form-control m-select2"></select>
                            </div>
                        </div>
                    </span>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="create_task_input__title">
                                <?= TranslationHelper::getTranslation('tasks_datatable_due_date_title', $lang, 'Due Date');?>
                            </label>
                            <div class="row">
                                <input type="hidden" name="Tasks[due_date]" class="task_due_date_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="due_date" class="task_input__due_date form-control m-input" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="due_time" class="task_input__due_time form-control m-input" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="create_task_input__email_reminder">
                                <?= TranslationHelper::getTranslation('task_edit_modal_email_reminder_title', $lang, 'Email reminder'); ?>
                            </label>
                            <div class="row">
                                <input type="hidden" name="Tasks[email_reminder]" class="email_reminder_timestamp">
                                <div class="col-7">
                                    <div class="input-group date">
                                        <input type="text" name="email_reminder_date" class="task_input__email_reminder_date form-control m-input" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="input-group time">
                                        <input type="text" name="email_reminder_time" class="task_input__email_reminder_time form-control m-input" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="edit_task_note_redactor">
                                <?= TranslationHelper::getTranslation('edit_task_note_redactor_title', $lang, 'Notes'); ?>
                            </label>
                            <?= Html::textarea(
                                    'Tasks[description]',
                                    '',
                                    [
                                        'id' => 'edit_task_note_textarea',
                                        'class' => 'form-control m-input',
                                        'rows' => '3',
                                        'placeholder' => 'Your notes...'
                                    ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="edit_task_type_select">
                                <?= TranslationHelper::getTranslation('tasks_datatable_column_type_title', $lang, 'Type'); ?>
                            </label>
                            <select name="Tasks[type_id]" id="edit_task_type_select" class="form-control m-select2">
                            </select>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                    <div class="row">
                        <div class="col-12">
                            <label for="edit_task_assigned_to_select">
                                <?= TranslationHelper::getTranslation('edit_task_assigned_to_title', $lang, 'Assigned to'); ?>
                            </label>
                            <select name="Tasks[assigned_to_id]" id="edit_task_assigned_to_id_select" class="form-control m-select2">
                            </select>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button type="button" id="delete_task_button" class="btn btn-danger">
                    <?= TranslationHelper::getTranslation('delete', $lang, 'Delete');?>
                </button>
                <button type="submit" form="edit_task_form" id="edit_task_submit" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('save_changes_button', $lang, 'Save changes');?>
                </button>
            </div>
        </div>
    </div>
</div>