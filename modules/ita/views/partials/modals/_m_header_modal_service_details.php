<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade modal-open" id="m_header_modal_service_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="hsm-create-label">
                    <?= TranslationHelper::getTranslation('mhr_select_service_modal_title', $lang, 'Add service') ?>
                </h5>
                <h5 class="modal-title modal-header__title" id="hsm-update-label">
                    <?= TranslationHelper::getTranslation('mhr_select_service_modal_title_update', $lang, 'Update service') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                    <input type="hidden" name="request_id" value="0"><!-- CHANGE ON REAL REQUEST ID! -->
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <?= TranslationHelper::getTranslation('mhr_select_service_modal_partlet_1_title', $lang, 'Provider and service price') ?>
                                        <small></small>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="header-service-default-details-block"></div>
                        </div>
                    </div>
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm mb-4" id="header-service-custom-details-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" id="header-service-custom-details-portlet-title"></h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="header-service-custom-details-block"></div>
                        </div>
                    </div>
                    <div id="header-service-duplicates">

                    </div>
                    <div class="text-center mb-4">
                        <button type="button" class="btn btn-primary" id="hsm-btn-duplicate">
                        </button>
                    </div>
                    <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm" id="header-service-additional-services-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" id="header-service-additional-services-portlet-title">
                                        <?= TranslationHelper::getTranslation('mhr_select_service_modal_partlet_2_title', $lang, 'Additional services') ?>
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav" id="hsm-add-srv-btn">

                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="container" id="header-service-additional-services-portlet-block">
                            </div>
                            <div class="container" id="header-service-additional-services-details">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer modal-footer--align-space-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="hsm-btn-back" data-toggle="modal" data-target="#m_modal_select_service">
                    <?= TranslationHelper::getTranslation('back', $lang, 'Back') ?>
                </button>
                <button type="button" class="btn btn-primary" id="hsm-btn-next">
                    <?= TranslationHelper::getTranslation('next', $lang, 'Next') ?>
                </button>
                <button type="button" class="btn btn-primary" id="hsm-btn-save" style="display: none">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>