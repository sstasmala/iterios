<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="requisites_help_modal" tabindex="-1" role="dialog" aria-labelledby="requisites_help_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requisites_help_modal_title">
                    <?= TranslationHelper::getTranslation('acc_requisites_help_modal_title', $lang, 'Reference') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <?= TranslationHelper::getTranslation('acc_requisites_help_modal_text', $lang, 'Reference text') ?>
                </p>
            </div>
        </div>
    </div>
</div>