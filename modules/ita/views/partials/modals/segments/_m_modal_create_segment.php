<?php
use app\helpers\TranslationHelper;
use app\models\Segments;
use yii\helpers\Url;

$lang = \Yii::$app->user->identity->tenant->language->iso;
$this->registerJsFile('@web/js/construct/conditions_constructor.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<div class="modal fade" id="create_segment_modal" tabindex="-1" role="dialog" aria-labelledby="segmentCreateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segmentCreateLabel">
                    <?= TranslationHelper::getTranslation('segments_index_add_segment_label', $lang, 'Add segment'); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="segment_create_form" action="<?= Url::toRoute(Yii::$app->user->identity->tenant->id . '/segments/create') ?>" method="POST">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
                    <div class="form-group">
                        <label class="form-control-label">
                            <?= TranslationHelper::getTranslation('segment_create_modal_name_label', $lang, 'Name') ?>:
                        </label>
                        <input type="text" name="Segments[name]" required class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('segment_create_modal_name_placeholder', $lang, 'Enter segment name') ?>">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            <?= TranslationHelper::getTranslation('segment_create_modal_modules_label', $lang, 'Modules') ?>:
                        </label>
                        <?php $modules = Segments::getModules(); ?>
                        <select name="Segments[modules]" id="segment_module_select2" required class="form-control m-select2">
                            <option></option>
                            <?php
                                foreach($modules as $key => $module){
                                    echo '<option value="' . $module . '">' . $module . '</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group segment-rules-block">
                        <input type="hidden" name="Segments[rules]">
                        <label class="form-control-label">
                            <?= TranslationHelper::getTranslation('segment_create_modal_rules_label', $lang, 'Selection rules') ?>:
                        </label>
                        <div id="segment_rules_repeater">
                            <div class="row mb-2 mb-md-0">
                                <div data-repeater-list="SegmentRulesCreate" class="col-12">
                                    <div class="row mb-3 mb-md-0" data-repeater-item>
                                        <div class="col-12 col-md-10 col-lg-11">
                                            <div class="row">
                                                <div class="col-12 col-md-4 mb-3">
                                                    <select name="tag" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('segment_create_modal_tag_placeholder', $lang, 'Select tag') ?>">
                                                        <option value="tag">Tag</option>
                                                        <option value="created_at">Created at</option>
                                                        <option value="birthday">Birthday</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-md-4 mb-3">
                                                    <select name="operator" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('segment_create_modal_operator_placeholder', $lang, 'Select operator') ?>">
                                                        <option value="0">Оператор 1</option>
                                                        <option value="1">Оператор 2</option>
                                                        <option value="2">Оператор 3</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-md-4 mb-3">
                                                    <select name="parameter" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('segment_create_modal_parameter_placeholder', $lang, 'Select parameter') ?>">
                                                        <option value="0"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 1</option>
                                                        <option value="1"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 2</option>
                                                        <option value="2"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 3</option>
                                                        <option value="3"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-2 col-lg-1">
                                            <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <a href="#" class="m-link" data-repeater-create="">
                                        +&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= \app\helpers\TranslationHelper::getTranslation('close_button', $lang, 'Close');?>
                </button>
                <button id="segment_create_submit" type="submit" form="segment_create_form" class="btn btn-primary">
                    <?= \app\helpers\TranslationHelper::getTranslation('save', $lang, 'Save');?>
                </button>
            </div>
        </div>
    </div>
</div>