<?php
use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="segment_contacts_modal" tabindex="-1" role="dialog" aria-labelledby="segmentContactsModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segmentContactsModalTitle">
                    <span class="segment-name"></span>
                    <span> - </span>
                    <?= TranslationHelper::getTranslation('sidt_contacts_table_title', $lang, 'Contacts') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
<!--                <div class="row modal-preloader">
                    <div class="col-12 text-center">
                        <div class="m-loader m-loader--brand" style="width: 30px; height: 30px; display: inline-block;"></div>
                    </div>
                </div>-->
                <div class="m_datatable" id="segments_contacts_datatable"></div>
            </div>
        </div>
    </div>
</div>