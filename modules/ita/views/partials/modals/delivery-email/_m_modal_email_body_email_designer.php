<?php
use app\helpers\TranslationHelper;
$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_email_body_email_designer" tabindex="-1" role="dialog" aria-labelledby="m_modal_email_body_email_designer_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_email_body_email_designer_ModalLabel">
                    <?= TranslationHelper::getTranslation('dess_email_body_select_email_designer_label', $lang, 'Email designer') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    EMAIL DESIGNER
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                </button>
                <button type="button" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save'); ?>
                </button>
            </div>
        </div>
    </div>
</div>