<?php
use app\helpers\TranslationHelper;
$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_email_body_templates" tabindex="-1" role="dialog" aria-labelledby="m_modal_email_body_templates_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="m-portlet m-portlet--tabs mb-0">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?= TranslationHelper::getTranslation('dess_email_body_select_templates_label', $lang, 'Templates') ?>
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_modal_email_body_templates_tab_1" role="tab">
                                    <i class="far fa-file-code"></i>
                                    <?= TranslationHelper::getTranslation('dess_email_body_my_templates_tab', $lang, 'My templates'); ?>
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_modal_email_body_templates_tab_2" role="tab">
                                    <i class="fa fa-image"></i>
                                    <?= TranslationHelper::getTranslation('dess_email_body_gallery_tab', $lang, 'Gallery'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_modal_email_body_templates_tab_1" role="tabpanel">
                            <div id="my_templates_select_block">
                                <ul>
                                    <li class="mb-2">
                                        <a href="#" class="m-link m--font-bolder template-selector">
                                            Шаблон 1
                                        </a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#" class="m-link m--font-bolder template-selector">
                                            Шаблон 2
                                        </a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#" class="m-link m--font-bolder template-selector">
                                            Шаблон 3
                                        </a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#" class="m-link m--font-bolder template-selector">
                                            Шаблон 4
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div id="my_templates_editor_block" class="d-none">
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <textarea name="document-body" id="email-body-template-editor-block"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <button type="button" class="btn btn-secondary" id="my_templates_editor_block__back_btn">
                                            <?= TranslationHelper::getTranslation('back', $lang, 'Back'); ?>
                                        </button>
                                    </div>
                                    <div class="col-8 text-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                                        </button>
                                        <button type="button" id="templates_save_btn" class="btn btn-primary">
                                            <?= TranslationHelper::getTranslation('save', $lang, 'Save'); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="m_modal_email_body_templates_tab_2" role="tabpanel">
                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>