<?php
use app\helpers\TranslationHelper;
$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_email_body_raw_html" tabindex="-1" role="dialog" aria-labelledby="m_modal_email_body_raw_html_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_email_body_raw_html_ModalLabel">
                    <?= TranslationHelper::getTranslation('dess_email_body_select_raw_html_label', $lang, 'Raw html') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-12">
                        <textarea name="document-body" id="email-body-raw-editor-block"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                </button>
                <button type="button" class="btn btn-primary save-raw-html">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save'); ?>
                </button>
            </div>
        </div>
    </div>
</div>