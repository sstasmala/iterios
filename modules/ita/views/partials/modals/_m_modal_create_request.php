<?php

use app\helpers\TranslationHelper;
use app\helpers\MCHelper;
use yii\helpers\Html;
use app\models\RequestType;

$this->registerCssFile('@web/plugins/phone-validator/build/css/intlTelInput.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/plugins/phone-validator/build/js/intlTelInput.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="modal right fade" id="m_modal_create_request" tabindex="-1" role="dialog" aria-labelledby="m_modal_create_request_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m_modal_create_request_title">
                    <?= TranslationHelper::getTranslation('mcr_modal_title', $lang, 'Create request') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="m_modal_create_request_form" autocomplete="off" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . Yii::$app->user->identity->tenant->id . '/requests/create' ?>" method="POST" class="m-form m-form--label-align-right m-form--group-seperator-dashed">
                    <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('contact_id', '') ?>
                    <div class="form-group m-form__group row customer_fields">
                        <div class="col-12 mb-3">
                            <h6 class="form_sub-title"><?= TranslationHelper::getTranslation('customer', $lang, 'Customer') ?>:</h6>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>
                                <?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?>:
                            </label>
                            <input class="form-control m-input masked-input" name="Contacts[email]" id="request_create__contact_email_input" placeholder="<?= TranslationHelper::getTranslation('mcr_email_select_placeholder', $lang, 'Enter customer email') ?>" data-inputmask="'alias': 'email'">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>
                                <?= TranslationHelper::getTranslation('field_phone', $lang, 'Phone') ?>:
                            </label>
                            <input class="form-control m-input masked-input" name="Contacts[phone]" id="request_create__contact_phone_input"  valid="phoneNumber" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>
                                <?= TranslationHelper::getTranslation('field_first_name', $lang, 'First name') ?>:
                            </label>
                            <input type="text" class="form-control m-input" name="Contacts[first_name]" placeholder="<?= TranslationHelper::getTranslation('mcr_first_name_input_placeholder', $lang, 'Enter customer first name') ?>" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>
                                <?= TranslationHelper::getTranslation('field_last_name', $lang, 'Last name') ?>:
                            </label>
                            <input type="text" class="form-control m-input" name="Contacts[last_name]" placeholder="<?= TranslationHelper::getTranslation('mcr_last_name_input_placeholder', $lang, 'Enter customer last name') ?>">
                        </div>
                    </div>

                    <div id="contacts" class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="display: none;">
                        <div class="m-demo__preview">
                            <!--begin::Widget 14-->
                            <div class="m-widget4">
                                <br/><br/>
                            </div>
                            <!--end::Widget 14-->
                        </div>
                    </div>

                    <div id="request_fields" style="display:none;">
                        <div class="form-group m-form__group row">
                            <div class="col-12 mb-3">
                                <h6 class="form_sub-title"><?= TranslationHelper::getTranslation('request', $lang, 'Request') ?>:</h6>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('country', $lang, 'Country') ?>:
                                </label>
                                <select class="form-control m-select2" id="request_create__contact_countries_select" name="Requests[countries][]" multiple></select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_city_select_label', $lang, 'City of departure') ?>:
                                </label>
                                <select class="form-control m-bootstrap-select" id="request_create__contact_city_select" name="Requests[cities][]" multiple></select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_departure_date_datepicker_label', $lang, 'Departure date') ?>:
                                </label>
                                <input type="text" name="Requests[departure_date_range]" id="request_create__contact_departure_date_picker" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('mcr_departure_date_datepicker_placeholder', $lang, 'Enter customer departure date') ?>">
                                <input type="hidden" name="Requests[departure_date_start]" id="request_create__contact_departure_date_picker_start">
                                <input type="hidden" name="Requests[departure_date_end]" id="request_create__contact_departure_date_picker_end">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_trip_duration_inputs_label', $lang, 'Trip duration') ?>:
                                </label>
                                <div class="row">
                                    <div class="col-6">
                                        <input type="number" name="Requests[trip_days_min]" id="request_create__contact_trip_start_date_input" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('mcr_trip_min_days_placeholder', $lang, 'Min') ?> "min="1" max="24">
                                    </div>
                                    <div class="col-6">
                                        <input type="number" name="Requests[trip_days_max]" id="request_create__contact_trip_end_date_input" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('mcr_trip_max_days_placeholder', $lang, 'Max') ?>" min="1" max="24">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_hotel_category_select_label', $lang, 'Hotel category') ?>:
                                </label>
                                <?php
                                    $hotels_categories = MCHelper::searchHotelCategory('', Yii::$app->user->identity->tenant->language->iso);
                                ?>
                                <select class="form-control m-bootstrap-select m_selectpicker" name="Requests[hotel_category][]" multiple>
                                    <?php foreach($hotels_categories as $category){ ?>
                                        <option value="<?= $category['id'] ?>"><?= $category['title'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <?php
                                    $main_currency_id = (isset(\Yii::$app->user->identity->tenant->main_currency)) ? \Yii::$app->user->identity->tenant->main_currency : null;
                                    $main_currency_text = null;
                                    if($main_currency_id){
                                        $mc_data = MCHelper::getCurrencyById($main_currency_id,$lang);
                                        $main_currency_text = (!empty($mc_data)) ? $mc_data[0]['iso_code'] : null;
                                    }
                                    $currencies_data = null;
                                    if(isset(\Yii::$app->user->identity->tenant->usage_currency)){
                                        $usage_currencies = json_decode(\Yii::$app->user->identity->tenant->usage_currency);
                                        if(!empty($usage_currencies)){
                                            $usage_currencies_ids = implode(',', $usage_currencies);
                                            $currencies_data = MCHelper::getCurrencyById($usage_currencies_ids,$lang);
                                        }
                                    }
                                ?>
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_budget_inputs_label', $lang, 'Budget') ?>:
                                </label>
                                <div class="input-group extra-dropdown-option" data-for-input-selector=".hidden-currency_id">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?= $main_currency_text ?>
                                        </button>
                                        <?php if(!is_null($currencies_data) && !empty($currencies_data)){ ?>
                                            <div class="dropdown-menu">
                                                <?php foreach ($currencies_data as $one_curr_type) { ?>
                                                    <a class="dropdown-item" href="#" data-value="<?= $one_curr_type['id'] ?>"><?= $one_curr_type['iso_code'] ?></a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <input type="hidden" class="hidden-currency_id" name="Requests[currency_id]" value="<?= $main_currency_id ?>">
                                    <input type="text" name="Requests[currency_value_min]" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_min_budget_input_paceholder', $lang, 'From') ?>">
                                    <input type="text" name="Requests[currency_value_max]" class="form-control m-input currency_mask" placeholder="<?= TranslationHelper::getTranslation('mcr_max_budget_input_paceholder', $lang, 'To') ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-12 mb-3">
                                <h6 class="form_sub-title"><?= TranslationHelper::getTranslation('tourists_group_title', $lang, 'Tourists group') ?>:</h6>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_adults_person_count_label', $lang, 'Adults persons count') ?>:
                                </label>
                                <select class="form-control m-bootstrap-select m_selectpicker" name="Requests[adults_person_count]">
                                    <?php for($i=1; $i<=10; $i++){ ?>
                                        <option value="<?= $i ?>"<?= ($i===1) ? ' selected' : '' ?>><?= $i ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('mcr_children_count_label', $lang, 'Сhildren count') ?>:
                                </label>
                                <select name="Requests[children_count]" id="request_create__children_count_select" class="form-control m-bootstrap-select m_selectpicker">
                                    <?php for($i=0; $i<=10; $i++){ ?>
                                        <option value="<?= $i ?>"<?= ($i===0) ? ' selected' : '' ?>><?= $i ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="row children-age">
                                    <div class="col-12">
                                        <label><?= TranslationHelper::getTranslation('mcr_children_age_label', $lang, 'Children age') ?>:</label>
                                    </div>
                                    <div class="col-12">
                                        <div class="row generated_inputs_block">
                                            <!-- Generated inuts -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-12 mb-3">
                                <h6 class="form_sub-title"><?= TranslationHelper::getTranslation('mcr_other_label', $lang, 'Other') ?>:</h6>
                            </div>
                            <div class="col-12 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('source', $lang, 'Source') ?>:
                                </label>
                                <?php
                                    $sources = RequestType::find()->where(['system' => false])->translate($lang)->all();
                                ?>
                                <select class="form-control m-bootstrap-select m_selectpicker" id="namecheck" name="Requests[source]">
                                    <option class="bs-title-option" value=""><?= TranslationHelper::getTranslation('choose_source', $lang, 'Choose source') ?></option>
                                    <?php foreach($sources as $source){ ?>
                                        <option value="<?= $source->id ?>"><?= $source->value ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-12 mb-3">
                                <label>
                                    <?= TranslationHelper::getTranslation('description', $lang, 'Description') ?>:
                                </label>
                                <textarea name="Requests[description]" id="request_create__description_textarea" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" form="m_modal_create_request_form" class="btn btn-primary" disabled="disabled">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>