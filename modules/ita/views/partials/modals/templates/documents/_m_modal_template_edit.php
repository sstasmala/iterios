<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;
$lang = Yii::$app->user->identity->tenant->language->iso;
$this->registerJsFile('@web/admin/plugins/ckeditor-full/ckeditor.js', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/templates/documents/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<div class="modal fade" id="m_modal_template_edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <input type="text" name="template_name" id="template_name_input" form="doc_form" placeholder="<?= TranslationHelper::getTranslation('teasi_template_name_placeholder', $lang, 'Enter template name') ?>" class="form-control m-input d-inline-block template_title_input">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="system-template-notice" class="row d-none">
                    <div class="col-12">
                        <div class="m-alert m-alert--icon m-alert--outline alert alert-brand" role="alert">
                            <div class="m-alert__icon">
                                <i class="la la-warning"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong><?= TranslationHelper::getTranslation('tdi_system_doc_message', $lang, 'System template') ?>!</strong> <?= TranslationHelper::getTranslation('tdi_system_doc_descripton', $lang, 'Editing is forbidden') ?>.
                            </div>
                            <div class="m-alert__actions">
                                <a href="#" class="btn btn-outline-brand m-btn m-btn--icon" id="copy_template_btn">
                                    <span>
                                        <i class="fa fa-copy"></i>
                                        <span><?= TranslationHelper::getTranslation('tdi_system_doc_copy_button', $lang, 'Create copy') ?></span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-xl-6 mb-4 mb-xl-0">
                        <form id="doc_form" action="#" method="POST">
                            <input type="hidden" name="template_id" id="template_id_input">
                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                            <h5 class="mb-3">
                                <?= TranslationHelper::getTranslation('tdi_doc_type_label', $lang, 'Template type') ?>:
                            </h5>
                            <select class="form-control m-bootstrap-select m_selectpicker mb-3" id="template_type" name="template-type">
                                <?php foreach ($documents_types as $documents_type): ?>
                                    <option value="<?= $documents_type['id'] ?>"><?= $documents_type['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <h5 class="mb-3">
                                <?= TranslationHelper::getTranslation('tdi_filter_languages', $lang, 'Languages') ?>:
                            </h5>
                            <select class="form-control m-bootstrap-select m_selectpicker mb-3" id="language" name="language">
                                <?php foreach ($languages as $language): ?>
                                    <option value="<?= $language['iso'] ?>"><?= $language['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <h5 class="mb-3">
                                <?= TranslationHelper::getTranslation('tdi_filter_supplier_select_label', $lang, 'Supplier') ?>:
                            </h5>
                            <select class="form-control m-bootstrap-select mb-3" name="supplier" id="supplier">
                            </select>
                            <h5 class="mb-3 mt-3">
                                <?= TranslationHelper::getTranslation('tdi_doc_body_block_title', $lang, 'Document body') ?>:
                            </h5>
                            <textarea name="document-body" id="document-body-template-editor"></textarea>
                        </form>
                    </div>
                    <div class="col-12 col-xl-6">
                        <h5 class="mb-3">
                            <?= TranslationHelper::getTranslation('marks', $lang, 'Marks') ?>:
                        </h5>
                        <div class="form-group mb-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>..." id="template_marks_search">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div id="template_marks_datatable" class="m_datatable"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="button" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>