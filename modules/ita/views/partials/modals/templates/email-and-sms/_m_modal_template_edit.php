<?php
use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="m_modal_template_edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <input type="text" name="template-name" id="template_name_input" form="template_form" placeholder="<?= TranslationHelper::getTranslation('teasi_template_name_placeholder', $lang, 'Enter template name') ?>" class="form-control m-input d-inline-block template_title_input">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-xl-6 mb-4 mb-xl-0">
                        <div id="template-modal-preloader">
                            <div class="m-loader m-loader--primary"></div>
                        </div>
                        <form id="template_form" action="#" method="POST" style="display: none;">
                            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []) ?>
                            <input type="hidden" name="template_id" id="template_id_input">
                            <h5 class="mb-3">
                                <?= TranslationHelper::getTranslation('teasi_doc_type_label', $lang, 'Template type') ?>:
                            </h5>
                            <select class="form-control m-bootstrap-select m_selectpicker mb-3" id="template_type" name="template-type" required disabled title="<?= TranslationHelper::getTranslation('teasi_doc_type_placeholder', $lang, 'Select template type') ?>">
                                <?php foreach ($delivery_types as $delivery_type): ?>
                                    <option value="<?= $delivery_type['id'] ?>" data-delivery-for="<?= $delivery_type['for'] ?>"><?= $delivery_type['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div id="template-theme-block" style="display: none;">
                                <h5 class="mb-3">
                                    <?= TranslationHelper::getTranslation('teasi_doc_theme_label', $lang, 'Template theme') ?>:
                                </h5>
                                <input type="text" id="template-theme-input" name="template-theme" disabled class="form-control m-input mb-3" placeholder="<?= TranslationHelper::getTranslation('teasi_doc_theme_placeholder', $lang, 'Enter template theme') ?>">
                            </div>
                            <div id="template-email-body-block" class="mb-3" style="display: none;">
                                <h5 class="mb-3">
                                    <?= TranslationHelper::getTranslation('teasi_email_body_block_title', $lang, 'Email body') ?>:
                                </h5>
                                <textarea name="template-body" id="email_body_editor"></textarea>
                            </div>
                            <div id="template-sms-body-block" class="mb-3" style="display: none;">
                                <h5 class="mb-3">
                                    <?= TranslationHelper::getTranslation('teasi_sms_body_block_title', $lang, 'SMS body') ?>:
                                </h5>
                                <textarea id="sms_body_editor" name="sms-body" disabled class="form-control m-input mb-2" rows="5" placeholder="<?= TranslationHelper::getTranslation('teasi_sms_body_editor_placeholder', $lang, 'Enter sms body') ?>"></textarea>
                                <div id="sms-body-counter">
                                    <div class="sms-info-item mr-3">
                                        <i class="la la-keyboard-o mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('teasi_sms_delivery_sms_character_length_label', $lang, 'Total') ?>:</span>&nbsp;<span class="length"></span>
                                    </div>
                                    <div class="sms-info-item mr-3">
                                        <i class="la la-flag-checkered mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('teasi_sms_delivery_sms_remain_label', $lang, 'Remain') ?>:</span>&nbsp;<span class="remaining"></span>
                                    </div>
                                    <div class="sms-info-item mr-3">
                                        <i class="la la-envelope mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('teasi_sms_delivery_sms_count_label', $lang, 'Sms') ?>:</span>&nbsp;<span class="messages"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__group form-group">
                                <div class="m-checkbox-list">
                                    <label class="m-checkbox">
                                        <input type="checkbox" name="is_default_template" id="email_sms_template_default" value="1"> <?= TranslationHelper::getTranslation('default', $lang, 'Default') ?>
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-xl-6">
                        <h5 class="mb-3">
                            <?= TranslationHelper::getTranslation('marks', $lang, 'Marks') ?>:
                        </h5>
                        <div class="form-group mb-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('search', $lang, 'Search') ?>..." id="template_marks_search">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div id="template_marks_datatable" class="m_datatable"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="button" class="btn btn-primary">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>