<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="edit_tourist_reminder_readonly_popup" tabindex="-1" role="dialog" aria-labelledby="edit_tourist_reminder_readonly_popup__label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title editable-title" id="edit_tourist_reminder_readonly_popup__label">
                    <span>
                        <span class="text"></span>
                        <i class="la la-pencil ml-1"></i>
                    </span>
                    <input type="text" class="m-input form-control notification_title_input" disabled name="title" value="">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="preloader">
                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                </div>
                <form id="edit_reminder_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/settings/reminders' ?>" method="POST" autocomplete="off">
                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('reminder_id', '', ['id' => 'reminder_id_hidden_input']) ?>
                    
                    <!-- Add "no-days" class or "no-time" class or both of them to trigger constructor block, for hide days/time inputs accordingly -->
                    
                    <div class="trigger-constructor row mb-2">
                        <div class="left-part col col-lg-8">
                            <div class="row">
                                <div class="col col-sm-4">
                                    <h6 class="mb-3"><?= TranslationHelper::getTranslation('notification_popup_operators_change_label', $lang, 'Operators') ?></h6>
                                    <select name="operator" class="form-control m-bootstrap-select m_selectpicker" disabled>
                                        <option value="all"><?= TranslationHelper::getTranslation('all', $lang, 'All') ?></option>
                                        <option value="any"><?= TranslationHelper::getTranslation('any', $lang, 'Any') ?></option>
                                    </select>
                                </div>
                                <div class="col col-sm-4">
                                    <h6 class="mb-3"><?= TranslationHelper::getTranslation('notification_popup_event_change_label', $lang, 'Event') ?></h6>
                                    <select name="event" class="form-control m-bootstrap-select m_selectpicker" disabled>
                                        <option value="1">Event 1</option>
                                        <option value="2">Event 2</option>
                                        <option value="3">Event 3</option>
                                    </select>
                                </div>
                                <div class="col col-sm-4">
                                    <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_notification_period_title', $lang, 'Notification period') ?></h6>
                                    <select name="notification_activate_mode" id="notification_activate_mode_select" class="form-control m-bootstrap-select m_selectpicker" disabled>
                                        <option value="in_event_moment"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_1', $lang, 'In event moment') ?></option>
                                        <option value="one_time"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_2', $lang, 'One time') ?></option>
                                        <option value="days_before" selected="selected"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_3', $lang, 'Days before') ?></option>
                                        <option value="days_after"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_4', $lang, 'Days after') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="right-part col col-lg-4">
                            <div class="row">
                                <div class="days-cell col col-sm-6">
                                    <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_notification_days_placeholder', $lang, 'Days') ?></h6>
                                    <input type="number" class="form-control" id="notification_period_days" disabled placeholder="<?= TranslationHelper::getTranslation('reminders_edit_popup_notification_days_placeholder', $lang, 'Days') ?>">
                                </div>
                                <div class="time-cell col col-sm-6">
                                    <h6 class="mb-3 notification_period_time_heading"><?= TranslationHelper::getTranslation('reminders_edit_popup_notification_time_placeholder', $lang, 'Time') ?></h6>
                                    <input type="text" class="form-control" id="notification_period_time" disabled placeholder="<?= TranslationHelper::getTranslation('reminders_edit_popup_notification_time_placeholder', $lang, 'Notification time') ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_additional_params_title', $lang, 'Additional params') ?></h6>
                            <div class="source_sets_repeater">
                                <div class="row mb-2">
                                    <div data-repeater-list="ReminderTriggersSet" class="col-12">
                                        <div class="row mb-2" data-repeater-item>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select repeater_m_selectpicker" disabled title="<?= TranslationHelper::getTranslation('nothing_selected', $lang, 'Nothing selected') ?>">
                                                    <option value="0"><?= TranslationHelper::getTranslation('tag', $lang, 'Tag') ?></option>
                                                    <option value="1"><?= TranslationHelper::getTranslation('filter_task_date_create', $lang, 'Created at') ?></option>
                                                    <option value="2"><?= TranslationHelper::getTranslation('birthday', $lang, 'Birthday') ?></option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select repeater_m_selectpicker" disabled title="<?= TranslationHelper::getTranslation('nothing_selected', $lang, 'Nothing selected') ?>">
                                                    <option value="0">Оператор 1</option>
                                                    <option value="1">Оператор 2</option>
                                                    <option value="2">Оператор 3</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select repeater_m_selectpicker" disabled title="<?= TranslationHelper::getTranslation('nothing_selected', $lang, 'Nothing selected') ?>">
                                                    <option value="0"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 1</option>
                                                    <option value="1"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 2</option>
                                                    <option value="2"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 3</option>
                                                    <option value="3"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="channel-field row mb-3" data-channel="Email">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?></h6>
                            <input type="text" name="email_theme" class="form-control m-input mb-3" disabled>
                            <textarea name="email_text" id="trmr_email_text_ckeditor"></textarea>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row align-items-end">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">
                                <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                            </button>
                            <button type="button" class="copy_reminder_btn btn btn-default mr-2">
                                <?= TranslationHelper::getTranslation('copy', $lang, 'Copy'); ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>