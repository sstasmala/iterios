<?php

use app\helpers\TranslationHelper;
use yii\helpers\Html;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="edit_agent_reminder_popup" tabindex="-1" role="dialog" aria-labelledby="edit_agent_reminder_popup__label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title editable-title" id="edit_agent_reminder_popup__label">
                    <span>
                        <span class="text"></span>
                        <i class="la la-pencil ml-1"></i>
                    </span>
                    <input type="text" class="m-input form-control notification_title_input" name="title" value="">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="preloader">
                    <div class="m-spinner m-spinner--success m-spinner--lg"></div>
                </div>
                <form id="edit_reminder_form" action="<?= \Yii::$app->params['baseUrl'] . '/ita/' . \Yii::$app->user->identity->tenant->id . '/settings/reminders' ?>" method="POST" autocomplete="off">
                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []) ?>
                    <?= Html::hiddenInput('reminder_id', '', ['id' => 'reminder_id_hidden_input']) ?>
                    <div class="row mb-2">
                        <div class="col-lg-3">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('notification_popup_operators_change_label', $lang, 'Operators') ?></h6>
                            <select name="operator" class="form-control m-bootstrap-select m_selectpicker">
                                <option value="all"><?= TranslationHelper::getTranslation('all', $lang, 'All') ?></option>
                                <option value="any"><?= TranslationHelper::getTranslation('any', $lang, 'Any') ?></option>
                            </select>
                        </div>
                        <div class="col-lg-3 offset-lg-3">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_notification_period_title', $lang, 'Notification period') ?></h6>
                            <select name="notification_activate_mode" id="notification_activate_mode_select" class="form-control m-bootstrap-select m_selectpicker">
                                <option value="1"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_1', $lang, 'In event moment') ?></option>
                                <option value="2"><?= TranslationHelper::getTranslation('notification_activate_mode_select_option_2', $lang, 'One time') ?></option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <h6 class="mb-3 notification_period_time_heading"><?= TranslationHelper::getTranslation('reminders_edit_popup_notification_time_placeholder', $lang, 'Time') ?></h6>
                            <input type="text" class="form-control" id="notification_period_time" placeholder="<?= TranslationHelper::getTranslation('reminders_edit_popup_notification_time_placeholder', $lang, 'Notification time') ?>">
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_source_title', $lang, 'Source') ?></h6>
                            <div id="source_sets_repeater">
                                <div class="row mb-2">
                                    <div data-repeater-list="ReminderTriggersSet" class="col-12">
                                        <div class="row mb-2" data-repeater-item>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select m_selectpicker">
                                                    <option value="1"><?= TranslationHelper::getTranslation('reminders_edit_popup_contacts_module_title', $lang, 'Contact modules') ?></option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select m_selectpicker">
                                                    <option value="0"><?= TranslationHelper::getTranslation('tag', $lang, 'Tag') ?></option>
                                                    <option value="1"><?= TranslationHelper::getTranslation('filter_task_date_create', $lang, 'Created at') ?></option>
                                                    <option value="2"><?= TranslationHelper::getTranslation('birthday', $lang, 'Birthday') ?></option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select m_selectpicker">
                                                    <option value="0">Оператор 1</option>
                                                    <option value="1">Оператор 2</option>
                                                    <option value="2">Оператор 3</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mb-3 mb-lg-0">
                                                <select name="" class="form-control m-bootstrap-select m_selectpicker">
                                                    <option value="0"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 1</option>
                                                    <option value="1"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 2</option>
                                                    <option value="2"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 3</option>
                                                    <option value="3"><?= TranslationHelper::getTranslation('parameter', $lang, 'Parameter') ?> 4</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" title="<?= TranslationHelper::getTranslation('delete', $lang, 'Delete') ?>">
                                                    <i class="la la-remove"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <a href="#" class="m-link" data-repeater-create="">
                                            +&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row">
                        <div class="col-lg-3">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_edit_popup_subscribers_select_title', $lang, 'Subscribers') ?></h6>
                            <select name="subscriber" class="form-control m-bootstrap-select m_selectpicker" id="notification_subscribers_select">
                                <option value="1">Все</option>
                                <option value="2">Владельцы записи</option>
                            </select>
                        </div>
                        <div class="col-lg-9">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('notification_popup_сhannel_change_label', $lang, 'Channel change') ?></h6>
                            <div class="channels-check-block col-12 mb-3 mb-lg-0"></div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="channel-field row" data-channel="Email">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('email_title', $lang, 'Email') ?></h6>
                            <input type="text" name="email_theme" class="form-control m-input mb-3" placeholder="<?= TranslationHelper::getTranslation('reminders_modal_input_placeholder_email_theme', $lang, 'Enter email theme') ?>">
                            <textarea name="email_text" class="form-control m-input reminder-text" rows="5" placeholder="<?= TranslationHelper::getTranslation('reminders_modal_input_placeholder_email_text', $lang, 'Enter email text') ?>"></textarea>
                            <div class="row mt-3">
                                <div class="col-12 text-right">
                                    <i class="flaticon-info editor_help_icon" data-toggle="modal" data-target="#reminder_editor_help_email"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="channel-field row" data-channel="Notice">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_table_notice_column_title', $lang, 'Notice') ?></h6>
                            <textarea name="notice_text" class="form-control m-input reminder-text" rows="5" maxlength="250" placeholder="<?= TranslationHelper::getTranslation('reminders_modal_input_placeholder_notice_text', $lang, 'Enter notice text') ?>"></textarea>
                            <div class="row mt-3">
                                <div class="col-12 text-right">
                                    <i class="flaticon-info editor_help_icon" data-toggle="modal" data-target="#reminder_editor_help_notice"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="channel-field row" data-channel="Task">
                        <div class="col-12">
                            <h6 class="mb-3"><?= TranslationHelper::getTranslation('reminders_table_task_column_title', $lang, 'Task') ?></h6>
                            <input name="task_name"class="m-input form-control mb-3" placeholder="<?= TranslationHelper::getTranslation('reminders_modal_input_placeholder_task_name', $lang, 'Enter task name') ?>">
                            <textarea name="task_text" class="form-control m-input reminder-text" rows="5" placeholder="<?= TranslationHelper::getTranslation('reminders_modal_input_placeholder_task_text', $lang, 'Enter what needs to be done in the task') ?>"></textarea>
                            <div class="row mt-3">
                                <div class="col-12 text-right">
                                    <i class="flaticon-info editor_help_icon" data-toggle="modal" data-target="#reminder_editor_help_task"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--sm"></div>
                    <div class="row align-items-end">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">
                                <?= TranslationHelper::getTranslation('close_button', $lang, 'Close'); ?>
                            </button>
                            <button type="button" class="copy_reminder_btn btn btn-default mr-2">
                                <?= TranslationHelper::getTranslation('copy', $lang, 'Copy'); ?>
                            </button>
                            <button type="submit" form="edit_reminder_form" class="btn btn-primary">
                                <?= TranslationHelper::getTranslation('edit', $lang, 'Edit'); ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>