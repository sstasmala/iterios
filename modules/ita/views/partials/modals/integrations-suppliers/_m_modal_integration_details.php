<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="integration_details_modal" tabindex="-1" role="dialog" aria-labelledby="integrationDetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="integrationDetailsModalLabel">
                    {{Integration name}}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p class="description">
                            <img alt="[SUPPLIER_NAME]" class="logo" src="">
                            <span></span>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('is_inx_features_list_label', $lang, 'Features') ?>:
                        </label>
                        <ul class="features">
                            <li class="feature-item">
                                <i class="fa fa-check m--font-success mr-1"></i>
                                <span class="m--font-bold">
                                    Harder
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('is_inx_stats_list_label', $lang, 'Supplier stats') ?>:
                        </label>
                        <div class="stats-block">
                            <table>
                                <tbody>
                                    <tr class="country">
                                        <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_country_label', $lang, 'Country') ?>:</td>
                                        <td>
                                            <div class="iti-flag ua mr-1"></div>
                                            <span class="country m--font-bold">Ukraine</span>
                                        </td>
                                    </tr>
                                    <tr class="code">
                                        <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_code_label', $lang, 'Code') ?>:</td>
                                        <td>
                                            <span class="m--font-bold">
                                                Teztour
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="company">
                                        <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_company_label', $lang, 'Company') ?>:</td>
                                        <td>
                                            <span class="m--font-bold">
                                                Туроператор
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="website">
                                        <td><?= TranslationHelper::getTranslation('is_inx_integration_stats_website_label', $lang, 'Website') ?>:</td>
                                        <td>
                                            <a href="megatourist.com" class="m-link m--font-bold" rel="nofollow" target="_blank">
                                                megatourist.com.ua
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <label>
                            <?= TranslationHelper::getTranslation('is_inx_integration_params_list_label', $lang, 'Integration params') ?>:
                        </label>
                        <form method="POST" action="" id="integration-detail-form" class="m-form">
                            <div id="supplier-credentials-repeater">
                                <div data-repeater-list="Credentials" class="row cred">
                                    <div class="col-12 mb-3" style="display: none;" data-repeater-item>
                                        <div class="input-group double-inputs-customize">
                                            <div class="input-group-prepend">
                                                <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('ip_inx_api_domain_placeholder', $lang, 'Enter server domain') ?>">
                                            </div>
                                            <input type="text" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('ip_inx_api_key_placeholder', $lang, 'Enter API key') ?>">
                                            <div class="input-group-append">
                                                <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon">
                                                    <i class="la la-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <span href="#" class="m-link" data-repeater-create="" style="cursor: pointer;">+&nbsp;<?= TranslationHelper::getTranslation('add', $lang, 'Add') ?></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <?= TranslationHelper::getTranslation('close_button', $lang, 'Close') ?>
                </button>
                <button type="submit" class="btn btn-primary" form="integration-detail-form">
                    <?= TranslationHelper::getTranslation('save', $lang, 'Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>