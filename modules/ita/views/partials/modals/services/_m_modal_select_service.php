<?php

use yii\helpers\Html;

//LANGUAGE
$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<!-- Begin:Modal #m_modal_select_service -->
<div class="modal fade" id="m_modal_select_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel">
                    Добавление услуги
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body modal-body--first">
                <div class="modal-body__group">
                    <?php foreach ($services as $i => $service) { ?>
                        <a href="#" class="btn-lg m-btn--icon col-2" data-toggle="modal"
                           data-target="#m_modal_service_details" data-dismiss="modal"
                           data-name="<?php print_r($service['name']); ?>">

                            <i class="btn-lg__icon fa <?php print_r($service['icon']); ?>"></i>
                            <span class="btn-lg__text"> <?php print_r($service['name']); ?> </span>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modal-footer__btn" data-dismiss="modal">
                    Отмена
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End:Modal #m_modal_select_service -->