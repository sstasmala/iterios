<!-- BEGIN:Modal #m_modal_service_details -->
<div class="modal fade modal-open" id="m_modal_service_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel">
                    Добавление услуги
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Поставщик и стоимость услуги
                                    <small>
                                        <!-- portlet sub title -->
                                    </small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="container">
                            <div class="row" id="labels">
                                <div class="col-3"><label class="m-input__title">Поставщик</label></div>
                                <div class="col-3"><label class="m-input__title">Цена услуги</label></div>
                                <div class="col-3"><label class="m-input__title">Валюта</label></div>
                                <div class="col-3"><label class="m-input__title">Комиссия</label></div>
                            </div>
                            <div class="row" id="inputs">
                                <div class="col-3">
                                    <div class="input-group m-input-group m-input-group--round">
                                        <select class="form-control m-input m_selectpicker" id="provider" title="- выберите">
                                            <option>
                                                item
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="input-group m-input-group">
                                        <input type="number" class="form-control m-input" placeholder="0" aria-describedby="basic-addon1">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">
                                                UAH
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="input-group m-input-group m-input-group--round">
                                        <select class="form-control m-input m_selectpicker" id="currency">
                                            <option selected>
                                                USD
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="input-group m-input-group m-input-group--round">
                                        <input type="number" class="form-control m-input" placeholder="0" aria-describedby="basic-addon1">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">
                                                %
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="labels">
                                <div class="col-3"><label class="m-input__title">К оплате</label></div>
                                <div class="col-3"></div>
                                <div class="col-3"></div>
                                <div class="col-3"></div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="input-group m-input-group">
                                        <input type="number" class="form-control m-input" placeholder="0" aria-describedby="basic-addon1">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">
                                                UAH
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">

                                </div>
                                <div class="col-3">
                                    <div class="income-money income-money__icon fa flaticon-clipboard"></div>
                                </div>
                                <div class="col-3">
                                    <div class="income-money income-money__info-holder">
                                        <p class="income-money__text">Прибыль</p>
                                        <p class="income-money__text">0.00 USD</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--bordered m-portlet--rounded m-portlet--unair m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text" id="portlet-title"></h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="container">

                        <!-- BEGIN:Modal data -->
                            <?php foreach($services as $service) { ?>
                                <div class="row service-fields" id="<?php echo $service['name'] ?>">
                                    
                                    <?php foreach($service['fields'] as $field) { ?>

                                        <?php if($field['type'] == 'date') { ?>
                                            <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                    <div class="input-group pull-right">
                                                        <input type="date" class="form-control m-input">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'time') { ?>
                                            <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                    <input class="form-control m-input" type="time">
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'dropdown') { ?>
                                            <div class="col-3">
                                                <div class="dropdown">
                                                   <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"
                                                   aria-haspopup="true" aria-expanded="false">
                                                       Dropdown button
                                                   </button>
                                                   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start">
                                                       <a class="dropdown-item" href="#">Action</a>
                                                       <a class="dropdown-item" href="#">Another action</a>
                                                       <a class="dropdown-item" href="#">Something else here</a>
                                                   </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'multiselect') { ?>
                                            <div class="col-3">
                                                <div class="">
                                                    <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                    name="" id="" multiple="multiple">
                                                        <option value="1">Multiselect 1</option>
                                                        <option value="2">Multiselect 2</option>
                                                        <option value="3">Multiselect 3</option>
                                                    <select>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'varchar') { ?>
                                            <div class="col-3">
                                                <div class="">
                                                     <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                     name="" id="">
                                                         <option value="1">Varchar 1</option>
                                                         <option value="2">Varchar 2</option>
                                                         <option value="3">Varchar 3</option>
                                                     <select>
                                                 </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'text') { ?>
                                            <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                    <input class="form-control m-input" type="text">
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'number') { ?>
                                            <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                    <input class="form-control m-input" type="text">
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'link') { ?>
                                            <div class="col-3">
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'radio') { ?>
                                            <div class="col-3">
                                                <div class="m-form__group form-group">
                                                    <div class="m-radio-list">
                                                        <label class="m-radio">
                                                            <input type="radio" name="radio" value="1" checked="checked"> Option 1
                                                            <span></span>
                                                        </label>
                                                        <label class="m-radio">
                                                           <input type="radio" name="radio" value="2"> Option 2
                                                           <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'departure_city') { ?>
                                             <div class="col-3">
                                                <div class="">
                                                    <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                    name="" id="">
                                                        <option value="1">Departure_city 1</option>
                                                        <option value="2">Departure_city 2</option>
                                                        <option value="3">Departure_city 3</option>
                                                    <select>
                                                </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'country') { ?>
                                             <div class="col-3">
                                                 <div class="">
                                                      <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                      name="" id="">
                                                          <option value="1">Country 1</option>
                                                          <option value="2">Country 2</option>
                                                          <option value="3">Country 3</option>
                                                      <select>
                                                  </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'city') { ?>
                                             <div class="col-3">
                                                <div class="">
                                                      <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                      name="" id="">
                                                          <option value="1">City 1</option>
                                                          <option value="2">City 2</option>
                                                          <option value="3">City 3</option>
                                                      <select>
                                                  </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'hotel') { ?>
                                            <div class="col-3">
                                                <div class="">
                                                    <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                    name="" id="">
                                                        <option value="1">Hotel 1</option>
                                                        <option value="2">Hotel 2</option>
                                                        <option value="3">Hotel 3</option>
                                                    <select>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'star_rating') { ?>
                                             <div class="col-3">
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'food') { ?>
                                             <div class="col-3">
                                                <div class="">
                                                     <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                     name="" id="">
                                                         <option value="1">Food 1</option>
                                                         <option value="2">Food 2</option>
                                                         <option value="3">Food 3</option>
                                                     <select>
                                                 </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'airport') { ?>
                                             <div class="col-3">
                                                <div class="">
                                                     <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                     name="" id="">
                                                         <option value="1">Airport 1</option>
                                                         <option value="2">Airport 2</option>
                                                         <option value="3">Airport 3</option>
                                                     <select>
                                                 </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'date_starting') { ?>
                                             <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                     <div class="input-group pull-right">
                                                         <input type="date" class="form-control m-input">
                                                         <div class="input-group-append">
                                                             <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'date_ending') { ?>
                                             <div class="col-3">
                                                <div class="input-group m-input-group m-input-group--round">
                                                    <div class="input-group pull-right">
                                                        <input type="date" class="form-control m-input">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'nights_count') { ?>
                                             <div class="col-3">
                                                <div class="">
                                                     <select class="form-control m-select2 select2-hidden-accessible select2-elems"
                                                     name="" id="">
                                                         <option value="1">Nights_count 1</option>
                                                         <option value="2">Nights_count 2</option>
                                                         <option value="3">Nights_count 3</option>
                                                     <select>
                                                 </div>
                                             </div>
                                        <?php } ?>

                                        <?php if($field['type'] == 'suppliers') { ?>
                                             <div class="col-3">
                                             </div>
                                        <?php } ?>

                                    <?php } ?>
                                </div>
                            <?php } ?>

                        <!-- BEGIN:Modal data end-->

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer--align-space-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" data-toggle="modal" data-target="#m_modal_select_service">
                    Вернуться
                </button>
                <button type="button" class="btn btn-primary">
                    Сохранить
                </button>
            </div>
        </div>
    </div>
</div>
<!-- END:Modal #m_modal_service_details -->