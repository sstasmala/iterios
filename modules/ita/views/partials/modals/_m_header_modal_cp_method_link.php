<?php

use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="header_cp_method_link_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_link" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="header_cp_method_link_modal_title">
                    <?= TranslationHelper::getTranslation('request_change_cp_method_link', $lang, 'Share Email') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <input type="text" id="header_copy_link_input" value="http://php.net/" class="form-control" placeholder="<?= TranslationHelper::getTranslation('request_cp_link_modal_link_input_placeholder', $lang, 'Here must be a link for copy') ?>">
                    <div class="input-group-append">
                        <button id="header_copy_link_button" class="btn btn-success m-btn m-btn--icon" type="button">
                            <span>
                                <i class="fa fa-copy"></i>
                                <span>
                                    <?= TranslationHelper::getTranslation('copy', $lang, 'Copy') ?>
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
                <textarea id="header_copy_bufer"></textarea>
            </div>
        </div>
    </div>
</div>