<?php

use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="cp_method_sms_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('request_change_cp_method_sms', $lang, 'Send SMS') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="cp_sms_form" action="/ENTER-YOUR-ACTION" method="POST">
                    <div class="row mb-3">
                        <div class="col-12 col-md-6 mb-3 mb-md-0">
                            <label for="cp_sms_name_input">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_name_label', $lang, 'SMS name') ?>
                                </span>
                            </label>
                            <input type="text" name="SMS[name]" id="cp_sms_name_input" class="m-input form-control" placeholder="<?= TranslationHelper::getTranslation('request_cp_sms_modal_name_placeholder', $lang, 'Enter SMS name') ?>">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="cp_from_who_input">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_from_who_label', $lang, 'From who') ?>
                                </span>
                            </label>
                            <input type="text" name="SMS[from_who]" id="cp_from_who_input" class="m-input form-control"  value="TravelAgent" placeholder="<?= TranslationHelper::getTranslation('request_cp_sms_modal_from_who_placeholder', $lang, 'Your public, personal or alpha name') ?>" disabled>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-6 mb-3 mb-md-0">
                            <label for="cp_for_who_select">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_for_who_label', $lang, 'For who') ?>
                                </span>
                            </label>
                            <select name="SMS[for_who]" id="cp_for_who_select" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('request_cp_sms_modal_for_who_select_placeholder', $lang, 'Select phone nubmer') ?>">
                                <option>
                                    38(098)566-8722
                                </option>
                                <option>
                                    38(067)653-8734
                                </option>
                                <option data-divider="true"></option>
                                <option data-icon="fas fa-plus mr-1" value="CREATE_NEW_CONTACT">
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_create_new_contact_phone_btn', $lang, 'Create new') ?>
                                </option>
                            </select>
                            <input type="text" name="SMS[new_contact_phone]" id="cp_for_who_input" class="m-input form-control masked-input" placeholder="<?= TranslationHelper::getTranslation('request_cp_sms_modal_for_who_input_placeholder', $lang, 'Enter phone nubmer') ?>" data-inputmask="'mask': '99(999)999-9999'">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="cp_template_select">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_template_label', $lang, 'SMS template') ?>
                                </span>
                            </label>
                            <select name="SMS[template]" id="cp_template_select" class="form-control m-bootstrap-select m_selectpicker" title="<?= TranslationHelper::getTranslation('request_cp_sms_modal_template_placeholder', $lang, 'Select sms template') ?>">
                                <option value="1">
                                    Template 1
                                </option>
                                <option value="2">
                                    Template 2
                                </option>
                                <option value="3">
                                    Template 3
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <label for="cp_sms_body_textarea">
                                <span>
                                    <?= TranslationHelper::getTranslation('request_cp_sms_modal_sms_body_label', $lang, 'Sms body') ?>
                                </span>
                            </label>
                            <textarea id="cp_sms_body_textarea" class="form-control m-input mb-2" name="SMS[body]" rows="3" placeholder="<?= TranslationHelper::getTranslation('request_cp_sms_modal_sms_body_placeholder', $lang, 'Enter sms body') ?>"></textarea>
                            <div id="sms-counter">
                                <div class="sms-info-item mr-3">
                                    <i class="la la-keyboard-o mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('request_cp_sms_modal_sms_character_length_label', $lang, 'Total') ?>:</span>&nbsp;<span class="length"></span>
                                </div>
                                <div class="sms-info-item mr-3">
                                    <i class="la la-flag-checkered mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('request_cp_sms_modal_sms_count_label', $lang, 'Remain') ?>:</span>&nbsp;<span class="remaining"></span>
                                </div>
                                <div class="sms-info-item mr-3">
                                    <i class="la la-envelope mr-1"></i>&nbsp;<span><?= TranslationHelper::getTranslation('request_cp_sms_modal_sms_remain_label', $lang, 'Sms') ?>:</span>&nbsp;<span class="messages"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success m-btn m-btn--icon">
                                <span>
                                    <span style="padding-right: .5em; padding-left: 0;">
                                        <?= TranslationHelper::getTranslation('send_button', $lang, 'Send') ?>
                                    </span>
                                    <i class="fa fa-arrow-right"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>