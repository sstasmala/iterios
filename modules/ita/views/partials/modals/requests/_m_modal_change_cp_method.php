<?php

use app\helpers\TranslationHelper;

$lang = \Yii::$app->user->identity->tenant->language->iso;
?>

<div class="modal fade" id="change_cp_method_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-header__title" id="exampleModalLabel">
                    <?= TranslationHelper::getTranslation('request_change_cp_method_title', $lang, 'Send method') ?>
                </h5>
                <button type="button" class="close modal-header__close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="change-methods-wrap">
                    <div class="row ml-0 mr-0">
                        <div class="cm-item col-12 col-sm-4" data-cp-method="email">
                            <div class="inner">
                                <div class="icon-wrap mb-2 mb-lg-3">
                                    <i class="far fa-envelope"></i>
                                </div>
                                <div class="text-wrap m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('request_change_cp_method_email', $lang, 'Send Email') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="cm-item col-12 col-sm-4" data-cp-method="sms">
                            <div class="inner">
                                <div class="icon-wrap mb-2 mb-lg-3">
                                    <i class="fa fa-mobile" style="font-size: 4.3rem;"></i>
                                </div>
                                <div class="text-wrap m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('request_change_cp_method_sms', $lang, 'Send SMS') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="cm-item col-12 col-sm-4" id="cp_share_link" data-cp-method="link">
                            <div class="inner">
                                <div class="icon-wrap mb-2 mb-lg-3">
                                    <i class="fa fa-unlink"></i>
                                </div>
                                <div class="text-wrap m--font-bold">
                                    <span><?= TranslationHelper::getTranslation('request_change_cp_method_link', $lang, 'Share link') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>