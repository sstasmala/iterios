<?php
/**
 * index.php
 *
 * @copyright © Iterios
 * @author    Valentin Smagluk 1valeks7@gmail.com
 */

$template = new DOMDocument();
$template->loadHTML($data->template);
$template->getElementsByTagName('title')->item(0)->nodeValue = $data->name;

echo $template->saveHTML();