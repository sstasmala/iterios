<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div id="handbooks_data_portlet" class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="fas fa-book-open" id="handbooks_data_portlet_icon"></i>
                </span>
                <h3 class="m-portlet__head-text" id="handbooks_data_portlet_title">
                    <?= TranslationHelper::getTranslation('hbi_page_title', $lang, 'Handbooks') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools d-none">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <div id="handbooks-filter" class="btn-group m-btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-brand" data-filter="all"><?= TranslationHelper::getTranslation('hbi_filter_title_all', $lang, 'All') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="system"><?= TranslationHelper::getTranslation('hbi_filter_title_system', $lang, 'System') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="my"><?= TranslationHelper::getTranslation('hbi_filter_title_my', $lang, 'My') ?></button>
                    </div>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" id="add_handbook_button" class="btn btn-outline-brand m-btn m-btn--icon" data-toggle="modal" data-target="">
                        <span>
                            <i class="la la-plus"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('add', $lang, 'Add') ?>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="handbook-item" id="data_block_city">
            <div class="row mb-4">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="data_block_city_filter_input">
                        <?= TranslationHelper::getTranslation('hbi_filter_by_city_name_label', $lang, 'Filter by city name') ?>
                    </label>
                    <input name="city_filter" id="data_block_city_filter_input" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hbi_filter_by_city_name_placeholder', $lang, 'Enter city name') ?>">
                </div>
                <div class="col-sm-6">
                    <label for="data_block_country_filter_input">
                        <?= TranslationHelper::getTranslation('hbi_filter_by_country_label', $lang, 'Filter by country') ?>
                    </label>
                    <select name="country_filter" id="data_block_country_filter_input" class="form-control m-bootstrap-select"></select>
                </div>
            </div>
            <div class="m_datatable" id="datatable_city"></div>
        </div>
        <div class="handbook-item" id="data_block_hotel">
            <div class="row mb-4">
                <div class="col-sm-6 col-xxl-3 mb-3 mb-xxl-0">
                    <label for="data_block_hotel_name_filter_input">
                        <?= TranslationHelper::getTranslation('hbi_filter_by_hotel_name_label', $lang, 'Filter by hotel name') ?>:
                    </label>
                    <input name="hotel_name" id="data_block_hotel_name_filter_input" class="form-control m-input" placeholder="<?= TranslationHelper::getTranslation('hbi_filter_by_hotel_name_placeholder', $lang, 'Enter hotel name') ?>">
                </div>
                <div class="col-sm-6 col-xxl-3 mb-3 mb-xxl-0">
                    <label for="data_block_hotel_category_filter_select">
                        <?= TranslationHelper::getTranslation('hbi_filter_by_hotel_category_label', $lang, 'Filter by hotel category') ?>:
                    </label>
                    <div id="data_block_hotel_category_filter_select_preloader" class="m-loader m-loader--brand" style="width: 30px; height: 30px; display: block; margin: 0 auto;"></div>
                    <select name="hotel_category" id="data_block_hotel_category_filter_select" class="form-control m-bootstrap-select" title="<?= TranslationHelper::getTranslation('hbi_filter_by_hotel_category_placeholder', $lang, 'Select hotel category') ?>"></select>
                </div>
                <div class="col-sm-6 col-xxl-3 mb-3 mb-sm-0">
                    <label for="data_block_hotel_filter_by_city_select">
                        <?= TranslationHelper::getTranslation('hbi_filter_hotel_by_city_label', $lang, 'Filter by city') ?>:
                    </label>
                    <select name="hotel_city" id="data_block_hotel_filter_by_city_select" class="form-control m-bootstrap-select"></select>
                </div>
                <div class="col-sm-6 col-xxl-3">
                    <label for="data_block_hotel_filter_by_country_select">
                        <?= TranslationHelper::getTranslation('hbi_filter_by_country_label', $lang, 'Filter by country') ?>:
                    </label>
                    <select name="country_filter" id="data_block_hotel_filter_by_country_select" class="form-control m-bootstrap-select"></select>
                </div>
            </div>
            <div class="m_datatable" id="datatable_hotel"></div>
        </div>
        <div class="handbook-item" id="data_block_source">
            <div class="m_datatable" id="datatable_source"></div>
        </div>
        <div class="handbook-item" id="data_block_reminders_orders">
            <div class="m_datatable" id="datatable_reminders_orders"></div>
        </div>
        <div class="handbook-item" id="data_block_reminders_requests">
            <div class="m_datatable" id="datatable_reminders_requests"></div>
        </div>
        <div class="handbook-item" id="data_block_company_type">
            <div class="m_datatable" id="datatable_company_type"></div>
        </div>
        <div class="handbook-item" id="data_block_activity_type">
            <div class="m_datatable" id="datatable_activity_type"></div>
        </div>
        <div class="handbook-item" id="data_block_company_status">
            <div class="m_datatable" id="datatable_company_status"></div>
        </div>
        <div class="handbook-item default text-center">
            <div class="m-nav-grid" id="handbooks_nav_grid">
                <div class="m-nav-grid__row">
                    <a href="#" class="m-nav-grid__item" data-table-name="city">
                        <i class="m-nav-grid__icon fas fa-building"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_city', $lang, 'City') ?>
                        </span>
                    </a>
                    <a href="#" class="m-nav-grid__item" data-table-name="hotel">
                        <i class="m-nav-grid__icon fas fa-h-square"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_hotel', $lang, 'Hotel') ?>
                        </span>
                    </a>
                    <a href="#" class="m-nav-grid__item" data-table-name="source">
                        <i class="m-nav-grid__icon fas fa-server"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_source', $lang, 'Source') ?>
                        </span>
                    </a>
                     <a href="#" class="m-nav-grid__item" data-table-name="reminders_orders">
                        <i class="m-nav-grid__icon fas fa-bell"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_reminders_orders', $lang, 'Orders reminders') ?>
                        </span>
                    </a>
                </div>
                <div class="m-nav-grid__row">
                    <a href="#" class="m-nav-grid__item" data-table-name="reminders_requests">
                        <i class="m-nav-grid__icon fas fa-bell"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_reminders_requests', $lang, 'Requests reminders') ?>
                        </span>
                    </a>
                    <a href="#" class="m-nav-grid__item" data-table-name="company_type">
                        <i class="m-nav-grid__icon fas fa-bookmark"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_company_type', $lang, 'Company type') ?>
                        </span>
                    </a>
                    <a href="#" class="m-nav-grid__item" data-table-name="activity_type">
                        <i class="m-nav-grid__icon fas fa-briefcase"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_activity_type', $lang, 'Activity type') ?>
                        </span>
                    </a>
                    <a href="#" class="m-nav-grid__item" data-table-name="company_status">
                        <i class="m-nav-grid__icon fas fa-crown"></i>
                        <span class="m-nav-grid__text">
                            <?= TranslationHelper::getTranslation('hbi_nav_company_status', $lang, 'Company status') ?>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>