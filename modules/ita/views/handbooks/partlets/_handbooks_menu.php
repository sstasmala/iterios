<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div id="handbooks_nav_portlet" class="m-portlet m-portlet--mobile">
    <div class="m-portlet__body">
        <ul class="m-nav">
            <li class="m-nav__section m-nav__section--first">
                <span class="m-nav__section-text">
                    <?= TranslationHelper::getTranslation('hbi_nav_services', $lang, 'Services') ?>
                </span>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="city">
                    <i class="m-nav__link-icon fas fa-building"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_city', $lang, 'City') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="hotel">
                    <i class="m-nav__link-icon fas fa-h-square"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_hotel', $lang, 'Hotel') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="source">
                    <i class="m-nav__link-icon fas fa-server"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_source', $lang, 'Source') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit">
            </li>
            <li class="m-nav__section">
                <span class="m-nav__section-text">
                    <?= TranslationHelper::getTranslation('hbi_nav_reminders', $lang, 'Reminders') ?>
                </span>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="reminders_orders">
                    <i class="m-nav__link-icon fas fa-bell"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_reminders_orders', $lang, 'Orders reminders') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="reminders_requests">
                    <i class="m-nav__link-icon fas fa-bell"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_reminders_requests', $lang, 'Requests reminders') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit">
            </li>
            <li class="m-nav__section">
                <span class="m-nav__section-text">
                    <?= TranslationHelper::getTranslation('hbi_nav_companies', $lang, 'Companies') ?>
                </span>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="company_type">
                    <i class="m-nav__link-icon fas fa-bookmark"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_company_type', $lang, 'Company type') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="activity_type">
                    <i class="m-nav__link-icon fas fa-briefcase"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_activity_type', $lang, 'Activity type') ?>
                    </span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="#" class="m-nav__link" data-table-name="company_status">
                    <i class="m-nav__link-icon fas fa-crown"></i>
                    <span class="m-nav__link-text">
                        <?= TranslationHelper::getTranslation('hbi_nav_company_status', $lang, 'Company status') ?>
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>