<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('hbi_page_title', $lang, 'Handbooks');
$this->registerCssFile('@web/css/handbooks/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/handbooks/datatables.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/handbooks/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_city');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_hotel');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_source');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_reminders_orders');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_reminders_requests');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_company_type');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_activity_type');
    echo $this->render('/partials/modals/handbooks/m_modal_add_handbook_item_company_status');
$this->endBlock();
?>
<div class="row">
    <div class="col-md-4 col-lg-3">
        <?= $this->render('partlets/_handbooks_menu', []) ?>
    </div>
    <div class="col-md-8 col-lg-9">
        <?= $this->render('partlets/_handbooks_datatable', []) ?>
    </div>
</div>
