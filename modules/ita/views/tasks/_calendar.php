<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row">
    <div class="col-sm-12 col-md-4 col-lg-3">
        <div id="tasks_calendar_filters" class="fc-unthemed">
            <div class='fc-event fc-event-external fc-start m-fc-event--primary m--margin-bottom-15'>
                <div class="fc-title">
                    <div class="fc-content">
                        <label class="m-checkbox m-checkbox--state-primary mb-sm-1 mb-md-0">
                            <input type="checkbox" class="calendar-filter-checkbox" data-type="filter_birthday">
                            <?= TranslationHelper::getTranslation('tasks_calendar_filters_birthday', $lang, 'Birthday') ?>
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class='fc-event fc-event-external fc-start m-fc-event--accent  m--margin-bottom-15'>
                <div class="fc-title">
                    <div class="fc-content">
                        <label class="m-checkbox m-checkbox--state-accent mb-sm-1 mb-md-0">
                            <input type="checkbox" class="calendar-filter-checkbox" data-type="filter_task">
                            <?= TranslationHelper::getTranslation('tasks_calendar_filters_task_complete', $lang, 'Сompleted tasks') ?>
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class='fc-event fc-event-external fc-start m-fc-event--focus  m--margin-bottom-15'>
                <div class="fc-title">
                    <div class="fc-content">
                        <label class="m-checkbox m-checkbox--state-focus mb-sm-1 mb-md-0">
                            <input type="checkbox" class="calendar-filter-checkbox" data-type="filter_request">
                            <?= TranslationHelper::getTranslation('tasks_calendar_filters_request_date_end', $lang, 'Request date end') ?>
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class='fc-event fc-event-external fc-start m-fc-event--warning  m--margin-bottom-15'>
                <div class="fc-title">
                    <div class="fc-content">
                        <label class="m-checkbox m-checkbox--state-warning mb-sm-1 mb-md-0">
                            <input type="checkbox" class="calendar-filter-checkbox">
                            <?= TranslationHelper::getTranslation('tasks_calendar_filters_holiday', $lang, 'Holiday') ?>
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-8 col-lg-9">
        <div id="tasks_reminders_calendar"></div>
    </div>
</div>
