<?php

use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<?php
$this->title = TranslationHelper::getTranslation('tasks_page_title', $lang, 'Tasks');
$this->registerJsFile('@web/js/tasks/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/js/tasks/filters.min.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/css/tasks/index.min.css', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerJsFile('@web/metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js', ['depends' => \app\assets\MetronicAsset::className()]);
$this->registerCSSFile('@web/metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css', ['depends' => \app\assets\MetronicAsset::className()]);
?>

<?php $this->beginBlock('filters-left') ?>
<?= $this->render('_filters-left'); ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('filters-right') ?>
<?= $this->render('_filters-right'); ?>
<?php $this->endBlock(); ?>

<?php
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/task/_m_modal_task_create', ['default_task_type' => $default_task_type]);
echo $this->render('/partials/modals/task/_m_modal_task_edit');
$this->endBlock();
?>

<div class="m-portlet m-portlet--mobile" id="tasks_datatable_portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= TranslationHelper::getTranslation('tasks_page_title', $lang, 'Tasks') ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="head_tools_left">
                <div id="bulk_task_buttons" class="hidden">
                    <button id="remove-tasks" class="btn btn-sm btn-outline-info" title="<?= TranslationHelper::getTranslation('contacts_delete', $lang); ?>">
                        <i class="la la-trash"></i>
                        <?= TranslationHelper::getTranslation('delete', $lang); ?>
                    </button>
                </div>
            </div>
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <div id="tasks-filter" class="btn-group m-btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-brand" data-filter="tasks"><?= TranslationHelper::getTranslation('tasks_page_tasks_filter_label', $lang, 'Tasks') ?></button>
                        <button type="button" class="btn btn-secondary" data-filter="calendar"><?= TranslationHelper::getTranslation('tasks_page_calendar_filter_label', $lang, 'Calendar') ?></button>
                    </div>
                </li>
                <li class="m-portlet__nav-item">
                    <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#add-task-popup">
                        <span>
                            <i class="la la-plus-circle"></i>
                            <span>
                                <?= TranslationHelper::getTranslation('add_task', $lang, 'Add task'); ?>
                            </span>
                        </span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div id="task_data_table" class="m_datatable task-index-portlet-content active" data-content-part-tasks="true"></div>
        <div class="row task-index-portlet-content" data-content-part-calendar="true">
            <?= $this->render('_calendar'); ?>
        </div>
    </div>
</div>