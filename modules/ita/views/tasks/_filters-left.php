<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
?>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="task-open">
        <?= TranslationHelper::getTranslation('tasks_filter_open', $lang) ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="task-overdue">
        <?= TranslationHelper::getTranslation('tasks_filter_overdue', $lang) ?>
    </a>
</div>
<div class="filter-item">
    <a href="#" class="m-link m--font-bold" id="task-completed">
        <?= TranslationHelper::getTranslation('tasks_filter_completed', $lang) ?>
    </a>
</div>