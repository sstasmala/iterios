<?php
$lang = Yii::$app->user->identity->tenant->language->iso;
?>

<div class="row">
    <div class="col-lg-6 filters-right-search">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <!--begin::Item-->
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="true">
                    <span class="m-accordion__item-title m--font-transform-u m--font-bolder">
                        <?= app\helpers\TranslationHelper::getTranslation('tasks_page_title', $lang, 'Tasks') ?>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" name="title" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_task_title', $lang, 'Tasks') ?>">
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_responsible" name="responsible">
                                <option value="<?= \Yii::$app->user->id ?>" selected>
                                    <?= \Yii::$app->user->identity->last_name . ' ' . \Yii::$app->user->identity->first_name ?>
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_author" name="author">
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control m-select2" id="select_type" name="type">
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="input-group" id="m_daterangepicker_completion">
                                <?= \yii\helpers\Html::input('hidden', 'completion_start') ?>
                                <?= \yii\helpers\Html::input('hidden', 'completion_end') ?>
                                <input type="text" class="form-control m-input" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_task_date_completion', $lang) ?>"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" id="m_daterangepicker_create">
                                <?= \yii\helpers\Html::input('hidden', 'create_start') ?>
                                <?= \yii\helpers\Html::input('hidden', 'create_end') ?>
                                <input type="text" class="form-control m-input" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_task_date_create', $lang) ?>"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" id="m_daterangepicker_update">
                                <?= \yii\helpers\Html::input('hidden', 'update_start') ?>
                                <?= \yii\helpers\Html::input('hidden', 'update_end') ?>
                                <input type="text" class="form-control m-input" placeholder="<?= app\helpers\TranslationHelper::getTranslation('filter_task_date_update', $lang) ?>"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
        </div>
    </div>
<!--    <div class="col-lg-6">-->
<!--        Column 2-->
<!--    </div>-->
</div>