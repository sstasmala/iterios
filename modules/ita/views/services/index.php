<?php
/**
 * @var $this \yii\web\View
 */
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = 'Services';

$this->registerCssFile('@web/css/partials/services.min.css',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);
$this->registerJsFile('@web/js/partials/services.min.js',
    ['position' => yii\web\View::POS_END, 'depends' => 'app\assets\MetronicAsset']);

/* Modals */
$this->beginBlock('extra_modals');
echo $this->render('/partials/modals/services/_m_modal_service_details', ['services' => $services]);
$this->endBlock();
?>

<div class="m-portlet m-portlet--full-height">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text"><?= $this->title ?></h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <a href="#" data-toggle="modal" data-target="#m_modal_select_service">
            <i class="m-menu__link-icon flaticon-squares-1"></i>
            <span class="m-menu__link-text">Подборка</span>
        </a>
    </div>
</div>