<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('billig_menu_item', $lang, 'Invoices and payments') . ' - ' . TranslationHelper::getTranslation('my_orders_menu_item', $lang, 'My orders');
$this->registerCssFile('@web/css/billing/my_orders/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/billing/my_orders/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    echo $this->render('/partials/modals/billing/_m_modal_payment_bill');
    echo $this->render('/partials/modals/billing/_m_modal_payment_card');
    echo $this->render('/partials/modals/billing/_m_modal_payment_invoice');
$this->endBlock();
?>

<div class="row">
    <div class="col-sm-12">
        <div class="m-portlet" id="general-billing-portlet">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-xl-4 total-balance-block">
                        <div class="m-widget14">
                            <div class="m-widget14__header mb-0">
                                <h3 class="m-widget14__title">
                                    <?= TranslationHelper::getTranslation('bmo_your_balance_label', $lang, 'Your balance') ?>
                                </h3>
                                <span class="m-widget14__desc">
                                    <?= TranslationHelper::getTranslation('bmo_your_balance_desc', $lang, 'Amount of money in your account') ?>
                                </span>
                            </div>
                            <div class="m-widget25">
                                <span class="m-widget25__price m--font-brand" id="tenant_balance">
                                    0
                                </span>
                                <span class="m-widget25__desc">
                                    <?= TranslationHelper::getTranslation('bmo_total_amount_label', $lang, 'Total amount') ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 replenish-account-block">
                        <div class="m-widget14">
                            <div class="m-widget14__header">
                                <h3 class="m-widget14__title">
                                    <?= TranslationHelper::getTranslation('bmo_replenish_account_label', $lang, 'Replenish an account') ?>
                                </h3>
                                <span class="m-widget14__desc">
                                    <?= TranslationHelper::getTranslation('bmo_replenish_account_desc', $lang, 'Choose your preferred payment method') ?>
                                </span>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 mb-2">
                                    <label class="m-radio mr-3">
                                        <input type="radio" name="payment_method" value="card" checked>
                                        <?= TranslationHelper::getTranslation('bmo_payment_method_card_label', $lang, 'Card') ?>
                                        <span></span>
                                    </label>
                                    <label class="m-radio mr-3">
                                        <input type="radio" name="payment_method" value="bill">
                                        <?= TranslationHelper::getTranslation('bmo_payment_method_bill_label', $lang, 'Bill') ?>
                                        <span></span>
                                    </label>
                                    <label class="m-radio mr-3">
                                        <input type="radio" name="payment_method" value="invoice">
                                        <?= TranslationHelper::getTranslation('bmo_payment_method_invoice_label', $lang, 'Invoice') ?>
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col-sm-12 mb-4">
                                    <div class="input-group">
                                        <input type="text" id="payment_summ_input" name="payment_sum" class="form-control m-input" placeholder="100.00">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                USD
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button type="button" id="pay_btn" class="btn btn-success" disabled>
                                        <span>
                                            <i class="fa fa-coins mr-1"></i>
                                            <span><?= TranslationHelper::getTranslation('bmo_pay_btn_label', $lang, 'Payment') ?></span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 chargeable-features-block">
                        <div class="m-widget14">
                            <div class="m-widget14__header">
                                <h3 class="m-widget14__title">
                                    <?= TranslationHelper::getTranslation('bmo_сhargeable_features_label', $lang, 'Chargeable features') ?>
                                </h3>
                                <span class="m-widget14__desc">
                                    <?= TranslationHelper::getTranslation('bmo_сhargeable_features_desc', $lang, 'Sms and emails remain') ?>
                                </span>
                            </div>
                            <div class="row сhargeable_features_stats">
                                <div class="col-sm-12 mb-3">
                                    <div class="m-widget15__item">
                                        <span class="m-widget15__stats">
                                            65%
                                        </span>
                                        <span class="m-widget15__text">
                                            <?= TranslationHelper::getTranslation('bmo_sms_remain_label', $lang, 'Sms remain') ?>
                                        </span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="m-widget15__item">
                                        <span class="m-widget15__stats">
                                            23%
                                        </span>
                                        <span class="m-widget15__text">
                                            <?= TranslationHelper::getTranslation('bmo_email_remain_label', $lang, 'Email remain') ?>
                                        </span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar bg-warning" role="progressbar" style="width: 23%;" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-xl-7 col-xxl-8">
        <div class="m-portlet m-portlet--full-height" id="my_orders_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <?= TranslationHelper::getTranslation('bmo_my_orders_portlet_title', $lang, 'My orders') ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget11">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>
                                        #
                                    </td>
                                    <td>
                                        <?= TranslationHelper::getTranslation('bmo_rate_name_col_name', $lang, 'Rate name') ?>
                                    </td>
                                    <td>
                                        <?= TranslationHelper::getTranslation('bmo_status_col_name', $lang, 'Status') ?>
                                    </td>
                                    <td>
                                        <?= TranslationHelper::getTranslation('bmo_valid until_col_name', $lang, 'Valid until') ?>
                                    </td>
                                    <td>
                                        <?= TranslationHelper::getTranslation('bmo_days_before_closing_col_name', $lang, 'Days before closing') ?>
                                    </td>
                                    <td>
                                        <?= TranslationHelper::getTranslation('bmo_cost_col_name', $lang, 'Cost') ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="orders_body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-xl-5 col-xxl-4">
        <div class="m-portlet m-portlet--fit" id="transactions_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <?= TranslationHelper::getTranslation('bmo_transactions_portlet_title', $lang, 'Transactions') ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget4 m-widget4--chart-bottom" id="transactions_list">
                </div>
                <div class="m-datatable m-datatable--default text-center">
                    <div class="m-datatable__pager">
                        <ul class="m-datatable__pager-nav mt-0" id="transactions_pagination">
                            <li>
                                <a title="<?= TranslationHelper::getTranslation('pagination_first', $lang, 'First') ?>" class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" data-page="1">
                                    <i class="la la-angle-double-left"></i>
                                </a>
                            </li>
                            <li>
                                <a title="<?= TranslationHelper::getTranslation('pagination_previous', $lang, 'Previous') ?>" class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" data-page="1">
                                    <i class="la la-angle-left"></i>
                                </a>
                            </li>
                            <li>
                                <a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="2" title="2">2</a>
                            </li>
                            <li>
                                <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active" data-page="3" title="3">3</a>
                            </li>
                            <li>
                                <a class="m-datatable__pager-link m-datatable__pager-link-number" data-page="4" title="4">4</a>
                            </li>
                            <li>
                                <a title="<?= TranslationHelper::getTranslation('pagination_next', $lang, 'Next') ?>" class="m-datatable__pager-link m-datatable__pager-link--next" data-page="2">
                                    <i class="la la-angle-right"></i>
                                </a>
                            </li>
                            <li>
                                <a title="<?= TranslationHelper::getTranslation('pagination_last', $lang, 'Last') ?>" class="m-datatable__pager-link m-datatable__pager-link--last" data-page="35">
                                    <i class="la la-angle-double-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>