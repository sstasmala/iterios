<?php
use app\helpers\TranslationHelper;

$lang = Yii::$app->user->identity->tenant->language->iso;
$this->title = TranslationHelper::getTranslation('billig_menu_item', $lang, 'Invoices and payments') . ' - ' . TranslationHelper::getTranslation('rates_menu_item', $lang, 'Rates');
$this->registerCssFile('@web/css/billing/rates/index.min.css', ['depends' => \app\assets\MainAsset::className()]);
$this->registerJsFile('@web/js/billing/rates/index.min.js', ['depends' => \app\assets\MetronicAsset::className()]);

/* Modals */
$this->beginBlock('extra_modals');
    // Render modals here...
$this->endBlock();
?>

<div class="m-pricing-table-4" id="billing-rates-table">
    <div class="m-pricing-table-4__top">
        <div class="m-pricing-table-4__top-container m-pricing-table-4__top-container--fixed">
            <div class="m-pricing-table-4__top-header">
                <div class="m-pricing-table-4__top-title m--font-light">
                    <h1>
                        <?= TranslationHelper::getTranslation('br_rates_table_title', $lang, 'Rates') ?>
                    </h1>
                </div>
            </div>
            <div class="m-pricing-table-4__top-body">
                <div class="m-pricing-table-4__top-items">
                    <?php foreach ($tariffs as $tariff):?>
                    <div class="m-pricing-table-4__top-item">
                        <span class="m-pricing-table-4__icon m--font-info">
                            <i class="fa flaticon-rocket"></i>
                        </span>
                        <h2 class="m-pricing-table-4__subtitle">
                            <?=$tariff['name']?>
                        </h2>
                        <div class="m-pricing-table-4__features">
                            <span>
                              <?=$tariff['description']?>
                            </span>
                        </div>
                        <span class="m-pricing-table-4__price">
                             <?=$tariff['price']?>
                        </span>
                        <span class="m-pricing-table-4__label">
                            USD
                        </span>
                        <div class="m-pricing-table-4__btn">
                            <?php if($tariff['current']):?>
                                <button type="button" class="btn m-btn--pill btn-success m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg">
                                    <?= TranslationHelper::getTranslation('br_current_rate_btn_label', $lang, 'Current') ?>
                                </button>
                            <?php else:?>
                                <button type="button" class="btn m-btn--pill  btn-info m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg">
                                    <?= TranslationHelper::getTranslation('br_purchase_btn_label', $lang, 'Purchase') ?>
                                </button>
                            <?php endif;?>
                        </div>
                        <div class="m-pricing-table-4__top-items-mobile">
                            <?php foreach ($features as $f):?>
                            <div class="m-pricing-table-4__top-item-mobile">
                                <span>
                                    <?=$f['name']?>
                                </span>
                                <br>
                                <span>
                                    <?php
                                        if($tariff['features'][$f['id']] === false)
                                            echo 'No';
                                        else
                                        if($tariff['features'][$f['id']] === true)
                                            echo 'Yes';
                                        else
                                        if($tariff['features'][$f['id']] === null)
                                            echo 'Unlimited';
                                        else
                                            echo  $tariff['features'][$f['id']]
                                    ?>
                                </span>
                            </div>
                            <?php endforeach;?>
                            <?php if($tariff['current']):?>
                            <button type="button" class="btn m-btn--pill btn-success m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg">
                                <?= TranslationHelper::getTranslation('br_current_rate_btn_label', $lang, 'Current') ?>
                            </button>
                            <?php else:?>
                            <div class="m-pricing-table-4__top-btn">
                                <button type="button" class="btn m-btn--pill  btn-info m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg">
                                    <?= TranslationHelper::getTranslation('br_purchase_btn_label', $lang, 'Purchase') ?>
                                </button>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
    <div class="m-pricing-table-4__bottom">
        <div class="m-pricing-table-4__bottom-container m-pricing-table-4__bottom-container--fixed">
            <?php foreach ($features as $feature):?>
            <div class="m-pricing-table-4__bottom-items">
                <div class="m-pricing-table-4__bottom-item">
                    <?= $feature['name']?>
                </div>
                <?php foreach ($tariffs as $tariff):?>
                <div class="m-pricing-table-4__bottom-item">
                    <?php
                    if($tariff['features'][$feature['id']] === false)
                        echo 'No';
                    else
                        if($tariff['features'][$feature['id']] === true)
                            echo 'Yes';
                        else
                            if($tariff['features'][$feature['id']] === null)
                                echo 'Unlimited';
                            else
                                echo  $tariff['features'][$feature['id']]
                    ?>
                </div>
                <?php endforeach;?>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>