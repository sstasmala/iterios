<?php

namespace app\modules\webhooks\controllers;
use yii\web\Response;

/**
 * LiqPayController.php
 * @copyright ©Iterios
 * @author Valentin Stepanenko catomik13@gmail.com
 */
class LiqPayController extends \app\modules\webhooks\controllers\base\WebhooksBaseController
{
    public function actionIndex()
    {
        $params = \Yii::$app->params['payment_providers'];
        $params = \array_filter($params,function($item){return $item['namespace']== 'utilities\payment_providers\LiqPay';});
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $params;
    }
}